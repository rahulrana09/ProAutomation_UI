package DataStructure.LinkedList;

import org.testng.annotations.Test;

/**
 * Created by rahulrana on 08/01/19.
 */
public class MergeSort {

    Node head;

    class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    Node getMiddle(Node h) {

        if (h != null) {
            Node sPoint = h;
            Node fPoint = h.next;

            while (fPoint != null && fPoint.next != null) {
                fPoint = fPoint.next.next;
                sPoint = sPoint.next;
            }
            return sPoint;
        } else {
            return h;
        }
    }

    void printLinkedList(Node n) {
        System.out.print("\n");
        while (n != null) {
            System.out.print(n.data + "\n");
            n = n.next;
        }
    }

    Node mergeSort(Node h){
        if(h == null || h.next == null)
            return h;

        Node middle = getMiddle(h);
        Node nextOfMiddle = middle.next;

        // set next of middle node to null
        middle.next = null;

        // recursion
        Node left = mergeSort(h);
        Node right = mergeSort(nextOfMiddle);

        Node merged = mergeProcedure(left, right);
        return merged;
    }

    Node mergeProcedure(Node a, Node b){
        Node result = null;

        if(a == null)
            return b;

        if(b == null)
            return a;

        if(a.data <= b.data){
            result = a;
            result.next = mergeProcedure(a.next, b);
        }else{
            result = b;
            result.next = mergeProcedure(a, b.next);
        }


        return result;

    }

    @Test
    public void run() {
        MergeSort lList = new MergeSort();
        lList.head = new Node(1);
        lList.head.next = new Node(4);
        lList.head.next.next = new Node(3);
        lList.head.next.next.next = new Node(2);
        lList.head.next.next.next.next = new Node(9);
        lList.head.next.next.next.next.next = new Node(5);
        lList.mergeSort(lList.head);
        lList.printLinkedList(lList.head);
    }
}
