package DataStructure.LinkedList;

import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by rahulrana on 09/01/19.
 */
public class SwapNodes {

    Node head;

    class Node {
        int data;
        Node next;

        Node(int d) {
            data = d;
            next = null;
        }
    }

    public void addLast(int d) {
        SwapNodes.Node newNode = new SwapNodes.Node(d);
        SwapNodes.Node temp = head;

        if (head == null) {
            head = newNode;
            return;
        }

        // traverse to the end of Linked List and add the new node
        while (temp.next != null) {
            temp = temp.next;
        }

        temp.next = newNode;
    }

    public void printList(Node n) {
        System.out.print("\n");
        Node temp = n;
        while (n != null) {
            System.out.print(n.data + "\n");
            n = n.next;
        }
    }

    void swapNodesAndReturnHead(int x, int y) {
        if (x == y)
            return;

        // search for x and keep track of previous node
        Node prevX = null;
        Node currentX = head;
        while (currentX != null && currentX.data != x) {
            prevX = currentX;
            currentX = currentX.next;
        }

        // search for y and keep track of previous node
        Node prevY = null;
        Node currentY = head;
        while (currentY != null && currentY.data != y) {
            prevY = currentY;
            currentY = currentY.next;
        }

        if (currentX == null || currentY == null)
            return; // either data is not present

        // if x is not head of linked list
        if (prevX != null)
            prevX.next = currentY;
        else
            head = currentY;

        // if y is not head
        if(prevY != null)
            prevY.next = currentX;
        else
            head = currentX;

        // swap the next pointers
        Node temp = currentX.next;
        currentX.next = currentY.next;
        currentY.next = temp;
    }


    @Test
    public void run() {
        SwapNodes sList = new SwapNodes();

        sList.head = new SwapNodes.Node(10);
        sList.addLast(20);
        sList.addLast(15);
        sList.addLast(4);
        sList.addLast(2);
        printList(sList.head);
        sList.swapNodesAndReturnHead(20, 4);
        printList(sList.head);

        List<Integer>

    }


}

