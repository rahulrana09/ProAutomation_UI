package DataStructure.LinkedList;

import org.testng.annotations.Test;

/**
 * Created by rahulrana on 04/01/19.
 */
public class LinkListTest_01 {

    @Test
    public void run() {
        LinkedList lList = new LinkedList();

        lList.head = new LinkedList.Node(1);
        LinkedList.Node second = new LinkedList.Node(2);
        LinkedList.Node third = new LinkedList.Node(3);
        lList.head.next = second;
        second.next = third;

        lList.push(4);
        lList.addLast(5);
        lList.addLast(9);
        lList.insertAfter(lList.head.next.next, 6);
        lList.deleteKey(5);
        lList.deleteNode(4);

        //lList.head.next.next.next =lList.head;
        System.out.print("\nHas Loop: " + lList.detectLoopInALinkedList(lList.head)+"\n");
        System.out.print("\nHas Loop: " + lList.detectLoopUsingSlowAndFastPointer()+"\n");
        System.out.print("\nLoop Size: " + lList.getLoopLength()+"\n");

        // print
        lList.printLinkedList(lList.head);
        System.out.print("\nCount - " + lList.getCount());
        System.out.print("\n" + lList.searchNode(lList.head, 1));
        System.out.print("\n" + lList.searchNodeRec(lList.head, 1));
        System.out.print("\n" + lList.getNthNode(1));
        System.out.print("\n" + lList.getNthFromLast(1)+"\n");
        System.out.print("\nTimes: " + lList.keyOccurence(9)+"\n");

        lList.printNthFromLastUsingTwoReference(1);
        lList.printMiddleUsingSlowAndFastPointer();
        lList.reverseLinkedList(lList.head);
        lList.printLinkedList(lList.head);




    }


}
