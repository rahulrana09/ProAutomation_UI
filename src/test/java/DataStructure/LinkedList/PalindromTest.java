package DataStructure.LinkedList;

import org.testng.annotations.Test;

/**
 * Created by rahulrana on 04/01/19.
 */
public class PalindromTest {
    Node head, left;
    Node sPoint, fPoint, secondHalf;

    class Node {
        String data;
        Node next;

        Node(String str) {
            this.data = str;
            this.next = null;
        }
    }

    public void addLast(String str) {
        Node newNode = new Node(str);
        Node temp = head;

        if (head == null) {
            head = newNode;
            return;
        }

        // traverse to the end of Linked List and add the new node
        while (temp.next != null) {
            temp = temp.next;
        }

        temp.next = newNode;
    }

    public void printList(Node n) {
        System.out.print("\n");
        Node temp = n;
        while (n != null) {
            System.out.print(n.data + "\n");
            n = n.next;
        }
    }



    public boolean isPalindrome(Node head) {
        sPoint = head;
        fPoint = head;
        Node psPoint = head; // previous of slow pointer
        Node midNode = null; // to handle odd size of the list
        boolean result = true;

        if (head != null && head.next != null) { // make sure list is not empty
            // get the middle of the list
            while (fPoint != null && fPoint.next != null) {
                fPoint = fPoint.next.next;
                psPoint = sPoint; // this is required for odd length linked list
                sPoint = sPoint.next;
            }

            // if there are even nodes fPoint will become null, else store the mid value
            if (fPoint != null) {
                midNode = sPoint;
                sPoint = sPoint.next;
            }

            // now reverse the second half and compare with the first half
            secondHalf = sPoint;
            psPoint.next = null; // this is to clear the 2nd half of th list

            reverse();
            result = compare(head, secondHalf);

            // re construct the original list
            reverse();

            if (midNode != null) {
                // if mid node exist then add the mid node to the previous slow node
                psPoint.next = midNode;
                // connect the second half
                midNode.next = secondHalf;
            } else {
                psPoint.next = secondHalf;
            }
        }

        return result;
    }

    public void reverse() {
        Node current = secondHalf;
        Node previous = null;
        Node next;

        while (current != null) {
            next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }
        secondHalf = previous;
    }

    public boolean compare(Node h1, Node h2) {

        Node t1 = h1;
        Node t2 = h2;

        while (t1 != null & t2 != null) {
            if (t1.data == t2.data) {
                t1 = t1.next;
                t2 = t2.next;
            }else{
                return false;
            }
        }

        if(t1==null && t2==null){
            return true;
        }


        return false;

    }

    public boolean isPalindromUtil(Node right){
        left = head;

        if(right == null)
            return true; // if right is null stop recursion

        /*
        If sub list is not palindrom, no need to proceed further
         */
        boolean isp = isPalindromUtil(right.next);
        if(isp == false)
            return false;

        /*
        Check values of current left and current right
         */
        boolean ispl = (right.data == left.data);
        left = left.next;


        return ispl;
    }

    public boolean isPlindromeRec(Node head){
        return isPalindromUtil(head);
    }

    @Test
    public void run() {
        PalindromTest sList = new PalindromTest();

        sList.head = new Node("r");
        sList.addLast("a");
        sList.addLast("d");
        sList.addLast("a");
        sList.addLast("r");
        printList(sList.head);

        boolean isp = isPalindrome(sList.head);
        System.out.print("\n is linked list a palindrome: "+ isp);

        boolean ispr = isPlindromeRec(sList.head);
        System.out.print("\n Rec, is linked list a palindrome: "+ ispr);

        printList(sList.head);


    }


}
