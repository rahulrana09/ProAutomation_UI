package scripts.demoScripts;

/**
 * Created by rahulrana on 12/04/19.
 */
abstract class B1 {
    int a, b;

    abstract int getA();

    abstract int getB();
}
