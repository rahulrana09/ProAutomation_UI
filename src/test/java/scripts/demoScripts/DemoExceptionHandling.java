package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.utility.common.Assertion;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import scripts.TestInit;

import javax.naming.ldap.ExtendedRequest;

/**
 * Created by rahulrana on 31/01/18.
 */
public class DemoExceptionHandling extends TestInit {

    @BeforeClass(alwaysRun = true)
    public void run() {

        try {
            int[] arr = new int[2];
            arr[0] = 10;
            arr[1] = 20;
            System.out.print(arr[1]);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test
    public void run1() {
        ExtentTest t1 = pNode.createNode("Test", "Description");

        try{
            int[] arr = new int[2];
            arr[0] = 10;
            arr[1] = 20;
            System.out.print(arr[2]);
        }catch (Exception e){
            markTestAsFailure(e, t1);
        }

    }
}
