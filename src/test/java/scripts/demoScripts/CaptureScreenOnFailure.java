package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageForLinkedInRegistration_pg1;
import framework.utility.Assertions;
import framework.utility.common.App;
import framework.utility.common.DriverFactory;
import org.testng.annotations.Test;
import scripts.TestInit;

/**
 * Created by rahulrana on 13/02/18.
 */
public class CaptureScreenOnFailure extends TestInit{

    @Test
    public void captureScreenOnFailure() throws Exception {
        // create extent test
        ExtentTest t1 = pNode.createNode("test-01",
                "Capture Screen On Failure And Attach to Extent Reports");

        // open the application
        App.init(t1)
                .openApplication();

        String pageTitle = DriverFactory.getDriver().getTitle();

        Assertions.verifyEqual(pageTitle, "Linked Wrong Page", "Verify Page Title", t1);

        PageForLinkedInRegistration_pg1.init(t1)
                .setFirstName("FirstName");


    }
}
