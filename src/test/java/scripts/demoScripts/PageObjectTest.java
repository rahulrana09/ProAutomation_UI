package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.login.LinkedInLoginPage;
import framework.utility.common.DriverFactory;
import framework.utility.globalConst.ConfigInput;
import org.testng.annotations.Test;
import scripts.TestInit;

import java.io.IOException;

/**
 * Created by rahulrana on 17/11/17.
 */
public class PageObjectTest extends TestInit {

    @Test
    public void linkedIn_Login_Test() throws IOException {
        ExtentTest t1 = pNode.createNode("linkedIn_Login_Test", "Using Page Object");

        // open linked in page1
        DriverFactory.getDriver().get(ConfigInput.url);
        t1.info("Navigate to Linked IN Page");

        // try login
        LinkedInLoginPage.init(t1)
                .setEmailOrPhone("rahul.rana.1009")
                .setPassword("Passwordxyz")
                .clickSubmit();

    }
}
