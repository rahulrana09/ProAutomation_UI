package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.objectIdentification.ObjectIdentificationPage;
import framework.utility.common.App;
import framework.utility.common.Navigation;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import scripts.TestInit;

/**
 * Created by rahulrana on 26/01/18.
 */
public class ObjectIdentification extends TestInit {

    /**
     * This test is to demo Object identification using Selenium WebDriver Locators
     */
    @Test
    public void demoObjectIdentificationUsingWebDriverLocators(){
        ExtentTest t1 = pNode.createNode("Demo Object Identification",
                "Demonstrate use of selenium WebDriver locators");

        // open application
        App.init(t1)
                .openApplication();

        // navigate to or identification exercise
        Navigation.init(t1)
                .toSyncObjectIdentificationTest();

        WebElement table = ObjectIdentificationPage.init(t1).getTable();

        System.out.print("Debugger");
    }

}
