package scripts.demoScripts;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.cascadeSelectBox.CascadeSelectBoxs_pg1;
import framework.pageObjects.userWizard.UserWizard_pg1;
import framework.pageObjects.userWizard.UserWizard_pg2;
import framework.pageObjects.alertDemo.DynamicAlert_pg1;
import framework.utility.common.DriverFactory;
import framework.utility.common.Navigation;
import framework.utility.globalConst.ConfigInput;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import scripts.TestInit;

import java.io.IOException;
import java.sql.Driver;

/**
 * Created by rahulrana on 07/12/17.
 */
public class SynchronizationTest extends TestInit {

    /**
     * Navigation Test
     * Fix the Issue with Left Navigation
     *
     * @throws IOException
     */
    @Test
    public void syncTest_01() {
        ExtentTest t1 = pNode.createNode("Test1", "Test Navigation");
        DriverFactory.getDriver().get(ConfigInput.url);

        // Navigate to Explicit wait Test 01
        Navigation.init(t1)
                .toSyncExplicitWaitTest1();

        // Navigate to Explicit wait Test 02
        Navigation.init(t1)
                .toSyncExplicitWaitTest2();

        // Navigate to Fluent wait Test
        Navigation.init(t1)
                .toSyncTutorialFluentWaitTest();
    }

    @Test
    public void userRegistration() {
        ExtentTest t2 = pNode.createNode("Test2", "Test azax user registration page using explicit wait.");
        DriverFactory.getDriver().get(ConfigInput.url);

        // Navigate to Explicit wait Test 01
        Navigation.init(t2)
                .toSyncExplicitWaitTest1();

        UserWizard_pg1.init(t2)
                .setFirstName("spider")
                .setLastName("parker")
                .clickNext();

        UserWizard_pg2.init(t2)
                .setEmail("spiderman@yahoo.com")
                .setPassword("pass@123")
                .setConfirmPassword("pass@123")
                .clickComplete();

        String message = UserWizard_pg2.init(t2)
                .getSuccessmessage();
        System.out.print("DATA: " + message + "\n");
    }

    @Test
    public void checkDynamicAlert() throws IOException {
        ExtentTest t3 = pNode.createNode("Test3", "Test Dynamic Alert using Explicit wait.");

        DriverFactory.getDriver().get(ConfigInput.url);

        Navigation.init(t3)
                .toSyncExplicitWaitTest2();

        DynamicAlert_pg1.init(t3)
                .clickOnBtnGetAlert()
                .clickOnOkay();
    }

    @Test
    public void checkCascadeSelect() {
        ExtentTest t4 = pNode.createNode("Test4", "Test Cascaded select boxes using fluent wait.");

        DriverFactory.getDriver().get(ConfigInput.url);

        Navigation.init(t4)
                .toSyncTutorialFluentWaitTest();

        CascadeSelectBoxs_pg1.init(t4)
                .selectComics("Marvel Studios")
                .selectCharacter("Iron Man");

    }
}
