package framework.utility.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.utility.globalConst.ConfigInput;
import framework.utility.propertiesManager.MessageReader;
import framework.utility.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rahul Rana on 30-05-2017.
 */
public class Assertion {
    private static WebDriverWait wait;
    private static WebDriver driver;
    private static SoftAssert sAssert;
    private static ExtentTest pNode;

    public Assertion(WebDriver driver, ExtentTest t1) {
        this.driver = driver;
        sAssert = new SoftAssert();
        pNode = t1;
        wait = new WebDriverWait(driver, ConfigInput.explicitWait);
    }


    /**
     * Get Action Message
     *
     * @return
     */
    public static String getActionMessage() throws Exception {
        return wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("success_message")))).getText();
    }

    public static String getErrorMessage() throws Exception {
        return wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.className("error")))).getText();
    }


    /**
     * Verify for error message
     *
     * @param expectedCode - expected code as present in Property resource bundle
     * @param message      - SuperAdmin defined message to describe the step verifiction
     * @param node         - Extent test, to which the verification logs to be attached
     * @return
     * @throws Exception
     */
    public static boolean verifyErrorMessage(String expectedCode, String message, ExtentTest node) throws Exception {
        Markup m = null;
        String code = null;
        String expectedMessage = MessageReader.getMessage(expectedCode, null);
        String actualMessage = getErrorMessage();

        if (actualMessage.contains(expectedMessage)) {
            code = "Verified Successfully - " + message + ":\nExpected: " + expectedMessage + "\nActual:   " + actualMessage;
            m = MarkupHelper.createCodeBlock(code);
            node.pass(m);
            return true;
        } else {
            code = "Verification Failure - " + message + ":\nExpected: " + expectedMessage + "\nActual: " + actualMessage;
            m = MarkupHelper.createCodeBlock(code);
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.captureScreen()).build());
        }
        return false;
    }


    public static void raiseExceptionAndStop(Exception e, ExtentTest node) throws IOException {
        node.fail(e);
        node.fatal("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.captureScreen()).build());
        e.printStackTrace();
        System.err.print("Stopping the execution, as an Exception has been Raised, Refer Reports for More Details!");
        Assert.fail(e.getMessage());
    }

    public static void raiseExceptionAndContinue(Exception e, ExtentTest node) throws IOException {
        node.fail(e);
        node.addScreenCaptureFromPath(ScreenShot.captureScreen());
        e.printStackTrace();
        System.out.print("Refer Reports for More Details!");
    }


}
