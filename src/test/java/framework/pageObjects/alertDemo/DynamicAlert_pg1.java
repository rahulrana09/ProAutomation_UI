package framework.pageObjects.alertDemo;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.utility.common.DriverFactory;
import framework.utility.reportManager.ScreenShot;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahulrana on 16/12/17.
 */
public class DynamicAlert_pg1 {
    private static ExtentTest pageInfo;
    /*
   Common Page objects like navigate, logout, etc
    */
    @FindBy(id = "random-alert")
    private WebElement btnAlert;

    public DynamicAlert_pg1 clickOnBtnGetAlert() {
        btnAlert.click();
        pageInfo.info("Click on button Random Alert!");
        return this;
    }

    public void clickOnOkay() throws IOException {
        /*WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 12);
        wait.until(ExpectedConditions.alertIsPresent());*/

        /*FluentWait wait =  new FluentWait(DriverFactory.getDriver());
        wait.withTimeout(5000, TimeUnit.MILLISECONDS);
        wait.pollingEvery(250, TimeUnit.MILLISECONDS);
        wait.ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.alertIsPresent());*/

        Alert alert = DriverFactory.getDriver().switchTo().alert();
        pageInfo.info("Alert Message: " + alert.getText());
        alert.accept();

    }

    public static DynamicAlert_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        return PageFactory.initElements(DriverFactory.getDriver(), DynamicAlert_pg1.class);
    }
}
