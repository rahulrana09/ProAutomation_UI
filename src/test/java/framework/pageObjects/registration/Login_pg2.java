package framework.pageObjects.registration;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.utility.globalConst.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.sun.jna.platform.win32.COM.tlb.TlbImp.logInfo;

/**
 * Created by rahulrana on 08/11/17.
 */
public class Login_pg2 extends PageInit {

    public Login_pg2(ExtentTest t1) {
        super(t1);
    }

    public static Login_pg2 init(ExtentTest t1){
        return new Login_pg2(t1);
    }

    /*
     * Page Objects
	 */
    @FindBy(name = "full_name")
    private WebElement txtFullName;

    public Login_pg2 setFullName(String text){
        txtFullName.sendKeys(text);
        logInfo("Set full name as - " + text);
        return this;
    }

    @FindBy(name = "address")
    private WebElement txtAddress;

    public Login_pg2 setAddress(String text){
        txtAddress.sendKeys(text);
        logInfo("Set Address - " + text);
        return this;
    }

    @FindBy(name = "city")
    private WebElement txtCity;

    public Login_pg2 setCity(String text){
        txtCity.sendKeys(text);
        logInfo("Set City - " + text);
        return this;
    }

    @FindBy(name = "rg-male")
    private WebElement radioMale;

    @FindBy(name = "rg-female")
    private WebElement radioFemale;

    public Login_pg2 setGender(String gender){

        if(gender.equals(Constants.USR_M)){
            radioMale.click();
            logInfo("Clicked on radio Male");
        }else{
            radioFemale.click();
            logInfo("Clicked on radio feMale");
        }

        return this;
    }

    @FindBy(name = "email")
    private WebElement txtEmail;

    public Login_pg2 setEmail(String text){
        txtEmail.sendKeys(text);
        logInfo("set Email as - " + text);
        return this;
    }

    @FindBy(name = "password")
    private WebElement txtPassword;

    public Login_pg2 setPassword(String text){
        txtPassword.sendKeys(text);
        logInfo("set Password as - " + text);
        return this;
    }

    @FindBy(name = "password_again")
    private WebElement txtPasswordAgain;

    public Login_pg2 setPasswordAgain(String text){
        txtPasswordAgain.sendKeys(text);
        logInfo("set re password as - " + text);
        return this;
    }

    @FindBy(name = "ch-agree")
    private WebElement chkIgree;

    public Login_pg2 checkAgree(){
        chkIgree.click();
        logInfo("Clicked on Checkbox Agree");
        return this;
    }

    @FindBy(className = "btn-primary")
    private WebElement btnSubit;

    public Login_pg2 clickOnButtonSubit(){
        btnSubit.click();
        logInfo("Clicked on button Submit");
        return this;
    }




}
