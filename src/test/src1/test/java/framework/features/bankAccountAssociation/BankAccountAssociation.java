package framework.features.bankAccountAssociation;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg1;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg2;
import framework.pageObjects.bankAccountDisaccociation.BankAccountDisassociate_page1;
import framework.pageObjects.bankAccountDisaccociation.BankAccountDisassociate_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static framework.util.common.DriverFactory.driver;

public class BankAccountAssociation {

    private static ExtentTest pNode;

    public static BankAccountAssociation init(ExtentTest t1) {
        pNode = t1;
        return new BankAccountAssociation();
    }

    public ArrayList<String> initiateBankAccountRegistration(User usr, String providerName, String bankName, String custId, String accNo, boolean getAccountDetails) throws Exception {
        ArrayList<String> accDetails = new ArrayList<String>();
        try {
            Markup m = MarkupHelper.createLabel("initiateBankAccountRegistration", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(pNode);
            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(pNode);

            page1.navigateToLink();
            page1.selectProvider(providerName);
            page1.selectUserType(usr.CategoryCode);
            page1.setMSISDNPrefN(usr.MSISDN);

            page1.clickSubmitBtnPrefN();
            Thread.sleep(2000);
            page2.clickAddMoreBtn();

            if (getAccountDetails) {
                Thread.sleep(1000);
                List<WebElement> rows = driver.findElements(By.xpath("(//form[@id='bankAdd_addMoreBankRows']/table/tbody/tr)[position()>1 and position()<last()]"));
                if (rows.size() >= 1) {
                    int size = rows.size() - 1;
                    WebElement bank = driver.findElement(By.name("bankCounterList[" + size + "].paymentTypeSelected"));
                    Select stat1 = new Select(bank);
                    stat1.selectByVisibleText(bankName);

                    accDetails.add(driver.findElement(By.name("bankCounterList[" + size + "].customerId")).getAttribute("value"));

                    accDetails.add(driver.findElement(By.name("bankCounterList[" + size + "].accountNumber")).getAttribute("value"));
                } else {
                    page2.selectLinkedBank(bankName);
                    accDetails.add(page2.getCustomerId());
                    accDetails.add(page2.getAccountNo());
                }

            } else {

                Thread.sleep(1000);
                List<WebElement> rows = driver.findElements(By.xpath("(//form[@id='bankAdd_addMoreBankRows']/table/tbody/tr)[position()>1 and position()<last()]"));
                if (rows.size() >= 1) {
                    int size = rows.size() - 1;
                    WebElement bank = driver.findElement(By.name("bankCounterList[" + size + "].paymentTypeSelected"));
                    Select stat1 = new Select(bank);
                    stat1.selectByVisibleText(bankName);
                    driver.findElement(By.name("bankCounterList[" + size + "].customerId")).sendKeys(custId);
                    driver.findElement(By.name("bankCounterList[" + size + "].accountNumber")).sendKeys(accNo);
                } else {
                    page2.selectLinkedBank(bankName);
                    page2.setCustomerId(custId);
                    page2.setAccountNo(accNo);
                    Thread.sleep(1000);
                }
                if (ConfigInput.isConfirm) {
                    page2.clickSubmitBtn();
                    if (ConfigInput.isAssert) {
                        Assertion.verifyActionMessageContain("bank.account.registration.initiation", "Bank Association Initiation", pNode, accNo);
                    }
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return accDetails;
    }

    /**
     * @param providerName
     * @param userType
     * @param operatorWallet
     * @param bankName
     * @throws Exception
     */
    public void associateBankAccountsForSystemWallet(String providerName, String userType, String operatorWallet, String bankName) throws Exception {
        Markup m = MarkupHelper.createLabel("Associate Bank Accounts For Operator System Wallet", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String customerId = DataFactory.getRandomNumberAsString(5);
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        String bankAccNum = DataFactory.getRandomNumberAsString(5);
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("BANK_ACC_REG");
            Login.init(pNode).login(optUsr);

            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(pNode);

            BankAccountRegistration_Pg1.init(pNode)
                    .navigateToLink().selectProvider(providerName)
                    .selectUser(userType).setWalletId(operatorWallet)
                    .clickSubmitBtnPrefN();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            page2.clickAddMoreBtn();
            List<WebElement> rows = driver.findElements(By.xpath("(//form[@id='bankAdd_addMoreBankRows']/table/tbody/tr)[position()>1 and position()<last()]"));
            if (rows.size() >= 1) {
                int size = rows.size() - 1;

                new Select(driver.findElement(By.name("bankCounterList[" + size + "].paymentTypeSelected")))
                        .selectByVisibleText(bankName);
                driver.findElement(By.name("bankCounterList[" + size + "].customerId")).sendKeys(customerId);
                driver.findElement(By.name("bankCounterList[" + size + "].accountNumber")).sendKeys(bankAccNum);
            }

            if (ConfigInput.isConfirm) {
                page2.clickSubmitBtn();
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("bank.account.registration.success",
                            "Bank Account Registration Successful", pNode, bankAccNum);

                    Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

                    String expectedUserId = MobiquityGUIQueries.getUserIdFromMtxWallet(operatorWallet);
                    String actualUserId = MobiquityGUIQueries.getUserIdFromCustAccounts(customerId);

                    Assertion.verifyEqual(expectedUserId, actualUserId,
                            "validating User Id From MTX_WALLET & MBK_CUST_ACCOUNTS Table", pNode);

                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    String status = MobiquityGUIQueries.getBankAccountStatus(customerId);
                    Assertion.verifyEqual(status, "Y",
                            "Verify That Bank Should Be Active", pNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param providerName
     * @param user
     * @param walletType
     * @param bankAccNum
     * @throws Exception
     */
    public void disassociateBankAccountsForSystemWallet(String providerName, String user, String walletType, String bankAccNum) throws Exception {
        Markup m = MarkupHelper.createLabel("Disassociate Bank Accounts For Operator System Wallet", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("BANK_ACC_DREG");
            Login.init(pNode).login(optUsr);

            BankAccountDisassociate_page1.init(pNode)
                    .navigateToLink().selectProvider(providerName).selectUser(user)
                    .setWalletType(walletType).clickSubmit();

            BankAccountDisassociate_page2.init(pNode)
                    .deleteBank(bankAccNum);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("bank.account.deregistration.success",
                        "Bank Account Disassociation Successful", pNode, bankAccNum);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void associateBankAccountsForRechargeOperator(String providerName, String userType, String operatorName, String bankName) throws Exception {
        Markup m = MarkupHelper.createLabel("Associate Bank Accounts For Operator System Wallet", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String customerId = DataFactory.getRandomNumberAsString(5);
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        String bankAccNum = DataFactory.getRandomNumberAsString(5);
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("BANK_ACC_REG");
            Login.init(pNode).login(optUsr);

            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(pNode);

            BankAccountRegistration_Pg1.init(pNode)
                    .navigateToLink().selectProvider(providerName)
                    .selectUser(userType).selectRechargeOperator(operatorName)
                    .clickSubmitBtnPrefN();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            page2.clickAddMoreBtn();
            List<WebElement> rows = driver.findElements(By.xpath("(//form[@id='bankAdd_addMoreBankRows']/table/tbody/tr)[position()>1 and position()<last()]"));
            if (rows.size() >= 1) {
                int size = rows.size() - 1;

                new Select(driver.findElement(By.name("bankCounterList[" + size + "].paymentTypeSelected")))
                        .selectByVisibleText(bankName);
                driver.findElement(By.name("bankCounterList[" + size + "].customerId")).sendKeys(customerId);
                driver.findElement(By.name("bankCounterList[" + size + "].accountNumber")).sendKeys(bankAccNum);
            }

            if (ConfigInput.isConfirm) {
                page2.clickSubmitBtn();
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("bank.account.registration.success",
                            "Bank Account Registration Successful", pNode, bankAccNum);

                    Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

                    String expectedUserId = MobiquityGUIQueries.getUserIdFromUserName(operatorName);
                    String actualUserId = MobiquityGUIQueries.getUserIdFromCustAccounts(customerId);

                    Assertion.verifyEqual(expectedUserId, actualUserId,
                            "validating User Id From MTX_WALLET & MBK_CUST_ACCOUNTS Table", pNode);

                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    String status = MobiquityGUIQueries.getBankAccountStatus(customerId);
                    Assertion.verifyEqual(status, "Y",
                            "Verify That Bank Should Be Active", pNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void disassociateBankAccountsForRechargeOperator(String providerName, String user, String operatorName, String bankAccNum) throws Exception {
        Markup m = MarkupHelper.createLabel("Disassociate Bank Accounts For Operator System Wallet", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("BANK_ACC_DREG");
            Login.init(pNode).login(optUsr);

            BankAccountDisassociate_page1.init(pNode)
                    .navigateToLink().selectProvider(providerName)
                    .selectUser(user).selectOperatorName(operatorName)
                    .clickSubmit();

            BankAccountDisassociate_page2.init(pNode)
                    .deleteBank(bankAccNum);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("bank.account.deregistration.success",
                        "Bank Account Disassociation Successful", pNode, bankAccNum);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public HashMap<String, String> modifySubscriberPageBankCheck(User subs, User modifyUser, String accNo) throws Exception {
        HashMap<String, String> values = new HashMap<String, String>();

        try {
            Markup m = MarkupHelper.createLabel("modifySubscriberPageBankCheck", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifySubscriber_page1 p1 = ModifySubscriber_page1.init(pNode);
            ModifySubscriber_page2 p2 = ModifySubscriber_page2.init(pNode);
            ModifySubscriber_page3 p3 = ModifySubscriber_page3.init(pNode);
            ModifySubscriber_page4 p4 = ModifySubscriber_page4.init(pNode);

            Login.init(pNode).login(modifyUser);

            p1.navSubscriberModification();
            p1.setMSISDN(subs.MSISDN);
            p1.clickOnSubmitPg1();
            p1.selectPrefix();
            p1.selectIdType();
            p1.uploadAddressProof(FilePath.uploadFile);
            p1.uploadIdentityProof(FilePath.uploadFile);
            p1.uploadPhotoProof(FilePath.uploadFile);
            p1.selectAddressProof();
            p1.selectIdentityProof();
            p1.selectPhotoProof();
            p2.clickOnNextPg2();
            p3.selectWebGroupRole(subs.WebGroupRole);
            p3.nextPage();
            // modifySubscriberPage4.confirmButtonNext_click();
            if (ConfigInput.isCoreRelease) {
                if (ConfigInput.isCoreRelease) {
                    p4.clickSubmitPg4();
                } else {
                    p4.clickOnConfirmPg4();
                }
                Thread.sleep(4000);
            }
            Utils.captureScreen(pNode);
            List<WebElement> acc = driver.findElements(By.xpath("//form[@id='subsRegistrationUpdateServiceBean_view']/table/tbody/tr/td[7]/input"));
            for (int i = 0; i < acc.size(); i++) {
                if (acc.get(i).getAttribute("value").equalsIgnoreCase(accNo)) {
                    values.put("LinkedBank", p4.getLinkedBank(i));
                    values.put("CustomerId", p4.getCustomerId(i));
                    values.put("AccountNumber", p4.getAccNo(i));
                    values.put("PrimaryAccount", p4.getPrimaryAccount(i));
                } else {
                    pNode.info("Bank is not added");
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return values;
    }

    public HashMap<String, String> viewChannelUserBankCheck(User user, String accNo) throws Exception {
        HashMap<String, String> values = new HashMap<String, String>();

        try {
            Markup m = MarkupHelper.createLabel("viewChannelUserBankCheck: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navViewChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforViewChannelUser(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.viewUserDetail();
            Thread.sleep(2000);
            values.put("LinkedBank", driver.findElement(By.xpath("//form[@id='viewForm']/table/tbody/tr//td[contains(text(),'" + accNo + "')]/..//td[2]")).getText().trim());
            values.put("CustomerId", driver.findElement(By.xpath("//form[@id='viewForm']/table/tbody/tr//td[contains(text(),'" + accNo + "')]/..//td[7]")).getText().trim());
            values.put("AccountNumber", driver.findElement(By.xpath("//form[@id='viewForm']/table/tbody/tr//td[contains(text(),'" + accNo + "')]")).getText().trim());

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return values;
    }

    public BankAccountAssociation modifyBankStatusForChannelUser(User user, String status, String accNo) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyBankForChannelUser", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
                page.clickNextEnterprisePreference();
                page.clickNextWalletCombination();
            }
            page.clickNextWalletMap();
            if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
                page.clickNextEnterprisePreferenceApproval();
            }

            try {
                WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
                WebElement elem = wait.until(ExpectedConditions
                        .elementToBeClickable(By.xpath("//tr/td/input[@value='" + accNo + "']/ancestor::tr[1]/td/select[contains(@name, 'statusSelected')]")));
                Select sel = new Select(elem);
                sel.selectByVisibleText(status);
                Utils.captureScreen(pNode);
                page.clickNextBankMapModification();
                page.finalSubmitForModification();
            } catch (Exception e) {
                Utils.captureScreen(pNode);
                e.printStackTrace();
                pNode.info("Bank is not added with account num: " + accNo);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @param status
     * @param bankName
     * @return customer id for the bank account in action
     * @throws Exception
     */
    public String modifyBankStatusForChannelUserBasedOnBankName(User user, String status, String bankName) throws Exception {
        String custId = null;
        try {
            Markup m = MarkupHelper.createLabel("modifyBankStatusForChannelUserBasedOnBankName", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            if (user.CategoryCode.equals("Enterprise")) {
                page.clickNextEnterprisePreference();
                page.clickNextWalletCombination();
            }
            page.clickNextWalletMap();
            if (user.CategoryCode.equals("Enterprise")) {
                page.clickNextEnterprisePreferenceApproval();
            }
            Thread.sleep(2000);
            Utils.captureScreen(pNode);
            List<WebElement> acc = driver.findElements(By.xpath("(//form[@id='editCH_addChannelUser']/table/tbody/tr/td[2])[position()>1]"));
            boolean isFound = false;
            for (int i = 0; i < acc.size(); i++) {
                Select sel = new Select(driver.findElement(By.id("walletTypeID" + i + "")));
                custId = driver.findElement(By.id("customerId" + i)).getAttribute("value");
                String bankNam = sel.getFirstSelectedOption().getText();
                if (bankName.equalsIgnoreCase(bankNam)) {
                    isFound = true;
                    Select sel1 = new Select(driver.findElement(By.name("bankCounterList[" + i + "].statusSelected")));
                    sel1.selectByVisibleText(status);
                    pNode.info("Status selected is :" + status);
                    break;
                }
            }
            if (!isFound)
                Assertion.markTestAsFailure("Could not find Bank entry for " + bankName, pNode);

            page.clickNextBankMapModification();
            if (ConfigInput.isConfirm) {
                page.finalSubmitForModification();
                if (ConfigInput.isAssert) {
                    //todo assert success message
                    Assertion.verifyActionMessageContain("channeluser.modify.approval",
                            "Verify that Channel user is successfully modify Initiated", pNode, user.FirstName, user.LastName);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return custId;
    }


    public BankAccountAssociation initiateBankAccountDeRegistration(User user, String provider, String accNo) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("initiateBankAccountDeRegistration", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            BankAccountDisassociate_page1 page1 = new BankAccountDisassociate_page1(pNode);
            BankAccountDisassociate_page2 page2 = new BankAccountDisassociate_page2(pNode);

            page1.navigateToLink();
            page1.selectProvider(provider);
            if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                page1.selectUser(Constants.USR_TYPE_SUBS);
            } else {
                page1.selectUser(Constants.USR_TYPE_CHANNEL);
            }
            page1.setMSISDN(user.MSISDN);
            page1.clickSubmit();
            page2.deleteBank(accNo);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("bank.account.deregistration.initiation", "Bank Account DeRegistration", pNode, String.valueOf(accNo));
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public BankAccountAssociation startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public BankAccountAssociation expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public BankAccountAssociation createLiquidationBanks() throws Exception {

        List<String> banks = MobiquityGUIQueries.getListOfBankForLiquidation();
        int flag = 0, flag1 = 0, flag2 = 0;
        String bankname = "AXIS", bankname1 = "AXIS2", bankname2 = "AXIS1";
        for (int i = 0; i < banks.size(); i++) {
            if (banks.get(i).equalsIgnoreCase(bankname)) {
                flag = 1;
            } else if (banks.get(i).equalsIgnoreCase(bankname2)) {
                flag1 = 1;
            } else if (banks.get(i).equalsIgnoreCase(bankname1)) {
                flag2 = 1;
            } else {
                continue;
            }
        }

        /*if (flag == 0) {
            Bank bank = new Bank(GlobalData.currencyProvider.get(0));
            bank.setBankName(bankname);
            CurrencyProviderMapping.init(pNode).addLiquidationBank(bank);
        }

        if (flag2 == 0) {
            Bank bank1 = new Bank(GlobalData.currencyProvider.get(0));
            bank1.setBankName(bankname1);
            CurrencyProviderMapping.init(pNode).addLiquidationBank(bank1);
        }

        if (flag1 == 0) {
            Bank bank2 = new Bank(GlobalData.currencyProvider.get(2));
            bank2.setBankName(bankname2);
            CurrencyProviderMapping.init(pNode).addLiquidationBank(bank2);
        }*/

        return this;
    }

}
