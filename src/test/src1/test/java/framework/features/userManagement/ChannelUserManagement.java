package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.CurrencyProvider;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.pageObjects.autoDebit.AutoDebitEnable_pg1;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by automation.team on 5/6/2017.
 */
public class ChannelUserManagement {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static WebDriver driver;
    private static FunctionLibrary fl;
    private static ExtentTest pNode;
    private static OperatorUser usrCreator, usrApprover, suspendApprover, bankApprover, delChUser, delChUserApprover, optBulk,
            baruser, bulkBankAssociator, bulkBankDisassociator;
    private static SuperAdmin saMaker, saChecker;
    private String FileHeaderBulkUserBankAssociation = "MSISDN*,Bank Id*,Provider Id*,User Type*";
    private boolean isOptionalFieldRequired = false;
    private FileWriter fileWriter = null;

    public static ChannelUserManagement init(ExtentTest t1) throws Exception {
        try {
            driver = DriverFactory.getDriver();
            fl = new FunctionLibrary(driver);
            pNode = t1;
            if (usrCreator == null) {
                optBulk = DataFactory.getOperatorUserWithAccess("BLK_CHUSR");
                usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.NETWORK_ADMIN);
                suspendApprover = DataFactory.getOperatorUserWithAccess("PTY_SCHAPP");
                usrApprover = DataFactory.getOperatorUsersWithAccess("PTY_CHAPP2").get(0);
                bankApprover = DataFactory.getOperatorUsersWithAccess("BNK_APR").get(0);
                saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
                delChUser = DataFactory.getOperatorUsersWithAccess("PTY_DCU", Constants.CHANNEL_ADMIN).get(0);
                delChUserApprover = DataFactory.getOperatorUsersWithAccess("PTY_DCHAPP", Constants.CHANNEL_ADMIN).get(0);
                baruser = DataFactory.getOperatorUsersWithAccess("PTY_BLKL").get(0);
                bulkBankAssociator = DataFactory.getOperatorUserWithAccess("SUB_BULK_BANK_ASSOC");
                bulkBankDisassociator = DataFactory.getOperatorUserWithAccess("BLK_BNK_DISOC");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new ChannelUserManagement();
    }

    /**
     * Used for Base Setup Initialization
     *
     * @param t1
     * @return
     * @throws Exception
     */
    public static ChannelUserManagement baseSetInit(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);

        if (saMaker == null) {
            saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
        }
        pNode = t1;
        return new ChannelUserManagement();
    }

    /**
     * Create Base Setup Channel Users
     *
     * @param user
     * @throws Exception
     */
    public void createBaseSetChannelUser(User user) throws Exception {
        String[] payIdArr = DataFactory.getPayIdApplicableForChannelUser();
        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(user, payIdArr);

        ChannelUserManagement.init(pNode)
                .createChannelUser(user);
    }

    /**
     * Create Channel User with Default Mapping Wallet and Bank Mapping
     *
     * @param user
     * @param isBankRequired - true if bank is to be associated with a user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement createChannelUserDefaultMapping(User user, boolean isBankRequired) throws Exception {
        String[] payIdArr = DataFactory.getPayIdApplicableForChannelUser();
        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(user, payIdArr);

        Login.init(pNode).login(usrCreator);
        initiateChannelUser(user);
        assignHierarchy(user);

        CommonUserManagement.init(pNode)
                .assignWebGroupRole(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference();
            setEnterpriseCategoryWalletCombination();
        }

        CommonUserManagement.init(pNode)
                .mapDefaultWalletPreferences(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference(user, isBankRequired);
        }

        if (!user.CategoryCode.equals(Constants.ENTERPRISE)) {
            if (isBankRequired) {
                CommonUserManagement.init(pNode)
                        .mapBankPreferences(user);
            } else {
                try {
                    WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
                    AddChannelUser_pg5 page = AddChannelUser_pg5.init(pNode);
                    page.clickNext(user.CategoryCode);
                    wait.until(ExpectedConditions.alertIsPresent()).accept();
                    wait.until(ExpectedConditions.alertIsPresent()).accept();
                    page.completeUserCreation(user, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Assertion.raiseExceptionAndContinue(e, pNode);
                }
            }
        }

        Login.init(pNode).login(usrApprover);
        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

        if (isBankRequired) {
            Login.init(pNode).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(user);
        }

        CommonUserManagement.init(pNode)
                .changeFirstTimePassword(user);

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeChannelUserMPinTPin(user);

        return this;
    }

    public ChannelUserManagement initiateAndApproveChUser(User user, boolean isBankRequired) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateAndApproveChUser " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String[] payIdArr = DataFactory.getPayIdApplicableForSubs();

        CurrencyProviderMapping.init(pNode)
                .mapWalletPreferencesUserRegistration(user, payIdArr);

        Login.init(pNode).login(usrCreator);
        initiateChannelUser(user);
        assignHierarchy(user);

        CommonUserManagement.init(pNode)
                .assignWebGroupRole(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference();
            setEnterpriseCategoryWalletCombination();
        }

        CommonUserManagement.init(pNode)
                .mapDefaultWalletPreferences(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference(user, isBankRequired);
        }

        if (!user.CategoryCode.equals(Constants.ENTERPRISE)) {
            if (isBankRequired) {
                CommonUserManagement.init(pNode)
                        .mapBankPreferences(user);
            } else {
                try {
                    WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
                    AddChannelUser_pg5 page = AddChannelUser_pg5.init(pNode);
                    page.clickNext(user.CategoryCode);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    driver.switchTo().alert().accept();
                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    driver.switchTo().alert().accept();
//                    wait.until(ExpectedConditions.alertIsPresent()).accept();
//                    wait.until(ExpectedConditions.alertIsPresent()).accept();
                    page.completeUserCreation(user, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Assertion.raiseExceptionAndContinue(e, pNode);
                }
            }
        }

        Login.init(pNode).login(usrApprover);
        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

        if (isBankRequired) {
            Login.init(pNode).login(bankApprover);

            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(user);
        }
        return this;
    }

    public ChannelUserManagement createChannelUserDefaultMappingWithoutChangingTpin(User user, boolean... mpinChange) throws Exception {
        boolean mpinChangeRequired = mpinChange.length > 0 ? mpinChange[0] : true;
        Login.init(pNode).login(usrCreator);
        initiateChannelUser(user);
        assignHierarchy(user);

        CommonUserManagement.init(pNode)
                .assignWebGroupRole(user);

        CommonUserManagement.init(pNode)
                .mapDefaultWalletPreferences(user);

        CommonUserManagement.init(pNode)
                .mapBankPreferences(user);

        if (!usrCreator.LoginId.equals(usrApprover.LoginId)) {
            Login.init(pNode).login(usrApprover);
        }
        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);


        CommonUserManagement.init(pNode).changeFirstTimePassword(user);

        // Change Mpin
        if (mpinChangeRequired) {
            Transactions.init(pNode)
                    .changeChannelUserMPin(user);
        }

        return this;
    }

    /**
     * Create a Channel User, note that bank
     *
     * @param user
     * @param bankAssociated - Bank is to be approved
     * @return
     * @throws Exception
     */
    public ChannelUserManagement createChannelUser(User user, boolean... bankAssociated) throws Exception {
        boolean bankAssociationRequired = bankAssociated.length > 0 ? bankAssociated[0] : true;

        Login.init(pNode).login(usrCreator);
        addChannelUser(user);

        Login.init(pNode).login(usrApprover);
        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

        if (bankAssociationRequired) {
            Login.init(pNode).login(bankApprover);
            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(user);
        }

        CommonUserManagement.init(pNode)
                .changeFirstTimePassword(user);

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeChannelUserMPinTPin(user);

        return this;
    }

    public ChannelUserManagement createChannelUserforRandomPassword(User user, boolean... bankAssociated) throws Throwable {
        Login.init(pNode).login(usrCreator);
        addChannelUserForRandomPassword(user);

        if (!usrCreator.LoginId.equals(usrApprover.LoginId)) {
            Login.init(pNode).login(usrApprover);
        }

        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

        if (!user.CategoryCode.equals(Constants.ENTERPRISE)) {
            Login.init(pNode).login(bankApprover);
            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(user);

        }
        CommonUserManagement.init(pNode)
                .changeFirstTimePasswordwithRandomPassword(user);

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeChannelUserMPinTPin(user);
        return this;
    }

    /**
     * Channel User Creation
     *
     * @param user
     * @throws Exception
     */
    public ChannelUserManagement addChannelUser(User user) throws Exception {
        initiateChannelUser(user);
        assignHierarchy(user);
        CommonUserManagement.init(pNode).assignWebGroupRole(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference();
            setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(pNode)
                    .mapDefaultWalletPreferences(user);

            setEnterprisePreference(user, true); //Change from 5.0, even Enterprise user can have Bank associated

        } else {
            CommonUserManagement.init(pNode)
                    .mapWalletPreferences(user);

            // map bank Preferences for.
            CommonUserManagement.init(pNode)
                    .mapBankPreferences(user);
        }

        return this;
    }


    public ChannelUserManagement addChannelUserForRandomPassword(User user) throws Exception {
        initiateChannelUserforRandomPassword(user);
        assignHierarchy(user);
        CommonUserManagement.init(pNode).assignWebGroupRole(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference();
            setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(pNode)
                    .mapDefaultWalletPreferences(user);

            setEnterprisePreference(user, false); // map preferences 2nd page
        } else {
            CommonUserManagement.init(pNode)
                    .mapWalletPreferences(user);

            CommonUserManagement.init(pNode)
                    .mapBankPreferences(user);
        }

        return this;
    }

    /**
     * Initiate Channel User Creation - Page one
     *
     * @param user
     * @throws Exception
     */
    public ChannelUserManagement initiateChannelUser(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(pNode);

            // * Navigate to Add Channel User Page *
            pageOne.navAddChannelUser();

            /*
             * Provide the general Information
             */
            pageOne.selectPrefix();
            pageOne.setFirstName(user.FirstName);
            pageOne.setLastName(user.LastName);
            pageOne.setExternalCode(user.ExternalCode);
            pageOne.setEmail(user.Email);
            pageOne.selectGender();
            pageOne.setDateOfBirth(new DateAndTime().getDate(-7300)); // 20 years less than current Date
            pageOne.selectIdType(1);

            pageOne.setRegType(user.RegistrationType);
            pageOne.setMerchantType(user.MerchantType); //TODO Check this

            /*if (!ConfigInput.isCoreRelease) {
                pageOne.setIdNumber(user.MSISDN); // functionality removed
            }*/
            pageOne.setWebLoginId(user.LoginId);
            Thread.sleep(Constants.WAIT_TIME);
            if (!AppConfig.isRandomPasswordAllowed) {
                pageOne.setPassword(ConfigInput.userCreationPassword);
                pageOne.setConfirmPassword(ConfigInput.userCreationPassword);
            }

            pageOne.setMSISDN(user.MSISDN);
            pageOne.selectLanguage();
            pageOne.allowAllDays();

            if (user.isSupportOnlineTxnReversal())
                pageOne.checkSupportOnlineTransactionReversal(true);
            else
                pageOne.checkSupportOnlineTransactionReversal(false);

            // fields specific To 5.0 Core release
            if (ConfigInput.isCoreRelease) {
                pageOne.setRelationshipOfficer("ROFC");
            }

            /**
             * Checking the preference for IS_IMT_SEND_MONEY_ENABLED
             *  IF true then input fields are mandatory
             */
            if (AppConfig.isImtSendMoneyEnabled) {
                pageOne.setIssueCountry();
                pageOne.setNationality();
                pageOne.setCurrentResidence();
                pageOne.setPostalCode();
                pageOne.setDateOfIssuer(new DateAndTime().getDate(0));
                pageOne.setDateOfExpiry(new DateAndTime().getDate(65));

                pageOne.setDateOfIssuer(new DateAndTime().getDate(0));
                pageOne.setEmployeeName();
            }

            pageOne.selectIDType();
            pageOne.uploadID(user.IDProof);

            pageOne.selectAddProofType();
            pageOne.uploadAddProof(user.AddressProof);

            pageOne.selectPhotoProofType();
            pageOne.uploadPhotoProof(user.PhotoProof);

            if (isOptionalFieldRequired) {
                pageOne.setContactPerson(user.ContactPerson);
            }

            pageOne.clickNext();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            if (ConfigInput.isAssert) {
                Assertion.assertEqual(AddChannelUser_pg2.init(pNode).isHierarchyPageOpen(), true,
                        "Complete Initiation Page and Navigated to Add Hierarchy Page", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Channel User Creation - Page one
     *
     * @param user
     * @throws Exception
     */
    public ChannelUserManagement initiateChannelUserforRandomPassword(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(pNode);

            // * Navigate to Add Channel User Page *
            pageOne.navAddChannelUser();

            /*
             * Provide the general Information
             */
            pageOne.selectPrefix();
            pageOne.setFirstName(user.FirstName);
            pageOne.setLastName(user.LastName);
            pageOne.setExternalCode(user.ExternalCode);
            pageOne.setEmail(user.Email);
            pageOne.selectGender();
            pageOne.setDateOfBirth(new DateAndTime().getDate(-3660)); // 10 years less than current Date
            pageOne.selectIdType(1);
            pageOne.setRegType(Constants.REGTYPE_NO_KYC);
            if (!ConfigInput.isCoreRelease) {
                pageOne.setIdNumber(user.MSISDN);
            }
            pageOne.setWebLoginId(user.LoginId);


            pageOne.setMSISDN(user.MSISDN);
            pageOne.selectLanguage();
            pageOne.allowAllDays();

            // fields specific To 5.0 Core release
            if (ConfigInput.isCoreRelease) {
                pageOne.setRelationshipOfficer("ROFC");
            }

            /**
             * Checking the preference for IS_IMT_SEND_MONEY_ENABLED
             *  IF true then input fields are mandatory
             */
            if (AppConfig.isImtSendMoneyEnabled) {
                pageOne.setDateOfIssuer(DataFactory.getCurrentDateSlash());
                pageOne.setIssueCountry();
                pageOne.setNationality();
                pageOne.setCurrentResidence();
                pageOne.setPostalCode();
                pageOne.setDateOfExpiry(new DateAndTime().getDate(+180));
                pageOne.setEmployeeName();
            }

            pageOne.selectIDType();
            pageOne.uploadID(user.IDProof);

            pageOne.selectAddProofType();
            pageOne.uploadAddProof(user.AddressProof);

            pageOne.selectPhotoProofType();
            pageOne.uploadPhotoProof(user.PhotoProof);

            pageOne.clickNext();
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * assign User Hierarchy
     *
     * @param user
     * @throws Exception
     */
    public CommonUserManagement assignHierarchy(User user) throws Exception {
        User parent, owner;
        try {
            Markup m = MarkupHelper.createLabel("assignHierarchy: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(pNode);
            hierarchy.selectDomain(user.DomainName);
            hierarchy.selectCategory(user.CategoryName);

            if (user.ParentUser == null && user.ParentCategoryCode != null) {
                parent = DataFactory.getChannelUserWithCategory(user.ParentCategoryCode);
                user.setParentUser(parent);
            }

            if (user.OwnerCategoryCode != null) {
                owner = DataFactory.getChannelUserWithCategory(user.OwnerCategoryCode);
                user.setOwnerUser(owner);
            }
            if (user.ParentUser == null && user.ParentCategoryCode != null) {
                parent = DataFactory.getChannelUserWithCategory(user.ParentCategoryCode);
                user.setParentUser(parent);
            }

            // Select Owner
            Thread.sleep(Constants.WAIT_TIME);
            if (hierarchy.OwnerName.isEnabled()) {
                hierarchy.selectOwnerName(user.OwnerUser.getPartialName());
            }

            // Select Parent
            Thread.sleep(Constants.WAIT_TIME);
            if (hierarchy.ParentCategory.isEnabled()) {
                hierarchy.selectParentCategory(user.ParentUser.CategoryName);
                hierarchy.setFirstName("%");
                hierarchy.selectParentName(user.ParentUser.getFullName());
            }

            hierarchy.selectZone(user.Geography.ZoneName);
            hierarchy.selectGeography(user.Geography.AreaName);
            if (user.OwnerUser != null) {
                hierarchy.selectChildOwnerName(user.OwnerUser.getPartialName());
                hierarchy.selectChildParentName(user.OwnerUser.getPartialName());
            }

            hierarchy.clickNext();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return CommonUserManagement.init(pNode);
    }


    /**
     * Modify a Channel user which has been Modified and waiting for Approval
     *
     * @param user
     * @param newName [optional] send the New Name in case the first name and last name are updated
     * @return
     * @throws Exception
     */
    public ChannelUserManagement modifyUserApproval(User user, String... newName) throws Exception {
        try {
            Utils.createLabelForMethod("modifyUserApproval: " + user.LoginId, pNode);

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModifiedApproval(user.CategoryCode);
            page.setMsisdn(user);
            page.submitModifiedApprovalDetail();
            page.confirmApproval();
            Utils.captureScreen(pNode);
            String expectedMessage = "";
            if (newName.length > 0)
                expectedMessage = MessageReader.getDynamicMessage("channeluser.message.updation", newName[0], newName[0]);
            else
                expectedMessage = MessageReader.getDynamicMessage("channeluser.message.updation", user.FirstName, user.LastName);

            Assertion.verifyEqual(page.getActionMessage(), expectedMessage, "Modify User Approval, " + user.LoginId, pNode);

            // db message
            String actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
            DBAssertion.verifyDBMessageContains(actualMessage, "000032", "Verify Channel User Modification Message in DB", pNode, user.LoginId);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * View a Channel user
     *
     * @param user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement viewChannelUser(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("viewChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navViewChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforViewChannelUser(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.viewUserDetail();
            if (ConfigInput.isAssert) {
                //Assertion is in below method
                page.viewChannelUserPageValidation(user);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Channel User Delete
     *
     * @param user
     * @return
     */
    public ChannelUserManagement initiateChannelUserDelete(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChannelUserDelete: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(delChUser);
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navDeleteChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforDeletion(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitDeleteDetail();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            if (ConfigInput.isConfirm)
                page.confirmDelete();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("channeluser.delete.approval", "Channel User Delete Initiated",
                        pNode, user.FirstName, user.LastName)) {
                    user.setStatus(Constants.AUT_STATUS_DELETE_INITIATE);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * Initiate Channel User Delete
     *
     * @param user
     * @return
     * @Optional approve - Pass false if the Approval need to be rejected, else don't pass nothing
     */
    public ChannelUserManagement approveRejectDeleteChannelUser(User user, boolean... approve) throws Exception {
        boolean isApprove = approve.length > 0 ? approve[0] : true;
        try {
            Markup m = MarkupHelper.createLabel("approveRejectDeleteChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navDeleteChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryForDeletionApproval(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitDeleteApprovalDetail();

            if (ConfigInput.isAssert) {
                if (isApprove) {
                    page.confirmApproval();
                    if (Assertion.verifyActionMessageContain("channeluser.deleted", "Channel User Delete Approval Confirm",
                            pNode, user.FirstName, user.LastName)) {
                        user.setStatus(Constants.AUT_STATUS_DELETED);
                        String actualMessage = MobiquityGUIQueries.getMobiquityUserMessage(user.MSISDN, "DESC");
                        DBAssertion.verifyDBMessageContains(actualMessage, "1036", "Verify Channel User Deletion Message", pNode);
                    }
                } else {
                    page.rejectApproval();
                    Assertion.verifyActionMessageContain("channeluser.message.reject", "Channel User Delete Approval Reject, " + user.LoginId, pNode, user.FirstName, user.LastName);
                }
            }

            return this;
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Confirm Channel User Deletion
     *
     * @param
     * @return
     */
    public ChannelUserManagement confirmDeleteChannelUser() throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("confirmDeleteChannelUser: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.confirmDelete();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public ChannelUserManagement initiateChannelUserModification(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateChannelUserModification: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement modifyChannelUserWithNoChange(User user) throws Exception {

        Markup m = MarkupHelper.createLabel("modifyChannelUserWithNoChange: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(usrCreator);
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();
            page.clickNxtModify();

            /*ChannelUserManagement.init(pNode)
                    .assignHierarchy(user);*/

            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(pNode);
            hierarchy.clickNext();

            page.clickNextWebRole();

            page.clickNextWalletMap();

            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);
            page5.clickNext(user.CategoryCode); // navigate to the next page

            //clickOnElement(btnSubmitChUserEdit, "btnSubmitChUserEdit");
            page5.completeUserCreation(user, true);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("channeluser.modify.approval",
                        "Modify User initiated, " + user.LoginId, pNode, user.FirstName, user.LastName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @param reject
     * @return
     * @throws Exception
     */
    public ChannelUserManagement approveRejectModifyChannelUser(User user, boolean... reject) throws Exception {
        boolean isReject = reject.length > 0 ? reject[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("approveRejectModifyChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModifiedApproval(user.CategoryCode);
            page.setMsisdn(user);
            page.submitModifiedApprovalDetail();
            String expectedMessage = "";
            if (!isReject) {
                page.confirmApproval();
                expectedMessage = MessageReader.getDynamicMessage("channeluser.message.updation", user.FirstName, user.LastName);
                Assertion.verifyEqual(page.getActionMessage(), expectedMessage, "Modify User Approval, " + user.LoginId, pNode);

            } else {
                page.clickOnModifyReject();
                expectedMessage = MessageReader.getDynamicMessage("channeluser.message.rej", user.FirstName, user.LastName);
                Assertion.verifyEqual(page.getActionMessage(), expectedMessage, "channeluser modify reject, " + user.LoginId, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Method:                     Description:                                 Date:
     * setEnterprisePreference     Set Enterprises Preferences                  16/6/2017
     */
    public ChannelUserManagement setEnterprisePreference() throws IOException {
        Markup m = MarkupHelper.createLabel("setEnterprisePreference: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        AddEnterprise_pg1 page = AddEnterprise_pg1.init(pNode);
        try {
            page.selectallpref();
            page.clickNext();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Method:                     Description:                                 Date:
     * setEnterprisePreference     Set Enterprises Preferences                  16/6/2017
     */
    public void setEnterprisePreference(User user, boolean isBankRequired) throws Exception {
        Markup m = MarkupHelper.createLabel("setEnterprisePreference: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(pNode);
        AddChannelUser_pg5 page1 = AddChannelUser_pg5.init(pNode);
        try {
            Thread.sleep(5000);
            page.setEnterpriseLimit2(user.EnterpriseLimit);
            page.clickOnBulkRegistrationIsRequired();
            page.selectBulkPayerType();

            page.setCompCode(DataFactory.getRandomNumberAsString(5)); // company code should be
            page.clickNext();
            if (ConfigInput.isCoreRelease) {
                if (isBankRequired) {
                    CommonUserManagement.init(pNode)
                            .mapBankPreferences(user);
                } else {
                    try {
                        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
                        page1.clickNext(user.CategoryCode);
                        wait.until(ExpectedConditions.alertIsPresent()).accept();
                        wait.until(ExpectedConditions.alertIsPresent()).accept();

                    } catch (Exception e) {
                        e.printStackTrace();
                        Assertion.raiseExceptionAndContinue(e, pNode);
                    }
                    page1.completeUserCreation(user, false);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void setEnterprisePreferenceForLiquidationBank() throws Exception {
        Markup m = MarkupHelper.createLabel("setEnterprisePreferenceForLiquidationBank: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(pNode);

        try {
            page.setEnterpriseLimit2("100");
            page.clickOnBulkRegistrationIsRequired();
            page.selectBulkPayerType();

            page.setCompCode(DataFactory.getRandomNumberAsString(5)); // company code should be
            page.clickNext();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void setEnterprisePreferenceforUnregisteredPayee(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("setEnterprisePreferenceforUnregisteredPayee: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(pNode);
        try {
            page.setEnterpriseLimit2("100");
            page.No_bulkRegi();
            page.selectBulkPayerType();
            page.selectUnregiBPmode();

            page.setCompCode(DataFactory.getRandomNumberAsString(5)); // company code should be
            page.clickNext();

            if (ConfigInput.isConfirm) {
                page.confirmEnterprise();
            }
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    /**
     * Select wallet category combination
     */
    public ChannelUserManagement setEnterpriseCategoryWalletCombination() throws IOException {
        Markup m = MarkupHelper.createLabel("setEnterpriseCategoryWalletCombination: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        AddEnterprise_pg1 page = AddEnterprise_pg1.init(pNode);
        try {
            page.selectAllType();
            page.clickNextLoadenterprise();
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement setEnterpriseCategoryWalletCombinationforNormal() throws IOException {
        Markup m = MarkupHelper.createLabel("setEnterpriseCategoryWalletCombinationforNormal: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        AddEnterprise_pg1 page = AddEnterprise_pg1.init(pNode);
        try {
            page.selectTypeNormal();
            page.clickNextLoadenterprise();
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /******************************************************************************************************
     * B U L K   C H A N N E L   U S E R   M A N A G E M E N T
     ******************************************************************************************************/

    /**
     * Bulk User Registration and approval
     *
     * @param usrList
     * @throws Exception
     */
    public void bulkUserRegistrationAndApproval(List<User> usrList) throws Exception {
        String id = bulkChannelUserRegistration(usrList);
        approveBulkRegistration(id);

        // Approve Linked Bank
        Login.init(pNode).login(bankApprover);
        for (int i = 0; i < usrList.size(); i++) {
            CommonUserManagement.init(pNode)
                    .approveAllAssociatedBanks(usrList.get(i));
        }
    }

    /**
     * Add Users as Bulk Channel User Registration
     *
     * @param usrList
     * @return Batch ID
     */
    public String bulkChannelUserRegistration(List<User> usrList) throws Exception {
        String id = null;
        try {
            String csvFile = ExcelUtil.getBulkUserRegistrationFile();
            for (int i = 0; i < usrList.size(); i++) {
                writeBulkChUserWalletDetailsInCSV(usrList.get(i), csvFile, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletId);
//                writeBulkChUserBankDetailsInCSV(usrList.get(i), csvFile);
            }

            BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(pNode);
            page.navBulkUserRegistration();
            page.uploadFile(csvFile);
            page.submitCsv();

            String message = driver.findElements(By.className("actionMessage")).get(0).getText();

            Assertion.verifyMessageContain(message, "bulkUpload.channel.label.success", "Bulk Channel User Reg.", pNode);

            id = message.split("ID :")[1].trim();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return id;
    }

    /**
     * Bulk User Approval
     *
     * @param txId
     */
    public void approveBulkRegistration(String txId) throws Exception {
        Markup m = MarkupHelper.createLabel("approveBulkRegistration: " + txId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(pNode);
        page.navBulkUserRegistrationApproval();

        int rowCount = driver.findElements(By.xpath("//*[@id='bulkUserRegAction_loadBulkUserDataForApproval']/table/tbody/tr")).size();

        // iterate through all the cells
        for (int i = 2; i < rowCount; i++) {
            String xPathTXid = "//*[@id='bulkUserRegAction_loadBulkUserDataForApproval']/table/tbody/tr[" + i + "]/td[1]";
            if (driver.findElement(By.xpath(xPathTXid)).getText().equals(txId)) {
                driver.findElement(By.xpath("//*[@id='bulkUserRegAction_loadBulkUserDataForApproval']/table/tbody/tr[" + i + "]/td[5]/a")).click();

                Alert alert = driver.switchTo().alert();
                alert.accept();

                String message = driver.findElements(By.className("actionMessage")).get(0).getText();

                Assertion.verifyMessageContain(message, "bulkuser.message.approval", "Approve Bulk Channel User Reg.", pNode);
                return;
            }
        }
        pNode.fail("Id Not Found : " + txId);
    }


    /**
     * This function will fetch wallet related details and write it to CSV
     *
     * @return - null
     * @author - Dalia Debnath
     * @deprecated
     */
    public ChannelUserManagement writeBulkChUserWalletDetailsInCSV(User user, String file, String providerName, String walletId) throws Exception {
        try {
            String walletName = DataFactory.getWalletName(walletId);
            Markup m = MarkupHelper.createLabel("writeBulkChUserWalletDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
            String ownerMsisdn, parentMsisdn, l_userStatus, l_primary, l_accStatus;

            // Below Code is written assuming only 2 level Hierarchy
            if (user.ParentUser != null) {
                ownerMsisdn = user.ParentUser.MSISDN;
                parentMsisdn = user.ParentUser.MSISDN;
            } else {
                ownerMsisdn = user.MSISDN;
                parentMsisdn = user.MSISDN;
            }
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = "28/02/2014";
            String webRole = user.WebGroupRole;


            boolean setA = false; //To set user status
            boolean setPrimary = false; //To set primary account status


            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, walletName);
            MobileGroupRole mRole = DataFactory.getMobileGroupRole(user.DomainName, user.CategoryName, user.GradeName, providerName, walletName);

            String l_tcpID = dbHandler.getIDofTCPprofile(insTcp.ProfileName);

            if (!setA) {
                l_userStatus = "A";
                setA = true;
            } else {
                l_userStatus = "N";
            }

            if (!setPrimary) {
                l_primary = "Y";
                setPrimary = true;
            } else {
                l_primary = "N";
            }
            l_accStatus = "A";
            String[] allData = {
                    "Mr",    //	NamePrefix (Mr/Mrs/Miss/M/s)*
                    user.LoginId,    //	FirstName*
                    user.LoginId,    //	LastName*
                    user.ExternalCode,    //	Identification Number*
                    "Bangalore",    //	City
                    "Karnataka",    //	State
                    "IN",    //	Country
                    user.Email,    //	Email*
                    "Quality",    //	Designation
                    user.LoginId,    //	Contact Person
                    user.MSISDN,    //	Contact Number
                    "Male",    //	Gender (Male/Female)*
                    dateOfBirth,    //	Date of Birth*(dd/mm/yyyy)
                    "124567",    //	Registration Form Number
                    user.LoginId,    //	Web login Id*
                    l_userStatus,    //	User Status*(A/M/N)
                    user.MSISDN,    //	MSISDN*
                    "English",    //	Preferred Language*
                    user.CategoryCode,    //	User Category*
                    ownerMsisdn,    //	Owner MSISDN*
                    parentMsisdn,    //	Parent MSISDN*
                    user.Geography.AreaCode,    //	Geography Domain Code*
                    webRole,    //	Group Role Code*
                    mRole.ProfileName,    //	Mobile Group Role Id*
                    user.GradeCode,    //	Grade Code*
                    l_tcpID,    //	TCP Profile Id*
                    "Y",    //	Primary Account (Y/N)*
                    "",    //	Customer Id
                    "",    //	Account Number
                    "",    //	Account Type
                    l_accStatus,    //	Wallet Type/Linked Bank Status* (A/M/S/D)
                    "ONLINE",    //	Merchant Type(ONLINE/OFFLINE)
                    issueDate,    //	IdIssueDate
                    expireDate,    //	IdExpiryDate
                    "IN",    //	IdIssueCountry
                    "IN",    //	ResidenceCountry
                    "120021",    //	PostalCode
                    "IN",    //	Nationality
                    "Mahindra Comviva",    //	EmployerName
                    "TRUE",    //	IdExpiresAllowed
                    "",    //  _shortName
                    "",       //  _address1
                    "",       //  _address2
                    "usr@123.com",    //	Url(Semicolon Separated)
                    "123",    //	Block Notification(0/1/2/3/4/5/6/7)
                    "Gurgaon", // Address*
                    "PASSPORT", //        IDType*
                    "AWRPN4042Q", //        IDNumber*
                    "ChU", //        Nickname*
                    "RELOFF12345",    //	Relationship Officer*
                    "Y",    //	Autosweep Allowed (Y/N)
                    "PASSPORT",    //	ContactIDType
                    "1",    //	AllowedDays
                    "00:00",    //	AllowedFormTime
                    "23:59",    //	AllowedToTime
                    "COMF1",    //	CommercialField1
                    "COMF2",    //	CommercialField2
                    "COMF3",    //	CommercialField3
                    "COMF4",    //	CommercialField4
                    "COMF5",    //	CommercialField5
                    "COMF6",    //	CommercialField6
                    "COMF7",    //	CommercialField7
                    "COMF8",    //	CommercialField8
                    "COMF9",    //	CommercialField9
                    "COMF10",    //	CommercialField10
                    "COMF11",    //	CommercialField11
                    "ShopName"    //	ShopName

            };


            pNode.info("Writing User Wallet Related info to CSV");
            ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, true, false);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * This function will fetch Bank related details and write it to CSV
     *
     * @return - null
     * @author - Dalia Debnath
     */
    public ChannelUserManagement writeBulkChUserBankDetailsInCSV(User user, String file) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("writeBulkChUserBankDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
            String ownerMsisdn, parentMsisdn, l_userStatus, l_primary, l_accStatus;

            // Below Code is written assuming only 2 level Hierarchy
            if (user.ParentUser != null) {
                ownerMsisdn = user.ParentUser.MSISDN;
                parentMsisdn = user.ParentUser.MSISDN;
            } else {
                ownerMsisdn = user.MSISDN;
                parentMsisdn = user.MSISDN;
            }

            String geoZone = dbHandler.dbGetGeoDomainName();
            String geoArea = dbHandler.dbGetChannelGeo(geoZone);
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = DataFactory.getCurrentDateSlash();
            String webRole = user.WebGroupRole;

            List<String> providerList = DataFactory.getAllProviderNames();

            boolean setA = false; //To set user status
            for (int i = 0; i < providerList.size(); i++) {
                String providerName = providerList.get(i);

                List<String> paymentTypeList = DataFactory.getAllBankNamesLinkedToProvider(providerName);

                boolean setPrimary = false; //To set primary account status

                for (int j = 0; j < paymentTypeList.size(); j++) {
                    String bank = paymentTypeList.get(j);

                    InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, bank);
                    MobileGroupRole mRole = DataFactory.getMobileGroupRole(user.DomainName, user.CategoryName, user.GradeName, providerName, bank);

                    String l_tcpID = dbHandler.getIDofTCPprofile(insTcp.ProfileName);

                    l_userStatus = "N";

                    if (!setPrimary) {
                        l_primary = "Y";
                        setPrimary = true;
                    } else
                        l_primary = "N";

                    l_accStatus = "A";

                    String accNumber = "" + DataFactory.getRandomNumber(9); // TODO make length generic
                    Thread.sleep(1000);
                    String custId = "" + DataFactory.getRandomNumber(9); // TODO make length generic

                    String[] allData = {"Mr", user.LoginId, user.LoginId, user.ExternalCode, "Gurgaon",
                            "Haryana", "India", user.Email, "Quality",
                            user.LoginId, user.MSISDN, "Male", dateOfBirth,
                            user.MSISDN, user.LoginId, l_userStatus, user.MSISDN,
                            "English", user.CategoryCode, ownerMsisdn, parentMsisdn,
                            geoArea, webRole, mRole.ProfileName, user.GradeCode,
                            l_tcpID, l_primary, accNumber, custId,
                            "Saving", l_accStatus, "ONLINE", issueDate,
                            expireDate, "India", "India", "120021",
                            "India", "Mahindra Comviva", expireDate,
                            "Address1", user.LoginId, "Address2"};

                    pNode.info("Writing User Bank Related info to CSV");
                    ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param file
     * @param user
     * @param providerName
     * @param payInstName
     * @param userStatus          User Status*(A/M/N)
     * @param payInstrumentStatus Wallet Type/Linked Bank Status* (A/M/S/D)
     * @param isPayInstPrimary    true/false
     * @param isAppend            true/false
     * @param fields
     *      *                   [0] - Merchant Type(ONLINE/OFFLINE)
     *      *                   [1] - Supports Online Trans Reversal (Y / N)
     * @return
     * @throws Exception
     */
    public ChannelUserManagement writeBulkChUserModificationDetailsInCSV(String file,
                                                                         User user,
                                                                         String providerName,
                                                                         String payInstName,
                                                                         String userStatus,
                                                                         String payInstrumentStatus,
                                                                         boolean isPayInstPrimary,
                                                                         boolean isAppend,
                                                                         String... fields) throws Exception {
        Markup m = MarkupHelper.createLabel("writeBulkChUserModificationDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            String ownerMsisdn, parentMsisdn;
            String merchantType, supportOnlineTransactionReversal;

            String isPrimary = (isPayInstPrimary) ? "Y" : "N";

            // Below Code is written assuming only 2 level Hierarchy
            if (user.ParentUser != null) {
                ownerMsisdn = user.ParentUser.MSISDN;
                parentMsisdn = user.ParentUser.MSISDN;
            } else {
                ownerMsisdn = user.MSISDN;
                parentMsisdn = user.MSISDN;
            }

            try {
                merchantType = fields[0];
            } catch (IndexOutOfBoundsException e) {
                merchantType = "ONLINE";
            }

            try {
                supportOnlineTransactionReversal = fields[1];
            } catch (IndexOutOfBoundsException e) {
                supportOnlineTransactionReversal = "N";
            }

            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = "28/02/2014";

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, payInstName);
            MobileGroupRole mRole = DataFactory.getMobileGroupRole(user.DomainName, user.CategoryName, user.GradeName, providerName, payInstName);

            String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

            String custId, accountNum;

            if (user.custIdOptional != null) {
                custId = user.custIdOptional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                custId = user.MSISDN;
            } else {
                custId = DataFactory.getRandomNumberAsString(9);
            }

            if (user.accNumOtional != null) {
                accountNum = user.accNumOtional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                accountNum = user.MSISDN;
            } else {
                accountNum = DataFactory.getRandomNumberAsString(9);
            }
            String geoCode = GlobalData.autGeography.AreaCode;

            String[] allData = {
                    "Mr",    //	NamePrefix (Mr/Mrs/Miss/M/s)*
                    user.LoginId,    //	FirstName*
                    user.LoginId,    //	LastName*
                    user.ExternalCode,    //	Identification Number*
                    "Bangalore",    //	City
                    "Karnataka",    //	State
                    "IN",    //	Country
                    user.LoginId + "@comviva.com",    //	Email*
                    "Quality",    //	Designation
                    user.LoginId,    //	Contact Person
                    user.MSISDN,    //	Contact Number
                    "Male",    //	Gender (Male/Female)*
                    dateOfBirth,    //	Date of Birth*(dd/mm/yyyy)
                    DataFactory.getRandomNumberAsString(6),    //	Registration Form Number
                    user.LoginId,    //	Web login Id*
                    userStatus,    //	User Status*(A/M/N)
                    user.MSISDN,    //	MSISDN*
                    "English",    //	Preferred Language*
                    user.CategoryCode,    //	User Category*
                    ownerMsisdn,    //	Owner MSISDN*
                    parentMsisdn,    //	Parent MSISDN*
                    geoCode,    //	Geography Domain Code*
                    user.WebGroupRole,    //	Group Role Code*
                    mRole.ProfileName,    //	Mobile Group Role Id*
                    user.GradeCode,    //	Grade Code*
                    l_tcpID,    //	TCP Profile Id*
                    isPrimary,    //	Primary Account (Y/N)*
                    custId,    //	Customer Id
                    accountNum,    //	Account Number
                    "Saving",    //	Account Type
                    payInstrumentStatus,    //	Wallet Type/Linked Bank Status* (A/M/S/D)
                    "ONLINE",    //	Merchant Type(ONLINE/OFFLINE)
                    supportOnlineTransactionReversal,    //	Merchant Type(ONLINE/OFFLINE)
                    issueDate,    //	IdIssueDate
                    expireDate,    //	IdExpiryDate
                    "IN",    //	IdIssueCountry
                    "IN",    //	ResidenceCountry
                    "120021",    //	PostalCode
                    "IN",    //	Nationality
                    "Mahindra Comviva",    //	EmployerName
                    "TRUE",    //	IdExpiresAllowed
                    "usr@123.com",    //	Url(Semicolon Separated)
                    "123",    //	Block Notification(0/1/2/3/4/5/6/7)
                    "RELOFF12345",    //	Relationship Officer*
                    "",       //  IsMerchant(Y/N)
                    "",       //  MerchantCategoryID
                    "",        //  DescriptionForMerchant
                    "",        //  ShowOnLocatorForMerchant(Y/N)
                    "",        //  IsAgent(Y/N)
                    "",        //  AgentCategoryID
                    "",        //  DescriptionForAgent
                    "",        //  ShowOnLocatorForAgent(Y/N)
                    "",        //  Latitude
                    "",        //  Longitude
                    "Y",    //	Autosweep Allowed (Y/N)
                    "PASSPORT",    //	ContactIDType
                    "3",    //	AllowedDays
                    "00:00",    //	AllowedFormTime
                    "23:59",    //	AllowedToTime
                    "COMF1",    //	CommercialField1
                    "COMF2",    //	CommercialField2
                    "COMF3",    //	CommercialField3
                    "COMF4",    //	CommercialField4
                    "COMF5",    //	CommercialField5
                    "COMF6",    //	CommercialField6
                    "COMF7",    //	CommercialField7
                    "COMF8",    //	CommercialField8
                    "COMF9",    //	CommercialField9
                    "COMF10",    //	CommercialField10
                    "COMF11",    //	CommercialField11
                    "ShopName"    //	ShopName

            };


            pNode.info("Writing User User Related info to CSV");
            ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, false, isAppend);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * @param file
     * @param user
     * @param providerName
     * @param payInstName
     * @param isPayInstPrimary true/false
     * @param isAppend         true/false
     * @return
     * @throws Exception fields:
     *                   [0] - Merchant Type(ONLINE/OFFLINE)
     *                   [1] - Supports Online Trans Reversal (Y / N)
     */
    public ChannelUserManagement writeBulkChUserRegistrationDetailsInCSV(String file,
                                                                         User user,
                                                                         String providerName,
                                                                         String payInstName,
                                                                         boolean isPayInstPrimary,
                                                                         boolean isAppend,
                                                                         String... fields) throws Exception {
        Markup m = MarkupHelper.createLabel("writeBulkChUserModificationDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);

        pNode.info(m); // Method Start Marker
        try {


            String ownerMsisdn, parentMsisdn, supportOnlineTransactionReversal, merchantType;

            String isPrimary = (isPayInstPrimary) ? "Y" : "N";

            try {
                merchantType = fields[0];
            } catch (IndexOutOfBoundsException e) {
                merchantType = "ONLINE";
            }

            try {
                supportOnlineTransactionReversal = fields[1];
            } catch (IndexOutOfBoundsException e) {
                supportOnlineTransactionReversal = "N";
            }


            // Below Code is written assuming only 2 level Hierarchy
            if (user.ParentUser != null) {
                ownerMsisdn = user.ParentUser.MSISDN;
                parentMsisdn = user.ParentUser.MSISDN;
            } else {
                ownerMsisdn = user.MSISDN;
                parentMsisdn = user.MSISDN;
            }
            String dateOfBirth = "28/02/1986";
            String expireDate = "28/02/2024";
            String issueDate = "28/02/2014";

            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, providerName, payInstName);
            MobileGroupRole mRole = user.hasPayInstTypeLinked(payInstName, providerName);

            String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

            String custId, accountNum;

            if (user.custIdOptional != null) {
                custId = user.custIdOptional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                custId = user.MSISDN;
            } else {
                custId = DataFactory.getRandomNumberAsString(9);
            }

            if (user.accNumOtional != null) {
                accountNum = user.accNumOtional;
            } else if (AppConfig.isBankAccLinkViaMsisdn) {
                accountNum = user.MSISDN;
            } else {
                accountNum = DataFactory.getRandomNumberAsString(9);
            }
            String geoCode = GlobalData.autGeography.AreaCode;


            String[] allData = {
                    "Mr",//	NamePrefix (Mr/Mrs/Miss/M/s)*
                    user.FirstName, //	FirstName*
                    user.LastName,  //	LastName*
                    user.ExternalCode, //	Identification Number*
                    "Gurugram", // City
                    "Haryana", //	State
                    "IN",//Country
                    user.Email,//Email *
                    "Pawam",//Designation
                    user.LoginId,//Contact Person
                    user.MSISDN, //Contact Number
                    "Male",//Gender(Male / Female)
                    dateOfBirth, //Date of Birth * (dd / mm / yyyy)
                    DataFactory.getRandomNumberAsString(6),//Registration Form Number
                    user.LoginId,//Web login Id *
                    "A", //User Status*(A / M / N)
                    user.MSISDN, //MSISDN *
                    "English",    //	Preferred Language*
                    user.CategoryCode,    //	User Category*
                    ownerMsisdn,    //	Owner MSISDN*
                    parentMsisdn,    //	Parent MSISDN*
                    geoCode,    //	Geography Domain Code*
                    user.WebGroupRole,    //	Group Role Code*
                    mRole.ProfileCode,    //	Mobile Group Role Id*
                    user.GradeCode,    //	Grade Code*
                    l_tcpID,    //	TCP Profile Id*
                    isPrimary,    //	Primary Account (Y/N)*
                    custId,    //	Customer Id
                    accountNum,    //	Account Number
                    "Saving",    //	Account Type
                    "A",    //	Wallet Type/Linked Bank Status* (A/M/S/D)
                    merchantType,    //	Merchant Type(ONLINE/OFFLINE)
                    supportOnlineTransactionReversal,    // Supports Online Trans Reversal (Y / N)
                    issueDate,    //	IdIssueDate
                    expireDate,    //	IdExpiryDate
                    "IN",    //	IdIssueCountry
                    "IN",    //	ResidenceCountry
                    "120021",    //	PostalCode
                    "IN",    //	Nationality
                    "Mahindra Comviva",    //	EmployerName
                    "TRUE",    //	IdExpiresAllowed
                    user.LastName,//_shortName
                    "Gurugram",//_address1
                    "Haryana",//address2
                    "usr@123.com",    //	Url(Semicolon Separated)
                    "123",    //	Block Notification(0/1/2/3/4/5/6/7)
                    "add", //Address *
                    "PASSPORT", //IDType *
                    user.ExternalCode, //IDNumber *
                    "Pawam", //Nickname *
                    "RELOFF12345",    //	Relationship Officer*
                    "N",//Autosweep Allowed (Y / N)
                    "",//ContactIDType
                    "",//AllowedDays
                    "",//AllowedFormTime
                    "",//AllowedToTime
                    "COMF1",    //	CommercialField1
                    "COMF2",    //	CommercialField2
                    "COMF3",    //	CommercialField3
                    "COMF4",    //	CommercialField4
                    "COMF5",    //	CommercialField5
                    "COMF6",    //	CommercialField6
                    "COMF7",    //	CommercialField7
                    "COMF8",    //	CommercialField8
                    "COMF9",    //	CommercialField9
                    "COMF10",    //	CommercialField10
                    "COMF11",    //	CommercialField11
                    "ShopName"    //	ShopName
            };

            pNode.info("Writing User User Related info to CSV");

            if (isAppend)
                ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, true, true);
            else
                ExcelUtil.writeDataToBulkUserRegisModifCsv(file, allData, true, false);


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Bar Channel User
     *
     * @param user
     * @param userType e.g. Constants.USER_TYPE_CHANNEL
     * @param barType
     * @return
     * @throws Exception
     */
    public String barChannelUser(User user, String userType, String barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("barChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(baruser);
            BarUser_pg1 pageOne = BarUser_pg1.init(pNode);

            pageOne.navigateNAToBarUserLink();
            pageOne.selectUserTypeByValue(userType);
            pageOne.setMsisdn(user.MSISDN);
            pageOne.barType(barType);
            pageOne.reasonForBar("BY_OPT");
            pageOne.remarksForBar("Testing");
            pageOne.clickSubmit();
            if (ConfigInput.isConfirm) {
                pageOne.clickConfirm();
                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("user.bar.success", "Verify Successfully Barring a Channel User:" + user.LoginId, pNode)) {
                        actualMessage = pageOne.getMessage();
                        user.setStatus(barType);
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return actualMessage;
    }

    /**
     * unBarChannelUser
     *
     * @param user
     * @param userType
     * @param barType
     * @return
     * @throws Exception
     */
    public String unBarChannelUser(User user, String userType, String barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("UnBarUser: " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (user.isBarred()) {
                UnBarUser_pg1 pageOne = new UnBarUser_pg1(pNode);

                // * Navigate to Add Channel User Page *
                pageOne.navigateNAToUnBarUserLink();

                pageOne.selectUserTypeByValue(userType);
                pageOne.setMsisdn(user.MSISDN);
                pageOne.clickSubmit();
                pageOne.selectUserFromList();
                pageOne.clickOnUnbarSubmit();
                pageOne.clickOnUnbarConfirm();

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("user.unbar.success", "Verify Un-Bar Channel user: " + user.LoginId, pNode)) {
                        user.setStatus(Constants.STATUS_ACTIVE);
                        actualMessage = pageOne.getMessage();
                    }
                }
            } else {
                pNode.skip("User status is already marked as Un Barred, re check User Object");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return actualMessage;
    }


    /**
     * Delete Channel User
     *
     * @param user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement deleteChannelUser(User user) throws Exception {
        Login.init(pNode).login(delChUser);
        initiateChannelUserDelete(user);

        Login.init(pNode).login(delChUserApprover);
        approveRejectDeleteChannelUser(user);

        return this;
    }

    /**
     * Reset User Category
     *
     * @param user
     * @param newCategory
     * @return
     * @throws Exception
     */
    public ChannelUserManagement resetUserCategory(User user, String newCategory) throws Exception {
        user.CategoryCode = newCategory;
        user.ParentUser = null;
        user.GradeName = null;
        user.WebGroupRole = DataFactory.getWebGroupRoleName(user.CategoryCode);
        user.setUserDetails();
        return this;
    }

    /**
     * Irrespective of the current page, this method completes the channel user modification
     *
     * @return
     * @throws Exception
     */
    public ChannelUserManagement completeChannelUserModification(String... bankStatus) throws Exception {

        Markup m = MarkupHelper.createLabel("completeChannelUserModification", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            String bankStatusChange = bankStatus.length > 0 ? bankStatus[0] : "Y";
            //AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(pNode);
            /*pageOne.setDateOfIssuer(new DateAndTime().getDate(0));
            pageOne.setDateOfExpiry(new DateAndTime().getDate(21));*/
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            if (fl.elementIsDisplayed(page.nextUserInfo)) {
                page.clickNextUserDetail();
            }

            if (fl.elementIsDisplayed(page.nextHierarcy)) {
                page.clickNextUserHeirarcy();
            }

            if (fl.elementIsDisplayed(page.nextGroupRole)) {
                page.clickNextWebRole();
            }

            if (fl.elementIsDisplayed(page.nextWalletMap)) {
                page.clickNextWalletMap();
            }

            if (fl.elementIsDisplayed(page.nextBankPref)) {
                page.clickNextBankMapModification();
            }

            AlertHandle.handleUnExpectedAlert(pNode);
            AlertHandle.handleUnExpectedAlert(pNode);


            if (fl.elementIsDisplayed(page.finalSubmitModification)) {
                page.finalSubmitForModification();
            }

            if (fl.elementIsDisplayed(page.selectEnterprisePreferences)) {
                page.clickEnterprisePreference();
            }

            if (ConfigInput.isAssert) {
                Utils.captureScreen(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Irrespective of the current page, this method takes you to the requested page by submitting all required pages.
     * <p>
     * 1 : Assign Hierarchy Page
     * 2 : Web Roles Page
     * 3 : Wallet Page
     * 4 : Bank Page
     * 5 : Confirm Page
     *
     * @return
     * @throws Exception
     */
    public ChannelUserManagement gotoPage(int pageID) throws Exception {
        Markup m = MarkupHelper.createLabel("gotoPage: " + pageID, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            // If Page ID == 1 method will submit till Assign Hierarchy Page.
            if (pageID >= 1 && fl.elementIsDisplayed(page.nextUserInfo)) {
                page.clickNextUserDetail();
            }

            // If Page ID == 2 method will submit till Web Roles Page.
            if (pageID >= 2 && fl.elementIsDisplayed(page.nextHierarcy)) {
                page.clickNextUserHeirarcy();
            }

            // If Page ID == 3 method will submit till Wallet Page.
            if (pageID >= 3 && fl.elementIsDisplayed(page.nextGroupRole)) {
                page.clickNextWebRole();
            }

            // If Page ID == 4 method will submit till Bank Page.
            if (pageID >= 4 && fl.elementIsDisplayed(page.nextWalletMap)) {
                page.clickNextWalletMap();
            }

            // If Page ID == 5 method will submit till confirm page.
            if (pageID >= 5 && fl.elementIsDisplayed(page.finalSubmitModification)) {
                page.finalSubmitForModification();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement validateFormFields() throws IOException {

        Boolean iseditable = isEditable("//*[@name='bankCounterList[0].providerSelected']", "MFS Provider", pNode);
        Assertion.verifyEqual(iseditable, false, "MFS Provider", pNode, true);

        iseditable = isEditable("//*[@name='bankCounterList[0].paymentTypeSelected']", "Bank", pNode);
        Assertion.verifyEqual(iseditable, false, "bank", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].channelGradeSelected']", "Grade", pNode);
        Assertion.verifyEqual(iseditable, true, "Grade", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].groupRoleSelected']", "Mobile Group Role", pNode);
        Assertion.verifyEqual(iseditable, true, "Mobile Group Role", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].tcpSelected']", "TCP Profile", pNode);
        Assertion.verifyEqual(iseditable, true, "TCP Profile", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].customerId']", "customerId", pNode);
        Assertion.verifyEqual(iseditable, false, "customerId", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].accountNumber']", "Account no", pNode);
        Assertion.verifyEqual(iseditable, false, "Account Number", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].primaryAccountSelected']", "Primary Account", pNode);
        Assertion.verifyEqual(iseditable, true, "Primary Account", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].accountTypeSelected']", "Account Type", pNode);
        Assertion.verifyEqual(iseditable, true, "Account Type", pNode, false);

        iseditable = isEditable("//*[@name='bankCounterList[0].statusSelected']", "Status", pNode);
        Assertion.verifyEqual(iseditable, true, "Status", pNode, false);

        return this;
    }

    public boolean isEditable(String xpath, String name, ExtentTest t1) {
        try {
            boolean eleStatus = driver.findElement(By.xpath(xpath)).isEnabled();
            t1.info("The " + name + " element is enabled set as " + eleStatus);
            return eleStatus;
        } catch (Exception e) {
            return false;
        }
    }

    public ChannelUserManagement changeBankStatus(String status, int bankSize) {
        Markup m = MarkupHelper.createLabel("changeBankStatus: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
        for (int i = 1; i <= bankSize; i++) {
            page.changeBankStatus(status, i);
        }

        return this;
    }


    public ChannelUserManagement changeBankStatus(String bankName, String status) {
        Markup m = MarkupHelper.createLabel("changeBankStatus: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

        page.changeBankStatus(bankName, status);

        return this;
    }

    public ChannelUserManagement completeEnterpriseModification() throws Exception {

        Markup m = MarkupHelper.createLabel("completeChannelUserModification: ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

        if (fl.elementIsDisplayed(page.nextUserInfo)) {
            page.clickNextUserDetail();
        }

        if (fl.elementIsDisplayed(page.nextHierarcy)) {
            page.clickNextUserHeirarcy();
        }

        if (fl.elementIsDisplayed(page.nextGroupRole)) {
            page.clickNextWebRole();
        }

        if (fl.elementIsDisplayed(page.selectEnterprisePreferences)) {
            page.clickEnterprisePreference();
        }

        if (fl.elementIsDisplayed(page.assosicateWalletNext)) {
            page.clickEnterpriseAssociateWallet();
        }

        if (fl.elementIsDisplayed(page.nextWalletMap)) {
            page.clickNextWalletMap();
        }

        if (fl.elementIsDisplayed(page.nextBankPrefModification)) {
            page.clickNextBankMapModification();
        }
        if (fl.elementIsDisplayed(page.finalSubmitModification)) {
            page.finalSubmitForModification();
        }


        return this;
    }


    public ChannelUserManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public ChannelUserManagement startNegativeTestWithoutConfirm() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        ConfigInput.isConfirm = false;
        return this;
    }

    /**
     * verifyFieldInApprovalPage - verify Fields at approval Page
     *
     * @param user
     * @param fieldvalue
     * @return
     * @throws Exception
     */
    public ChannelUserManagement verifyFieldInApprovalPage(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldInApprovalPage: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navigateToChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforApproval(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitApprovalDetail();
            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On Approval page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On Approval page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement verifyFieldInModApprovalPage(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldInModApprovalPage: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModifiedApproval(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifiedApprovalDetail();

            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On Modify Approval page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On Modify Approval page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement verifyFieldOnDeleteChUserPage(User user, String fieldvalue) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("verifyFieldOnDeleteChUserPag: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navDeleteChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforDeletion(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitDeleteDetail();

            try {
                String temTxt = DriverFactory.getDriver()
                        .findElement(By.xpath("//label[contains(text(),'" + fieldvalue + "')]")).getText();

                Assertion.verifyEqual(temTxt, fieldvalue + ":",
                        "Verify for field: " + fieldvalue + " On Delete Ch user page for user - " + user.LoginId, pNode);
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pNode.fail("failed to find the field " + fieldvalue + " On Delete Ch user page for user - " + user.LoginId);
                Assertion.raiseExceptionAndContinue(e, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Initiate Hierarchy Branch Movement
     *
     * @param user
     * @return
     * @throws Exception
     */
    public void approveSuspendChannelUser(User user, boolean approve) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveSuspendChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode)
                    .login(suspendApprover);

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUserApproval();
            page.selectSuspendDomain(user.DomainCode);
            page.selectSuspendApprovalCategory(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.suspendSubmitFirstPageBtn();

            if (approve == true) {
                page.confirmApproval();
                Assertion.verifyActionMessageContain("channeluser.message.suspend",
                        "Approve, Suspend Channel User - " + user.LoginId, pNode, user.FirstName, user.LastName);
            } else {
                page.clickonSuspendReject();
                Assertion.verifyActionMessageContain("channeluser.message.reject",
                        "Reject, Suspend Channel User - " + user.LoginId, pNode, user.FirstName, user.LastName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

   /* public void suspendChannelUserApproval(User user, boolean approve) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateSuspendUserApproval: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode)
                    .login(usrApprover);

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUserApproval();
            page.selectSuspendDomainName(user.DomainName);
            page.selectSuspendCategory(user.CategoryName);
            page.setMsisdnForAction(user.MSISDN);

            if(approve == true) {
                page.suspendSubmitFirstPageBtn();
                page.confirmApproval();

                Assertion.assertActionMessageContain(MessageReader.getDynamicMessage("channeluser.message.suspend",
                        user.LoginId, user.LoginId), "Suspend Channel User Approved " + user.LoginId, pNode);
            }
            else
            {
                page.clickonSuspendReject();
                Assertion.assertActionMessageContain(MessageReader.getDynamicMessage("channeluser.message.reject",
                        user.LoginId, user.LoginId), "Suspend Channel User Reject " + user.LoginId, pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }
*/

    /**
     * viewBarredChannelUser
     *
     * @param user
     * @param userType
     * @param barType
     * @return
     * @throws Exception
     */
    public void viewBarredUser(User user, String userType, String... barType) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("viewBarredUser: " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ViewBarUser_pg1 pageOne = ViewBarUser_pg1.init(pNode);
            // * Navigate to Add Channel User Page *
            pageOne.navigateToLink();

            pageOne.selectUserTypeByValue(userType);
            pageOne.setMsisdn(user.MSISDN);
            pageOne.clickSubmit();
            String msisdn = pageOne.getUserMSISDN(user.MSISDN);
            String name = pageOne.getUserName(user.MSISDN);

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(msisdn, user.MSISDN, "View Barred User", pNode, true);
                Assertion.verifyEqual(name, user.FirstName, "View Barred User", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    /**
     * unBarChannelUser
     *
     * @param user
     * @param userType
     * @param barType
     * @return
     * @throws Exception
     */
    public String unBarChannelUser(User user, String userType, String... barType) throws Exception {
        String actualMessage = null;
        try {
            Markup m = MarkupHelper.createLabel("UnBarUser: " + user.MSISDN, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (user.isBarred()) {
                UnBarUser_pg1 pageOne = new UnBarUser_pg1(pNode);

                // * Navigate to Add Channel User Page *
                pageOne.navigateNAToUnBarUserLink();

                pageOne.selectUserTypeByValue(userType);
                pageOne.setMsisdn(user.MSISDN);
                pageOne.clickSubmit();
                pageOne.selectUserFromList();
                pageOne.clickOnUnbarSubmit();
                pageOne.clickOnUnbarConfirm();

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("user.unbar.success", "Un Bar User", pNode)) {
                        user.setStatus(Constants.STATUS_ACTIVE);
                    }
                }
            } else {
                pNode.skip("User status is already un Barred, re check User Object!");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return actualMessage;
    }


    public ChannelUserManagement suspendChannelUser(User user) throws Exception {
        try {
            OperatorUser initiator = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);
            OperatorUser approver = DataFactory.getOperatorUserWithAccess("PTY_SCHAPP", Constants.CHANNEL_ADMIN);
            Login.init(pNode)
                    .login(initiator);
            initiateSuspendChannelUser(user);

            // this is to handle the Application inconsistency,
            // when trying to suspend use on same session, category options is not getting populated consistently
            Login.resetLoginStatus();
            Login.init(pNode)
                    .login(approver);
            suspendChannelUserApproval(user);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }


    /**
     * Suspend Channel User
     *
     * @param user
     * @return
     * @throws IOException
     */
    public ChannelUserManagement initiateSuspendChannelUser(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateSuspendChannelUser: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUser();
            page.selectSuspendDomain(user.DomainCode);
            page.selectSuspendCategory(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.suspendSubmitFirstPageBtn();
            page.confirmDelete();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("channeluser.suspend.approval",
                        "Suspend Channel User ", pNode, user.FirstName, user.LastName)) {
                    user.setStatus(Constants.STATUS_SUSPEND_INITIATE);
                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public ChannelUserManagement suspendChannelUserApproval(User user, boolean... isApprove) throws IOException {
        boolean isApproved = isApprove.length > 0 ? isApprove[0] : true;
        try {
            Markup m = MarkupHelper.createLabel("suspendChannelUserApproval: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUserApproval();
            page.selectSuspendDomain(user.DomainCode);
            page.selectSuspendApprovalCategory(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.suspendSubmitFirstPageBtn();
            if (isApproved) {
                page.clickSuspendApprove();

                if (ConfigInput.isAssert) {
                    if (Assertion.verifyActionMessageContain("channeluser.message.suspend",
                            "Verify Suspen Channel User: " + user.LoginId, pNode, user.FirstName, user.LastName)) {
                        user.setStatus(Constants.STATUS_SUSPEND);
                    }
                }
            } else {
                page.clickSuspendReject();
                if (ConfigInput.isAssert) {
                    String actual = Assertion.getActionMessage();
                    String expected = MessageReader.getDynamicMessage("channeluser.message.suspend.reject", user.FirstName, user.LastName);
                    Assertion.verifyEqual(actual, expected, "Suspend Channel User Rejection", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement resumeChannelUser(User user) throws Exception {

        OperatorUser usrResume = DataFactory.getOperatorUsersWithAccess("PTY_RCU").get(0);
        OperatorUser usrResumeApp = DataFactory.getOperatorUsersWithAccess("PTY_RCU").get(0);

        Login.init(pNode).login(usrResume);

        resumeInitiateChannelUser(user);

        if (!usrResume.LoginId.equalsIgnoreCase(usrResumeApp.LoginId))
            Login.init(pNode).login(usrResumeApp);

        resumeChannelUserApproval(user, true);

        return this;
    }

    public ChannelUserManagement resumeInitiateChannelUser(User user) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("resumeChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ResumeChannelUser_Page page = new ResumeChannelUser_Page(pNode);

            page.navResumeChannelUser();
            page.selectDomain(user.DomainCode);
            page.selectCategory(user.CategoryCode);
            page.setFirstName(user.FirstName);
            page.clickSubmit();
            page.clickConfirm();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = MessageReader.getDynamicMessage("channeluser.resume.approval", user.FirstName, user.LastName);
                Assertion.verifyEqual(actual, expected, "Resume Channel User", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public ChannelUserManagement writeBulkUserBankAssociationDetailsInCSV(User user, String file, boolean... append) throws Exception {
        boolean appendstatus = append.length > 0 ? append[0] : false;
        String userType;
        try {
            Markup m = MarkupHelper.createLabel("writeBulkChUserRegDetailsInCSV: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            //todo - explain
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

            userType = (DataFactory.isLeafDomain(user.CategoryCode)) ? "SUBSCRIBER" : "CHANNEL";

            if (AppConfig.isUserTypeAllowed) {
                String[] allData = {user.MSISDN,
                        DataFactory.getDefaultBankIdForDefaultProvider(),
                        defaultProvider.ProviderId,
                        userType};
                pNode.info("Writing User Bank Related info to CSV");
                ExcelUtil.writeDataToBulkBankAssociCsv(file, allData);
            } else {
                String[] allData = {user.MSISDN,
                        DataFactory.getDefaultBankIdForDefaultProvider(),
                        defaultProvider.ProviderId};
                pNode.info("Writing User Bank Related info to CSV");
                if (appendstatus)
                    ExcelUtil.writeDataToBulkBankAssociCsv(file, allData, true);
                else
                    ExcelUtil.writeDataToBulkBankAssociCsv(file, allData);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Resume Channel User Approval / Rejection
     *
     * @param user
     * @param approve - False if approval need to be rejected
     * @return
     * @throws Exception
     */
    public ChannelUserManagement resumeChannelUserApproval(User user, boolean approve) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("resumeChannelUserApproval: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            OperatorUser usrResume = DataFactory.getOperatorUsersWithAccess("PTY_RCU").get(0);

            Login.init(pNode).login(usrResume);

            ResumeChannelUserApproval_Page page = new ResumeChannelUserApproval_Page(pNode);

            page.navResumeChannelUserApproval();
            page.selectDomain(user.DomainCode);
            page.selectCategory(user.CategoryCode);
            page.setFirstName(user.FirstName);
            page.clickSubmit();

            if (approve == true) {
                page.clickApprove();

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("channeluser.message.resume",
                            "Verify Resume Channel User Approval", pNode, user.FirstName, user.LastName);
                }
            } else {
                page.clickOnResumeReject();
                Assertion.verifyActionMessageContain("channeluser.message.reject",
                        "Suspend Channel User Approved ", pNode, user.LoginId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement approveChannelUser(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveChannelUser:" + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            if (!usrCreator.LoginId.equals(usrApprover.LoginId)) {
                Login.init(pNode).login(usrApprover);
            }

            CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

      /*  if (!user.CategoryCode.equals(Constants.ENTERPRISE)) {
            Login.init(pNode).login(bankApprover);
            CommonUserManagement.init(pNode)
                   .approveAllAssociatedBanks(user);
        }*/
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public void verifyErrorMsgforAddchannelUser() throws Exception {
        Assertion.verifyErrorMessageContain("channeluser.validation.userName", "First Name is mandatory.", pNode);
        Assertion.verifyErrorMessageContain("user.error.lastnamerequired", "Last Name is mandatory", pNode);
        Assertion.verifyErrorMessageContain("subs.error.emailId", "Email is mandatory.", pNode);
        Assertion.verifyErrorMessageContain("systemparty.label.garson.relationship.ui", "Relationship Officer Required", pNode);
        Assertion.verifyErrorMessageContain("subs.error.selectnameprefix", "Please select the name prefix.", pNode);
        Assertion.verifyErrorMessageContain("subs.error.genderrequired", "Please Select the gender", pNode);
        Assertion.verifyErrorMessageContain("subs.externalCode.required", "Identification Number is mandatory", pNode);
        Assertion.verifyErrorMessageContain("pls.enter.validweb.loginid", "Please enter valid Web Login ID", pNode);
        //Assertion.verifyErrorMessageContain("#c2c.error.payeeAccessId","Please enter Mobile Number",pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.doc1", "Select a identity proof to upload", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.doc2", "Select a address proof to upload", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.doc3", "Select a photo proof to upload", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.proof1", "Please select identity proof type", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.proof2", "Please select address proof type", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.proof3", "Please select photo proof type", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.id.type", "Please Select ID Type", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.id.no", "Please enter ID Number", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.webLoginId", "Web Login ID is mandatory", pNode);
        Assertion.verifyErrorMessageContain("channeluser.error.password.required", "Password is required", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.passwdlength.ui", "The password length must be between 6 & 8", pNode);
        Assertion.verifyErrorMessageContain("user.error.preflangrequired", "Preferred Language required", pNode);
    }

    public void verifyErrorMsgforModifychannelUser() throws Exception {
        Assertion.verifyErrorMessageContain("systemparty.validation.prefix", "User Name prefix is mandatory", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.userName", "First Name is mandatory", pNode);
        Assertion.verifyErrorMessageContain("user.error.lastnamerequired", "Last Name is mandatory", pNode);
        Assertion.verifyErrorMessageContain("subs.error.emailId", "Email is mandatory", pNode);
        Assertion.verifyErrorMessageContain("user.error.genderprefixrequired", "Gender prefix is mandatory", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.externalCode.empty", "Identification Number is mandatory", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.id.type", "Please Select ID Type", pNode);
        Assertion.verifyErrorMessageContain("subsreg.error.id.no", "Please enter ID Number", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.webLoginId", "Web Login ID is mandatory", pNode);
        Assertion.verifyErrorMessageContain("systemparty.validation.loginlength.ui", "The login Id length must be between 3 & 20", pNode);
        Assertion.verifyErrorMessageContain("channeluser.validation.password", "Password is mandatory", pNode);
        Assertion.verifyErrorMessageContain("systemparty.validation.confirmpassword", "Confirm Password is mandatory", pNode);
    }


    /**
     * Enable autoDebit By channel User
     *
     * @param biller
     * @param amt
     * @return
     * @throws Exception
     */
    public String enableAutoDebitByChanneluser(Biller biller, String customerMSISDN, String billAccNum, String amt) throws Exception {
        Markup m = MarkupHelper.createLabel("enableAutoDebitByChanneluser", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String providerName = DataFactory.getDefaultProvider().ProviderName;
        String wallet = DataFactory.getDefaultWallet().WalletName;
        String txnid = null;

        //String Reason = reason.length > 0 ? reason[0] : "";
        try {

            AutoDebitEnable_pg1 page = AutoDebitEnable_pg1.init(pNode);
            page.navAutoDebitEnable();
            page.enterMsisdn(customerMSISDN);
            page.selectProviderId(providerName);
            page.selectWallet(wallet);
            page.selectBeneficiary(biller.BillerCode);
            page.selectAccountNumber(billAccNum);
            page.enterAmount(amt);
            page.clickSubmit();


            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();//TODO specific assertion
                txnid = actual.split("Id ")[1].trim();
                String expected = MessageReader.getDynamicMessage("autodebit.enable.successful", customerMSISDN, txnid);
                Assertion.verifyEqual(actual, expected, "Auto Debit Enable initiate", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return txnid;
    }

    public ChannelUserManagement billPayment(Biller biller, String processType, String service, String accNo, String amount, User usr) throws Exception {
        Markup m = MarkupHelper.createLabel("billPayment", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String txnid = null;
        User billPayer = DataFactory.getChannelUserWithAccess("BP_RET");
        try {
            Login.init(pNode).login(billPayer);

            BillPayment_pg1 page = BillPayment_pg1.init(pNode);

            page.navigateBillPaymentLink();
            page.selectProcess(processType);
            page.selectServices(service);
            page.selectBillerCode(biller.BillerCode);
            page.selectProvider(DataFactory.getDefaultProvider().ProviderName);
            page.setBillerAccNo(accNo);
            page.setAmount(amount);
            if (service.equalsIgnoreCase(Services.AgentAssistedPresentmentBillPayment)) {
                page.setBillNumber(accNo);
            }
            page.setReferenceMSISDN(usr.MSISDN);
            page.clickSubmit();

            page.clickConfirm();

            if (ConfigInput.isAssert) {

                String actual = Assertion.getActionMessage();//TODO specific assertion
                txnid = actual.split("ID: ")[1].trim();
                if (service.equalsIgnoreCase(Services.AgentAssistedPresentmentBillPayment)) {
                    String expected = MessageReader.getDynamicMessage("agent.assisten.presentment.bill.success", MobiquityGUIQueries.getCurrencyCode(DataFactory.getDefaultProvider().ProviderId), amount, billPayer.LoginId, biller.BillerCode, txnid);
                    Assertion.verifyEqual(actual, expected, "Bill Payment", pNode, true);
                } else {
                    String expected = MessageReader.getDynamicMessage("agent.assisted.bill.success", amount, billPayer.LoginId, biller.BillerCode, txnid);
                    Assertion.verifyEqual(actual, expected, "Bill Payment", pNode, true);
                }

            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    public ChannelUserManagement checkIfUserBalanceisCredited(BigDecimal preBalance, BigDecimal postBalance, boolean isDebit, String userName) throws Exception {
        pNode.info("PreBalance: " + preBalance + " / PostBalance: " + postBalance);

        if (isDebit) {
            Assertion.verifyEqual(preBalance.compareTo(postBalance) > 0, true, "Verify that User Balance is Debited", pNode);
        } else {
            Assertion.verifyEqual(preBalance.compareTo(postBalance) < 0, true, "Verify that User Balance is Credited", pNode);
        }
        return this;
    }

    /**
     * Initiate Hierarchy Branch Movement
     *
     * @param user
     * @return
     * @throws Exception
     */
    public void initiateSuspendUserApproval(User user, boolean approve) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateSuspendUserApproval: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode)
                    .login(usrApprover);

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUserApproval();
            page.selectSuspendDomain(user.DomainCode);
            page.selectSuspendCategory(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.suspendSubmitFirstPageBtn();

            if (approve == true) {
                page.confirmApproval();
                Assertion.assertActionMessageContain(MessageReader.getDynamicMessage("channeluser.message.suspend", user.LoginId, user.LoginId), "Suspend Channel User Approved " + user.LoginId, pNode);
            } else {
                page.clickonSuspendReject();
                Assertion.assertActionMessageContain(MessageReader.getDynamicMessage("channeluser.message.reject",
                        user.LoginId, user.LoginId), "Suspend Channel User Approved " + user.LoginId, pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public void completeChannelUserCreation(User user, Boolean isCreated, ExtentTest chNode) throws Exception {
        Markup m = MarkupHelper.createLabel("completeChannelUserCreation: ", ExtentColor.BLUE);
        chNode.info(m); // Method Start Marker

        try {
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(chNode);
            page5.completeUserCreation(user, false);

            if (isCreated) {
                Assertion.assertActionMessageContain("channeluser.add.approval",
                        "Create User Having Associated with Parent's Account Number and Customer ID", chNode);
            } else {
                Assertion.verifyActionMessageContain("channeluser.modify.approval",
                        "Modify User Having Associated with Parent's Account Number and Customer ID",
                        chNode, user.FirstName, user.LastName);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, chNode);
        }

    }

    /**
     * Modify Channel User Details
     *
     * @param user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement modifyChannelUserMSISDN(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyChannelUserMSISDN " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            user.setMSISDN();
            page.setMsisdnInUserModificationPage(user.MSISDN);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public ChannelUserManagement suspendOrDeleteUserWallet(User user, String status, String walletId) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("suspendOrDeleteUserWallet: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();

            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();

            page.updateWalletStatus(status, walletId);
            page.clickNextWalletMap();
            page.clickNextBankMapModification();
            try {
                //AlertHandle.acceptAlert(pNode);
                //AlertHandle.acceptAlert(pNode);
                driver.switchTo().alert().accept();
                driver.switchTo().alert().accept();
            } catch (NoAlertPresentException e) {
                pNode.info("No alert");
            }
            page.finalSubmitForModification();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @param bankName
     * @return
     * @throws Exception
     */

    public ChannelUserManagement createChannelUserDefaultMappingwithliquidationbank(User user, String bankName) throws Exception {
        Login.init(pNode).login(usrCreator);
        initiateChannelUser(user);
        assignHierarchy(user);

        CommonUserManagement.init(pNode)
                .assignWebGroupRole(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreference();
            setEnterpriseCategoryWalletCombination();
        }

        CommonUserManagement.init(pNode)
                .mapDefaultWalletPreferences(user);

        if (user.CategoryCode.equals(Constants.ENTERPRISE)) {
            setEnterprisePreferenceForLiquidationBank(); // map preferences 2nd page
        }

        Liquidation liq = new Liquidation();

        CommonUserManagement.init(pNode)
                .mapDefaultBankPreferencesforliquidation(user, bankName, liq);


        Login.init(pNode).login(usrApprover);
        CommonUserManagement.init(pNode).addInitiatedApproval(user, true);

        Login.init(pNode).login(bankApprover);

        CommonUserManagement.init(pNode)
                .approveAssociatedBanksForUser(user, DataFactory.getDefaultProvider().ProviderName, bankName);

        CommonUserManagement.init(pNode).changeFirstTimePassword(user);

        // Change Mpin Tpin
        Transactions.init(pNode)
                .changeChannelUserMPinTPin(user);

        return this;
    }

    /**
     * ApproveChannelUserModification
     *
     * @param user
     * @return
     * @throws Exception
     */

    public ChannelUserManagement approveChannelUserModification(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveChannelUserModification: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUserApproval();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModifiedApproval(user.CategoryCode);
            page.setMsisdn(user);
            page.submitModifiedApprovalDetail();
            if (ConfigInput.isConfirm) {
                page.submitModifiedFinalApprovalDetail();
                // todo assertion
                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("channeluser.message.updation",
                            "Verify that channel user is successfully approved", pNode, user.FirstName, user.LastName);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * suspend Initiate ChannelUser
     *
     * @param user
     * @return
     * @throws Exception
     */

    public ChannelUserManagement suspendInitiateChannelUser(User user) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("suspendInitiateChannelUser: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navSuspendChannelUser();
            page.selectDomainNameforsuspend(user.DomainName);
            page.selectCategoryforsuspend(user.CategoryName);
            page.setMsisdnForAction(user.MSISDN);
            page.suspendSubmitFirstPageBtn();
            page.confirmDelete();

            Assertion.verifyActionMessageContain("channeluser.suspend.approval", "Suspend Channel User ", pNode, user.LoginId, user.LoginId);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @return
     * @throws Exception
     */
    public ChannelUserManagement modifyChannelUserLoginIdPassword(User user, String loginId, String password) throws Exception {

        Markup m = MarkupHelper.createLabel("modifyChannelUserLoginIdPassword: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            Login.init(pNode).login(usrCreator);
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

            page.navModifyChannelUser();
            page.selectDomainName(user.DomainCode);
            page.selectCategoryforModification(user.CategoryCode);
            page.setMsisdnForAction(user.MSISDN);
            page.submitModifyDetail();
            page.modifyLoginId(loginId);
            page.modifyPassword(password);
            page.clickNxtModify();

            if (ConfigInput.isConfirm) {
                AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(pNode);
                hierarchy.clickNext();

                page.clickNextWebRole();

                page.clickNextWalletMap();

                AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(pNode);
                page5.clickNext(user.CategoryCode); // navigate to the next page

                //clickOnElement(btnSubmitChUserEdit, "btnSubmitChUserEdit");
                page5.completeUserCreation(user, true);

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("channeluser.modify.approval",
                            "Modify User initiated, " + user.LoginId, pNode, user.FirstName, user.LastName);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public ChannelUserManagement expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public ChannelUserManagement setIsContactPersonRequired() {
        this.isOptionalFieldRequired = true;
        return this;
    }

    public String generateBulkUserBankAssociationcsvFile(User user1, User user2, String bankID) {
        final String mfsProvider = DataFactory.getDefaultProvider().ProviderId;

        String filePath = "uploads/" + "BulkUserBankAssociation" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        //String bankID = DataFactory.getDefaultBankId(DataFactory.getDefaultProvider().ProviderId);

        File f = new File(fileName);
        try {

            fileWriter = new FileWriter(f);
            fileWriter.append(FileHeaderBulkUserBankAssociation);
            fileWriter.append(NEW_LINE_SEPARATOR);

            /*for (String key : msisdn) {

                if (!(users.get(key).CategoryCode.contains("Enterprise"))) {
*/
            fileWriter.append(user1.MSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(bankID);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.USR_TYPE_SUBS);
            fileWriter.append("\n");

            fileWriter.append(user2.MSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(bankID);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mfsProvider);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Constants.USR_TYPE_SUBS);
            fileWriter.append("\n");
              /*  }
            }*/
        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            e.printStackTrace();
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }

    public String bulkUserBankDisAssociation(String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("bulkUserBankDisAssociation:", ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(bulkBankDisassociator);

            AddChannelUser_pg1 page = AddChannelUser_pg1.init(pNode);
            page.navBulkBankDisAssociation();
            Thread.sleep(2000);

            if (page.uploadFile(fileName)) {
                // if (page.fileUpload(fileName)) {
                page.clickOnSubmitBtn();
                Thread.sleep(5000);

                if (ConfigInput.isAssert) {
                    String actualMessage = page.getActionMessage();
                    if (actualMessage != null) {
                        String s = actualMessage.split("Bulk ID :")[1].trim();
                        bulkID = s;
                        Assertion.verifyActionMessageContain("bulk.bank.DisAssociation",
                                "Bulk Bank Association", pNode, bulkID);
                    }
                }
            } else {
                Assert.fail("Failed to Initiate BulkUserBankDisassociation, exiting the test");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    public String initiateBulkUserBankAssociation(String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkUserBankAssociation:", ExtentColor.BLUE);
            pNode.info(m);

            String lastLogin = Login.init(pNode).getLastLogin();
            if (lastLogin == null || !lastLogin.toLowerCase().contains("NWADM")) {
                Login.init(pNode).login(bulkBankAssociator);
            }

            AddChannelUser_pg1 page = AddChannelUser_pg1.init(pNode);
            page.navBulkBankAssociation();
            Thread.sleep(2000);

            if (page.uploadFile(fileName)) {
                // if (page.fileUpload(fileName)) {
                page.clickOnSubmitBtn();
                Thread.sleep(5000);

                if (ConfigInput.isAssert) {
                    String actualMessage = page.getActionMessage();
                    if (actualMessage != null) {
                        String[] s = actualMessage.split("batch with ")[1].trim().split(" ");
                        bulkID = s[0];
                        Assertion.verifyActionMessageContain("bulk.bank.Association", "Bulk Bank Association", pNode, bulkID);
                    }
                }
            } else {
                Assert.fail("Failed to Initiate BulkUserBankAssociation, exiting the test");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }


    public ChannelUserManagement initiateApproveBulkUserModification(String file) throws IOException {

        try {
            Login.init(pNode).login(optBulk);
            BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(pNode);
            page.navBulkUserRegistration();
            page.uploadFile(file);
            page.submitCsv();

            // Verify that user Registration is Successful

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("bulkUpload.channel.label.success", "Verify that the Bulk User Modification is successful", pNode)) {
                    String message = driver.findElements(By.className("actionMessage")).get(0).getText();
                    String batchId = message.split("ID :")[1].trim();
                    ChannelUserManagement.init(pNode)
                            .approveBulkRegistration(batchId);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }


        return this;
    }

    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }


}
