package framework.features.userManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.pageObjects.homeScreen_pg;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by amarnath.vb on 5/23/2017.
 */
public class PseudoUserManagement {
    private static ExtentTest pNode;
    private static OperatorUser usrCreator, usrApprover, bankApprover;
    private static WebDriver driver;



    public static PseudoUserManagement init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;
            driver = DriverFactory.getDriver();
            if (usrCreator == null) {
                usrCreator = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);
                usrApprover = DataFactory.getOperatorUsersWithAccess("PTY_CHAPP2").get(0);
                bankApprover = DataFactory.getOperatorUsersWithAccess("BNK_APR").get(0);


            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new PseudoUserManagement();
    }

    public void CreatePseudoCategory(User user) throws Exception {
        Login.init(pNode).login(usrCreator.LoginId, usrCreator.Password);
        ExtentTest chNode = pNode.createNode("Initiate ChannelUser: " + user.LoginId);
        AddPseudoCategoryPage1 pageOne = new AddPseudoCategoryPage1(chNode);

        // * Navigate to Add Channel User Page *
        pageOne.navPseudoUserCategoryCreation();
        chNode.info("Successfully navigated to Pseudo User Category Management Page!");
    }

    //modifyPseudoCategory
    public String modifyPseudoCategory(User user, String categoryName) throws Exception {

        Markup m = MarkupHelper.createLabel("modifyPseudoCategory: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            modifyPseudoCategory_Pg1 pageOne = modifyPseudoCategory_Pg1.init(pNode);
            String newCategoryName = "PsCat" + DataFactory.getRandomNumber(5);
            Login.init(pNode).login(user);

            pageOne.navPseudoUserCategoryManagement();
            pageOne.selectCategoryForUpdation(categoryName);
            pageOne.clickUpdate();

            pageOne.setNewCategoryCode(newCategoryName);
            pageOne.setNewCategoryName(newCategoryName);
            pageOne.clickModify();
            pageOne.clickPseudomodifyConfirm();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Pseudo.modify.initiate.Category", "Verify Pseudo Category Update Initiation", pNode, newCategoryName);
            }
            return newCategoryName;
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;

    }


    // Delete PseudoCategory

    public String deletePseudoCategory(User userToLogin, String categoryName) throws Exception {

        Utils.createLabelForMethod("deletePseudoCategory", pNode);

        try {
            modifyPseudoCategory_Pg1 pageOne = modifyPseudoCategory_Pg1.init(pNode);
            Login.init(pNode).login(userToLogin);
            // * Navigate to pseudoCategoryManagement Page *
            pageOne.navPseudoUserCategoryManagement();
            pageOne.selectCategoryForUpdation(categoryName);
            pageOne.clickDelete();
            AlertHandle.acceptAlert(pNode);

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("Pseudo.delete.initiate.Category", categoryName);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "verification for category initiate message", pNode, true);
            }
            return categoryName;
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;

    }


    public String UpdatePseudoCategory(User parentUser) throws Exception {
        try {
            String categoryName = initiateAndApprovePseudoCategory(parentUser);
            modifyPseudoCategory(parentUser, categoryName);
            return categoryName;
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }


    public String DeletePseudoUserCategory(User parentUser) throws Exception {
        try {
            String categoryName = initiateAndApprovePseudoCategory(parentUser);
            deletePseudoCategory(parentUser, categoryName);
            return categoryName;
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * Create Pseudo Category and also the Group Role
     *
     * @param user
     * @return
     * @throws Exception
     */
    public String createPseudoCategoryAndGroupRole(User user) throws Exception {
        String categoryCode = "PsCat" + DataFactory.getRandomNumber(5);
        String categoryName = initiatePseudoCategory(user,categoryCode,categoryCode);
        createPseudoGroupRole(categoryName);
        return categoryName;
    }

    /**
     *
     * @param psCatCode
     */
    public String createPseudoCategoryIfNotExist(User parentUser,String psCatCode) throws Exception {

        String categoryCode= null;
        Login.init(pNode).login(parentUser);

        String query = "select PSEUDO_CAT_CODE from MTX_PSEUDO_CATEGORIES where PSEUDO_CAT_CODE='"+psCatCode+"' and status='Y'";

        String psCatCOdeDB = MobiquityGUIQueries.executeQueryAndReturnResult(query,"PSEUDO_CAT_CODE");

        if(psCatCOdeDB != null){
            pNode.info("Pseudo Category Code Already Exist");
        }else {
            categoryCode = initiatePseudoCategory(parentUser, psCatCode, psCatCode);
            approvePseudoUserCategory(categoryCode);
        }

       return categoryCode;
    }

    public String initiateAndApprovePseudoCategory(User parentUser) throws Exception {
        try {
            String categoryCode = "PsCat" + DataFactory.getRandomNumber(5);
            String categoryName = initiatePseudoCategory(parentUser,categoryCode,categoryCode);
            approvePseudoUserCategory(categoryName);
            return categoryName;
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    /**
     * Initiate Pseudo User Category
     *
     * @param parentUser
     * @throws Exception
     */
    public String initiatePseudoCategory(User parentUser,String catCode,String catName) throws Exception {

        Utils.createLabelForMethod("initiatePseudoCategory: " + parentUser.LoginId, pNode);

        try {
            AddPseudoCategoryPage1 pageOne = new AddPseudoCategoryPage1(pNode);

            Login.init(pNode).login(parentUser);
            pageOne.navPseudoUserCategoryCreation();
            pageOne.clickAddInitiate();
            pageOne.setParentCategoryList(parentUser.CategoryName);
            pageOne.setNewCategoryCode(catCode);
            pageOne.setNewCategoryName(catName);
            pageOne.clickAdd();
            pageOne.clickPseudoAddConfirm();
            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("PSEUDO_CAT_MGMT_INITIATE", catName);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "verification for category initiate message", pNode, true);
            }
            return catCode;
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;
    }

    /**
     * @param categoryName
     * @throws Exception
     */
    public void approvePseudoUserCategory(String categoryName) throws Exception {
        try {
            OperatorUser chnlAdmin = DataFactory.getOperatorUserWithAccess("PSEUDO_CAT_APPROVAL", Constants.CHANNEL_ADMIN);
            Login.init(pNode).login(chnlAdmin);
            homeScreen_pg homeScreen = homeScreen_pg.init(pNode);
            homeScreen.navigateToApproveCategoryPage();
            if (ConfigInput.isConfirm)
                homeScreen.approveCategoryForPseudoUser(categoryName);
            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("pseudouser.category.approval", categoryName);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "verification for category approval message", pNode, true);
            }


        } catch (Exception e) {
            Assertion.markAsFailure("Failed to approve Pseudo User Category ");
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


    /**
     * Pseudo User Group Role Creation
     *
     * @param pseudoCategory
     * @return -  Newly Created Pseudo User Category Name
     * @throws Exception
     */
    public PseudoUserManagement createPseudoGroupRole(String pseudoCategory) throws Exception {
        Markup m = MarkupHelper.createLabel("createPseudoGroupRole : for category " + pseudoCategory, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            AddPseudoCategoryPage1 pageOne = new AddPseudoCategoryPage1(pNode);
            homeScreen_pg homeScreen = homeScreen_pg.init(pNode);
            pseudoGroupRole_pg1 groupRole = pseudoGroupRole_pg1.init(pNode);

            pageOne.navPseudoUserCategoryCreation();
            homeScreen.clickPseudoGroupRole();
            groupRole.setPseudoCategoryCodeList(pseudoCategory);
            groupRole.groupRoleSubmit();
            groupRole.clickGroupRoleAdd();
            groupRole.setGroupRoleCode(pseudoCategory);
            groupRole.setAddGroupRoleName(pseudoCategory);
            groupRole.clickAddGroupRoleCheckAll();
            //groupRole.clickAddGroupRoleScreenAdd();
            if (ConfigInput.isConfirm)
                groupRole.clickAddGroupRoleScreenAdd();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grouprole.added", "Groupe role added", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    /**
     * Add Pseudo User
     *
     * @param parentUser
     * @param psUser
     * @throws Exception
     */
    public void addPseudoUser(User parentUser, PseudoUser psUser) throws Exception {
        Markup m = MarkupHelper.createLabel("assignHierarchy: " + psUser.LoginId, ExtentColor.BLUE);
        pNode.info(m);

        try {
            AddPseudoUser_pg1_2 pageOne = AddPseudoUser_pg1_2.init(pNode);
            homeScreen_pg homeScreenPg = homeScreen_pg.init(pNode);

            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);
            // * Navigate to Pseudo User Page *
            homeScreenPg.navPseudoUserCreation();
            homeScreenPg.navAddPseudoUserPage();

            pageOne.setPseudoNamePrefix();
            pageOne.setPseudoFirstName(psUser.FirstName);
            pageOne.setPseudoLastName(psUser.LastName);
            pageOne.setPseudoMSISDN(psUser.MSISDN);
            pageOne.setPseudoAgentCode(psUser.MSISDN);
            pageOne.setPseudoUserIdNo(psUser.ExternalCode);
            pageOne.setPseudoDivision(psUser.Division);
            pageOne.setPseudoDepartment(psUser.Department);
            pageOne.setPseudocontactNo(psUser.MSISDN);
            pageOne.setPseudoContactPerson(psUser.MSISDN);
            pageOne.setPseudoAppointmentDate(DataFactory.getCurrentDate());
            pageOne.setPseudoCheckAll();
            pageOne.setPseudoLoginId(psUser.LoginId);
            pageOne.setPseudoPassword(ConfigInput.userCreationPassword);
            pageOne.setPseudoConfPassword(ConfigInput.userCreationPassword);
            pageOne.clickNext();

            pageOne.setCategoryCodeList(psUser.PseudoCategoryCode);
            pageOne.setPseudoParentId(parentUser.CategoryName);
            pageOne.setPseudoParentName(parentUser.ParentCategoryName);
            pageOne.setPseudoOwnerId(parentUser.OwnerCategoryName);
            pageOne.setPseudoGroupRole(psUser.PseudoGroupRole);// to be taken from created pseudo group role
            pageOne.setPseudoSave();
            pageOne.setpseudoSaveConfirm();

            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("Pseudo.Initiation", psUser.MSISDN);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Assert initiate message", pNode, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Change First Time Password
     *
     * @param user
     */
    public PseudoUserManagement changeFirstTimePassword(PseudoUser user) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("changeFirstTimePassword: " + user.LoginId, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChangePasswordPage changPwd = ChangePasswordPage.init(pNode);
            FunctionLibrary fl = new FunctionLibrary(driver);

            Login.init(pNode).login(user.LoginId, ConfigInput.userCreationPassword);
            fl.contentFrame();

            changPwd.setOldPassword(ConfigInput.userCreationPassword);
            changPwd.setNewPassword(user.Password);
            changPwd.setConfirmPassword(user.Password);
            Thread.sleep(3000);
            changPwd.clickSubmit();

            if(Assertion.verifyActionMessageContain("changePassword.label.success", "Change User Password", pNode)){
                user.setIsCreated();
            }

            if (driver.findElements(By.id("skip_for_later")).size() > 0) {
                changPwd.clickSkip();
                driver.switchTo().alert().accept();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Create Complete Pseudo User
     *
     * @param psUser
     * @return
     * @throws Exception
     */
    public PseudoUser createCompletePseudoUser(PseudoUser psUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("createCompletePseudoUser: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);

            //add a pseudo user category
            PseudoUserManagement pseudoUserManagement = PseudoUserManagement.init(pNode);

            String categoryName = initiateAndApprovePseudoCategory(psUser.parentUser);
            psUser.setCategoryDetails(categoryName);


            //login again as parent user for creating group role
            Login.init(pNode).login(psUser.parentUser);
            pseudoUserManagement.createPseudoGroupRole(psUser.PseudoCategoryCode);


            psUser.setCategoryDetails(categoryName);
            pseudoUserManagement.addPseudoUser(psUser.parentUser, psUser);

            //login using channel admin and approve pseudo user
            Login.init(pNode).login(netAdmin);
            approvePseudoUser(psUser);

            //change pseudo user 1st time password
            pseudoUserManagement.changeFirstTimePassword(psUser);
            Transactions.init(pNode).changeChannelUserMPinTPinforPseudoUser(psUser);

            return psUser;

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;
    }


    public PseudoUser createModifyPseudoUser(PseudoUser psUser, User parentUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("createCompletePseudoUser: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);

            //add a pseudo user category
            PseudoUserManagement pseudoUserManagement = PseudoUserManagement.init(pNode);

            String categoryName = initiateAndApprovePseudoCategory(parentUser);
            psUser.setCategoryDetails(categoryName);


            //login again as parent user for creating group role
            Login.init(pNode).login(parentUser);
            pseudoUserManagement.createPseudoGroupRole(psUser.PseudoCategoryCode);


            psUser.setCategoryDetails(categoryName);
            pseudoUserManagement.addPseudoUser(parentUser, psUser);

            //login using channel admin and approve pseudo user
            Login.init(pNode).login(netAdmin);
            approvePseudoUser(psUser);

            //change pseudo user 1st time password
            pseudoUserManagement.changeFirstTimePassword(psUser);
            Transactions.init(pNode).changeChannelUserMPinTPinforPseudoUser(psUser);

            pseudoUserManagement.modifyPseudoUser(parentUser, psUser, categoryName);
            OperatorUser chAdm = DataFactory.getOperatorUserWithAccess("PSEUDO_MOD2");
            approvePseudoUserModification(chAdm, psUser);
            return psUser;

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return null;
    }


    public void approvePseudoUser(PseudoUser psUser) throws Exception {
        try {
            homeScreen_pg homeScreen = homeScreen_pg.init(pNode);
            homeScreen.navigateToApprovePseudoUser();
            homeScreen.approvePseudoUserAddition(psUser.MSISDN);
            if (ConfigInput.isAssert) {
                String id = Assertion.getActionMessage();
                id = id.split("for")[1].trim();
                String dynamicMessage = MessageReader.getDynamicMessage("pseudo.initiated.successfully", id);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Assert Approve message", pNode);

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyPseudoUser(User parentUser, PseudoUser pseudoUser, String pseudoCategoryCode) throws Exception {
        Markup m = MarkupHelper.createLabel("modifyPseudoUser : " + parentUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);

        try {
            AddPseudoUser_pg1_2 pageOne = AddPseudoUser_pg1_2.init(pNode);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);

            pageOne.navPseudoUserModification();

            pageOne.setPseudocat(pseudoCategoryCode);
            pageOne.setPseudoParentCategory();
            pageOne.setPseudoOwnerCategory();
            pageOne.setMsisdnTxtField(pseudoUser.MSISDN);
            pageOne.clickonSubit();
            pageOne.clickonNxt1();
            pageOne.clickonNxt2();
            pageOne.clickConfirmInitiate();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("PSEUDO_MODIFY_INITIATE",
                        "Pseudo User Modify Initiation Successfully", pNode, pseudoUser.MSISDN);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void approvePseudoUserModification(OperatorUser parentUser, PseudoUser pseudoUser) throws Exception {
        Markup m = MarkupHelper.createLabel("approvePseudoUserModification: " + pseudoUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);

        try {
            AddPseudoUser_pg1_2 page = AddPseudoUser_pg1_2.init(pNode);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);

            page.navModifyApprove();
            page.clickEntryForApproval(pseudoUser.MSISDN);
            page.clickConfirmComplete();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("pseudo.user.modify.approve.success",
                        "Verify Pseudo User modificaion is Approved Successfully: " + pseudoUser.LoginId, pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }


    }

    /**
     * @param pseudoUser
     * @throws Exception
     */
    public PseudoUserManagement initiateDeletePseudoUser(PseudoUser pseudoUser) throws Exception {
        Markup m = MarkupHelper.createLabel("Delete Initiate Pseudo User: " + pseudoUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);
        try {

            DeleteInitiatePseudoUser_page1.init(pNode).navToDeletePseudoUser()
                    .setPseudoCatCode(pseudoUser.PseudoCategoryCode)
                    .setParentCatCode().setOwnerCatCode()
                    .setMsisdn(pseudoUser.MSISDN).clickSubmit()
                    .confirmDelete();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("pseudo.delete.user.initiate",
                        "Pseudo User Deleted", pNode, pseudoUser.MSISDN);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public void DeletePseudoUser(User parentUser, PseudoUser pseudoUser, String pseudoCategoryCode) throws Exception {
        Markup m = MarkupHelper.createLabel("Delete Initiate Pseudo User: " + pseudoUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);
        try {
            DeleteInitiatePseudoUser_page1 pageOne = DeleteInitiatePseudoUser_page1.init(pNode);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);
            pageOne.navToDeletePseudoUser();
            pageOne.setPseudoCatCode(pseudoCategoryCode);
            pageOne.setParentCatCode();
            pageOne.setOwnerCatCode();
            pageOne.setMsisdn(pseudoUser.MSISDN);
            pageOne.clickSubmit();
            pageOne.confirmDelete();


            if (ConfigInput.isAssert) {
                String dynamicMessage = MessageReader.getDynamicMessage("pseudo.delete.user.initiate", pseudoUser.MSISDN);
                Assertion.verifyEqual(Assertion.getActionMessage(), dynamicMessage, "Pseudo User Deleted", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * @param pseudoUser
     * @param isApprove
     */
    public void approveDeletePseudoUser(PseudoUser pseudoUser, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("Delete Approve Pseudo User: " + pseudoUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);
        String userID = MobiquityGUIQueries.getUserUserId(pseudoUser.MSISDN);

        OperatorUser chAdm = DataFactory.getOperatorUserWithAccess("PSEUDO_DELETE2");
        Login.init(pNode).login(chAdm);
        try {
            if (isApprove) {
                DeleteApprovalPseudoUser_page1.init(pNode)
                        .navToDeletePsuedoUserApprove()
                        .clickApprove(pseudoUser.MSISDN)
                        .confirmDelete();

                if (ConfigInput.isAssert) {
                    Assertion.verifyActionMessageContain("pseudo.user.delete.approve", "Psuedo User Delete Approved", pNode, userID);
                }

            } else {
                DeleteApprovalPseudoUser_page1.init(pNode)
                        .navToDeletePsuedoUserApprove()
                        .clickReject(pseudoUser.MSISDN);

                if (ConfigInput.isAssert) {
                    Thread.sleep(Constants.WAIT_TIME);
                    driver.switchTo().alert().accept();
                    Assertion.verifyActionMessageContain("pseudo.user.delete.reject", "Pseudo User Delete Rejected", pNode, userID);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param pseudoUser
     * @param providerID
     * @param walletType
     * @param profileName
     * @param shortName
     * @throws Exception
     */
    public void addUpdatePseudoTcp(PseudoUser pseudoUser, String providerID, String walletType, String profileName, String shortName) throws Exception {
        Markup m = MarkupHelper.createLabel("addUpdatePseudoTcp: " + pseudoUser.LoginId, ExtentColor.BLUE);
        pNode.info(m);

        User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        Login.init(pNode).login(chUser);

        try {
            Add_Update_PseudoUserTCP_page1.init(pNode)
                    .navToModifyPseudoTcp()
                    .setMsisdn(pseudoUser.MSISDN);
           Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
            Add_Update_PseudoUserTCP_page1.init(pNode).setMFSProvider(providerID)
                    .selectWallet(walletType)
                    .setProfileName(profileName).setProileShortName(shortName)
                    .clickSubmitBtn1()
                    .clickSubmitBtn2()
                    .clickConfirm();

            if (ConfigInput.isAssert) {
                String expMsg = Assertion.getActionMessage();
                String actMsg1 = MessageReader.getMessage("pseudo.tcp.add", null);
                if (expMsg.contains(actMsg1)) {
                    Assertion.verifyActionMessageContain("pseudo.tcp.add", "Pueudo TCP Added", pNode);
                } else {
                    Assertion.verifyActionMessageContain("pseudo.tcp.update", "Pseudo Role Updated Successfully", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void PseudoUserDeletion(PseudoUser psUser, User parentUser) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("createCompletePseudoUser: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);

            //add a pseudo user category
            PseudoUserManagement pseudoUserManagement = PseudoUserManagement.init(pNode);

            String categoryName = createPseudoCategoryIfNotExist(parentUser,Constants.AUT_PSEUDO_CATEGORY);
            psUser.setCategoryDetails(categoryName);


            //login again as parent user for creating group role
            Login.init(pNode).login(parentUser);
            pseudoUserManagement.createPseudoGroupRole(psUser.PseudoCategoryCode);


            psUser.setCategoryDetails(categoryName);
            pseudoUserManagement.addPseudoUser(parentUser, psUser);

            //login using channel admin and approve pseudo user
            Login.init(pNode).login(netAdmin);
            approvePseudoUser(psUser);

            //change pseudo user 1st time password
            pseudoUserManagement.changeFirstTimePassword(psUser);
            Transactions.init(pNode).changeChannelUserMPinTPinforPseudoUser(psUser);
            DeletePseudoUser(parentUser, psUser, categoryName);
            approveDeletePseudoUser(psUser, true);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }


    }


    public void createandUpdatePseudoGroupRole(PseudoUser psUser, User parentUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("createPseudoGroupRole: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
//            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);

            //add a pseudo user category
           // PseudoUserManagement pseudoUserManagement = PseudoUserManagement.init(pNode);
            createCompletePseudoUser(psUser);

//            String categoryName = createPseudoCategoryIfNotExist(parentUser,Constants.AUT_PSEUDO_CATEGORY);
//            psUser.setCategoryDetails(categoryName);


            //login again as parent user for creating group role
//            Login.init(pNode).login(parentUser);
//
//            createPseudoGroupRole(psUser.PseudoCategoryCode);
//
//
//            psUser.setCategoryDetails(categoryName);
//            addPseudoUser(parentUser, psUser);
//
//            //login using channel admin and approve pseudo user
//            Login.init(pNode).login(netAdmin);
//            approvePseudoUser(psUser);
//
//            //change pseudo user 1st time password
//            changeFirstTimePassword(psUser);
//            Transactions.init(pNode).changeChannelUserMPinTPinforPseudoUser(psUser);
            ModifyPseudoRole(parentUser, psUser, psUser.PseudoCategoryCode);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public void ModifyPseudoRole(User parentUser, PseudoUser psUser, String pseudoCategoryCode) throws Exception {
        Markup m = MarkupHelper.createLabel("Modify Pseudo Group Role: " + parentUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);
        try {
            ModifyPseudoRoles_page1 page1 = ModifyPseudoRoles_page1.init(pNode);
            String providerID = DataFactory.getDefaultProvider().ProviderId;
            String role = "pseudo" + DataFactory.getRandomNumberAsString(3);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);

            page1.navToModifyPseudoRoles();
            page1.setMsisdn(psUser.MSISDN);
            page1.setMFSProvider(providerID);
            page1.selectWallet(Constants.NORMAL_WALLET);
            page1.setRoleCode(role).setRoleName(role);
            page1.clickAddButton().clickConfirm();

            Assertion.verifyActionMessageContain("pseudo.roles.success", "Pseudo Role Updated Successfully", pNode);
        } catch (Exception e) {
        }
    }


    public void ModifyPseudoTCP(User parentUser, PseudoUser psUser, String pseudoCategoryCode) throws Exception {
        Markup m = MarkupHelper.createLabel("Modify Pseudo TCP: " + parentUser.LoginId, ExtentColor.YELLOW);
        pNode.info(m);
        try {
            Add_Update_PseudoUserTCP_page1 page1 = Add_Update_PseudoUserTCP_page1.init(pNode);
            String providerID = DataFactory.getDefaultProvider().ProviderId;
            String tcp = "pseudoTcp" + DataFactory.getRandomNumberAsString(3);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);

            page1.navToModifyPseudoTcp();
            page1.setMsisdn(psUser.MSISDN);
            page1.setMFSProvider(providerID);
            page1.selectWallet(Constants.NORMAL_WALLET);
            page1.setProfileName(tcp).setProileShortName(tcp);
            page1.clickSubmitBtn1().clickSubmitBtn2().clickConfirm();

            if (ConfigInput.isAssert) {
                String expMsg = Assertion.getActionMessage();
                String actMsg1 = MessageReader.getMessage("pseudo.tcp.add", null);
                if (expMsg.contains(actMsg1)) {
                    Assertion.verifyActionMessageContain("pseudo.tcp.add", "Pseudo TCP Added", pNode);
                } else {
                    Assertion.verifyActionMessageContain("pseudo.tcp.update", "Pseudo Role Updated Successfully", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void createandUpdatePseudoTCP(PseudoUser psUser, User parentUser) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("createPseudoTCP: ", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker
            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);

            //add a pseudo user category
            PseudoUserManagement pseudoUserManagement = PseudoUserManagement.init(pNode);

            String categoryName = initiateAndApprovePseudoCategory(parentUser);
            psUser.setCategoryDetails(categoryName);


            //login again as parent user for creating group role
            Login.init(pNode).login(parentUser);
            pseudoUserManagement.createPseudoGroupRole(psUser.PseudoCategoryCode);


            psUser.setCategoryDetails(categoryName);
            pseudoUserManagement.addPseudoUser(parentUser, psUser);

            //login using channel admin and approve pseudo user
            Login.init(pNode).login(netAdmin);
            approvePseudoUser(psUser);

            //change pseudo user 1st time password
            pseudoUserManagement.changeFirstTimePassword(psUser);
            Transactions.init(pNode).changeChannelUserMPinTPinforPseudoUser(psUser);
            pseudoUserManagement.ModifyPseudoTCP(parentUser, psUser, psUser.PseudoCategoryCode);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public void ModifyCountAndAmountInPseudoTCP(User parentUser, PseudoUser psUser, String payerCount,String payerAmount) throws Exception {
        Markup m = MarkupHelper.createLabel("Modify Pseudo TCP: ", ExtentColor.YELLOW);
        pNode.info(m);
        try {

            Add_Update_PseudoUserTCP_page1 page1 = Add_Update_PseudoUserTCP_page1.init(pNode);
            String providerID = DataFactory.getDefaultProvider().ProviderId;
            String tcp = "pseudoTcp" + DataFactory.getRandomNumberAsString(3);
            Login.init(pNode).login(parentUser.LoginId, parentUser.Password);

            page1.navToModifyPseudoTcp();
            page1.setMsisdn(psUser.MSISDN);
            Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
            page1.setMFSProvider(providerID);
            Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
            page1.selectWallet(Constants.NORMAL_WALLET);
            page1.setProfileName(tcp).setProileShortName(tcp);
            page1.clickSubmitBtn1();
            page1.modifyTCPThresholdAllServices(payerCount, payerAmount);
            page1.clickSubmitBtn2().clickConfirm();

            if (ConfigInput.isAssert) {
                String expMsg = Assertion.getActionMessage();
                String actMsg1 = MessageReader.getMessage("pseudo.tcp.add", null);
                if (expMsg.contains(actMsg1)) {
                    Assertion.verifyActionMessageContain("pseudo.tcp.add", "Pueudo TCP Added", pNode);
                } else {
                    Assertion.verifyActionMessageContain("pseudo.tcp.update", "Pseudo Role Updated Successfully", pNode);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


}







