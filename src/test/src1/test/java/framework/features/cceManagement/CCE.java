/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 01-Jan-2018
*  Purpose: Features of CCE
*/

package framework.features.cceManagement;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.CCE.CustomerCareExecutiveEnquiryPage1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.math.BigDecimal;


/**
 * Created by surya.dhal on 1/8/2018.
 */
public class CCE {
    private static WebDriver driver;
    private static ExtentTest pNode;

    public static CCE init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new CCE();
    }


    public CCE enterUserDetails(User user, String accountIdentifier, String filterData) throws IOException {
        String userType;
        if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            userType = Constants.USR_TYPE_SUBS;
        } else
            userType = Constants.CHANNEL_REIMB;
        CustomerCareExecutiveEnquiryPage1 pageObj = new CustomerCareExecutiveEnquiryPage1(pNode);
        try {
            pageObj.navCustomerCareExecutiveEnquiry();
            Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            pageObj.handleWindow();
            pageObj.selectUserType(userType);
            pageObj.selectAccountIdentifier(accountIdentifier);
            pageObj.setFilterData(filterData);
            pageObj.clickOnSubmitButton();
            Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
            pageObj.closeChildWindow();
            pageObj.switchToMainWindow();
        }
        return this;
    }


    /**
     * Method to check User details
     *
     * @param user
     * @throws Exception
     */
    public void checkUserDetails(User user) throws IOException {

        Markup m = MarkupHelper.createLabel("checkTransactionDetails", ExtentColor.CYAN);
        pNode.info(m);
        try {
            String guiStatus, dbStatus, status;
            CustomerCareExecutiveEnquiryPage1 pageObj = new CustomerCareExecutiveEnquiryPage1(pNode);
            guiStatus = pageObj.getConsumerStatus();
            dbStatus = MobiquityGUIQueries.getUserStatus(user.MSISDN, user.CategoryCode);
            if (dbStatus.equalsIgnoreCase("Y")) {
                status = "Active";
            } else status = dbStatus;
            Assertion.verifyEqual(guiStatus, status, "Verify User Status", pNode);
            pageObj.closeChildWindow();
            pageObj.switchToMainWindow();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Method to Navigate to Account information TAB and verify Balance and Grade
     *
     * @param user
     * @throws Exception
     */
    public void checkAccountInformation(User user, String infoType) throws IOException {

        Markup m = MarkupHelper.createLabel("checkAccountInformation", ExtentColor.ORANGE);
        pNode.info(m);
        try {
            CustomerCareExecutiveEnquiryPage1 pageObj = new CustomerCareExecutiveEnquiryPage1(pNode);
            pageObj.clickOnAccountInformationTab();
            Utils.putThreadSleep(NumberConstants.SLEEP_3000);
            if (infoType.equalsIgnoreCase("sva")) {
                String gradeOnGUI = pageObj.getGrade();
                Assertion.verifyEqual(gradeOnGUI, user.GradeName, "Verify Account Information", pNode);
                String balanceOnGUI = pageObj.getBalance();
                BigDecimal userBalance = MobiquityGUIQueries.getUserBalance(user, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId).Balance;
                Assertion.verifyEqual(balanceOnGUI, userBalance, "Verify Account Information", pNode);
            } else if (infoType.equalsIgnoreCase("bank")) {
                pageObj.clickOnBankInfoLink();
                String accountID = pageObj.getAccountID();
                Assertion.verifyEqual(accountID, user.DefaultAccNum, "Verify Account ID of user", pNode);
            }
            pageObj.closeChildWindow();
            pageObj.switchToMainWindow();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Feature Method to Reset User Credentials
     *
     * @param credentialType
     * @throws Exception
     */
    public void resetCredentials(String credentialType) throws IOException {

        Markup m = MarkupHelper.createLabel("resetCredentials", ExtentColor.ORANGE);
        pNode.info(m);
        String actual = null, expected = null;

        try {
            CustomerCareExecutiveEnquiryPage1 pageObj = new CustomerCareExecutiveEnquiryPage1(pNode);
            pageObj.clickOnResetCredentialsTab();
            Utils.putThreadSleep(NumberConstants.SLEEP_3000);
            if (credentialType.equals("mpin")) {
                pageObj.clickOnResetPINLink();
                pageObj.selectMobileResetPinRadioButton();
                pageObj.selectMPINRadioButton();
                pageObj.clickOnResetPINSubmitButton();
                expected = MessageReader.getMessage("", null);
            } else if (credentialType.equalsIgnoreCase("webPassword")) {
                pageObj.clickOnResetPINLink();
                pageObj.selectWebPasswordResetRadioButton();
                pageObj.clickWebPasswordSubmitButton();
                expected = MessageReader.getMessage("", null);
            } else if (credentialType.equalsIgnoreCase("employeePin")) {
                pageObj.clickOnResetEmployeePINLink();
                pageObj.selectMobileResetPinRadioButton();
                pageObj.selectMPINRadioButton();
                expected = MessageReader.getMessage("", null);
            }
            driver.switchTo().activeElement();
            actual = pageObj.getNotificationMessage();
            // pageObj.closeChildWindow();
            //pageObj.switchToMainWindow();
            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(actual, expected, "Verify Successful Message ", pNode);
            }

            pageObj.closeChildWindow();
            pageObj.switchToMainWindow();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * For negative test
     * Set the assert flag to false
     * this will be reset once the method has completed in the @AfterMethod
     *
     * @return current instance
     */
    public CCE startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        return this;
    }

}
