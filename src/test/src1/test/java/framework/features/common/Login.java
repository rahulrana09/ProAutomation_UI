package framework.features.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.*;
import framework.pageObjects.PageInit;
import framework.util.common.Application;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.io.IOException;

/**
 * Created by Automation Team on 5/6/2017.
 */
public class Login extends PageInit {

    private static String lastLogin = null;
    /*
     * Page Objects
     */
    @FindBy(id = "login_doaccesscodes_loginId")
    private WebElement UserName;
    @FindBy(id = "password")
    private WebElement Password;
    @FindBy(id = "login_doaccesscodes_button_signin")
    private WebElement SignIn;
    @FindBy(id = "login_doaccesscodes_request_locale")
    private WebElement Language;
    @FindBy(className = "errorMessage")
    private WebElement errorMessage;
    @FindBy(className = "toplink")
    private WebElement TopLink;
    @FindBy(partialLinkText = "Logout")
    private WebElement Logout;
    @FindBy(id = "skip_for_later")
    private WebElement SkipForLater;

    public Login(ExtentTest t1) {
        super(t1);
    }

    public static void resetLoginStatus() {
        lastLogin = null;
    }

    public static void setLoginStatus(String loginId) {
        lastLogin = loginId;
    }

    public static Login init(ExtentTest t1) {
        return new Login(t1);
    }

    public void loginAsSuperAdmin(SuperAdmin admin) throws Exception {
        login(admin.LoginId, admin.Password);
    }

    public void loginAsSuperAdmin(String roleCode) throws Exception {
        SuperAdmin admin = DataFactory.getSuperAdminWithAccess(roleCode);
        if (admin != null) {
            login(admin.LoginId, admin.Password);
        } else {
            String msg = "Failed to Fetch Superadmin with Role Code: " + roleCode;
            pageInfo.fail(msg);
            Assert.fail(msg);
        }
    }

    /**
     * Login
     *
     * @param loginId
     * @param password
     * @throws IOException
     */
    public void login(String loginId, String password, int... loginAttempt) throws IOException {
        Markup m = MarkupHelper.createLabel("Login as: " + loginId + " : URL: " + ConfigInput.URL, ExtentColor.PURPLE);
        pageInfo.info(m); // Method Start Marker

        try {
            if (loginId.equalsIgnoreCase(lastLogin)) {
                pageInfo.pass("Already logged in as User - " + loginId);
                return;
            }
            int countLoginAttempt = 0;
            if (loginAttempt.length > 0) {
                countLoginAttempt = loginAttempt[0];
            }

            // try to login
            tryLogin(loginId, password);

            // Handle Popup
            if (AppConfig.isSecurityQuestionOnLoginEnabled && fl.elementIsDisplayed(SkipForLater)) {
                clickOnElement(SkipForLater, "Skip for Later");
                driver.switchTo().alert().accept();
            }

            // verify that login is successful
            if (fl.elementIsDisplayed(Logout)) {
                pageInfo.pass("Successfully Logged in as: " + loginId);
                setLoginStatus(loginId);
                return;
            }

            pageInfo.info("Inconsistent Login Behaviour, trying to re-login: " + loginId);
            countLoginAttempt++;
            if (countLoginAttempt < 3) { // run this loop for max 3 iteration else fail the Login
                login(loginId, password, countLoginAttempt);
            } else {
                pageInfo.fail("Failed to Login after multiple attempts");
                Utils.captureScreen(pageInfo);
                Assert.fail("Failed to Login!");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }
    }

    public void forceLogin(Object usr) throws IOException {
        Login.resetLoginStatus();
        login(usr);
    }

    /**
     * Login
     *
     * @param usr
     * @throws IOException
     */
    public void login(Object usr, int... loginAttempt) throws IOException {

        if (usr == null) {
            String msg = "Couldn't Get the User from Data Factory, Check the AppData and Console log";
            pageInfo.fail(msg);
            Assert.fail(msg);
        }

        String loginId = null, password = null;
        if (usr instanceof User) {
            loginId = ((User) usr).LoginId;
            password = ((User) usr).Password;
        } else if (usr instanceof OperatorUser) {
            loginId = ((OperatorUser) usr).LoginId;
            password = ((OperatorUser) usr).Password;
        } else if (usr instanceof Biller) {
            loginId = ((Biller) usr).LoginId;
            password = ((Biller) usr).Password;
        } else if (usr instanceof PseudoUser) {
            loginId = ((PseudoUser) usr).LoginId;
            password = ((PseudoUser) usr).Password;
        }

        Markup m = MarkupHelper.createLabel("Login as: " + loginId + " : URL: " + ConfigInput.URL, ExtentColor.PURPLE);
        pageInfo.info(m); // Method Start Marker

        try {
            if (loginId.equalsIgnoreCase(lastLogin)) {
                pageInfo.pass("Already logged in as User - " + loginId);
                return;
            }

            int countLoginAttempt = 0;
            if (loginAttempt.length > 0) {
                countLoginAttempt = loginAttempt[0];
            }

            tryLogin(loginId, password);

            // Handle Popup
            if (AppConfig.isSecurityQuestionOnLoginEnabled && fl.elementIsDisplayed(SkipForLater)) {
                clickOnElement(SkipForLater, "Skip for Later");
                driver.switchTo().alert().accept();
            }

            // verify that login is successful
            if (fl.elementIsDisplayed(TopLink)) {
                pageInfo.pass("Successfully Logged in as: " + loginId);
                setLoginStatus(loginId);
                return;
            }

            pageInfo.info("Inconsistent Login Behaviour, trying to re-login: " + loginId);
            countLoginAttempt++;
            if (countLoginAttempt < 3) { // run this loop for max 3 iteration else fail the Login
                login(usr, countLoginAttempt);
            } else {
                pageInfo.fail("Failed to Login after multiple attempts");
                Assert.fail("Failed to Login!");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }

    }

    /**
     * Try Login
     */
    public boolean tryLogin(String userName, String password) throws Exception {
        boolean loginStatus = false;
        try {
            openApplication();
            Thread.sleep(ConfigInput.contentSwitchSync);
            setUserName(userName);
            setPassword(password);
            setLanguage(ConfigInput.prefLanguage);
            Utils.putThreadSleep(ConfigInput.loginSync);
            loginStatus = signIn();
        } catch (Exception e) {
            pageInfo.warning("Issue in Login");
            Assertion.attachScreenShotAndLogs(pageInfo);
            //Assertion.raiseExceptionAndContinue(e, pageInfo);
        }
        return loginStatus;
    }

    /**
     * Open Application
     *
     * @throws IOException
     */
    public void openApplication() throws IOException, InterruptedException {
        try {
            if (fl.elementIsDisplayed(Logout)) {
                clickOnElement(Logout, "Logout");
            }
            Application.openeWebApp();
            pageInfo.info(" Application opened.");
        } catch (Exception e) {
            if (e.getMessage().contains("unexpected alert open")) {
                pageInfo.warning("An unhandled alert was open and handled!");
                driver.switchTo().alert().accept();
                Application.openeWebApp();
            } else {
                e.printStackTrace();
                Assertion.raiseExceptionAndContinue(e, pageInfo);
            }
        }

        Thread.sleep(1000);
    }

    /**
     * Set the User name
     *
     * @param username
     */
    public void setUserName(String username) {
        setTextUsingJs(UserName, username, "User Name");
    }

    /**
     * Set the Password
     *
     * @param password
     */
    public void setPassword(String password) {
        setTextUsingJs(Password, password, "Password");
    }

    /**
     * Sign In
     */
    public boolean signIn() throws Exception {
        clickOnElement(SignIn, "Sign In");
        Utils.putThreadSleep(ConfigInput.loginSync);
        if (!fl.elementIsDisplayed(errorMessage)) {
            fl.contentFrame();
            return true;
        } else {
            pageInfo.info(errorMessage.getText());
            Utils.captureScreen(pageInfo);
            return false;
        }

    }

    //specific to p1 tc
    public void login() throws Exception {
        clickOnElement(SignIn, "Login In");
        Thread.sleep(10000);
    }

    /**
     * Select Language
     *
     * @param language
     */
    public void setLanguage(String language) {
        selectValue(Language, language, "Language");
    }

    public void performLogout() {
        if (fl.elementIsDisplayed(Logout)) {
            clickOnElement(Logout, "Logout");
            Utils.putThreadSleep(Constants.TWO_SECONDS);
        }
    }

    public boolean checkLogoutLinkPresent() throws Exception {
        // if(driver.findElements(By.xpath("//a[contains(@onclick,'logOutParty') and @class='toplink']")).size()>0){
        fl.contentFrame();
        if (fl.elementIsDisplayed(Logout)) {
            pageInfo.info("LogOut Link is Present");
            return true;
        } else {
            return false;
        }
    }

    public boolean checkUserNameAutoComplete() {

        pageInfo.info("Checking AutoComplete of UserName.. ");
        if (UserName.getAttribute("autocomplete").equalsIgnoreCase(Constants.AUTOCOMPLETE_OFF))
            return true;
        else
            return false;
    }

    public boolean checkPasswordAutoComplete() {
        pageInfo.info("Checking AutoComplete of Password field.. ");
        if (Password.getAttribute("autocomplete").equalsIgnoreCase(Constants.AUTOCOMPLETE_OFF))
            return true;
        else
            return false;

    }

    public Login startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);

        ConfigInput.isAssert = false;
        return this;
    }

    public String getLastLogin() {
        return lastLogin;
    }


    /**
     * @param roleCode
     * @return
     * @throws Exception
     */
    public Login loginAsChannelUserWithRole(String roleCode) throws Exception {
        User userWithAccess = DataFactory.getChannelUserWithAccess(roleCode);
        if (userWithAccess != null) {
            login(userWithAccess);
        } else {
            Assertion.markTestAsFailure("Channel User Not Found with Access :" + roleCode, pageInfo);
        }
        return this;
    }


    /**
     * @param roleCode
     * @return
     * @throws Exception
     */
    public Login loginAsOperatorUserWithRole(String roleCode) throws Exception {
        OperatorUser optUserWithAccess = DataFactory.getOperatorUserWithAccess(roleCode);
        if (optUserWithAccess != null) {
            login(optUserWithAccess);
        } else {
            Assertion.markTestAsFailure("Operator User Not Found with Access :" + roleCode, pageInfo);
        }
        return this;
    }
}
