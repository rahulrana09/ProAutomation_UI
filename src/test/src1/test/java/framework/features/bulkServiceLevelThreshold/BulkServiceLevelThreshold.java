package framework.features.bulkServiceLevelThreshold;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.bulkServiceLevelThreshold.BulkServiceLevelThreshold_Page;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class BulkServiceLevelThreshold {

    private static OperatorUser nwBulkServiceThreshold;
    private static ExtentTest pNode;
    private static WebDriver driver;

    // File headers
    String CommonFileHeaderConfiguration = "Add(A) or Modify(M)*,Service Code*,Sender Grade Code*,Receiver Grade Code*,Profile Name*,Multiples of*,Minimum Transaction Amount*,Maximum Transaction Amount*,Sender Payment Method Type(WALLET/BANK)*,Receiver Payment Method Type(WALLET/BANK)*,Sender Account Type ID/ Bank ID*,Receiver Account Type ID/ Bank ID*,Sender Provider ID*,Receiver Provider ID*";

    public static BulkServiceLevelThreshold init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();

        if (nwBulkServiceThreshold == null) {
            nwBulkServiceThreshold = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        }
        return new BulkServiceLevelThreshold();
    }

    public void downloadCurrentConfiguration() throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("downloadCurrentConfiguration", ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(nwBulkServiceThreshold);
            BulkServiceLevelThreshold_Page page = new BulkServiceLevelThreshold_Page(pNode);

            page.navigateToLink();
            Thread.sleep(2000);

            page.clickOnDownloadLink();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
