package framework.features.notification;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import framework.util.JsonPathOperation;
import framework.util.propertiesManagement.MessageReader;
import framework.util.propertiesManagement.MfsTestProperties;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;
import static framework.features.notification.NotificationTemplateContracts.notificationTemplateJson;
import static framework.util.propertiesManagement.MfsTestUtils.moneyUri;
import static framework.util.propertiesManagement.MfsTestUtils.notificationSimulatorUri;

public class NotificationTemplateOperations extends MessageReader {
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();
    private static int pollInterval = mfsTestProperties.getIntProperty("notificationMessage.response.pollIntervalInMillis", 200);
    private static int maxPollTime = mfsTestProperties.getIntProperty("notificationMessage.response.maxPollTimeInMillis", 8000);

    public static ValidatableResponse createNotificationTemplate(JsonPathOperation... operations) {
        return moneyUri().contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(notificationTemplateJson(operations))
                .post("/notification/templates/addTemplate")
                .then().statusCode(200);
    }

    public static ValidatableResponse deleteNotificationTemplate(String templateName) {
        return moneyUri().accept(ContentType.JSON)
                .delete("/notification/templates/deleteTemplate?templateName=" + templateName)
                .then().statusCode(200);
    }

    public static void waitForNotificationMessage(Map requestParameters, Matcher<String> matcher, Locale locale, Object... params) {
        String string = matcher.toString();
        String subSting = string.substring(1, string.length() - 1);
        String message = (getMessage(subSting, locale));
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params);
        }
        await().atMost(maxPollTime, TimeUnit.MILLISECONDS).pollInterval(pollInterval, TimeUnit.MILLISECONDS).
                until(() -> getFirstNotificationMessage(requestParameters), CoreMatchers.equalTo(message));
    }

    public static String getFirstNotificationMessage(Map requestParameters) {
        return getNotificationMessageResponse(requestParameters)
                .extract().jsonPath().getString("messageList[0].message");
    }

    public static ValidatableResponse getNotificationMessageResponse(Map requestParameters) {
        return notificationSimulatorUri().accept(ContentType.JSON)
                .parameters(requestParameters)
                .get("/serviceRequest/GET_NOTIFICATION")
                .then().statusCode(200).log().all();
    }
}
