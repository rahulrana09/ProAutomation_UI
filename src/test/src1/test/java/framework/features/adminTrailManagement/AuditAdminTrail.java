package framework.features.adminTrailManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.pageObjects.auditTrail.AdminTrail_Page1;
import framework.pageObjects.auditTrail.AuditTrail_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by navin.pramanik on 8/9/2017.
 * This file contains Audit Trail and Admin Trail features
 */


/*
    _   _   _ ____ ___ _____  _    ____  __  __ ___ _   _ _____ ____      _    ___ _
   / \ | | | |  _ \_ _|_   _|/ \  |  _ \|  \/  |_ _| \ | |_   _|  _ \    / \  |_ _| |
  / _ \| | | | | | | |  | | / _ \ | | | | |\/| || ||  \| | | | | |_) |  / _ \  | || |
 / ___ \ |_| | |_| | |  | |/ ___ \| |_| | |  | || || |\  | | | |  _ <  / ___ \ | || |___
/_/   \_\___/|____/___| |_/_/   \_\____/|_|  |_|___|_| \_| |_| |_| \_\/_/   \_\___|_____|

 */
public class AuditAdminTrail {

    private static ExtentTest pNode;
    private static WebDriver driver;

    public static AuditAdminTrail init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new AuditAdminTrail();
    }

    public static void downloadAdminFile() throws Exception {
        AdminTrail_Page1 adminTrailPage1 = AdminTrail_Page1.init(pNode);
        Assertion.verifyEqual(adminTrailPage1.downloadButtonClick(), true,
                "Verify Log File is downloaded successfully", pNode);
    }

    public static void verifyAdminDetails(OperatorUser opt) throws IOException {

        Markup m = MarkupHelper.createLabel("verifyAdminDetails", ExtentColor.CYAN);
        pNode.info(m);

        //Map<String, String> auditDetails = MobiquityDBAssertionQueries.getAdminTrailDetails(action, opt.MSISDN);

        String webUserMSISDN, webPartyID, dbPartyID, webCategoryCode, createdBy;

        dbPartyID = MobiquityGUIQueries.fetchUserIDFromUsers(opt.MSISDN);

        if (driver.findElements(By.xpath("//tr/td[contains(text(),'" + opt.MSISDN + "')]/ancestor::tr[1]/td[1]")).size() > 0) {
            webPartyID = driver.findElement(By.xpath("//tr/td[contains(text(),'" + opt.MSISDN + "')]/ancestor::tr[1]/td[1]")).getText();
            webUserMSISDN = driver.findElement(By.xpath("//tr/td[contains(text(),'" + opt.MSISDN + "')]/ancestor::tr[1]/td[2]")).getText();
            webCategoryCode = driver.findElement(By.xpath("//tr/td[contains(text(),'" + opt.MSISDN + "')]/ancestor::tr[1]/td[5]")).getText();

            Assertion.verifyEqual(webUserMSISDN, opt.MSISDN, "Verify User MSISDN.", pNode);
            Assertion.verifyEqual(webPartyID, dbPartyID, "Verify Party ID.", pNode);
            Assertion.verifyEqual(webCategoryCode, opt.CategoryCode, "Verify Category Code.", pNode);

        } else {
            pNode.fail("No record found in Admin trail for User " + opt.MSISDN);
            Utils.captureScreen(pNode);
        }


    }

    /**
     * Method to Verify Admin Trail Table Headers.
     *
     * @throws IOException
     */
    public static void verifyAdminTrailHeaders() throws IOException {

        Markup m = MarkupHelper.createLabel("verifyAdminTrailHeaders", ExtentColor.CYAN);
        pNode.info(m);

        String[] headers = {"User ID", "User MSISDN", "Logged In", "Log Out", "Category", "Action Type",
                "Action Performed On", "Barred User", "Remarks", "Created By", "Created On", "Att 1 Name",
                "Att 1 Value", "Att 2 Name", "Att 2 Value", "Att 3 Name", "Att 3 Value"};

        AdminTrail_Page1 page1 = new AdminTrail_Page1(pNode);
        List<WebElement> list = page1.getTableHeaders();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getText().equalsIgnoreCase(headers[i])) {
                Assertion.verifyEqual(list.get(i).getText(), headers[i], "Verify Admin Trail Table Headers.", pNode);
            }
        }
    }

    public void verifyAuditTrail(String domainCode, String categoryCode, String fromDate, String toDate) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("verifyAuditTrail", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AuditTrail_Page1 auditTrailPage1 = AuditTrail_Page1.init(pNode);

            auditTrailPage1.navigateToLink();
            auditTrailPage1.selectDomainByValue(domainCode);
            auditTrailPage1.selectCategoryByValue(categoryCode);

            if (fromDate != null)
                auditTrailPage1.fromDateSetText(fromDate);
            if (toDate != null)
                auditTrailPage1.toDateSetText(toDate);

            auditTrailPage1.submitButtonClick();

            Assertion.verifyEqual(auditTrailPage1.downloadButtonClick(), true,
                    "Verify Log File is downloaded successfully", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Perform adminTrail  by categoryCode  and domainCode,fromDate and toDate
     *
     * @param optUser  --> Domain Code will require to select the Domain dropdown by its value
     * @param fromDate --> From Date
     * @param toDate   --> To Date
     * @throws Exception --> Throw Exception if it would occur
     */
    public void adminTrail(OperatorUser optUser, String fromDate, String toDate) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("adminTrail", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AdminTrail_Page1 adminTrailPage1 = AdminTrail_Page1.init(pNode);

            adminTrailPage1.navigateToLink();
            if (optUser.DomainCode != null)
                adminTrailPage1.selectDomainByValue(optUser.DomainCode);
            //Thread.sleep(2000);
            if (optUser.CategoryCode != null)
                adminTrailPage1.selectCategoryByValue(optUser.CategoryCode);

            if (fromDate != null)
                adminTrailPage1.fromDateSetText(fromDate);
            if (toDate != null)
                adminTrailPage1.toDateSetText(toDate);
            adminTrailPage1.submitButtonClick();
            if (ConfigInput.isAssert) {
                verifyAdminDetails(optUser);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Perform adminTrail  by categoryCode  and domainCode,fromDate and toDate
     *
     * @param domainCode   --> Domain Code will require to select the Domain dropdown by its value
     * @param categoryCode --> Category Code
     * @param fromDate     --> From Date
     * @param toDate       --> To Date
     * @throws Exception --> Throw Exception if it would occur
     */
    public void adminTrail(String domainCode, String categoryCode, String fromDate, String toDate) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("adminTrail", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AdminTrail_Page1 adminTrailPage1 = AdminTrail_Page1.init(pNode);

            adminTrailPage1.navigateToLink();
            adminTrailPage1.selectDomainByValue(domainCode);
            Thread.sleep(2000);
            adminTrailPage1.selectCategoryByValue(categoryCode);

            if (fromDate != null)
                adminTrailPage1.fromDateSetText(fromDate);
            if (toDate != null)
                adminTrailPage1.toDateSetText(toDate);

            adminTrailPage1.submitButtonClick();

            downloadAdminFile();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Method verifyAuditTrail for a user from specific dates
     *
     * @param user
     * @param fromDate
     * @param toDate
     * @param txnID
     * @throws Exception
     */
    public void verifyAuditTrail(User user, String fromDate, String toDate, String txnID) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("verifyAuditTrail", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AuditTrail_Page1 auditTrailPage1 = AuditTrail_Page1.init(pNode);

            auditTrailPage1.navigateToLink();
            auditTrailPage1.selectDomainByValue(user.DomainCode);
            auditTrailPage1.selectCategoryByValue(user.CategoryCode);

            if (fromDate != null)
                auditTrailPage1.fromDateSetText(fromDate);
            if (toDate != null)
                auditTrailPage1.toDateSetText(toDate);

            auditTrailPage1.submitButtonClick();

            //To verify Screen data with DB data
            verifyAuditDetails(txnID, user.MSISDN);

            Assertion.verifyEqual(auditTrailPage1.downloadButtonClick(), true,
                    "Verify Log File is downloaded successfully", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Added for verifying Preference update in Admin Trail
     *
     * @param optUser
     * @param actionType
     * @param fromDate
     * @param toDate
     * @param prefCode
     * @param prefValue
     * @throws Exception
     */
    public void adminTrailForPreferenceUpdate(OperatorUser optUser, String actionType, String fromDate, String toDate, String prefCode, String prefValue) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("adminTrail", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AdminTrail_Page1 adminTrailPage1 = AdminTrail_Page1.init(pNode);

            adminTrailPage1.navigateToLink();
            adminTrailPage1.selectDomainByValue(DataFactory.getDomainCode(optUser.CategoryCode));
            adminTrailPage1.selectCategoryByValue(optUser.CategoryCode);

            if (fromDate != null)
                adminTrailPage1.fromDateSetText(fromDate);
            if (toDate != null)
                adminTrailPage1.toDateSetText(toDate);

            adminTrailPage1.submitButtonClick();

            verifyAdminDetails(optUser);

            Assertion.verifyEqual(adminTrailPage1.downloadButtonClick(), true,
                    "Verify Log File is downloaded successfully", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Method - verifyAuditDetails
     * This method will verify the Audit trail details
     * on the GUI with the value user have entered
     *
     * @param txnID
     * @param MSISDN
     * @throws NoSuchElementException
     * @throws IOException
     */
    public void verifyAuditDetails(String txnID, String MSISDN) throws NoSuchElementException, IOException, InterruptedException {

        Markup m = MarkupHelper.createLabel("verifyAuditDetails", ExtentColor.CYAN);
        pNode.info(m);

        try {

            Map<String, String> auditDetailsFromHeaderTable = MobiquityDBAssertionQueries.getAuditTrailDetailsFromMtxTxnHeader(txnID);

            String actualTransactionID, partyID, partyAccessID, serviceType, transactionSubtype,
                    account1, account2, payerAccount, payeeAccount, transactionStatus, payerPaymentInstrument,
                    payeePaymentInstrument, payerWalletType, payeeWalletType,
                    payerProvider, payeeProvider;

            payerAccount = MobiquityGUIQueries.getWalletNumber(auditDetailsFromHeaderTable.get("PAYER_USER_ID"));
            payeeAccount = MobiquityGUIQueries.getWalletNumber(auditDetailsFromHeaderTable.get("PAYEE_USER_ID"));

            while (true)
                try {
                    driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[1])[1]")).isDisplayed();
                    break;
                } catch (Exception e) {
                    driver.findElement(By.xpath("//a[text() = 'Next']")).click();
                }

            driver.findElement(By.xpath("//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[1]")).isDisplayed();
            actualTransactionID = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[1])[1]")).getText();
            partyID = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[3])[1]")).getText();
            partyAccessID = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[4])[1]")).getText();
            serviceType = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[5])[1]")).getText();
            account1 = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[6])[1]")).getText();
            account2 = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[7])[1]")).getText();

            transactionStatus = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[8])[1]")).getText();

            payerProvider = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[11])[1]")).getText();
            payerPaymentInstrument = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[12])[1]")).getText();
            payerWalletType = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[13])[1]")).getText();

            payeeProvider = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[15])[1]")).getText();
            payeePaymentInstrument = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[16])[1]")).getText();
            payeeWalletType = driver.findElement(By.xpath("(//tr/td[contains(text(),'" + txnID + "')]/ancestor::tr[1]/td[17])[1]")).getText();


            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String walletName = DataFactory.getDefaultWallet().WalletName;

            Assertion.verifyEqual(actualTransactionID, txnID, "Verify Transaction ID.", pNode);
            Assertion.verifyEqual(partyID, auditDetailsFromHeaderTable.get("PAYER_USER_ID"), "Verify User ID.", pNode);
            Assertion.verifyEqual(partyAccessID, MSISDN, "Verify User Access ID.", pNode);

            serviceType = serviceType.replaceAll("\\s+", "").toUpperCase();

            Assertion.verifyEqual(serviceType, auditDetailsFromHeaderTable.get("SERVICE_TYPE").toUpperCase(), "Verify Service type.", pNode);


            String dbTxnStatus = auditDetailsFromHeaderTable.get("TRANSFER_STATUS");
            if (dbTxnStatus.trim().equalsIgnoreCase(Constants.TXN_STATUS_SUCCESS)) {
                dbTxnStatus = Constants.TXN_SUCCESS_STRING;
            } else {
                dbTxnStatus = Constants.TXN_FAIL_STRING;
            }
            //TODO [rana: Need to re verify this: 23/09]
            //Assertion.verifyEqual(transactionStatus, dbTxnStatus, "Verify Transaction Status.", pNode);

            Assertion.verifyEqual(account1, payerAccount, "Verify Payer Account Number.", pNode);
            Assertion.verifyEqual(account2, payeeAccount, "Verify Payee Account Number.", pNode);

            //Verify Payment Instrument
            Assertion.verifyEqual(payerPaymentInstrument, auditDetailsFromHeaderTable.get("PAYER_PAYMENT_METHOD_TYPE"), "Verify Payer Payment Instrument.", pNode);
            Assertion.verifyEqual(payeePaymentInstrument, auditDetailsFromHeaderTable.get("PAYEE_PAYMENT_METHOD_TYPE"), "Verify Payee Payment Instrument.", pNode);

            //Verify Wallet.
            Assertion.verifyEqual(payerWalletType, walletName, "Verify Payer Wallet", pNode);
            Assertion.verifyEqual(payeeWalletType, walletName, "Verify Payee Wallet.", pNode);

            //Verify Provider.
            Assertion.verifyEqual(payerProvider, providerName, "Verify Payer Provider", pNode);
            Assertion.verifyEqual(payeeProvider, providerName, "Verify Payee Provider.", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public AuditAdminTrail negativeTestWithoutConfirm() {
        Markup m = MarkupHelper.createLabel("Confirmation flag for Negative Tests", ExtentColor.BLUE);
        // Set the confirm flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        ConfigInput.isConfirm = false;

        return this;
    }

}

