package framework.features.loyaltyManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.LMSProfile;
import framework.pageObjects.Promotion.*;
import framework.util.common.Assertion;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.testng.Assert;

import java.io.IOException;
import java.text.MessageFormat;

public class LMS {

    private static AddPromotion_page1 addPromotion;
    private static AddPromotion_page2 addPromotion2;
    private static AddPromotion_page3 addPromotion3;
    private static AddPromotion_page4 addPromotion4;

    private static ApproveLMS_page1 approveLMS;
    private static ApproveLMS_page2 approveLMS2;

    private static ModifyLMS_page1 ModifyLMS;
    private static ModifyLMS_page2 ModifyLMS2;
    private static ModifyLMS_page3 ModifyLMS3;
    private static ModifyLMS_page4 ModifyLMS4;

    private static DeleteLMS_page deleteLMS;
    private static ViewPromotion_page ViewPromotion;
    private static ExtentTest pNode;

    private String dateFormat = "dd-MM-yyyy";
    private String time = " 02:30:00 PM";
    private String fromDate = new DateAndTime().getDate(dateFormat, 3) + time;
    private String toDate = new DateAndTime().getDate(dateFormat, 21) + time;


    public static LMS init(ExtentTest t1) throws IOException {
        pNode = t1;

        try{
            if(addPromotion == null){
                addPromotion = new AddPromotion_page1(pNode);
                addPromotion2 = new AddPromotion_page2(pNode);
                addPromotion3 = new AddPromotion_page3(pNode);
                addPromotion4 = new AddPromotion_page4(pNode);
                ViewPromotion = new ViewPromotion_page(pNode);

                deleteLMS = new DeleteLMS_page(pNode);

                ModifyLMS = new ModifyLMS_page1(pNode);
                ModifyLMS2 = new ModifyLMS_page2(pNode);
                ModifyLMS3 = new ModifyLMS_page3(pNode);
                ModifyLMS4 = new ModifyLMS_page4(pNode);
                approveLMS = new ApproveLMS_page1(pNode);
                approveLMS2 = new ApproveLMS_page2(pNode);
            }

        }catch (Exception e){
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return new LMS();
    }


    public boolean addLMSpromotion(String profilename, String from, String to, String servicetype) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("addLMSpromotion", ExtentColor.BLUE);
            pNode.info(m);

            addPromotion.NavigateNA();
            addPromotion.sendname(profilename);
            addPromotion.sendFromdate(from);
            addPromotion.sendTodate(to);
            addPromotion.selectServiceType(servicetype);
            addPromotion.clickOnNextButton();

            addPromotion2.selectPayerMfsProvider();
            addPromotion2.selectPayerDomain();
            addPromotion2.selectPayerCat();
            addPromotion2.selectPayerPaymentInstruction();
            addPromotion2.selectPayerWB();
            addPromotion2.selectPayerGrade();

            addPromotion2.selectPayeeMfsProvider();
            addPromotion2.selectPayeeDomain();
            addPromotion2.selectPayeeCat();
            addPromotion2.selectPayeePaymentInstruction();
            addPromotion2.selectPayeeWB();
            addPromotion2.selectPayeeGrade();
            addPromotion2.clickOnNextButton();

            String error = Assertion.getErrorMessage();
            String expected = MessageReader.getMessage("promotion.association.exists.name", null);
            expected = MessageFormat.format(expected, "");
            if (error != null) {
                if (error.contains(expected)) {
                    pNode.skip("Profile is already created");

                } else {
                    pNode.fail("error message is coming " + error);
                }
                pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                Assert.fail();
            }


            if (ConfigInput.isConfirm) {
                addPromotion3.enterdata("Payer", "Loyalty Points");
                addPromotion3.clickonsavebutton();
            }


            if (ConfigInput.isConfirm)
                addPromotion4.clickOnConfrim();
            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.verifyContains(msg, "Promotion Creation Intitiated Successfully with Promotion Name:", "Profile is Created", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
            return false;
        }
        return true;

    }

    public boolean addLMSpromotion(LMSProfile profile) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("addLMSpromotion", ExtentColor.BLUE);
            pNode.info(m);

            addPromotion.NavigateNA();
            addPromotion.sendname(profile.profileName);
            addPromotion.sendFromdate(profile.fromDate);
            addPromotion.sendTodate(profile.toDate);
            addPromotion.selectServiceType(profile.serviceCode);
            addPromotion.clickOnNextButton();

            addPromotion2.selectPayerMfsProvider(profile.payerProvider);
            addPromotion2.selectPayerDomain(profile.payer.DomainName);
            addPromotion2.selectPayerCat(profile.payer.CategoryName);
            addPromotion2.selectPayerPaymentInstruction(profile.payerPayInst);
            addPromotion2.selectPayerWB(profile.payerPayInstId);
            addPromotion2.selectPayerGrade(profile.payer.GradeName);

            addPromotion2.selectPayeeMfsProvider(profile.payeeProvider);
            addPromotion2.selectPayeeDomain(profile.payee.DomainName);
            addPromotion2.selectPayeeCat(profile.payee.CategoryName);
            addPromotion2.selectPayeePaymentInstruction(profile.payeePayInst);
            addPromotion2.selectPayeeWB(profile.payeePayInstId);
            addPromotion2.selectPayeeGrade(profile.payee.GradeName);
            addPromotion2.clickOnNextButton();

            if (Assertion.isErrorInPage(pNode)) {
                if (Assertion.verifyErrorMessageContain("promotion.association.exists.name",
                        "Promotion with profile name : "+profile.profileName+ " Already Exist", pNode)) {
                    String profileName = Assertion.getErrorMessage().split("Promotion Name :")[1].trim().split(" ")[0];
                    profile.setProfileName(profileName);
                    return true;
                }else{
                    Utils.captureScreen(pNode);
                    Assertion.markAsFailure("Error while adding LMS profile");
                    return false;
                }
            }

            if (ConfigInput.isConfirm){
                addPromotion3.enterdata("Payer", "Loyalty Points");
                addPromotion3.clickonsavebutton();
                addPromotion4.clickOnConfrim();
                if (ConfigInput.isAssert) {
                    String tid = Utils.extractTxnIdFromMessage(Assertion.getActionMessage(),"Id :");
                    if(Assertion.verifyActionMessageContain("promotion.addinitiate.message.success",
                            "Verify that LMS profile is successfully created", pNode, profile.profileName,tid)){
                        return true;
                    }else{
                        return  false;
                    }
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return false;

    }
    public LMS createPromotion(String profilename, String payer, String payee, String servicetype) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("createPromotion", ExtentColor.ORANGE);
            pNode.info(m);
            addPromotion.NavigateNA();
            addPromotion.sendname(profilename);
            addPromotion.sendFromdate(fromDate);
            addPromotion.sendTodate(toDate);
            addPromotion.selectServiceType(servicetype);
            addPromotion.clickOnNextButton();

            addPromotion2.selectPayerMfsProvider();
            addPromotion2.selectPayerDomain(payer);
            addPromotion2.selectPayerCat(payer);
            addPromotion2.selectPayerPaymentInstruction();
            addPromotion2.selectPayerWB();
            addPromotion2.selectPayerGrade();

            addPromotion2.selectPayeeMfsProvider();
            addPromotion2.selectPayeeDomain(payee);
            addPromotion2.selectPayeeCat(payee);
            addPromotion2.selectPayeePaymentInstruction();
            addPromotion2.selectPayeeWB();
            addPromotion2.selectPayeeGrade();
            addPromotion2.clickOnNextButton();

            String error = Assertion.getErrorMessage();
            String expected = MessageReader.getMessage("promotion.association.exists.name", null);
            expected = MessageFormat.format(expected, "");
            if (error != null) {
                if (error.contains(expected)) {
                    pNode.skip("Profile is already created");
                } else {
                    pNode.fail("!!! Error message is : " + error);
                }
                pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                Assert.fail();
            }


            if (ConfigInput.isConfirm) {
                addPromotion3.enterdata("Payer", "Loyalty Points");
                addPromotion3.clickonsavebutton();
                addPromotion4.clickOnConfrim();
            }


            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.verifyContains(msg, "Promotion Creation Intitiated Successfully with Promotion Name:", "Profile is Created", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void LMSchannelUserENQ(String value,String userType) throws Exception {

        LMS_ENQpage1.init(pNode).
                NavigateToLink().
                selectUserType(userType).
                setMsisdn(value).
                selectProvider().
                clickOnSubmitButton();
    }

    public void LMSchannelSelfUserENQ(String password) throws Exception {
        LMS_ENQpage1.init(pNode).
                NavigateToSelfBalanceEnquiryLink().
                selectSelfProvider().
                clickOnSelfSubmitButton().
                clickOnPassword(password).
                clickOnPasswordSubmitButton();
    }

    public void ViewLMS(String profilename, String from, String to) throws Exception {
        Markup m = MarkupHelper.createLabel("ViewLMS", ExtentColor.ORANGE);
        pNode.info(m);
        ViewPromotion.NavigateNA();
        ViewPromotion.sendFromdate(from);
        ViewPromotion.sendTodate(to);
        ViewPromotion.clickonnextbutton();
        if (ConfigInput.isAssert) {
            Assertion.assertErrorInPage(pNode);
        }
    }

    public void approveLMS(String profileName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("approveLMS", ExtentColor.ORANGE);
            pNode.info(m);
            approveLMS.NavigateNA();
            approveLMS.selectProfile(profileName);
            approveLMS2.clickOnApprove();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyLMS(String profilename) throws Exception {
        try {
            ModifyLMS.NavigateNA();
            ModifyLMS.selectprofile(profilename);
            ModifyLMS2.checkServiceType();
            ModifyLMS2.clickonnext();
            ModifyLMS3.clickonnext();
            ModifyLMS4.clickondelete();
            ModifyLMS4.clickonsave();
            ModifyLMS4.clickmodify();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.verifyActionMessageContain( "promotion.modify.initiate.success", "Verify Profile Modification", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void suspendLMSProfile(String profilename) throws Exception {
        try {
            SuspendLMS_page suspendLMS = SuspendLMS_page.init(pNode);
            suspendLMS.NavigateNA();
            suspendLMS.selectprofile(profilename);
            suspendLMS.clickonConfirm();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void resumeLMSProfile(String profilename) throws Exception {
        try {
            ResumeLMS_Page resumeLMS = new ResumeLMS_Page(pNode);
            resumeLMS.navigateToResumePromotion();
            resumeLMS.selectprofile(profilename);
            resumeLMS.clickonConfirm();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    public void deleteLMSProfile(String profileName) throws Exception {
        try {
            deleteLMS.NavigateNA();
            deleteLMS.selectprofile(profileName);
            deleteLMS.clickonConfirm();

            if(ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("promotion.delete.initiate.success", "Verify Profile Deletion", pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

}
