package framework.features.transactionManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.UsrBalance;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.pageObjects.InverseC2C.InverseC2C_page1;
import framework.pageObjects.InverseC2C.InverseC2C_page2;
import framework.pageObjects.c2c.ChannelToChannelTransfer_page1;
import framework.pageObjects.c2c.ChannelToChannelTransfer_page2;
import framework.pageObjects.cashInCashOut.CashIn_page1;
import framework.pageObjects.cashInCashOut.CashOut_page1;
import framework.pageObjects.ownerToChannel.O2CApproval1_Page1;
import framework.pageObjects.ownerToChannel.O2CApproval2_Page1;
import framework.pageObjects.ownerToChannel.O2CInitiation_Page1;
import framework.pageObjects.svaToOwnBankTransfer.View_Mod_Schedule_Transfer_Page1;
import framework.pageObjects.svaToOwnBankTransfer.svaToOwnBankTransfer_Page1;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.math.BigDecimal;


/**
 * Created by dalia.debnath on 6/2/2017.
 */
public class TransactionManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static OperatorUser o2cInitiator, o2cApprover1, o2cApprover2;

    public static TransactionManagement init(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        pNode = t1;

        try {
            if (o2cInitiator == null) {
                o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
                o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
                o2cApprover2 = DataFactory.getOperatorUserWithAccess("O2C_APP2");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new TransactionManagement();
    }


    /******************************************************************************************************************
     *  __   __   __
     * |  |  __| |
     * |__| |___ |__  (O W N E R   T O   C H A N N E L)
     *
     *****************************************************************************************************************/

    /**
     * Make Sure that the Channel User has balance more than the threshold amount
     *
     * @param user
     * @param amount
     * @return
     * @throws Exception
     */
    public TransactionManagement makeSureChannelUserHasBalance(User user, BigDecimal... amount) throws Exception {
        Markup m = MarkupHelper.createLabel("makeSureChannelUserHasBalance: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        BigDecimal thresholdAmt = (amount.length > 0) ? amount[0] : new BigDecimal(Constants.MAX_O2C_AMOUNT);

        for (String provider : DataFactory.getAllProviderNames()) {
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(user, null, provider);
            if (balance == null) {
                continue;
            }
            pNode.info("Current Balance:" + balance.Balance);

            if (balance.Balance.compareTo(thresholdAmt) < 0) {
               /* ServiceCharge sCharge = new ServiceCharge(Services.O2C, new OperatorUser(Constants.NETWORK_ADMIN), user, null, null, null, null);

                ServiceChargeManagement.init(pNode)
                        .configureServiceCharge(sCharge);*/

                String providerId = DataFactory.getProviderId(provider);
                String serviceId = Transactions.init(pNode)
                        .initiateO2C(o2cInitiator, user, Constants.MAX_O2C_AMOUNT, DataFactory.getDefaultWallet().WalletId, providerId)
                        .ServiceRequestId;

                Transactions.init(pNode).approveO2C(o2cApprover2, serviceId);

//                initiateAndApproveO2CWithProvider(user, provider, thresholdAmt.toString());
            } else {
                pNode.info(user.LoginId + "Has Available Balance - " + balance.Balance + " with provider - " + provider);
            }
        }

        return this;
    }

    public Boolean checkLeafUserHasBalance(User subscriber, BigDecimal... amount) {

        try {
            BigDecimal thresholdAmt = (amount.length > 0) ? amount[0] : new BigDecimal(Constants.MAX_CASHIN_AMOUNT);
            BigDecimal cashInAmount = thresholdAmt.add(new BigDecimal(Constants.CASHIN_BUFFER).add(new BigDecimal(DataFactory.getRandomNumber(2))));

            UsrBalance balance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);


            if (balance.Balance.compareTo(cashInAmount) <= 0) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * Make sure that Channel user has sufficient Balance
     *
     * @param user
     * @param walletId
     * @param amount
     * @return
     * @throws Exception
     */
    public TransactionManagement makeSureChannelUserHasBalanceInWallet(User user, String walletId, BigDecimal... amount) throws Exception {
        Markup m = MarkupHelper.createLabel("makeSureChannelUserHasBalanceInWallet: " + user.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        BigDecimal thresholdAmt = (amount.length > 0) ? amount[0] : new BigDecimal(Constants.MAX_CASHIN_AMOUNT);
        String walletName = DataFactory.getWalletName(walletId);

        for (String provider : DataFactory.getAllProviderNames()) {
            String providerId = DataFactory.getProviderId(provider);
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(user, walletId, provider);
            if (balance == null) {
                continue;
            }
            pNode.info("Current Balance:" + balance.Balance + ". WalletId: " + walletId);

            if (balance.Balance.compareTo(thresholdAmt) < 0) {
                initiateAndApproveO2CWithWallet(user, walletName, providerId, Constants.MAX_O2C_AMOUNT);
            } else {
                pNode.info(user.LoginId + "Has Available Balance - " + balance.Balance + " with provider - " + provider + " and Wallet: " + walletName);
            }
        }

        return this;
    }

    /**
     * Make Sure that Subscriber has enough balance
     *
     * @param subscriber
     * @param amount     - optional
     * @return
     * @throws Exception
     */
    public TransactionManagement makeSureLeafUserHasBalance(User subscriber, BigDecimal... amount) throws Exception {
        Markup m = MarkupHelper.createLabel("makeSureLeafUserHasBalance: " + subscriber.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BigDecimal thresholdAmt = (amount.length > 0) ? amount[0] : new BigDecimal(Constants.MAX_CASHIN_AMOUNT);
            BigDecimal cashInAmount = thresholdAmt.add(new BigDecimal(Constants.CASHIN_BUFFER).add(new BigDecimal(DataFactory.getRandomNumber(2))));

            UsrBalance balance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);


            if (balance.Balance.compareTo(thresholdAmt) <= 0) {
                User wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

                // Make sure the Channel User also has enough Balance
                makeSureChannelUserHasBalance(wholeSaler, cashInAmount);

                // Make sure that Cash In Transfer Rule is Configured
                ServiceCharge tRuleCashIn = new ServiceCharge(Services.CASHIN, wholeSaler, subscriber, null, null, null, null);

                // Configure Transfer Rule
                TransferRuleManagement.init(pNode).configureTransferRule(tRuleCashIn);

                Transactions.init(pNode)
                        .initiateCashIn(subscriber, wholeSaler, cashInAmount);
            } else {
                pNode.info(subscriber.LoginId + "Has Available Balance : " + balance.Balance);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Make Sure that Subscriber has enough balance, perform CASH IN via GUI
     *
     * @param subscriber
     * @param amount     - optional
     * @return
     * @throws Exception
     */
    public TransactionManagement makeSureLeafUserHasBalanceUI(User subscriber, BigDecimal... amount) throws Exception {
        Markup m = MarkupHelper.createLabel("makeSureLeafUserHasBalance: " + subscriber.LoginId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            BigDecimal thresholdAmt = (amount.length > 0) ? amount[0] : new BigDecimal(Constants.MAX_CASHIN_AMOUNT);
            BigDecimal cashInAmount = thresholdAmt.add(new BigDecimal(Constants.CASHIN_BUFFER).add(new BigDecimal(DataFactory.getRandomNumber(2))));

            UsrBalance balance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);


            if (balance.Balance.compareTo(thresholdAmt) <= 0) {
                User wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);

                // Make sure the Channel User also has enough Balance
                makeSureChannelUserHasBalance(wholeSaler, cashInAmount);

                // Make sure that Cash In Transfer Rule is Configured
                ServiceCharge tRuleCashIn = new ServiceCharge(Services.CASHIN, wholeSaler, subscriber, null, null, null, null);

                // Configure Transfer Rule
                TransferRuleManagement.init(pNode).configureTransferRule(tRuleCashIn);

                // perform Cash in via UI
                Login.init(pNode).login(wholeSaler);
                performCashIn(subscriber, cashInAmount.toString(), null);
            } else {
                pNode.info(subscriber.LoginId + "Has Available Balance : " + balance.Balance);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }

    /**
     * Initiate and approve Operator to Channel
     *
     * @param user
     * @return
     * @throws Exception
     */
    public TransactionManagement initiateAndApproveO2C(User user, String amount, String remarks, String... refNo) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("initiateAndApproveO2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cInitiator);
            String reference = refNo.length > 0 ? refNo[0] : "" + DataFactory.getRandomNumber(5);

            String txnId = initiateO2C(user, amount, remarks, reference);

            if (txnId != null) {
                o2cApproval1(txnId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate O2C
     *
     * @param user
     * @return
     * @throws Exception
     */
    public String initiateO2C(User user, String amount, String remarks, String... refNo) throws Exception {
        String txnId = null;

        String referenceNum = (refNo.length > 0) ? refNo[0] : DataFactory.getRandomNumberAsString(5);
        try {
            Markup m = MarkupHelper.createLabel("initiateO2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(pNode);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(user.MSISDN);
            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderId);
            page1.selectWallet(DataFactory.getDefaultWallet().WalletName);
            if (!AppConfig.isLinkStockCreateWithO2C) {
                page1.setO2CAmount(amount);
            }
            page1.setRefNumber(referenceNum);
            page1.selectPaymentType("Cash");
            page1.setRemarks(remarks);
            page1.clickSubmit();
            //page1.verifyServiceChargeAndCommision(amount);
            if (ConfigInput.isConfirm) {
                Thread.sleep(Constants.MAX_WAIT_TIME);
                page1.clickConfirm();

                if (ConfigInput.isAssert) {
                    String msg = Assertion.getActionMessage();
                    txnId = msg.split("ID:")[1].trim();
                    Assertion.verifyMessageContain(msg, "o2c.initiate.success", "Initiate O2C", pNode, txnId);

                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_INITIATED, "Transaction Initiation Status Verified successfully.", pNode);
                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnId).toString(), amount,
                            "Transaction Amount Verified Successfully", pNode);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * Initiate and approve Operator to Channel
     *
     * @param user
     * @param provider - Provider Name
     * @return
     * @throws Exception
     */
    public TransactionManagement initiateAndApproveO2CWithProvider(User user, String provider, String amount, String... refId) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("initiateAndApproveO2CWithProvider", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.resetLoginStatus();
            Login.init(pNode).login(o2cInitiator);

            String txnId = initiateO2CWithProvider(user, provider, amount, refId);

            if (txnId != null) {
                o2cApproval1(txnId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate and Approve O2C to a specific wallet for the User
     *
     * @param user
     * @param walletName
     * @param providerId
     * @param amount
     * @return
     * @throws Exception
     */
    public TransactionManagement initiateAndApproveO2CWithWallet(User user, String walletName, String providerId, String amount) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("initiateAndApproveO2CWithWallet", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.resetLoginStatus();
            Login.init(pNode).login(o2cInitiator);

            String txnId = initiateO2CForSpecificWallet(user, walletName, providerId, amount, null);

            if (txnId != null) {
                o2cApproval1(txnId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param user
     * @param amount
     * @param provider [string] provider Name
     * @return
     * @throws Exception
     */
    public String initiateO2CWithProvider(User user, String provider, String amount, String... refId) throws Exception {
        String txnId = null;

        try {

            String referenceId = (refId.length > 0) ? refId[0] : DataFactory.getRandomNumberAsString(5);

            Markup m = MarkupHelper.createLabel("initiateO2CWithProvider", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cInitiator);

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(pNode);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(user.MSISDN);
            page1.selectProviderName(DataFactory.getProviderId(provider));
            page1.selectWallet(DataFactory.getDefaultWallet().WalletName);

            if (!AppConfig.isLinkStockCreateWithO2C) {
                page1.setO2CAmount(amount);
            }

            page1.setRefNumber(referenceId);
            page1.selectPaymentType("Cash");
            page1.setRemarks("O2C initiate");
            page1.clickSubmit();
            //page1.verifyServiceChargeAndCommision(amount);
            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
                if (ConfigInput.isAssert) {
                    String msg = Assertion.getActionMessage();
                    txnId = msg.split("ID:")[1].trim();
                    Assertion.verifyMessageContain(msg, "o2c.initiate.success", "Initiate O2C", pNode, txnId);

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * Approve Level 1 O2C request
     *
     * @param txnId
     * @return
     * @throws Exception pass the value of reject only if you want o2c to be rejected else don't pass anything
     */
    public TransactionManagement o2cApproval1(String txnId, boolean... reject) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("o2cApproval1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cApprover1);

            O2CApproval1_Page1 page1 = new O2CApproval1_Page1(pNode);
            page1.navigateToOwner2ChannelApproval1Page();
            page1.selectTransactionId(txnId);
            page1.clickSubmit();
            if (reject.length > 0) {

                page1.clickReject();
                String actualMessage = Assertion.getActionMessage();
                if (actualMessage.contains(MessageReader.getMessage("o2c.approval.reject", null))) {
                    pNode.pass("Successfully Rejected Owner to Channel transaction!");
                } else {
                    pNode.fail("Failed to Reject O2C");
                    Utils.captureScreen(pNode);
                }

            } else {
                page1.clickApprove();
                if (ConfigInput.isAssert) {
                    String actualMessage = Assertion.getActionMessage();
                    if (actualMessage.contains(MessageReader.getMessage("o2c.approval.label.secondLevelNeeded", null))) {
                        Assertion.verifyActionMessageContain("o2c.approval.label.secondLevelNeeded", "O2C Approval 1", pNode);
                        pNode.pass("Successfully Approved Level 1, Level 2 Approval Is Required!");
                        o2cApproval2(txnId);
                    } else if (actualMessage.contains(MessageReader.getMessage("o2c.approval.success", null))) {
                        Assertion.verifyActionMessageContain("o2c.approval.success", "O2C Approval 1", pNode);
                        pNode.pass("Successfully Approved O2C");
                        DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                                Constants.TXN_STATUS_SUCCESS, "Transaction Approval Status Verified successfully.", pNode);

                    } else {
                        pNode.fail("Failed to Approve O2C");
                        Utils.captureScreen(pNode);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * This method will Approve O2C transaction at Level one and will not go
     * to Second Level Approval
     * //TODO Pass Approval Limit
     *
     * @param txnId
     * @param reject
     * @return
     * @throws Exception
     */
    public TransactionManagement approveO2CLevelOne(String txnId, String... remark) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("o2cApproval1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cApprover1);

            O2CApproval1_Page1 page1 = new O2CApproval1_Page1(pNode);
            page1.navigateToOwner2ChannelApproval1Page();
            page1.selectTransactionId(txnId);
            page1.clickSubmit();
            if (remark.length > 0)
                page1.setRemark(remark[0]);
            page1.clickApprove();
            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                if (actualMessage.contains(MessageReader.getMessage("o2c.approval.label.secondLevelNeeded", null))) {
                    Assertion.verifyActionMessageContain("o2c.approval.label.secondLevelNeeded", "O2C Approval 1", pNode);
                    pNode.pass("Successfully Approved Level 1, Level 2 Approval Is Required!");
                } else if (actualMessage.contains(MessageReader.getMessage("o2c.approval.success", null))) {
                    Assertion.verifyActionMessageContain("o2c.approval.success", "O2C Approval 1", pNode);
                    pNode.pass("Successfully Approved O2C");
                    DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                            Constants.TXN_STATUS_SUCCESS, "Transaction Approval Status Verified successfully.", pNode);

                } else {
                    pNode.fail("Failed to Approve O2C");
                    Utils.captureScreen(pNode);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     * Method to do Approve at Level 2 O2C Request
     *
     * @param txnId
     * @throws Exception
     */
    public void o2cApproval2(String txnId, String... remark) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("o2cApproval2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (!o2cApprover2.LoginId.equals(o2cApprover1.LoginId)) {
                Login.init(pNode).login(o2cApprover2);
            }

            O2CApproval2_Page1 page = O2CApproval2_Page1.init(pNode);

            page.navigateToOwner2ChannelApproval2Page();
            page.selectTransactionId(txnId);
            page.clickSubmit();
            if (remark.length > 0)
                page.setRemark(remark[0]);
            page.clickApprove();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("o2c.approval.success", "2nd Level Approve O2C", pNode);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_SUCCESS, "Transaction Approval Status Verified successfully", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /******************************************************************************************************************
     * SVA TO BANK
     *****************************************************************************************************************/
    /**
     * Initiate SVA to Own Bank
     *
     * @return
     * @throws Exception
     */
    public String initiateSVAtoBank(User user, String transferType, String provider, String walletName, String bankName, String amount, String... accNum) throws Exception {
        String transID = null;
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            page1.selectBankName(bankName);
            Thread.sleep(2000);
            if (accNum.length > 0) {
                page1.selectAccountNumberByValue(accNum[0]);
            } else {
                page1.selectAccountByIndex();//TODO - select the particular account number
            }
            //page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();

            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
            }

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                transID = msg.split("with ")[1].trim().split(" ")[0];
                Assertion.verifyActionMessageContain("sva.ambiguous.message", "Initiate SVA to Bank", pNode, transID);

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return transID;
    }


    public String initiateSVAtoBank(Biller biller, String transferType, String provider, String walletName, String bankName, String amount) throws Exception {
        String transID = null;
        try {
            Login.init(pNode).login(biller);

            Markup m = MarkupHelper.createLabel("initiateSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();

            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
            }

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.assertActionMessageContain("sva.success.transfernow.message", "Initiate SVA to Bank", pNode);
                transID = msg.split(":")[1].trim();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return transID;
    }


    /***
     *
     * @param user
     * @param transferType
     * @return
     * @throws Exception
     */
    public String initiateSVAtoBankTransfer(User user, String transferType, String transferAmount) throws Exception {
        String txnId = null;
        try {

            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiate SVA to Bank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);

            page1.navigateToSVA2OwnBankTransferInWeb();

            if (transferType.equals(Constants.SVA_TRANSFER_NOW))
                page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderName);
            page1.selectWallet(DataFactory.getDefaultWallet().WalletName);
            page1.selectBankName(DataFactory.getDefaultBankNameForDefaultProvider());
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickSubmit();
            page1.clickConfirm();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.verifyMessageContain(msg, "sva.success.transfernow.message", "Initiate SVA to Bank", pNode);
                txnId = msg.split("ID:")[1].trim();
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    public TransactionManagement initiateScheduledSVAtoBank(User user, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(Constants.SVA_SCHEDULE_TRANSFER);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.scrollToBottomOfPage();
            Utils.putThreadSleep(1000);

            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            if (ConfigInput.isConfirm) {
                Utils.scrollToBottomOfPage();
                page1.clickConfirmSchedule();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("success.scheduledTransfer.add", "Initiate SVA to Bank", pNode);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement viewSVAtoBank(User user) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("ViewSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            View_Mod_Schedule_Transfer_Page1 page1 = View_Mod_Schedule_Transfer_Page1.init(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToViewModStopDelSVA();
            page1.selectTransferRadio();
            page1.clickView();
            page1.fetchAcNo();
            page1.fetchAmount();
            page1.fetchCreationDate();
            page1.fetchNextPaymentDate();
            page1.fetchNoOfOccurance();
            page1.fetchNumberOfPaymentLeft();
            page1.fetchWalletBalance();
            page1.fetchRequestNo();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement ModifySVAtoBank(User user, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("ViewSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            View_Mod_Schedule_Transfer_Page1 page1 = View_Mod_Schedule_Transfer_Page1.init(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToViewModStopDelSVA();
            page1.selectTransferRadio();
            page1.clickModify();
            page1.fetchAcNo();
            page1.fetchAmount();
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance("19");
            page1.setRemarks(" ");
            page1.setRemarks("Automation22");
            page1.clickModModify();
            page1.clickConfirm();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_Cancel_Prompt_2(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount("123");
            page1.setRemarks("Automation22");
            page1.clickSubmit();
            page1.clickCancel();

            String message = driver.switchTo().alert().getText();
            driver.switchTo().alert().accept();
            String expected = MessageReader.getMessage("confirmation.scheduledTransfer.cancel", null);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVAtoBankWalletBalance(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.WalletBalanceDisabled();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_Total_Balance(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.TotalBalance();
            page1.setRemarks("Automation22");
            page1.clickSubmit();
            /*page1.clickCancel();*/

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_Remarks_Check(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.setRemarks("Automation22");

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public TransactionManagement SVA_Confirm_Back(User user, String transferType, String provider, String walletName, String bankName, String amount) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();
            page1.clickConfirmBack();
            page1.checkSubmitDisplayed();


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public TransactionManagement SVAtoBank_Transfer(User user, String transferType, String provider, String walletName, String bankName, String amount) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();
            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
            }

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.verifyMessageContain(msg, "sva.success.transfernow.message", "Initiate SVA to Bank", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_BlankAC(User user, String transferType, String provider, String walletName, String bankName, String amount) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number

            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public TransactionManagement SVA_Cancel_Prompt_1(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();


            page1.clickCancel();

            String message = driver.switchTo().alert().getText();
            driver.switchTo().alert().accept();
            String excpected = MessageReader.getMessage("confirmation.scheduledTransfer.cancel", null);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_NextCheck(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();
            page1.SVAtoBankTT_page();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransactionManagement SVA_Schedule_Total_Balance(User user, String transferType, String provider, String walletName, String bankName, String day) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.TotalBalance();
            page1.setOccurance("2");
            page1.setRemarks("Automation22");
            page1.clickSubmitSchedule();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public TransactionManagement SVA_Schedule_BlankAC(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            /*page1.selectAccountByIndex();*/
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            if (ConfigInput.isConfirm) {
                page1.clickConfirmSchedule();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("success.scheduledTransfer.add", "Initiate SVA to Bank", pNode);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }

    public TransactionManagement SVA_Schedule_TotalBalance(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.TotalBalance();
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            /*
             * */
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }


    public TransactionManagement SVA_Schedule_Daily(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickDaily();
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            if (ConfigInput.isConfirm) {
                page1.clickConfirmSchedule();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("success.scheduledTransfer.add", "Initiate SVA to Bank", pNode);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }


    public TransactionManagement SVA_Schedule_Monthly(User user, String transferType, String provider, String walletName, String bankName, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickMonthly();
            page1.monthlyDayDdrop("1");
            page1.monthlyMonthDdrop("1");
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            if (ConfigInput.isConfirm) {
                page1.clickConfirmSchedule();
            }

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("success.scheduledTransfer.add", "Initiate SVA to Bank", pNode);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }


    public TransactionManagement SVA_Schedule_Yearly(User user, String transferType, String provider, String walletName, String bankName, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickYearly();
            page1.yearlyMonthDdrop("January");
            page1.yearlyDayDdrop("2");
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);

            if (ConfigInput.isConfirm) {
                page1.clickConfirmSchedule();
            }

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("success.scheduledTransfer.add", "Initiate SVA to Bank", pNode);

            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }


    public TransactionManagement SVA_Schedule_Delete(User user) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("DeleteScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            View_Mod_Schedule_Transfer_Page1 page1 = new View_Mod_Schedule_Transfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToViewModStopDelSVA();
            page1.clickDelete();
            page1.clickDeleteP2();

            String message = driver.switchTo().alert().getText();
            driver.switchTo().alert().accept();
            String excpected = MessageReader.getMessage("confirmation.scheduledTransfer.delete", null);

            if (ConfigInput.isAssert) {
                Assertion.assertActionMessageContain("success.scheduledTransfer.delete", "Deleted Scheduled SVA to Bank", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }


    public TransactionManagement SVA_Schedule_Cancel(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            Utils.putThreadSleep(1000);
            page1.clickCancel();
            String message = driver.switchTo().alert().getText();
            driver.switchTo().alert().accept();
            String excpected = MessageReader.getMessage("confirmation.scheduledTransfer.cancel", null);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }

    public TransactionManagement SVA_Schedule_CheckCalvisi(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();
            page1.ClickEndDate();
            page1.CheckCalenderVisiablity();


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }

    public TransactionManagement SVA_Schedule_Confirm_Back(User user, String transferType, String provider, String walletName, String bankName, String day, String transferAmount, String Occurance) throws Exception {
        try {
            Login.init(pNode).login(user);

            Markup m = MarkupHelper.createLabel("initiateScheduledSVAtoBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            //page1.selectBankName(bankName);
            //page1.selectAccountNumber("1"); //TODO - select the particular account number
            page1.selectAccountByIndex();
            page1.selectAmountType();
            page1.setTransferAmount(transferAmount);
            page1.clickWeekly();
            page1.weeklyDayDdrop(day);
            page1.setOccurance(Occurance);
            page1.setRemarks("Automation");
            Utils.putThreadSleep(1000);
            page1.clickSubmitSchedule();
            page1.ClickScheduleConfirmBack();
            page1.CheckSubmitSchedule();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;


    }

    /******************************************************************************************************************
     *  __  __   __
     * |    __| |
     * |__ |___ |__
     *
     *****************************************************************************************************************/

    public String performC2C(User usr, String amount, String paymentID) throws IOException {

        String txnID = null;

        try {
            Markup m = MarkupHelper.createLabel("performC2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ChannelToChannelTransfer_page1 page1 = ChannelToChannelTransfer_page1.init(pNode);
            ChannelToChannelTransfer_page2 page2 = ChannelToChannelTransfer_page2.init(pNode);

            page1.navigateToLink();
            page1.channelUserProviderSelectValue(DataFactory.getDefaultProvider().ProviderId);
            page1.channelUserWalletSelectValue(DataFactory.getDefaultWallet().WalletId);
            page1.domainNameSelectValue(usr.DomainCode);
            page1.payeeCategorySelectValue(usr.CategoryCode);
            page1.msisdnSetText(usr.MSISDN);
            driver.findElement(By.xpath("//img[@alt = 'SomtelLogo']")).click();
            page1.payeeProviderSelectValue(DataFactory.getDefaultProvider().ProviderId);
            page1.payeeWalletTypeSelectValue(DataFactory.getDefaultWallet().WalletId);
            page1.clickSubmitButton();

            page2.amountSetText(amount);
            page2.paymentIDSetText(paymentID);
            page2.clickSubmitButton();
            if (ConfigInput.isConfirm) {
                page2.clickConfirmButton();
            }


            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                Assertion.assertMessageContain(msg, "retailer.label.transfer.success", "C2C Transfer", pNode);
                txnID = msg.split(":")[1].trim();
                pNode.info("Transaction ID :" + txnID);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID),
                        Constants.TXN_STATUS_SUCCESS, "Transaction Success Status Verified successfully.", pNode);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnID).toString(), amount,
                        "Transaction Amount Verified Successfully", pNode);

            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return txnID;

    }

    public TransactionManagement clickBack() throws Exception {
        ChannelToChannelTransfer_page2 page2 = new ChannelToChannelTransfer_page2(pNode);
        page2.clickBackButton();
        return this;
    }

    public void negativeMSISDN(String msisdn) throws Exception {
        ChannelToChannelTransfer_page1 page1 = new ChannelToChannelTransfer_page1(pNode);
        page1.navigateToLink();
        page1.msisdnSetText(msisdn);
        page1.clickSubmitButton();
    }

    /***
     *    ____   _    ____  _   _ ___ _   _
     *  / ___|  / \  / ___|| | | |_ _| \ | |
     *  | |    / _ \ \___ \| |_| || ||  \| |
     *  | |___/ ___ \ ___) |  _  || || |\  |
     *  \____/_/   \_\____/|_| |_|___|_| \_|
     *
     */
    public String performCashIn(User usr, String amount, String provider) throws IOException, InterruptedException {
        String txnID = null;
        Markup m = MarkupHelper.createLabel("performCashIn", ExtentColor.BLUE);
        pNode.info(m);
        try {

            WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 5);

            if (provider == null) {
                provider = DataFactory.getDefaultProvider().ProviderName;
            }

            CashIn_page1 cashIn_page1 = CashIn_page1.init(pNode);

            cashIn_page1.navigateToLink();
            cashIn_page1.channelUserProviderSelectText(provider);
            try {
                wait.until(ExpectedConditions.alertIsPresent()).accept();
            } catch (Exception e) {
                ;
            }

            cashIn_page1.channelUserWalletSelectText(DataFactory.getDefaultWallet().WalletName);

            cashIn_page1.subscriberMSISDNSetText(usr.MSISDN);
            Thread.sleep(Constants.TWO_SECONDS);
            if (DataFactory.getDefaultProvider().ProviderName != null)
                cashIn_page1.selectPartyProviderName(provider);

            if (DataFactory.getDefaultWallet().WalletName != null)
                cashIn_page1.subscriberWalletSelectText(DataFactory.getDefaultWallet().WalletName);

            cashIn_page1.amountSetText(amount);

            if (AppConfig.isIdentificationNumRequired) {
                cashIn_page1.identificationNumberSetText(usr.ExternalCode);
            }

            cashIn_page1.senderMSISDNSetText(usr.MSISDN);
            //cashIn_page1.senderName_SetText(usr.FullName);
            if (ConfigInput.is4o14Release) {
                cashIn_page1.paymentNoSetText("123455");
            }

            cashIn_page1.clickSubmit();
            cashIn_page1.clickConfirmButton();
            Thread.sleep(Constants.TWO_SECONDS);

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = null;

                if (ConfigInput.isCoreRelease) {
                    txnID = actual.split(":")[1].trim();
                    Assertion.verifyActionMessageContain("cashin.success.core", "Cash In Success", pNode, txnID);

                    expected = MessageReader.getMessage("cashin.success.core", null);
                } else {
                    expected = MessageReader.getDynamicMessage("cashin.success", usr.FirstName, usr.LastName);
                }

//                Assertion.verifyContains(actual, expected, "Cash-In ", pNode);
//                txnID = actual.split(":")[1].trim();
//                pNode.info("Transaction ID :" + txnID);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID),
                        Constants.TXN_STATUS_SUCCESS, "Transaction Success Status Verified successfully.", pNode);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnID).toString(), amount,
                        "Transaction Amount Verified Successfully", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }

    public String performCashIn(User usr, String amount) throws IOException, InterruptedException {
        String txnID = null;

        try {
            Markup m = MarkupHelper.createLabel("performCashIn", ExtentColor.BLUE);
            pNode.info(m);

            CashIn_page1 cashIn_page1 = new CashIn_page1(pNode);

            cashIn_page1.navigateToLink();
            cashIn_page1.channelUserProviderSelectText(DataFactory.getDefaultProvider().ProviderName);
            cashIn_page1.channelUserWalletSelectText(DataFactory.getDefaultWallet().WalletName);
            cashIn_page1.subscriberMSISDNSetText(usr.MSISDN);

            if (DataFactory.getDefaultProvider().ProviderName != null)
                cashIn_page1.providerSelectValue(DataFactory.getDefaultProvider().ProviderName);

            if (DataFactory.getDefaultWallet().WalletName != null)
                cashIn_page1.subscriberWalletSelectText(DataFactory.getDefaultWallet().WalletName);

            cashIn_page1.amountSetText(amount);

            if (AppConfig.isIdentificationNumRequired) {
                cashIn_page1.identificationNumberSetText(usr.ExternalCode);
            }

            if (!(ConfigInput.isCoreRelease || ConfigInput.is4o14Release)) {
                cashIn_page1.senderMSISDNSetText(usr.MSISDN);
                //cashIn_page1.senderName_SetText(usr.FullName);
            }

            if (ConfigInput.is4o14Release) {
                cashIn_page1.paymentNoSetText(Constants.paymentID);
            }

            cashIn_page1.clickSubmit();
            cashIn_page1.clickConfirmButton();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                String expected = null;

                if (ConfigInput.isCoreRelease) {
                    txnID = actual.split(":")[1].trim();
                    pNode.info("Transaction ID :" + txnID);

                    expected = MessageReader.getDynamicMessage("cashin.success.core", txnID);
                } else {
                    expected = MessageReader.getDynamicMessage("cashin.success", usr.FirstName, usr.LastName);
                }

                Assertion.verifyContains(actual, expected, "Cash-In ", pNode);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID),
                        Constants.TXN_STATUS_SUCCESS, "Transaction Success Status Verified successfully.", pNode);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnID).toString(), amount,
                        "Transaction Amount Verified Successfully", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }


    /**
     * Perform Cash In Using User's ID number
     *
     * @param usr
     * @param amount
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String performCashInUsingIDnum(User usr, String amount) throws IOException, InterruptedException {
        String txnID = null;
        try {
            Markup m = MarkupHelper.createLabel("performCashInUsingIDnum", ExtentColor.BLUE);
            pNode.info(m);

            CashIn_page1.init(pNode)
                    .navigateToLink()
                    .channelUserProviderSelectText(DataFactory.getDefaultProvider().ProviderName)
                    .channelUserWalletSelectText(DataFactory.getDefaultWallet().WalletName)
                    .subscriberMSISDNSetText(usr.MSISDN)
                    .selectPartyProviderName(DataFactory.getDefaultProvider().ProviderName)
                    .subscriberWalletSelectText(DataFactory.getDefaultWallet().WalletName)
                    .amountSetText(amount)
                    .identificationNumberSetText(usr.ExternalCode)
                    .clickSubmit()
                    .clickConfirmButton();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                txnID = actual.split(":")[1].trim();
                Assertion.verifyActionMessageContain("cashin.success.core", "Verify Cash In using ID Number", pNode, txnID);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }


    /**
     * $$$$$$\   $$$$$$\   $$$$$$\  $$\   $$\  $$$$$$\  $$\   $$\ $$$$$$$$\
     * $$  __$$\ $$  __$$\ $$  __$$\ $$ |  $$ |$$  __$$\ $$ |  $$ |\__$$  __|
     * $$ /  \__|$$ /  $$ |$$ /  \__|$$ |  $$ |$$ /  $$ |$$ |  $$ |   $$ |
     * $$ |      $$$$$$$$ |\$$$$$$\  $$$$$$$$ |$$ |  $$ |$$ |  $$ |   $$ |
     * $$ |      $$  __$$ | \____$$\ $$  __$$ |$$ |  $$ |$$ |  $$ |   $$ |
     * $$ |  $$\ $$ |  $$ |$$\   $$ |$$ |  $$ |$$ |  $$ |$$ |  $$ |   $$ |
     * \$$$$$$  |$$ |  $$ |\$$$$$$  |$$ |  $$ | $$$$$$  |\$$$$$$  |   $$ |
     * \______/ \__|  \__| \______/ \__|  \__| \______/  \______/    \__|
     */


    /**
     * Perform Cash out (Normal Cash-out)
     *
     * @param usr
     * @param amount
     * @param provider
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public String performCashOut(User usr, String amount, String provider) throws IOException {
        String txnID = null;
        if (provider == null) {
            provider = DataFactory.getDefaultProvider().ProviderName;
        }
        System.out.println(provider);
        String wallet = DataFactory.getDefaultWallet().WalletName;
        try {
            Markup m = MarkupHelper.createLabel("performCashOut", ExtentColor.BLUE);
            pNode.info(m);

            CashOut_page1 cashOut_page1 = CashOut_page1.init(pNode);

            cashOut_page1.navigateToLink();
            cashOut_page1.channelUserprovider_SelectText(provider);
            cashOut_page1.channelUserwallet_SelectText(wallet);
            cashOut_page1.subscriberMsisdn_SetText(usr.MSISDN);
            cashOut_page1.provider_SelectText(provider);
            cashOut_page1.subscriberWallet_SelectText(wallet);
            cashOut_page1.amount_SetText(amount);

            Boolean idNumber = (DriverFactory.getDriver().findElements(By.id("cashOutWeb_confirm_idNumber")).size() > 0) ? true : false;
            if (idNumber)
                cashOut_page1.identificationNumber_SetText(usr.ExternalCode);

            cashOut_page1.submit_Click();
            cashOut_page1.confirm_Click();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                txnID = actual.split(":")[1].trim();
                Assertion.verifyActionMessageContain("cashout.init.success", "Cashout Success", pNode, txnID);
                pNode.info("Transaction ID :" + txnID);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID), Constants.TXN_STATUS_INITIATED,
                        "Transaction Status in DB verified successfully.", pNode);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnID).toString(), amount,
                        "Transaction Amount Verified Successfully", pNode);


                return txnID;
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return null;
    }

    public String inverseC2C(User payer, String amount) throws IOException {
        String txnID = null;
        String provider = DataFactory.getDefaultProvider().ProviderName;
        String wallet = DataFactory.getDefaultWallet().WalletName;

        try {
            Markup m = MarkupHelper.createLabel("inverseC2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            InverseC2C_page1 page1 = InverseC2C_page1.init(pNode);
            InverseC2C_page2 page2 = InverseC2C_page2.init(pNode);

            //page 1
            page1.navInversec2c();
            page1.selectDomain(payer.DomainName);
            page1.selectPayerCategory(payer.CategoryName);
            page1.enterPayerMSISDN(payer.MSISDN);
            page1.selectPayerProvider(provider);
            page1.selectPayerWallet(wallet);
            page1.clickSubmit();

            //page 2
            page2.enterAmount(amount);
            page2.enterPaymentId(DataFactory.getRandomNumberAsString(3));
            page2.clickSubmit();
            page2.clickConfirm();

            if (ConfigInput.isAssert) {
                String actual = Assertion.getActionMessage();
                txnID = actual.split(":")[1].trim();
                ;
                Assertion.verifyActionMessageContain("inv.c2c.initiate.success", "inv c2c initiate success", pNode, txnID);

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }


    public TransactionManagement approvePendingCashout(String txnId, boolean isApprove) throws Exception {
        Markup m = MarkupHelper.createLabel("approvePendingCashout", ExtentColor.BLUE);
        pNode.info(m);

        try {
            CashOut_page1.init(pNode)
                    .navigateToTransactionsApproval()
                    .select_ServiceName("Cash Out Confirmation by Agent")
                    .selectTransactionForAction(txnId, isApprove);

            if (ConfigInput.isAssert) {
                if (isApprove)
                    Assertion.verifyActionMessageContain("cashout.approve.success", "Verify Approval is successful", pNode, txnId);
                else {
                    Assertion.verifyActionMessageContain("cashout.reject.success", "Verify Reject is successful", pNode, txnId);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }


    public boolean isTransactionIdAvailable(String txnId) throws Exception {
        Markup m = MarkupHelper.createLabel("isTransactionIdAvailable", ExtentColor.BLUE);
        pNode.info(m);

        try {
            return CashOut_page1.init(pNode)
                    .navigateToTransactionsApproval()
                    .select_ServiceName("Cash Out Confirmation by Agent")
                    .isTransactionAvailable(txnId);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return false;
    }

    /**
     * @param user
     * @param walletName
     * @param amount
     * @param providerId
     * @param refNo
     * @return
     * @throws Exception
     */
    public String initiateO2CForSpecificWallet(User user, String walletName, String providerId, String amount, String refNo) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateO2CForSpecificWallet", ExtentColor.AMBER);
        pNode.info(m);
        String txnId = null;

        String referenceNum = (refNo != null) ? refNo : DataFactory.getRandomNumberAsString(5);
        try {

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(pNode);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(user.MSISDN);
            page1.selectProviderName(providerId);

            // to select the wallet
            page1.selectWallet(walletName);
            page1.setO2CAmount(amount);
            page1.setRefNumber(referenceNum);
            page1.selectPaymentType("Cash");
            page1.setRemarks("O2C initiate");
            page1.clickSubmit();
            if (ConfigInput.isConfirm)
                page1.clickConfirm();

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID:")[1].trim();
                Assertion.verifyActionMessageContain("o2c.initiate.success", "Verify that O2C is successfully initiated", pNode, txnId);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    /**
     * Check Performing cashIn
     *
     * @param usr
     * @param amount
     * @return
     * @throws IOException
     * @throws InterruptedException
     */

    public String CheckCashIn(User usr, String amount) throws IOException, InterruptedException {
        String txnID = null;
        try {
            Markup m = MarkupHelper.createLabel("performCashIn", ExtentColor.BLUE);
            pNode.info(m);

            CashIn_page1 cashIn_page1 = CashIn_page1.init(pNode);

            cashIn_page1.navigateToLink();
            cashIn_page1.channelUserProviderSelectText(DataFactory.getDefaultProvider().ProviderName);
            cashIn_page1.channelUserWalletSelectText(DataFactory.getDefaultWallet().WalletName);
            cashIn_page1.subscriberMSISDNSetText(usr.MSISDN);

            if (DataFactory.getDefaultProvider().ProviderName != null)
                cashIn_page1.selectPartyProviderName(DataFactory.getDefaultProvider().ProviderName);

            if (DataFactory.getDefaultWallet().WalletName != null)
                cashIn_page1.subscriberWalletSelectText(DataFactory.getDefaultWallet().WalletName);

            cashIn_page1.amountSetText(amount);

            if (AppConfig.isIdentificationNumRequired) {
                cashIn_page1.identificationNumberSetText(usr.ExternalCode);
            }

            if (ConfigInput.is4o9Release) {
                cashIn_page1.senderMSISDNSetText(usr.MSISDN);

            }

            cashIn_page1.clickSubmit();
            cashIn_page1.clickConfirmButton();

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }


    /**
     * Initiate and Reject Operator to Channel
     *
     * @param user
     * @return
     * @throws Exception
     */
    public TransactionManagement initiateAndRejectO2C(User user, String provider, String amount, String remarks, String... refNo) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("initiateAndRejectO2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            String reference = refNo.length > 0 ? refNo[0] : "" + DataFactory.getRandomNumber(5);

            String txnId = initiateO2CWithProvider(user, provider, amount, reference);

            if (txnId != null) {
                rejectO2CLevel1(txnId);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Method to Reject O2C
     *
     * @param txnId
     * @return
     * @throws IOException
     */
    public TransactionManagement rejectO2CLevel1(String txnId, String... remark) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("o2cApproval1", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cApprover1);

            O2CApproval1_Page1 page1 = new O2CApproval1_Page1(pNode);
            page1.navigateToOwner2ChannelApproval1Page();
            page1.selectTransactionId(txnId);
            page1.clickSubmit();
            if (remark.length > 0)
                page1.setRemark(remark[0]);
            page1.clickReject();

            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                Assertion.assertMessageContain(actualMessage, "o2c.approval.reject", "Reject O2C", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Method to Reject O2C
     *
     * @param txnId
     * @return
     * @throws IOException
     */
    public TransactionManagement rejectO2CLevel2(String txnId, String... remark) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("o2cApproval2", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            O2CApproval2_Page1 page1 = new O2CApproval2_Page1(pNode);
            page1.navigateToOwner2ChannelApproval2Page();
            page1.selectTransactionId(txnId);
            page1.clickSubmit();
            if (remark.length > 0)
                page1.setRemark(remark[0]);
            page1.clickReject();

            if (ConfigInput.isAssert) {
                String actualMessage = Assertion.getActionMessage();
                Utils.captureScreen(pNode);
                Assertion.verifyMessageContain(actualMessage, "o2c.approval.reject", "Reject O2C", pNode);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_FAIL, "Transaction Failed Status Verified successfully.", pNode);

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void cashInNegative(User usr, String amount, String paymentID) throws IOException, InterruptedException {
        try {
            Markup m = MarkupHelper.createLabel("performCashIn", ExtentColor.BLUE);
            pNode.info(m);

            CashIn_page1 cashIn_page1 = new CashIn_page1(pNode);

            cashIn_page1.navigateToLink();
            cashIn_page1.channelUserProviderSelectText(DataFactory.getDefaultProvider().ProviderName);
            cashIn_page1.channelUserWalletSelectText(DataFactory.getDefaultWallet().WalletName);
            cashIn_page1.subscriberMSISDNSetText(usr.MSISDN);

            if (DataFactory.getDefaultProvider().ProviderName != null)
                cashIn_page1.selectPartyProviderName(DataFactory.getDefaultProvider().ProviderName);

            if (DataFactory.getDefaultWallet().WalletName != null)
                cashIn_page1.subscriberWalletSelectText(DataFactory.getDefaultWallet().WalletName);

            cashIn_page1.amountSetText(amount);


            if (ConfigInput.is4o14Release) {
                cashIn_page1.paymentNoSetText(paymentID);
            } else {
                Boolean idNumber = (driver.findElements(By.id("selectForm_idNumber")).size() > 0) ? true : false;
                if (idNumber)
                    cashIn_page1.identificationNumberSetText(usr.ExternalCode);
                boolean senderMSISDN = (driver.findElements(By.id("requestorMsisdn")).size() > 0) ? true : false;

                if (senderMSISDN)
                    cashIn_page1.senderMSISDNSetText(usr.MSISDN);

                //cashIn_page1.senderName_SetText(usr.FullName);
            }

            cashIn_page1.clickSubmit();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public String initiateO2CforCommissionWallet(User user, String amount, String remarks, String... refNo) throws Exception {
        String txnId = null;
        String referenceNum = (refNo.length > 0) ? refNo[0] : DataFactory.getRandomNumberAsString(5);
        try {
            Markup m = MarkupHelper.createLabel("initiateO2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(pNode);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(user.MSISDN);
            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderId);
            page1.selectWallet(Constants.COMMISSION_WALLET_STRING);
            page1.setO2CAmount(amount); // TODO - get this value from Config
            page1.setRefNumber(referenceNum);
            page1.selectPaymentType("Cash");
            page1.setRemarks(remarks);
            page1.clickSubmit();
            //page1.verifyServiceChargeAndCommision(amount);
            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
            }

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                txnId = msg.split("ID:")[1].trim();
                Assertion.verifyMessageContain(msg, "o2c.initiate.success", "Initiate O2C", pNode, txnId);

                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnId),
                        Constants.TXN_STATUS_INITIATED, "Transaction Initiation Status Verified successfully.", pNode);
                DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(txnId).toString(), amount,
                        "Transaction Amount Verified Successfully", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnId;
    }

    public TransactionManagement performO2CforCommissionWallet(User user, String amount, String remarks, String... refNo) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("initiateAndApproveO2C", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            Login.init(pNode).login(o2cInitiator);
            String reference = refNo.length > 0 ? refNo[0] : "" + DataFactory.getRandomNumber(5);

            String txnId = initiateO2CforCommissionWallet(user, amount, remarks, reference);

            if (txnId != null) {
                o2cApproval1(txnId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


}