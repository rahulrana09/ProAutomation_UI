package framework.features.domainCategoryManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.Domain;
import framework.entity.MFSDomain;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.DomainManagement.AddNew_Domain;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class DomainManagement {
    private static ExtentTest pNode;
    private static OperatorUser naAddDomain;
    private static AddNew_Domain addDomain_pg1;

    public static DomainManagement init(ExtentTest t1) throws IOException {
        try {
            pNode = t1;
            if (naAddDomain == null) {
                addDomain_pg1 = new AddNew_Domain(pNode);
                naAddDomain = DataFactory.getOperatorUserWithAccess("DOM_ADD");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        return new DomainManagement();
    }

    public void addNewDomain(MFSDomain domain) throws IOException {
        try {
            Login.init(pNode).login(naAddDomain);
            addDomain_pg1.navDomainManagementLink();
            addDomain_pg1.setDomainName(domain.domainName);
            addDomain_pg1.setDomainCode(domain.domainCode);
            addDomain_pg1.setNumberOfCategory(domain.numCategory + "");
            addDomain_pg1.submit();
            if (ConfigInput.isAssert) {
                addDomain_pg1.submitf();
                if (Assertion.verifyActionMessageContain("domain.added.successfully",
                        "Successfully added domain", pNode, domain.domainName)) {
                    domain.setIsCreated();
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }



    public DomainManagement addDomain(Domain dom) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("adddomain ", ExtentColor.BLUE);
            pNode.info(m);

            AddNew_Domain page = AddNew_Domain.init(pNode);

            page.navDomainManagementLink();
            page.setDomainName(dom.domainName);
            page.setDomainCode(dom.domainCode);
            page.submit();

            if (ConfigInput.isConfirm) {
                page.submitf();
            }

            if (ConfigInput.isAssert) {

                Assertion.verifyActionMessageContain("Domain.new.Added", "Domain Added Successfully", pNode, dom.domainName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }
}
