package framework.features.accessManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.pageObjects.accessManagement.ResetPassword_Page;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;

import java.io.IOException;

/**
 * Created by navin.pramanik on 9/7/2017.
 */
public class AccessManagement {

    private static ExtentTest pNode;

    public static AccessManagement init(ExtentTest t1) {
        pNode = t1;
        return new AccessManagement();
    }


    public void resetPassword(String uType, String loginID) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("resetPassword", ExtentColor.TEAL);
            pNode.info(m);

            ResetPassword_Page resetPasswordPage = ResetPassword_Page.init(pNode);

            resetPasswordPage.navResetPasswordLink();
            resetPasswordPage.setUserTypeByValue(uType);
            resetPasswordPage.setLoginId(loginID);
            resetPasswordPage.clickSubmit();
            resetPasswordPage.clickReset();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Reset Password", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void resetPassword(User usr, String usrType) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("resetPassword", ExtentColor.TEAL);
            pNode.info(m);

            ResetPassword_Page resetPasswordPage = ResetPassword_Page.init(pNode);

            resetPasswordPage.navResetPasswordLink();
            resetPasswordPage.setUserTypeByValue(usrType);
            resetPasswordPage.setLoginId(usr.LoginId);
            resetPasswordPage.clickSubmit();
            resetPasswordPage.clickReset();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Web Message Verification", pNode, usr.FirstName, usr.LastName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void resetPassword(OperatorUser optUser) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("resetPassword", ExtentColor.TEAL);
            pNode.info(m);

            ResetPassword_Page resetPasswordPage = ResetPassword_Page.init(pNode);

            resetPasswordPage.navResetPasswordLink();
            resetPasswordPage.setUserTypeByValue(Constants.USER_TYPE_OPT_USER);
            resetPasswordPage.setLoginId(optUser.LoginId);
            resetPasswordPage.clickSubmit();
            resetPasswordPage.clickReset();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sendReset.label.pwdResetSend", "Reset Password", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


}
