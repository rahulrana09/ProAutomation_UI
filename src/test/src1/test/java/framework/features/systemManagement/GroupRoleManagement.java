package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.GradeDB;
import framework.entity.*;
import framework.features.common.Login;
import framework.pageObjects.groupRole.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class GroupRoleManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static SuperAdmin saAddGroupRole;
    private static FunctionLibrary fl;

    public static GroupRoleManagement init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;
            driver = DriverFactory.getDriver();
            fl = new FunctionLibrary(driver);

            // get the Users
            if (saAddGroupRole == null) {
                saAddGroupRole = DataFactory.getSuperAdminWithAccess("GRP_ROL");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new GroupRoleManagement();
    }

    /**
     * Add Web group Role
     *
     * @param userRole
     * @throws Exception
     */
    public GroupRoleManagement addWebGroupRole(WebGroupRole userRole, ExtentTest... eTest) throws Exception {

        ExtentTest t1 = (eTest.length > 0) ? eTest[0] : pNode;
        Markup m = MarkupHelper.createLabel("addWebGroupRole: " + userRole.CategoryCode, ExtentColor.BLUE);
        t1.info(m); // Method Start Marker

        try {
            // update the group role object add additional details required for creating the Group Role
            //navigate Page One---
            userRole.setGroupRoleDetails();
            if (!openWebRolePage(userRole.CategoryCode, t1)) {
                t1.info("Web Group Role is Not Available for " + userRole.CategoryCode);
                return this;
            }

            /*
             * Page 2
             * If Web Role Already exists then initiate update
             * else initiate a fresh new Web Group Role
             */
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(t1);
            if (pageTwo.isRoleExists(userRole.RoleName)) {
                t1.info("Web Role Already Exists: " + userRole.RoleName);
                pageTwo.initiateUpdateGroupRole(userRole.RoleName);
                userRole.setIsUpdated(true);
            } else {
                t1.info("Web Role Does Not Exists: " + userRole.RoleName);
                pageTwo.initiateAddGroupRole();
                userRole.setIsUpdated(false);
            }

            String isError = Assertion.expectErrorInPage();
            if (isError != null && Assertion.verifyErrorMessageContain("sysGroupRole.subs.onlyone",
                    "Only one web group role can be created for subscriber", pNode)) {
                pNode.skip("Skipping the case as:-> Only one web group role can be created for subscriber");
                userRole.setWebRoleName(pageTwo.getSubcriberRoleName());
                return this;
            }

            if(isError != null) {
                Utils.captureScreen(pNode);
                pNode.fail("Failed to Create Group Role for Category - " + userRole.CategoryName);
                return this;
            }

            /*
             * Page 3
             * Add Update the form and complete Web Group Role Creation
             * Set the Role Name and code only if it is new group role to be added
             */
            GroupRole_Page4 finalPage = GroupRole_Page4.init(t1);
            if (!userRole.isUpdated) {
                finalPage.setGroupRoleName(userRole.RoleName);
                finalPage.setGroupRoleCode(userRole.RoleName);
            }

            // check the specified roles
            finalPage.selectUserRoles(userRole.ApplicableRoles);

            Utils.scrollToBottomOfPage();
            // Complete action and Assertions
            if (userRole.isUpdated) {
                finalPage.updateGroupRole();
                if (Assertion.verifyActionMessageContain("grouprole.updated", "Update Group Role", t1))
                    userRole.setIsCreated();

            } else {
                finalPage.addGroupRole();
                if (Assertion.verifyActionMessageContain("grouprole.added", "Add Group Role", t1))
                    userRole.setIsCreated();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        return this;
    }

    /**
     * Open Page One after Selecting Domain and Category
     *
     * @return
     * @throws Exception
     */
    public boolean openWebRolePage(String catCode, ExtentTest... eTest) throws Exception {
        ExtentTest t1 = (eTest.length > 0) ? eTest[0] : pNode;
        GroupRole_Page1 pageOne = GroupRole_Page1.init(t1);
        try {
            // Navigate to Add Web Group Role and switch to Content Frame
            pageOne.navAddWebGroupRole();
            /*
             * Page1
             * Select Domain Category
             */
            pageOne.selectDomainName(DataFactory.getDomainName(catCode));
            pageOne.selectCategoryName(DataFactory.getCategoryName(catCode));
            if (pageOne.checkWebGroupRole()) {
                pageOne.submit();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return false;
    }

    /**
     * Duplicate existing WebGroup Role
     *
     * @param role
     * @return
     * @throws IOException TODO - this need to be extended, once duplicate update the roles
     */
    public WebGroupRole duplicateWebRole(WebGroupRole role) throws IOException {
        Markup m = MarkupHelper.createLabel("duplicateWebRole: " + role.RoleName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            GroupRole_Page1 pageOne = GroupRole_Page1.init(pNode);

            // update the group role object add additional details required for creating the Group Role
            role.setGroupRoleDetails();

            // Navigate to Add Web Group Role and switch to Content Frame
            pageOne.navAddWebGroupRole();

            /*
             * Page1
             * Select Domain Category
             */
            pageOne.selectDomainName(role.DomainName);
            pageOne.selectCategoryName(role.CategoryName);
            if (pageOne.checkWebGroupRole()) {
                pageOne.submit();
            } else {
                pNode.warning("Web Group Role is Not Available for " + role.CategoryCode);
                return role;
            }

            /*
             * Page 2
             * Select the Web Role and click on Duplicate
             */
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(pNode);
            if (pageTwo.isRoleExists(role.RoleName)) {
                pageTwo.initiateDuplicateGroupRole(role.RoleName);
                role.setIsDuplicated();
            } else {
                pNode.fail("Web Role Does Not Exists: " + role.RoleName);
            }

            /**
             * Assert that the Same roles are Checked By Default
             */
            //verifyApplicableRole(role.ApplicableRoles); [rahul.rana][commented, due to UI inconsistency][27/03/2018]

            /*
             * Page 3
             * Add Update the form and complete Web Group Role Creation
             * Set the Role Name and code only if it is new group role to be added
             */
            GroupRole_Page4 finalPage = GroupRole_Page4.init(pNode);
            String duplicateRoleName = "d" + role.CategoryCode + DataFactory.getRandomNumber(3);
            role.RoleName = duplicateRoleName;
            finalPage.setDuplicateGroupRoleName(duplicateRoleName);
            finalPage.setDuplicateGroupRoleCode(duplicateRoleName);

            finalPage.addDuplicatedGroupRole();
            Assertion.verifyActionMessageContain("grouprole.added", "Duplicate Web Group Role", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return role;
    }

    /**
     * Delete Web Role
     *
     * @param userRole
     * @throws IOException
     */
    public void deleteWebGroupRoleDetails(WebGroupRole userRole) throws IOException {
        Markup m = MarkupHelper.createLabel("deleteWebGroupRoleDetails: " + userRole.CategoryCode, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        GroupRole_Page2 pageTwo = GroupRole_Page2.init(pNode);

        try {
            if (!openWebRolePage(userRole.CategoryCode)) {
                pNode.info("Web Group Role is Not Available for " + userRole.CategoryCode);
                return;
            }

            //Navigate to second Page and click on view details for specific web group----
            if (pageTwo.selectWebRole(userRole.RoleName)) {
                pageTwo.clickOnDelete();

                if (ConfigInput.isAssert) {
                    Assertion.assertActionMessageContain("grouprole.deleted", "Delete Group Role", pNode);
                }
            } else {
                pNode.info("Role " + userRole.RoleName + "  is not available for deletion");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void viewWebGroupRoleDetails(WebGroupRole userRole) throws IOException {
        Markup m = MarkupHelper.createLabel("viewWebGroupRole: " + userRole.CategoryCode, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        GroupRole_Page2 pageTwo = GroupRole_Page2.init(pNode);
        GroupRoleView_Page1 rolePg4 = new GroupRoleView_Page1(pNode);

        try {
            if (!openWebRolePage(userRole.CategoryCode)) {
                pNode.info("Web Group Role is Not Available for " + userRole.CategoryCode);
                return;
            }

            //Navigate to second Page and click on view details for specific web group----
            pageTwo.selectWebRole(userRole.RoleName);
            pageTwo.clickOnView();
            Assertion.verifyEqual(rolePg4.getGroupRoleCode(), userRole.RoleName, "Web Group Role Code", pNode);
            Assertion.verifyEqual(rolePg4.getGroupRoleName(), userRole.RoleName, "Web Group Role Name", pNode);
            verifyApplicableRole(userRole.ApplicableRoles);
            verifyApplicableRole(userRole.ApplicableRoles);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public ArrayList<String> getExistingApplicableRoles(String catCode, String roleName) throws Exception {
        Markup m = MarkupHelper.createLabel("getExistingApplicableRoles - " + roleName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        ArrayList<String> applicableRoles = new ArrayList<>();
        try {
            openWebRolePage(catCode);

            GroupRole_Page2 pageTwo = GroupRole_Page2.init(pNode);
            pageTwo.selectWebRole(roleName);
            pageTwo.clickOnView();

            List<WebElement> checkBoxes = driver.findElements(By.xpath("//input[@name='check']"));

            for (int i = 0; i < checkBoxes.size(); i++) {
                if (checkBoxes.get(i).isSelected()) {
                    String code = checkBoxes.get(i).getAttribute("value");
                    applicableRoles.add(code);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return applicableRoles;
    }

    /**
     * Add all the Mobile Group Roles - this is part of base Setup
     *
     * @throws Exception
     */
    public void addMobileGroupRoles() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("CREATE_MOBILE_ROLE_TEST_01",
                    "Check for Mobile role Creation options by iterating Over UI");
            /**
             * There is a distinct similarity between the Mobile Roles and Instrument TCP
             * Hence, we are using the TCP object for the respective Mobile role creations
             */
            List<String> arrCategoryNotToInclude = Arrays.asList(
                    Constants.NETWORK_ADMIN,
                    Constants.BANK_ADMIN,
                    Constants.CHANNEL_ADMIN,
                    Constants.CUSTOMER_CARE_EXE,
                    Constants.BULK_PAYER_ADMIN,
                    Constants.BANK_USER,
                    Constants.SUPER_ADMIN
            );

            // get all combinations of TCPs for Category available in ConfigInput
            List<String> rnrCategoryList = DataFactory.getAllCategoriesFromRnr();
            List<String> bankList = DataFactory.getAllBankNamesForMobileRoleCreation();
            // get all the mobile roles from DB, all roles start from the Automation prefix 'Mob'
            List<MobileGroupRole> dbMoileRoles = MobiquityGUIQueries.fetchAvailableMobileGroupRoles();
            List<InstrumentTCP> lInstTcp = new ArrayList<>();

            for (String category : rnrCategoryList) {
                String catCode = DataFactory.getCategoryCode(category);

                // exclude categories, like nwadm, superadmin...OPT
                if (arrCategoryNotToInclude.contains(catCode))
                    continue;

                String domainName = DataFactory.getDomainName(catCode);
                List<GradeDB> grades = DataFactory.getGradesForCategory(catCode);

                if (grades.size() == 0) {
                    continue;
                }

                for (GradeDB grade : grades) {
                    List<InstrumentTCP> tInsTcp = DataFactory.getListOfRequiredInstrumentTCPs(
                            domainName,
                            catCode,
                            grade.GradeName,
                            bankList
                    );
                    lInstTcp.addAll(tInsTcp);
                }
            }

            for (InstrumentTCP instTCP : lInstTcp) {

                String categoryCode = DataFactory.getCategoryCode(instTCP.CategoryName);
                MobileGroupRole tempRole = new MobileGroupRole(categoryCode, instTCP.GradeName);
                tempRole.setProvider(instTCP.CurrencyProvider);
                tempRole.setPayInstrument(instTCP.PaymentInstrument);
                tempRole.setPayInstrumentType(instTCP.InstrumentType);
                tempRole.setMobileProvider(instTCP.CurrencyProvider);
                tempRole.addApplicableService("CHECK_ALL"); // if role need to be created, select all roles

                ExtentTest t2 = pNode.createNode("CREATE_MOBILE_ROLE_" + tempRole.MobileProvider + "_" +
                                tempRole.CategoryName + "_" +
                                tempRole.GradeName + "_" +
                                tempRole.MobilePayInstType,
                        "Create Mobile role for Given Combination");

                // check if role combination already exist in DB
                MobileGroupRole dbMobRole = isRoleCombinationAlreadyExisting(dbMoileRoles,
                        tempRole.CategoryName,
                        tempRole.GradeName,
                        tempRole.MobileProvider,
                        tempRole.MobilePayInstType
                );

                if (dbMobRole == null) {
                    // create the mobile role, as it's not exist in DB
                    addMobileRole(tempRole, t2);
                    tempRole.writeDataToExcel();
                } else {
                    // write the data to AppData
                    t2.pass("Mobile Group Role exist: " + dbMobRole.ProfileName);
                    dbMobRole.writeDataToExcel();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public MobileGroupRole isRoleCombinationAlreadyExisting(List<MobileGroupRole> dbList, String catName,
                                                            String gradeName,
                                                            String providerName,
                                                            String payInstrument
    ) {
        for (MobileGroupRole role : dbList) {
            if (role.CategoryName.equalsIgnoreCase(catName) &&
                    role.GradeName.equalsIgnoreCase(gradeName) &&
                    role.MobilePayInstType.equalsIgnoreCase(payInstrument) &&
                    role.MobileProvider.equalsIgnoreCase(providerName)) {
                return role;
            }
        }
        return null;
    }

    public void addMobileRole(MobileGroupRole role, ExtentTest... t1) throws IOException {
        ExtentTest tNode = (t1.length > 0)? t1[0] : pNode;
        Markup m = MarkupHelper.createLabel("addUpdateMobileRole", ExtentColor.LIME);
        tNode.info(m); // Method Start Marker

        try {
            boolean isExist;

            // page init
            GroupRole_Page1 pageOne = GroupRole_Page1.init(tNode);
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(tNode);
            GroupRole_Page3 pageThree = new GroupRole_Page3(tNode);
            GroupRole_Page4 finalPage = GroupRole_Page4.init(tNode);

            // login as admin
            Login.init(tNode)
                    .loginAsSuperAdmin("GRP_ROL");

            // navigation
            pageOne.navAddWebGroupRole();

            // page 1
            pageOne.selectDomainName(role.DomainName);
            pageOne.selectCategoryName(role.CategoryName);
            pageOne.selectGradeName(role.GradeName);
            pageOne.checkMobileGroupRole();
            pageOne.submit();

            // create new
            pageTwo.initiateAddGroupRole();
            pageThree.selectCurrencyProvider(role.MobileProvider);
            pageThree.selectPaymentInstrumentByValue(role.MobilePayInst);
            pageThree.selectWalletOrBank(role.MobilePayInstType);
            pageThree.Submit();
            finalPage.setMobileGroupRoleName(role.ProfileName);
            finalPage.setMobileGroupRoleCode(role.ProfileCode);
            finalPage.checkRole(role.ApplicableRoles);
            finalPage.addMobileGroupRole();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("grouprole.added", "Verify Successfully adding mobile role", tNode)) {
                    role.setIsCreated();
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, tNode);
        }
    }

    public void addUpdateMobileRole(MobileGroupRole role) throws IOException {
        Markup m = MarkupHelper.createLabel("addUpdateMobileRole", ExtentColor.LIME);
        pNode.info(m); // Method Start Marker

        try {
            boolean isExist;

            // page inits
            GroupRole_Page1 pageOne = GroupRole_Page1.init(pNode);
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(pNode);
            GroupRole_Page3 pageThree = new GroupRole_Page3(pNode);
            GroupRole_Page4 finalPage = GroupRole_Page4.init(pNode);

            // login as admin
            Login.init(pNode)
                    .loginAsSuperAdmin("GRP_ROL");

            // navigation
            pageOne.navAddWebGroupRole();

            // page 1
            pageOne.selectDomainName(role.DomainName);
            pageOne.selectCategoryName(role.CategoryName);
            pageOne.selectGradeName(role.GradeName);
            pageOne.checkMobileGroupRole();
            pageOne.submit();

            // page 2
            isExist = pageTwo.isMobileRoleExist(role.ProfileName);
            if (isExist) {
                // update existing
                pageTwo.initiateUpdateGroupRole(role.ProfileName);
                finalPage.checkRole(role.ApplicableRoles);
                finalPage.updateGroupRole();
            } else {
                // create new
                pageTwo.initiateAddGroupRole();
                pageThree.selectCurrencyProvider(role.MobileProvider);
                pageThree.selectPaymentInstrumentByValue(role.MobilePayInst);
                pageThree.selectWalletOrBankByValue(role.MobilePayInstType);
                pageThree.Submit();
                finalPage.setMobileGroupRoleName(role.ProfileName);
                finalPage.setMobileGroupRoleCode(role.ProfileCode);
                finalPage.checkRole(role.ApplicableRoles);
                finalPage.addMobileGroupRole();
            }

            if (ConfigInput.isAssert) {
                if (isExist) {
                    if (Assertion.verifyActionMessageContain("grouprole.updated", "Update Mobile Group Role", pNode)) {
                        role.setIsCreated();
                    }
                } else {
                    if (Assertion.verifyActionMessageContain("grouprole.added", "Verify Successfully adding mobile role", pNode)) {
                        role.setIsCreated();
                    }
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Add all the Mobile Group Roles - this is part of base Setup
     *
     * @throws Exception
     */
    public void addMobileGroupRolesForSpecificGrade(User user, String gradeName, String payInstType, String payInstName) throws Exception {
        ExtentTest chNode = pNode.createNode("CHECK_MOBILE_ROLE_OPTIONS_TEST01", "Check for Mobile role Creation options by iterating Over UI");

        try {
            // Login with Superadmin with user having appropriate access
            Login.init(chNode)
                    .loginAsSuperAdmin("GRP_ROL");


            // Get Existing TCP Map from UI
            Map<String, MobileGroupRole> mobileRoleMap = null;

            MobileGroupRole mobGroupRole = new MobileGroupRole(user.CategoryCode, gradeName);

            // Page one initialize
            GroupRole_Page1 pageOne = GroupRole_Page1.init(chNode);

            // Navigate to Add Web Group Role and switch to Content Frame
            pageOne.navAddWebGroupRole();

            /*
             * Page1
             * Select Domain Category
             */
            pageOne.selectDomainName(mobGroupRole.DomainName);
            pageOne.selectCategoryName(mobGroupRole.CategoryName);
            pageOne.selectGradeName(mobGroupRole.GradeName);
            pageOne.checkMobileGroupRole();

            pageOne.submit();


            /*
             * Page2
             * Initiate add mobile Group Role
             */
            GroupRole_Page2 pageTwo = GroupRole_Page2.init(chNode);

            pageTwo.initiateAddGroupRole();

            /*
             * Page3
             * get combination of Currency Provider, PaymentInstrument and Payment Type
             */


            /*
             * Get the currency Providers and iterate
             */
            GroupRole_Page3 pageThree = new GroupRole_Page3(chNode);


            // Role is not present, hence it has to be created
            pageThree.selectCurrencyProvider(DataFactory.getDefaultProvider().ProviderName);
            pageThree.selectPaymentInstrument(payInstType);
            pageThree.selectWalletOrBank(payInstName);

            // Submit
            pageThree.Submit();



            /*
             * Page4
             * Get The Mobile Group Role Object, as the loop did not exit in previous step
             * Create Mobile Group Role
             */
            GroupRole_Page4 finalPage = GroupRole_Page4.init(pNode);
            String roleName = "Mob" + DataFactory.getRandomNumber(5);
            finalPage.setMobileGroupRoleName(roleName);
            finalPage.setMobileGroupRoleCode(roleName);
            finalPage.checkAllRoles();
            finalPage.addMobileGroupRole();

            String message = finalPage.getActionMessage();
            if (message.equals(MessageReader.getMessage("grouprole.added", null))) {
                pNode.pass("Successfully created Mobile Group Role: " + mobGroupRole.ProfileName);
                mobGroupRole.setMobileRoleName(roleName);
                mobGroupRole.writeDataToExcel();
            } else {
                pNode.fail("Failed to create Mobile Group Role: " + mobGroupRole.ProfileName);
                mobGroupRole.setRoleStatusasFail();
                mobGroupRole.writeDataToExcel();
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, chNode);
        }
    }


    /**
     * Get the Mobile Group Role, if existing in the System, UI bases Method
     *
     * @param map
     * @param provider
     * @param payInst
     * @param payInstType
     * @return
     */
    public String getMobileRolefromUIMap(Map<String, MobileGroupRole> map, String provider, String payInst, String payInstType) {
        for (MobileGroupRole gRole : map.values()) {
            if (gRole.MobileProvider.equalsIgnoreCase(provider)
                    && gRole.MobilePayInst.equalsIgnoreCase(payInst)
                    && gRole.MobilePayInstType.equalsIgnoreCase(payInstType)) {
                return gRole.ProfileName;
            }
        }
        return null;
    }

    public String getMobileRoleCodeFromUIMap(Map<String, MobileGroupRole> map, String profileName) {
        for (MobileGroupRole gRole : map.values()) {
            if (gRole.ProfileName.equalsIgnoreCase(profileName)) {
                return gRole.ProfileCode;
            }
        }
        return null;
    }

    /**
     * Verify that the Applicable roles are checked
     *
     * @param applicableRoles
     * @return
     * @throws IOException
     */
    public GroupRoleManagement verifyApplicableRole(List<String> applicableRoles) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyRole", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            for (int i = 0; i < applicableRoles.size(); i++) {
                String roleName = applicableRoles.get(i);
                if (!roleName.equals("CHECK_ALL")) {
                    if (driver.findElement(By.cssSelector("input[name='check'][value='" + roleName + "']")).isSelected()) {
                        pNode.pass(roleName + " is checked");
                    } else {
                        pNode.fail(roleName + " is Not checked");
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Remove a specific Role
     *
     * @param role
     * @param roleCode
     * @throws Exception
     */
    public void removeSpecificRole(WebGroupRole role, String[] roleCode) throws Exception {
        Markup m = MarkupHelper.createLabel("removeSpecificRole", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).loginAsSuperAdmin(saAddGroupRole);

            GroupRole_Page1 page = GroupRole_Page1.init(pNode);
            page.navAddWebGroupRole();
            page.selectDomainName(role.DomainName);
            page.selectCategoryName(role.CategoryName);
            page.checkWebGroupRole();
            page.submit();

            GroupRole_Page2 page2 = GroupRole_Page2.init(pNode);
            page2.initiateUpdateGroupRole(role.RoleName);
            page2.uncheckAroleByCode(roleCode);

            GroupRole_Page4.init(pNode).updateGroupRole();
            Assertion.verifyActionMessageContain("grouprole.updated", "Update Group Role", pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Remove a specific Role
     *
     * @param role
     * @param roleCode
     * @throws Exception
     */
    public void addSpecificRole(WebGroupRole role, String roleCode) throws Exception {
        Markup m = MarkupHelper.createLabel("removeSpecificRole", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).loginAsSuperAdmin(saAddGroupRole);

            GroupRole_Page1 page = GroupRole_Page1.init(pNode);
            page.navAddWebGroupRole();
            page.selectDomainName(role.DomainName);
            page.selectCategoryName(role.CategoryName);
            page.checkWebGroupRole();
            page.submit();

            GroupRole_Page2 page2 = GroupRole_Page2.init(pNode);
            page2.initiateUpdateGroupRole(role.RoleName);
            page2.checkArole(roleCode);

            GroupRole_Page4.init(pNode).updateGroupRole();
            Assertion.verifyActionMessageContain("grouprole.updated", "Update Group Role", pNode);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void addOrRemoveSpecificMobileRole(User usr, String Wallettype, String roleName, String provider, boolean... add) throws Exception {
        boolean toAdd = add.length > 0 ? add[0] : true;
        Markup m = MarkupHelper.createLabel("addOrRemoveSpecificMobileRole", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            Login.init(pNode).loginAsSuperAdmin(saAddGroupRole);

            GroupRole_Page1 page = GroupRole_Page1.init(pNode);
            page.navAddWebGroupRole();
            page.selectDomainName(usr.DomainName);
            page.selectCategoryName(usr.CategoryName);
            page.selectGradeName(usr.GradeName);
            page.checkMobileGroupRole();
            page.submit();

            GroupRole_Page2 page2 = GroupRole_Page2.init(pNode);
            page2.initiateUpdateGroupRole1(Wallettype, provider);
            if (toAdd) {
                page2.checkArole(roleName);
            } else {
                page2.uncheckArole(roleName);
            }

            GroupRole_Page4.init(pNode).updateGroupRole();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("grouprole.updated", "Update Group Role", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public boolean V5_openMobileRolePage(String catCode, String gradeName, ExtentTest... eTest) throws Exception {
        ExtentTest t1 = (eTest.length > 0) ? eTest[0] : pNode;
        GroupRole_Page1 pageOne = GroupRole_Page1.init(t1);

        // Navigate to Add mobile Group Role and switch to Content Frame
        //pageOne.navAddWebGroupRole();
        /*
         * Page1
         * Select Domain Category
         */
        pageOne.selectDomainName(DataFactory.getDomainName(catCode));
        pageOne.selectCategoryName(DataFactory.getCategoryName(catCode));
        pageOne.selectGradeName(gradeName);
        pageOne.checkMobileGroupRole();
        pageOne.V5_submit();
        return false;
    }

    public GroupRoleManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public GroupRoleManagement verifyUpdateGroupRolePage(WebGroupRole role) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyUpdateGroupRolePage", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            GroupRole_Page2.init(pNode)
                    .initiateUpdateGroupRole(role.RoleName);

            GroupRoleUpdate_pg1 page = GroupRoleUpdate_pg1.init(pNode);
            Assertion.verifyEqual(page.getRoleNameUI(), role.RoleName, "Verify RoleName From UI", pNode);
            Assertion.verifyEqual(page.getRoleCodeUI(), role.RoleName, "Verify RoleCode From UI", pNode);
            Assertion.verifyEqual(page.getDomainNameUI(), role.DomainName, "Verify DomainName From UI", pNode);
            Assertion.verifyEqual(page.getCategoryNameUI(), role.CategoryName, "Verify CategoryName From UI", pNode);

            Assertion.verifyEqual(fl.elementIsEnabled(page.roleName), false, "Verify that RoleName is disabled", pNode);
            Assertion.verifyEqual(fl.elementIsEnabled(page.roleCode), false, "Verify that roleCode is disabled", pNode, true);

            WebElement checkAll = page.getCheckboxCheckAll();
            Assertion.verifyEqual(fl.elementIsEnabled(checkAll), true, "Verify that Check All Role is enabled", pNode);

            verifyApplicableRole(role.ApplicableRoles);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }


        return this;
    }

}
