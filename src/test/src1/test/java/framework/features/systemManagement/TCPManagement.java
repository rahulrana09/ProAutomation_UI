package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.GradeDB;
import framework.entity.*;
import framework.features.common.Login;
import framework.pageObjects.tcp.*;
import framework.util.common.*;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class TCPManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static SuperAdmin saCustTCPCreate, saCustTcpApprove;
    private static OperatorUser naInstTCPCreate, naInstTcpApprove;
    private static boolean isSmoke = false;


    public TCPManagement() throws Exception {
    }

    public static TCPManagement init(ExtentTest t1) throws Exception {
        try {
            driver = DriverFactory.getDriver();
            pNode = t1;

            if (saCustTCPCreate == null) {
                saCustTCPCreate = DataFactory.getSuperAdminWithAccess("TCP_USER");
                saCustTcpApprove = DataFactory.getSuperAdminWithAccess("TCP_USERAPP");
                naInstTCPCreate = DataFactory.getOperatorUserWithAccess("TCP_INSTRMENT");
                naInstTcpApprove = DataFactory.getOperatorUserWithAccess("TCP_INSTRAPPR");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new TCPManagement();
    }

    public TCPManagement isSmokeCase() {
        isSmoke = true;
        return this;
    }

    /**
     * Add all customer TCPs
     *
     * @throws Exception
     */
    public void addApproveAllCustomerTCP() throws Exception {

        // Get Existing TCP Map from UI
        ExtentTest t1 = pNode.createNode("CREATE_CUST_TCP_TEST-01",
                "Login as Network admin with correct permission and check for UI options for creating Instrument TCps");

        try {
            List<String> arrCategoryNotToInclude = Arrays.asList(
                    Constants.NETWORK_ADMIN,
                    Constants.BANK_ADMIN,
                    Constants.CHANNEL_ADMIN,
                    Constants.CUSTOMER_CARE_EXE,
                    Constants.BULK_PAYER_ADMIN,
                    Constants.BANK_USER,
                    Constants.SUPER_ADMIN
            );

            List<String> regTypeToInclude = Arrays.asList("No KYC", "Full KYC");
            List<String> rnrCategoryList = DataFactory.getAllCategoriesFromRnr();
            List<CustomerTCP> lCustTcp = new ArrayList<>();

            for (String category : rnrCategoryList) {
                String catCode = DataFactory.getCategoryCode(category);
                String domainName = DataFactory.getDomainName(catCode);

                // exclude categories, like nwadm, superadmin...OPT
                if (arrCategoryNotToInclude.contains(catCode))
                    continue;

                // get required TCP for particular domain and category
                List<CustomerTCP> tCustTcp = DataFactory.loadRequiredCustomerTCPs(
                        domainName,
                        category,
                        regTypeToInclude
                );

                // append to all TCPs required in the system List...
                lCustTcp.addAll(tCustTcp);
            }

            // get all the existing Customer TCP from the UI
            Login.init(t1).loginAsSuperAdmin("TCP_USER");
            AddCustomerTCP_pg1 nav = AddCustomerTCP_pg1.init(t1);
            nav.navAddCustomerTcp();
            Map<String, CustomerTCP> tcpMap = nav.getExistingCustomerTCPmap();

            // create and approve all the required Customer TCPs
            for (CustomerTCP tcp : lCustTcp) {
                ExtentTest t2 = pNode.createNode("CREATE_CUST_TCP_" + tcp.CategoryName + "_" + tcp.RegType,
                        "Create Customer TCP for the Category '" + tcp.CategoryName + "', RegType '" + tcp.RegType + "'");

                String tcpNameFromUI = getExistingTCPName(tcpMap, tcp);
                if (tcpNameFromUI == null) {
                    createAndApproveCustomerTCP(tcp, t2);
                } else {
                    tcp.setProfileName(tcpNameFromUI);
                    tcp.setIsCreated();
                    tcp.setIsApproved();
                    t2.pass("TCP exists: " + tcpNameFromUI);
                }
                tcp.writeDataToExcel();
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


    /**
     * Approve Customer TCP
     *
     * @param tcp
     * @param t1  - as in case of base setup tcp are created in loop, so extent test instance
     *            is required
     * @return
     * @throws Exception
     */
    public boolean approveCustomerTCP(CustomerTCP tcp, ExtentTest t1) throws Exception {
        Markup m = MarkupHelper.createLabel("approveCustomerTCP: " + tcp.ProfileName, ExtentColor.BLUE);
        t1.info(m);
        try {
            ApproveCustomerTCP_pg1 pageOne = ApproveCustomerTCP_pg1.init(t1);
            pageOne.navCustomerTCPApproval();
            pageOne.selectTcpToApprove(tcp.ProfileName);
            pageOne.approveTCP();

            if (pageOne.assertTCPApproved(tcp)) {
                t1.pass("Successfully Approved TCP: " + tcp.ProfileName);
                tcp.setIsApproved();
                return true;
            } else {
                t1.fail("Failed to Approved TCP: " + tcp.ProfileName);
                t1.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return false;
    }

    /**
     * approveAllPendingTCP
     * Approve all the TCP in pending approval
     *
     * @throws Exception
     */
    public TCPManagement approveAllPendingCustomerTCP() throws Exception {
        ExtentTest tNode = pNode.createNode("APPROVE_PENDING_CUST_TCPs", "Approve All Pending Customer TCPs");

        try {
            Login.init(tNode)
                    .loginAsSuperAdmin("TCP_USERAPP");

            ApproveCustomerTCP_pg1 pageOne = ApproveCustomerTCP_pg1.init(tNode);
            pageOne.navCustomerTCPApproval();

            List<WebElement> tcps = driver.findElements(By.xpath("//*[@data-parent='#cust_tcp_approval_list']"));

            if (tcps.size() == 0) {
                tNode.pass("No Customer TCP is Pending For Approval");
                Utils.captureScreen(tNode);
                return this;
            }

            for (int i = 0; i < tcps.size(); i++) {
                pageOne.navCustomerTCPApproval();
                DriverFactory.getDriver().findElement(By.xpath("//*[@id='tcp0']")).click();

                Thread.sleep(1500);
                pageOne.approveTCP();
                waitForMessage();

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, tNode);
        }

        return this;
    }

    private void waitForMessage() {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("successMessage")));
        } catch (Exception e) {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("action_message")));
        }
    }

    /**
     * Get TCP name from the UI
     *
     * @param map - hash map of the UI elements, fetched before starting to create Base Setup
     * @param tcp - tcp object, runtime combination
     * @return tcp Name if its present in the Hash Map
     */
    public String getExistingTCPName(Map<String, CustomerTCP> map, CustomerTCP tcp) {
        for (CustomerTCP value : map.values()) {
            if (value.DomainCategory.equals(tcp.DomainName + " - " + tcp.CategoryName) &&
                    value.RegType.replaceAll("[^a-zA-Z0-9]+", "")
                            .equalsIgnoreCase(tcp.RegType.replaceAll("[^a-zA-Z0-9]+", "")) &&
                    value.ProviderName.equalsIgnoreCase(tcp.ProviderName)) {
                return value.ProfileName;
            }
        }
        return null;
    }


    public String getExistingTCPName(CustomerTCP tcp) throws Exception {
        Login.init(pNode).loginAsSuperAdmin(saCustTCPCreate);
        AddCustomerTCP_pg1 pg1 = new AddCustomerTCP_pg1(pNode);
        pg1.navAddCustomerTcp();
        Map<String, CustomerTCP> map = pg1.getExistingCustomerTCPmap();
        for (CustomerTCP value : map.values()) {
            if (value.DomainCategory.equals(tcp.DomainName + " - " + tcp.CategoryName) &&
                    value.RegType.equals(tcp.RegType)) {
                return value.ProfileName;
            }
        }
        return null;
    }

    /**********************************************************************************************
     * I N S T R U M E N T   T C P
     **********************************************************************************************/

    /**
     * Get TCP name from the UI
     *
     * @param map - hash map of the UI elements, fetched before starting to create Base Setup
     * @param tcp - tcp object, runtime combination
     * @return tcp Name if its present in the Hash Map
     */
    public String getExistingInstrumentTCPName(Map<String, InstrumentTCP> map, InstrumentTCP tcp) {
        for (InstrumentTCP value : map.values()) {
            if (value.DomainCategory.equals(tcp.DomainName + " - " + tcp.CategoryName) &&
                    value.GradeName.equals(tcp.GradeName) &&
                    value.CurrencyProvider.equals(tcp.CurrencyProvider) &&
                    value.InstrumentType.equals(tcp.InstrumentType)) {
                return value.ProfileName;
            }
        }
        return null;
    }

    /**
     * Approve a Instrument TCP
     *
     * @param tcp
     * @throws Exception
     */
    public boolean approveInstrumentTCP(InstrumentTCP tcp, ExtentTest t1) throws Exception {
        try {
            ExtentTest chNode = t1.createNode("APPROVE_INSTRUMENT_TCP_" + tcp.DomainName + "_" + tcp.CategoryName + "_"
                            + tcp.GradeName + "_" + tcp.InstrumentType + "_" + tcp.PaymentInstrument,
                    "Approve Instrument TCP" + tcp.DomainName + ", " + tcp.CategoryName + ", " + tcp.GradeName
                            + ", " + tcp.InstrumentType + ", " + tcp.PaymentInstrument);

            ApproveInstrumentTCP_pg1 pageOne = ApproveInstrumentTCP_pg1.init(chNode);
            pageOne.navInstrumentTCPApproval();
            pageOne.selectTcpToApprove(tcp.ProfileName);
            pageOne.approveTCP();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            if (pageOne.assertTCPApproved(tcp)) {
                chNode.pass("Successfully Approved the Instrument TCP: " + tcp.ProfileName);
                tcp.setIsApproved();
                return true;
            } else {
                chNode.fail("Failed to Approved the Instrument TCP: " + tcp.ProfileName);
                chNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
                return false;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        return false;
    }


    public boolean rejectInstrumentTCP(InstrumentTCP tcp, ExtentTest t1) throws Exception {
        try {
            ExtentTest chNode = t1.createNode("REJECT_INSTRUMENT_TCP_" + tcp.DomainName + "_" + tcp.CategoryName + "_"
                            + tcp.GradeName + "_" + tcp.InstrumentType + "_" + tcp.PaymentInstrument,
                    "Reject Instrument TCP" + tcp.DomainName + ", " + tcp.CategoryName + ", " + tcp.GradeName
                            + ", " + tcp.InstrumentType + ", " + tcp.PaymentInstrument);

            ApproveInstrumentTCP_pg1 pageOne = ApproveInstrumentTCP_pg1.init(chNode);
            pageOne.navInstrumentTCPApproval();
            pageOne.selectTcpToApprove(tcp.ProfileName);
            pageOne.rejectTCP();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        return false;
    }

    /**
     * approveAllPendingTCP
     * Approve all the TCP in pending approval
     *
     * @throws Exception
     */
    public TCPManagement approveAllPendingInstrumentTCP() throws Exception {
        ExtentTest tNode = pNode.createNode("APPROVE_PENDING_INST_TCPs", "Approve All Pending Instrument TCPs");
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 100);
        try {
            Login.init(tNode)
                    .login(naInstTcpApprove);

            ApproveInstrumentTCP_pg1 pageOne = ApproveInstrumentTCP_pg1.init(tNode);
            pageOne.navInstrumentTCPApproval();

            List<WebElement> approvalLinks = DriverFactory.getDriver().findElements(By.cssSelector(".accordion-toggle.collapsed"));
            for (WebElement elem : approvalLinks) {
                pageOne.clickOnApprovalElement(elem);
                pageOne.approveTCP();
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("successMessage")));
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, tNode);
        }
        return this;
    }

//*********************************************************************************************************************

    /**
     * M E T H O D S  -  S P E C I F I C -   F O R -  T E S T -  C A S E S
     * N O T   P A R T   O F    D A T A B U I L D E R
     */

    public void addApproveCustomerTCP(CustomerTCP tcp) throws Exception {

        try {

            Login.init(pNode).loginAsSuperAdmin(saCustTCPCreate);

            Markup m = MarkupHelper.createLabel("addApproveCustomerTCP: " + tcp.ProfileName, ExtentColor.ORANGE);
            pNode.info(m);

            AddCustomerTCP_pg1 pageOne = AddCustomerTCP_pg1.init(pNode);

            pageOne.fillPageOneDetails(tcp);

            String errorMessage = pageOne.getModalErrorMessage();
            if (errorMessage != null) {
                String expected = MessageReader.getMessage("trfProfileDao.error.CustProfileExists", null);
                if (Assertion.verifyEqual(errorMessage, expected, "Already Exist", pNode)) {
                    pNode.info("Skipping the case as Customer TCP already exists");
                    return;
                }

            }

            AddCustomerTCP_pg2 pageTwo = AddCustomerTCP_pg2.init(pNode);
            pageTwo.fillAlltheTextField();
            Thread.sleep(1200);
            pageTwo.submit();
            pageTwo.confirm();
            Thread.sleep(1300);

            if (pageOne.assertTCPCreation(tcp)) {
                pNode.pass("Successfully Created the Customer TCP : " + tcp.ProfileName);

                Login.init(pNode).loginAsSuperAdmin(saCustTcpApprove);

                if (approveCustomerTCP(tcp, pNode)) {
                    pNode.pass("Successfully created Customer TCP");
                    tcp.setIsCreated();
                }

            } else {
                pNode.fail("Fail to Create Customer TCP : " + tcp.ProfileName + ", Domain - " + tcp.DomainName + ", Category - " + tcp.CategoryName);
                pNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }


            /**
             * Login as TCP Approve rights
             * Approve All the created TCPs
             */


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void addInstrumentTCP(InstrumentTCP tcp, boolean... isApprove) throws Exception {

        ExtentTest chNode = pNode.createNode("Add Instrument TCP - " + tcp.DomainName + "-" + tcp.CategoryName
                + "-" + tcp.GradeName + "-" + tcp.InstrumentType + "-" + tcp.PaymentInstrument);

        try {

            if ((tcp.DomainName.equalsIgnoreCase("Enterprise") ||
                    tcp.DomainName.equalsIgnoreCase("International Merchant")) &&
                    tcp.PaymentInstrument.equalsIgnoreCase("BANK")) {
                pNode.warning("TCP cannot be created For Bank of Enterprise & International Merchant");
                return;
            }

            Login.init(chNode).login(naInstTCPCreate);

            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(chNode);

            pageOne.fillPageOneDetails(tcp);

            String errorMessage = Assertion.checkErrorPresent();
            if (errorMessage != null) {
                String expected = MessageReader.getMessage("trfProfileDao.error.InstProfileExists", null);
                if (Assertion.verifyEqual(errorMessage, expected, "Already Exist Instrument TCP", pNode)) {
                    pNode.skip("Skipping the case as Instrument TCP already exists");
                    pNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
                    return;
                }
            }

            AddInstrumentTCP_pg2 pageTwo = AddInstrumentTCP_pg2.init(chNode);
            pageTwo.fillAllTextField();
            /**
             * Extra assertion are added to see few element Should be displayed at the screen
             *
             */
            if (isSmoke)
                pageTwo.extraAssertionsForSmoke();

            pageTwo.Next();
            pageTwo.Confirm();

            if (pageOne.assertTCPCreation(tcp)) {
                chNode.pass("Successfully Created Instrument TCP");
            } else {
                chNode.fail("Failed to Create Instrument TCP");
                chNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }


            /**
             * Login as User with Approver Rights and
             * Complete Approval
             */
            Login.init(chNode).login(naInstTcpApprove);

            //ExtentTest chNode1 = pNode.createNode("Approve Customer TCP: " + tcp.DomainName + "-" + tcp.CategoryName);
            boolean approve = (isApprove.length > 0) ? isApprove[0] : true;
            if (approve) {
                approveInstrumentTCP(tcp, chNode);
                Assertion.verifyEqual(tcp.APPROVED, "Y", "Verify that Instrument TCP is successfully Approved", chNode);
            } else {
                rejectInstrumentTCP(tcp, chNode);
                Assertion.assertActionMessageContain("systemparty.message.rejected", "Reject  Operator User", pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, chNode);
        }
    }

    public void viewCustomerTCP(String domainName, String categoryName, String regType) throws Exception {

        try {

            Utils.createLabelForMethod("viewCustomerTCP", pNode);

            Login.init(pNode).loginAsSuperAdmin(saCustTCPCreate);

            AddCustomerTCP_pg1 pageOne = AddCustomerTCP_pg1.init(pNode);

            CustomerTCP custTCP = new CustomerTCP(domainName, categoryName, regType);

            pageOne.cancelAddTCP();

            pageOne.navAddCustomerTcp();

            Map<String, CustomerTCP> customerTCPMap = pageOne.getExistingCustomerTCPmap();

            String tcpNameUI = getExistingTCPName(customerTCPMap, custTCP);

            if (tcpNameUI != null) {
                pNode.info("TCP profile is present: " + tcpNameUI);
                pageOne.clickTCPViewLink(tcpNameUI);

                ViewCustomerTCPPage1 viewPage = new ViewCustomerTCPPage1(pNode);

                Utils.putThreadSleep(3000);

                Assertion.verifyEqual(viewPage.getDomainNameFromUI(), domainName, "Verify Domain Name", pNode);
                Assertion.verifyEqual(viewPage.getCategoryNameFromUI(), categoryName, "Verify Category Name", pNode);
                Assertion.verifyEqual(viewPage.getRegTypeFromUI(), regType, "Verify Regn Type Name", pNode);
                Assertion.verifyEqual(viewPage.getProfileNameFromUI(), tcpNameUI, "Verify TCP Profile Name", pNode);


            } else {
                pNode.skip("TCP profile is Not Found. Skipping the case." + tcpNameUI);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param Value
     * @throws Exception
     * @deprecated EdittcpforSubs this method is not generic
     * Hard coded for particular grade user
     */
    public void EdittcpforSubs(String Value, boolean minValue, boolean maxValue, ExtentTest test) throws Exception {
        Markup m = MarkupHelper.createLabel("Edit Customer TCP Minimum Transaction AMount:", ExtentColor.ORANGE);
        test.info(m);
        try {
            OperatorUser operatorUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(pNode).login(operatorUser);

            EditTCP pageOne = EditTCP.init(test);
            pageOne.navAddInstrumentTcp();
            pageOne.selectontcp();
            pageOne.clickedit();
            if (minValue) {
                pageOne.editonMintcp(Value);
            }
            if (maxValue) {
                pageOne.editonMaxtcp(Value);
            }
            pageOne.clickNext();
            pageOne.clickconfirm();
            pageOne.navInstrumentTcpApproval();
            pageOne.clickdetails();
            pageOne.clickonApprove();
            Utils.captureScreen(test);
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(test);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, test);
        }
    }


    /**
     * @param Value
     * @throws Exception
     * @deprecated This class is deprecated as it edits a particular tcp which is hardcoded
     */
    public void editInstrumenttcp(String Value) throws Exception {

        try {
            OperatorUser tcpCreator = DataFactory.getOperatorUsersWithAccess("TCP_INSTRMENT").get(0);
            Login.init(pNode).login(tcpCreator);

            Markup m = MarkupHelper.createLabel("EditCustomerTCP:", ExtentColor.ORANGE);
            pNode.info(m);

            EditTCP pageOne = EditTCP.init(pNode);

            pageOne.navAddInstrumentTcp();
            pageOne.selectontcp();
            pageOne.clickedit();
            pageOne.O2SamountTCP(Value);
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            pageOne.clickNext();
            pageOne.clickConfirm();

            pageOne.navInstrumentTcpApproval();
            pageOne.clickdetails();
            pageOne.clickonApprove();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * viewInstrumentTCP
     *
     * @param instrumentTCP Pass the instrument tcp object
     * @throws Exception
     */
    public void viewInstrumentTCP(InstrumentTCP instrumentTCP) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("viewInstrumentTCP ", ExtentColor.BLUE);
            pNode.info(m);
            Login.init(pNode).login(naInstTCPCreate);

            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(pNode);
            pageOne.navAddInstrumentTcp();
            pageOne.clickTCPViewLink(instrumentTCP.ProfileName);
            ViewInstrumentTCPPage viewPage = new ViewInstrumentTCPPage(pNode);
            Utils.putThreadSleep(3000);
            Utils.scrollToTopOfPage();

            Assertion.verifyEqual(viewPage.getDomainNameFromUI(), instrumentTCP.DomainName, "Verify Domain Name", pNode);
            Assertion.verifyEqual(viewPage.getCategoryNameFromUI(), instrumentTCP.CategoryName, "Verify Category Name", pNode);
            Assertion.verifyEqual(viewPage.getGradeNameFromUI(), instrumentTCP.GradeName, "Verify TCP GradeName", pNode);
            Assertion.verifyEqual(viewPage.getPaymentInstrumentFromUI(), instrumentTCP.PaymentInstrument, "Verify TCP PaymentInstrument", pNode);
            Assertion.verifyEqual(viewPage.getProfileNameFromUI(), instrumentTCP.ProfileName, "Verify TCP Profile Name", pNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /**
     * @param instrumentTCP
     * @param payerCount
     * @param payerAmount
     * @param payeeCount
     * @param payeeAmount
     * @return
     * @throws Exception
     */
    public TCPManagement modifyInitiateInstrumentTCP(InstrumentTCP instrumentTCP, String payerCount, String payerAmount, String payeeCount, String payeeAmount) throws Exception {

        try {

            Markup m = MarkupHelper.createLabel("modifyInitiateInstrumentTCP ", ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(naInstTCPCreate);

            Thread.sleep(3000);

            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(pNode);

            pageOne.navAddInstrumentTcp();

            Thread.sleep(3000);

            pageOne.clickTCPEditLink(instrumentTCP.ProfileName);

            ModifyInstrumentTCPPage modifyPage = new ModifyInstrumentTCPPage(pNode);

            modifyPage.clickEditButton();

            modifyPage.modifyTCPThresholdAllServices(payerCount, payerAmount, payeeCount, payeeAmount);

            modifyPage.clickNextButton();
            modifyPage.clickConfirmButton();

            Utils.scrollToTopOfPage();

            String expectedmsg = MessageReader.getDynamicMessage("tcprofile.success.modifyinitiateInstTransferProfile", instrumentTCP.ProfileName, instrumentTCP.DomainName, instrumentTCP.CategoryName);
            Assertion.verifyContains(modifyPage.getSuccessMessage(), expectedmsg, "Verify Instrument TCP Modify", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * @param profileName
     * @param fromUser
     * @param toUser
     * @param grade
     * @param provider
     * @param ServiceName
     * @param Value
     * @throws Exception note : Please do revert the edited tcp to default value
     */

    public void editTcpForChannelUser(User user, String profileName, String fromUser, String toUser, String grade, String provider, String ServiceName, String Value) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("editTcpForChannelUser:", ExtentColor.ORANGE);
            pNode.info(m);

            EditTCP pageOne = EditTCP.init(pNode);
            ModifyInstrumentTCPPage modifyPage = new ModifyInstrumentTCPPage(pNode);
            ApproveInstrumentTCP_pg1 approvePg1 = new ApproveInstrumentTCP_pg1(pNode);

            pageOne.navAddInstrumentTcp();
            pageOne.selectontcpForChannelUser(profileName, fromUser, toUser, grade, provider);
            pageOne.clickedit();
            pageOne.editonServiceName(ServiceName, Value);
            pageOne.clickNext();
            pageOne.clickconfirm();

            String expectedmsg = MessageReader.getDynamicMessage("tcprofile.success.modifyinitiateInstTransferProfile", profileName, user.DomainName, user.CategoryName);
            Assertion.verifyContains(modifyPage.getSuccessMessage(), expectedmsg, "Verify Instrument TCP Modify", pNode);

            Login.init(pNode).login(naInstTcpApprove);
            pageOne.navInstrumentTcpApproval();
            pageOne.clickdetails();
            pageOne.clickonApprove();

            String expectedmsg1 = MessageReader.getDynamicMessage("tcprofile.instrument.success.editApproved", profileName, user.DomainName, user.CategoryName);
            Assertion.verifyContains(approvePg1.getSuccessMessage(), expectedmsg1, "Verify Instrument TCP Modify", pNode);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }


    }


    /**
     * modifyApproveInstrumentTCP
     *
     * @param instrumentTCP
     */
    public void modifyApproveInstrumentTCP(InstrumentTCP instrumentTCP) {

        Markup m = MarkupHelper.createLabel("modifyApproveInstrumentTCP :" + instrumentTCP.ProfileName, ExtentColor.BLUE);
        pNode.info(m);

        try {
            Login.init(pNode).login(naInstTcpApprove);

            ApproveInstrumentTCP_pg1 approvePg1 = new ApproveInstrumentTCP_pg1(pNode);

            approvePg1.navInstrumentTCPApproval();

            approvePg1.selectTcpToApprove(instrumentTCP.ProfileName);

            approvePg1.approveTCP();

            Utils.scrollToTopOfPage();

            if (ConfigInput.isAssert) {
                String expectedmsg = MessageReader.getDynamicMessage("tcprofile.instrument.success.editApproved", instrumentTCP.ProfileName, instrumentTCP.DomainName, instrumentTCP.CategoryName);
                Assertion.verifyContains(approvePg1.getSuccessMessage(), expectedmsg, "Verify Instrument TCP Modify", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TCPManagement deleteInitiateInstrumentTCP(InstrumentTCP instrumentTCP) throws Exception {

        try {

            Markup m = MarkupHelper.createLabel("deleteInitiateInstrumentTCP: " + instrumentTCP.ProfileName, ExtentColor.BLUE);
            pNode.info(m);

            Login.init(pNode).login(naInstTCPCreate);

            Thread.sleep(3000);

            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(pNode);

            pageOne.navAddInstrumentTcp();

            Thread.sleep(3000);

            pageOne.clickTCPDeleteLink(instrumentTCP.ProfileName);

            Utils.scrollToTopOfPage();

            WebElement successMsg = driver.findElement(By.id("main_success_msg"));
            new FunctionLibrary(driver).waitWebElementVisible(successMsg);

            String actualMsg = successMsg.getText();
            String expectedmsg = MessageReader.getDynamicMessage("tcprofile.success.deleteInstInitiation", instrumentTCP.ProfileName, instrumentTCP.DomainName, instrumentTCP.CategoryName);
            Assertion.verifyContains(actualMsg, expectedmsg, "Verify Instrument TCP Delete", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void deleteApproveInstrumentTCP(InstrumentTCP instrumentTCP) {

        Markup m = MarkupHelper.createLabel("deleteApproveInstrumentTCP :" + instrumentTCP.ProfileName, ExtentColor.BLUE);
        pNode.info(m);

        try {
            Login.init(pNode).login(naInstTcpApprove);

            ApproveInstrumentTCP_pg1 approvePg1 = new ApproveInstrumentTCP_pg1(pNode);

            approvePg1.navInstrumentTCPApproval();

            approvePg1.selectTcpToApprove(instrumentTCP.ProfileName);

            approvePg1.approveTCP();

            Utils.scrollToTopOfPage();

            String expectedmsg = MessageReader.getDynamicMessage("tcprofile.instrument.success.deleteApproved", instrumentTCP.ProfileName, instrumentTCP.DomainName, instrumentTCP.CategoryName);
            Assertion.verifyContains(approvePg1.getSuccessMessage(), expectedmsg, "Verify Instrument TCP Deletion Approval", pNode);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /**********************************************************************************************
     * INITIATE LIMIT RESET
     **********************************************************************************************/

    /**
     * Method to INITIATE LIMIT RESET
     *
     * @throws Exception
     */
    public void initiateLimitReset(User user, String userType, String serviceName, String extension, String limit, String frequency, String bearer) throws Exception {
        /*
        Clean and Create Header
         */
        try {
            OperatorUser tcpLimiter = DataFactory.getOperatorUsersWithAccess("TCP_INITLIMITRESET").get(0);
            Login.init(pNode).login(tcpLimiter);

            Markup m = MarkupHelper.createLabel("InitiateLimitReset:", ExtentColor.ORANGE);
            pNode.info(m);

            InitiateLimitReset pageOne = InitiateLimitReset.init(pNode);

            pageOne.navInitiateLimitReset();
            pageOne.selectUserType(userType);
            pageOne.enterMobilerNumber(user.MSISDN);
            pageOne.selectServiceName(serviceName);
            pageOne.selectExtension(extension);
            pageOne.selectLimit(limit);
            pageOne.selectFrequency(frequency);
            pageOne.selectBearer(bearer);

            pageOne.selectExpiryDate(4);
            pageOne.clickOnSubmit();


            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("tcp.limit.reset.initiate", "Limit Reset Initiated", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**********************************************************************************************
     * APPROVE LIMIT RESET
     **********************************************************************************************/

    /**
     * Method to APPROVE LIMIT RESET
     *
     * @throws Exception
     */
    public void approveLimitReset(User user, String userType, boolean... approve) throws Exception {
        boolean isApprove = approve.length > 0 ? approve[0] : true;
        try {
            OperatorUser tcpLimiter = DataFactory.getOperatorUsersWithAccess("TCP_APPRLIMITRESET").get(1);
            Login.init(pNode).login(tcpLimiter);

            Markup m = MarkupHelper.createLabel("ApproveLimitReset:", ExtentColor.ORANGE);
            pNode.info(m);

            ApprovalLimitReset pageOne = ApprovalLimitReset.init(pNode);

            pageOne.navApprovalLimitReset();
            pageOne.selectFromDate(0);
            pageOne.selectToDate(0);
            pageOne.selectUserType(userType);
            pageOne.enterMobilerNumber(user.MSISDN);
            pageOne.clickOnSubmit();
            pageOne.clickOnCheckBox(user.LoginId);
            pageOne.clickOnConfirm();
            if (ConfigInput.isAssert) {
                if (isApprove) {
                    pageOne.clickOnApprove();
                    Assertion.verifyActionMessageContain("tcp.limit.reset.approval", "Limit Reset Approved", pNode);
                } else {
                    pageOne.clickOnReject();
                    Assertion.verifyActionMessageContain("tcp.limit.reset.reject", "Limit Reset Rejected", pNode);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    public void addAndRejectInstrumentTCP(InstrumentTCP tcp) throws Exception {

        ExtentTest chNode = pNode.createNode("Add Instrument TCP - " + tcp.DomainName + "-" + tcp.CategoryName
                + "-" + tcp.GradeName + "-" + tcp.InstrumentType + "-" + tcp.PaymentInstrument);

        try {

            if ((tcp.DomainName.equalsIgnoreCase("Enterprise") ||
                    tcp.DomainName.equalsIgnoreCase("International Merchant")) &&
                    tcp.PaymentInstrument.equalsIgnoreCase("BANK")) {
                pNode.warning("TCP cannot be created For Bank of Enterprise & International Merchant");
                return;
            }

            Login.init(chNode).login(naInstTCPCreate);

            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(chNode);

            pageOne.fillPageOneDetails(tcp);

            String errorMessage = Assertion.checkErrorPresent();
            if (errorMessage != null) {
                String expected = MessageReader.getMessage("trfProfileDao.error.InstProfileExists", null);
                if (Assertion.verifyEqual(errorMessage, expected, "Already Exist Instrument TCP", pNode)) {
                    pNode.skip("Skipping the case as Instrument TCP already exists");
                    pNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
                    return;
                }
            }

            AddInstrumentTCP_pg2 pageTwo = AddInstrumentTCP_pg2.init(chNode);
            pageTwo.fillAllTextField();
            /**
             * Extra assertion are added to see few element Should be displayed at the screen
             *
             */
            if (isSmoke)
                pageTwo.extraAssertionsForSmoke();

            pageTwo.Next();
            pageTwo.Confirm();

            if (pageOne.assertTCPCreation(tcp)) {
                chNode.pass("Successfully Created Instrument TCP");

            } else {
                chNode.fail("Failed to Create Instrument TCP");
                chNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }


            /**
             * Login as User with Approver Rights and
             *
             */
            if (!naInstTCPCreate.LoginId.equals(naInstTcpApprove.LoginId)) {
                Login.init(chNode).login(naInstTcpApprove);
            }


            //Reject the created Instrument Level TCP
            rejectInstrumentTCP(tcp, chNode);
            String actual = driver.findElement(By.xpath("//*[@id='successMessage']")).getText();
            String expected = MessageReader.getDynamicMessage("tcprofile.instrument.success.AIRejected", tcp.ProfileName, tcp.DomainName, tcp.CategoryName);
            Assertion.verifyEqual(actual, expected, "Assert reject TCP", chNode);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, chNode);
        }
    }


    public TCPManagement copyExistingCustomerTCP(CustomerTCP existingTCP, CustomerTCP newTCP) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("CopyCustomerTCP: " + existingTCP.ProfileName, ExtentColor.ORANGE);
            pNode.info(m);

            Login.init(pNode).loginAsSuperAdmin(saCustTCPCreate);


            AddCustomerTCP_pg1 pageOne = AddCustomerTCP_pg1.init(pNode);
            CopyCustomerTCPPage copyCustomerTCPPage = new CopyCustomerTCPPage(pNode);

            pageOne.navAddCustomerTcp();

            Map<String, CustomerTCP> customerTCPMap = pageOne.getExistingCustomerTCPmap();

            String tcpNameUI = getExistingTCPName(customerTCPMap, existingTCP);

            if (tcpNameUI != null) {
                pNode.info("TCP profile is present: " + tcpNameUI);
                JavascriptExecutor jse = (JavascriptExecutor) driver;
                jse.executeScript("window.scrollTo(0,500)");
                pageOne.clickOnCopyCustomerTCP(tcpNameUI);
            } else {
                pNode.info("TCP profile is not Present of this combination: " + existingTCP.DomainCategory);
                pNode.skip("Skipping the case as TCP profile is not Present of this combination");
                return this;
            }


            copyCustomerTCPPage.selectDomainName(newTCP.DomainName);
            copyCustomerTCPPage.selectCategoryName(newTCP.CategoryName);
            copyCustomerTCPPage.selectRegTypeByValue(newTCP.RegType);
            copyCustomerTCPPage.setProfileName(newTCP.ProfileName);
            copyCustomerTCPPage.setDescription(newTCP.ProfileName);
            copyCustomerTCPPage.clickOnCopyButton();

            String errorMessage = copyCustomerTCPPage.getModalErrorMessage();//pageOne.getModalErrorMessage();
            if (errorMessage != null) {
                String expected = MessageReader.getMessage("trfProfileDao.error.CustProfileExists", null);
                if (Assertion.verifyEqual(errorMessage, expected, "Already Exist", pNode)) {
                    pNode.info("Skipping the case as Customer TCP already exists");
                    return this;
                }
            }

            if (pageOne.assertTCPCreation(newTCP)) {
                pNode.pass("Successfully Created the Customer TCP : " + newTCP.ProfileName);

                Login.init(pNode).loginAsSuperAdmin(saCustTcpApprove);

                if (approveCustomerTCP(newTCP, pNode)) {
                    pNode.pass("Successfully created Customer TCP");
                }

            } else {
                pNode.fail("Fail to Create Customer TCP : " + newTCP.ProfileName + ", Domain - " + newTCP.DomainName + ", Category - " + newTCP.CategoryName);
                pNode.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public TCPManagement deleteCustomerTCP(CustomerTCP customerTCP) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteCustomerTCP: " + customerTCP.ProfileName, ExtentColor.ORANGE);
            pNode.info(m);

            Login.init(pNode).loginAsSuperAdmin(saCustTCPCreate);

            AddCustomerTCP_pg1 pageOne = AddCustomerTCP_pg1.init(pNode);

            pageOne.navAddCustomerTcp();

            String tcpNameUI = getExistingTCPName(customerTCP);

            if (tcpNameUI != null) {
                pNode.info("TCP profile is present: " + tcpNameUI);
                pageOne.clickOnDeleteCustomerTCP(tcpNameUI);
            } else {
                pNode.info("TCP profile is not Present of this combination: " + customerTCP.DomainCategory);
                pNode.skip("Skipping the case as TCP profile is not Present of this combination");
                return this;
            }

            if (ConfigInput.isAssert) {
                String actual = pageOne.getActionMessage();
                Assertion.verifyMessageContain(actual, "tcprofile.success.deleteCustInitiation", "", pNode, customerTCP.ProfileName, customerTCP.DomainName, customerTCP.CategoryName);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    /**
     *  Modify All Instrument TCP
     *  todo Remove this code
     */
    /**
     * Add Instrument Level TCP
     *
     * @throws Exception
     */
    public void modifyAllInstrumentTCPs() throws Exception {
        try {

            ExtentTest iterator = pNode.createNode("Modify",
                    "Login as Network admin with correct permission and check for UI options for creating Instrument TCps");

            // Navigate to add Instrument TCP page
            Login.init(iterator).login(naInstTCPCreate);

            AddInstrumentTCP_pg1 nav = AddInstrumentTCP_pg1.init(iterator);
            nav.navAddInstrumentTcp();

            // Get Existing TCP Map from UI
            Map<String, InstrumentTCP> tcpMap = nav.getExistingInstrumentTCPmap();


            for (InstrumentTCP tcp : tcpMap.values()) {
                String tcpNameFromUI = getExistingInstrumentTCPName(tcpMap, tcp);
                if (tcpNameFromUI.contains("InstTCP")) {
                    pNode.info("TCP exists: Modifying TCP" + tcpNameFromUI);
                    modifyInitiateInstrumentTCP(tcp, Constants.MAX_THRESHOLD, Constants.MAX_THRESHOLD, Constants.MAX_THRESHOLD, Constants.MAX_THRESHOLD);
                    modifyApproveInstrumentTCP(tcp);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Add approve Instrument TCP
     *
     * @throws Exception
     */
    public void addAndApproveAllInstrumentTCP() throws Exception {
        try {
            List<String> arrCategoryNotToInclude = Arrays.asList(
                    Constants.NETWORK_ADMIN,
                    Constants.BANK_ADMIN,
                    Constants.CHANNEL_ADMIN,
                    Constants.CUSTOMER_CARE_EXE,
                    Constants.BULK_PAYER_ADMIN,
                    Constants.BANK_USER,
                    Constants.SUPER_ADMIN
            );

            // get all combinations of TCPs for Category available in ConfigInput
            List<String> rnrCategoryList = DataFactory.getAllCategoriesFromRnr();
            List<String> bankList = DataFactory.getAllBankNamesForMobileRoleCreation();
            List<InstrumentTCP> lInstTcp = new ArrayList<>();

            for (String category : rnrCategoryList) {
                String catCode = DataFactory.getCategoryCode(category);

                // exclude categories, like nwadm, superadmin...OPT
                if (arrCategoryNotToInclude.contains(catCode))
                    continue;

                String domainName = DataFactory.getDomainName(catCode);
                List<GradeDB> grades = DataFactory.getGradesForCategory(catCode);

                if (grades.size() == 0) {
                    continue;
                }

                for (GradeDB grade : grades) {
                    List<InstrumentTCP> tInsTcp = DataFactory.getListOfRequiredInstrumentTCPs(
                            domainName,
                            catCode,
                            grade.GradeName,
                            bankList
                    );
                    lInstTcp.addAll(tInsTcp);
                }
            }

            ExtentTest t1 = pNode.createNode("CREATE_INST_TCP_TEST-01",
                    "Login as Network admin with correct permission and check for UI options for creating Instrument TCps");

            // Get Existing TCP Map from UI
            AddInstrumentTCP_pg1 nav = AddInstrumentTCP_pg1.init(t1);
            Login.init(t1).login(naInstTCPCreate);
            nav.navAddInstrumentTcp();
            Map<String, InstrumentTCP> tcpMap = nav.getExistingInstrumentTCPmap();

            for (InstrumentTCP tcp : lInstTcp) {
                ExtentTest t2 = pNode.createNode("CREATE_INST_TCP_" +
                                tcp.CategoryName + "_" +
                                tcp.PaymentInstrument + "_" +
                                tcp.PaymentInstrument + "_" +
                                tcp.GradeName,
                        "Create Instrument TCP for the category '" + tcp.CategoryName + "', " +
                                "PayInstrument '" + tcp.PaymentInstrument + "' " +
                                "Grade '" + tcp.GradeName + "' ");

                String tcpNameFromUI = getExistingInstrumentTCPName(tcpMap, tcp);

                if (tcpNameFromUI == null) {
                    createAndApproveInstrumentTCP(tcp, t2);
                } else {
                    t2.pass("Instrument TCP with profile name: "+ tcpNameFromUI +", already exists!");
                    tcp.setProfileName(tcpNameFromUI);
                    tcp.setIsCreated();
                    tcp.setIsApproved();
                }
                tcp.writeDataToExcel();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Create an approve Instrument TCP
     *
     * @param tcp
     * @param t1  [optional][Extent Test] - When Creating instrument TCP from base Setup, this has to be passed
     * @throws Exception
     */
    public void createAndApproveInstrumentTCP(InstrumentTCP tcp, ExtentTest... t1) throws Exception {

        ExtentTest tNode = (t1.length > 0) ? t1[0] : pNode;
        // Marker
        Markup m = MarkupHelper.createLabel("createAndApproveInstrumentTCP: " + tcp.ProfileName, ExtentColor.BLUE);
        tNode.info(m);
        try {
            AddInstrumentTCP_pg1 pageOne = AddInstrumentTCP_pg1.init(tNode);

            Login.init(tNode).login(naInstTCPCreate);
            pageOne.navAddInstrumentTcp();
            pageOne.openCreateTCP();
            pageOne.selectProvider(tcp.CurrencyProvider);
            pageOne.selectDomainName(tcp.DomainName);
            pageOne.selectCategoryName(tcp.CategoryName);
            pageOne.selectGradeName(tcp.GradeName);
            pageOne.selectPaymentIstrument(tcp.PaymentInstrument);
            pageOne.selectWalletType(tcp.InstrumentType);
            pageOne.setProfileName(tcp.ProfileName);
            pageOne.setDescription(tcp.ProfileName);
            pageOne.Submit();

            /*
             * Check if there is any error message
             * if error exists, exit the loop, update the TCP Object
             */
            String errorMessage = pageOne.getErrorMessage();
            if (!errorMessage.isEmpty()) {
                tNode.warning(errorMessage);
                tcp.setError(errorMessage);
                tcp.writeDataToExcel();
                pageOne.openCreateTCP();
                return;
            }

            /*
             * Page 2
             * Fill the Thresholds
             */
            AddInstrumentTCP_pg2 pageTwo = AddInstrumentTCP_pg2.init(tNode);
            pageTwo.fillAllTextField();
            pageTwo.Next();
            pageTwo.Confirm();

            if (pageOne.assertTCPCreation(tcp)) {
                tNode.pass("Successfully Created Instrument TCP");
                tcp.setIsCreated();

                /*
                Approve TCP
                */
                Login.init(tNode).login(naInstTcpApprove);
                if (approveInstrumentTCP(tcp, tNode)) {
                    tcp.setIsApproved();
                }
            } else {
                tNode.fail("Failed to Create Instrument TCP");
                Utils.captureScreen(tNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, tNode);
        }
    }

    /**
     * Create and approve Customer TCP
     *
     * @param tcp
     * @throws Exception
     */
    public void createAndApproveCustomerTCP(CustomerTCP tcp, ExtentTest... t1) throws Exception {
        ExtentTest tNode = (t1.length > 0) ? t1[0] : pNode;

        // Marker
        Markup m = MarkupHelper.createLabel("createAndApproveCustomerTCP: " + tcp.ProfileName, ExtentColor.BLUE);
        tNode.info(m);

        AddCustomerTCP_pg1 pageOne = AddCustomerTCP_pg1.init(tNode);

        try {

            Login.init(tNode).loginAsSuperAdmin(saCustTCPCreate);

            // Complete TCP Creation
            pageOne.navAddCustomerTcp();
            pageOne.addInitiateCustomerTCP();
            pageOne.selectProviderName(tcp.ProviderName);
            pageOne.selectDomainName(tcp.DomainName);
            pageOne.selectCategoryName(tcp.CategoryName);
            pageOne.selectRegType(tcp.RegType);
            pageOne.setProfileName(tcp.ProfileName);
            pageOne.setDescription(tcp.ProfileName);
            pageOne.Submit();

            /*
             * Check if there is any error message
             * if error exists, exit the loop, update the TCP Object
             */
            String errorMessage = pageOne.getModalErrorMessage();
            if (errorMessage != null) {
                tNode.warning(errorMessage);
                tcp.setError(errorMessage);
                tcp.writeDataToExcel();
                return;
            }

            /*
             * Page2
             * if new TCP has to be added
             */
            AddCustomerTCP_pg2 pageTwo = AddCustomerTCP_pg2.init(tNode);
            pageTwo.fillAlltheTextField();
            Thread.sleep(Constants.THREAD_SLEEP_1000);
            pageTwo.submit();
            pageTwo.confirm();
            Thread.sleep(Constants.THREAD_SLEEP_1000);

            /**
             * If TCP is Successfully Created then set isCreated flag
             * Approve the TCP, set is approved
             * Write to DatSet Sheet
             */
            if (pageOne.assertTCPCreation(tcp)) {
                tNode.pass("Successfully Created the Customer TCP : " + tcp.ProfileName);
                tcp.setIsCreated();

                // approve Customer TCP
                Login.init(tNode).loginAsSuperAdmin(saCustTcpApprove);
                approveCustomerTCP(tcp, tNode);

            } else {
                tNode.fail("Fail to Create Customer TCP : " + tcp.ProfileName + ", Domain - " + tcp.DomainName + ", Category - " + tcp.CategoryName);
                Utils.captureScreen(tNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, tNode);
        }

    }
}
