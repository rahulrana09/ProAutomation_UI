package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.Wallet;
import framework.entity.Bank;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.bankMaster.AddBank_Page1;
import framework.pageObjects.bankMaster.AddServiceProviderBankAccounts1;
import framework.pageObjects.bankMaster.DeleteBank_Page1;
import framework.pageObjects.bankMaster.ModifyServiceProviderBankAccount_Pg1;
import framework.pageObjects.mfsProviderBankTypeMaster.ModifyDeleteBank_Page1;
import framework.pageObjects.mfsProviderWalletTypeMaster.AddWallet_Page1;
import framework.pageObjects.mfsProviderWalletTypeMaster.ModifyDeleteWallet_Page1;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.*;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by rahul.rana on 6/1/2017.
 */
public class CurrencyProviderMapping {
    private static ExtentTest pNode;
    private static SuperAdmin saMapWallet;
    private static WebDriver driver;


    public static CurrencyProviderMapping init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;
            driver = DriverFactory.getDriver();
            if (saMapWallet == null) {
                saMapWallet = DataFactory.getSuperAdminWithAccess("CAT_PREF");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return new CurrencyProviderMapping();
    }

    /**
     * Add Data to Add branch CSV for Band addition
     *
     * @throws InterruptedException
     */
    public static void writeDataToAddBranchCsv() throws InterruptedException {
        try {
            // Write the fields to CSV
            BufferedWriter out = new BufferedWriter(new FileWriter(FilePath.fileAddBranch, false));

            out.write("BranchCode" + "," +
                    "BranchName" + "\n" +
                    DataFactory.getRandomNumberAsString(6) + "," +
                    DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3) + "\n");

            out.newLine();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Bank Master > Add Bank
     *
     * @return
     * @throws Exception
     */
    public CurrencyProviderMapping addBank(Bank bank) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addBank: Adding Banks in system", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddBank_Page1 page1 = AddBank_Page1.init(pNode);

            page1.navAddBank();
            page1.selectProviderName(bank.ProviderName);
            page1.setBankName(bank.BankName);

            if (AppConfig.isBankIdRequired) {
                page1.setBankId(bank.BankID);
            }

            page1.selectBankType(bank.TrustType);

            if (AppConfig.isBankPoolAccountRequired && !bank.TrustType.equals(Constants.BANK_TYPE_LIQUIDATION)) {
                page1.setPoolAccountNumber(bank.PoolAccNum);
                page1.selectPoolAccountType(bank.PoolAccType);
                page1.selectCBSTypeV5(bank.CBSType);
            }

            if (bank.TrustType.equals(Constants.BANK_TYPE_LIQUIDATION)) {
                bank.setDefaultRoutingBankId(DataFactory.getTrustBankid(bank.ProviderName));
                page1.selectDefaultRoutingBank(bank.defaultRoutingBankId);
            }

            if (AppConfig.isBankBranchCSVUploadRequired && !bank.TrustType.equals(Constants.BANK_TYPE_LIQUIDATION)) {
                page1.uploadBranchCSV(bank.BranchFileCSV);
            }

            page1.clickOnSubmit();

            if (!ConfigInput.isAssert) {
                return this; // return as no further assertion is required
            }

            if (Assertion.isErrorMessageExists("bankName.alreay.Exist", "Bank Already Exist", pNode)) {
                pNode.info("Bank with Bank Name: " + bank.BankName + " already exists ");
                bank.setExisting(true);
                return this;
            }

            if (Assertion.isErrorMessageExists("Bank.validation.bank.Account", "Pool Account Exist", pNode)) {
                pNode.warning("Bank with Pool account num: " + bank.PoolAccNum + " already exists | please check the config input");
                bank.setExisting(true);
                return this;
            }

            if (AppConfig.isBankBranchCSVUploadRequired && !bank.TrustType.equals(Constants.BANK_TYPE_LIQUIDATION)) {
                Assertion.verifyActionMessageContain("Bank.branches.added.successfully", "Add Bank", pNode);
                bank.setCreated(true);
            } else {
                Assertion.verifyActionMessageContain("Bank.added.successfully", "Add Bank", pNode);
                bank.setCreated(true);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Associate banks with MFS provider
     *
     * @return
     * @throws Exception
     */
    public void initiateBanksCurrencyProviderMapping(String providerName) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("initiateBanksCurrencyProviderMapping:" + providerName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            List<String> trustBanks, banks;

            framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1 page1 = framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1.init(pNode);

            banks = DataFactory.getAllBankNamesLinkedToProvider(providerName);
            trustBanks = DataFactory.getTrustBankNameList(providerName);

            for (String trustBank : trustBanks) {
                page1.navMFSProviderBankTypeMaster();
                page1.selectProviderName(providerName);
                System.out.println("Trust bank: " + trustBank);
                page1.selectDefaultBankName(trustBank);

                for (int j = 0; j < banks.size(); j++) {
                    driver.findElement(By.xpath(".//*[text()='" + banks.get(j) + "']/../td/input")).click();
                }
                page1.clickSubmit();

                // Validation
                if (Assertion.isErrorInPage(pNode)) {
                    if (Assertion.verifyErrorMessageContain("Mapping.done.for.this.mfsProvider", "Bank Mapping Already Exist", pNode)) {
                        continue;
                    } else {
                        pNode.fail("Error in Mapping Page");
                        Utils.captureScreen(pNode);
                    }
                }

                driver.findElement(By.id("MfsBankMapping_AddBankMapping_checkAll")).click();
                driver.findElement(By.id("MfsBankMapping_AddBankMapping_button_submit")).click();

                Assertion.verifyActionMessageContain("Bank.Mpping.Added.Successfully", "Bank Mapping Done Successfully", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void initiateBanksCurrencyProviderMapping(Bank bank) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("initiateBanksCurrencyProviderMapping:" + bank.BankName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1 page1 = framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1.init(pNode);

            List<String> banks = DataFactory.getAllBankNamesLinkedToProvider(bank.ProviderName);

            page1.navMFSProviderBankTypeMaster();
            page1.selectProviderName(bank.ProviderName);
            page1.selectDefaultBankName(bank.BankName);

            for (int j = 0; j < banks.size(); j++) {
                driver.findElement(By.xpath(".//*[text()='" + banks.get(j) + "']/../td/input")).click();
            }
            page1.clickSubmit();

            // Validation
            if (Assertion.isErrorInPage(pNode)) {
                if (Assertion.verifyErrorMessageContain("Mapping.done.for.this.mfsProvider", "Bank Mapping Already Exist", pNode)) {
                    return;
                } else {
                    pNode.fail("Error in Mapping Page");
                    Utils.captureScreen(pNode);
                }
            }

            driver.findElement(By.id("MfsBankMapping_AddBankMapping_checkAll")).click();
            driver.findElement(By.id("MfsBankMapping_AddBankMapping_button_submit")).click();

            Assertion.verifyActionMessageContain("Bank.Mpping.Added.Successfully", "Bank Mapping Done Successfully", pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Modify Association of banks with MFS provider
     *
     * @return
     * @throws Exception
     */
    public void modifyBankCurrencyProviderMapping(String provider) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("modifyBankCurrencyProviderMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            List<String> currencyProviders, banks;

            ModifyDeleteBank_Page1 page1 = new ModifyDeleteBank_Page1(pNode);

            banks = DataFactory.getAllBankNamesLinkedToProvider(provider);

            page1.navigateToModifyDeleteBank();
            driver.findElement(By.xpath(".//*[text()='" + provider + "']")).click();
            page1.initiateModify();

            for (String bank : banks) {
                page1.checkBank(bank);
            }

            page1.initiateModify2();
            page1.checkAllServices();
            page1.submitModify();

            // Validation
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Bank.Mpping.Modified.Successfully", "Modify Bank-Provider Mapping", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Associate wallets with MFS provider
     *
     * @return
     * @throws Exception
     */
    public void initiateWalletCurrencyProviderMapping(String provider) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("initiateWalletCurrencyProviderMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            List<Wallet> walletList;
            List<String> currencyProviders;

            AddWallet_Page1 page1 = AddWallet_Page1.init(pNode);
            page1.navigateToMFSProviderWalletTypeMaster();
            walletList = DataFactory.getAllWallet();

            page1.selectProviderName(provider);

            for (Wallet wallet : walletList) {
                page1.selectDefaultWallet(wallet.WalletId);

                // select the checkboxes
                for (Wallet chkWallet : walletList) {
                    driver.findElement(By.xpath("//input[@value='" + chkWallet.WalletId + "' and @type='checkbox']")).click();
                }
                page1.Submit.click();

                if (Assertion.isErrorInPage(pNode)) {
                    String errorMsg = Assertion.getErrorMessage();
                    if (errorMsg.contains(MessageReader.getMessage("Mapping.done.for.this.mfsProvider", null))) {
                        pNode.pass("Mapping is Already done for Provider : " + provider);
                        continue;
                    } else {
                        pNode.fail("ERROR: " + errorMsg);
                    }
                }

                driver.findElement(By.id("MfsType_AddWallet_checkAll")).click();
                driver.findElement(By.id("MfsType_AddWallet_button_submit")).click();

                Assertion.verifyActionMessageContain("Wallet.Mpping.Added.Successfully", "Add Wallet Mapping", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Modify Wallet Association with MFS provider
     *
     * @return
     * @throws Exception
     */
    public void modifyWalletCurrencyProviderMapping(String providerName) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("modifyWalletCurrencyProviderMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);
            List<Wallet> wallets = DataFactory.getAllWallet();

            page1.navigateToModifyDeleteWallet();
            page1.selectProvider(providerName);
            page1.clickOnmodify1();
            page1.selectDefaultWallet(DataFactory.getDefaultWallet().WalletName);
            page1.selectPayInstrumentToUpdateMapping(wallets);
            page1.clickOnModify2();
            page1.checkAllServices(); // this can be parametrized in future
            page1.submitModification();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Wallet.Mpping.Modified.Successfully", "Verify Updating Wallet Mapping for Provider: " + providerName, pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Map Wallet Preferences
     * Auto Login included
     *
     * @deprecated
     */
    public void mapSpecialWalletPreference(String prefType, String categoryCode, String gradeName) throws Exception {
        Markup m = MarkupHelper.createLabel("mapSpecialWalletPreference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        if (categoryCode.equals(Constants.ENTERPRISE))
            return;

        try {
            /**
             * Map Wallet Preferences for the specific Grade
             * Map the default wallet as primary
             * Map the special Wallets only if the IS_COMMISSION_WALLET_REQUIRED is set ti true
             * Map the special Wallets only if the IS_REMITTANCE_WALLET_REQUIRED is set ti true
             *
             * known that Commission Wallet ID is '13'
             * known that Remittance Wallet ID is '14'
             */
            List<String> providerNameList = DataFactory.getAllProviderNames();
            String walletId = (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ?
                    DataFactory.getWalletUsingAutomationCode(Wallets.REMMITANCE).WalletId :
                    DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletId;

            String regId = (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ? "NO_KYC" : "FULL_KYC";

            if ((AppConfig.isCommissionWalletRequired &&
                    !categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                    ||
                    (AppConfig.isRemittanceRequired &&
                            categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
            ) {

                String domainName = DataFactory.getDomainName(categoryCode);
                String categoryName = DataFactory.getCategoryName(categoryCode);
                List<String> providerList = DataFactory.getAllProviderNames();

                WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);
                Login.init(pNode).loginAsSuperAdmin(saMapWallet);
                page.navMapWalletPreferences();
                page.selectPreferenceTypeVyVal(prefType);
                page.selectDomainName(domainName);
                page.selectCategoryName(categoryName);
                page.selectRegTypeByVal(regId);

                int counter = 0;

                //Configure Wallet Preferences
                for (String provider : providerList) {
                    page.configurePaymentInstrumentMapping(counter, provider, gradeName, walletId, false, null, true);
                    counter++;
                }

                Thread.sleep(2000);
                page.clickNext();
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                        "Map Wallet Preferences", pNode);

                Thread.sleep(3000);

            } else {
                pNode.info("Special Wallet is not required to be mapped, Skipping Wallet preferences");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void mapWalletPreferencesUserRegistration(User user, String... payId) throws Exception {
        if (user.CategoryCode.equals(Constants.ENTERPRISE) || user.CategoryCode.equals(Constants.INT_PARTNER))
            return;

        Markup m = MarkupHelper.createLabel("mapWalletPreferencesUserRegistration", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {

            String[] walletIds = (payId.length > 0) ? payId : new String[]{DataFactory.getDefaultWallet().WalletId};

            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);
            String domainName = DataFactory.getDomainName(user.CategoryCode);
            String categoryName = DataFactory.getCategoryName(user.CategoryCode);
            List<String> providerList = DataFactory.getAllProviderNames();

            // check if wallet mapping is required
            boolean isWalletMapped = true;
            for (String provider : providerList) {
                String providerId = DataFactory.getProviderId(provider);
                for (String walletId : walletIds) {
                    String roleName = user.hasPayInstTypeLinked(DataFactory.getWalletName(walletId), provider).ProfileName;

                    // make sure that wallet preference is mapped to the role created as part of Base Setup.
                    Boolean isMapped = MobiquityGUIQueries
                            .isWalletMappingForUserRegistrationWithSpecificRole(
                                    providerId,
                                    user.CategoryCode,
                                    user.RegistrationType,
                                    walletId,
                                    DataFactory.getMobileRoleCode(roleName));

                    isWalletMapped = isWalletMapped && isMapped;
                }
            }

            // if Wallet Preference are already mapped
            if (isWalletMapped) {
                pNode.info("wallet Preference is already mapped!");
                return;
            }


            Login.init(pNode)
                    .loginAsSuperAdmin(saMapWallet);

            page.navMapWalletPreferences();
            if (ConfigInput.isCoreRelease) {
                page.selectPreferenceTypeVyVal(Constants.PREFERENCE_TYPE_USER_REGISTRATION);
            }
            page.selectDomainName(domainName);
            page.selectCategoryName(categoryName);
            page.selectRegTypeByVal(user.RegistrationType);

            int counter = 0;

            // Map Wallet Preferences
            for (String provider : providerList) {
                for (String walletId : walletIds) {
                    // get Role name
                    String walletName = DataFactory.getWalletName(walletId);

                    // check if user object has any specific mobile role associatde
                    String roleName1 = user.hasPayInstTypeLinked(walletName, provider).ProfileName;
                    page.configurePaymentInstrumentMapping(counter, provider, user.GradeName, walletId, false, roleName1, true);

                    counter++;
                }
            }

            page.clickNext();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                        "Map Wallet Preferences", pNode);
            }

            Thread.sleep(3000);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void linkPrimaryBankWalletPreference(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("linkPrimaryBankWalletPreference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);
            String domainName = DataFactory.getDomainName(user.CategoryCode);
            String categoryName = DataFactory.getCategoryName(user.CategoryCode);
            List<String> providerList = DataFactory.getAllProviderNames();

            // check if Bank Linking is required
            boolean isWalletMapped = true;
            for (String provider : providerList) {
                String providerId = DataFactory.getProviderId(provider);
                for (Bank bank : DataFactory.getAllBanksLinkedToProviderForBankLinkMapping(provider)) {
                    String bankId = DataFactory.getBankId(bank.BankName);

                    // check if user object has any specific mobile role associatde
                    String roleName = user.hasPayInstTypeLinked(bank.BankName, provider).ProfileName;

                    Boolean isMapped = MobiquityGUIQueries
                            .isWalletMappingForBankLinkingWithSpecificRole(
                                    providerId,
                                    user.CategoryCode,
                                    user.RegistrationType,
                                    bankId,
                                    DataFactory.getMobileRoleCode(roleName));

                    isWalletMapped = isWalletMapped && isMapped;
                }
            }

            // if Wallet Preference are already mapped
            if (isWalletMapped) {
                pNode.info("Bank Linking is already done!");
                return;
            }

            Login.init(pNode)
                    .loginAsSuperAdmin(saMapWallet);

            page.navMapWalletPreferences();
            page.selectPreferenceTypeVyVal(Constants.PREFERENCE_TYPE_BANK_ACCOUNT_LINKING);
            page.selectDomainName(domainName);
            page.selectCategoryName(categoryName);
            page.selectRegTypeByVal(user.RegistrationType);

            int counter = 0;

            // Map Wallet Preferences
            for (String provider : providerList) {
                for (Bank bank : DataFactory.getAllBanksLinkedToProviderForBankLinkMapping(provider)) {
                    String bankId = DataFactory.getBankId(bank.BankName);
                    // check if user object has any specific mobile role associated, this facilitates to add user defined mobile role
                    String roleName1 = user.hasPayInstTypeLinked(bank.BankName, provider).ProfileName;
                    page.configurePaymentInstrumentMapping(counter, provider, user.GradeName, bankId, false, roleName1, false);
                    counter++;
                }
            }

            page.clickNext();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                        "Map Wallet Preferences", pNode);
            }

            Thread.sleep(3000);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param providerName
     * @throws Exception
     * @deprecated
     */
    public void mapAllWalletPreferences(String providerName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mapAllWalletPreferences", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);

            page1.navigateToModifyDeleteWallet();
            page1.selectProviderRadio(providerName);
            page1.clickOnmodify1();
            page1.checkAllWalletType();
            page1.clickOnModify2();
            page1.clickOnCheckAll();
            page1.clickSubmit();

            if (ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("Wallet.Mpping.Modified.Successfully", "Update Wallet Mapping", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Configure Wallet Preference for primary wallet
     *
     * @param user
     * @throws Exception
     */
    public void mapPrimaryWalletPreference(User user) throws Exception {
        Markup m = MarkupHelper.createLabel("mapPrimaryWalletPreference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            Wallet wallet1 = GlobalData.defaultWallet;
            String domainName = DataFactory.getDomainName(user.CategoryCode);
            String categoryName = DataFactory.getCategoryName(user.CategoryCode);
            List<String> providerNameList = DataFactory.getAllProviderNames();

            // check if wallet mapping is required
            boolean isWalletMapped = true;
            for (String provider : providerNameList) {
                String providerId = DataFactory.getProviderId(provider);

                // make sure that wallet preference is mapped to the role created as part of Base Setup.
                String roleName = DataFactory.getMobileGroupRole(user.DomainName, user.CategoryName, user.GradeName, provider, wallet1.WalletName).ProfileName;
                Boolean isMapped = MobiquityGUIQueries
                        .isWalletMappingForUserRegistrationWithSpecificRole(
                                providerId,
                                user.CategoryCode,
                                user.RegistrationType,
                                wallet1.WalletId,
                                DataFactory.getMobileRoleCode(roleName));

                isWalletMapped = isWalletMapped && isMapped;
            }

            // if Wallet Preference are already mapped
            if (isWalletMapped) {
                pNode.info("wallet Preference is already mapped!");
                return;
            }

            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);

            Login.init(pNode)
                    .loginAsSuperAdmin(saMapWallet);

            page.navMapWalletPreferences();
            if (ConfigInput.isCoreRelease) {
                page.selectPreferenceTypeVyVal(Constants.PREFERENCE_TYPE_USER_REGISTRATION);
            }
            page.selectDomainName(domainName);
            page.selectCategoryName(categoryName);
            page.selectRegTypeByVal(user.RegistrationType);

            int counter = 0;

            //Configure Wallet Preferences
            boolean setPrimary = false;
            for (String provider : providerNameList) {
                if (provider.contains(DataFactory.getDefaultProvider().ProviderName))
                    setPrimary = true;

                String roleName1 = user.hasPayInstTypeLinked(wallet1.WalletName, provider).ProfileName;
                page.configurePaymentInstrumentMapping(counter, provider, user.GradeName, wallet1.WalletId, setPrimary, roleName1, true);
                counter++;
            }

            Thread.sleep(2000);
            page.clickNext();
            Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                    "Map Wallet Preferences", pNode);

            Thread.sleep(3000);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void viewBank(Bank bank) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("View Bank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            if (ConfigInput.isConfirm) {

            }
            if (ConfigInput.isAssert) {

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /**
     * *******************    N O T  F O R  D A T A  B U I L D E R    *******************#
     * ###############  S P E C I F I C  M E T H O D S  F O R  T E S T C A S E S  ################
     */


    public boolean verifyBankAlreadyPresent() throws Exception {
        if (Assertion.isErrorInPage(pNode)) {
            if (Assertion.verifyErrorMessageContain("bankName.alreay.Exist", "Bank Already Exist", pNode)) {
                pNode.skip("Skipping case..As Bank already exist");
                return true;
            }
        }
        return false;
    }


    /**
     * addParticularBank : To add particular Bank
     *
     * @param provider  : MFS provider
     * @param bankName  : Bank Name
     * @param accountNo : Account Number
     * @param acType
     * @throws NoSuchElementException
     */
    public void addParticularBank(String provider, String bankName, String accountNo, String acType) throws NoSuchElementException, IOException {
        try {
            Markup m = MarkupHelper.createLabel("addParticularBank", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddBank_Page1 page1 = new AddBank_Page1(pNode);

            page1.navAddBank();
            page1.selectProviderName(provider);
            page1.setBankName(bankName);
            page1.setPoolAccountNumber(accountNo);
            page1.selectPoolAccountType(acType);
            page1.selectCBSType(Constants.BANK_CBS_SPARROW);
            page1.clickAddBankSubmit();

            if (Assertion.isErrorInPage(pNode)) {
                if (Assertion.verifyErrorMessageContain("bankName.alreay.Exist", "Bank Already Exist", pNode)) {
                    pNode.skip("Skipping case..As Bank already exist");
                }
            } else {
                Assertion.verifyActionMessageContain("Bank.added.successfully", "Add Bank", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyServiceProviderBankAccount(String bankName, String acNo) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("modifyServiceProviderBankAccount", ExtentColor.BLUE);
            pNode.info(m);

            ModifyServiceProviderBankAccount_Pg1 page = new ModifyServiceProviderBankAccount_Pg1(pNode);

            page.navModifyServiceProviderBankAc();
            page.selectBank(bankName);
            page.clickSubmit();
            page.clickOnModify();
            page.clickOnConfirm();

            Assertion.verifyActionMessageContain("bank.account.modify.success", "Modify Service Provider Bank A/c", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void initiateMFSProviderWalletMapping(String providerName, String walletName) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("initiateMFSProviderWalletMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddWallet_Page1 page1 = AddWallet_Page1.init(pNode);

            page1.navigateToMFSProviderWalletTypeMaster();
            page1.selectProviderName(providerName);
            page1.selectDefaultWalletByText(walletName);
            page1.checkWalletName(walletName);

            if (ConfigInput.isConfirm)
                page1.Submit.click();

            if (ConfigInput.isAssert) {
                if (Assertion.isErrorInPage(pNode)) {
                    String errorMsg = Assertion.getErrorMessage();
                    if (errorMsg.contains(MessageReader.getMessage("Mapping.done.for.this.mfsProvider", null))) {
                        pNode.pass("Mapping is Already done for Provider : " + providerName);
                    } else {
                        pNode.fail("ERROR: " + errorMsg);
                    }
                } else {
                    driver.findElement(By.id("MfsType_AddWallet_checkAll")).click();
                    driver.findElement(By.id("MfsType_AddWallet_button_submit")).click();

                    Assertion.verifyActionMessageContain("Wallet.Mpping.Added.Successfully", "Add Wallet Mapping", pNode);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void initiateMFSProviderBankMapping(String providerName, String bankName) throws Exception {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("initiateMFSProviderBankMapping", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1 page1 = framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1.init(pNode);

            page1.navMFSProviderBankTypeMaster();
            page1.selectProviderName(providerName);
            page1.selectDefaultBankName(DataFactory.getDefaultBankNameForDefaultProvider());

            driver.findElement(By.xpath(".//*[text()='" + bankName.toUpperCase() + "']/../td/input")).click();
            page1.clickSubmit();
            if (Assertion.isErrorInPage(pNode)) {
                if (Assertion.verifyErrorMessageContain("Mapping.done.for.this.mfsProvider", "Bank Already Exist", pNode)) {
                    pNode.pass("Mapping already done");
                }
            } else {
                driver.findElement(By.id("MfsBankMapping_AddBankMapping_checkAll")).click();
                driver.findElement(By.id("MfsBankMapping_AddBankMapping_button_submit")).click();

                Assertion.verifyActionMessageContain("Bank.added.successfully", "Add Bank", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyMFSProviderWalletMapping(String providerName, String walletType, boolean... uncheckAll) throws Exception {
        boolean uncheckAllService = (uncheckAll.length > 0) ? uncheckAll[0] : false;
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("modifyMFSProviderWalletMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);

            page1.navigateToModifyDeleteWallet();
            page1.selectProviderRadio(providerName);
            page1.clickOnmodify1();

            WebElement walletCheck = driver.findElement(By.xpath("//td[contains(text(),'" + walletType + "')]//following::input[1]"));
            if (!walletCheck.isSelected() && walletCheck.isEnabled()) {
                walletCheck.click();
                pNode.info("Select the Pay Instrument Type: " + walletType);
            }

            page1.clickOnModify2();
            if (uncheckAllService) {
                page1.unCheckAllServices();
                Utils.captureScreen(pNode);
            } else {
                page1.checkAllServices();
            }
            page1.submitModification();

            if (ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("Wallet.Mpping.Modified.Successfully", "Update Wallet Mapping", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void modifyServiceProviderBankMapping(String providerName, String bankName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyServiceProviderBankMapping", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteBank_Page1 page1 = new ModifyDeleteBank_Page1(pNode);

            page1.navigateToModifyDeleteBank();
            DriverFactory.getDriver().findElement(By.xpath(".//*[text()='" + providerName + "']")).click();
            page1.initiateModify();

            WebElement bankCheck = DriverFactory.getDriver().findElement(By.xpath("//td[contains(text(),'" + bankName + "')]//following::input[1]"));
            if (!bankCheck.isSelected() && bankCheck.isEnabled()) {
                bankCheck.click();
            }
            page1.initiateModify2();
            if (ConfigInput.isConfirm) {
                page1.checkAllServices();
                page1.submitModify();
            }

            if (ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("Bank.Mpping.Modified.Successfully", "Modify Bank-Provider Mapping", pNode);


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void deleteServiceProviderBankMappingNew(String providerID, String bankName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteServiceProviderBankMapping", ExtentColor.ORANGE);
            pNode.info(m);

            ModifyDeleteBank_Page1 page1 = new ModifyDeleteBank_Page1(pNode);

            page1.navigateToModifyDeleteBank();
            page1.selectProviderRadio(providerID);
            page1.clickDeleteButton();

            WebElement bankCheck = DriverFactory.getDriver().findElement(By.xpath("//td[contains(text(),'" + bankName + "')]//following::input[1]"));
            if (!bankCheck.isSelected() && bankCheck.isEnabled()) {
                bankCheck.click();
            }

            page1.clickConfirmDeleteButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyErrorMessageContain("provider.grouprole.bank.mappingexists", "Group role it is associated", pNode, bankName);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void deleteServiceProviderBankMapping(String providerID, String bankName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteServiceProviderBankMapping", ExtentColor.ORANGE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteBank_Page1 page1 = new ModifyDeleteBank_Page1(pNode);

            page1.navigateToModifyDeleteBank();
            page1.selectProviderRadio(providerID);
            page1.clickDeleteButton();

            WebElement bankCheck = DriverFactory.getDriver().findElement(By.xpath("//td[contains(text(),'" + bankName + "')]//following::input[1]"));
            if (!bankCheck.isSelected() && bankCheck.isEnabled()) {
                bankCheck.click();
                pNode.info("Clicked on Check Box having Bank Name :" + bankName);
            }

            page1.clickConfirmDeleteButton();

            //if (Assertion.isErrorInPage(pNode)) {
            if (Assertion.isErrorInPage(pNode)) {
                if (Assertion.verifyErrorMessageContain("Bank.associated.with.user.or.associates.with.TCP", "Bank Associated With TCP/User", pNode)) {
                    pNode.info("Bank is Associated with User or TCP.. Not able to Delete");
                    Utils.captureScreen(pNode);
                }
            } else {
                Assertion.verifyActionMessageContain("Bank.Mpping.Deleted.Successfully", "Wallet mappin deleted success", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void deleteMFSProviderWalletMapping(String providerName, String walletName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("deleteMFSProviderWalletMapping", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);

            page1.navigateToModifyDeleteWallet();
            page1.selectProviderRadio(providerName);
            page1.clickDeleteButton();
            WebElement walletCheck = DriverFactory.getDriver().findElement(By
                    .xpath("//td[contains(text(),'" + walletName + "')]//following::input[1]"));

            if (!walletCheck.isSelected() && walletCheck.isEnabled()) {
                walletCheck.click();
                pNode.info("Select the Pay Instrument Type: " + walletName);
            }
            page1.clickConfirmDeleteButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Wallet.Mpping.Deleted.Successfully", "Wallet mappin deleted success", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * T O  D E L E T E  B A N K
     *
     * @param bankName--> BANK NAME TO DELETE
     * @throws Exception --> THROWS EXCEPTION IF THERE
     */
    public CurrencyProviderMapping deleteBank(String bankName) throws Exception {
        Markup m = MarkupHelper.createLabel("deleteBank", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        DeleteBank_Page1 page = new DeleteBank_Page1(pNode);
        List<String> bankList = page.navDeleteBank()
                .getBankOptionsfromSelect();

        if (bankList.contains(bankName.toUpperCase())) {
            page.selectBankForDeletion(bankName)
                    .clickSubmitPg1()
                    .clickSubmitPg2();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Bank.Deleted.Successfully", "Delete Bank without Pool account Detail", pNode);
            }
        } else {
            Utils.captureScreen(pNode);
            pNode.info("Bank " + bankName + " cannot be deleted as it is associated with Pool Account number. ");
        }
        return this;
    }

    public CurrencyProviderMapping deleteBank(Bank bank) throws Exception {
        Markup m = MarkupHelper.createLabel("deleteBank", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            DeleteBank_Page1 page = new DeleteBank_Page1(pNode);
            page.navDeleteBank();
            page.selectBankForDeletion(bank.BankName)
                    .clickSubmitPg1()
                    .clickSubmitPg2();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("Bank.Deleted.Successfully", "Delete Bank without Pool account Detail", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

        return this;
    }


    public void addServiceProviderBankAccount(String bankName, String acNo) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("addServiceProviderBankAccount", ExtentColor.BLUE);
            pNode.info(m);
            AddServiceProviderBankAccounts1 page = new AddServiceProviderBankAccounts1(pNode);

            page.navAddBank();
            page.setBankName(bankName);
            page.clickOnBankSubmit();
            page.setPoolAccountNumber(acNo);
            page.clickOnBankSubmit2();

            if (ConfigInput.isConfirm)
                page.clickOnConfirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("bank.account.addition.success", "Add Bank", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


    /**
     * @param bankName Name of Bank
     * @param acNo     Account no
     * @param button   Button name
     * @return current instance
     */
    public CurrencyProviderMapping checkAddServiceProviderBankAcPageButtons(String bankName, String acNo, String button) throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("checkAddServiceProviderBankAcPageButtons", ExtentColor.GREEN);
            pNode.info(m);

            AddServiceProviderBankAccounts1 page = new AddServiceProviderBankAccounts1(pNode);

            addServiceProviderBankAccount(bankName, acNo);

            switch (button) {

                case Button.BACK:
                    page.clickOnBack();
                    if (page.isSubmitButtonVisible()) {
                        pNode.pass("Successfully navigate to previous page.");
                        Utils.captureScreen(pNode);
                    }
                    break;


                case Button.ADD_MORE_BANK_AC:
                    page.clickOnBack();
                    page.clickOnAddMoreBankAcButton();
                    Assertion.verifyEqual(page.isBankAccountNo2TboxVisible(), true, "Verify Add More Button", pNode);
                    if (page.isBankAccountNo2TboxVisible()) {
                        pNode.pass("Extra Row is visible after clicking on Add More.");
                    }
                    break;


                case Button.REMOVE:
                    page.clickOnBack();
                    page.clickOnRemoveBankAcButton();
                    Utils.captureScreen(pNode);
                    break;

                case Button.REMV_MORE_BANK_AC:
                    page.clickOnBack();
                    page.clickOnAddMoreBankAcButton();
                    page.clickOnRemoveBankAcButton();
                    break;


                default:
                    pNode.info("No button clicked...");
            }


        } catch (NoSuchElementException ne) {
            Assertion.raiseExceptionAndContinue(ne, pNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * initiateMFSProviderWalletMapping
     *
     * @throws NoSuchElementException
     */
    public void verifyCheckBoxFunctionality() throws IOException {
        try {
            WebDriver driver = DriverFactory.getDriver();
            Markup m = MarkupHelper.createLabel("verifyCheckBoxFunctionality", ExtentColor.GREEN);
            pNode.info(m); // Method Start Marker
            String walletName = DataFactory.getWalletName(Constants.NORMAL_WALLET);
            String providerName = DataFactory.getDefaultProvider().ProviderName;

            AddWallet_Page1 page1 = AddWallet_Page1.init(pNode);

            page1.navigateToMFSProviderWalletTypeMaster();
            page1.selectProviderName(providerName);
            page1.selectDefaultWalletByText(walletName);

            List<WebElement> checkBoxes = driver.findElements(By.name("check"));
            int i = 4;
            for (WebElement check : checkBoxes) {
                Utils.putThreadSleep(NumberConstants.SLEEP_1000);
                check.click();
                String walletType = driver.findElement(By.xpath(".//*[@id='MfsType_input']/table/tbody/tr[" + i + "]/td[1]")).getText();
                if (check.isSelected()) {
                    pNode.pass(walletType + " Check Box is clickable");
                    check.click();
                } else {
                    pNode.fail(walletType + " Check Box is not clickable");
                }
                i++;

            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * initiateMFSProviderWalletMapping
     *
     * @throws NoSuchElementException
     */
    public void verifyServicesForWallet(String walletTypeID) throws IOException {
        try {

            Markup m = MarkupHelper.createLabel("verifyServicesForWallet", ExtentColor.GREEN);
            pNode.info(m); // Method Start Marker

            List<String> dbServices = MobiquityGUIQueries.getAllServicesAgainstWallet(walletTypeID, Constants.PAYINST_WALLET_CONST);
            List<WebElement> guiServices = driver.findElements(By.xpath(".//input[contains(@value,'" + walletTypeID + ":')][@id='MfsModifyType_displayServicesForUpdate_checkbox']/preceding::td[1]"));
            WebElement walletName = driver.findElement(By.xpath(".//td[contains(text(),'" + DataFactory.getWalletName(walletTypeID) + "')]"));

            for (String dbService : dbServices) {
                for (WebElement guiService : guiServices) {
                    if (dbService.equalsIgnoreCase(guiService.getText())) {
                        Utils.scrollToAnElement(guiService);
                        pNode.pass("'" + guiService.getText() + "'" + " Service Verified Successfully.");
                    }
                }
                Utils.captureScreen(pNode);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void verifyAllWalletTypes(List<WebElement> wallets) throws Exception {

        List<String> expectedWallets = MobiquityGUIQueries.getAllWallet();
        for (String expWallet : expectedWallets) {
            for (WebElement wallet : wallets) {
                if (expWallet.equalsIgnoreCase(wallet.getText())) {
                    pNode.pass("Wallet Type '" + expWallet + "' Verified Successfully");
                }
            }
        }
        Utils.captureScreen(pNode);
    }

    public void associateServicesWithWallet(String walletName, String providerName) throws Exception {

        String walletID = MobiquityGUIQueries.getWalletID(walletName);
        ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);
        page1.navigateToModifyDeleteWallet();
        page1.selectProviderRadio(providerName);
        page1.clickOnmodify1();
        Utils.captureScreen(pNode);
        page1.selectDefaultWalletByValue(DataFactory.getDefaultWallet().WalletId);
        page1.checkDefaultWalletTypeCheckBox(walletID);
        Utils.captureScreen(pNode);
        page1.clickOnModify2();
        List<WebElement> checkBoxes = driver.findElements(By.xpath(".//input[contains(@value,'" + walletID + ":')][@id='MfsModifyType_displayServicesForUpdate_checkbox']"));
        for (int i = 0; i <= 5; i++) {
            //Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            Utils.scrollToAnElement(checkBoxes.get(i));
            checkBoxes.get(i).click();
            if (checkBoxes.get(i).isSelected()) {
            } else {
                checkBoxes.get(i).click();
            }
        }
        Utils.captureScreen(pNode);
        page1.clickSubmit();
        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("Wallet.Mpping.Modified.Successfully", "Verify Modification Message", pNode);
            pNode.info("Services are Associated with Wallet : " + walletName);
        }
    }

    public void deAssociateServicesWithWallet(String walletName, String serviceID, String providerName) throws Exception {
        String walletID = MobiquityGUIQueries.getWalletID(walletName);
        ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);
        page1.navigateToModifyDeleteWallet();
        page1.selectProviderRadio(providerName);
        page1.clickOnmodify1();
        Utils.captureScreen(pNode);
        page1.selectDefaultWalletByValue(DataFactory.getDefaultWallet().WalletId);
        page1.checkDefaultWalletTypeCheckBox(walletID);
        Utils.captureScreen(pNode);
        page1.clickOnModify2();
        List<WebElement> guiServices = driver.findElements(By.xpath(".//input[contains(@value,'" + walletID + ":')][@id='MfsModifyType_displayServicesForUpdate_checkbox']"));
        for (int i = 0; i <= 2; i++) {
            Utils.scrollToAnElement(guiServices.get(i));
            Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            if (guiServices.get(i).isSelected()) {
                guiServices.get(i).click();
            }
        }
        Utils.captureScreen(pNode);
        page1.clickSubmit();
        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("Wallet.Mpping.Modified.Successfully", "Verify Modification Message", pNode);
            pNode.info("Service is De-Associated with Wallet : " + walletName);
        }

    }


    public void mapSpecialWalletPreference(String categoryCode, String gradeName) throws Exception {
        Markup m = MarkupHelper.createLabel("mapSpecialWalletPreference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            /**
             * Map Wallet Preferences for the specific Grade
             * Map the default wallet as primary
             * Map the special Wallets only if the IS_COMMISSION_WALLET_REQUIRED is set ti true
             * Map the special Wallets only if the IS_REMITTANCE_WALLET_REQUIRED is set ti true
             *
             * known that Commission Wallet ID is '13'
             * known that Remittance Wallet ID is '14'
             */
            List<String> providerNameList = DataFactory.getAllProviderNames();
            if (!categoryCode.equalsIgnoreCase(Constants.ENTERPRISE)) {
                String walletId = (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ?
                        DataFactory.getWalletUsingAutomationCode(Wallets.REMMITANCE).WalletId :
                        DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletId;

                String regId = (categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) ? "NO_KYC" : "FULL_KYC";

                if ((AppConfig.isCommissionWalletRequired &&
                        !categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                        ||
                        (AppConfig.isRemittanceRequired &&
                                categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                ) {

                    String domainName = DataFactory.getDomainName(categoryCode);
                    String categoryName = DataFactory.getCategoryName(categoryCode);
                    List<String> providerList = DataFactory.getAllProviderNames();

                    WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);

                    Login.init(pNode)
                            .loginAsSuperAdmin(saMapWallet);

                    page.navMapWalletPreferences();

                    page.selectPreferenceType(Constants.PREFERENCE_TYPE_USER_REGISTRATION);
                    page.selectDomainName(domainName);
                    page.selectCategoryName(categoryName);
                    page.selectRegTypeByVal(regId);

                    int counter = 0;

                    //Configure Wallet Preferences
                    for (String provider : providerList) {
                        page.configurePaymentInstrumentMapping(counter, provider, gradeName, walletId, false, null, true);
                        counter++;
                    }

                    Thread.sleep(2000);
                    page.clickNext();
                    Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                            "Map Wallet Preferences", pNode);

                    Thread.sleep(3000);
                }

            } else {
                pNode.info("Special Wallet is not required to be mapped, Skipping Wallet preferences");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void mapSVCWalletPreference(User user, String bankId) throws Exception {
        Markup m = MarkupHelper.createLabel("mapSVCWalletPreference", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            String walletId = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId;
            String regId = "NO_KYC";

            List<String> providerList = DataFactory.getAllProviderNames();

            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);

            Login.init(pNode)
                    .loginAsSuperAdmin(saMapWallet);

            page.navMapWalletPreferences();

            page.selectPreferenceType(Constants.PREFERENCE_TYPE_SAVING_CLUB_CREATION);
            page.selectRegTypeByVal(regId);

            int counter = 0;

            //Configure Wallet Preferences
            for (String provider : providerList) {
                if (AppConfig.isBankMandatoryForSavingsClub) {
                    String bankName = DataFactory.getBankName(bankId);
                    String roleName = user.hasPayInstTypeLinked(bankName, provider).ProfileName;
                    counter = page.configureSVAWalletPref(counter, provider, user.GradeName, bankId, false, true, roleName);
                    counter++;
                }

                // get wallet mapped
                String walletName = DataFactory.getWalletName(walletId);
                String roleName = user.hasPayInstTypeLinked(walletName, provider).ProfileName;
                counter = page.configureSVAWalletPref(counter, provider, user.GradeName, walletId, false, false, roleName);
                counter++;
            }

            Thread.sleep(Constants.MAX_WAIT_TIME);
            page.clickNextForSavingsClub();
            Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                    "Map Wallet Preferences", pNode);

            Thread.sleep(3000);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void addBankAndAccountDetails(Bank bank) throws Exception {
        Markup m = MarkupHelper.createLabel("addBankAndAccountDetails", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker

        try {
            // login as superadmin with Add bank Rights
            Login.init(pNode).loginAsSuperAdmin("BANK_ADD");

            // Add Bank
            CurrencyProviderMapping.init(pNode).addBank(bank);

            // bank is created for the first time and accounts need to be mapped
            if (bank.getCreated() && bank.TrustType.equalsIgnoreCase(Constants.BANK_TYPE_TRUST)) {
                Login.init(pNode)
                        .loginAsSuperAdmin("MFSBTM01");
                CurrencyProviderMapping.init(pNode)
                        .initiateBanksCurrencyProviderMapping(bank);

                Login.init(pNode)
                        .loginAsSuperAdmin("MFSBMD");

                CurrencyProviderMapping.init(pNode).
                        modifyBankCurrencyProviderMapping(bank.ProviderName);

                Login.init(pNode)
                        .loginAsSuperAdmin("OPT_BANK_ACC_ADD");

                CurrencyProviderMapping.init(pNode)
                        .addServiceProviderBankAccount(bank.BankName, DataFactory.getRandomNumberAsString(9));
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

}
