package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.entity.WalletPreference;
import framework.features.common.Login;
import framework.pageObjects.preferences.NotificationConfiguration_pg1;
import framework.pageObjects.preferences.SystemPreferences_Page1;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by navin.pramanik on 9/11/2017.
 */
public class Preferences {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static SuperAdmin saMapWallet;
    String value;

    public static Preferences init(ExtentTest t1) throws Exception {

        try {
            pNode = t1;
            driver = DriverFactory.getDriver();
            saMapWallet = DataFactory.getSuperAdminWithAccess("CAT_PREF");
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
        return new Preferences();
    }


    public Preferences configureWalletPreferences(WalletPreference walletPref) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("configureWalletPreferences", ExtentColor.ORANGE);
            pNode.info(m);

            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);

            page.navMapWalletPreferences();
            if (ConfigInput.isCoreRelease) {
                page.selectPreferenceType(walletPref.preferenceType);
            }
            page.selectDomainName(walletPref.DomainName);
            page.selectCategoryName(walletPref.CategoryName);
            page.selectRegTypeByVal(walletPref.RegType);

            page.selectProvider(walletPref.Provider);
            page.selectPaymentInstrument(walletPref.PaymentInst);
            page.selectWalletType(walletPref.PaymentInstName);
            page.selectGrade(walletPref.GradeName);
            page.selectMobileRole(walletPref.mobileRole);
            page.selectTCP(walletPref.TCP);
            if (!Constants.PREFERENCE_TYPE_USER_REGISTRATION.equalsIgnoreCase(walletPref.preferenceType)) {
                page.selectPrimaryAccount(walletPref.isPrimary);
            }
            page.clickNext();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                        "Map Wallet Preferences", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }


    public Preferences modifySystemPreferences(String code, String value) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifySystemPreferences", ExtentColor.ORANGE);
            pNode.info(m);

            SystemPreferences_Page1 page1 = new SystemPreferences_Page1(pNode);

            page1.navigateToSystemPreference();
            page1.selectCheckBox(code);
            page1.enterValueToTextBox(code, value);
            page1.clickSubmit();
            page1.clickFinalSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update", "System Preference Updated", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }


    public Preferences doLoginAndModifySystemPreferences(String code, String value) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("doLoginAndModifySystemPreferences", ExtentColor.ORANGE);
            pNode.info(m);

            //Login included in this method
            Login.init(pNode).loginAsSuperAdmin("PREF001");

            SystemPreferences_Page1 page1 = new SystemPreferences_Page1(pNode);

            page1.navigateToSystemPreference();
            page1.selectCheckBox(code);
            page1.enterValueToTextBox(code, value);
            page1.clickSubmit();
            page1.clickFinalSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update", "System Preference Updated", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }


    public Preferences viewSystemPreferences(String code, String expected) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("modifySystemPreferences", ExtentColor.ORANGE);
            pNode.info(m);

            SystemPreferences_Page1 page1 = new SystemPreferences_Page1(pNode);

            page1.navigateToSystemPreference();

            String value = page1.fetchTextBoxValue(code);

            String block = "Value of Code :" + code + " is \n" + value;
            Assertion.verifyEqual(value, expected, "Verify value of System Preference Code: " + code, pNode);

            m = MarkupHelper.createCodeBlock(block);
            pNode.info(m);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * @param serviceCode
     * @param messageCode
     * @return
     * @throws IOException
     */
    public Preferences updateSMSConfigurationMessageText(String serviceCode, String messageCode, String message) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("updateSMSConfigurationMessageText", ExtentColor.ORANGE);
            pNode.info(m);

            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(pNode);

            page1.navigateToSMSConfig();
            page1.selectServiceByValue(serviceCode);
            page1.selectMessageByValue(messageCode);
            page1.selectLanguage();

            if (message == null) {
                message = page1.getMessageTextArea();
            }
            page1.setMessageText(message);
            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sms.cr.message.update.success",
                        "Verify Updating Text Message for SMS Configuration", pNode, messageCode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * @param serviceCode
     * @param messageCode
     * @param staticMessage
     * @return
     * @throws IOException
     */
    public Preferences updateSMSConfigurationWithTransactionData(String serviceCode, String messageCode, String... staticMessage) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("updateSMSConfigurationWithTransactionData", ExtentColor.ORANGE);
            pNode.info(m);

            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(pNode);

            page1.navigateToSMSConfig();
            page1.selectServiceByValue(serviceCode);
            page1.selectMessageByValue(messageCode);
            page1.selectLanguage();
            page1.selectTransactionDataByText();
            page1.clickAddButton();

            if (staticMessage.length > 0) {
                value = page1.EnterStaticMessage(staticMessage);
            }

            value = page1.fetchMessage();
            page1.setMessageText(value);

            page1.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("sms.cr.message.update.success",
                        "Verify SMS Configuration is succesfully updated", pNode, messageCode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Update Email Notification
     *
     * @param service
     * @param subject
     * @param message
     * @return
     * @throws IOException
     */
    public Preferences updateEmailNotification(String service, String subject, String message) throws IOException {
        Markup m = MarkupHelper.createLabel("updateEmailNotificationSubject", ExtentColor.ORANGE);
        pNode.info(m);
        try {
            NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(pNode);

            page.navigateToEmailConfig();
            page.selectServiceByValue(service);
            page.selectLanguage();
            if (subject != null) {
                page.setEmailSubject(subject);
            }
            if (message != null) {
                page.setMessageText(message);
            }
            page.clickSubmit();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("action.message.common.success.update",
                        "Verify that email notification configuration is successfully updated", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Get The String message text from SMS Configuration page
     *
     * @param serviceCode - provide the service code e.g O2C
     * @param messageCode - provider the message code e.g. 1007
     * @return String - message txt
     */
    public String getSMSMessageText(String serviceCode, String messageCode) {
        Markup m = MarkupHelper.createLabel("getSMSMessageText", ExtentColor.ORANGE);
        pNode.info(m);
        String messageText = "";
        NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(pNode);
        try {
            page.navigateToSMSConfig();
            page.clickSmsConfigRadio();
            page.selectServiceByValue(serviceCode);
            page.selectMessageByValue(messageCode);
            page.selectLanguage();
            messageText = page.getMessageTextArea();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageText;
    }

    /**
     * @param walletPrefbank
     * @param walletPref
     * @param prefValue
     * @return
     * @throws Exception
     * @deprecated - use Preferences.setSVCPreferences()
     */
    public Preferences configureWalletPreferencesForSavingsClub(WalletPreference walletPrefbank, WalletPreference walletPref, String prefValue) throws Exception {
        List<WalletPreference> walletpreference = new ArrayList<WalletPreference>();
        walletpreference.add(walletPrefbank);
        walletpreference.add(walletPref);

        try {
            Markup m = MarkupHelper.createLabel("configureWalletPreferences", ExtentColor.ORANGE);
            pNode.info(m);

            WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(pNode);
            Thread.sleep(3000);
            page.navMapWalletPreferences();
            page.selectPreferenceType(walletPref.preferenceType);
            page.selectRegTypeByVal(walletPref.RegType);
            Thread.sleep(3000);

            if (prefValue.equalsIgnoreCase("TRUE")) {
                for (int i = 0; i < 2; i++) {

                    Thread.sleep(3000);
                    Select sel = new Select(driver.findElement(By.id("paymentMethodTypeId" + i + "")));
                    sel.selectByValue(walletpreference.get(i).PaymentInst);
                    sel = new Select(driver.findElement(By.id("walletTypeID" + i + "")));
                    sel.selectByVisibleText(walletpreference.get(i).PaymentInstName);
                    Thread.sleep(3000);
                    sel = new Select(driver.findElement(By.id("grade" + i + "")));
                    sel.selectByVisibleText(walletpreference.get(i).GradeName);
                    page.selectMobileRoleByIndex();
                    Thread.sleep(3000);
                    sel = new Select(driver.findElement(By.id("tcp" + i + "")));
                    if (sel.getOptions().size() > 1) {
                        sel.selectByIndex(1);
                    } else {
                        sel.selectByIndex(1);
                    }
                    if (!walletpreference.get(i).isPrimary) {
                        sel = new Select(driver.findElement(By.name("counterList[" + i + "].primaryAccountSelected")));
                        sel.selectByVisibleText("No");
                    } else {
                        sel.selectByVisibleText("Yes");
                    }
                    Thread.sleep(3000);
                }

            } else {
                page.selectPaymentInstrument(walletPref.PaymentInst);
                page.selectWalletType(walletPref.PaymentInstName);
                Thread.sleep(3000);
                page.selectGrade(walletPref.GradeName);
                page.selectMobileRoleByIndex();
                Thread.sleep(3000);
                page.selectTCPByIndex();
                page.selectPrimaryAccount(walletPref.isPrimary);
                Thread.sleep(3000);
            }
            page.clickNextForSavingsClub();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("subscriber.category.preferences.success",
                        "Map Wallet Preferences", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Set SVC preferences
     *
     * @param bankId       - bank ID
     */
    public void setSVCPreferences(User user, String bankId) throws Exception {
        Markup m = MarkupHelper.createLabel("setSVCPreferences", ExtentColor.ORANGE);
        pNode.info(m);

        if (!(AppConfig.clubBankId.contains(bankId))) {
            SystemPreferenceManagement.init(pNode)
                    .updateSystemPreference("CLUB_BANK_ID", bankId);
        }

        if (!AppConfig.isAddMakerReqClub) {
            SystemPreferenceManagement.init(pNode)
                    .updateSystemPreference("ADD_MAKER_REQ_CLUB", "Y");
        }

        // Map Wallet preference for Saving club
        CurrencyProviderMapping.init(pNode)
                .mapSVCWalletPreference(user, bankId);
    }

    public Preferences startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }
}
