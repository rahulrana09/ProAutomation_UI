package framework.features.systemManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.features.common.Login;
import framework.pageObjects.transferRule.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Dalia on 24-05-2017.
 */
public class TransferRuleManagement {
    private static ExtentTest pNode;
    private static WebDriver driver;
    private static OperatorUser usrTRuleCreator, usrTRuleApprover;
    private static Wallet rWallet, cWallet;

    public static TransferRuleManagement init(ExtentTest t1) throws Exception {

        try {
            driver = DriverFactory.getDriver();
            pNode = t1;

            if (usrTRuleCreator == null) {
                usrTRuleCreator = DataFactory.getOperatorUserWithAccess("T_RULES");
                usrTRuleApprover = DataFactory.getOperatorUserWithAccess("T_RULESAPP");
                rWallet = DataFactory.getWalletUsingAutomationCode(Wallets.REMMITANCE);
                cWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new TransferRuleManagement();
    }

    /**
     * Configure Service Charge
     *
     * @param sCharge
     * @return
     */
    public TransferRuleManagement configureTransferRule(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("configureTransferRule", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

        /*
        /*
         * Check if Transfer Rule exists
         */
        Map<String, String> dbTRule = dbHandle.dbGetTransferRule(sCharge);
        if (!dbTRule.isEmpty()) {
            /*
             * If suspended then Resume the same
             * if Approval Initiated, initiated for remove/delete then reject and Create New
             * If does not exist, create New		 *
             */
            sCharge.setTransferRulName(dbTRule.get("Name"));
            sCharge.setTransferRuleID(dbTRule.get("ID"));
            if (dbTRule.get("Status").equals("Y")) {
                pNode.info("The Transfer Rule already exists : " + dbTRule.get("ID"));
            } else if (dbTRule.get("Status").equals("S")) {
                resumeTransferRule(sCharge);
            } else if (notActiveStatus.contains(dbTRule.get("Status"))) {
                approveRejectTransferRule(dbTRule.get("ID"), false);
                initiateAndApproveTransferRule(sCharge);
            }
        } else {
            initiateAndApproveTransferRule(sCharge);
        }
        return this;
    }

    /**
     * Resume a suspended transfer rule
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public TransferRuleManagement resumeTransferRule(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("resumeTransferRule", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            // make sure any pending initiation is handled
            rejectPendingTransferRuleApproval(sCharge.TransferRuleID);
            Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
            setSenderReceiverDetailsForTransferRule(sCharge);

            TransferRule_Pg2.init(pNode)
                    .clickOnEdit(sCharge.TransferRuleID)
                    .selectStatusByVal("Y")
                    .submit()
                    .confirm();
            Thread.sleep(2000);

            if (Assertion.verifyActionMessageContain("trfrules.success.modInitiateTRule.withId",
                    "Verify successfully initiated resuming transfer rule", pNode, sCharge.TransferRuleID)) {

                Login.init(pNode)
                        .login(usrTRuleApprover); // Login as Trule Approver

                approveRejectTransferRule(sCharge.TransferRuleID, true);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * check if a transfer Rule is available
     *
     * @param sCharge
     * @return
     * @throws IOException
     * @throws SQLException
     */
    public boolean isTransferRuleAvailable(ServiceCharge sCharge) throws Exception {
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();
        Map<String, String> dbTRule = dbHandle.dbGetTransferRule(sCharge);
        List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");

        if (!dbTRule.isEmpty()) {
            /*
             * If suspended then Resume the same
             * if Approval Initiated, initiated for remove/delete then reject and Create New
             * If does not exist, create New		 *
             */
            if (dbTRule.get("Status").equals("Y")) {
                pNode.info("The Transfer Rule already exists : " + dbTRule.get("ID"));
                sCharge.setTransferRulName(dbTRule.get("Name"));
                sCharge.setTransferRuleID(dbTRule.get("ID"));
                return true;
            } else if (notActiveStatus.contains(dbTRule.get("Status"))) {
                //approveRejectTransferRule(dbTRule.get("ID"), false);
                return false;
            }
        }
        return false;
    }

    /**
     * Check if transfer rule is not present, create
     * If present and is in some other state then resume the same!
     *
     * @param sCharge
     * @return
     * @throws Exception
     */

    public TransferRuleManagement initiateAndApproveTransferRule(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
        setSenderReceiverDetailsForTransferRule(sCharge);
        setCategoryDetailsForTransferRule(sCharge);
        addInitiateTransferRule(sCharge);
        Login.init(pNode).login(usrTRuleApprover); // Login as Trule Approver
        approvePendingTRules();
        return this;
    }

    /**
     * Add Initiate transfer rule - Page-I
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public TransferRuleManagement setSenderReceiverDetailsForTransferRule(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("setSenderReceiverDetailsForTransferRule: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
            pageOne.navAddTransferRule();

            if(ConfigInput.isCoreRelease){
                // NEW UI
                pageOne.setOgSelServiceName(sCharge.ServiceInfo.ServcieType);
                pageOne.setOgSelPayerProvider(sCharge.Payer.ProviderName);
                pageOne.setOgSelPayerDomain(sCharge.Payer.DomainName);
                pageOne.setOgSelPayerPayInstrument(sCharge.ServiceInfo.PayerPaymentType);
                pageOne.setOgSelPayerPayInstrumentType(sCharge.Payer.PaymentType);
                pageOne.setOgSelPayeeProvider(sCharge.Payee.ProviderName);
                pageOne.setOgSelPayeeDomain(sCharge.Payee.DomainName);
                pageOne.setOgSelPayeeInstrument(sCharge.ServiceInfo.PayeePaymentType);
                pageOne.setOgSelPayeeInstrumenttype(sCharge.Payee.PaymentType);
            }else{
                // OLD UI
                pageOne.selectServiceName(sCharge.ServiceInfo);
                pageOne.selectSenderProvider(sCharge.Payer.ProviderName);
                pageOne.selectSenderDomain(sCharge.Payer.DomainName);
                pageOne.selectSenderPayInstrument(sCharge.ServiceInfo.PayerPaymentType);
                pageOne.selectSenderPayInstrumentType(sCharge.Payer.PaymentType);
                pageOne.selectReceiverProvider(sCharge.Payee.ProviderName);
                pageOne.selectReceiverDomain(sCharge.Payee.DomainName);
                pageOne.selectReceiverPayInstrument(sCharge.ServiceInfo.PayeePaymentType);
                pageOne.selectReceiverPayInstrumentType(sCharge.Payee.PaymentType);
            }
            pageOne.submit();
        } catch (Exception e) {
            pNode.warning("Also Check the ServiceCharge Object");
            setSenderReceiverDetailsForTransferRule(sCharge);
        }
        return this;
    }

    /**
     * Add Initiate transfer rule - Page-II
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public TransferRuleManagement setCategoryDetailsForTransferRule(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("setCategoryDetailsForTransferRule: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);


            if (!(sCharge.Payer.CategoryCode.equalsIgnoreCase(Constants.BILL_COMPANY))) {
                pageTwo.selectFromCategory(sCharge.Payer.CategoryName);
            } else {
                pageTwo.selectFromCategoryByValue(sCharge.Payer.CategoryCode);
            }
            if (!(sCharge.Payee.CategoryCode.equalsIgnoreCase(Constants.BILL_COMPANY))) {
                pageTwo.selectToCategory(sCharge.Payee.CategoryName);
            } else {
                pageTwo.selectToCategoryForBillPayment(sCharge.Payee.CategoryCode);
            }

            if (ConfigInput.isCoreRelease) {
                pageTwo.selectFromGrade(sCharge.payerGrade);
                pageTwo.selectToGrade(sCharge.payeeGrade);
            }
            pageTwo.submit();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Add Initiate transfer rule - Page-III
     *
     * @param sCharge
     * @return
     * @throws Exception
     */
    public TransferRuleManagement addInitiateTransferRule(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("addInitiateTransferRule: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);

            pageThree.selectStatus(GlobalData.transferRuleInput.StatusId);
            pageThree.selectTransferType(GlobalData.transferRuleInput.TransferType);
            pageThree.selectGeoDomain(GlobalData.transferRuleInput.GeoDomainCode);
            pageThree.setDirectTransfer();
            /*
            TODO NEED TO FIND ALT SOLUTION FOR THIS
            In case of agent assisted merchant payment in the controlled trf level dropdown
            there is no entry as system type and there is only one option as domain type
            which gets automatically selected when the page opens so selecting the controlled trf level only
            when service type is other than agent assisted merchant payment
             */

           /* if (!sCharge.ServiceInfo.ServcieType.equalsIgnoreCase(Services.AGENT_INITIATED_MERCHPAY)) {
                pageThree.selectControlTxLevel(GlobalData.transferRuleInput.ControlledTxLevel);
            }*/

            try {
                pageThree.selectControlTxLevel(GlobalData.transferRuleInput.ControlledTxLevel);
            } catch (Exception e) {
                ;
            }

            pageThree.selectFixedTxLevel(GlobalData.transferRuleInput.FixedTxLevel);
            pageThree.submit();
            pageThree.confirm();
            Thread.sleep(Constants.MAX_WAIT_TIME);

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("trfrules.success.initiateTRule", "Verify Transfer Rule is Successfully Add Initaited", pNode)) {
                    String trId = Assertion.getActionMessage().split("ID:")[1].trim();
                    sCharge.TransferRuleID = trId;
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public void approveTransferRule(String ruleID, String approvalType, boolean... isReject) throws Exception {

        try {
            Markup mark = MarkupHelper.createLabel("approveTransferRule", ExtentColor.BLUE);

            pNode.info(mark); // Method Start Marker

            TransferRuleApprove_Pg1 pageOne = TransferRuleApprove_Pg1.init(pNode);

            pageOne.navApproveTransferRule();

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            WebElement approveLink = driver.findElement(By.xpath("//td[contains(text(),'" + ruleID + "')]/ancestor::tr[1]/td/a[contains(@href,'approve')]"));
            approveLink.click();

            if (approvalType.equalsIgnoreCase("AI")) {
                pageOne.clickSubmit();
                Assertion.assertActionMessageContain("trfrules.approve.newTRule", "Approve Transfer Rule", pNode);
            } else if (approvalType.equalsIgnoreCase("DI")) {
                driver.switchTo().alert().accept();
                Assertion.assertActionMessageContain("trfrules.success.delTRule", "Approve Transfer Rule", pNode);
            } else {
                pageOne.clickSubmit();
                Assertion.assertActionMessageContain("trfrules.approve.newTRule", "Approve Transfer Rule", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * approve all pending Transfer Rule approvals
     *
     * @throws Exception
     */
    public void approvePendingTRules() throws Exception {
        Markup mark = MarkupHelper.createLabel("approvePendingTRules", ExtentColor.BLUE);
        pNode.info(mark); // Method Start Marker
        TransferRuleApprove_Pg1 pageOne = TransferRuleApprove_Pg1.init(pNode);

        // Navigate to TCP approval Page
        pageOne.navApproveTransferRule();

        int rowCount = driver.findElements(By.xpath("//*[@id='content']/form/table/tbody/tr")).size();
        String firstElem = driver.findElement(By.xpath("//*[@id='content']/form/table/tbody/tr[2]/td[1]")).getText();
        if (firstElem.equals(MessageReader.getMessage("systemUser.blanklist.label", null))) {
            pNode.info("No Transfer Rule to Approve!");
            return;
        }
        int index = (ConfigInput.isCoreRelease) ? 9 : 7;

        for (int i = 2; i <= rowCount; i++) {
            String xPathApprove = "//*[@id='content']/form/table/tbody/tr[2]/td[" + index + "]/a";

            driver.findElement(By.xpath(xPathApprove)).click();
            pageOne.clickSubmit();

            Assertion.assertActionMessageContain("trfrules.approve.newTRule", "Approve Transfer Rule", pNode);
        }
    }


    /**
     * Approve Reject transfer Rule
     *
     * @param ruleId  - Transfer Rule Name
     * @param approve - true if transfer rule need to be Approved
     * @throws Exception
     */
    public void approveRejectTransferRule(String ruleId, boolean approve) throws Exception {
        Markup mark = MarkupHelper.createLabel("approveRejectTransferRule", ExtentColor.BLUE);
        Login.init(pNode).login(usrTRuleApprover);
        pNode.info(mark); // Method Start Marker
        try {
            TransferRuleApprove_Pg1 pageOne = TransferRuleApprove_Pg1.init(pNode);

            // Navigate to TCP approval Page
            pageOne.navApproveTransferRule();

            if (approve) {
                driver.findElement(By.xpath("//td[contains(text(),'" + ruleId + "')]/ancestor::tr[1]/td/a[contains(@href,'approve')]")).click();
                pNode.info("Click on Approve Link for RuleID" + ruleId);
                pageOne.clickSubmit();
                Assertion.assertActionMessageContain("trfrules.approve.newTRule",
                        "Approve Transfer Rule with rule ID: " + ruleId, pNode);
            } else {
                driver.findElement(By.xpath("//td[contains(text(),'" + ruleId + "')]/ancestor::tr[1]/td/a[contains(@href,'reject')]")).click();
                driver.switchTo().alert().accept();
                pNode.info("Click on Reject Link for RuleID" + ruleId);
                Assertion.assertActionMessageContain("trfrules.reject.delTRule",
                        "Reject Transfer Rule with rule ID: " + ruleId, pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * This method can be used for rejecting auy pending transfer rules pending for approval
     *
     * @param ruleId
     * @return
     */
    public TransferRuleManagement rejectPendingTransferRuleApproval(String ruleId) throws IOException {
        Markup mark = MarkupHelper.createLabel("rejectPendingTransferRuleApproval", ExtentColor.TEAL);
        pNode.info(mark); // Method Start Marker
        try {
            Login.init(pNode).forceLogin(usrTRuleApprover);
            TransferRuleApprove_Pg1 pageOne = TransferRuleApprove_Pg1.init(pNode);

            // Navigate to TCP approval Page
            pageOne.navApproveTransferRule();
            List<WebElement> link = driver.findElements(By.xpath("//td[contains(text(),'" + ruleId + "')]/ancestor::tr[1]/td/a[contains(@href,'reject')]"));
            if (link.size() > 0) {
                link.get(0).click();
                driver.switchTo().alert().accept();
                pNode.info("Click on Reject Link for RuleID" + ruleId);
                Assertion.assertActionMessageContain("trfrules.reject.delTRule",
                        "Reject Transfer Rule", pNode);
            } else {
                pNode.info("No pending Approvals are available");
                Utils.captureScreen(pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Create all the missing O2C Transfer Rules
     * Note- it does not modify the existing T Rules
     *
     * @throws Exception
     */
    public void baseSetCreateO2CTRules() throws Exception {
        Markup mark = MarkupHelper.createLabel("baseSetCreateO2CTRules", ExtentColor.BLUE);
        pNode.info(mark); // Method Start Marker

        OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("O2C_TRULES", Constants.NETWORK_ADMIN);

        try {

            ExtentTest iterator = pNode.createNode("CREATE_O2C_TRANSFER_RULE_TEST-01",
                    "Login with Network Admin with appropriate Role and get Options from UI for creating O2C Transfer Rule");

            List<String> arrCategoryNotToInclude = Arrays.asList(
                    Constants.NETWORK_ADMIN,
                    Constants.BANK_ADMIN,
                    Constants.CHANNEL_ADMIN,
                    Constants.CUSTOMER_CARE_EXE,
                    Constants.BULK_PAYER_ADMIN,
                    Constants.BANK_USER,
                    Constants.SUBSCRIBER,
                    Constants.BILL_COMPANY,
                    Constants.INT_PARTNER,
                    Constants.SUPER_ADMIN
            );

            Wallet cWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);


            List<String> rnrCategoryList = DataFactory.getAllCategoryCodesFromRnr();
            List<String> walletList = DataFactory.getWalletForChannelUser();
            List<String> providerList = DataFactory.getAllProviderNames();
            O2CTransferRule_Pg1 pageOne = O2CTransferRule_Pg1.init(iterator);

            Login.init(iterator).login(netAdmin);
            pageOne.navAddTransferRule();
            Utils.captureScreen(iterator);
            for (String catCode : rnrCategoryList) {

                // exclude categories, like nwadm, superadmin, OPT...
                if (arrCategoryNotToInclude.contains(catCode))
                    continue;

                for (String wallet : walletList) {
                    // as commission wallet is not applicable for Enterprise, skip
                    if (catCode.equalsIgnoreCase(Constants.ENTERPRISE) && wallet.equalsIgnoreCase(cWallet.WalletName))
                        continue;

                    for (String providerName : providerList) {
                        String domainName = DataFactory.getDomainName(catCode);
                        ExtentTest tNode = pNode.createNode("CREATE_O2C_TRANSFER_RULE_" +
                                domainName + "_" + catCode + "_" + providerName + "_" + "Wallet" + "_" +
                                wallet, "Create Owner To Channel Transfer Rule with Domain:" +
                                domainName + ", Category: " + catCode + ", Provider: " +
                                providerName + "& Wallet: " + wallet);

                        O2CTransferRule_Pg1 chPage = O2CTransferRule_Pg1.init(tNode);
                        chPage.navAddTransferRule();
                        chPage.selectDomainName(domainName);
                        chPage.selectCategoryCode(catCode);
                        chPage.selectCurrencyProvider(providerName);
                        chPage.selectPaymentInstrument();
                        chPage.selectPayInstrumentType(wallet);

                        String firstAmt = pageOne.getFirstApproveLimit();

                        if (!firstAmt.isEmpty()) {
                            Utils.captureScreen(tNode);
                            tNode.pass("O2C Transfer Rule already exist for:" +
                                    domainName + " : " +
                                    catCode + " : " +
                                    providerName + " : " +
                                    wallet);
                            continue;
                        }

                        chPage.setFirstApprovalLimit(Constants.MAX_THRESHOLD);
                        chPage.submit();
                        chPage.confirm();
                        Assertion.verifyActionMessageContain("o2cTrf.success", "Verify Successfully created O2C transfer Rule", tNode);
                    }
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public void setO2CTransferLimit(String provider, String domain, String category, String payInst, String payInstType, boolean... isresetreq) throws Exception {
        boolean isresetrequ = isresetreq.length > 0 ? isresetreq[0] : true;
        try {
            O2CTransferRule_Pg1 rulePg1 = O2CTransferRule_Pg1.init(pNode);

            rulePg1.navAddTransferRule();

            rulePg1.selectDomainName(domain);
            rulePg1.selectCategoryName(category);
            rulePg1.selectCurrencyProvider(provider);
            rulePg1.selectPayInstrument(payInst);
            rulePg1.selectPayInstrumentType(payInstType);

            String firstAmt = rulePg1.getFirstApproveLimit();

            if (!firstAmt.isEmpty()) {
                pNode.info("O2C Transfer rule already exist for:" + category);
                rulePg1.setFirstApprovalLimit(Constants.MAX_THRESHOLD);
                if (isresetrequ) {
                    rulePg1.resetFirstApprovalLimit();
                    rulePg1.setFirstApprovalLimit(Constants.MAX_THRESHOLD);
                }
                rulePg1.submit();
                rulePg1.confirm();
                Assertion.verifyActionMessageContain("o2cTrf.modify", "Set O2C Transfer Limit", pNode);
            } else {
                rulePg1.setFirstApprovalLimit(Constants.MAX_THRESHOLD);
                rulePg1.submit();
                rulePg1.confirm();
                Assertion.verifyActionMessageContain("o2cTrf.success", "Set O2C Transfer Limit", pNode);

            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    public String setO2CTransferLimitWithAmount(String provider, String domain, String category, String payInst, String payInstType, String amount) throws Exception {
        String firstAmt = null;
        try {
            O2CTransferRule_Pg1 rulePg1 = O2CTransferRule_Pg1.init(pNode);

            rulePg1.navAddTransferRule();

            rulePg1.selectDomainName(domain);
            rulePg1.selectCategoryName(category);
            rulePg1.selectCurrencyProvider(provider);
            rulePg1.selectPayInstrument(payInst);
            rulePg1.selectPayInstrumentType(payInstType);

            firstAmt = rulePg1.getFirstApproveLimit();

            if (!firstAmt.isEmpty()) {
                pNode.info("O2C Transfer rule already exist for:" + category);
                rulePg1.setFirstApprovalLimit(amount);
                rulePg1.submit();
                rulePg1.confirm();
                Assertion.verifyActionMessageContain("o2cTrf.modify", "Set O2C Transfer Limit", pNode);
            } else {
                rulePg1.setFirstApprovalLimit(amount);
                rulePg1.submit();
                rulePg1.confirm();
                Assertion.verifyActionMessageContain("o2cTrf.success", "Set O2C Transfer Limit", pNode);

            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return firstAmt;
    }


    //******************************************************************************************

    public TransferRuleManagement modifyTransferRule(String service, String payInst) throws Exception {
        Markup mark = MarkupHelper.createLabel("modifyTransferRule", ExtentColor.BLUE);
        pNode.info(mark); // Method Start Marker
        try {
            Login.init(pNode).login(usrTRuleCreator);

            TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
            pageOne.navAddTransferRule();
            pageOne.selectfirstAll(service, payInst);

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
            pageTwo.ClickonEdit();

            TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);
            pageThree.submit();
            pageThree.confirm();

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            Assertion.verifyActionMessageContain("trfrules.success.modInitiateTRule", "Modify Transfer Rule", pNode);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public TransferRuleManagement modifyTransferRule(ServiceCharge sCharge) throws Exception {
        Markup mark = MarkupHelper.createLabel("modifyTransferRule", ExtentColor.BLUE);
        pNode.info(mark); // Method Start Marker
        try {
            Login.init(pNode).login(usrTRuleCreator);

            setSenderReceiverDetailsForTransferRule(sCharge);
            driver.findElement(By.xpath("//td[contains(text(),'" + sCharge.TransferRuleID + "')]/ancestor::tr[1]/td/a[contains(@href,'edit')]")).click();
            TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);
            pageThree.submit();
            pageThree.confirm();
            Thread.sleep(Constants.TWO_SECONDS);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("trfrules.success.modInitiateTRule", "Modify Transfer Rule", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * initiateApproveSuspenedTransferRule - initiate and approve Suspend an existing transfer Rule
     *
     * @param sCharge
     * @param approve - pass false if suspendLMSProfile has to be rejected
     * @return
     * @throws Exception
     */
    public TransferRuleManagement initiateApproveSuspenedTransferRule(ServiceCharge sCharge, String status, boolean approve) throws Exception {
        Markup mark = MarkupHelper.createLabel("initiateApproveSuspenedTransferRule", ExtentColor.BLUE);
        pNode.info(mark); // Method Start Marker
        try {
            Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
            setSenderReceiverDetailsForTransferRule(sCharge);
            driver.findElement(By.xpath("//td[contains(text(),'" + sCharge.TransferRuleID + "')]/ancestor::tr[1]/td/a[contains(@href,'edit')]")).click();
            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
            pageTwo.selectStatus(status); // to change the status as suspendLMSProfile (By default status is "Initiated")

            TransferRule_Pg3.init(pNode)
                    .submit()
                    .confirm();
            Thread.sleep(2000);

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("trfrules.success.modInitiateTRule", "Modify Transfer Rule", pNode);
            }

            Login.init(pNode)
                    .login(usrTRuleApprover); // Login as Trule Approver

            approveRejectTransferRule(sCharge.TransferRuleID, approve);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }


    public void modifySpecificTransferRule(ServiceCharge sCharge) throws Exception {
        try {
            Login.init(pNode).login(usrTRuleCreator);

            MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

            Map<String, String> dbTRule = dbHandle.dbGetTransferRuleForSpecificGrade(sCharge, sCharge.payerGrade, sCharge.payeeGrade);
            String TransferRuleID = dbTRule.get("ID");

            setSenderReceiverDetailsForTransferRule(sCharge);

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
            pageTwo.ClickEdit(TransferRuleID);

            TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);
            pageThree.submit();
            pageThree.confirm();

            String msg = Assertion.getActionMessage();
            String txnId = msg.split("ID:")[1].trim();
            String expectedMsg = MessageReader.getDynamicMessage("trfrules.success.modInitiateTRule", txnId);

            Assertion.verifyMessageContain(msg, expectedMsg, "Modify Transfer Rule", pNode);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * View Transfer Rule
     *
     * @param sCharge
     * @throws Exception
     */
    public void viewTransferRule(ServiceCharge sCharge) throws Exception {
        try {


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * Delete an existing Transfer Rule
     *
     * @param tRule
     * @return
     * @throws IOException
     */
    public String deleteTransferRule(ServiceCharge tRule) throws Exception {
        Markup mark = MarkupHelper.createLabel("Delete Transfer Rule", ExtentColor.RED);
        pNode.info(mark);

        String transferRuleID = null;
        if (isTransferRuleAvailable(tRule)) {
            try {
                Login.init(pNode)
                        .login(usrTRuleCreator);

                setSenderReceiverDetailsForTransferRule(tRule);
                MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();
                Map<String, String> dbTRule = dbHandle.dbGetTransferRule(tRule);
                transferRuleID = dbTRule.get("ID");
                driver.findElement(By.xpath("//td[contains(text(),'" + tRule.TransferRuleID + "')]/ancestor::tr[1]/td/a[contains(@href,'delete')]")).click();
                driver.switchTo().alert().accept();
                Assertion.assertActionMessageContain("trfrules.success.delInitaiteTRule",
                        "Delete Transfer Rule with rule ID: " + tRule.TransferRuleID, pNode);

                Login.init(pNode)
                        .login(usrTRuleApprover);
                approveTransferRule(tRule.TransferRuleID, "DI");
            } catch (Exception e) {
                Assertion.raiseExceptionAndContinue(e, pNode);
            }

        } else {
            pNode.info("Transfer Rule with ID:" + tRule.TransferRuleID + " is not available");
            Utils.captureScreen(pNode);
        }

        return transferRuleID;
    }

    /**
     * @param sCharge
     * @param approve
     * @return
     * @throws Exception
     */
    public TransferRuleManagement initiateAndApproveDeleteTransferRule(ServiceCharge sCharge, boolean approve) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
        setSenderReceiverDetailsForTransferRule(sCharge);
        setCategoryDetailsForTransferRule(sCharge);

        driver.findElement(By.xpath("//td[contains(text(),'" + sCharge.TransferRuleID + "')]/ancestor::tr[1]/td/a[contains(@href,'delete')]")).click();
        driver.switchTo().alert().accept();

        Assertion.verifyActionMessageContain("trfrules.success.delInitaiteTRule",
                "Verify Delete Initiate Transfer Rule; rule ID: " + sCharge.TransferRuleID, pNode);

        Login.init(pNode).login(usrTRuleApprover); // Login as Trule Approver
        TransferRuleManagement.init(pNode).approveTransferRule(sCharge.TransferRuleID, "DI");

        return this;
    }

    public String createFirstTransferRule() throws Exception {
        String ruleId = null;
        try {
            TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
            pageOne.navAddTransferRule();
            pageOne.selectfirstall();

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
            List<String> From = pageTwo.getAllFromCategory();
            List<String> To = pageTwo.getAllToCategory();
            pageTwo.selectFromCategory(From.get(0));
            pageTwo.selectToCategory(To.get(0));
            pageTwo.submit();

            Boolean istrue = true;

            String msg = Assertion.expectErrorInPage();
            if (msg != null) {
                Assertion.verifyMessageContain(msg, "208", "TransferRule Creation", pNode);
                istrue = false;
                ruleId = pageTwo.fetchRuleID();
            }

            if (istrue) {
                TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);
                pageThree.selectAlldata();
                pageThree.submit();
                pageThree.confirm();
                String succMsg = Assertion.getActionMessage();
                Assertion.verifyActionMessageContain("trfrules.success.initiateTRule", "Transfer Rule Created", pNode);
                ruleId = succMsg.split(":")[1].trim();
            } else {
                pNode.info("Transfer Rule is Already Created ");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return ruleId;
    }


    public String createFirstTransferRule(String serviceName, String payInst) throws Exception {
        Markup mark = MarkupHelper.createLabel("createFirstTransferRule", ExtentColor.RED);
        pNode.info(mark); // Method Start Marker

        String ruleId = null;
        boolean isExist = false;
        try {
            TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
            pageOne.navAddTransferRule();
            pageOne.selectfirstAll(serviceName, payInst);

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
            List<String> From = pageTwo.getAllFromCategory();
            List<String> To = pageTwo.getAllToCategory();
            pageTwo.selectFromCategory(From.get(0));
            pageTwo.selectToCategory(To.get(0));
            if (ConfigInput.isConfirm)
                pageTwo.submit();

            if (ConfigInput.isAssert) {
                Boolean istrue = true;

                String msg = Assertion.expectErrorInPage();
                if (msg != null) {
                    Assertion.verifyMessageContain(msg, "208", "TransferRule Creation", pNode);
                    istrue = false;
                    isExist = true;
                    ruleId = pageTwo.fetchRuleID();
                }

                if (istrue) {
                    TransferRule_Pg3 pageThree = TransferRule_Pg3.init(pNode);
                    pageThree.selectAlldata();
                    pageThree.submit();
                    pageThree.confirm();
                    Utils.putThreadSleep(Constants.TWO_SECONDS);
                    String succMsg = Assertion.getActionMessage();
                    Assertion.verifyActionMessageContain("trfrules.success.initiateTRule", "Transfer Rule Created", pNode);
                    ruleId = succMsg.split(":")[1].trim();
                } else {
                    pNode.info("Transfer Rule is Already Created ");
                }

                if (!isExist) {
                    approveTransferRule(ruleId, Constants.STATUS_ADD_INITIATE);
                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return ruleId;
    }


    public void CreateO2CTRulesForCommissionWallet() throws Exception {
        try {

            Markup mark = MarkupHelper.createLabel("baseSetCreateO2CTRules: Get UI combination", ExtentColor.BLUE);
            pNode.info(mark); // Method Start Marker

            String defaulWalletType = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName;

            O2CTransferRule_Pg1 pageOne = O2CTransferRule_Pg1.init(pNode);

            pageOne.navAddTransferRule();

            String domainNames, categoryNames, currencyProviders, payInstruments, payInstType;
            domainNames = DataFactory.getDomainName(Constants.WHOLESALER);

            pageOne.selectDomainName(domainNames);

            /*
             * Get the Category and Iterate
             */
            Thread.sleep(3000);
            categoryNames = DataFactory.getCategoryName(Constants.WHOLESALER);
            pageOne.selectDomainName(domainNames);
            pageOne.selectCategoryName(categoryNames);

            /*
             * Get The Currency Provider and Iterate
             */
            currencyProviders = DataFactory.getDefaultProvider().ProviderName;

            pNode.info("baseSetCreateO2CTRules" + domainNames + "-" + categoryNames + "-" +
                    currencyProviders + "-" + "Wallet" + "-" + defaulWalletType);

            O2CTransferRule_Pg1 chPage = O2CTransferRule_Pg1.init(pNode);

            chPage.selectDomainName(domainNames);
            chPage.selectCategoryName(categoryNames);
            chPage.selectCurrencyProvider(currencyProviders);

            /*
             * Get the pay Instrument and Iterate
             */

            chPage.selectPaymentInstrument();
            chPage.selectPayInstrumentType(defaulWalletType);

            /* Check if the First Approval is already set
             * if not then create the O2C Transfer Rule
             */
            String firstAmt = pageOne.getFirstApproveLimit();


            /*
             * Continue with the Creation
             */
            chPage.setFirstApprovalLimit(Constants.MAX_THRESHOLD);
            chPage.submit();
            chPage.confirm();

            Assertion.verifyActionMessageContain("o2cTrf.modify", "Create O2C Transfer Rule", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void configureTransferRuleForBillPayment(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("configureTransferRule", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        List<String> notActiveStatus = Arrays.asList("UI", "DI", "AI");
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

        /*
         * Check if Transfer Rule exists
         */
        Map<String, String> dbTRule = dbHandle.dbGetTransferRule(sCharge);
        if (!dbTRule.isEmpty()) {
            /*
             * If suspended then Resume the same
             * if Approval Initiated, initiated for remove/delete then reject and Create New
             * If does not exist, create New		 *
             */
            if (dbTRule.get("Status").equals("Y")) {
                pNode.info("The Transfer Rule already exists : " + dbTRule.get("ID"));
                sCharge.setTransferRulName(dbTRule.get("Name"));
                sCharge.setTransferRuleID(dbTRule.get("ID"));
            } else if (dbTRule.get("Status").equals("S")) {
                //resumeTransferRule($rule, $dbTRule.ID, $rule.StatusID); // TODO
            } else if (notActiveStatus.contains(dbTRule.get("Status"))) {
                //rejectTransferRule($dbTRule.ID);
                initiateAndApproveTransferRuleForBillPayment(sCharge);
            }
        } else {
            initiateAndApproveTransferRuleForBillPayment(sCharge);
        }
    }

    public TransferRuleManagement initiateAndApproveTransferRuleForBillPayment(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
        setSenderReceiverDetailsForTransferRule(sCharge);
        setCategoryDetailsForTransferRuleForBillPayment(sCharge);
        addInitiateTransferRule(sCharge);

        if (!usrTRuleCreator.LoginId.equals(usrTRuleApprover.LoginId)) {
            Login.init(pNode).login(usrTRuleApprover); // Login as Trule Approver
        }

        approvePendingTRules(); //TODO - Approval of initiated transfer rule is in progress
        return this;
    }

    public TransferRuleManagement setCategoryDetailsForTransferRuleForBillPayment(ServiceCharge sCharge) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("setCategoryDetailsForTransferRule: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);

            pageTwo.selectFromCategory(sCharge.Payer.CategoryName);
            pageTwo.selectToCategoryForBillPayment(sCharge.Payee.CategoryCode);
            pageTwo.submit();

            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public void checkServiceInTransferRule(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
        try {
            Markup m = MarkupHelper.createLabel("checkServiceInTransferRule: " + sCharge.ServiceChargeName, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
            pageOne.navAddTransferRule();
            pageOne.checkServiceName(sCharge.ServiceInfo);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public void deleteTransferRuleForSpecificGrade(ServiceCharge sCharge, String payerGrade, String payeeGrade) throws Exception {
        try {

            Markup m = MarkupHelper.createLabel("DeleteTransferRule", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

            Map<String, String> dbTRule = dbHandle.dbGetTransferRuleForSpecificGrade(sCharge, payerGrade, payeeGrade);
            String TransferRuleID = dbTRule.get("ID");

            if (TransferRuleID != null) {
                Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
                setSenderReceiverDetailsForTransferRule(sCharge);//Fill the first page of Trasfer Rule

                TransferRule_Pg2 pageTwo = TransferRule_Pg2.init(pNode);
                pageTwo.clickDelete(TransferRuleID);
                AlertHandle.acceptAlert(pNode);
                Assertion.verifyActionMessageContain("trfrules.success.delInitaiteTRule", "Delete Transfer Rule", pNode);

                Login.init(pNode).login(usrTRuleApprover); // Login as Trule Approver
                TransferRuleApprove_Pg1 pageOne = TransferRuleApprove_Pg1.init(pNode);

                // Navigate to approval Page
                pageOne.navApproveTransferRule();
                driver.findElement(By.xpath("//td[contains(text(),'" + TransferRuleID + "')]/../td/a[text()='Approve']")).click();
                AlertHandle.acceptAlert(pNode);
            } else {
                pNode.info("TRansfer Rule for the provided combination is not defined");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void verifyPaymentInstrumentType(ServiceCharge sCharge, String walletType) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator

        Markup m = MarkupHelper.createLabel("verifyPaymentInstrumentType " + sCharge.ServiceChargeName, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        TransferRule_Pg1 pageOne = TransferRule_Pg1.init(pNode);
        pageOne.navAddTransferRule();
        pageOne.selectServiceName(sCharge.ServiceInfo);
        pageOne.selectSenderProvider(sCharge.Payer.ProviderName);
        pageOne.selectSenderDomain(sCharge.Payer.DomainName);
        pageOne.selectSenderPayInstrument(sCharge.ServiceInfo.PayerPaymentType);
        try {
            pageOne.selectSenderPayInstrumentType(walletType);
            pageOne.selectReceiverProvider(sCharge.Payee.ProviderName);
            pNode.fail(walletType + " is displayed");
        } catch (Exception e) {
            pNode.pass(walletType + " is not displayed");
        }

    }


    public TransferRuleManagement initiateTransferRuleCreation(ServiceCharge sCharge) throws Exception {
        Login.init(pNode).login(usrTRuleCreator); // Login as Trule Creator
        setSenderReceiverDetailsForTransferRule(sCharge);
        setCategoryDetailsForTransferRule(sCharge);
        addInitiateTransferRule(sCharge);
        return this;
    }


}
