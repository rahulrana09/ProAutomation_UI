package framework.features.apiManagement;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.TxnResponse;
import framework.util.JsonPathOperation;
import framework.util.globalConstant.TransactionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import static framework.features.apiManagement.OldTxnContracts.*;
import static framework.util.propertiesManagement.MfsTestUtils.moneyOldUri;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class OldTxnOperations {

    private static final Logger LOGGER = LoggerFactory.getLogger(OldTxnOperations.class);
    public static String correctionInitiatedURL = "/TxnWebapp/money/moneyServices/transactionCorrectionInitiation?LOGIN=Api_Bearer&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=API&REQUEST_GATEWAY_TYPE=API";
    public static String lastNSuccessTransURL = "/TxnWebapp/money/moneyServices/lastNSuccessTxnChannel?LOGIN=Web_Bearer2&PASSWORD=cMLUHe2osdK6KlPLhUyaGa04FOs=&REQUEST_GATEWAY_CODE=WEB&REQUEST_GATEWAY_TYPE=WEB";
    public static String lastNTimeOutTransChannelURL = "/TxnWebapp/money/moneyServices/lastNTimeOutTxnOfChannel?LOGIN=Web_Bearer2&PASSWORD=cMLUHe2osdK6KlPLhUyaGa04FOs=&REQUEST_GATEWAY_CODE=WEB&REQUEST_GATEWAY_TYPE=WEB";
    public static String oldTxnURLForHSB = "/TxnWebapp/JsonSelector?LOGIN=Api_Bearer&PASSWORD=MPtc1ToayCkCMZZeHUu0snA3aUaPbSFQ9UzIkNGbVRU=&REQUEST_GATEWAY_CODE=API&REQUEST_GATEWAY_TYPE=API";
    public static String oldTxnURLForLSB = "/TxnWebapp/JsonSelector?LOGIN=Web_Bearer2&PASSWORD=cMLUHe2osdK6KlPLhUyaGa04FOs=&REQUEST_GATEWAY_CODE=WEB&REQUEST_GATEWAY_TYPE=WEB";
    public static String miniStatementURL = "/TxnWebapp/money/moneyServices/customerBankMiniStatement?LOGIN=Web_Bearer2&PASSWORD=cMLUHe2osdK6KlPLhUyaGa04FOs=&REQUEST_GATEWAY_CODE=WEB&REQUEST_GATEWAY_TYPE=WEB";
    public String bodyString;

    /**
     *
     */
    public OldTxnOperations(String transcType, JsonPathOperation... operations) {
        switch (transcType) {
            case TransactionType.SUBS_BILLER_ASSOC_BY_RETAILER:
                bodyString = subscriberBillerAssociationByRetailerJson(operations);
                break;

            case TransactionType.SUBS_BILLER_APPROVAL_BY_RETAILER:
                bodyString = subscriberBillerAssociationApprovalByRetailerJson(operations);
                break;

            case TransactionType.CHANGE_MPIN_CHANNEL_USR:
                bodyString = channelUserChangeMPinJSON(operations);
                break;

            case TransactionType.CHANGE_TPIN_CHANNEL_USR:
                bodyString = channelUserChangeTPinJSON(operations);
                break;

            case TransactionType.CHANGE_MPIN_CUSTOMER_USR:
                bodyString = customerChangeMPinJSON(operations);
                break;

            case TransactionType.CHANGE_TPIN_CUSTOMER_USR:
                bodyString = customerChangeTPinJSON(operations);
                break;

            case TransactionType.DELETE_BILLER_REG_BY_RETAILER:
                bodyString = deleteSubscriberBillerAssociationByRetailerJson(operations);
                break;

            case TransactionType.DELETE_BILLER_REG_BY_RETAILER_APPROVAL:
                bodyString = deleteSubscriberBillerAssociationByRetailerJson(operations);
                break;

            case TransactionType.SUBSCRIBER_AQUISITION:
                bodyString = CustomerAcquisitionServiceJson(operations);
                break;

            case TransactionType.SELF_SUBSCRIBER_REGISTRATION:
                bodyString = subscriberSelfRegistrationJson(operations);
                break;

            case TransactionType.SUBSCRIBER_REGISTRATION_CHANNEL_USER:
                bodyString = subscriberRegistrationByChannelUserJson(operations);
                break;

            case TransactionType.ADD_SAVING_CLUB:
                bodyString = addSavingClubJSON(operations);
                break;

            case TransactionType.JOIN_SAVING_CLUB:
                bodyString = joinSavingClubJson(operations);
                break;

            case TransactionType.DEPOSIT_SAVING_CLUB:
                bodyString = depositSavingClubJson(operations);
                break;

            case TransactionType.RESIGN_SAVING_CLUB:
                bodyString = resignClubJson(operations);
                break;

            case TransactionType.GET_SVC_LIST:
                bodyString = getSVCListJson(operations);
                break;

            case TransactionType.GET_SVC_WALLET_DETAILS:
                bodyString = getSVCWalletDetailJson(operations);
                break;

            case TransactionType.SUBS_ACCOUNT_CLOSURE:
                bodyString = subsAccountClosureJson(operations);
                break;

            case TransactionType.WEB_CASHOUT:
                bodyString = webCashOut(operations);
                break;

            case TransactionType.SVC_BALANCE_ENQUIRY:
                bodyString = svcBalanceEnquiryJson(operations);
                break;

            case TransactionType.SVC_BANK_MINI_STATEMENT:
                bodyString = svcBankMiniStatementJson(operations);
                break;

            case TransactionType.SVC_BANK_TO_WALLET:
                bodyString = svcBankToWalletJson(operations);
                break;

            case TransactionType.SVC_WALLET_TO_BANK:
                bodyString = svcWalletToBankJson(operations);
                break;

            case TransactionType.SVC_BANK_TO_WALLET_CONFIRMATION:
                bodyString = svcBankToWalletConfirmationJson(operations);
                break;

            case TransactionType.SVC_WALLET_TO_BANK_CONFIRMATION:
                bodyString = svcWalletToBankConfirmationJson(operations);
                break;

            case TransactionType.SVC_PENDING_TXN:
                bodyString = svcPendingTransactionsJson(operations);
                break;

            case TransactionType.SVC_BANK_BAL:
                bodyString = svcBankBalJson(operations);
                break;

            case TransactionType.SVC_MINI_STATEMENT:
                bodyString = svcMiniStatementJson(operations);
                break;

            case TransactionType.SETTLEMENT_INITIATION_CLUB:
                bodyString = settlementInitiation(operations);
                break;

            case TransactionType.SETTLEMENT_CONFIRMATION_CLUB:
                bodyString = settlementConfirmation(operations);
                break;

            case TransactionType.SVC_WITHDRAW_CLUB:
                bodyString = svcWithdrawClubJson(operations);
                break;

            case TransactionType.SVC_WITHDRAW_CLUB_APPROVAL:
                bodyString = svcWithdrawClubApprovalJson(operations);
                break;

            case TransactionType.SUBSCRIBER_REG_AS400:
                bodyString = subRegByAS400Json(operations);
                break;

            case TransactionType.SVC_DISBURSE_STOCK:
                bodyString = svcDisburseAmtJson(operations);
                break;

            case TransactionType.SVC_DISBURSE_CONFIRM:
                bodyString = svcDisburseConfirmJson(operations);
                break;

            case TransactionType.GENERATE_OTP:
                bodyString = generateOTPServiceJSON(operations);
                break;

            case TransactionType.SVC_PROMOTE_DEMOTE:
                bodyString = svcPromoteOrDemoteMemberJson(operations);
                break;

            case TransactionType.CREATE_EMPLOYEE:
                bodyString = createEmployee(operations);
                break;

            case TransactionType.Get_EMPLOYEEList:
                bodyString = getEmployeeList(operations);
                break;

            case TransactionType.Last_N_Transaction:
                bodyString = Last_N_Transaction(operations);
                break;


            case TransactionType.SUBS_ACCOUNT_CLOSURE_BY_AGENT:
                bodyString = subsAccountClosurebyAgent(operations);
                break;

            case TransactionType.Last_N_Pending_Transaction:
                bodyString = lastNPendingTransactions(operations);
                break;

            case TransactionType.AUTO_DEBIT_SUBS_SELFREQ:
                bodyString = AutoDebitSubsEnable(operations);
                break;

            case TransactionType.AUTO_DEBIT_CONFIRMATION_BY_CUSTOMER:
                bodyString = AutoDebit_Confirmation_Customer(operations);
                break;

            case TransactionType.AUTO_DEBIT_ENABLE_BY_CHANNELUSER:
                bodyString = AutoDebitEnableByChanneluser(operations);
                break;

            case TransactionType.AUTHORIZATION_REQUEST:
                bodyString = AuthorizationRequest(operations);
                break;

            case TransactionType.AUTOMATIC_O2C:
                bodyString = automaticO2C(operations);
                break;

            case TransactionType.AUTOMATIC_REIMBURSEMENT:
                bodyString = automaticReimbursement(operations);
                break;

            case TransactionType.Day_Txn_Summary:
                bodyString = channelUserDayTransactionJson(operations);
                break;

            case TransactionType.My_Account_Details:
                bodyString = channelUserAccountDetails(operations);
                break;

            case TransactionType.CHANGE_EMPLOYEE_MPIN:
                bodyString = changeEmployeeMpin(operations);
                break;

            case TransactionType.CHANGE_EMPLOYEE_PIN:
                bodyString = changeEmployeeTpin(operations);
                break;

            case TransactionType.SUSPEND_EMPLOYEE:
                bodyString = suspendEmployee(operations);
                break;

            case TransactionType.RESUME_EMPLOYEE:
                bodyString = resumeEmployee(operations);
                break;

            case TransactionType.SELF_REIMBURSEMENT:
                bodyString = selfReimbursement(operations);
                break;

            case TransactionType.UnBar_EMPLOYEE:
                bodyString = unBarEmployeeJson(operations);
                break;

            case TransactionType.BANK_CASH_OUT:
                bodyString = bankCashOutJson(operations);
                break;

            case TransactionType.BANK_CASH_IN:
                bodyString = bankCashInJson(operations);
                break;

            case TransactionType.BANK_CASH_OUT_CONFIRM:
                bodyString = bankCashOutConfirmJson(operations);
                break;

            case TransactionType.BALANCE_ENQUIRY:
                bodyString = balanceEnquiryJson(operations);
                break;

            case TransactionType.CREATE_SHIFTBASED_EMPLOYEE:
                bodyString = createShiftBasedEmployeeJson(operations);
                break;

            case TransactionType.SUB_BALANCE_ENQUIRY:
                bodyString = subscriberbalanceEnquiryJson(operations);
                break;

            case TransactionType.ENABLE_STANDARD_INSTRUCTIONS_CHANLUSER:
                bodyString = enableStandardInstructionBySelfJson(operations);
                break;

            case TransactionType.ENABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER:
                bodyString = enableStandardInstructionBySubscriberJson(operations);
                break;

            case TransactionType.DISABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER:
                bodyString = disableStandardInstructionBySubscriberJson(operations);
                break;
            case TransactionType.ADD_BENIFICIARY_BANK:
                bodyString = addBenificiaryforBankJson(operations);
                break;
            case TransactionType.DELETE_BENIFICIARY:
                bodyString = deleteBenificiaryJson(operations);
                break;

            case TransactionType.BALANCE_ENQ_BANK_CHANNELUSR:
                bodyString = channelUsrBalEnqBank(operations);
                break;

            case TransactionType.P2P_SEND_MONEY_SUBS:
                bodyString = p2pSendMoneyTransactionSubsJson(operations);
                break;

            case TransactionType.FETCH_LIST_OF_CURRENCIES:
                bodyString = fetchListOfCurrencies(operations);
                break;

            case TransactionType.CHANGE_CURRENCY_CODE:
                bodyString = changeCurrencyCode(operations);
                break;

            case TransactionType.DEFAULT_CURRENCY_CODE:
                bodyString = userInfo(operations);
                break;

            case TransactionType.SIM_SWAP:
                bodyString = SimSwap(operations);
                break;

            case TransactionType.UserenquiryAPI:
                bodyString = userEnquiryApi(operations);
                break;

            case TransactionType.BANK_REGISTRATION:
                bodyString = bankRegistrationAutoEnhancementJson(operations);
                break;

            case TransactionType.BANK_DEREGISTRATION:
                bodyString = bankDeRegistrationAutoEnhancementJson(operations);
                break;

            case TransactionType.BARorUNBAR:
                bodyString = UserBarUnbarJson(operations);
                break;

            case TransactionType.VIEW_BILL:
                bodyString = viewBillBySubscriber(operations);
                break;

            case TransactionType.GET_ANSWER_FOR_USER_QUESTIONS:
                bodyString = getAnsForUsrQns(operations);
                break;

            case TransactionType.VALIDATE_mPIN:
                bodyString = validateMpinJson(operations);
                break;

            case TransactionType.VIEW_LIST_OF_BENEFICIARY_AD_SI:
                bodyString = viewListOfBeneficiaryADSIJson(operations);
                break;

            case TransactionType.AUTO_DEBIT_DISABLE_BY_CUSTOMER:
                bodyString = disableAutoDebitByCustomer(operations);
                break;
            case TransactionType.BANK_ASSOCIATION:
                bodyString = associateUserWithBank(operations);
                break;
            case TransactionType.TRANSACTION_ENQUIRY:
                bodyString = transactionEnquiryApi(operations);
                break;
            case TransactionType.DEBIT_MONEY_FROM_SVA:
                bodyString = debitMoneyFromSVA(operations);
                break;
            case TransactionType.CREDIT_MONEY_TO_WALLET:
                bodyString = creditMoneyToSVA(operations);
                break;
            case TransactionType.BILLER_ASSOCIATION_SUBSCRIBER:
                bodyString = billerAssociationBySubscriber(operations);
                break;

            case TransactionType.DISBURSEMENT_INITIATION_CLUB:
                bodyString = disbursementInitiation(operations);
                break;

            case TransactionType.DISBURSEMENT_CONFIRMATION_CLUB:
                bodyString = disbursementConfirmation(operations);
                break;

            case TransactionType.gradeNetworkAssociation:
                bodyString = gradeNetworkAssociation(operations);
                break;

            case TransactionType.CUSTOMER_BNK_MINI_STATEMENT:
                bodyString = customerStatement(operations);
                break;

            case TransactionType.RETAILER_BANK_REQ:
                bodyString = retailerBankRequestJson(operations);
                break;

            case TransactionType.SUSPEND_RESUME_USER:
                bodyString = UserSuspendResumeJson(operations);
                break;

            case TransactionType.DELETE_CUSTOMER:
                bodyString = deleteCustomerJson(operations);
                break;

            default:
                LOGGER.error("Couldn't find the Specified Transaction Type");
                Assert.fail("Couldn't find the Specified Transaction Type");
        }
    }

    public static ValidatableResponse modifyTCPDetails(JsonPathOperation... operations) {
        ValidatableResponse response = moneyOldUri().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(modifyTCPDetailsJSON(operations))
                .when()
                .post(oldTxnURLForHSB)
                .then().statusCode(200).log().all();
        String message = response.extract().jsonPath().getString("COMMAND.MESSAGE");
        assertThat(message, equalTo("Success"));
        return response;
    }

    /**
     * Post requests and get the the Response
     *
     * @return
     */
    public TxnResponse postOldTxnURLForHSB(ExtentTest pNode) {
        pNode.info("REQUEST: " + bodyString);
        return new TxnResponse(moneyOldUri().contentType(ContentType.JSON)
                .accept(ContentType.ANY)
                .body(bodyString)
                .post(oldTxnURLForHSB)
                .then().log().all(), pNode);
    }
}
