/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Feature Of Master
*/

package framework.features.master;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.master.NetworkStatus_page1;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Master {

    private static WebDriver driver;
    private static ExtentTest pNode;

    public static Master init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new Master();
    }

    /**
     * Method to change network status
     *
     * @param status
     * @return
     * @throws IOException
     */
    public Master changeNetworkStatus(String... status) throws IOException {
        String netStatus = (status.length > 0) ? status[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("changeNetworkStatus", ExtentColor.ORANGE);
            pNode.info(m);

            NetworkStatus_page1 networkStatusPage1 = new NetworkStatus_page1(pNode);

            networkStatusPage1.navigateToLink();
            if (netStatus != null) {
                networkStatusPage1.setEnglishLangStatusBox(netStatus);
            } else {
                networkStatusPage1.setEnglishLangStatusBox(networkStatusPage1.getEnglishLangStatusValue());
            }

            networkStatusPage1.clickSaveButton();
            networkStatusPage1.clickConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("network.status.update.success", "Network Status Update", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Method to disable confirm button
     *
     * @return
     */
    public Master negativeTestConfirmation() {
        Markup m = MarkupHelper.createLabel("Setting Confirmation flag for Negative Tests", ExtentColor.BLUE);
        // Set the confirm flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    /**
     * Method to start Negative test case
     *
     * @return
     */
    public Master startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public Master suspendNetworkStatus() throws IOException {

        try {
            Markup m = MarkupHelper.createLabel("changeNetworkStatus", ExtentColor.ORANGE);
            pNode.info(m);

            NetworkStatus_page1 networkStatusPage1 = new NetworkStatus_page1(pNode);

            networkStatusPage1.navigateToLink();
            networkStatusPage1.statusBox_Click();
            networkStatusPage1.clickSaveButton();
            networkStatusPage1.clickConfirmButton();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("network.status.update.success", "Network Status Update", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }
}
