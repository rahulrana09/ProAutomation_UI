/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 28-Dec-2017
*  Purpose: Feature Of Reconciliation
*/
package framework.features.reconciliationManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.reconciliation.CheckReconciliation;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by febin.thomas on 12/28/2017.
 */

public class ReconciliationManagement extends TestInit {
    private static WebDriver driver;
    private static ExtentTest pNode;

    public static ReconciliationManagement init(ExtentTest t1) throws Exception {
        pNode = t1;
        driver = DriverFactory.getDriver();
        return new ReconciliationManagement();
    }

    public static String getSummaryBalance() throws Exception {
        String summaryBalance = null;
        try {
            Markup m = MarkupHelper.createLabel("getSummaryBalance", ExtentColor.CYAN);
            pNode.info(m);

            CheckReconciliation pageObject = new CheckReconciliation(pNode);
            pageObject.navigateToLink();
            if (ConfigInput.isCoreRelease) {
                pageObject.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
                pageObject.clickSubmit();
            }
            summaryBalance = pageObject.getReconBalance();
            BigDecimal DBrecon = MobiquityGUIQueries.getReconBalance();
            DBrecon = DBrecon.divide(AppConfig.currencyFactor);
            pNode.info("DB reconciliation Amount is : " + DBrecon);
            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(summaryBalance, DBrecon.toString(), "Verify Summary Balance of reconciliation", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return summaryBalance;
    }

    public String getChurnBalance(String providerId) throws Exception {
        String churnBalance = null;
        try {
            Markup m = MarkupHelper.createLabel("GetChurnBalance", ExtentColor.CYAN);
            pNode.info(m);

            CheckReconciliation pageObject = new CheckReconciliation(pNode);
            pageObject.navigateToLink();
            if (ConfigInput.isCoreRelease) {
                pageObject.selectMFSProvider(providerId);
                pageObject.clickSubmit();
            }
            churnBalance = pageObject.getReconChurn();
            BigDecimal DBchurn = MobiquityGUIQueries.getChurnBalance(providerId);
            DBchurn = DBchurn.divide(AppConfig.currencyFactor);
            pNode.info("DB Churn Amount is : " + DBchurn);
            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(churnBalance, DBchurn.toString(), "Verify Churn Balance of reconciliation", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return churnBalance;
    }

    public String getOnTheFlyBalance() {
        String onTheFlyBalance = null;
        try {
            Markup m = MarkupHelper.createLabel("getOnTheFlyBalance", ExtentColor.CYAN);
            pNode.info(m);

            CheckReconciliation pageObject = new CheckReconciliation(pNode);
            pageObject.navigateToLink();
            //if (ConfigInput.is4o9Release) {
            if (ConfigInput.isCoreRelease) {
                pageObject.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
                pageObject.clickSubmit();
            }
            onTheFlyBalance = pageObject.getOnTheFlyBalance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return onTheFlyBalance;
    }

    public String getOnTheFlyOnHold() {
        String onTheFlyOnHold = null;
        try {
            Markup m = MarkupHelper.createLabel("getOnTheFlyOnHold", ExtentColor.CYAN);
            pNode.info(m);

            CheckReconciliation pageObject = new CheckReconciliation(pNode);
            pageObject.navigateToLink();
            //if (ConfigInput.is4o9Release) {
            if (ConfigInput.isCoreRelease) {
                pageObject.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
                pageObject.clickSubmit();
            }
            onTheFlyOnHold = pageObject.getOnTheFlyOnHold();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return onTheFlyOnHold;
    }
}