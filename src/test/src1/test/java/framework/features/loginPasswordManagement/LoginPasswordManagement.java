package framework.features.loginPasswordManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.features.common.Login;
import framework.pageObjects.userManagement.ChangePasswordPage;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by navin.pramanik on 9/8/2017.
 */
public class LoginPasswordManagement {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pNode;

    public static LoginPasswordManagement init(ExtentTest t1) {
        pNode = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return new LoginPasswordManagement();
    }

    public void changePassword(String oldPwd, String newPwd, String... confrmPwd) throws IOException {

        try {

            Markup m = MarkupHelper.createLabel("changePassword", ExtentColor.ORANGE);
            pNode.info(m);

            String cnfPwd = (confrmPwd.length > 0) ? confrmPwd[0] : newPwd;

            ChangePasswordPage changePasswordPage = ChangePasswordPage.init(pNode);

            changePasswordPage.clickChangePwdLink();
            changePasswordPage.setOldPassword(oldPwd);
            changePasswordPage.setNewPassword(newPwd);
            changePasswordPage.setConfirmPassword(cnfPwd);
            changePasswordPage.clickSubmit();

            if (ConfigInput.isAssert)
                Assertion.verifyActionMessageContain("changePassword.label.success", "Password Successfully Changed", pNode);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }


    public void invalidLogin(String loginID, String pass) throws IOException {

        Markup m = MarkupHelper.createLabel("invalidLogin : Login ID " + loginID + "Password :" + pass, ExtentColor.ORANGE);
        pNode.info(m);

        try{
            Login login = new Login(pNode);

            login.openApplication();
            login.setUserName(loginID);
            login.setPassword(pass);
            login.signIn();

            String error = Assertion.getErrorMessage();
            pNode.info("Error Message : " + error);
        }catch (Exception e){
            Assertion.raiseExceptionAndStop(e, pNode);
        }


    }

    public void invalidLogin(String loginID, int noOftime) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("invalidLogin", ExtentColor.ORANGE);
            pNode.info(m);

            for (int i = 0; i < noOftime; i++) {

                invalidLogin(loginID, "invalid");

                String error = Assertion.getErrorMessage();

                if (error != null) {
                    pNode.pass("Not allowed to login with invalid password");
                } else {
                    pNode.fail("User is able to Login with invalid password");
                }

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public LoginPasswordManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

}
