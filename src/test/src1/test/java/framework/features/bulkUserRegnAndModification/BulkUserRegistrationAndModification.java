package framework.features.bulkUserRegnAndModification;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.bulkUserRegnAndModification.BulkUserApproval_Page;
import framework.pageObjects.bulkUserRegnAndModification.BulkUserRegnAndModification_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


/**
 * This class contains the methods for the bulk reporting tools
 *
 * @author
 */
public class BulkUserRegistrationAndModification {

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final String mfsProvider = "101";
    private static final String payInst = "WALLET";
    private static final String walletNo = "12";
    private static ExtentTest pNode;
    WebDriver driver;
    // File headers
    String csvFileHeader = "NamePrefix (Mr/Mrs/Miss/M/s)*,FirstName*,LastName*,Identification Number*,City,State,Country," +
            "Email,Designation,Contact Person,Contact Number,Gender (Male/Female)*,Date of Birth(dd/mm/yyyy)," +
            "Registration Form Number,Web login Id*,User Status*(A/M/N),MSISDN*,Preferred Language*,User Category*," +
            "Owner MSISDN*,Parent MSISDN*,Geography Domain Code*,Group Role Code*,Mobile Group Role Id*,Grade Code*," +
            "TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type,Wallet Type/Linked Bank Status* (A/M/S/D)," +
            "IdIssueDate,IdExpiryDate,IdIssueCountry,ResidenceCountry,PostalCode,Nationality,EmployerName,IdExpiresAllowed,_address1,_shortName,_address2," +
            "Url(Semicolon Separated),Block Notification(0/1/2/3/4/5/6/7)";
    String csvFileHeaderNew = "NamePrefix (Mr/Mrs/Miss/M/s)*,FirstName*,LastName*,Identification Number*,City,State,Country,Email*,Designation,Contact Person,Contact Number,Gender (Male/Female)*,Date of Birth*(dd/mm/yyyy),Registration Form Number,Web login Id*,User Status*(A/M/N),MSISDN*,Preferred Language*,User Category*,Owner MSISDN*,Parent MSISDN*,Geography Domain Code*,Group Role Code*,Mobile Group Role Id*,Grade Code*,TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type,Wallet Type/Linked Bank Status* (A/M/S/D),Merchant Type(ONLINE/OFFLINE),IdIssueDate,IdExpiryDate,IdIssueCountry,ResidenceCountry,PostalCode,Nationality,EmployerName,IdExpiresAllowed,_address1,_shortName,_address2,Url(Semicolon Separated),Block Notification(0/1/2/3/4/5/6/7),Relationship Officer*";
    String csvFileHeader5_0 = "NamePrefix (Mr/Mrs/Miss/M/s)*,FirstName*,LastName*,Identification Number*,City,State,Country,Email*,Designation,Contact Person,Contact Number,Gender (Male/Female)*,Date of Birth*(dd/mm/yyyy),Registration Form Number,Web login Id*,User Status*(A/M/N),MSISDN*,Preferred Language*,User Category*,Owner MSISDN*,Parent MSISDN*,Geography Domain Code*,Group Role Code*,Mobile Group Role Id*,Grade Code*,TCP Profile Id*,Primary Account (Y/N)*,Customer Id,Account Number,Account Type,Wallet Type/Linked Bank Status* (A/M/S/D),Merchant Type(ONLINE/OFFLINE),IdIssueDate,IdExpiryDate,IdIssueCountry,ResidenceCountry,PostalCode,Nationality,EmployerName,IdExpiresAllowed,Url(Semicolon Separated),Block Notification(0/1/2/3/4/5/6/7),Relationship Officer*,IsMerchant(Y/N),MerchantCategoryID,DescriptionForMerchant,ShowOnLocatorForMerchant(Y/N),IsAgent(Y/N),AgentCategoryID,DescriptionForAgent,ShowOnLocatorForAgent(Y/N),Latitude,Longitude,Autosweep Allowed (Y/N)";
    private FileWriter fileWriter = null;
    private String amount = "100";

    public static BulkUserRegistrationAndModification init(ExtentTest t1) {
        pNode = t1;
        return new BulkUserRegistrationAndModification();
    }

    public String generateBulkUserRegnCsvFile(User chUsr, String status, String ownerMSISDN) {

        String filePath = "uploads/" + "BulkChannelRegistrationCore" + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        String tcpProfileID = MobiquityGUIQueries.fetchTCPId(chUsr.CategoryCode, chUsr.GradeCode);
        String mobGroupRoleName = MobiquityGUIQueries.fetchMobileRole(chUsr.CategoryCode, chUsr.GradeCode);
        String webGroupRole = MobiquityGUIQueries.fetchWebGroupRole(chUsr.CategoryCode);
        String geoDomain = MobiquityGUIQueries.fetchGeographyDomain();
        String gender = "Homme";
        File f = new File(fileName);

        try {
            File directory = new File(FilePath.dirFileUploads);
            if (!directory.exists()) {
                directory.mkdir();
            }

            fileWriter = new FileWriter(f);

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);
            fileWriter.append("MR");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.FirstName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.LastName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.ExternalCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(gender);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.LoginId);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(status);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.MSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("English");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.CategoryCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(ownerMSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(ownerMSISDN);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(geoDomain);
            fileWriter.append(COMMA_DELIMITER);
            //fileWriter.append(chUsr.WebGroupRole);
            fileWriter.append(webGroupRole);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(mobGroupRoleName);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(chUsr.GradeCode);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(tcpProfileID);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("Y"); //Primary Account
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(status);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append("");
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(NEW_LINE_SEPARATOR);

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            closeResources();
        }
        return f.getAbsolutePath();
    }



    // ----------------------------------------------------------------------------------------------------------------------
    // ---------------------- BulkUserRegistrationAndModification FILE METHODS ------------------
    // ----------------------------------------------------------------------------------------------------------------------

    public BulkUserRegistrationAndModification initiateAndApproveBulkRegnOrModificn(String filename, boolean approve) throws Exception {

        OperatorUser optUser = DataFactory.getOperatorUsersWithAccess(Roles.BULK_USER_REGISTRATION_AND_MODIFICATION).get(0);
        OperatorUser approver = DataFactory.getOperatorUsersWithAccess(Roles.BULK_USER_APPROVAL).get(0);

        Login.init(pNode).login(optUser);

        String id = initiateBulkUserRegistration(filename);

        if (id != null) {
            if (!optUser.LoginId.equalsIgnoreCase(approver.LoginId)) {
                Login.init(pNode).login(approver);
            }
            approveBulkRegnAndModn(id, approve);
        }

        return this;
    }


    public String initiateBulkUserRegistration(String fileName) throws IOException {
        String bulkID = null;
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkUserRegistration", ExtentColor.TEAL);
            pNode.info(m);

            BulkUserRegnAndModification_Page1 page = new BulkUserRegnAndModification_Page1(pNode);

            page.navigateToBulkUsrRegnAndModifn();
            page.uploadFile(fileName);
            page.clickSubmit();

            page.downloadErrorLogs();

            if (ConfigInput.isAssert) {
                String errorStringPresent = Assertion.checkErrorPresent();
                if (errorStringPresent != null) {
                    pNode.fail("Error occurred :" + errorStringPresent);
                    Utils.captureScreen(pNode);
//                    page.downloadErrorLogs();
                } else {
                    String actual = Assertion.getActionMessage();
                    String expected = MessageReader.getMessage("bulkUpload.channel.label.success", null);
                    Assertion.verifyContains(actual, expected, "Bulk Channel Upload", pNode);
                    bulkID = actual.split(":")[1].trim();
                    pNode.info("Bulk ID Generated: " + bulkID);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return bulkID;
    }

    public void approveBulkRegnAndModn(String bulkID, boolean approve) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("initiateBulkUserRegistration", ExtentColor.TEAL);
            pNode.info(m);

            BulkUserApproval_Page page = new BulkUserApproval_Page(pNode);

            page.navigateToBulkUsrApproval();

            if (approve) {
                page.clickApprovalLink(bulkID);
                page.alertHandle(true);

                if (ConfigInput.isAssert) {
                    String errorStringPresent = Assertion.checkErrorPresent();
                    if (errorStringPresent != null) {
                        pNode.fail("Error occurred :" + errorStringPresent);
                        Utils.captureScreen(pNode);
                        page.downloadErrorLogs();
                    } else {
                        Assertion.verifyActionMessageContain("bulkuser.message.approval", "Bulk Channel Upload", pNode);
                    }

                }
            } else {
                page.clickRejectLink(bulkID);
                page.alertHandle(true);
                Assertion.verifyActionMessageContain("bulkuser.message.reject", "Bulk Channel Upload", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * Return the last batch payout ID
     *
     * @return
     * @throws Exception
     */
    public String getLastBatchPayoutID() throws Exception {
        MobiquityGUIQueries dBHandler = new MobiquityGUIQueries();
        String id = dBHandler.dbGetLastBatchPayoutID();
        dBHandler.closeConnection();
        return id;
    }

    public void closeResources() {
        try {
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Error while flushing/closing fileWriter !!!");
            e.printStackTrace();
        }
    }

    private void createUploadDirectory() {
        File directory = new File(FilePath.dirFileUploads);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }


    public String generateBulkUserRegnModnCsvFile(List<User> userList, String walletStatus, String userStatus) throws Exception {

        User parent, owner;
        String filePath = FilePath.dirFileUploads + Constants.FILEPREFIX_BULK_USER_REGISTER + DataFactory.getTimeStamp();
        String fileName = filePath + ".csv";
        File f = new File(fileName);
        String namePrefix = "MR";

        try {
            fileWriter = new FileWriter(f);

            //It will create the Uploads directory if not already exist
            createUploadDirectory();

            //Write CSV File headers
            fileWriter.append(csvFileHeaderNew);
            fileWriter.append(NEW_LINE_SEPARATOR);

            for (User user : userList) {

                //  User user = new User(catCode, gradeName);

                String tcpProfileID = MobiquityGUIQueries.fetchTCPId(user.CategoryCode, user.GradeCode);
                String mobGroupRoleName = MobiquityGUIQueries.fetchMobileRole(user.CategoryCode, user.GradeCode);
                String geoDomain = MobiquityGUIQueries.fetchGeographyDomain();
                String webGroupRole = user.WebGroupRole;

                user.setRelationshipOfficer("ROFC");

                if (user.ParentUser == null && user.ParentCategoryCode != null) {
                    parent = DataFactory.getChannelUserWithCategory(user.ParentCategoryCode);
                    user.setParentUser(parent);
                } else {
                    user.setParentUser(user);
                }

                if (user.OwnerCategoryCode != null) {
                    owner = DataFactory.getChannelUserWithCategory(user.OwnerCategoryCode);
                    user.setOwnerUser(owner);
                } else {
                    user.setOwnerUser(user);
                }

                String[] valuesToPutInCSV = null;
                //todo Set Parent User and Owner User MSISDN
                if (ConfigInput.isCoreRelease) {
                    valuesToPutInCSV = new String[]{namePrefix, user.FirstName, user.LastName, user.ExternalCode, user.City, user.District, Constants.BLANK_CONSTANT, user.Email, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, "Male", user.DateOfBirth, Constants.BLANK_CONSTANT, user.LoginId, userStatus, user.MSISDN, "English", user.CategoryCode, user.OwnerUser.MSISDN, user.ParentUser.MSISDN,
                            geoDomain, webGroupRole, mobGroupRoleName, user.GradeCode, tcpProfileID, "Y", Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                            walletStatus, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                            Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, user.RelationshipOfficer,
                            Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT};
                } else {
                    valuesToPutInCSV = new String[]{namePrefix, user.FirstName, user.LastName, user.ExternalCode, user.City, user.District, Constants.BLANK_CONSTANT, user.Email, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, "Male", user.DateOfBirth, Constants.BLANK_CONSTANT, user.LoginId, userStatus, user.MSISDN, "English", user.CategoryCode, user.OwnerUser.MSISDN, user.ParentUser.MSISDN,
                            geoDomain, webGroupRole, mobGroupRoleName, user.GradeCode, tcpProfileID, "Y", Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                            walletStatus, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                            Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT,
                            Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, Constants.BLANK_CONSTANT, user.RelationshipOfficer};
                }

                for (String data : valuesToPutInCSV) {
                    fileWriter.append(data);
                    fileWriter.append(COMMA_DELIMITER);
                }

                fileWriter.append(NEW_LINE_SEPARATOR);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeResources();
        }

        return f.getAbsolutePath();
    }

}
