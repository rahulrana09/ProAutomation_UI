package framework.features.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.pageObjects.enterpriseManagement.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class Enterprise_Management {
    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    public static Enterprise_Management page;
    private static ExtentTest pNode;
    private static WebDriver driver;


    //private FileWriter fileWriter = null;
    String csvFileHeader = "Mobile Number*,Amount*,First Name,Last Name,ID Number";

    public static Enterprise_Management init(ExtentTest t1) {
        pNode = t1;
        if (page == null) {
            driver = DriverFactory.getDriver();
        }
        return new Enterprise_Management();
    }

    public static String generateBulkFile(String msisdn, String Amount) throws IOException {
        String csvFileHeader = "Mobile Number*,Amount*,First Name,Last Name,Id Number,Remarks";

        String filePath = "uploads/" + "salary" + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {
            //FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(msisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Amount);
            fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }

    public static String generateBulkpayee(String msisdn, String CODE) throws IOException {
        String csvFileHeader = "Mobile Number*,Unique Code*";

        String filePath = "uploads/" + "BULK_PAYEE_" + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {
            //FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);

            fileWriter.append(msisdn);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(CODE);
            fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
            pNode.error("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }

    public Enterprise_Management addBulkPayeeBatch(User user, String uniqueCode) throws Exception {

        BatchBulkPayee.init(pNode)
                .navigateToLink()
                .uploadFile(generateBulkpayee(user.MSISDN, uniqueCode))
                .clickOnSubmit()
                .clickOnConfirm();
        if (ConfigInput.isAssert) {
            Assertion.verifyActionMessageContain("employee.success.employeesCreated", "Verify Add Bull Payee", pNode, "1");
        }

        return this;
    }


    public Enterprise_Management AddBulkPayee(String value, String uniqueCode) throws Exception {
        try {
            //String ran = DataFactory.getRandomNumberAsString(5);
            EnterpriseManagementPage1.init(pNode)
                    .NavigateToLinkADD()
                    .EnterMsisdn(value)
                    .ClickOnSubmit();

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            EnterpriseManagementPage2.init(pNode)
                    .EnterUniqueCode(uniqueCode)
                    .ClickOnSubmit()
                    .ClickOnConfirm();

            if (ConfigInput.isAssert) {
                String message = MessageReader.getDynamicMessage("enterprise.label.addSuccess", uniqueCode);
                Assertion.verifyEqual(Assertion.getActionMessage(), message, "Add Bulk Payee", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public Enterprise_Management ModifyBulkPayee(String value, String uniqueCode) throws Exception {
        try {
            //String ran = DataFactory.getRandomNumberAsString(5);
            EnterpriseManagementPage1.init(pNode).NavigateToLinkModify().EntermodifyMsisdn(value).ClickOnmodifySubmit();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            EnterpriseManagementModifyPage.init(pNode).EnterUniqueCode(uniqueCode).ClickOnModify();
            String message = MessageReader.getDynamicMessage("enterprise.label.modifySuccess", uniqueCode);
            if (ConfigInput.isAssert) {
                EnterpriseManagementModifyPage.init(pNode).ClickOnConfirm();
                Assertion.verifyEqual(Assertion.getActionMessage(), message, "Add Bulk Payee", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Enterprise_Management DeleteBulkPayee(String value, String empCode) throws Exception {

        try {
            EnterpriseManagementPage1.init(pNode).NavigateToLinkDelete().EnterDeleteMsisdn(value).ClickOnDeleteSubmit();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            EnterpriseManagementDeletePage.init(pNode).ClickOnSubmit();
            if (ConfigInput.isAssert) {
//                String message = MessageReader.getDynamicMessage("enterprise.label.deleteSuccess");
//                Assertion.verifyEqual(Assertion.getActionMessage(), message, "Delete Bulk Payee", pNode);
                Assertion.verifyActionMessageContain("enterprise.label.deleteSuccess",
                        "Employee Deleted Successfully", pNode, empCode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }
}

