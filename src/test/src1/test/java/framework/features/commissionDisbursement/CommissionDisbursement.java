/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Features of commissionDisbursement
*/

package framework.features.commissionDisbursement;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page2;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page2;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Automation Team on 10/30/2017.
 */
public class CommissionDisbursement {

    private static WebDriver driver;
    private static FunctionLibrary fl;
    private static ExtentTest pNode;
    private String CommissonWithdrawalFile;

    public static CommissionDisbursement init(ExtentTest t1) throws Exception {
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        pNode = t1;
        return new CommissionDisbursement();
    }

    /**
     * IT is used for Commission Disbursement_Page1
     *
     * @param chUser
     * @throws Exception
     */
    @Deprecated
    public void initiateCommissionDisbursement(User chUser) throws Exception {
        try {
            new Commission_Disbursement_Page1(pNode)
                    .navigateToLink()
                    .selectProvider()
                    .enterMsisdn(chUser.MSISDN)
                    .clickOnSubmit();

            if (Assertion.checkErrorMessageContain("comm.error.balance.disbursement.nouser", "Check Error for No user Found ", pNode)) {
                pNode.warning("No user Found Under This Selection");
                MobiquityDBAssertionQueries.dbTxnStatus = Constants.TXN_STATUS_FAIL;
            } else {
                Commission_Disbursement_Page2 obj = new Commission_Disbursement_Page2(pNode);
                obj.verifyUsers(chUser);
                obj.selectCommission();
                obj.selectMethodType(Constants.PAYINST_WALLET_CONST);
                obj.selectWalletType(Constants.NORMAL_WALLET);

                if (ConfigInput.isConfirm) {
                    // Approval is coming only if user found
                    obj.clickonApprove();
                    pNode.pass("valid user can perform Commission Disbursement Procedure");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /**
     * IT is used for Commission Disbursement_Page1
     *
     * @param chUser
     * @throws Exception
     */
    public String commissionDisburse(User chUser) throws Exception {
        String txnID = null;
        try {
            new Commission_Disbursement_Page1(pNode)
                    .navigateToLink()
                    .selectProvider()
                    .enterMsisdn(chUser.MSISDN)
                    .clickOnSubmit();

            if (Assertion.checkErrorMessageContain("comm.error.balance.disbursement.nouser", "Check Error for No user Found ", pNode)) {
                pNode.warning("No user Found Under This Selection");
                MobiquityDBAssertionQueries.dbTxnStatus = Constants.TXN_STATUS_FAIL;
                return null;
            } else if (Assertion.checkErrorMessageContain("comm.error.disbursement.nouser", "Check Error for No user Found ", pNode)) {
                pNode.warning("No User found for Commission Disbursement");
                MobiquityDBAssertionQueries.dbTxnStatus = Constants.TXN_STATUS_FAIL;
                return null;
            } else {

                if (ConfigInput.isConfirm) {
                    Commission_Disbursement_Page2 obj = new Commission_Disbursement_Page2(pNode);
                    obj.verifyUsers(chUser);

                    obj.selectCommission();
                    obj.selectMethodType(Constants.PAYINST_WALLET_CONST);
                    obj.selectWalletType(Constants.NORMAL_WALLET);
                    // Approval is coming only if user found
                    obj.clickonApprove();
                }

                if (ConfigInput.isAssert) {
                    String message = MessageReader.getDynamicMessage("commission.disbursed.success", chUser.LoginId);
                    Assertion.verifyEqual(Assertion.getActionMessage(), message, "Commission Disbursed", pNode);
                    MobiquityDBAssertionQueries.dbTxnStatus = Constants.TXN_STATUS_SUCCESS;
                    txnID = MobiquityDBAssertionQueries.getReferenceFromSentSMS(chUser.MSISDN);
                    if (txnID == null) {
                        txnID = MobiquityDBAssertionQueries.getReferenceFromSMSDelivered(chUser.MSISDN);
                    }
                    if (txnID != null) {
                        SentSMS.verifyPayerPayeeDBMessage(chUser, chUser, Services.COMMISSION_DISBURSEMENT, txnID, pNode);
                    }

                }
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return txnID;
    }


    /**
     * Click on commission disbursement process link
     * Select MFS provider dropdown
     * Select the domain from the dropdown values
     * Select the category from dropdown value
     * Click on submit
     * Click on download to CSV button
     * Override the amount for one of the entry in the downloaded file
     *
     * @return
     * @throws Exception
     */
    public String exportCommission_Disbursement(User user, String providerName) throws Exception {
        try {
            new Commission_Disbursement_Page1(pNode).navigateToLink()
                    .selectProvider(providerName)
                    .SelectDomain(user.DomainName)
                    .SelectCategory(user.CategoryName)
                    .clickOnSubmit();

            new Commission_Disbursement_Page2(pNode)
                    .selectCommissionDisbursement(user.MSISDN)
                    .downloadCommissionDisbursement();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        } finally {
            Utils.closeUntitledWindows();
        }
        // TODO, need to add validation, return only if download  is successful
        return FilePath.fileCommissionDisbursement;
    }

    public void initiateCommissionDisbursementForBank(User chUser, String bankName, String bankAccountNumber) throws Exception {
        try {
            new Commission_Disbursement_Page1(pNode)
                    .navigateToLink()
                    .selectProvider()
                    .enterMsisdn(chUser.MSISDN)
                    .clickOnSubmit();

            if (Assertion.checkErrorMessageContain("comm.error.balance.disbursement.nouser", "Check Error for No user Found ", pNode)) {
                pNode.warning("No user Found Under This Selection");
                MobiquityDBAssertionQueries.dbTxnStatus = Constants.TXN_STATUS_FAIL;
            } else {
                Commission_Disbursement_Page2 obj = new Commission_Disbursement_Page2(pNode);
                obj.verifyUsers(chUser);
                obj.selectCommission();
                obj.selectMethodType(Constants.PAYINST_BANK_CONST);
                obj.selectBankName(bankName);
                obj.selectBankAcc(bankAccountNumber);


                if (ConfigInput.isConfirm) {
                    // Approval is coming only if user found
                    new Commission_Disbursement_Page2(pNode).downloadCommissionDisbursement().updateCsvWithNewAmount("1");
                    pNode.pass("valid user can perform Commission Disbursement Procedure");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    public String getUserCommissionBalance(User user, String providerName) throws Exception {
        String amt = null;

        try {
            new Commission_Disbursement_Page1(pNode)
                    .navigateToLink()
                    .selectProvider(providerName)
                    .SelectDomain(user.DomainName)
                    .SelectCategory(user.CategoryName)
                    .clickOnSubmit();

            amt = new Commission_Disbursement_Page2(pNode).getCommissionamount(user.MSISDN);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return amt;
    }

    public String exportCommission_Withdrawal(User user) throws Exception {
        String file = null;
        try {
            Commission_Withdrawal_Page1.init(pNode).NavigateToLink().SelectProvider()
                    .SelectDomain(DataFactory.getDomainName(Constants.WHOLESALER))
                    .SelectCategory(DataFactory.getCategoryName(Constants.WHOLESALER)).ClickOnSubmit();
            Commission_Withdrawal_Page2.init(pNode).selectCommissionDisbursement(user.MSISDN);
            Commission_Withdrawal_Page2.init(pNode).downloadCommissionWithdrawal().getCommissionWithdrawalFile();
            List<String> windows = new ArrayList<>(driver.getWindowHandles());
            driver.switchTo().window(windows.get(1));
            driver.close();
            driver.switchTo().window(windows.get(0));
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return file;
    }

    public void VerifyCommissionWithdrawalCSVFile(List<String> status) throws Exception {
        pNode.info("Verifying the Commission Withdrawal CSV File");
        try {

            ArrayList fields = new ArrayList();
            //Header i.e 1st Row
            fields.add("MFSProvider*");
            fields.add("ChannelUserMobileNumber*");
            fields.add("Amount*");
            fields.add("IndividualRemarks");
            //FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "CommissionDisbursement");
            CommissonWithdrawalFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "CommissionWithdrawal");
            BufferedReader br = new BufferedReader(new FileReader(CommissonWithdrawalFile));
            String line = "";
            int row = 0;
            List<String> records = new ArrayList<>();

            //Getting the number of rows in the file and will store it in records
            while ((line = br.readLine()) != null) {
                records.add(line);
                row++;
            }
            System.out.println("Number of lines in records:" + records.size());
            String[] subString = records.get(0).split(",");

            //Verifying the header of the file i.e row 0
            pNode.info("Header Verification ");
            for (int i = 0; i < subString.length; i++) {
                System.out.println(subString[i]);
                System.out.println(fields.get(i));
                Assert.assertEquals(subString[i], fields.get(i));
                pNode.info(subString[i] + " Verified");

            }

            //Verifying the other row of the file apart from row 0

            for (int i = 1; i < records.size(); i++) {
                System.out.println(" Substring:" + i);
                String[] subStrings = records.get(i).split(",");
                for (int k = 0; k < subStrings.length; k++) {
                    System.out.print(subStrings[k] + ",");
                }
                for (int o = 0; o < subStrings.length; o++) {
                    System.out.println("E " + subStrings[o]);
                    System.out.println("A " + status.get(o));
                    Assert.assertEquals(subStrings[o].trim(), status.get(o).trim());

                }
                pNode.pass("Passed");
                pNode.info("Row " + i + " Verified ");

            }
        } catch (Exception e) {
            pNode.fail("Failed");
            pNode.info("Records not verified ");
            e.printStackTrace();
        }
    }

    /**
     * It is used for select the drop down and check
     * if there is any user is available or not
     *
     * @return
     * @throws Exception
     */
    public CommissionDisbursement commissionByDropdowns() throws Exception {
        try {
            new Commission_Disbursement_Page1(pNode).
                    navigateToLink().
                    selectProvider().
                    selectDomain().
                    selectCat().
                    selectZone().
                    selectArea().
                    clickOnSubmit();

            if (Assertion.checkErrorMessageContain("comm.error.disbursement.nouser", "Check Error for No user Found ", pNode)) {
                pNode.warning("No user Found Under This Selection");
                return this;
            }

            if (ConfigInput.isConfirm) {
                new Commission_Disbursement_Page2(pNode).verifyUsers();
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public CommissionDisbursement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        pNode.info(m);
        ConfigInput.isAssert = false;
        return this;
    }

    /**
     * Set the confirm flag to false
     * this will be reset once the method has completed in the @AfterMethod
     *
     * @return
     */
    public CommissionDisbursement negativeTestWithoutConfirm() {
        Markup m = MarkupHelper.createLabel("Confirmation flag for Negative Tests", ExtentColor.BLUE);

        ConfigInput.isConfirm = false;
        ConfigInput.isAssert = false;
        return this;
    }
}
