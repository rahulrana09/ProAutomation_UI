package framework.features.blockNotification;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.features.accessManagement.AccessManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.blockNotification.ExternalSystemDefinitions_Page1;
import framework.util.common.Assertion;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExternalSystemDefinitions {

    private static ExtentTest pNode;

    public static ExternalSystemDefinitions init(ExtentTest t1) {
        pNode = t1;
        return new ExternalSystemDefinitions();
    }


    public ExternalSystemDefinitions saveExternalSystem(String sourceName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("saveExternalSystem", ExtentColor.TEAL);
            pNode.info(m);
            ExternalSystemDefinitions_Page1.init(pNode)
                    .navigateToBlockNotification()
                    .enterSourceName(sourceName)
                    .clickSubmit();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.record.success", "Verify Adding External Sysyten", pNode, sourceName);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public ExternalSystemDefinitions modifyExternalSystem(String sourceName, String newSourceName) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("modifyExternalSystem", ExtentColor.TEAL);
            pNode.info(m);
            ExternalSystemDefinitions_Page1.init(pNode)
                    .navigateToBlockNotification()
                    .clickModifyDetails()
                    .radioBtn_ClickBasedOnText(sourceName.toLowerCase())
                    .clickModify()
                    .enterNewSourceName(newSourceName)
                    .clickSubmit();
            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.record.update", "Verify Modifying External Sysyten", pNode, sourceName.toLowerCase(), newSourceName);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public ExternalSystemDefinitions deleteExternalSystem(String sourceName) throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("modifyExternalSystem", ExtentColor.TEAL);
            pNode.info(m);
            ExternalSystemDefinitions_Page1.init(pNode)
                    .navigateToBlockNotification()
                    .clickModifyDetails()
                    .radioBtn_ClickBasedOnText(sourceName.toLowerCase())
                    .clickDelete();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("message.record.delete", "Verify Deleting External Sysyten", pNode, sourceName.toLowerCase());
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    public boolean sourceNameSize(String sourceName) {
        if (sourceName.length() <= 50) {
            return true;
        } else {
            return false;
        }
    }

}
