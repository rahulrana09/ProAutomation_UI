package framework.features.pricingEngine;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.features.common.Login;
import framework.pageObjects.pricingEng.PricingEngine_Page1;
import framework.pageObjects.pricingEng.PricingEngine_Page2;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.propertiesManagement.MfsTestProperties;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import static framework.util.common.Utils.createLabelForMethod;


import java.util.List;

public class PricingEngine {

    private static WebDriver driver;
    private static ExtentTest pNode;
    private static OperatorUser usrSChargeCreator, usrPricingEngineInitiator, usrPricingEngineApprover;
    private static MobiquityGUIQueries dbHandle;

    public static PricingEngine init(ExtentTest t1) throws Exception {
        try {
            driver = DriverFactory.getDriver();
            pNode = t1;
            if (usrSChargeCreator == null) {
                dbHandle = new MobiquityGUIQueries();
                usrSChargeCreator = DataFactory.getOperatorUserWithAccess(Roles.PRICING_ENGINE);
                usrPricingEngineInitiator = DataFactory.getOperatorUserWithAccess(Roles.ADD_EDIT_DELETE_PRICING_POLICY_INITIATION);
                usrPricingEngineApprover = DataFactory.getOperatorUserWithAccess(Roles.PRICING_POLICY_APPROVAL);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new PricingEngine();
    }

    /**
     * This method is used to Add Service Charge through Pricing Engine.
     *
     * @param sCharge
     * @throws Exception
     */
    public static String[] addServiceCharge(ServiceCharge sCharge, String ruleType) throws Exception {
        String[] serviceChargeValues = new String[1];
        try {
           createLabelForMethod("addServiceCharge",pNode);

            String messageKey;
            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
            page1.navToPricingEngine();
            if (ruleType.equalsIgnoreCase("SERVICE"))
                page1.clickServiceChargeMainMenu();
            else if (ruleType.equalsIgnoreCase("COMMISSION"))
                page1.clickCommissionMainMenu();
            else
                pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
            page1.selectService(sCharge.ServiceInfo.ServcieType);
            Thread.sleep(2000);
            page1.clickOnAddNewRuleButton();
            serviceChargeValues[0] = page2.enterRoleName(sCharge.ServiceChargeName);
            page2.enterStartDate();
            page2.enterEndDate();
            page2.enterMinimumCharge("0");
            page2.enterMaximumCharge("10");
            if (ruleType.equalsIgnoreCase("COMMISSION")) {
                page2.selectSenderRoleComm(sCharge.Payer.DomainCode);
                page2.selectHierachyComm("WHS");
            }
            page2.selectSenderRole();
            page2.selectSenderCategory(sCharge.Payer.DomainCode);
            page2.selectSenderGrade(sCharge.Payer.GradeCode);
            page2.selectReceiverRole();
            page2.selectReceiverCategory(sCharge.Payee.DomainCode);
            page2.selectReceiverGrade(sCharge.Payee.GradeCode);

            if (ruleType.equalsIgnoreCase("COMMISSION")) {
                page2.selectChargePayerCom();
                page2.selectChargePayeeCom("Wholesaler");
                page2.selectSVACom("primary");
                messageKey = "pricing.commissionPolicy.success";
            } else {
                page2.selectChargePayer();
                page2.selectSVAType(DataFactory.getDefaultWallet().WalletId);
                page2.selectChargePayee();
                messageKey = "pricing.serviceCharge.success";
            }

            page2.enterPricingPercentage("FLAT", "0.05");
            //page2.savePolicy();
            page2.clickOnSubmitForApprovalLink();
            page2.clickSubmit();
            if (ConfigInput.isAssert) {
                String actualMessage = page2.getSuccessMessage();
                String expectedMessage = MessageReader.getMessage(messageKey, null);
                Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return serviceChargeValues;
    }

    /**
     * This method is used to Add Service Charge through Pricing Engine.
     *
     * @param sCharge
     * @throws Exception
     */
    public static String[] addServiceChargeForDefaultCurrency(ServiceCharge sCharge, String ruleType, String currencyCode, String value) throws Exception {

        Markup m = MarkupHelper.createLabel("addServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String[] serviceChargeValues = new String[1];
        String messageKey;
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        if (ruleType.equalsIgnoreCase("SERVICE"))
            page1.clickServiceChargeMainMenu();
        else if (ruleType.equalsIgnoreCase("COMMISSION"))
            page1.clickCommissionMainMenu();
        else
            pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        new PricingEngine().init(pNode).selectCurrency(currencyCode);
        page1.clickOnAddNewRuleButton();
        serviceChargeValues[0] = page2.enterRoleName(sCharge.ServiceChargeName);
        page2.enterStartDate();
        page2.enterEndDate();
        page2.enterMinimumCharge("0");
        page2.enterMaximumCharge("10");
        if (ruleType.equalsIgnoreCase("COMMISSION")) {
            page2.selectSenderRoleComm(sCharge.Payer.DomainCode);
            page2.selectHierachyComm("WHS");
        }

        if (ruleType.equalsIgnoreCase("COMMISSION")) {
            page2.selectChargePayerCom();
            page2.selectChargePayeeCom("Wholesaler");
            page2.selectSVACom("commission");
            messageKey = "pricing.commissionPolicy.success";
        } else {
            page2.selectChargePayer();
            page2.selectSVAType(DataFactory.getDefaultWallet().WalletId);
            page2.selectChargePayee();
            messageKey = "pricing.serviceCharge.success";
        }
        page2.enterPricingPercentage("FLAT", value);
        page2.savePolicy();
        page2.clickSubmit();
        Thread.sleep(10000);
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage = MessageReader.getMessage(messageKey, null);
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
        }
        return serviceChargeValues;
    }

    public static String[] addApprovalForServiceCharge(String service, String ruleType, String user) throws Exception {

        String[] serviceChargeValues = new String[1];
        try {
            Markup m = MarkupHelper.createLabel("addApprovalForServiceCharge", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            String messageKey = "pricing.serviceCharge.approval.success";//"pricing.serviceCharge.success.approval";
            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
            page1.navToPricingEngine();
            page1.clickApproval();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            if (ruleType.equalsIgnoreCase("COMMISSION")) {
                page1.clickCommissionApproval();
            }
            page1.clickOnService(service, user);
            page1.priorityReview();
            page1.clickOnApproveImage();
            page1.clickOnApprovePolicyImage();
            page1.clickOnApproveImage();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean disc = page1.clickOnDiscountRule();
            if (disc) {
                page1.clickOnApproveImage();
            }

            boolean tax = page1.clickOnTaxationRule();
            if (tax) {
                page1.clickOnApproveImage();
            }

            page1.clickSubmitApproval();
            //Thread.sleep(2000);
            page1.clickSubmitConfirmationApproval();
            Thread.sleep(12000);
            if (ConfigInput.isAssert) {
                String actualMessage = page2.getSuccessMessage();
                String expectedMessage = MessageReader.getMessage("pricing.serviceCharge.success.approval", null);
                Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return serviceChargeValues;
    }

    public static String[] addTax(ServiceCharge sCharge, String value) throws Exception {

        Markup m = MarkupHelper.createLabel("addServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String[] serviceChargeValues = new String[1];
        String messageKey;
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.clickServiceChargeMainMenu();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        page1.selectCurrency(MfsTestProperties.getInstance().getProperty("mfs1.currency.code"));
        FunctionLibrary fl = new FunctionLibrary(driver);
        fl.contentFrame();
        page1.selectProceed();
        Thread.sleep(1000);
        page1.clickTax();
        page1.clickOnAddNewRuleButtonForTax();
        serviceChargeValues[0] = page2.enterRoleName(sCharge.ServiceChargeName);
        page2.enterStartDate();
        page2.enterEndDate();
        page2.enterPricingPercentage("FLAT", value);
        page2.savePolicy();
        page2.clickSubmit();
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage = MessageReader.getMessage("pricing.serviceCharge.success", null);
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
        }
        return serviceChargeValues;
    }

    /**
     * This method is used to Delete Service Charge through Pricing Engine.
     *
     * @param sCharge
     * @throws Exception
     */
    public static void deleteServiceChargeForDefaultCurrency(ServiceCharge sCharge, String ruleType) throws Exception {

        Markup m = MarkupHelper.createLabel("deleteServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        page1.navToPricingEngine();
        if (ruleType.equalsIgnoreCase("SERVICE"))
            page1.clickServiceChargeMainMenu();
        else if (ruleType.equalsIgnoreCase("COMMISSION"))
            page1.clickCommissionMainMenu();
        else
            pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        page1.selectCurrency(MfsTestProperties.getInstance().getProperty("mfs1.currency.code"));
        FunctionLibrary fl = new FunctionLibrary(driver);
        fl.contentFrame();
        page1.selectProceed();
        Thread.sleep(3000);
        page1.deleteAllServiceCharge();

    }

    public static void changeStatus(ServiceCharge sCharge, String policyName, String status) throws Exception {

        Markup m = MarkupHelper.createLabel("change status of policy [" + policyName + "]" +
                " to [" + status + "]", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.clickServiceChargeMainMenu();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        boolean changed = page1.changeStatus(policyName, status);
        page1.clickSaveDraft();
        Thread.sleep(3000);
        page2.savePolicy();
        page2.clickSubmit();
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage;
            if (changed)
                expectedMessage = MessageReader.getMessage("pricing.serviceCharge.approval"/*"pricing.serviceCharge.success"*/, null);
            else expectedMessage = MessageReader.getMessage("pricing.serviceCharge.noPolicyChange", null);
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Service rule status", pNode, true);
        }
        page1.clickBackToHome();
    }

    public static void suspendService(ServiceCharge sCharge) throws Exception {

        Markup m = MarkupHelper.createLabel("change status of policy ", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        page1.selectCurrency(MfsTestProperties.getInstance().getProperty("mfs1.currency.code"));
        FunctionLibrary fl = new FunctionLibrary(driver);
        fl.contentFrame();
        page1.selectProceed();
        Thread.sleep(2000);
        page1.changeStatus();
        page1.clickSaveDraft();
        Thread.sleep(5000);
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage = "Service Charge Policy draft has been saved successfully";
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Service Charge Save Draft Message", pNode, true);
        }
        page1.clickBackToHome();
    }

    public static String[] saveDraftServiceCharge(ServiceCharge sCharge) throws Exception {

        Markup m = MarkupHelper.createLabel("saveDraftServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String[] serviceChargeValues = new String[1];
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        page1.clickOnAddNewRuleButton();
        serviceChargeValues[0] = page2.enterRoleName(sCharge.ServiceChargeName);
        page2.enterStartDate();
        page2.enterEndDate();
        page2.enterMinimumCharge("0");
        page2.enterMaximumCharge("10");
        page2.selectSenderRole();
        page2.selectSenderCategory(sCharge.Payer.DomainCode);
        page2.selectSenderGrade(sCharge.Payer.GradeCode);
        page2.selectReceiverRole();
        page2.selectReceiverCategory(sCharge.Payee.DomainCode);
        page2.selectReceiverGrade(sCharge.Payee.GradeCode);
        page2.selectChargePayer();
        page2.selectSVAType(DataFactory.getDefaultWallet().WalletId);
        page2.selectChargePayee();
        page2.enterPricingPercentage("FLAT", "0.05");
        page1.clickSaveDraft();
        Thread.sleep(5000);
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage = "Service Charge Policy draft has been saved successfully";
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Service Charge Save Draft Message", pNode, true);
        }
        if (page1.isPolicyExist(serviceChargeValues[0]))
            pNode.pass("Policy saved in the system.");
        else pNode.fail("Saved policy not found.");
        return serviceChargeValues;
    }

    public boolean checkServiceInPricingEnginePage(String module, String serviceChargeName) throws Exception {
        boolean res = false;
        try {
            Login.init(pNode).login(usrSChargeCreator); // Login as ServiceCharge Creator


            Markup m = MarkupHelper.createLabel("Checking " + serviceChargeName + " Service In " + module, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            page1.navToPricingEngine();
            page1.selectServiceType(module);
            Thread.sleep(5000);
            List<WebElement> service = page1.getServiceList();
            for (WebElement allServices : service) {
                String serviceName = allServices.getText();
                res = serviceName.contains(serviceChargeName);
            }
            page1.clickBackToHome();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return res;
    }

    public boolean viewPricingEngine(boolean validate) throws Exception {
        boolean newPricingPage = false;
        String verifymsg = null;
        try {
            Login.init(pNode).login(usrSChargeCreator);

            Markup m = MarkupHelper.createLabel("viewPricingEngine", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            page1.navToPricingEngine();
            pNode.info("User navigated to Pricing Engine page.");
            if (!validate) {
                page1.isBackToHomeVisible();
                verifymsg = "Verify Pricing engine module";
            } else {
                page1.clickBackToHome();
                verifymsg = "Verify the new page for Pricing engine module";
            }
            newPricingPage = true;
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(newPricingPage);
            String expectedMessage = String.valueOf(true);
            Assertion.assertEqual(actualMessage, expectedMessage, verifymsg, pNode, true);
        }
        return newPricingPage;
    }

    public void reorderPricingRules(ServiceCharge sCharge, boolean priorityReq) throws Exception {


        Markup m = MarkupHelper.createLabel("reorderPricingRules", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        page1.clickReOrderBtn();
        pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        int numOfRules = page1.reorderServiceRule();
        page1.clickDoneBtn();
        page2.savePolicy();
        String[] priority = page1.priorityCheck(numOfRules, priorityReq);
        page2.clickSubmit();
        pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        if (ConfigInput.isAssert) {
            String actualMessage = page2.getSuccessMessage();
            String expectedMessage;
            if (numOfRules <= 1) {
                expectedMessage = MessageReader.getMessage("pricing.serviceCharge.noPolicyChange", null);
                ;
            } else expectedMessage = MessageReader.getMessage("pricing.serviceCharge.success", null);
            Assertion.assertEqual(actualMessage, expectedMessage, "Verify Service Charge reordering success Message", pNode, false);
        }
        if (ConfigInput.isAssert && priorityReq && numOfRules > 1) {
            String actualMessage = "Old Priority: 1 | New Priority: 2";
            String expectedMessage = "Old Priority: " + priority[0] + " | " + "New Priority: " + priority[1];
            Assertion.assertEqual(actualMessage, expectedMessage, "Validating the priority changes ", pNode, false);
        }
        page1.clickBackToHome();
        pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
    }

    public void checkSubMenusForServiceCharge(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("checkRuleTypesforServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        boolean allExist = page1.checkSubMenusOfServiceCharge();
        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(allExist);
            String expectedMessage = String.valueOf(true);
            Assertion.assertEqual(actualMessage, expectedMessage, "Validating: all required subMenus exist for Service Charge.", pNode, true);
        }
        page1.clickBackToHome();
    }

    public void checkSubMenusForCommission(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("checkRuleTypesforServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String[] serviceChargeValues = new String[1];
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        Thread.sleep(2000);
        page1.clickCommissionMainMenu();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(2000);
        boolean allExist = page1.checkSubMenusOfCommission();
        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(allExist);
            String expectedMessage = String.valueOf(true);
            Assertion.assertEqual(actualMessage, expectedMessage, "Validating: all required subMenus exist for Commission.", pNode, true);
        }
        page1.clickBackToHome();
    }

    public void viewRule(ServiceCharge sCharge, String ruleType, String policyName) throws Exception {
        Markup m = MarkupHelper.createLabel("viewRule", ExtentColor.BLUE);
        pNode.info(m);

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        // PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        if (ruleType.equalsIgnoreCase("SERVICE"))
            page1.clickServiceChargeMainMenu();
        else if (ruleType.equalsIgnoreCase("COMMISSION"))
            page1.clickCommissionMainMenu();
        else
            pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        boolean policyVisible = page1.isPolicyExist(policyName);
        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(policyVisible);
            String expectedMessage = String.valueOf(true);
            Assertion.assertEqual(actualMessage, expectedMessage, "Validating: The saved draft policy is visible to user. ", pNode, true);
        }
    }

    public void cloneThePolicy(ServiceCharge sCharge, String ruleType, String policyName) throws Exception {
        Markup m = MarkupHelper.createLabel("viewRule", ExtentColor.BLUE);
        pNode.info(m);

        String[] serviceChargeValues = new String[1];
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        if (ruleType.equalsIgnoreCase("SERVICE"))
            page1.clickServiceChargeMainMenu();
        else if (ruleType.equalsIgnoreCase("COMMISSION"))
            page1.clickCommissionMainMenu();
        else
            pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        page1.createCloneOfPolicy(policyName);
        page2.clickClonesubmit();
        page1.clickSaveDraft();
        page1.checkPolicyCount(policyName);
        int countPolicy = page1.checkPolicyCount(policyName);
        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(countPolicy);
            String expectedMessage = String.valueOf(2);
            Assertion.assertEqual(actualMessage, expectedMessage, "Validating that the policy is cloned and visible with same name. ", pNode, true);
        }
    }

    public String[] modifyCharge(ServiceCharge sCharge, String ruleType, String[] serviceChargeValues, boolean wantToSaveNSubmit) throws Exception {

        Markup m = MarkupHelper.createLabel("modifyServiceCharge", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        //String[] serviceChargeValues = new String[1];
        String messageKey = null;
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        if (ruleType.equalsIgnoreCase("SERVICE")) {
            page1.clickServiceChargeMainMenu();
            messageKey = "pricing.serviceCharge.success";
        } else if (ruleType.equalsIgnoreCase("COMMISSION")) {
            page1.clickCommissionMainMenu();
            messageKey = "pricing.commissionPolicy.success";
        } else
            pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(5000);
        page1.clickOnRule(serviceChargeValues[0]);
        page2.enterPricingPercentage("FLAT", "2");
        if (wantToSaveNSubmit) {
            page2.savePolicy();
            page2.clickSubmit();
            if (ConfigInput.isAssert) {
                String actualMessage = page2.getSuccessMessage();
                String expectedMessage = MessageReader.getMessage(messageKey, null);
                Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
            }
        }
        return serviceChargeValues;
    }

    /**
     *
     * @param sCharge
     * @param ruleType
     * @return
     * @throws Exception
     */
    public String[] pricingCalculator(ServiceCharge sCharge, String ruleType) throws Exception {

        Markup m = MarkupHelper.createLabel("pricingCalculator", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        String[] values = null;
        try {

            String messageKey = null;
            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            //PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
            page1.navToPricingEngine();
            page1.clickOnPricingCalculator();
            page1.selectService(sCharge.ServiceInfo.ServcieType);
            page1.selectPricingCalculatorItems("sender", "roleCode", sCharge.Payer.DomainCode);
            page1.selectPricingCalculatorItems("sender", "hierarchyRoleCode", "WHS");
            page1.selectPricingCalculatorItems("sender", "gradeCode", sCharge.Payer.GradeCode);
            page1.selectPricingCalculatorItems("receiver", "roleCode", sCharge.Payee.DomainCode);
            page1.selectPricingCalculatorItems("receiver", "hierarchyRoleCode", sCharge.Payee.DomainCode);
            page1.selectPricingCalculatorItems("receiver", "gradeCode", sCharge.Payee.GradeCode);
            page1.enterTxnAmount("100");
            page1.enterBearerCode("WEB");
            page1.clickCalculateBtn();
            values = page1.checkForCaluclatedValue(ruleType);
            if (ConfigInput.isAssert) {
                String actualMessage = values[0];
                String expectedMessage = "2.00";
                Assertion.assertEqual(actualMessage, expectedMessage,
                        "Verify service charge via pricing calculator.", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return values;
    }

    public void selectCurrency(String currencyCode) throws Exception {
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);

        Select sel = new Select(driver.findElement(By.id("currency-for-policy")));
        List<WebElement> list = sel.getOptions();
        int i;
        for (i = 0; i < list.size(); i++) {
            if (list.get(i).getText().equals(currencyCode)) {
                System.out.println("The index of the selected option[" + currencyCode + "] is: " + i);
                break;
            }
        }
        if (i != 0) {
            page1.selectCurrency(MfsTestProperties.getInstance().getProperty(currencyCode));
            FunctionLibrary fl = new FunctionLibrary(driver);
            fl.contentFrame();
            page1.selectProceed();
            Thread.sleep(1000);
        }
    }


    /**
     * @param sCharge
     * @param ruleType
     * @throws Exception
     * @deprecated : Don't use this method because this is not a Generic Method
     */
    public void checkPreviousPolicyVersion(ServiceCharge sCharge, String ruleType) throws Exception {
        Markup m = MarkupHelper.createLabel("checkPreviousPolicyVersion", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        // String[] serviceChargeValues = new String[1];
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        //PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);

        String[] serviceRuleValues = addServiceCharge(sCharge, ruleType);
        Login.init(pNode).login(usrPricingEngineInitiator);
        modifyCharge(sCharge, ruleType, serviceRuleValues, true);


        Thread.sleep(2000);
        page1.clickServiceChargeMainMenu();

        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(5000);
        page1.clickViewVersions();
        page1.enterVersion("1");
        page1.clickSubmitViewVersions();
        String version = page1.getDisplayedVersion();

        boolean previousVersion = false;
        if (version.contains("1")) {
            previousVersion = true;
        }

        if (ConfigInput.isAssert) {
            String actualMessage = String.valueOf(previousVersion);
            String expectedMessage = String.valueOf(true);
            Assertion.assertEqual(actualMessage, expectedMessage, "Previous Version for the Policy is displayed", pNode, true);
        }
        page1.clickBackToHome();
    }


    public void searchRules(ServiceCharge sCharge, String ruleType) throws Exception {
        Markup m = MarkupHelper.createLabel("searchRules", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String[] serviceChargeValues = new String[1];
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);

        String[] serviceRuleValues = addServiceCharge(sCharge, ruleType);

        Thread.sleep(2000);
        page1.clickServiceChargeMainMenu();

        page1.searchRuleName(sCharge.ServiceChargeName);
        page1.clickSearchButton();

    }

    public static String[] rejectServiceCharge(String service, String ruleType, String user) throws Exception {

        String[] serviceChargeValues = new String[1];
        try {
            Markup m = MarkupHelper.createLabel("addServiceCharge", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            String messageKey = "pricing.serviceCharge.success.reject";
            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
            page1.navToPricingEngine();
            page1.clickApproval();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            if (ruleType.equalsIgnoreCase("COMMISSION")) {
                page1.clickCommissionApproval();
            }
            page1.clickOnService(service, user);
            page1.clickOnRejectImage();
            page1.clickOnSave();
            page1.clickOnApprovePolicyImage();
            page1.clickOnRejectImage();
            page1.clickOnSave();
            Thread.sleep(2000);

            page1.clickSubmitApproval();
            Thread.sleep(2000);
            page1.clickSubmitConfirmationApproval();
            Thread.sleep(12000);
            if (ConfigInput.isAssert) {
                String actualMessage = page2.getSuccessMessage();
                String expectedMessage = MessageReader.getMessage(messageKey, null);
                Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return serviceChargeValues;
    }

    public String[] pricingCalculatorCommon(ServiceCharge sCharge, String ruleType, String expectedServCharge) throws Exception {

        Markup m = MarkupHelper.createLabel("pricingCalculator", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        String[] values;
        String messageKey = null;
        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
        PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
        page1.navToPricingEngine();
        page1.clickOnPricingCalculator();
        page1.selectService(sCharge.ServiceInfo.ServcieType);
        page1.selectPricingCalculatorItems("sender", "roleCode", sCharge.Payer.DomainCode);
        page1.selectPricingCalculatorItems("sender", "hierarchyRoleCode", "WHS");
        page1.selectPricingCalculatorItems("sender", "gradeCode", sCharge.Payer.GradeCode);
        page1.selectPricingCalculatorItems("receiver", "roleCode", sCharge.Payee.DomainCode);
        page1.selectPricingCalculatorItems("receiver", "hierarchyRoleCode", sCharge.Payee.DomainCode);
        page1.selectPricingCalculatorItems("receiver", "gradeCode", sCharge.Payee.GradeCode);
        page1.enterTxnAmount("100");
        page1.enterBearerCode("WEB");
        page1.clickCalculateBtn();
        values = page1.checkForCaluclatedValue(ruleType);
        pNode.info("Service charge amount: " + values[0] + " and Service charge name: " + values[1]);
        if (ConfigInput.isAssert) {
            String actualMessage = values[0];
            String expectedMessage = expectedServCharge;
            Assertion.assertEqual(actualMessage, expectedMessage,
                    "Verify service charge via pricing calculator.", pNode, true);
        }
        return values;
    }


    /**
     * SOME NEW METHODS ADDED
     */

    public void checkPreviousPricingPolicyVersion(ServiceCharge sCharge) throws Exception {
        Markup m = MarkupHelper.createLabel("checkPreviousPolicyVersion", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);

        if (!page1.checkIfAlreadyInPricingEnginePage()) {
            page1.navToPricingEngine();
        }
        page1.clickServiceChargeMainMenu();

        page1.selectService(sCharge.ServiceInfo.ServcieType);
        Thread.sleep(5000);
        page1.clickViewVersions();
        page1.enterVersion("1");
        page1.clickSubmitViewVersions();
        String version = page1.getDisplayedVersion();

        boolean previousVersion = false;
        if (version.contains("1")) {
            previousVersion = true;
        }

        if (ConfigInput.isAssert) {
            Assertion.assertEqual(previousVersion, true, "Check if Previous Version for the Policy is displayed", pNode, true);
        }
        page1.clickBackToHome();
    }


    public static String[] addServiceChargeTaxationRule(ServiceCharge sCharge, String ruleType) throws Exception {
        String[] serviceChargeValues = new String[1];
        try {
            Markup m = MarkupHelper.createLabel("addServiceCharge", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            String messageKey;
            PricingEngine_Page1 page1 = new PricingEngine_Page1(pNode);
            PricingEngine_Page2 page2 = new PricingEngine_Page2(pNode);
            page1.navToPricingEngine();
            if (ruleType.equalsIgnoreCase("SERVICE"))
                page1.clickServiceChargeMainMenu();
            else if (ruleType.equalsIgnoreCase("COMMISSION"))
                page1.clickCommissionMainMenu();
            else
                pNode.error("ruleType argument must contain either 'SERVICE' or 'COMMISSION' .");
            page1.selectService(sCharge.ServiceInfo.ServcieType);
            Thread.sleep(2000);
            page1.clickOnTaxationRuleFromService();
            page1.ClickOnaddNewRuleBtnForTaxation();
            serviceChargeValues[0] = page2.enterRoleName(sCharge.ServiceChargeName);
            page2.enterStartDate();
            page2.enterEndDate();
            page2.SelectmaxOf();
            page2.setpercentValue("1");
            page2.setFixedAmount("10");
            page2.selectSenderRole();
            page2.selectSenderCategoryForTaxationRule(sCharge.Payer.DomainName);
            page2.selectSenderGradeForTaxation(sCharge.Payer.GradeCode);
            page2.selectReceiverRole();
            page2.selectReceiverCategoryForTaxationRule(sCharge.Payee.DomainName);
            page2.selectReceiverGradeForTaxation(sCharge.Payee.GradeCode);
            page2.clickOnSubmitForApprovalLink();
            page2.clickSubmit();
            if (ConfigInput.isAssert) {
                String actualMessage = page2.getSuccessMessage();
                String expectedMessage = MessageReader.getMessage("pricing.serviceCharge.success", null);
                Assertion.assertEqual(actualMessage, expectedMessage, "Verify Success Message", pNode, true);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return serviceChargeValues;
    }

}
