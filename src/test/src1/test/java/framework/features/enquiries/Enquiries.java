package framework.features.enquiries;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.*;
import framework.features.common.Login;
import framework.pageObjects.customerCareManagement.CustomerCareExecutive_page1;
import framework.pageObjects.enquiries.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static framework.util.common.Assertion.markAsFailure;
import static framework.util.common.Assertion.raiseExceptionAndContinue;
import static framework.util.common.DriverFactory.driver;

/**
 * Created by navin.pramanik on 9/7/2017.
 */
public class Enquiries {

    private static ExtentTest pNode;
    private String webCat, webStatusExpected, webStatusActual;
    private String MainWindow;

    public static Enquiries init(ExtentTest t1) {
        pNode = t1;
        return new Enquiries();
    }

    /**
     * Method for Reset PIN
     *
     * @param usr
     * @param uType
     * @param pinType
     * @throws IOException
     */
    public void resetPin(User usr, String uType, String pinType) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("resetPIN", ExtentColor.BLUE);
            pNode.info(m);

            ChangePin_pg1 changePinPg1 = new ChangePin_pg1(pNode);

            changePinPg1.navResetPin();
            changePinPg1.selectUserTypeByValue(uType);
            changePinPg1.EnterUserNumber(usr.MSISDN);
            changePinPg1.selectPinByValue(pinType);

            changePinPg1.clickonSubmit();

            if (ConfigInput.isConfirm) {
                changePinPg1.clickResetButton();
            }

            if (ConfigInput.isAssert) {
                if (pinType.equals(Constants.RESET_MPIN_CONST)) {// to validate 2 success messages
                    Assertion.verifyActionMessageContain("ENQ_ResetMPin_success",
                            "The MPIN was reset and sent successfully", pNode, usr.FirstName, usr.LoginId);
                } else {
                    Assertion.verifyActionMessageContain("ENQ_ResetTPin_success",
                            "The TPIN was reset and sent successfully", pNode, usr.FirstName, usr.LastName);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /**
     * @param usr
     * @param resetPin true, if mpin has to be reset, false if tpin has to be reset, null if password has to be reset
     * @throws Exception
     */
    public void resetUserPin(Object usr, Boolean resetPin) throws Exception {
        String mainWindow = null;
        ChannelSubsEnq_pg1 pg1 = new ChannelSubsEnq_pg1(pNode);
        Markup m = MarkupHelper.createLabel("resetUserPin", ExtentColor.BLUE);
        pNode.info(m);

        try {
            String reimbType = null, msisdn = null;
            if (usr instanceof User) {
                reimbType = ((User) usr).getUserReimbType();
                msisdn = ((User) usr).MSISDN;
            } else if (usr instanceof Employee) {
                reimbType = Constants.CUSTOMER_REIMB;
                msisdn = ((Employee) usr).UserMSISDN;
            }
            mainWindow = initiateGlobalSearch(reimbType, Constants.CCE_MOBILE_NUMBER, msisdn);
            pg1.clickOnResetCredentials();

            if (resetPin) {
                pg1.mobilePinResetRadioBtn();
                pg1.mPinRadioBtn();
            } else if (!resetPin) {
                pg1.mobilePinResetRadioBtn();
                pg1.tPinRadioBtn();
            } else
                pg1.clickOnResetpwdRadio();


            if (resetPin != null)
                pg1.submitResetPin();
            else
                pg1.clickSubmitResetPassword();

            if (ConfigInput.isAssert) {
                Thread.sleep(5000);
                Utils.captureScreen(pNode);
                String message = pg1.getNotificationMessage();
                Assertion.verifyMessageContain(message, "cce.pin.change.success", "Verify that pin has been successfully changed", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
    }

    public void channelSubsEnquiry(User usr, String utype, String... agentCode) throws IOException {

        String agent = (agentCode.length) > 0 ? agentCode[0] : null;
        try {
            Markup m = MarkupHelper.createLabel("channelSubsEnquiry", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navChannelSubsEnquiry();
            channelSubsEnqPg1.selectUserTypeByValue(utype);
            if (agent == null) {
                channelSubsEnqPg1.setMSISDN(usr.MSISDN);
            } else {
                channelSubsEnqPg1.setAgentCode(agent);
            }

            Utils.putThreadSleep(NumberConstants.SLEEP_1000);
            channelSubsEnqPg1.clickProviderDdown();

            Utils.putThreadSleep(NumberConstants.SLEEP_3000);
            channelSubsEnqPg1.clickonSubmit();

            channelSubsEnqPg1.clickShowUserDetailsLink();
            Utils.putThreadSleep(NumberConstants.SLEEP_1000);

            channelSubsEnqPg1.handleWindow();
            Utils.putThreadSleep(NumberConstants.SLEEP_2000);

            String webFName = channelSubsEnqPg1.getFirstName();
            String webMSISDN = channelSubsEnqPg1.getSubscriberMSISDN();
            String webCat = channelSubsEnqPg1.getcategory();

            channelSubsEnqPg1.closeChildWindow();
            channelSubsEnqPg1.switchToMainWindow();

            if (ConfigInput.isAssert) {
                Assertion.verifyEqual(webFName, usr.FirstName, "Verify FirstName", pNode);
                Assertion.verifyEqual(webMSISDN, usr.MSISDN, "Verify MSISDN", pNode);
                Assertion.verifyEqual(webCat, Constants.WHOLESALER, "Verify Category", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    /***
     * @param userType
     * @param filterType
     * @param filterData
     * @param statusType
     * @return
     * @throws Exception
     */

    public Enquiries barUser(String userType, String filterType, String filterData, String statusType) throws Exception {
        CustomerCareExecutive_page1 page = CustomerCareExecutive_page1.init(pNode);
        try {
            Markup m = MarkupHelper.createLabel("BarUser: " + filterData, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            page.navigateToCCE()
                    .switchToWindow(1)
                    .selectUser(userType)
                    .selectFilter(filterType)
                    .setFilterData(filterData)
                    .clickSubmit();

            page.clickDown();
            page.changeStatus(statusType);
            page.setReason("Testing");
            Thread.sleep(2000);
            page.clickFinalSubmit();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }


    public Enquiries getTxnInfoFromExternalReferenceId(String userType, String filterType, String filterData, String ftxnId) throws Exception {
        Markup m = MarkupHelper.createLabel("Search Transaction Details Based On External Reference ID: " + ftxnId, ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker
        try {
            OperatorUser optUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(pNode).login(optUser);

            CustomerCareExecutive_page1 page = CustomerCareExecutive_page1.init(pNode);
            page.navigateToCCE()
                    .switchToWindow(1)
                    .selectUser(userType)
                    .selectFilter(filterType)
                    .setFilterData(filterData)
                    .clickSubmit();

            page.clickOnTransactionsTab()
                    .searchExternalRefId(ftxnId);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Enquiries switchToMainWindow() throws Exception {
        try {
            driver.close();
            CustomerCareExecutive_page1.init(pNode).switchToWindow(0);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Enquiries validateTransactionDetails(String txnId, String status) throws Exception {
        String expTxnId = null;
        String expStatus = null;
        try {
            CustomerCareExecutive_page1 page = new CustomerCareExecutive_page1(pNode);
            List<String> details = page.getTransactionStatus();
            for (int i = 0; i < details.size(); i++) {
                if (details.get(i).contains(txnId)) {
                    expTxnId = details.get(i);
                }
                if (details.get(i).contains(status)) {
                    expStatus = details.get(i);
                }
            }
            Assertion.verifyEqual(expTxnId, txnId, "Validating Transaction ID on CCE Portal", pNode);
            Assertion.verifyEqual(expStatus, status, "Validating Transaction Status on CCE Portal", pNode, true);
        } catch (Exception e) {
            raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Enquiries unBarUser(String userType, String filterType, String filterData, String statusType) throws Exception {

        CustomerCareExecutive_page1 page = CustomerCareExecutive_page1.init(pNode);
        try {
            Markup m = MarkupHelper.createLabel("UnBar User: " + filterData, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            page.navigateToCCE()
                    .switchToWindow(1)
                    .selectUser(userType)
                    .selectFilter(filterType)
                    .setFilterData(filterData)
                    .clickSubmit();

            page.clickDown();
            page.changeStatus(statusType);
            Thread.sleep(2000);
            page.clickFinalSubmit();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public Enquiries suspend_resume_user(String userType, String filterType, String filterData, String statusType) throws Exception {

        CustomerCareExecutive_page1 page = CustomerCareExecutive_page1.init(pNode);
        try {
            Markup m = MarkupHelper.createLabel("UnBar User: " + filterData, ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            page.navigateToCCE()
                    .switchToWindow(1)
                    .selectUser(userType)
                    .selectFilter(filterType)
                    .setFilterData(filterData)
                    .clickSubmit();

            page.clickDown();
            page.changeStatus(statusType);
            page.setReason("Testing");
            Thread.sleep(2000);
            page.clickFinalSubmit();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Method for verify channel and subscriber Details link
     *
     * @param usr
     * @param utype
     * @param detailType
     * @throws IOException
     */
    public void channelSubsDetailLink(User usr, String utype, String detailType) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("channelSubsEnquiry", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navChannelSubsEnquiry();
            channelSubsEnqPg1.selectUserTypeByValue(utype);
            channelSubsEnqPg1.setMSISDN(usr.MSISDN);

            Thread.sleep(Constants.THREAD_SLEEP_1000);
            channelSubsEnqPg1.clickProviderDdown();

            Utils.putThreadSleep(NumberConstants.SLEEP_3000);
            channelSubsEnqPg1.clickonSubmit();

            if (!ConfigInput.isConfirm) {
                return;
            }

            channelSubsEnqPg1.selectDetailType(detailType);
            channelSubsEnqPg1.handleWindow();

            if (detailType.equalsIgnoreCase(Constants.SHOW_USER_DETAILS) || (detailType.equalsIgnoreCase(Constants.SHOW_USER_ACCESS_DETAILS))) {
                String webFName = channelSubsEnqPg1.getFirstName();
                String webMSISDN = channelSubsEnqPg1.extractMSISDN();
                if (!detailType.equalsIgnoreCase(Constants.SHOW_USER_ACCESS_DETAILS)) {
                    webCat = channelSubsEnqPg1.extractCategory();
                    if (!usr.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                        Assertion.verifyEqual(webCat, usr.CategoryCode, "Verify Category Name", pNode);
                    else Assertion.verifyEqual(webCat, usr.CategoryName, "Verify Category Name", pNode);
                }
                Assertion.verifyEqual(webFName, usr.FirstName, "Verify FirstName", pNode);
                Assertion.verifyEqual(webMSISDN, usr.MSISDN, "Verify MSISDN", pNode);
            } else if (detailType.equalsIgnoreCase(Constants.SHOW_TRANS_CONTROL)) {
                String tcpID = channelSubsEnqPg1.extractTCPId();
                String tcpIDdB = MobiquityGUIQueries.fetchTCPId(usr.CategoryCode, usr.GradeCode);
                Assertion.verifyEqual(tcpID, tcpIDdB, "Verify TCP ID", pNode);
            } else if (detailType.equalsIgnoreCase(Constants.SHOW_USER_BALANCE)) {
                String userBalance = channelSubsEnqPg1.extractBalance();
                String currencyCode = MobiquityGUIQueries.getCurrencyCode(DataFactory.getDefaultProvider().ProviderId);
                String userDBalance = MobiquityDBAssertionQueries.getUserBalance(usr, Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId).toString();
                Assertion.verifyContains(userBalance, currencyCode + " " + userDBalance, "Verify USER BALANCE", pNode);
            }
            channelSubsEnqPg1.closeChildWindow();
            channelSubsEnqPg1.switchToMainWindow();

        } catch (
                Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

    }

    /**
     * Method to disable confirm button
     *
     * @return
     */
    public Enquiries negativeTestConfirmation() {
        Markup m = MarkupHelper.createLabel("Setting Confirmation flag for Negative Tests", ExtentColor.BLUE);
        // Set the confirm flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    /**
     * Method to start Negative test case
     *
     * @return
     */
    public Enquiries startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    /**
     * Method for Transaction Details
     *
     * @param txnID
     * @throws IOException
     */
    /**
     * Method for Transaction Details
     *
     * @param id
     * @throws IOException
     */
    public void transactionDetails(String id, boolean... extRefID) throws IOException {
        boolean extRefIDRequired = extRefID.length > 0 ? extRefID[0] : false;
        try {
            Markup m = MarkupHelper.createLabel("transactionDetails", ExtentColor.TEAL);
            pNode.info(m);

            TransactionDetail_page1 txnDetail = new TransactionDetail_page1(pNode);
            txnDetail.navigateToLink();

            if (!extRefIDRequired) {
                txnDetail.setTransactionID(id);
            } else {
                txnDetail.setExternalReferenceID(id);
            }
            txnDetail.clickSubmitButton();

            if (!ConfigInput.isAssert)
                return;

            if (!extRefIDRequired) {
                webStatusActual = txnDetail.getTxnStatus(id);
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatus(id);
                if (dbStatus.equalsIgnoreCase(Constants.TXN_STATUS_SUCCESS))
                    webStatusExpected = Constants.TXN_SUCCESS_STRING;
                else if (dbStatus.equalsIgnoreCase(Constants.TXN_STATUS_FAIL))
                    webStatusExpected = Constants.TXN_FAIL_STRING;

                Assertion.verifyEqual(webStatusActual, webStatusExpected, "Verify Transaction Status", pNode);
            } else {
                webStatusActual = txnDetail.getTxnStatus(id);
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatusWithFTXNID(id);
                if (dbStatus.equalsIgnoreCase(Constants.TXN_STATUS_SUCCESS))
                    webStatusExpected = Constants.TXN_SUCCESS_STRING;
                else if (dbStatus.equalsIgnoreCase(Constants.TXN_STATUS_FAIL))
                    webStatusExpected = Constants.TXN_FAIL_STRING;

                Assertion.verifyEqual(webStatusActual, webStatusExpected, "Verify Transaction Status", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }


    /**
     * Method to do Enquiry of Invalid type MSISDN
     *
     * @param msisdn
     * @throws IOException
     */
    public void invalidMsisdn(String msisdn) throws IOException {
        try {
            Markup m = MarkupHelper.createLabel("invalidMsisdn", ExtentColor.TEAL);
            pNode.info(m);
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navChannelSubsEnquiry();
            channelSubsEnqPg1.selectUserTypeByValue(Constants.CHANNEL_USERS_CONST);
            channelSubsEnqPg1.setMSISDN(msisdn);
            channelSubsEnqPg1.clickonSubmit();
            Thread.sleep(Constants.THREAD_SLEEP_1000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void initiateGlobalSearchForUser(String userType, String AccountIdentifier, User user) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateGlobalSearch", ExtentColor.BLUE);
        pNode.info(m);
        String mainWindow = driver.getWindowHandle();
        try {
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);
            // window handling


            channelSubsEnqPg1.navCustomerCareExecutive();
            Set<String> s1 = driver.getWindowHandles();
            Iterator<String> i1 = s1.iterator();
            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(user.MSISDN);
                    channelSubsEnqPg1.clickOnSubmit();
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    pNode.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                    boolean status = Utils.checkElementPresent("mobileNumberLabel", Constants.FIND_ELEMENT_BY_ID);
                    Assertion.verifyEqual(status, true, "mobile_number Field", pNode);

                    status = Utils.checkElementPresent("firstNameLabel", Constants.FIND_ELEMENT_BY_ID);
                    Assertion.verifyEqual(status, true, "first_name Field", pNode);

                    status = Utils.checkElementPresent("lastNameLabel", Constants.FIND_ELEMENT_BY_ID);
                    Assertion.verifyEqual(status, true, "last_name Field", pNode);

                    status = Utils.checkElementPresent("dateOfBirthLabel", Constants.FIND_ELEMENT_BY_ID);
                    Assertion.verifyEqual(status, true, "date_of_birth Field", pNode);


                    String msisdn = driver.findElement(By.id("MSISDN")).getText();
                    Assertion.verifyEqual(msisdn, user.MSISDN, "Verify MSISDN", pNode);

                    String name = driver.findElement(By.id("user_name")).getText();
                    Assertion.verifyEqual(name, user.FirstName + " " + user.LastName, "Verify Name", pNode);

                    //Commented below code because it is applicable to Channel Users only
                    /*String agentCode = MobiquityGUIQueries.getAgentCode(user.MSISDN);
                    String agentCode1 = driver.findElement(By.id("agent_merchant_code")).getText();
                    Assertion.verifyEqual(agentCode1, agentCode, "Verify Agent Code", pNode);
*/
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
    }

    public void updateUserStatus(User user, String status) throws Exception {
        Markup m = MarkupHelper.createLabel("updateUserStatus", ExtentColor.BLUE);
        pNode.info(m);
        String mainWindow = null;
        try {
            mainWindow = initiateGlobalSearch(user.getUserReimbType(), Constants.CCE_MOBILE_NUMBER, user.MSISDN);

            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);
            channelSubsEnqPg1.setDisplayDropDown();
            channelSubsEnqPg1.selectStatusByValue(status);

            if (!status.equalsIgnoreCase(Constants.UNBAR)) {
                channelSubsEnqPg1.setReasonToChangeStatus();
            }

            channelSubsEnqPg1.clickOnStatusSubmit();
            if (ConfigInput.isAssert) {
                verifyUserStatusChange(status);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
    }


    /**
     * Initiate Global Search
     *
     * @param userType
     * @param AccountIdentifier
     * @param filterData
     * @throws IOException
     */
    public String initiateGlobalSearch(String userType, String AccountIdentifier, String filterData) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateGlobalSearch", ExtentColor.BLUE);
        pNode.info(m);
        String mainWindow = null;
        try {
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);
            // window handling
            mainWindow = driver.getWindowHandle();

            channelSubsEnqPg1.navCustomerCareExecutive();
            Set<String> s1 = driver.getWindowHandles();

            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(filterData);
                    channelSubsEnqPg1.clickOnSubmit();
                    channelSubsEnqPg1.waitTillHeaderTitleIsShown();
                    if (!ConfigInput.isAssert) {
                        verifyErrorMsgWhileGlobalSearch();
                    }
                    break;
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return mainWindow;
    }

    public ChannelSubsEnq_pg1 enterCCEPortal(String userType, String AccountIdentifier, String filterData) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateGlobalSearch", ExtentColor.BLUE);
        pNode.info(m);
        String mainWindow = null;
        ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);
        try {
            // window handling
            mainWindow = driver.getWindowHandle();

            channelSubsEnqPg1.navCustomerCareExecutive();
            Set<String> s1 = driver.getWindowHandles();

            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(filterData);
                    channelSubsEnqPg1.clickOnSubmit();
                    Thread.sleep(5000);

                    if (!ConfigInput.isAssert) {
                        verifyErrorMsgWhileGlobalSearch();
                    }
                    break;
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return channelSubsEnqPg1;
    }

    public void resetCredentials(String userType, String AccountIdentifier, String filterData) throws IOException {
        String mainWindow = null;

        try {
            Markup m = MarkupHelper.createLabel("resetCredentials", ExtentColor.BLUE);
            pNode.info(m);
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navCustomerCareExecutive();
            Thread.sleep(3000);

            // window handling
            mainWindow = driver.getWindowHandle();
            Set<String> s1 = driver.getWindowHandles();

            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(filterData);
                    channelSubsEnqPg1.clickOnSubmit();
                    channelSubsEnqPg1.clickOnResetCredentials();

                    if (!ConfigInput.isAssert) {
                        verifyErrorMsgWhileReset();

                    } else {

                        String successMsg = channelSubsEnqPg1.pinSuccessMsg();
                        String expected = MessageReader.getDynamicMessage("cce.pin.change.success");

                        if (successMsg.contains(expected)) {

                            pNode.pass("success message verified ");
                            pNode.info("", MediaEntityBuilder
                                    .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                            pNode.info("Actual  :  " + successMsg);
                            pNode.info("Expected  :  " + expected);

                        } else {
                            pNode.fail("success message not verified");

                            pNode.info("", MediaEntityBuilder
                                    .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                            markAsFailure("Failure: Verify Message Contains! refer reports");
                        }
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    public void accountInformation(User user, String userType, String AccountIdentifier, String filterData) throws IOException {
        String mainWindow = null;

        try {
            Markup m = MarkupHelper.createLabel("accountInformation", ExtentColor.BLUE);
            pNode.info(m);
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navCustomerCareExecutive();
            Thread.sleep(3000);

            // window handling
            mainWindow = driver.getWindowHandle();
            Set<String> s1 = driver.getWindowHandles();

            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(filterData);
                    channelSubsEnqPg1.clickOnSubmit();
                    channelSubsEnqPg1.clickAccountInformationTab();
                    channelSubsEnqPg1.clickOnSvaLink();

                    if (ConfigInput.isAssert) {
                        String actual = channelSubsEnqPg1.getGradeName();
                        String expected = user.GradeName;

                        Assertion.verifyEqual(actual, expected, "grade name verified in SVA details", pNode);
                    } else {
                        pNode.fail("grade name not verified");
                        markAsFailure("Failure: Verify Message Contains! refer reports");
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }


    /**
     * @throws IOException
     */
    public void verifyErrorMsgWhileGlobalSearch() throws IOException {
        String actual = null;
        Markup m = MarkupHelper.createLabel("verifyErrorMsgWhileGlobalSearch", ExtentColor.BLUE);
        pNode.info(m);
        try {
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            actual = channelSubsEnqPg1.fetchErrorMessage();
            String expected = MessageReader.getDynamicMessage("error.CCE");
            Utils.captureScreen(pNode);
            if (actual.contains(expected)) {
                pNode.pass("error message verified ");
                pNode.pass("Actual  :  " + actual);
                pNode.pass("Expected  :  " + expected);
                channelSubsEnqPg1.clickOK();
            } else {
                pNode.fail("error message not verified");
                markAsFailure("Failed to verify error!");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    public void verifyUserStatusChange(String status) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyUserStatusChange", ExtentColor.BLUE);
        pNode.info(m);
        try {
            ChannelSubsEnq_pg1 pg1 = new ChannelSubsEnq_pg1(pNode);

            Utils.captureScreen(pNode);
            String message = pg1.getNotificationMessage();
            if (status.equalsIgnoreCase(Constants.SUSPEND)) {
                Assertion.verifyMessageContain(message, "cce.status.success.suspend",
                        "Verify that User is successfully Suspended", pNode);
            } else if (status.equalsIgnoreCase(Constants.UNBAR)) {
                Assertion.verifyMessageContain(message, "cce.status.success.unbar",
                        "Verify that User is successfully Unbarred", pNode);
            } else {
                Assertion.verifyMessageContain(message, "cce.status.success.barred",
                        "Verify that User is successfully Barred", pNode);
            }
            pg1.clickOK();
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

    /**
     * @throws IOException
     */
    public void verifyErrorMsgWhileReset() throws IOException {
        String actual = null;

        try {

            Markup m = MarkupHelper.createLabel("verifyErrorMsgWhileReset", ExtentColor.BLUE);
            pNode.info(m);

            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);
            Thread.sleep(2000);

            actual = channelSubsEnqPg1.fetchResetCredentialsErrorMsg();
            Thread.sleep(2000);

            String expected = MessageReader.getDynamicMessage("error.cce.resetCredentials");

            if (actual.contains(expected)) {

                pNode.pass("error message verified ");
                pNode.info("", MediaEntityBuilder
                        .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                pNode.info("Actual  :  " + actual);
                pNode.info("Expected  :  " + expected);

            } else {
                pNode.fail("error message not verified");

                pNode.info("", MediaEntityBuilder
                        .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                markAsFailure("Failure: Verify Message Contains! refer reports");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Method To Do Self Balance Enquiry.
     *
     * @param user
     * @return
     * @throws Exception
     */
    public void doSelfBalanceEnquiry(Object user) throws Exception {

        String password = null, loginId = null, categoryCode = null;
        if (user instanceof User) {
            password = ((User) user).Password;
            loginId = ((User) user).LoginId;
            categoryCode = ((User) user).CategoryCode;
        } else if (user instanceof OperatorUser) {
            password = ((OperatorUser) user).Password;
            loginId = ((OperatorUser) user).LoginId;
            categoryCode = ((OperatorUser) user).CategoryCode;
        } else if (user instanceof Biller) {
            loginId = ((Biller) user).LoginId;
            password = ((Biller) user).Password;
            categoryCode = ((Biller) user).CategoryCode;
        } else if (user instanceof PseudoUser) {
            loginId = ((PseudoUser) user).LoginId;
            password = ((PseudoUser) user).Password;
        }

        Double actualBalance = null;
        Markup m = MarkupHelper.createLabel("SelfBalanceEnquiry", ExtentColor.TEAL);
        pNode.info(m);
        SelfBalanceEnquiry_Page1 page = new SelfBalanceEnquiry_Page1(pNode);
        page.navigateToSelfBalanceEnquiryPage();
        page.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
        page.selectPaymentInstrument(Constants.PAYINST_WALLET_CONST);
        page.selectWalletType(Constants.NORMAL_WALLET);
        page.clickOnNextButton();
        page.enterPassword(password);
        page.clickOnSubmit();

        if (ConfigInput.isAssert) {
            Utils.captureScreen(pNode);
            actualBalance = page.getUserBalance();

            String userId = null;
            if (categoryCode.equals(Constants.BULK_PAYER_ADMIN))
                userId = MobiquityGUIQueries.getUserId(((OperatorUser) user).getParentUserName());
            else
                userId = MobiquityGUIQueries.getUserId(loginId);

            double userBalance = MobiquityDBAssertionQueries.getWalletBalanceByUserID(userId,
                    Constants.NORMAL_WALLET, DataFactory.getDefaultProvider().ProviderId).doubleValue();
            String expectedBalance = Double.toString(userBalance);
            Assertion.verifyEqual(actualBalance.toString(), expectedBalance, "Verify User Balance, UI vs DB", pNode);
        }


    }

    public String Last5Transactions(String userType, String AccountIdentifier, String filterData, String txnID) throws IOException {
        String mainWindow = null;

        try {
            Markup m = MarkupHelper.createLabel("Last5Transactions", ExtentColor.BLUE);
            pNode.info(m);
            ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(pNode);

            channelSubsEnqPg1.navCustomerCareExecutive();
            Thread.sleep(3000);

            // window handling
            mainWindow = driver.getWindowHandle();
            Set<String> s1 = driver.getWindowHandles();

            for (String s : s1) {
                WebDriver childWindow = driver.switchTo().window(s);

                if (childWindow.getTitle().equalsIgnoreCase("mobiquity")) {

                    channelSubsEnqPg1.selectUserTypeByVisibleText(userType);
                    channelSubsEnqPg1.selectAccountIdentifierByVisibleText(AccountIdentifier);
                    channelSubsEnqPg1.enterFilterData(filterData);
                    channelSubsEnqPg1.clickOnSubmit();
                    channelSubsEnqPg1.clickOnTransactionTab();
                    channelSubsEnqPg1.clickOnLast5Transactions();

                    driver.findElement(By.xpath("//*[@id='last_transactions_content']//td[.='" + txnID + "']/..//input")).click();

                    channelSubsEnqPg1.clickOnResendNotificationBtn();
                }
                if (ConfigInput.isAssert) {
                    String successMsg = channelSubsEnqPg1.pinSuccessMsg();
                    String expected = MessageReader.getDynamicMessage("cce.pin.change.success");

                    if (successMsg.contains(expected)) {

                        pNode.info("", MediaEntityBuilder
                                .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                        pNode.pass("success message verified ");
                        pNode.pass("Actual  :  " + successMsg);
                        pNode.pass("Expected  :  " + expected);


                    } else {
                        pNode.fail("success message not verified");

                        pNode.info("", MediaEntityBuilder
                                .createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

                        markAsFailure("Failure: Verify Message Contains! refer reports");
                    }
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return mainWindow;
    }

    public Enquiries performChannelSubsEnquiry(User user) throws IOException {
        // * add method markup
        try {
            ChannelSubsEnqPage1 pg1 = new ChannelSubsEnqPage1(pNode);

            pg1.navigateToChannelSubsEnquiry();
            pg1.selectUserTypeByValue(user);
            pg1.setMSISDN(user.MSISDN);

            //* remove thread sleep
            Utils.putThreadSleep(1000);
            pg1.clickProviderDdown();

            //* remove thread sleep
            Utils.putThreadSleep(3000);
            pg1.clickonSubmit();

        }catch (Exception e){
            Assertion.raiseExceptionAndStop(e,pNode);
        }

        return this;

    }

}

 