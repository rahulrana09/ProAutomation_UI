package framework.features.savingClubManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.savingsClubManagement.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

/**
 * Created by Dalia on 12-09-2017.
 */
public class SavingsClubManagement {
    private static ExtentTest pNode;
    private static MobiquityGUIQueries dbHandle;
    private static OperatorUser naAddClub, naAppClub, naModClub, naModAppClub,
            naDelClub, naDelAppClub, naViewClub, naTrfClubAdm, naTrfClubAdmApp,
            naClubViewReport, naClubTrfReport;

    public static SavingsClubManagement init(ExtentTest t1) throws Exception {
        pNode = t1;

        try {
            if (naAddClub == null) {
                dbHandle = new MobiquityGUIQueries();
                naAddClub = DataFactory.getOperatorUsersWithAccess("ADD_CLUB").get(0);
                naAppClub = DataFactory.getOperatorUsersWithAccess("APP_CLUB").get(0);
                naModClub = DataFactory.getOperatorUsersWithAccess("MOD_CLUB").get(0);
                naModAppClub = DataFactory.getOperatorUsersWithAccess("MODAPP_CLUB").get(0);
                naDelClub = DataFactory.getOperatorUsersWithAccess("DEL_CLUB").get(0);
                naDelAppClub = DataFactory.getOperatorUsersWithAccess("DELAPP_CLUB").get(0);
                naViewClub = DataFactory.getOperatorUsersWithAccess("VIEW_CLUB").get(0);
                naTrfClubAdm = DataFactory.getOperatorUsersWithAccess("TRF_CLUB_ADM").get(0);
                naTrfClubAdmApp = DataFactory.getOperatorUsersWithAccess("APPTRF_CLUB_ADM").get(0);
                naClubViewReport = DataFactory.getOperatorUsersWithAccess("CLUB_VIEW_REPORT").get(0);
                naClubTrfReport = DataFactory.getOperatorUsersWithAccess("CLUB_TRF_REPORT").get(0);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }

        return new SavingsClubManagement();
    }

    /**
     * Set SVC Preferences for Saving Club
     *
     * @param chairMan - for the user Saving Club need to be configured
     * @param bankId   - optional, else null
     * @throws Exception
     */
    public void setSVCPreferencePrerequisite(User chairMan, String bankId) throws Exception {
        Markup m = MarkupHelper.createLabel("setSVCPreferencePrerequisite", ExtentColor.BROWN);
        pNode.info(m);

        // if bank id is not provided then get the default bank of default provider
        if (bankId == null) {
            bankId = DataFactory.getDefaultBankIdForDefaultProvider();
        }

        //Updating the preference as display and modified allowed from GUI
        pNode.info("updateDisplayAndModifiedAllowedFromGUI - CLUB_BANK_ID to 'Y'");
        dbHandle.updateDisplayAndModifiedAllowedFromGUI("CLUB_BANK_ID", "Y");

        //Updating the bank id for the CLUB_BANK_ID preference
        SystemPreferenceManagement.init(pNode)
                .updateSystemPreference("CLUB_BANK_ID", bankId);

        Assertion.verifyActionMessageContain("mBanking.message.success.preferences.update", "Preference update successfully", pNode);

        /*
         * TODO
         * Below steps can be removed once UI is build to add preferences for creating CLUB
         */
        String runQuery = dbHandle.dbUpdateSVCGradePreferencesTable(chairMan.GradeCode, bankId);
        pNode.info("DQuery executed:" + runQuery);
    }


    public SavingsClubManagement createSavingsClub(SavingsClub sClub) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateSavingsClub", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        Login.init(pNode).login(naAddClub);
        initiateSavingsClub(sClub);

        if (AppConfig.isAddMakerReqClub) { // if the preference ADD_MAKER_REQ_CLUB is 'Y' club approval is required
            Login.init(pNode).login(naAppClub);
            approveSavingsClub(sClub);
        }
        return this;

    }

    public SavingsClubManagement initiateSavingsClub(SavingsClub sClub) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("initiateSavingsClub", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            AddClub_Page1 page1 = AddClub_Page1.init(pNode);

            page1.navigateToAddClubPage();
            page1.setClubName(sClub.ClubName);
            page1.setChairmanMsisdn(sClub.CMMsisdn);
            page1.setLocation(sClub.Location);
            page1.selectClubType(sClub.ClubType);
            page1.selectChairmanGrade(sClub.CMGradeName);
            page1.selectChairmanWalletTCP(sClub.ClubWalletTCP);
            page1.selectChairmanWalletMobileGroupRole(sClub.ClubWalletMobileGrpRole);
            page1.selectChairmanBankTCP(sClub.ClubBankTCP);
            page1.selectChairmanBankMobileGroupRole(sClub.ClubBankMobileGrpRole);

            // set Min and Max member and approved
            page1.setMinMembers(sClub.MinMemberCount);
            page1.setMinApprovers(sClub.MinApproverCount);
            page1.setMaxMembers(sClub.MaxMemberCount);
            page1.setMaxApprovers(sClub.MaxApproverCount);

            // submit
            page1.clickSubmit();

            /*
            Below block has to be maintained as after clicking on submit there can be multiple errors in a page
            to skip confirm for negative test, set isErrorExpected to false and execute the steps
             */
            if (ConfigInput.isConfirm) {
                AddClub_Confirm_Page1.init(pNode).clickConfirm();
            }

            if (ConfigInput.isAssert) {
                if(!AppConfig.isAddMakerReqClub){
                    Assertion.verifyActionMessageContain("saving.club.creation.success", "Club Added Successfully", pNode);
                } else {
                    Assertion.verifyActionMessageContain("saving.club.creation.initiated", "Successfully Initiated Savings Club Creation", pNode);
                    // set the Club id
                    String clubId = Assertion.getActionMessage().split(":")[1].trim();
                    sClub.setClubId(clubId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public SavingsClubManagement approveSavingsClub(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("approveSavingsClub", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            ApproveClub_Page1.init(pNode)
                    .navApproveSavingClub()
                    .selectClub(sClub.ClubName)
                    .approveClub()
                    .confirmAction();

            if (ConfigInput.isAssert) {
                if (Assertion.verifyActionMessageContain("saving.club.addition.approve.success", "Successfully Approved Club Addition", pNode)) {
                    sClub.isApproved = "Y"; // set the Approved flag as Y
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public void viewClubReport(String clubId) throws IOException {
        Markup m = MarkupHelper.createLabel("viewClubReport", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            Login.init(pNode).login(naAddClub);

            AddClub_Page1 page1 = AddClub_Page1.init(pNode);

            page1.navigateToClubReportPage();
            page1.SelectFromandToDateForApproval(new DateAndTime().getDate(-1), new DateAndTime().getDate(0));
            page1.clickViewReport();
            page1.verifyClubId(clubId);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }

    }

    public SavingsClubManagement approveClubModification(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("approverClubModification", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            ApproveClub_Page1.init(pNode)
                    .navModifyApproveClub()
                    .selectClub(sClub.ClubName)
                    .approveClub()
                    .confirmAction();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("savingclub.modified.approve", "Successfully Approved Club Modification", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    public SavingsClubManagement rejectSavingsClub(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("approveSavingsClub", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {
            ApproveClub_Page1.init(pNode)
                    .navApproveSavingClub()
                    .selectClub(sClub.ClubName)
                    .setRejectReason("Rejecting, For Testing Purpose...")
                    .rejectClub();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("saving.club.addition.reject.success", "Successfully Rejected Club Addition", pNode);
            }


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Modify Club using Club Id
     *
     * @param clubId
     * @throws IOException
     */
    public SavingsClubManagement initiateModifyClubUsingClubId(String clubId) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateModifyClubUsingClubId", ExtentColor.BLUE);
        pNode.info(m);
        try {
            new ModifyClub_Page1(pNode)
                    .navModifySavingClub()
                    .setClubId(clubId)
                    .clickSubmit();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Modify Club using Club Chairman MSISDn
     *
     * @param msisdn
     * @throws IOException
     */
    public SavingsClubManagement initiateModifyClubUsingChairmanMsisdn(String msisdn) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateModifyClubUsingChairmanMsisdn", ExtentColor.BLUE);
        pNode.info(m);
        try {
            new ModifyClub_Page1(pNode)
                    .navModifySavingClub()
                    .setCMMsisdn(msisdn)
                    .clickSubmit();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Initiate Modify Club using Club Name
     *
     * @param clubName
     * @throws IOException
     */
    public SavingsClubManagement initiateModifyClubUsingClubName(String clubName) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateModifyClubUsingClubName", ExtentColor.BLUE);
        pNode.info(m);
        try {
            new ModifyClub_Page1(pNode)
                    .navModifySavingClub()
                    .setClubNameInitials(clubName.substring(0, 2))
                    .selectClubName(clubName)
                    .clickSubmit();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Verify that the club information is intact
     * before modification validation
     *
     * @param sClub Todo
     */
    public SavingsClubManagement verifyInitiateClubModification(SavingsClub sClub) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyInitiateClubModification", ExtentColor.BLUE);
        pNode.info(m);
        try {

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    public SavingsClubManagement startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }

    public SavingsClubManagement expectErrorB4Confirm() {
        Markup m = MarkupHelper.createLabel("expectErrorB4Confirm", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public SavingsClubManagement initiateChairManTransfer(String clubId, String newCmMsisdn, String memberMsisdn, String reason, boolean isTobeRemoved) throws IOException {
        Markup m = MarkupHelper.createLabel("initiateChairManTransfer", ExtentColor.BLUE);
        pNode.info(m);
        try {
            Login.init(pNode)
                    .login(naTrfClubAdm);
            TransferCM_Page1.init(pNode)
                    .navSavingClubCMTransferInitiate()
                    .setClubId(clubId)
                    .clickOnSubmit()
                    .setNewChairmanMsisdn(newCmMsisdn)
                    .setClubMemberMsisdn(memberMsisdn)
                    .selectReasonForTransfer(reason)
                    .selectRemoveFromClub(isTobeRemoved)
                    .clickOnSubmit();

            if (ConfigInput.isAssert) {
                // assertion comes here
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }

        return this;
    }

    /**
     * Delete Saving Club
     *
     * @param clubId
     * @return
     * @throws Exception
     */
    public SavingsClubManagement deleteClub(String clubId) throws Exception {
        Login.init(pNode)
                .login(naDelClub);

        initiateClubDeletion(clubId);

        Login.init(pNode)
                .login(naDelAppClub);

        approveClubDeletion(clubId);

        return this;
    }

    /**
     * Initiate Saving Club Delete
     *
     * @param clubId
     * @return
     * @throws Exception
     */
    public SavingsClubManagement initiateClubDeletion(String clubId) throws Exception {
        Markup m = MarkupHelper.createLabel("initiateClubDeletion:" + clubId, ExtentColor.BLUE);
        pNode.info(m);

        try {
            DeleteClub_Page1.init(pNode)
                    .navDeleteSavingClub()
                    .setClubId(clubId)
                    .clickSubmit()
                    .clickConfirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("savingclub.delete.initiated", "Initiate Delete Saving Club", pNode);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }

    /**
     * Approve Saving Club Delete
     *
     * @param clubId
     * @return
     * @throws Exception
     */
    public SavingsClubManagement approveClubDeletion(String clubId) throws Exception {
        Markup m = MarkupHelper.createLabel("approveClubDeletion:" + clubId, ExtentColor.BLUE);
        pNode.info(m);

        try {
            ApproveDeleteClub_Page1.init(pNode)
                    .navApproveDeleteClub()
                    .selectClub(clubId)
                    .clickApprove()
                    .clickConfirm();

            if (ConfigInput.isAssert) {
                Assertion.verifyActionMessageContain("savingclub.delete.success", "Delete Saving Club", pNode);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
        return this;
    }
}
