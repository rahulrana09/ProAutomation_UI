package framework.features.channelEnquiry;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.channelEnquiry.ChannelEnquiry_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;

/**
 * Created by navin.pramanik on 9/7/2017.
 */
public class ChannelEnquiry {

    private static ExtentTest pNode;

    public static ChannelEnquiry init(ExtentTest t1) {
        pNode = t1;
        return new ChannelEnquiry();
    }


    public void getValidUserAndLogin() {
        try {
            OperatorUser optUsr = DataFactory.getOperatorUsersWithAccess("O2C_ENQ").get(0);
            Login.init(pNode).login(optUsr);
        } catch (Exception e) {
            pNode.warning("Users Not Found.. Please Check Operator User Sheet");
        }
    }

    public void doO2CTransferEnquiryUsingTxnID(String txnID) throws Exception {

        try {
            getValidUserAndLogin();

            Utils.createLabelForMethod("doO2CTransferEnquiryUsingTxnID",pNode);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(pNode);

            page.navChannelEnquiry();
            page.setTxnId(txnID);
            page.clickOnSubmit();
            page.selectTxnID(txnID);
            String webStatus = page.extractTxnStatus(txnID);
            if (ConfigInput.isConfirm) {
                Thread.sleep(1200);
                page.clickOnFinalSubmit();
            }

            if (ConfigInput.isAssert) {
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
                Assertion.verifyEqual(webStatus, dbStatus, "Verify Transaction Status", pNode);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void doO2CTransferEnquiryUsingMobileNumber(String msisdn, String txnID, boolean... isRequired) throws Exception {

        try {
            getValidUserAndLogin();
            boolean value = true;
            if (isRequired.length > 0) {
                value = isRequired[0];
            }

            Markup m = MarkupHelper.createLabel("doO2CTransferEnquiryUsingMobileNumber", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(pNode);

            page.navChannelEnquiry();
            page.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
            page.selectPayInstrument(Constants.PAYINST_WALLET_CONST);
            if (value) {
                page.selectPayInstrumentType(DataFactory.getDefaultWallet().WalletId);
            }
            page.setMSISDN(msisdn);
            Utils.putThreadSleep(1000);
            Utils.captureScreen(pNode);
            page.clickOnSubmit();

            if (ConfigInput.isAssert) {
                page.selectTxnID(txnID);
                String webStatus = page.extractTxnStatus(txnID);
                page.clickOnFinalSubmit();
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
                Assertion.verifyEqual(webStatus, dbStatus, "Verify Transaction Status", pNode);
            }

        } catch (
                Exception e)

        {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void doO2CTransferEnquiryUsingDateRange(String txnID, String fromDate, String toDate, String... status) throws Exception {

        try {
            getValidUserAndLogin();
            String value = null;
            if (status.length > 0) {
                value = status[0];
            }

            Markup m = MarkupHelper.createLabel("doO2CTransferEnquiryUsingDateRange", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(pNode);

            page.navChannelEnquiry();
            page.selectMFSProvider1(DataFactory.getDefaultProvider().ProviderId);
            page.selectPayInstrument1(Constants.PAYINST_WALLET_CONST);
            page.selectPayInstrumentType1(DataFactory.getDefaultWallet().WalletId);
            page.setFromDate(fromDate);
            page.setToDate(toDate);
            if (value != null) {
                page.selectTxnStatus(value);
            }
            page.clickOnSubmit();

            if (ConfigInput.isAssert) {
                page.selectTxnID(txnID);

                String webStatus = page.extractTxnStatus(txnID);
                page.clickOnFinalSubmit();
                Utils.putThreadSleep(5000);
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
                Assertion.verifyEqual(webStatus, dbStatus, "Verify Transaction Status", pNode);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void doO2CTransferEnquiryUsingAllValue(String msisdn, String txnID) throws Exception {

        try {
            getValidUserAndLogin();

            Markup m = MarkupHelper.createLabel("doO2CTransferEnquiryUsingAllValue", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(pNode);

            page.navChannelEnquiry();
            page.setTxnId(txnID);
            page.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
            page.selectPayInstrument(Constants.PAYINST_WALLET_CONST);
            page.selectPayInstrumentType(DataFactory.getDefaultWallet().WalletId);
            page.setMSISDN(msisdn);
            page.clickOnSubmit();
            Assertion.verifyErrorMessageContain("userenq.EnterOneValue.error", "Verify Error Message", pNode);
        } catch (
                Exception e)

        {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public void doO2CTransferEnquiryWithoutPaymentID(String msisdn, String txnID) throws Exception {

        try {
            getValidUserAndLogin();

            Markup m = MarkupHelper.createLabel("doO2CTransferEnquiryWithoutPaymentID", ExtentColor.ORANGE);
            pNode.info(m);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(pNode);

            page.navChannelEnquiry();
            page.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
            page.setMSISDN(msisdn);
            page.clickOnSubmit();
            Assertion.verifyErrorMessageContain("userenq.paymentMethod.error", "Verify Error Message", pNode);
        } catch (
                Exception e)

        {
            Assertion.raiseExceptionAndContinue(e, pNode);
        }
    }

    public ChannelEnquiry negativeTestConfirmation() {
        Markup m = MarkupHelper.createLabel("Setting Confirmation flag for Negative Tests", ExtentColor.BLUE);
        // Set the confirm flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isConfirm = false;
        return this;
    }

    public ChannelEnquiry startNegativeTest() {
        Markup m = MarkupHelper.createLabel("Starting Negative Test", ExtentColor.BROWN);
        // Set the assert flag to false
        // this will be reset once the method has completed in the @AfterMethod
        ConfigInput.isAssert = false;
        return this;
    }
}

