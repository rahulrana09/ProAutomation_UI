package framework.features.bulkUserDeletion;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.bulkPayoutTool.BulkPayoutDashboard_page1;
import framework.pageObjects.bulkUserDeletion.BulkUserDeletion_pg1;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;

import java.io.*;
import java.util.*;

public class BulkUserDeletion {

    private static final String COMMA_DELIMITER = ",";
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static ExtentTest pNode;
    String csvFileHeader = "MSISDN,USER_TYPE";
    private FileWriter fileWriter = null;

    public static BulkUserDeletion init(ExtentTest t1) {
        pNode = t1;
        return new BulkUserDeletion();
    }

    public String generateFileForDeletion(HashMap<String, String> msisdnList) throws Exception {

        Markup m = MarkupHelper.createLabel("generateFileForDeletion", ExtentColor.TEAL);
        pNode.info(m); // Method Start Marker

        Thread.sleep(2000);
        Set<String> msisdn = msisdnList.keySet();

        pNode.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_USER_DELETION);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_USER_DELETION); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        File f = new File(FilePath.dirFileDownloads + "BulkUserDeletion.csv");

        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(f, true));
            e.write(csvFileHeader);
            e.newLine();
            for (String mobNo : msisdn) {
                e.write(mobNo + "," + msisdnList.get(mobNo));
                e.newLine();
            }

            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pNode);
        }

        return f.getAbsolutePath();
    }

    public void performBulkUserDeletion(String fileName) throws Exception {

        Markup m = MarkupHelper.createLabel("performBulkUserDeletion", ExtentColor.ORANGE);
        pNode.info(m);

        BulkUserDeletion_pg1 page1 = new BulkUserDeletion_pg1(pNode);
        page1.navBulkUserDelete();
        page1.setUploadFile(fileName);
        page1.clickSubmit();
        Utils.putThreadSleep(5000);
        page1.downloadLogFile();
        Assertion.verifyErrorMessageContain("bulkUserDeletion.uploadMessage", "Verify Bulk User Delete ", pNode);

    }


    public void verifyLogs(Map<String, String> msisdn_expMsg) throws Exception {
        Markup m = MarkupHelper.createLabel("verifyLogs", ExtentColor.BLUE);
        pNode.info(m);

        try {
            BulkUserDeletion_pg1 page1 = new BulkUserDeletion_pg1(pNode);
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserDeletionLogs");

            Thread.sleep(5000);
            page1.downloadLogFile();

            pNode.info("Click on the download Log file");
            pNode.info("Downloading file at location " + FilePath.dirFileDownloads);

            Thread.sleep(5000);
            String logFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserDeletionLogs");
            System.out.println(logFile);

            Set<String> keys = msisdn_expMsg.keySet();
            File file = new File(logFile);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String data = bufferedReader.readLine();
            while (data != null) {
                for (String mobNum : keys) {
                    if (data.contains(mobNum)) {
                        if (data.contains(msisdn_expMsg.get(mobNum))) {
                            Assertion.verifyContains(data, msisdn_expMsg.get(mobNum).toString(),
                                    "Verify Message From Log file", pNode);
                        }
                    }
                }
                data = bufferedReader.readLine();
            }

        } catch (Exception e) {
            System.out.println(e);
            Assertion.raiseExceptionAndStop(e, pNode);
        }
    }

}
