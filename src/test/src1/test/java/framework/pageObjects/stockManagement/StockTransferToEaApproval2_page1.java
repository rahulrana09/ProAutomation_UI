/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Transfer To EA Approval Level 2 page 1
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class StockTransferToEaApproval2_page1 extends PageInit {

    @FindBy(xpath = "//*[@type='radio']")
    private WebElement radioBtn;
    @FindBy(id = "stockTransferApprove2_approve_button_submit")
    private WebElement submitBtn;
    @FindBy(id = "stockTransferApprove2_confirmApproval_button_reject")
    private WebElement rejectTransferLev2;

    public StockTransferToEaApproval2_page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToEAStockApprovalLevel2() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STR_APP2", "EA stock approval L2");
    }


    public void radioBtn_Click() {
        clickOnElement(radioBtn, "Radio Button");
    }


    public void selectTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + transID + "']"));
        clickOnElement(el, "TRansaction ID: " + transID);
    }

    public String getDate(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[2]"));
        return el.getText();
    }

    public String getReferenceNumber(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[3]"));
        return el.getText();
    }

    public String getTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[4]"));
        return el.getText();
    }

    public String getCurrentStatus(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[6]"));
        return el.getText();
    }

    public void clickSubmit() {
        clickOnElement(submitBtn, "Submit");
    }

    public void clickOnRejectButton() {
        clickOnElement(rejectTransferLev2, "RejectButton");
    }

    public List<WebElement> getTableHeaders() {
        //ArrayList<String> list = new ArrayList<>();
        List<WebElement> list = driver.findElements(By.xpath(".//*[@class='tabhead']"));
        return list;
    }

}