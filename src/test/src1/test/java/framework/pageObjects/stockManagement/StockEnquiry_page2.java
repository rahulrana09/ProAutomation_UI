/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Stock Enquiry Page 1
*/
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockEnquiry_page2 extends PageInit {
    @FindBy(id = "stockEnq_view_button_submit")
    private WebElement submitButton;
    @FindBy(id = "stockEnq_view_submit")
    private WebElement backButton;
    @FindBy(id = "stockEnq_view_transactionMap_currentStatus")
    private WebElement statusField;
    @FindBy(id = "stockEnq_view_transactionMap_transferId")
    private WebElement tidValueFromPage;

    public StockEnquiry_page2(ExtentTest t1) {
        super(t1);
    }

    public void submitButton_Click() {
        clickOnElement(submitButton, "submit Button");
    }

    public void backButton_Click() {
        clickOnElement(backButton, "back Button");

    }

    public String getTransactionID() {
        return tidValueFromPage.getText();
    }

    public void VerifyTransID(String Tid) {
        String id = driver.findElement(By.xpath("//*[@id='stockEnq_view_transactionMap_transferId']")).getText();
        if (id.contains(Tid) || id.equalsIgnoreCase(Tid)) {
            pageInfo.info("Verified Transaction ID");
        } else {
            pageInfo.fail("Transaction  ID Didn't matched");
        }
    }

    public void VerifyAmount(String Amount) {
        String Amt = driver.findElement(By.xpath("//*[@id='stockEnq_view']/table/tbody/tr[8]/td[3]")).getText();
        if (Amt.contains(Amount) || Amt.equalsIgnoreCase(Amount)) {
            pageInfo.info("Verified Amount ");
        } else {
            pageInfo.fail("Transaction  Amount Didn't Matched");
        }
    }


    public void selectTransaction(String tid) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + tid + "']"));
        el.click();
    }

    public String getTransactionStatus() {
        return statusField.getText();
    }
}
