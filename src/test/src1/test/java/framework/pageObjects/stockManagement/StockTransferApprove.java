/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Transfer Approval
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockTransferApprove extends PageInit {

    @FindBy(xpath = "//*[@type='radio']")
    private WebElement radioBtn;
    @FindBy(id = "stockApprove1_approve_button_submit")
    private WebElement submitBtnLev1Approval;
    @FindBy(id = "stockTransferApprove2_approve_button_submit")
    private WebElement submitBtnLev2Approval;
    @FindBy(id = "action:stockApprove1_confirmApproval")
    private WebElement confirmTransferLev1;
    @FindBy(id = "stockApprove1_confirmApproval_button_reject")
    private WebElement rejectTransferLev1;
    @FindBy(id = "stockTransferApprove2_confirmApproval_button_approve")
    private WebElement confirmTransferLev2;
    @FindBy(id = "stockTransferApprove2_confirmApproval_button_reject")
    private WebElement rejectTransferLev2;
    @FindBy(id = "stockTransferApprove2_confirmApproval__requestedValue")
    private WebElement amount;
    @FindBy(id = "stockTransferApprove2_confirmApproval_button_reject")
    private WebElement remarkslevel2;
    @FindBy(id = "stockTransferApprove2_confirmApproval_back")
    private WebElement back;

    public StockTransferApprove(ExtentTest t1) {
        super(t1);
    }

    public void backClick() {
        clickOnElement(back, "Back Button");
    }

    public void enterRemaksLevel2(String remark) {
        setText(rejectTransferLev2, remark, "remarks level2");
    }


    public void clickConfirmTransferLev1() {
        clickOnElement(confirmTransferLev1, "confirm Button");
    }

    public void clickRejectTransferLev1() {
        clickOnElement(rejectTransferLev1, "Reject Button");

    }


    public Boolean verifyAmount() {
        try {
            String old = amount.getText();
            amount.sendKeys("50");
            if (old.contains(amount.getText())) {
                pageInfo.pass("Amount Field is not Editable");
                return false;
            } else {
                pageInfo.fail("Amount Should not be Editable ");
                return true;
            }

        } catch (Exception e) {
            pageInfo.pass("Amount Field is not Editable");
        }
        return true;
    }

    public void clickConfirmTransferLev2() {
        clickOnElement(confirmTransferLev2, "Confirm Button");
    }

    public void clickRejectTransferLev2() {
        clickOnElement(rejectTransferLev2, "Reject Button");
    }

    public void navApproveStockTranasferLev1() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_APP1", "Stock Transfer Approval Level 1");
    }

    public void navApproveStockTranasferLev2() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_APP2", "Stock Transfer Approval Level 2");
    }

    //Select transaction ID
    public void selectRadioButton(String transactionId) {
        driver.findElement(By.xpath(".//*[@value='" + transactionId + "']")).click();
    }

    public void selectTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + transID + "']"));
        el.click();
        pageInfo.info("Radio Button for selecting the transction " + el.getText());
    }


    public String getDate(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[2]"));
        return el.getText();
    }

    public String getReferenceNumber(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[3]"));
        return el.getText();
    }

    public String getTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[4]"));
        return el.getText();
    }


    public String getCurrentStatus(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[6]"));
        return el.getText();
    }


    public void clickSubmitLev1Approval() {
        clickOnElement(submitBtnLev1Approval, " Submit Button");
    }

    public void clickSubmitLev2Approval() {
        clickOnElement(submitBtnLev2Approval, " Submit Button");
    }


}
