/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Transfer Ea Enquiry page1
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class StockTransferEaEnquiry_page1 extends PageInit {

    @FindBy(id = "stockEnq_loadDetails_txnId")
    private WebElement transactionID;
    @FindBy(id = "stockEnq_loadDetails_button_submit")
    private WebElement submitButton;
    @FindBy(name = "inputFromDate")
    private WebElement fromDate;
    @FindBy(name = "inputToDate")
    private WebElement toDate;
    @FindBy(id = "stockEnq_loadDetails_status")
    private WebElement status;

    public StockTransferEaEnquiry_page1(ExtentTest t1) {
        super(t1);
    }

    public StockTransferEaEnquiry_page1 navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STR_ENQ", "EA Transfer Enquiry");
        return this;
    }

    public StockTransferEaEnquiry_page1 transactionID_SetText(String transID) {
        setText(transactionID, transID, "transactionID");
        return this;
    }

    public StockTransferEaEnquiry_page1 fromDate_SetText(String text) {
        setDate(fromDate, text, "fromDate");
        return this;
    }

    public StockTransferEaEnquiry_page1 toDate_SetText(String text) {
        setDate(toDate, text, "toDate");
        return this;
    }

    public StockTransferEaEnquiry_page1 status_SelectValue(String text) {
        selectValue(status, text, "status");
        return this;
    }

    public StockTransferEaEnquiry_page2 submitButton_Click() {
        clickOnElement(submitButton, "submitButton");
        return new StockTransferEaEnquiry_page2(pageInfo);
    }


}
