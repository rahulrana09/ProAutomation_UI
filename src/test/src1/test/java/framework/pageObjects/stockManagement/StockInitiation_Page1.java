/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of StockInitiationPage1
*/
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class StockInitiation_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "bankId")
    private WebElement BankName;

    @FindBy(id = "stockInit_confirmInitiate__refNo")
    private WebElement ReferenceNumber;

    @FindBy(id = "stockInit_confirmInitiate__requestedQuantity")
    private WebElement Amount;

    @FindBy(id = "mfsProvidersList")
    private WebElement providerName;

    @FindBy(id = "stockInit_confirmInitiate__requesterRemarks")
    private WebElement Remarks;

    @FindBy(id = "stockInit_confirmInitiate_button_submit")
    private WebElement submit;

    @FindBy(id = "stockButt")
    private WebElement confirm;


    public StockInitiation_Page1(ExtentTest t1) {
        super(t1);
    }

    public static StockInitiation_Page1 init(ExtentTest t1) {
        return new StockInitiation_Page1(t1);
    }

    /**
     * Navigate to Stock Management >  Stock Initiation page
     *
     * @throws Exception
     */
    public StockInitiation_Page1 navigateToStockInitiationPage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_INIT", "Stock Initiation Page");
        return this;
    }

    //Select Provider
    public StockInitiation_Page1 selectProviderName(String providerName) throws Exception {
        selectVisibleText(this.providerName, providerName, "providerName");
        return this;
    }

    public void setReferenceNumber(String text) throws Exception {
        setText(ReferenceNumber, text, "Reference Number");
    }

    public void setStockAmount(String text) throws Exception {
        setText(Amount, text, "Amount");
    }

    public void setRemarks(String remarks) throws Exception {
        setText(Remarks, remarks, "Remarks");
    }

    //Select Default Bank
    public void selectBankName(String bankName) throws Exception {
        selectVisibleText(BankName, bankName, "Bank Name");
    }

    //Select Default Bank
    public void selectBankNameIndex(int bankName) throws Exception {
        selectIndex(BankName, bankName, "Bank Name");
    }

    public List<String> getBankNameOptionsValues() {
        return fl.getOptionText(BankName);
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    //Click Confirm button
    public void clickConfirm() {
        clickOnElement(confirm, "confirm Button");
    }

}
