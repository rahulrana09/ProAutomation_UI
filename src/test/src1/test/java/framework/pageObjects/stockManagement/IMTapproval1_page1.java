/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of IMT Approval Level 1
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IMTapproval1_page1 extends PageInit {

    @FindBy(xpath = "//*[@type='radio']")
    private WebElement radioBtn;
    @FindBy(id = "stockIMTApprove1_approve_button_submit")
    private WebElement submitBtn;
    @FindBy(id = "stockIMTApprove1_approve_button_approve")
    private WebElement approveBtn;
    @FindBy(id = "stockIMTApprove1_approve_button_reject")
    private WebElement rejectBtn;
    @FindBy(id = "stockIMTApprove1_approve__approverRemarks")
    private WebElement remarksBtn;
    @FindBy(id = "stockIMTApprove1_approve_back")
    private WebElement backBtn;

    public IMTapproval1_page1(ExtentTest t1) {
        super(t1);
    }

    public void backButtonClick() {
        clickOnElement(backBtn, "Back Button");

    }

    public void enterRemarks(String remarks) {
        setText(remarksBtn, remarks, "Remarks");
    }

    public void NavigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_IMT_APPROVAL1", "Approve IMT level 1");
    }


    public void radioBtn_Click() {
        clickOnElement(radioBtn, "Radio Button");
    }


    public void selectTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + transID + "']"));
        el.click();
        pageInfo.info("Radio Button for selecting the transction " + transID);
    }

    public String getDate(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[2]"));
        return el.getText();
    }

    public String getReferenceNumber(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[3]"));
        return el.getText();
    }

    public String getTransactionID(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[4]"));
        return el.getText();
    }

    public String getCurrentStatus(String transID) {
        WebElement el = driver.findElement(By.xpath("//table/tbody/tr/td/input[@type='radio' and @value='" + transID + "']/following::td[6]"));
        return el.getText();
    }


    public void submitBtn_Click() {
        clickOnElement(submitBtn, "Submit Button");
    }

    public void approveBtn_Click() {
        clickOnElement(approveBtn, "approve Button");
    }


    public void rejectBtn_Click() {
        clickOnElement(rejectBtn, "reject Button");

    }
}