package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 6/21/2017.
 */
public class StockTransferInitiate_pg1 extends PageInit {

    @FindBy(name = "_refNo")
    public WebElement refNo;
    @FindBy(name = "payeeProviderId")
    WebElement mfsProvidersList;
    @FindBy(name = "_requestedQuantity")
    WebElement requestAmount;
    @FindBy(name = "_requesterRemarks")
    WebElement remark;
    @FindBy(name = "button.submit")
    WebElement submitButtonStockInitiate;
    @FindBy(id = "stockButt")
    WebElement confirmButtonStockInitiate;

    public StockTransferInitiate_pg1(ExtentTest t1) {
        super(t1);
    }

    public static StockTransferInitiate_pg1 init(ExtentTest t1) {
        return new StockTransferInitiate_pg1(t1);
    }

    public void navStockTransferInitiatePage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STR_INIT", "EA Stock Initiate Page");
    }

    public void selectMfsProvider(String provider) {
        selectValue(mfsProvidersList, provider, "mfsProvidersList");
    }

    public void refNo_SetText(String text) {
        setText(refNo, text, "Reference Number");
    }

    public void requestAmount_SetText(String text) {
        setText(requestAmount, text, "Request Amount");
    }

    public void remark_SetText(String text) {
        setText(remark, text, "Remarks");
    }

    public void clickOnSubmitTransferInitiate() {
        clickOnElement(submitButtonStockInitiate, "Submit Button");
    }

    public void clickOnConfirmTransferInitiate() {
        clickOnElement(confirmButtonStockInitiate, "confirm Button");
    }
}
