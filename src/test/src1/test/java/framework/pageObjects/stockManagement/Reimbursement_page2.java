/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Reimbursement Page2
*/
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.ArrayList;
import java.util.List;


public class Reimbursement_page2 extends PageInit {

    @FindBy(id = "withdraw_confirmDisplay_amount")
    private WebElement amount;
    @FindBy(id = "withdraw_confirmDisplay_butn")
    private WebElement confirm;
    @FindBy(id = "withdraw_confirmDisplay_back")
    private WebElement back;
    @FindBys({@FindBy(xpath = "//form[@id='withdraw_confirmDisplay']/table/tbody/tr/td[2]/label")})
    private List<WebElement> details;
    @FindBy(xpath = "//td[contains(text(),'Payer Balance')]/..//td[2]")
    private WebElement payerBal;
    @FindBys({@FindBy(xpath = "//form[@id='withdraw_back2']/table/tbody/tr/td[2]/label")})
    private List<WebElement> backDetails;
    @FindBy(id = "withdraw_back2_amount")
    private WebElement backAmount;
    @FindBy(id = "withdraw_back2_back")
    private WebElement back1;

    public Reimbursement_page2(ExtentTest t1) {
        super(t1);
    }

    public void back_Click() {
        clickOnElement(back, "back");
    }


    public Reimbursement_page2 amount_SetText(String text) {
        setTextUsingJs(amount, text, "Amount");
        return this;
    }


    public Reimbursement_page2 confirm_Click() {
        clickOnElement(confirm, "confirm");
        return this;
    }

    public List<String> returnDetails() {
        ArrayList<String> label = new ArrayList<String>();

        for (WebElement det : details) {
            label.add(det.getText());
        }
        return label;
    }

    public String getPayerBalance() {
        String value = payerBal.getText();
        return value;
    }

    public List<String> getLabelDetails() {
        ArrayList<String> label = new ArrayList<String>();

        for (WebElement det : backDetails) {
            label.add(det.getText());
        }
        return label;
    }

    public String getAmount() {
        String amt = backAmount.getAttribute("value");
        return amt;
    }

    public void back_Click1() {
        clickOnElement(back1, "back");
    }

}
