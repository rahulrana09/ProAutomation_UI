package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StockTransferRAInitiation_Approval_Pg2 extends PageInit {


    @FindBy(name = "button.submit")
    WebElement submitBtn;
    @FindBy(id = "stockButt")
    WebElement confirmBtn;
    @FindBy(id = "stockInitLoyalty_confirmInitiate_providerName")
    WebElement provider;
    @FindBy(id = "stockLoyaltyApprove2_approve__approverRemarks")
    WebElement remarks;
    @FindBy(name = "button.submit")
    WebElement confirmSubmit;
    @FindBy(id = "stockLoyaltyApprove2_approve_button_approve")
    WebElement confirmApprove;

    public StockTransferRAInitiation_Approval_Pg2(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_LOYALTY_APPR2", " Stock Transfer to RA Approval");
    }


    public void selectProvider(String value) {
        selectVisibleText(provider, value, "Provider Name");
    }

    public void clickOnSubmit() {
        clickOnElement(submitBtn, "Submit Button");
    }

    public void clickOnConfirm() {
        clickOnElement(confirmBtn, "Confirm Button");
    }


    public void clickRadioBtn(String txnid) {
        driver.findElement(By.xpath("//input[@name='check' and contains(@value,'" + txnid + "')]")).click();
        pageInfo.info("Click on the iniated stock");
    }

    public void clickOnSubmitBtn() {
        clickOnElement(confirmSubmit, "Submit Button");
    }


    public void clickOnApproveBtn() {
        clickOnElement(confirmApprove, "Confirm Button");
    }

    public void setRemarks(String value) {
        setText(remarks, value, "Remarks");
    }
}
