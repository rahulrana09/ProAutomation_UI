/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of Reimbursement Approval Page1
*/
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class ReimbursementApproval_page1 extends PageInit {


    @FindBy(id = "OPTWithdrawlActionApproval_displayTransactionDetails_button_submit")
    private WebElement submit;
    @FindBys({@FindBy(xpath = "(//form[@id='OPTWithdrawlActionApproval_displayTransactionDetails']/table/tbody/tr/td[2])[2]")})
    private List<WebElement> requestor;
    @FindBys({@FindBy(xpath = "(//form[@id='OPTWithdrawlActionApproval_displayTransactionDetails']/table/tbody/tr/td[5])[2]")})
    private List<WebElement> txnids;

    public ReimbursementApproval_page1(ExtentTest t1) {
        super(t1);
    }

    public void navigate() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_REAP", "Stock Reimbursement");
    }

    public void selectTransaction(String tid) {
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        WebElement el = DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + tid + "')]/ancestor::tr[1]/td[1]/input[@type='radio']"));
        el.click();
        pageInfo.info("Selecting txnId :" + tid);
    }

    public void submit_Click() {
        clickOnElement(submit, "submit");
    }

    public List<WebElement> getRequestors() {
        return requestor;
    }

    public List<WebElement> getTxnIds() {
        return txnids;
    }

}
