/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Withdrawal page
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class StockWithdrawal_page1 extends PageInit {

    @FindBy(id = "stockWithdraw_walletId")
    private WebElement walletID;
    @FindBy(id = "bankId")
    private WebElement bank;
    @FindBy(id = "sel2")
    private WebElement bankAccno;
    @FindBy(id = "mfsProvidersList")
    private WebElement provider;
    @FindBy(id = "stockWithdraw_amount")
    private WebElement transferAmt;
    @FindBy(id = "stockWithdraw_button_submit")
    private WebElement submit;
    @FindBy(xpath = "//*[@id=\"stockWithdraw\"]/table/tbody/tr[2]/td[3]/a")
    private WebElement availableBalanceLink;
    @FindBy(id = "stockWithdraw_availableBalance")
    private WebElement availableBalance;
    @FindBy(id = "stockWithdraw_initiatedBalance")
    private WebElement transactionInitiatedBalance;

    public StockWithdrawal_page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * navToStockWithdrawalPage
     */
    public StockWithdrawal_page1 navToStockWithdrawalPage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_WITHDRAWDM", "Stock Withdrawal");
        return this;
    }


    public void bankAccno_SelectDefault() {
        bankAccno.click();
        Select s_bankAccno = new Select(bankAccno);
        //Get all the Options
        List<WebElement> el = s_bankAccno.getOptions();
        if (el.size() > 0) {
            String topval = el.get(0).getText().toLowerCase();
            if (topval.equalsIgnoreCase("Select")) {
                s_bankAccno.selectByIndex(1);
                pageInfo.info("Select Account No. at index 1!");
            } else {
                s_bankAccno.selectByIndex(0);
                pageInfo.info("Select Account No. at index 0!");
            }
        }

    }


    public void walletID_Select(String walletType) {
        selectVisibleText(walletID, walletType, "Wallet type");
    }

    public void provider_SelectIndex(int index) {

        selectIndex(provider, index, "provider");
    }

    public void bank_SelectIndex(int index) {

        selectIndex(bank, index, "Bank");
    }

    public void selectProviderByValue(String id) {
        Select s_provider = new Select(provider);
        s_provider.selectByValue(id);
        pageInfo.info("Selected Provider id: " + id);
        selectValue(provider, id, "provider");
    }

    public void selectBankName(String name) {
        selectVisibleText(bank, name, "bank");
    }


    public void bankAccno_SelectIndex(int index) {
        selectIndex(bankAccno, index, "bankAccno");
    }


    public void setTransferAmount(String amount) {
        setText(transferAmt, amount, "Transfer Amount");
    }

    public void submit_Click() {
        clickOnElement(submit, "submit Button");
    }

    public void transactionInitiatedBalanceCheck(String amt) {
        clickOnElement(transactionInitiatedBalance, "transactionInitiatedBalance");
        if (transactionInitiatedBalance.getText().compareToIgnoreCase(amt) == 0) {
            pageInfo.info("TransactionInitiated balance found as" + transactionInitiatedBalance.getText());
            pageInfo.pass("TransactionInitiated balance validation passed");
        } else {
            pageInfo.info("TransactionInitiated balance found as" + transactionInitiatedBalance.getText() + "expected was:" + amt);
            pageInfo.fail("TransactionInitiated balance validation failed");
        }
    }

    public void availableBalanceLinkCheck(String amt) throws Exception {
        clickOnElement(availableBalanceLink, "availableBalanceLink");
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        boolean status = false;
        if (availableBalance.getText().compareToIgnoreCase(amt) == 0) {
            status = true;
        }
        Assertion.verifyEqual(status, true, "Available balance found as" + availableBalance.getText(), pageInfo, true);
    }

    public List<String> getBankNameOptionsValues() throws Exception {
        return fl.getOptionText(bank);
    }

    public WebElement getWalletElement() {
        return walletID;
    }
}