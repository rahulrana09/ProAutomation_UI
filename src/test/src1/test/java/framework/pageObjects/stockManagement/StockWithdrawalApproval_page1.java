/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Withdrawal page
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.List;


public class StockWithdrawalApproval_page1 extends PageInit {

    @FindBy(id = "StockWithdrawApprove_displayTransactionDetails_button_submit")
    private WebElement btnSubmitRequest;

    @FindBy(id = "StockWithdrawApprove_displayTransactionDetails_remark")
    private WebElement txtRemark;

    @FindBy(id = "StockWithdrawApprove_displayTransactionDetails_button_approve")
    private WebElement btnApproveRequest;

    @FindBy(id = "StockWithdrawApprove_displayTransactionDetails_button_reject")
    private WebElement btnRejectRequest;

    public void clickOnSubmit(){
        clickOnElement(btnSubmitRequest, "Submit");
    }

    public void clickOnApprove(){
        clickOnElement(btnApproveRequest, "Approve");
    }

    public void clickOnReject(){
        clickOnElement(btnRejectRequest, "Reject");
    }

    public void setRemark(String remark){
        setText(txtRemark, remark, "Remark");
    }

    public StockWithdrawalApproval_page1(ExtentTest t1) {
        super(t1);
    }

    public static StockWithdrawalApproval_page1 init(ExtentTest t1) {
        return new StockWithdrawalApproval_page1(t1);
    }

    public StockWithdrawalApproval_page1 navApproveStockWithdrawal() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_WITHDRAW_APPDM", "Stock Withdrawal Approval");
        return this;
    }

    public boolean isRequestAvailableForApproval(String requestId) {
        try {
            Utils.captureScreen(pageInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<WebElement> elemList = DriverFactory.getDriver().findElements(By.xpath("//tr/td[contains(text(), '" + requestId + "')]"));
        if (elemList.size() > 0)
            return true;

        return false;
    }

    public Boolean isApprovalLinkPresentInLeftNavigation() throws Exception {
        fl.clickLink("STOCK_ALL");
        try {
            Utils.captureScreen(pageInfo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<WebElement> elemList = DriverFactory.getDriver().findElements(By.id("STOCK_STK_WITHDRAW_APPDM"));
        if (elemList.size() > 0)
            return true;

        return false;

    }

    public void selectRequestId(String requestId) {
        WebElement ele = wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//tr/td[contains(text(), '" + requestId + "')]/ancestor::tr[1]/td/input[@type='radio']")));
        clickOnElement(ele, "RequestId: "+ requestId);
    }

}