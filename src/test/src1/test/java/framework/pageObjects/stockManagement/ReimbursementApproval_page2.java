/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Prashant Kumar
 *  Date: 9/12/2017
 *  Purpose: Page Object of Reimbursement Approval Page2
 */
package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ReimbursementApproval_page2 extends PageInit {


    @FindBy(id = "confirmApprove")
    private WebElement approve;
    @FindBy(id = "confirmReject")
    private WebElement reject;

    @FindBy(id = "OPTWithdrawlActionApproval_displayTransactionDetails_remark")
    private WebElement approverRemark;

    public ReimbursementApproval_page2(ExtentTest t1) {
        super(t1);
    }

    public void approve_Click() {
        clickOnElement(approve, "approve");
    }

    public void reject_Click() {
        clickOnElement(reject, "reject");
    }

    public void setRemark(String remark) {
        setText(approverRemark, remark, "Remark");
    }
}
