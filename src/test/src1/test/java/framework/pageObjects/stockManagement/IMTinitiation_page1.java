/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page Object of IMT initiation page
*/

package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Stock initaite page 1
 *
 * @author sapan.dang
 */
public class IMTinitiation_page1 extends PageInit {


    @FindBy(id = "stockInitIMT_confirmInitiate__requesterRemarks")
    public WebElement remark;
    @FindBy(id = "stockInitIMT_confirmInitiate_bankId")
    private WebElement remittancePartner;
    @FindBy(id = "mfsProvidersList")
    private WebElement provider;
    @FindBy(id = "stockInitIMT_confirmInitiate__refNo")
    private WebElement refNo;
    @FindBy(id = "stockInitIMT_confirmInitiate__requestedQuantity")
    private WebElement requestedAmount;
    @FindBy(id = "stockInitIMT_confirmInitiate_button_submit")
    private WebElement submitButton;

    public IMTinitiation_page1(ExtentTest t1) {
        super(t1);
    }

    public static IMTinitiation_page1 init(ExtentTest t1) {
        return new IMTinitiation_page1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_STK_IMT_INI", "Initaite IMT page");
    }


    public void remittancePartner_SelectIndex(int index) {
        selectIndex(remittancePartner, index, "Remittance Partner");
    }

    public void provider_SelectIndex(int index) {
        selectIndex(provider, index, "provider");
    }


    public void refNo_SetText(String ref) {
        setText(refNo, ref, "Reference Number");
    }


    public void requestedAmount_SetText(String amount) {
        setText(requestedAmount, amount, "Requested amount");
    }

    public void remark_SetText(String text) {
        setText(remark, text, "Remarks");
    }


    public void submitButton_Click() {
        clickOnElement(submitButton, "Submit Button");
    }


}