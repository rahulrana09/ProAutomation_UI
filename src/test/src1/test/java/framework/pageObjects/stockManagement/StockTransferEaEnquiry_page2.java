/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation team
 *  Date: 9/12/2017
*  Purpose: Page Object of  Stock Transfer Ea Enquiry page 2
*/


package framework.pageObjects.stockManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class StockTransferEaEnquiry_page2 extends PageInit {

    @FindBy(id = "stockEnq_view_button_submit")
    private WebElement submit;
    @FindBy(id = "stockEnq_view_transactionMap_currentStatus")
    private WebElement statusField;
    //Page 3
    @FindBy(id = "stockEnq_view_submit")
    private WebElement backButton;
    @FindBy(className = "wwFormTableC")
    private WebElement enqTable;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[1]/td[2]")
    private WebElement Requester;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[2]/td[2]")
    private WebElement RequestedAmount;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[3]/td[2]")
    private WebElement RequestedDate;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[4]/td[2]")
    private WebElement RefNo;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[5]/td[2]")
    private WebElement transactionID;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[6]/td[2]")
    private WebElement CurrentStatus;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[7]/td[2]")
    private WebElement MFSProvider;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[8]/td[2]")
    private WebElement ModifiedOn;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[10]/td[3]")
    private WebElement MainAccountBalance;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[11]/td[3]")
    private WebElement EarningAccountBalance;
    @FindBy(xpath = "//form[@id='stockEnq_view']/table/tbody/tr[13]/td[2]")
    private WebElement Remarks;


    public StockTransferEaEnquiry_page2(ExtentTest t1) {
        super(t1);
    }

    public StockTransferEaEnquiry_page2 selectTransaction(String tid) {
        WebElement el = driver.findElement(By.xpath("//input[@value='" + tid + "']"));
        clickOnElement(el, "Transaction - " + tid);
        return this;
    }

    public StockTransferEaEnquiry_page2 submitButton_Click_pg2() {
        clickOnElement(submit, "submit");
        return this;
    }

    public String getTxnIDFromUI() {
        return driver.findElement(By.xpath("//*[@id='stockEnq_view_transactionMap_transferId']")).getText();
    }


    public StockTransferEaEnquiry_page2 backButton_Click() {
        clickOnElement(backButton, "back Button");
        return this;
    }

    public String getTransactionStatus() {
        return statusField.getText();
    }

    public boolean isTxnIdShown(String txnId) {
        return isStringPresentInWebTableAsLabel(enqTable, txnId);
    }

    public String getRequester() {
        return Requester.getText();
    }

    public String getRequestedAmount() {
        return RequestedAmount.getText();
    }

    public String getRequestedDate() {
        return RequestedDate.getText();
    }

    public String getRefNo() {
        return RefNo.getText();
    }

    public String getTransactionID() {
        return transactionID.getText();
    }

    public String getCurrentStatus() {
        return CurrentStatus.getText();
    }

    public String getMFSProvider() {
        return MFSProvider.getText();
    }

    public String getModifiedOn() {
        return ModifiedOn.getText();
    }

    public String getMainAccountBalance() {
        return MainAccountBalance.getText();
    }

    public String getEarningAccountBalance() {
        return EarningAccountBalance.getText();
    }

    public String getRemarks() {
        return Remarks.getText();
    }


}



