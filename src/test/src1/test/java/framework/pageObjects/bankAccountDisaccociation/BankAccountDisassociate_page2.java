package framework.pageObjects.bankAccountDisaccociation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BankAccountDisassociate_page2 extends PageInit {

    @FindBy(id = "bankAdd_confirmDereg_submit")
    WebElement backButton;

    public BankAccountDisassociate_page2(ExtentTest test) {
        super(test);
    }

    public static BankAccountDisassociate_page2 init(ExtentTest test) {
        return new BankAccountDisassociate_page2(test);
    }

    public void deleteBank(String bankAccNum) {
        WebElement element = driver.findElement(By.xpath("//tr[td[contains(text(),'" + bankAccNum + "')]]/td/a"));
        clickOnElement(element, "Delete Bank Account Button");
    }

    public void clickBackButton() {
        clickOnElement(backButton, "Click back Button");
    }
}
