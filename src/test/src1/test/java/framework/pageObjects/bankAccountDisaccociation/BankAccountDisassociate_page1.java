package framework.pageObjects.bankAccountDisaccociation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BankAccountDisassociate_page1 extends PageInit {

    @FindBy(id = "mfsProvidersList")
    WebElement provider;
    @FindBy(id = "userType1")
    WebElement user;

    @FindBy(id = "msisdn1")
    WebElement msisdn;
    @FindBy(name = "optWalletId")
    WebElement wallet;
    @FindBy(id = "bankAdd_displayBankAccountDeRegistration_submit")
    WebElement submit;
    @FindBy(name = "selectedOperatorId")
    WebElement optName;

    public BankAccountDisassociate_page1(ExtentTest test) {
        super(test);
    }

    public static BankAccountDisassociate_page1 init(ExtentTest test) {
        return new BankAccountDisassociate_page1(test);
    }

    public BankAccountDisassociate_page1 navigateToLink() throws Exception {
        navigateTo("BNKACCLINK_ALL", "BNKACCLINK_BANK_ACC_DREG",
                "Navigate to Bank Account DeRegistration Link");
        return this;
    }
    public void setMSISDN(String text)
    {
        msisdn.sendKeys(text);
        pageInfo.info("Set Mobile Number - " + text);
    }

    public BankAccountDisassociate_page1 selectOperatorName(String operatorName) {
        selectVisibleText(optName, operatorName, "Select Operator Name");
        return this;
    }

    public BankAccountDisassociate_page1 selectProvider(String providerName) {
        selectVisibleText(provider, providerName, "Select Provider Name");
        return this;
    }

    public BankAccountDisassociate_page1 selectUser(String userType) {
        selectValue(user, userType, "Select User Type");
        return this;
    }

    public BankAccountDisassociate_page1 setWalletType(String walletType) {
        setText(wallet, walletType, "Select Wallet Type");
        return this;
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

}
