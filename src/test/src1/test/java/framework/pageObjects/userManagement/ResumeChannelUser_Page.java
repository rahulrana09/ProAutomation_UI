package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.Set;


public class ResumeChannelUser_Page extends PageInit {

    /******************************************************************
     *  R E S U M E  - C H A N N E L  - P A G E  -  O B J E C T S
     ******************************************************************/


    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr/td[2]/span/img")
    private WebElement domainDropDown;
    @FindBy(xpath = "//*[@id='resumeCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement categoryDropDown;
    @FindBy(xpath = "//input[@id='resumeCH_loadChannelUser_userName' and @type='text']")
    private WebElement firstNameTextBox;
    @FindBy(xpath = "//input[@id='resumeCH_loadChannelUser_msisdn' and @type='text']")
    private WebElement msisdnTextBox;
    @FindBy(xpath = "//*[@id='resumeCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement firstNameSearch;
    @FindBy(id = "resumeCH_loadChannelUser_button_submit")
    private WebElement submitButton;
    @FindBy(id = "viewForm_button_submit")
    private WebElement confirmButton;

    public ResumeChannelUser_Page(ExtentTest t1) {
        super(t1);
    }

    /******************************************************************
     *  R E S U M E  - C H A N N E L  - P A G E  -  O P E R A T I O N S
     ******************************************************************/


    public void navResumeChannelUser() throws Exception {
        fl.leftNavigation("CHUMGMT_ALL", "CHUMGMT_PTY_RCU");
        pageInfo.info("Navigate to Channel User Resume Page!");
    }


    public void navResumeChannelUserApproval() throws Exception {
        fl.leftNavigation("CHUMGMT_ALL", "CHUMGMT_PTY_RCUAP1");
        pageInfo.info("Navigate to Channel User Resume Approval Page!");
    }


    public void selectDomain(String domainCode) throws InterruptedException {
        domainDropDown.click();
        Thread.sleep(500);
        DriverFactory.getDriver().findElement(By.xpath("//div[@resultvalue='" + domainCode + "']")).click();
        Thread.sleep(2500); // static wait, as selecting domain is not consistent!
        pageInfo.info("Selected Domain Name: " + domainCode);
    }

    public void selectCategory(String categoryCode) throws InterruptedException {
        categoryDropDown.click();
        Thread.sleep(500);
        DriverFactory.getDriver().findElement(By.xpath("//div[@resultvalue='" + categoryCode + "']")).click();
        Thread.sleep(2500);
        pageInfo.info("Selected Category Code: " + categoryCode);
    }

    public void selectSuspendFirstName(String firstName) throws Exception {
        if (DriverFactory.getDriver().findElements(By.xpath("//*[@id=\"resumeApprCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")).size() != 0)
            selectFirstName(firstNameSearch, firstName);
        else if (DriverFactory.getDriver().findElements(By.xpath("//*[@id=\"resumeCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")).size() != 0)
            selectFirstName(DriverFactory.getDriver().findElement(By.xpath("//*[@id=\"resumeCH_loadChannelUser\"]/table/tbody/tr[1]/td[3]/a/img")), firstName);
        pageInfo.info("Selected First Name " + firstName);
    }


    public void setFirstName(String fName) {
        firstNameTextBox.sendKeys(fName);
        pageInfo.info("Set First name: " + fName);
    }

    public void setMSISDN(String msisdn) {
        msisdnTextBox.sendKeys(msisdn);
        pageInfo.info("Set MSISDN: " + msisdn);
    }

    public void suspendSubmitFirstPageBtn() {
        if (DriverFactory.getDriver().findElements(By.id("suspendCH_loadChannelUser_button_submit")).size() != 0)
            submitButton.click();
        else if (DriverFactory.getDriver().findElements(By.id("suspendApprCH_loadChannelUser_button_submit")).size() != 0)
            DriverFactory.getDriver().findElement(By.id("suspendApprCH_loadChannelUser_button_submit")).click();
        pageInfo.info("Click on submit");
    }

    public void clickSubmit() {
        submitButton.click();
        pageInfo.info("Clicked on Submit Button..");
    }

    public void clickConfirm() {
        confirmButton.click();
        pageInfo.info("Clicked on Confirm Button..");
    }

    /**
     * selectFirstName
     *
     * @throws Exception
     */
    private void selectFirstName(WebElement elem, String firstName) throws Exception {
        elem.click();
        Thread.sleep(1000);
        String MainWindow = DriverFactory.getDriver().getWindowHandle();
        Set<String> s1 = DriverFactory.getDriver().getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();
            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                DriverFactory.getDriver().switchTo().window(ChildWindow);
                WebElement wFirstName = DriverFactory.getDriver().findElement(By.xpath(".//*[@id='searchCH_setSearchedChannelUser_userName']"));
                WebElement wSearch = DriverFactory.getDriver().findElement(By.xpath("html/body/table[1]/tbody/tr[2]/td/table[2]/tbody/tr/td[3]/a/img"));

                wFirstName.sendKeys(firstName);
                wSearch.click();

                WebElement wUserlist = DriverFactory.getDriver().findElement(By.xpath(".//*[@id='searchCH_setSearchedChannelUser_userId']"));
                Select wSelect = new Select(wUserlist);
                wSelect.selectByIndex(1);

                WebElement wSubmit = DriverFactory.getDriver().findElement(By.xpath("html/body/table[1]/tbody/tr[2]/td/table[2]/tbody/tr[3]/td/input"));
                wSubmit.click();
            }
        }
        // Switching to Parent window i.e Main Window.
        DriverFactory.getDriver().switchTo().window(MainWindow);
        fl.contentFrame();
    }


}
