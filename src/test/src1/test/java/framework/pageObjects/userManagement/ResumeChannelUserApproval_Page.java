package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ResumeChannelUserApproval_Page extends PageInit {

    /******************************************************************
     *  R E S U M E  - C H A N N E L  - P A G E  -  O B J E C T S
     ******************************************************************/


    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr/td[2]/span/img")
    private WebElement domainDropDown;
    @FindBy(xpath = "//*[@id='resumeApprCH_loadChannelUser']/table/tbody/tr/td[2]/span/img")
    private WebElement categoryDropDown;
    @FindBy(xpath = "//input[@id='resumeApprCH_loadChannelUser_userName' and @type='text']")
    private WebElement firstNameTextBox;
    @FindBy(xpath = "//input[@id='resumeApprCH_loadChannelUser_msisdn' and @type='text']")
    private WebElement msisdnTextBox;
    @FindBy(xpath = "//*[@id='resumeApprCH_loadChannelUser']/table/tbody/tr[1]/td[3]/a/img")
    private WebElement firstNameSearch;
    @FindBy(id = "resumeApprCH_loadChannelUser_button_submit")
    private WebElement submitButton;
    @FindBy(id = "viewForm_button_approve")
    private WebElement approveButton;
    @FindBy(id = "viewForm_reject")
    private WebElement resumeRejectBtn;

    public ResumeChannelUserApproval_Page(ExtentTest t1) {
        super(t1);
    }

    /******************************************************************
     *  R E S U M E  - C H A N N E L  - P A G E  -  O P E R A T I O N S
     ******************************************************************/

    public void navResumeChannelUserApproval() throws Exception {
        fl.leftNavigation("CHUMGMT_ALL", "CHUMGMT_PTY_RCUAP1");
        pageInfo.info("Navigate to Channel User Resume Approval Page!");
    }


    public void selectDomain(String domainCode) throws InterruptedException {
        domainDropDown.click();
        Thread.sleep(500);
        DriverFactory.getDriver().findElement(By.xpath("//div[@resultvalue='" + domainCode + "']")).click();
        Thread.sleep(2000);
        pageInfo.info("Selected Domain Name: " + domainCode);
    }

    public void selectCategory(String categoryCode) throws InterruptedException {
        categoryDropDown.click();
        Thread.sleep(500);
        DriverFactory.getDriver().findElement(By.xpath("//div[@resultvalue='" + categoryCode + "']")).click();
        Thread.sleep(2500);
        pageInfo.info("Selected Category Code: " + categoryCode);
    }


    public void setFirstName(String fName) {
        firstNameTextBox.sendKeys(fName);
        pageInfo.info("Set First name: " + fName);
    }

    public void setMSISDN(String msisdn) {
        msisdnTextBox.sendKeys(msisdn);
        pageInfo.info("Set MSISDN: " + msisdn);
    }


    public void clickSubmit() {
        submitButton.click();
        pageInfo.info("Clicked on Submit Button..");
    }

    public void clickApprove() {
        approveButton.click();
        pageInfo.info("Clicked on Confirm Button..");
    }

    public void clickOnResumeReject() {
        resumeRejectBtn.click();
        pageInfo.info("Clicked on Reject Button..");
    }


}
