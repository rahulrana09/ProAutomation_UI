package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BillPayment_pg1 extends PageInit {

    @FindBy(id = "billingMode")
    WebElement processType;

    @FindBy(id = "serviceListId")
    WebElement serviceList;

    @FindBy(id = "billerCode")
    WebElement billerCode;

    @FindBy(id = "providerListSel")
    WebElement provider;

    @FindBy(id = "billerAccountNo")
    WebElement accNo;

    @FindBy(id = "billPayment_input_amount")
    WebElement amount;

    @FindBy(id = "billNumber")
    WebElement billNo;

    @FindBy(id = "billPayment_input_refIdOrOthrMsisdn")
    WebElement refeMSISDN;

    @FindBy(name = "action:billPayment_confirm")
    WebElement submit;

    @FindBy(id = "billPayment_confirm_button_confirm")
    WebElement confirm;

    public BillPayment_pg1(ExtentTest test) {
        super(test);
    }

    public static BillPayment_pg1 init(ExtentTest test) {
        return new BillPayment_pg1(test);
    }

    public BillPayment_pg1 navigateBillPaymentLink() throws Exception {
        navigateTo("BP_ALL", "BP_BP_RET", "Bill Payment Page");
        return this;
    }

    public BillPayment_pg1 selectProcess(String value) {
        selectValue(processType, value, "Select Bill Process Type");
        return this;
    }

    public BillPayment_pg1 selectServices(String value) {
        selectValue(serviceList, value, "Select Service List");
        return this;
    }

    public BillPayment_pg1 selectBillerCode(String value) {
        selectValue(billerCode, value, "Biller Code");
        return this;
    }

    public BillPayment_pg1 selectProvider(String providerName) {
        //provider.click();
        clickOnElement(provider, "Provider Select Box");
        selectVisibleText(provider, providerName, "select provider");
        return this;
    }

    public BillPayment_pg1 setBillerAccNo(String billerAccNo) {
        setText(accNo, billerAccNo, "Biller Account Number");
        return this;
    }

    public BillPayment_pg1 setAmount(String billAmount) {
        setText(amount, billAmount, "Amount");
        return this;
    }

    public BillPayment_pg1 setBillNumber(String billNumber) {
        setText(billNo, billNumber, "Bill Number");
        return this;
    }

    public BillPayment_pg1 setReferenceMSISDN(String value) {
        setText(refeMSISDN, value, "Reference MSISDN");
        return this;
    }

    public BillPayment_pg1 clickSubmit() throws Exception {
        clickOnElement(submit, "Submit Button");
        return this;
    }

    public void clickConfirm() throws Exception {
        clickOnElement(confirm, "Confirm Button");
    }
}
