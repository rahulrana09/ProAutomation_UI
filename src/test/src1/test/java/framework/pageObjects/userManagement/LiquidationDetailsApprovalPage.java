package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LiquidationDetailsApprovalPage extends PageInit {

    public LiquidationDetailsApprovalPage(ExtentTest t1) {
        super(t1);
    }

    public void verifyHeaders() {
        ArrayList fields = new ArrayList();
        fields.add("MFS Provider");
        fields.add("Liquidation Bank Name");
        fields.add("Liquidation Bank Account Number");
        fields.add("Liquidation Bank Branch Name");
        fields.add("Account Holder Name");
        fields.add("Frequency Trading Name");

        for (int i = 1, j = 0; i < 7; i++, j++) {

            String elementText = "//td[contains(text(),'USDLIQ')]/..//td[contains(text(),'MFS1')]/..//td[" + i + "]";
            WebElement elem = driver.findElement(By.xpath(elementText));
            boolean isElementExist = elem.isDisplayed();
            Assert.assertTrue(isElementExist);
            System.out.println(fields.get(j) + " is Present");
        }
    }

    public void verifyHeadersDetails(String Provider, String BankName, String LiquidationBankAccountNumber, String LiquidationBankBranchName, String AccountHolderName, String FrequencyTradingName) throws Exception {

        ArrayList fields = new ArrayList();
        fields.add(Provider);
        fields.add(BankName);
        fields.add(LiquidationBankAccountNumber);
        fields.add(LiquidationBankBranchName);
        fields.add(AccountHolderName);
        fields.add(FrequencyTradingName);

        Collections.sort(fields);
        Object[] expected = fields.toArray();

        List<String> values = new ArrayList<>();
        List<WebElement> links = driver.findElements(By.xpath("//td[contains(text(),'" + BankName + "')]/..//td[contains(text(),'" + Provider + "')]/..//td"));
        for (WebElement link : links) {
            values.add(link.getText());
        }
        Collections.sort(values);
        Object[] actual = values.toArray();

        boolean status = false;
        if (Arrays.equals(actual, expected)) {
            status = true;
        }

        Assertion.assertEqual(status, true, "Fields Are Validated In Approval Page", pageInfo, true);

    }

    public void verifyHeadersDetailsinModifyPage(String Provider, String Bankid, String LiquidationBankAccountNumber, String LiquidationBankBranchName, String AccountHolderName, String FrequencyTradingName) throws Exception {


        ArrayList fields = new ArrayList();
        fields.add(Provider);
        fields.add(Bankid);
        fields.add(LiquidationBankAccountNumber);
        fields.add(LiquidationBankBranchName);
        fields.add(AccountHolderName);
        fields.add(FrequencyTradingName);


        for (int i = 1, j = 0; i < 3; i++, j++) {
            String elementText = "//tbody[@id='liquidationDetails']/tr[2]/td[" + i + "]/select/option[text()='" + fields.get(j) + "']";
            String elem = driver.findElement(By.xpath(elementText)).getText();
            Assert.assertEquals(elem, fields.get(j));

        }
        for (int i = 3, j = 2; i < 7; i++, j++) {
            String elementText = "//tbody[@id='liquidationDetails']/tr[2]/td[" + i + "]/input[@value='" + fields.get(j) + "']";
            String elem = driver.findElement(By.xpath(elementText)).getAttribute("value");
            Assert.assertEquals(elem, fields.get(j));

        }
    }

    public void verifyHeadersDetailsinModifyPageareDisabled() throws Exception {

        ArrayList fields = new ArrayList();
        fields.add("Provider");
        fields.add("BankName");
        fields.add("LiquidationBankAccountNumber");
        fields.add("LiquidationBankBranchName");
        fields.add("AccountHolderName");
        fields.add("FrequencyTradingName");


        for (int i = 1, j = 0; i < 3; i++, j++) {
            WebElement elementText = driver.findElement(By.xpath("//tbody[@id='liquidationDetails']/tr[2]/td[" + i + "]/select"));
            boolean result = elementText.isEnabled();
            Assert.assertEquals(result, false);
        }
        for (int i = 3, j = 2; i < 7; i++, j++) {
            WebElement elementText = driver.findElement(By.xpath("//tbody[@id='liquidationDetails']/tr[2]/td[" + i + "]/input"));
            boolean result = elementText.isEnabled();
            Assert.assertEquals(result, false);

        }


    }

    public void verifyHeadersInBillerApprovalPage() {
        ArrayList fields = new ArrayList();
        fields.add("MFS Provider");
        fields.add("Liquidation Bank Name");
        fields.add("Liquidation Bank Account Number");
        fields.add("Liquidation Bank Branch Name");
        fields.add("Account Holder Name");
        fields.add("Frequency Trading Name");

        for (int i = 1, j = 0; i < 7; i++, j++) {

            String elementText = ".//*[@id='liquidationDetails']/tr[3]/td[" + i + "]";
            WebElement elem = driver.findElement(By.xpath(elementText));
            boolean isElementExist = elem.isDisplayed();
            Assert.assertTrue(isElementExist);
            System.out.println(fields.get(j) + " is Present");
        }
    }

    public void verifyHeadersDetailsInBillerApprovalPage(String Provider, String Bankid, String LiquidationBankAccountNumber, String LiquidationBankBranchName, String AccountHolderName, String FrequencyTradingName) throws Exception {

        ArrayList fields = new ArrayList();
        fields.add(Provider);
        fields.add(Bankid);
        fields.add(LiquidationBankAccountNumber);
        fields.add(LiquidationBankBranchName);
        fields.add(AccountHolderName);
        fields.add(FrequencyTradingName);


        for (int i = 1, j = 0; i < 3; i++, j++) {

            String elementText = ".//*[@id='liquidationDetails']/tr[3]/td[" + i + "]/select";
            Select elem = new Select(driver.findElement(By.xpath(elementText)));
            String eleme = elem.getFirstSelectedOption().getText();
            Assert.assertEquals(eleme, fields.get(j));

        }

        for (int i = 3, j = 2; i < 7; i++, j++) {

            String elementText = ".//*[@id='liquidationDetails']/tr[3]/td[" + i + "]/input";
            String eleme = driver.findElement(By.xpath(elementText)).getAttribute("value");
            Assert.assertEquals(eleme, fields.get(j));

        }
    }

    public void verifyHeadersInEnterpriseApprovalPage() {
        ArrayList fields = new ArrayList();
        fields.add("MFS Provider");
        fields.add("Liquidation Bank Name");
        fields.add("Liquidation Bank Account Number");
        fields.add("Liquidation Bank Branch Name");
        fields.add("Account Holder Name");
        fields.add("Frequency Trading Name");

        for (int i = 1, j = 0; i < 7; i++, j++) {

            String elementText = "//form[@id ='viewForm']/table/tbody/tr[61]/td[" + i + "]";
            String elem = driver.findElement(By.xpath(elementText)).getText();
            Assert.assertEquals(elem, fields.get(j));
            System.out.println(fields.get(j) + " is Present");
        }
    }

    public void verifyHeadersDetailsInEnterpriseApprovalPage(String Provider, String Bankid, String LiquidationBankAccountNumber, String LiquidationBankBranchName, String AccountHolderName, String FrequencyTradingName) throws Exception {

        ArrayList fields = new ArrayList();
        fields.add(Provider);
        fields.add(Bankid);
        fields.add(LiquidationBankAccountNumber);
        fields.add(LiquidationBankBranchName);
        fields.add(AccountHolderName);
        fields.add(FrequencyTradingName);


        for (int i = 1, j = 0; i < 7; i++, j++) {
            String elementText = "//form[@id ='viewForm']/table/tbody/tr[62]/td[" + i + "]";
            String elem = driver.findElement(By.xpath(elementText)).getText();
            Assert.assertEquals(elem, fields.get(j));
            System.out.println(fields.get(j) + " is Present");
        }
    }


}

