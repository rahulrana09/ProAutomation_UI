package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class AddChannelUser_pg4 extends PageInit {


    /*
     * Page Objects
	 */
    @FindBy(id = "add1_addChannelUser_submit")
    WebElement btnAddMore;

    @FindBy(name = "action:confirm2_assignBankOrEnteprise")
    WebElement btnNext;

    @FindBy(name = "action:subsRegistrationServiceBean_assignBankToUser")
    WebElement btnNextSubs;

    @FindBy(name = "action:subsRegistrationServiceBean_assignBankToUser")
    WebElement btnNextModify;

    @FindBy(id = "subsRegistrationServiceBean_output_submit")
    WebElement btnAddMoreSubs;

    @FindBy(name = "action:confirm2_addMoreWalletRows")
    WebElement addNextBtn;

    public AddChannelUser_pg4(ExtentTest t1) {
        super(t1);
    }

    public static AddChannelUser_pg4 init(ExtentTest t1) {
        return new AddChannelUser_pg4(t1);
    }

    public boolean isBtnNextAvailable() {
        return fl.elementIsDisplayed(btnNext);
    }

    public void clickAddMore(String categoryCode) throws Exception {
        Thread.sleep(1000);
        try {
            if (categoryCode.equals(Constants.SUBSCRIBER))
                clickOnElement(btnAddMoreSubs, "btnAddMoreSubs");
            else
                clickOnElement(btnAddMore, "btnAddMore");
        } catch (Exception e) {
            Thread.sleep(7000);
            if (categoryCode.equals(Constants.SUBSCRIBER))
                clickOnElement(btnAddMoreSubs, "btnAddMoreSubs");
            else
                clickOnElement(btnAddMore, "btnAddMore");
        }
    }

    //------------------------
    public void btnAddMore() throws Exception {
        clickOnElement(addNextBtn,"Add More Button");
    }



    public void clickNext(String categoryCode) throws InterruptedException, IOException {
        Thread.sleep(1500);
        Utils.captureScreen(pageInfo);
        if (driver.findElements(By.name("action:editCH_assignBankOrEntepriseEdit")).size() != 0) {
            driver.findElement(By.name("action:editCH_assignBankOrEntepriseEdit")).click();
        } else {
            if (categoryCode.equals(Constants.SUBSCRIBER))
                clickOnElement(btnNextSubs, "Next Button");
            else
                clickOnElement(btnNext, "Next Button");

        }

    }


    public void selectProvider(int counter,String providerName) {
        selectVisibleText(driver.findElement(By.name("counterList[" + counter + "].providerSelected")),providerName,"Provider");
    }

    public void selectPaymentType(int counter,String payType) {
        selectVisibleText(driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected")),payType,"TCP ");
    }

    public void selectGrade(int counter,String gradeName) {
        selectVisibleText(driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected")),gradeName,"TCP ");
    }

    public void selectInstrumentTcp(int counter,String tcpName) {
        selectVisibleText(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")),tcpName,"TCP ");
    }

    public void selectInstrumentTcpByIndex(int counter,int index) {
        selectIndex(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")),index,"TCP Select");
    }


    public void selectPrimary(int counter, boolean isPrimary) {
        Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected")));
        if (isPrimary) {
            selPrimaryAccount.selectByIndex(0);
        } else {
            selPrimaryAccount.selectByIndex(1);
        }
    }
}
