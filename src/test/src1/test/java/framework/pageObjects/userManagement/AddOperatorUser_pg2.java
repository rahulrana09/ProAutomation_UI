package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddOperatorUser_pg2 extends PageInit {


    @FindBy(id = "confirm_confirmSystemParty_action_partyNamePrefixId")
    WebElement prefix;

    @FindBy(id = "confirm_confirmSystemParty_action_userName")
    WebElement firstName;

    @FindBy(id = "confirm_confirmSystemParty_action_lastName")
    WebElement lastName;

    @FindBy(id = "confirm_confirmSystemParty_action_msisdnUser")
    WebElement msisdn;

    @FindBy(id = "confirm_confirmSystemParty_action_departmentId")
    WebElement department;

    @FindBy(id = "confirm_confirmSystemParty_action_msisdnWeb")
    WebElement webLoginID;

    @FindBy(id = "confirm_confirmSystemParty_action_prefLanguage")
    WebElement preferredLanguage;

    @FindBy(id = "password")
    WebElement password;

    @FindBy(id = "confirm_confirmSystemParty_action_shortName")
    WebElement shortName;

    @FindBy(id = "confirm_confirmSystemParty_action_address1")
    WebElement address1;

    @FindBy(id = "confirm_confirmSystemParty_action_address2")
    WebElement address2;

    @FindBy(id = "confirm_confirmSystemParty_action_ssn")
    WebElement SSN;

    @FindBy(id = "confirm_confirmSystemParty_action_designation")
    WebElement designation;

    @FindBy(id = "confirm_confirmSystemParty_action_city")
    WebElement city;

    @FindBy(id = "confirm_confirmSystemParty_action_state")
    WebElement state;

    @FindBy(id = "confirm_confirmSystemParty_action_contactPerson")
    WebElement contactPerson;

    @FindBy(id = "confirm_confirmSystemParty_action_email")
    WebElement email;

    @FindBy(id = "confirm_confirmSystemParty_action_externalCode")
    WebElement identificationNumber;

    @FindBy(id = "confirm_confirmSystemParty_action_divisionId")
    WebElement division;

    @FindBy(id = "confirm_confirmSystemParty_action_contactNo")
    WebElement contactNumber;


    @FindBy(id = "confirm_confirmSystemParty_action_checkAll")
    WebElement allowedDaysCheckAll;

    @FindBy(id = "confirmPassword")
    WebElement confirmPassword;

    @FindBy(xpath = "//a[@href='javascript:assignRole()']")
    WebElement roleAssign;

    @FindBy(id = "confirm_confirmSystemParty_action_geo")
    WebElement geoAssign;

    @FindBy(id = "confirm_confirmSystemParty_action_button_add")
    WebElement add;

    @FindBy(id = "_addButt")
    WebElement saveButton;

    @FindBy(id = "confirm_confirmSystemParty_action_back")
    WebElement back;

    @FindBy(xpath = "//input[@type='reset']")
    WebElement reset;

    @FindBys({@FindBy(xpath = "//table[@class='wwFormTableC']//td[2]")})
    List<WebElement> zoneList;

    @FindBy(id = "_systemparty_label_division")
    WebElement readOnlyDivision;

    @FindBy(id = "_systemparty_label_userName")
    WebElement readOnlyFirstName;

    @FindBy(id = "_systemparty_label_contactno")
    WebElement readOnlyContactNum;

    @FindBy(id = "_systemparty_label_msisdnWeb")
    WebElement readOnlyLoginId;

    @FindBy(id = "_back")
    WebElement backBtn;

    @FindBy(id = "asgnRoleOpt_refreshsys_button_submit")
    WebElement submitWebRole;
    @FindBy(id = "confirm_confirmSystemParty_action_allowedIp")
    WebElement allowedIP;
    //------- FOR BANK ADMIN ONLY ---------
    @FindBy(id = "confirm_confirmSystemParty_action_bankName")
    WebElement bankName;
    //========================used in p1=============//
    @FindBy(id = "confirm_confirmSystemParty_action_allowedFormTime")
    WebElement fromTime;
    @FindBy(id = "confirm_confirmSystemParty_action_allowedToTime")
    WebElement toTime;

    public AddOperatorUser_pg2(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static AddOperatorUser_pg2 init(ExtentTest t1) {
        return new AddOperatorUser_pg2(t1);
    }

    //-------------------------------------------------------------------------

    //---FOR BANK ADMIN ONLY-----

    public String getDivisionReadOnlyText() {
        return wait.until(ExpectedConditions.elementToBeClickable(readOnlyDivision)).getText();
    }

    public String getFirstNameReadOnlyText() {
        return wait.until(ExpectedConditions.elementToBeClickable(readOnlyFirstName)).getText();
    }

    public String getContactNumReadOnlyText() {
        return wait.until(ExpectedConditions.elementToBeClickable(readOnlyContactNum)).getText();
    }

    public String getLoginIdReadOnlyText() {
        return wait.until(ExpectedConditions.elementToBeClickable(readOnlyLoginId)).getText();
    }

    public void bankName_SelectIndex(int index) {
        selectIndex(bankName, index, "bankName");
    }

    public void selectBankName(String bank) {
        selectVisibleText(bankName, bank, "bankName");
    }

    public void bankName_SelectText(String text) {
        selectVisibleText(bankName, text, "bank name");
    }

    /**
     * Return the list of all the bank
     * Avalaible in the system
     *
     * @return
     */
    public List<String> getAllBank() {
        Select bankname = new Select(bankName);
        int noofbank = bankname.getOptions().size();

        List<WebElement> we = bankname.getOptions();

        List<String> ls = new ArrayList<String>();
        for (WebElement a : we) {
            if (!a.getText().equals("Select")) {
                ls.add(a.getText());
                System.out.println(" Got Bank " + a.getText());
            }

        }

        return ls;
    }

    //--------------------------
    public void setIP(String IP) {
        setText(allowedIP, IP, "Allowed IP Text box");
    }

    public void prefix_SelectIndex(int index) {
        selectIndex(prefix, index, "User Prefix");
    }

    public void firstName_SetText(String text) {
        setText(firstName, text, "First Name");
    }

    public String getFirstName() {
        String text = firstName.getText();
        pageInfo.info("Enter First name :" + text);
        return text;
    }

    public void lastName_SetText(String text) {
        setText(lastName, text, "Last Name");
    }

    public void msisdn_SetText(String text) {
        setText(msisdn, text, "MSISDN");
    }

    public void email_SetText(String text) {
        setText(email, text, "Email");
    }

    public void identificationNumber_SetText(String text) {
        setText(identificationNumber, text, "Identification Number");
    }

    public void division_SetText(String text) {
        setText(division, text, "Division");
    }

    public void contactNumber_SetText(String text) {
        contactNumber.clear();
        contactNumber.sendKeys(text);
    }

    public void department_SetText(String text) {
        setText(department, text, "Department");
    }

    public void webLoginID_SetText(String text) {
        setText(webLoginID, text, "Login Id");
    }

    public void preferredLanguage_SelectIndex(int index) {
        selectIndex(preferredLanguage, index, "Preference Language");
    }

    public void password_SetText(String text) {
        setText(password, text, "Password");
    }

    public void confirmPassword_SetText(String text) {
        setText(confirmPassword, text, "Confirm Password");
    }

    public void allowedDaysCheckAll_Click() {
        clickOnElement(allowedDaysCheckAll, "Allowed All Days");
    }

    public void add_Click() {
        clickOnElement(add, "Add");
    }

    //--------------------------------
    public void selectRole(String text) throws Exception {
        clickOnElement(roleAssign, "Role Assign");
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();
            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                // Switching to Child window
                driver.switchTo().window(ChildWindow);
                WebElement radio = driver.findElement(By.cssSelector("input[name=check][value=" + text + "]"));
                clickOnElement(radio, "Select Role " + text);
                clickOnElement(submitWebRole, "Submit Button");
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
        fl.contentFrame();
    }

    public void selectGeography(OperatorUser user) throws Exception {
        if (user.Geography == null) {


            clickOnElement(geoAssign, "geoAssign");
            String MainWindow = driver.getWindowHandle();
            Set<String> s1 = driver.getWindowHandles();
            Iterator<String> i1 = s1.iterator();
            while (i1.hasNext()) {
                String ChildWindow = i1.next();

                if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                    // Switching to Child window
                    driver.switchTo().window(ChildWindow);
                    // WebElement radio =

                    WebElement submitBtn = driver.findElement(By.id("asgnRoleOpt_refreshsysgeo_button_submit"));
                    submitBtn.click();
                }
            }
            // Switching to Parent window i.e Main Window.
            driver.switchTo().window(MainWindow);
            fl.contentFrame();
        } else {
            clickOnElement(geoAssign, "geoAssign");
            String MainWindow = driver.getWindowHandle();
            Set<String> s1 = driver.getWindowHandles();
            Iterator<String> i1 = s1.iterator();
            while (i1.hasNext()) {
                String ChildWindow = i1.next();

                if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                    // Switching to Child window
                    driver.switchTo().window(ChildWindow);
                    driver.findElement(By.xpath("//tr[td[contains(text(),'" + user.Geography + "')]]/td/input")).click();

                    WebElement submitBtn = driver.findElement(By.id("asgnRoleOpt_refreshsysgeo_button_submit"));
                    submitBtn.click();
                }
            }
            // Switching to Parent window i.e Main Window.
            driver.switchTo().window(MainWindow);
            fl.contentFrame();
        }

    }

    public void saveButton_Click() {
        clickOnElement(saveButton, "Save Button");
    }

    public void CheckGeography(String zonename) throws Exception {
        geoAssign.click();
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                // Switching to Child window
                driver.switchTo().window(ChildWindow);
                // WebElement radio =
                for (WebElement zone : zoneList) {
                    String t = zone.getText();
                    if (t.equalsIgnoreCase(zonename)) {
                        if (zone.isDisplayed()) {
                            pageInfo.info("added zone is displayed " + t);
                        } else {
                            pageInfo.info("added zone is not displayed");
                        }
                    }
                }

                WebElement submitBtn = driver.findElement(By.id("asgnRoleOpt_refreshsysgeo_button_submit"));
                submitBtn.click();

                driver.switchTo().window(MainWindow);
                fl.contentFrame();
            }
        }

    }

    public void selectParticularGeography(String Zone) throws Exception {
        geoAssign.click();
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                // Switching to Child window
                driver.switchTo().window(ChildWindow);
                driver.findElement(By.xpath("//tr[td[contains(text(),'" + Zone + "')]]/td/input")).click();

                WebElement submitBtn = driver.findElement(By.id("asgnRoleOpt_refreshsysgeo_button_submit"));
                submitBtn.click();
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
        fl.contentFrame();
    }

    public void setShortName_SetText(String text) {
        setText(shortName, text, "Short name");
    }

    public void setAddress1_SetText(String text) {
        setText(address1, text, "Address 1");
    }

    public void setAddress2_SetText(String text) {
        setText(address2, text, "Address 2");
    }

    public void setSSN_SetText(String text) {
        setText(SSN, text, "SSN");
    }

    public void setDesignation_SetText(String text) {
        setText(designation, text, "Designation");
    }

    public void setCity_SetText(String text) {
        setText(city, text, "City");
    }

    public void setState_SetText(String text) {
        setText(state, text, "State");
    }

    public void setContactPerson_SetText(String text) {
        setText(contactPerson, text, "Contact Person");
    }

    public void clickOnBackButton() {
        clickOnElement(back, "Back Button");

    }

    public void clickOnBackBtn() {
        clickOnElement(backBtn, "Back Button");

    }

    public void clickOnResetButton() {
        clickOnElement(reset, "Reset Button");
    }

    public void setFromTime(String HHMM) {
        setText(fromTime, HHMM, "FromTime");

    }

    public void setToTime(String HHMM) {
        setText(toTime, HHMM, "toTime");
    }

    public void checkFirstnameIsAvailable() {
        Utils.checkElementPresent("confirm_confirmSystemParty_action_userName", Constants.FIND_ELEMENT_BY_ID);
    }

}
