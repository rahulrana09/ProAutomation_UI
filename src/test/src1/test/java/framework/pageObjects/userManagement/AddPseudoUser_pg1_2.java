package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by amarnath.vb on 5/24/2017.
 */
public class AddPseudoUser_pg1_2 extends PageInit {


    /************************************************************
     * Pseudo User Add PAGE 1
     *************************************************************/

    @FindBy(id = "addPseudo_save_namePrefix")
    private WebElement pseudoNamePrefix;

    @FindBy(id = "addPseudo_save_userName")
    private WebElement pseudoFirstName;

    @FindBy(id = "addPseudo_save_lastName")
    private WebElement pseudoLastName;

    @FindBy(id = "addPseudo_save_msisdn")
    private WebElement pseudoMSISDN;

    @FindBy(id = "addPseudo_save_shortName")
    private WebElement pseudoShortName;

    @FindBy(id = "addPseudo_save_address1")
    private WebElement pseudoAddress1;

    @FindBy(id = "addPseudo_save_address2")
    private WebElement pseudoAddress2;

    @FindBy(id = "addPseudo_save_agentCode")
    private WebElement pseudoAgentCode;

    @FindBy(id = "addPseudo_save_designation")
    private WebElement pseudoDesignation;

    @FindBy(id = "addPseudo_save_city")
    private WebElement pseudoCity;

    @FindBy(id = "addPseudo_save_state")
    private WebElement pseudoState;

    @FindBy(id = "addPseudo_save_country")
    private WebElement pseudoCountry;

    @FindBy(id = "addPseudo_save_identificationNo")
    private WebElement pseudoUserIdNo;

    @FindBy(id = "addPseudo_save_email")
    private WebElement pseudoEmail;

    @FindBy(id = "addPseudo_save_division")
    private WebElement pseudoDivision;

    @FindBy(id = "addPseudo_save_department")
    private WebElement pseudoDepartment;

    @FindBy(id = "addPseudo_save_contactNo")
    private WebElement pseudocontactNo;

    @FindBy(id = "addPseudo_save_contactPerson")
    private WebElement pseudoContactPerson;

    @FindBy(name = "dojo.appointmentDate")
    private WebElement pseudoAppointmentDate;

    @FindBy(id = "addPseudo_save_webLoginId")
    private WebElement pseudoLoginId;

    @FindBy(id = "addPseudo_save_webPassword")
    private WebElement pseudoPassword;

    @FindBy(id = "addPseudo_save_confWebPassword")
    private WebElement pseudoConfPassword;

    @FindBy(name = "button.next")
    private WebElement pseudoNext;

    @FindBy(name = "button.reset")
    private WebElement pseudoReset;

    @FindBy(id = "addPseudo_save_checkAll")
    private WebElement pseudoCheckAll;
    /************************************************************
     * Pseudo User Add PAGE 2
     *************************************************************/

    @FindBy(id = "categoryCodeList")
    private WebElement pseudoCategoryList;
    @FindBy(id = "parentId")
    private WebElement pseudoParentCategoryId;
    @FindBy(id = "parentUserNameId")
    private WebElement pseudoParentUserName;
    @FindBy(id = "ownerId")
    private WebElement ownerCategoryId;
    @FindBy(id = "roleId")
    private WebElement pseudoGroupRoleId;
    @FindBy(id = "addPseudo_addConfirm_button_save")
    private WebElement pseudoAddConfirm;
    @FindBy(id = "addPseudo_addConfirm_button_back")
    private WebElement pseudoConfirmBack;
    @FindBy(id = "addPseudo_saveConfirm_button_confirm")
    private WebElement pseudoSaveConfirm;
    /**********************************
     * Page-3 where success message comes
     */

    @FindBy(className = "actionMessage")
    private WebElement actualMessage;
    @FindBy(id = "categoryCodeList")
    private WebElement category;
    @FindBy(id = "parentId")
    private WebElement parentCategory;
    @FindBy(id = "ownerId")
    private WebElement ownerCategorydropdown;
    @FindBy(id = "msisdn")
    private WebElement msisdnTxtBox;
    @FindBy(id = "modPseudo_fetchPseudoUser_button_submit")
    private WebElement submitbtn;
    @FindBy(id = "modPseudo_modify_button_next")
    private WebElement next1Btn;
    @FindBy(id = "modPseudo_modifyHierarchyConfirm_button_next")
    private WebElement next2Btn;
    @FindBy(id = "modPseudo_modifyConfirm_button_confirm")
    private WebElement confirm;
    @FindBy(xpath = "//span[@class='actionMessage']")
    private WebElement modInitiationTxtMsg;
    @FindBy(id = "modPseudo_modifyApproveConfirm_button_confirm")
    private WebElement confirmBtn;

    public AddPseudoUser_pg1_2(ExtentTest t1) {
        super(t1);
    }

    public static AddPseudoUser_pg1_2 init(ExtentTest t1) {
        return new AddPseudoUser_pg1_2(t1);

    }

    /*************************************************************************
     *Setting values for Pseudo User Creation Page
     **************************************************************************/


    public void setPseudoNamePrefix() {
        Select sel = new Select(pseudoNamePrefix);
        sel.selectByIndex(1);
        pageInfo.info("Select Name Prefix at index 1!");
    }

    public void setPseudoFirstName(String firstName) {
        pseudoFirstName.sendKeys(firstName);
        pageInfo.info("Set First Name: " + firstName);
    }

    public void setPseudoLastName(String firstName) {
        pseudoLastName.sendKeys(firstName);
        pageInfo.info("Set Last Name: " + firstName);
    }

    public void setPseudoMSISDN(String msisdn) {
        pseudoMSISDN.sendKeys(msisdn);
        pageInfo.info("Set MSISDN: " + msisdn);
    }

    public void setPseudoAgentCode(String msisdn) {
        pseudoAgentCode.sendKeys(msisdn);
        pageInfo.info("Set Agent Code:" + msisdn);
    }

    public void setPseudoUserIdNo(String msisdn) {
        pseudoUserIdNo.sendKeys(msisdn + "1234");
    }

    public void setPseudoDivision(String division) {
        pseudoDivision.sendKeys(division);
        pageInfo.info("Set Pseudo Division:" + division);
    }

    public void setPseudoDepartment(String Dept) {
        pseudoDepartment.sendKeys(Dept);
        pageInfo.info("Set Pseudo Division:" + Dept);
    }

    public void setPseudocontactNo(String pseudoMSISDN) {
        pseudocontactNo.sendKeys(pseudoMSISDN);
        pageInfo.info("Set Pseudo Division:" + pseudoMSISDN);
    }

    public void setPseudoContactPerson(String ContPer) {
        pseudoContactPerson.sendKeys(ContPer);
        pageInfo.info("Set Pseudo Division:" + ContPer);
    }

    public void setPseudoAppointmentDate(String appDate) {
        pseudoAppointmentDate.sendKeys(appDate); //to be updated to take current date dynamically
        pageInfo.info("Set Appointment date:" + appDate);
    }

    public void setPseudoLoginId(String pseudoFirstName) {
        pseudoLoginId.sendKeys(pseudoFirstName); //to be updated to take current date dynamically
        pageInfo.info("Set Pseudo Login ID:" + pseudoFirstName);
    }

    public void setPseudoPassword(String pwd) {
        pseudoPassword.sendKeys(pwd); //to be updated with framework password from configuration file
        pageInfo.info("Set password:" + pwd);
    }

    public void setPseudoConfPassword(String pwd) {
        pseudoConfPassword.sendKeys(pwd);
        pageInfo.info("Set confirmation password:" + pwd);
    }


    /*
    * ---------------------modify Pseudo User------------------
    * */

    public void setPseudoCheckAll() {
        pseudoCheckAll.click();

    }

    public void clickNext() {
        pseudoNext.click();
    }

    public void setCategoryCodeList(String pseudoCategoryCode) {
        /*Select sel = new Select(pseudoCategoryList);
        fl.waitSelectOptions(sel);
        sel.selectByVisibleText(pseudoCategoryCode);
        pageInfo.info("Select Category Code:" + pseudoCategoryCode);*/
        selectVisibleText(pseudoCategoryList,pseudoCategoryCode,"Category Code");
    }

    public void setPseudoParentId(String pseudoParentCategory) {
       /* Select sel = new Select(pseudoParentCategoryId);
        fl.waitSelectOptions(sel);
        sel.selectByVisibleText(pseudoParentCategory);
        pageInfo.info("Select Category Code:" + pseudoParentCategory);*/
        selectVisibleText(pseudoParentCategoryId,pseudoParentCategory,"Pseudo Parent Category");

    }

    public void setPseudoParentName(String parentUserName) {
        Select sel = new Select(pseudoParentUserName);
        fl.waitSelectOptions(sel);
        //sel.selectByVisibleText(parentUserName);
        sel.selectByIndex(1);
        pageInfo.info("Select parent user name:" + sel.getFirstSelectedOption());

    }

    public void setPseudoOwnerId(String ownerCategoryCode) {
        Select sel = new Select(ownerCategoryId);
        fl.waitSelectOptions(sel);
        //sel.selectByVisibleText(ownerCategoryCode);
        sel.selectByIndex(1);
        pageInfo.info("Select Category Code:" + sel.getFirstSelectedOption());

    }

    public void setPseudoGroupRole(String pseudoGroupRoleCode) {
        Select sel = new Select(pseudoGroupRoleId);
        fl.waitSelectOptions(sel);
//        sel.selectByVisibleText(pseudoGroupRoleCode);
        sel.selectByIndex(1);
        pageInfo.info("Select Category Code:" + sel.getFirstSelectedOption());

    }

    public void setPseudoSave() {
        clickOnElement(pseudoAddConfirm, "pseudoAddConfirm");
    }

    public void setpseudoConfirmBack() {
        clickOnElement(pseudoAddConfirm, "pseudoAddConfirm");
    }

    public void setpseudoSaveConfirm() {
        clickOnElement(pseudoSaveConfirm, "pseudoSaveConfirm");
    }

    public void navPseudoCategoryPage() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_CAT_MGMT", "Pseudo User Category Management");
    }

    public String getActualMessage() {
        return String.valueOf(actualMessage);
    }

    public void navPseudoUserCreation() throws Exception {
        fl.leftNavigation("PSEUDO_ALL", "PSEUDO_ALL");
        pageInfo.info("Navigate to Pseudo User Creation Page!");
    }

    public void navPseudoUserModification() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_MOD", "Pseudo User Management");
    }

    public void navPseudoUserDeletion() throws Exception {
        navigateTo("PSEUDO_ALL", "PSEUDO_PSEUDO_DEL", "Pseudo User Management");
    }

    public void setPseudocat(String pseudoCategoryCode) {
        selectVisibleText(category, pseudoCategoryCode, "Category");
    }

    public void setPseudoParentCategory() {
        selectIndex(parentCategory, 1, "parentCategory");
    }

    public void setPseudoOwnerCategory() {
        selectIndex(ownerCategorydropdown, 1, "ownerCategorydropdown");
    }

    public void setMsisdnTxtField(String msisdn) {
        setText(msisdnTxtBox, msisdn, "msisdnTxtBox");
    }

    public void clickonSubit() {
        clickOnElement(submitbtn, "submitbtn");
    }

    public void clickonNxt1() {
        clickOnElement(next1Btn, "next1Btn");
    }

    public void clickonNxt2() {
        clickOnElement(next2Btn, "next2Btn");
    }

    /*
    * ---------------------------------------- Modify Pseudo User Approve -------------------------------------------
    * */

    public void clickConfirmInitiate() throws Exception {
        clickOnElement(confirm, "confirm");
        Thread.sleep(Constants.WAIT_TIME);
    }

    public void clickConfirmComplete() {
        clickOnElement(confirmBtn, "confirmBtn");
    }

    public String getmodInitiationTxtMsg() {
        String txt = modInitiationTxtMsg.getText();
        pageInfo.info(txt);
        return txt;
    }

    public void navModifyApprove() throws Exception {
        navigateTo("PSEUDO2_ALL", "PSEUDO2_PSEUDO_MOD2", "Pseudo User Managemnet");
    }

    public void clickEntryForApproval(String msisdn) {
        WebElement appLink = driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr[1]/td/a[contains(text(), 'Approve')]"));
        clickOnElement(appLink, "Approval: " + msisdn);
    }

    public void clickEntryForRejection(String msisdn) {
        WebElement appLink = driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr[1]/td/a[contains(text(), 'Reject')]"));
        clickOnElement(appLink, "Rejection: " + msisdn);
    }
}