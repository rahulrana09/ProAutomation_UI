package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class ChangePasswordPage extends PageInit {


    @FindBy(id = "oldPassword")
    private WebElement OldPassword;

    @FindBy(id = "newPassword")
    private WebElement NewPassword;

    @FindBy(id = "confirmNewPassword")
    private WebElement ConfirmNewPassword;

    @FindBy(id = "skip_for_later")
    private WebElement SkipForLater;

    @FindBy(id = "changePwd_change_button_submit")
    private WebElement Submit;

    @FindBy(xpath = "//*[@id='header']/table/tbody/tr/td/table/tbody/tr[3]/td[2]/ul/li/span")
    private WebElement ActionMessage;


    public ChangePasswordPage(ExtentTest t1) {
        super(t1);
    }

    public static ChangePasswordPage init(ExtentTest t1) {
        return new ChangePasswordPage(t1);
    }

    public boolean isChangePasswordPageVisible() {
        return fl.elementIsDisplayed(OldPassword);
    }

    /**
     * Set Old Password
     *
     * @param pwd
     */
    public ChangePasswordPage setOldPassword(String pwd) {
        setText(OldPassword, pwd, "OldPassword");
        return this;
    }

    /**
     * Set New Password
     *
     * @param pwd
     */
    public ChangePasswordPage setNewPassword(String pwd) {
        setText(NewPassword, pwd, "NewPassword");
        return this;
    }

    /**
     * Set Confirm password
     *
     * @param pwd
     */
    public ChangePasswordPage setConfirmPassword(String pwd) {
        setText(ConfirmNewPassword, pwd, "ConfirmNewPassword");
        return this;
    }

    /**
     * Submit
     */
    public ChangePasswordPage clickSubmit() throws InterruptedException {
        Thread.sleep(8000);
        clickOnElement(Submit, "Submit");
        Utils.putThreadSleep(ConfigInput.loginSync);
        return this;
    }

    /**
     * Verify if password change was successful
     */
    public void verifyChangePassword() {
        Assert.assertTrue(ActionMessage.getText().equals(MessageReader.getMessage("changePassword.label.success", null)), "Verify that Password has successfully changed");
    }

    public void clickSkip() {
        clickOnElement(SkipForLater, "");
    }

    public void clickChangePwdLink() {
        WebElement chPwdLink = DriverFactory.getDriver().findElement(By.xpath("//a[contains(@href,'changePwd_input.action') and @class='toplink']"));
        clickOnElement(chPwdLink, "chPwdLink");
    }
}
