package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by amarnath.vb on 5/24/2017.
 */
public class pseudoGroupRole_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /************************************************************
     * Pseudo User Group Role Add PAGE 1
     *************************************************************/

    @FindBy(id = "categoryCodeList")
    private WebElement pseudoCategoryCodeList;
    @FindBy(id = "addPseudoGroupRoles_forward_button_submit")
    private WebElement pseudoGroupRoleSubmit;
    @FindBy(id = "PSEUDO_PSEUDO_GRP")
    private WebElement pseudoCategory;
    /*******************************
     * Pseudo Group Role Add PAGE:2
     *******************************/
    @FindBy(id = "addPseudoGroupRoles_forward_button_add")
    private WebElement AddGroupRole;
    @FindBy(id = "addPseudoGroupRoles_forward_back")
    private WebElement AddGroupRoleBack;
    @FindBy(id = "addPseudoGroupRoles_forward_button_update")
    private WebElement GroupRoleUpdate;
    @FindBy(id = "addPseudoGroupRoles_forward_button_view")
    private WebElement GroupRoleView;
    @FindBy(xpath = ".//*[@id='addPseudoGroupRoles_forward']/table/tbody/tr[4]/td/center/input[5]")
    private WebElement GroupRoleDelete;
    @FindBy(id = "addPseudoGroupRoles_forward_button_duplicate")
    private WebElement GroupRoleDuplicate;
    /****************************************
     *****Group Role Addition page:3
     ***************************************/
    @FindBy(id = "addPseudoGroupRoles_add_groupRoleCode")
    private WebElement addGroupRoleCode;
    @FindBy(id = "addPseudoGroupRoles_add_groupRoleName")
    private WebElement addGroupRoleName;
    @FindBy(xpath = ".//*[@id='addPseudoGroupRoles_add']/table/tbody/tr[3]/td[3]/input")
    private WebElement addGroupRoleCheckAll;
    @FindBy(id = "addPseudoGroupRoles_add_button_add")
    private WebElement addGroupRoleScreenAdd;
    @FindBy(id = "addPseudoGroupRoles_add_button_back")
    private WebElement addGroupRoleScreenBack;

    public static pseudoGroupRole_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        pseudoGroupRole_pg1 page = PageFactory.initElements(driver, pseudoGroupRole_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void setPseudoCategory(String Category) {
        Select sel = new Select(pseudoCategory);
        fl.waitSelectOptions(sel);
        sel.selectByVisibleText(Category);
        pageInfo.info("Select Category Code:" + pseudoCategory);
    }

    public void groupRoleSubmit() {
        pseudoGroupRoleSubmit.click();
    }

    public void setPseudoCategoryCodeList(String Category) {
        Select sel = new Select(pseudoCategoryCodeList);
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        sel.selectByVisibleText(Category);
        pageInfo.info("Select category Code:" + Category);
    }

    public void clickGroupRoleAdd() {
        AddGroupRole.click();
    }

    /*************Setting values in Group Role Addition screen***************/

    public void setGroupRoleCode(String Code) {
        addGroupRoleCode.sendKeys(Code);
    }

    public void setAddGroupRoleName(String Code) {
        addGroupRoleName.sendKeys(Code);
    }

    public void clickAddGroupRoleScreenAdd() {
        addGroupRoleScreenAdd.click();
    }

    public void clickAddGroupRoleScreenBack() {
        addGroupRoleScreenBack.click();
    }

    public void clickAddGroupRoleCheckAll() {
        addGroupRoleCheckAll.click();
    }
}