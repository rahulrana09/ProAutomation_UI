package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.KinUser;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/6/2017.
 */
public class AddChannelUser_pg1 extends PageInit {

    /************************************************************
     * User Creation PAGE 1
     *************************************************************/
    @FindBy(id = "confirm2_confirmAddChannelUser_userNamePrefixId")
    private WebElement UserPrefix;

    @FindBy(id = "confirm2_confirmAddChannelUser_userName")
    private WebElement FirstName;

    @FindBy(id = "confirm2_confirmAddChannelUser_lastName")
    private WebElement LastName;

    @FindBy(id = "confirm2_confirmAddChannelUser_externalCode")
    private WebElement IdentificationNum;

    @FindBy(id = "confirm2_confirmAddChannelUser_email")
    private WebElement Email;

    @FindBy(id = "confirm2_confirmAddChannelUser_genderId")
    private WebElement Gender;

    @FindBy(name = "dob")
    private WebElement DateOfBirth;

    @FindBy(name = "idIssueDate")
    private WebElement DateOfIssue;

    @FindBy(name = "idExpiryDate")
    private WebElement DateOfExpiry;

    @FindBy(id = "confirm2_confirmAddChannelUser_idType")
    private WebElement IDType;

    @FindBy(id = "confirm2_confirmAddChannelUser_idNo")
    private WebElement IDNumber;

    @FindBy(name = "merchantType")
    private WebElement merchantType;

    @FindBy(id = "confirm2_confirmAddChannelUser_webLoginId")
    private WebElement LoginId;

    @FindBy(id = "confirm2_confirmAddChannelUser_webPassword")
    private WebElement Password;

    @FindBy(id = "confirm2_confirmAddChannelUser_confWebPassword")
    private WebElement ConfirmPassword;

    @FindBy(id = "confirm2_confirmAddChannelUser_msisdn")
    private WebElement MSISDN;

    @FindBy(id = "confirm_msisdn")
    private WebElement modifyMSISDN;

    @FindBy(id = "confirm_userName")
    private WebElement modifyFirstName;

    @FindBy(id = "confirm_lastName")
    private WebElement modifyLastName;

    @FindBy(id = "confirm2_confirmAddChannelUser_prefLanguage")
    private WebElement Language;

    @FindBy(id = "confirm2_confirmAddChannelUser_button_next")
    private WebElement Next;

    @FindBy(id = "confirm2_confirmAddChannelUser_doc1")
    private WebElement UploadID;

    @FindBy(id = "confirm2_confirmAddChannelUser_doc2")
    private WebElement UploadAddProof;

    @FindBy(id = "confirm2_confirmAddChannelUser_doc3")
    private WebElement UploadPhotoProof;

    @FindBy(id = "confirm2_confirmAddChannelUser_proofType1")
    private WebElement IdType;

    @FindBy(id = "confirm2_confirmAddChannelUser_proofType2")
    private WebElement AddProofType;

    @FindBy(id = "confirm2_confirmAddChannelUser_proofType3")
    @CacheLookup
    private WebElement PhotoProofType;

    @FindBy(id = "confirm2_confirmAddChannelUser_checkAll")
    private WebElement AllowedAllDay;

    @FindBy(id = "confirm2_confirmAddChannelUser_idIssueCountry")
    private WebElement IssueCountry;

    @FindBy(id = "confirm2_confirmAddChannelUser_nationality")
    private WebElement Nationality;

    @FindBy(id = "confirm2_confirmAddChannelUser_residenceCountry")
    private WebElement CurrentResidence;

    @FindBy(id = "confirm2_confirmAddChannelUser_employerName")
    private WebElement EmployeeName;

    @FindBy(id = "confirm2_confirmAddChannelUser_postalCode")
    private WebElement PostalCode;

    @FindBy(className = "errorMessage")
    private WebElement ErrorMessage;

    @FindBy(id = "confirm_button_next")
    @CacheLookup
    private WebElement nextUserInfo;

    @FindBy(name = "kinFirstname")
    private WebElement KinFirstName;

    @FindBy(name = "kinMiddlename")
    private WebElement KinMiddleName;

    @FindBy(name = "kinLastname")
    private WebElement KinLastName;

    @FindBy(name = "kinRelationship")
    private WebElement KinRelationship;

    @FindBy(name = "kinNationality")
    private WebElement KinNationality;

    @FindBy(name = "kinNationalityNo")
    private WebElement KinIdNum;

    @FindBy(name = "kinAge")
    private WebElement KinDOB;

    @FindBy(name = "kincontactNumber")
    private WebElement KinContact;

    @FindBy(name = "registrationType")
    private WebElement selRegType;

    @FindBy(id = "confirm2_confirmAddChannelUser_contactPerson")
    private WebElement contactPersonTxtField;

    @FindBy(id = "uplFile")
    private WebElement upload;

    @FindBy(xpath = "//*[@class='actionMessage']")
    private WebElement txtUploadInfo;

    @FindBy(id = "submitButton")
    private WebElement submitBtn;

    @FindBy(id = "supportsOnlineTransReversal")
    private WebElement chkSupportOnlineTxnReversal;

    @FindBy(name = "autoSweepAllowed")
    private WebElement autoSweepAllowedRadio;

    public boolean getStatusSupportOnlineTransacionCheckBox() throws Exception {
        Utils.scrollToAnElement(chkSupportOnlineTxnReversal);
        Utils.captureScreen(pageInfo);
        return chkSupportOnlineTxnReversal.isSelected();
    }

    public void checkSupportOnlineTransactionReversal(boolean status){
        if(status && chkSupportOnlineTxnReversal.isSelected()){
            pageInfo.info("Checkbox for support Online txn reversal is already checked");
        }else if(status && !chkSupportOnlineTxnReversal.isSelected()){
            clickOnElement(chkSupportOnlineTxnReversal, "chkSupportOnlineTxnReversal");
        }else if(!status && chkSupportOnlineTxnReversal.isSelected()){
            clickOnElement(chkSupportOnlineTxnReversal, "chkSupportOnlineTxnReversal");
        }else if(!status && !chkSupportOnlineTxnReversal.isSelected()){
            pageInfo.info("Checkbox for support Online txn reversal is already NOT checked");
        }
    }

    @FindBy(xpath = "//td[contains(text(),'Download a File template')]/a/img")
    private WebElement download;


    // --------------------------Bulk Bank Associate-------------------------------------------------------
    @FindBy(id = "confirm2_confirmAddChannelUser__relationship")
    private WebElement RelationShipOfficer;

    public AddChannelUser_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AddChannelUser_pg1 init(ExtentTest t1) {
        return new AddChannelUser_pg1(t1);
    }

    public void navAddChannelUser() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_PTY_ACU", "Channel User Management");
    }

    public void navBulkBankDisAssociation() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_BLK_BNK_DISOC", "Bulk Bank DisAssociation");
    }

    public void navBulkBankAssociation() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUB_BULK_BANK_ASSOC", "Bulk Bank Association");
    }

    public void setRegType(String value) {
        selectValue(selRegType, value, "Registration Type");
    }

    public void setPostalCode() {
        setText(PostalCode, "122001", "Postal Code");
    }

    public void setEmployeeName() {
        setText(EmployeeName, "EMP" + DataFactory.getRandomNumber(3), "Employee Name");
    }

    public void setIssueCountry() {
        selectIndex(IssueCountry, 2, "IssueCountry");
    }

    public void setNationality() {
        selectIndex(Nationality, 2, "Nationality");
    }

    public void setCurrentResidence() {
        selectIndex(CurrentResidence, 2, "CurrentResidence");
    }

    public void clickNextUserDetail() {
        clickOnElement(nextUserInfo, "nextUserInfo");
    }

    public void selectPrefix() {
        selectIndex(UserPrefix, 1, "UserPrefix");
    }

    public void setFirstName(String text) {
        setText(FirstName, text, "FirstName");
    }

    public void setContactPerson(String contactPerson) {
        setText(contactPersonTxtField, contactPerson, "contactPersonTxtField");
    }

    public void setLastName(String lastName) {
        setText(LastName, lastName, "LastName");
    }

    public void setExternalCode(String extCode) {
        setText(IdentificationNum, extCode, "IdentificationNum");
    }

    public AddChannelUser_pg1 setEmail(String eMail) {
        setText(Email, eMail, "eMail");
        return this;
    }

    public void selectGender() {
        selectIndex(Gender, 1, "Gender");
    }

    public void setDateOfBirth(String date) {
        setDate(DateOfBirth, date, "DateOfBirth");
    }

    public void setDateOfIssuer(String date) {
        setDate(DateOfIssue, date, "DateOfIssue");
    }

    public void setDateOfExpiry(String date) {
        setDate(DateOfExpiry, date, "DateOfExpiry");
    }

    public void selectIdType(int index) {
        selectIndex(IDType, index, "IDType");
    }

    public void setIdNumber(String idNumber) {
        setText(IDNumber, idNumber, "IDNumber");
    }

    public void clickOnSubmitBtn() {
        clickOnElement(submitBtn, "Submit");
    }

    /**
     * @param value
     */
    public void setMerchantType(String value) {
        Select s = new Select(merchantType);
        s.selectByVisibleText(value);
        pageInfo.info("Set Merchant Type to: " + value);
    }

    /**
     * Set the Login ID
     *
     * @param loginId
     */
    public void setWebLoginId(String loginId) {
        setText(LoginId, loginId, "LoginId");
    }

    public void allowAllDays() {
        clickOnElement(AllowedAllDay, "AllowedAllDay");
    }

    public void setPassword(String passWord) {
        setText(Password, passWord, "Password");
    }

    public boolean isDisplayPassword() {
        return Utils.checkElementPresent("confirm2_confirmAddChannelUser_webPassword", Constants.FIND_ELEMENT_BY_ID, pageInfo);
    }

    public void setConfirmPassword(String passWord) {
        setText(ConfirmPassword, passWord, "ConfirmPassword");
    }

    public void setMSISDN(String msisdn) {
        setText(MSISDN, msisdn, "MSISDN");
    }

    public void selectLanguage() {
        selectIndex(Language, 1, "Language");
    }

    public void uploadID(String idPath) throws Exception {
        Thread.sleep(1000);
        setText(UploadID, idPath, "UploadID");
    }

    public void uploadAddProof(String addressPath) throws Exception {
        Thread.sleep(1000);
        setText(UploadAddProof, addressPath, "UploadAddProof");
    }

    public void uploadPhotoProof(String photoPath) throws Exception {
        Thread.sleep(1000);
        setText(UploadPhotoProof, photoPath, "UploadPhotoProof");
    }

    public void selectIDType() {
        selectIndex(IdType, 1, "IdType");
    }

    public void selectAddProofType() {
        selectIndex(AddProofType, 2, "AddProofType");
    }

    public void selectPhotoProofType() {
        selectIndex(PhotoProofType, 3, "PhotoProofType");
    }

    public AddChannelUser_pg2 clickNext() {
        clickOnElement(Next, "Next");
        return AddChannelUser_pg2.init(pageInfo);
    }

    public void changeMSISDN(String msisdn) {
        setText(modifyMSISDN, msisdn, "modifyMSISDN");
        clickNextUserDetail();
    }

    /**
     * Change MSISDN, only for User Modification
     *
     * @param name
     */
    public void changeUserName(String name) {
        this.modifyFirstName.clear();
        this.modifyFirstName.sendKeys(name);
        pageInfo.info("Set First Name: " + name);
        this.modifyLastName.clear();
        this.modifyLastName.sendKeys(name);
        pageInfo.info("Set First Name: " + name);
    }

    public void setRelationshipOfficer(String text) {
        if (fl.elementIsDisplayed(RelationShipOfficer)) {
            RelationShipOfficer.sendKeys(text);
            pageInfo.info("Set RelationShip Officer as - " + text);
        } else {
            pageInfo.info("Set RelationShip Officer Element is not available");
        }
    }

    public void setKinDetails(KinUser kinUser) throws IOException {

        try{
            setText(KinFirstName, kinUser.FirstName, "Kin First Name");
            setText(KinMiddleName, kinUser.MiddleName, "Kin Middle Name");
            setText(KinLastName, kinUser.LastName, "Kin Last Name");
            setText(KinRelationship, kinUser.RelationShip, "Kin RelationShip");
            selectVisibleText(KinNationality, kinUser.Nationality, "Kin Nationality");
            setDate(KinDOB, kinUser.DateOfBirth, "Kin User Date of Birth");
            setText(KinContact, kinUser.ContactNum,"Kin Contact num");
            setText(KinIdNum, kinUser.KinNumber, "Kin Number");
        }catch (Exception e){
            Assertion.raiseExceptionAndContinue(e, pageInfo);
        }
    }

    public boolean uploadFile(String filepath) {
        try {
            upload.sendKeys(filepath);
            pageInfo.info("Uploading file" + filepath);
            pageInfo.pass("Successfully uploaded the file:" + filepath);
            return true;
        } catch (Exception e) {
            pageInfo.fail("Failed to upload file");
            return false;
        }
    }

    public boolean fileUpload(String filePath) throws Exception {
        uploadFile(filePath);
        Utils.captureScreen(pageInfo);
        Thread.sleep(1500);
        if (ConfigInput.isAssert) {
            if (fl.elementIsDisplayed(txtUploadInfo)) {
                if (txtUploadInfo.getText().toLowerCase().contains("successful")) {
                    pageInfo.pass("Successfully uploaded the file:" + filePath);
                    return true;
                } else {
                    pageInfo.fail("Failed to uploaded the file:" + filePath);
                    return false;
                }
            } else {
                pageInfo.fail("Failed to uploaded the file:" + filePath);
            }
        }
        return false;
    }

    public String getActionMessage() {
        if (fl.elementIsDisplayed(txtUploadInfo)) {
            return txtUploadInfo.getText();
        } else {
            return null;
        }
    }

    public void clickOnStartDownload() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkUserBankAssociation");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.BULK_BANK_ASSOC);
        Thread.sleep(5000);
        download.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    public List<String> getProofTypesFromDropdown() {
        return fl.getOptionValues(PhotoProofType);
    }

    public boolean checkAutoSweepAllowedRadioAvailable(){
        if(fl.elementIsDisplayed(autoSweepAllowedRadio)){
            Utils.scrollToAnElement(autoSweepAllowedRadio);
            Utils.highLightElement(autoSweepAllowedRadio);
            return true;
        }
        return false;
    }

}
