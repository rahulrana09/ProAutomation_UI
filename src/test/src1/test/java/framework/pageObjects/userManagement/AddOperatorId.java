package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class AddOperatorId extends PageInit {

    @FindBy(id = "operator_checkAddedOperator_operatorName")
    WebElement operatorname;
    @FindBy(id = "gradeCode")
    WebElement gradeCode;
    @FindBy(id = "operator_checkAddedOperator_button_add")
    WebElement addButton;
    @FindBy(id = "operator_verifyAddOperator_smscId")
    WebElement smsId;
    @FindBy(id = "operator_verifyAddOperator_topupId")
    WebElement topupId;
    @FindBy(id = "operator_verifyAddOperator_series")
    WebElement denomination;
    @FindBy(id = "operator_confirmAddOperator_button_confirm")
    WebElement confirmButton;
    @FindBy(id = "operator_verifyAddOperator_button_add")
    WebElement submitaddButton;

    public AddOperatorId(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static AddOperatorId init(ExtentTest t1) {
        return new AddOperatorId(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void NavigateNA() throws Exception {
        navigateTo("OPPMGM_ALL", "OPPMGM_OPPADD", "Operator Management Page");
    }

    public void setOperatorname(String optName) throws Exception {
        setText(operatorname, optName, "operator Name");
        clickOnElement(addButton, "Add Button");
    }

    public void selectgradecode(String value) {
        selectValue(gradeCode, value, "Grade Code");
    }

    public void selectSmsid() {
        selectIndex(smsId, 1, "SMS Id");
    }

    public void selecttopup() {
        selectIndex(topupId, 1, "Top Up Id");
    }

    public void setDenomination(String amt) {
        setText(denomination, amt, "Denominaiton");
    }

    public void selectProviderAndConfirm(String providerid) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox' and @value='" + providerid + "']"))).click();
        clickOnElement(submitaddButton, "Submit");
        clickOnElement(confirmButton, "Confirm");
    }

}
