package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by amarnath.vb on 5/24/2017.
 */
public class pseudoUserCommon {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "categoryCodeList")
    private WebElement pseudoCategoryCode;

    public static pseudoUserCommon init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        pseudoUserCommon page = PageFactory.initElements(driver, pseudoUserCommon.class);
        fl = new FunctionLibrary(driver);
        return page;
    }
}
