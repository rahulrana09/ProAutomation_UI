package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.InstrumentTCP;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class AddChannelUser_pg5 extends PageInit {

    @FindBy(id = "subsRegistrationServiceBean_confirm_residenceCountrydesc")
    static WebElement labelSysResidenceCountry;
    @FindBy(id = "subsRegistrationServiceBean_confirm_proofType1")
    static WebElement labelSysIdType;
    /*
     * Page Objects
     */
    @FindBy(id = "add1_addChannelUser_submit")
    WebElement btnAddMore;

    @FindBy(className = "wwFormTableC")
    WebElement webTable;

    @FindBy(id = "editCH_addChannelUser_submit")
    WebElement btnEditAddMore;

    @FindBy(className = "actionMessage")
    WebElement ActionMessage;

    @FindBy(name = "action:subsRegistrationServiceBean_addMoreBankRows")
    WebElement btnAddMoreBankForSubs;

    @FindBy(name = "action:confirm2_addMoreBankRows")
    WebElement btnAddMoreBankForChUserCreation;

    @FindBy(id = "editCH_addChannelUser_submit")
    WebElement btnAddMoreBankForChUserEdit;

    @FindBy(name = "action:confirm2_save")
    WebElement btnConfirmChUserCreation;

    @FindBy(name = "action:confirm2_confirm")
    WebElement btnConfirmChUserEdit;

    @FindBy(name = "action:subsRegistrationServiceBean_confirm")
    WebElement btnSubmitSubsCreation;

    @FindBy(name = "action:confirm2_confirm")
    WebElement btnSubmitChUserCreation;

    @FindBy(name = "action:editCH_confirm")
    WebElement btnSubmitChUserEdit;

    @FindBy(name = "liquidationBankAccountNo1[0]")
    WebElement liquidationBankAccountNo;

    @FindBy(name = "liquidationBranchName1[0]")
    WebElement liquidationBranchName;

    @FindBy(name = "liquidationAccHolderName1[0]")
    WebElement liquidationAccHolderName;

    @FindBy(name = "liquidationTradingName1[0]")
    WebElement liquidationTradingName;

    @FindBy(name = "action:confirm2_addMoreLiquidationBank")
    WebElement liquidation;

    @FindBy(name = "action:editCH_addMoreLiquidationBankMod")
    WebElement modifyLiquidation;


    @FindBy(name = "bankCounterList[0].paymentTypeSelected")
    private WebElement bankNameDdown;

    @FindBy(xpath = "//input[contains(@name,'addMoreBankRows')]")
    private WebElement addMoreBank;

    @FindBy(id = "subsRegistrationServiceBean_confirm_kinFirstname")
    public WebElement confirmKinFirstName;

    @FindBy(id = "subsRegistrationServiceBean_confirm_kinMiddlename")
    private WebElement confirmKinMiddleName;

    @FindBy(id = "subsRegistrationServiceBean_confirm_kinLastname")
    private WebElement confirmKinLastName;

    @FindBy(id = "subsRegistrationServiceBean_confirm_kinRelationship")
    public WebElement confirmKinRelationShip;

    public String getConfirmKinFirstName() {
        return confirmKinFirstName.getText();
    }

    public String getConfirmKinMiddleName() {
        return confirmKinMiddleName.getText();
    }

    public String getConfirmKinLastName() {
        return confirmKinLastName.getText();
    }

    public String getConfirmKinRelationShip() {
        return confirmKinRelationShip.getText();
    }

    public AddChannelUser_pg5 addMoreBank() {
        clickOnElement(addMoreBank, "Add More Bank");
        return this;
    }


    public AddChannelUser_pg5(ExtentTest t1) {
        super(t1);
    }

    public static String GetSysIDOnConfirmationPage() {
        return new Select(labelSysIdType).getFirstSelectedOption().getText();
    }

    public static String GetCountryOfResidenceOnConfirmationPage() {
        return wait.until(ExpectedConditions.elementToBeClickable(labelSysResidenceCountry)).getText();
    }

    /**
     * INIT
     *
     * @return
     */
    public static AddChannelUser_pg5 init(ExtentTest t1) {
        return new AddChannelUser_pg5(t1);
    }

    /**
     * Click on Add More
     *
     * @throws Exception
     */
    public void clickAddMore(String categoryCode) throws Exception {
        if (categoryCode.equals(Constants.SUBSCRIBER)) {
            clickOnElement(btnAddMoreBankForSubs, "btnAddMoreBankForSubs");
        } else {
            if (fl.elementIsDisplayed(btnAddMoreBankForChUserCreation)) {
                clickOnElement(btnAddMoreBankForChUserCreation, "btnAddMoreBankForChUserCreation");
            } else if (fl.elementIsDisplayed(btnAddMoreBankForChUserEdit)) {
                clickOnElement(btnAddMoreBankForChUserEdit, "btnAddMoreBankForChUserEdit");
            } else {
                pageInfo.fail("Couldn't find the Add more button!");
            }
        }
    }

    public boolean isLiquidationFieldDisplayed() {
        return fl.elementIsDisplayed(modifyLiquidation);

    }

    /**
     * Click Next
     *
     * @param user
     * @param isModified
     */
    public void completeUserCreation(User user, boolean isModified) throws Exception {
        Utils.captureScreen(pageInfo);
        if (fl.elementIsDisplayed(btnConfirmChUserCreation)) {
            Utils.scrollToBottomOfPage();
            clickOnElement(btnConfirmChUserCreation, "btnConfirmChUserCreation");
            Utils.putThreadSleep(5000);
        } else if (fl.elementIsDisplayed(btnConfirmChUserEdit)) {
            Utils.scrollToBottomOfPage();
            clickOnElement(btnConfirmChUserEdit, "btnConfirmChUserEdit");
            Utils.putThreadSleep(5000);
        } else {
            pageInfo.fail("Couldn't find the button to Confirm Channel User Creation/Edit!");
        }
    }

    /**
     * Click On next
     */
    public AddChannelUser_pg5 clickNext(String categoryCode) throws IOException {
        Utils.captureScreen(pageInfo);
        if (categoryCode.equals(Constants.SUBSCRIBER)) {
            clickOnElement(btnSubmitSubsCreation, "btnSubmitSubsCreation");
        } else {
            if (fl.elementIsDisplayed(btnSubmitChUserCreation)) {
                clickOnElement(btnSubmitChUserCreation, "btnSubmitChUserCreation");
            } else if (fl.elementIsDisplayed(btnSubmitChUserEdit)) {
                clickOnElement(btnSubmitChUserEdit, "btnSubmitChUserEdit");
            } else {
                pageInfo.fail("Couldn't find the button to Submit Channel User Creation/Edit!");
            }
        }
        return this;
    }

    public boolean isSelectedTextIsShownOnConfirmPage(String text) {
        if (webTable.findElements(By.xpath("//select/option[text() = '" + text + "']")).size() > 0) {
            return true;
        }
        return false;
    }

    private void clickOnAddMore(int counter, String catCode) throws Exception {
        if (counter != 0) {
            try {
                clickAddMore(catCode);
            } catch (Exception e) {
                e.printStackTrace();
                if (e.getMessage().contains("element is not attached")) {
                    Thread.sleep(8000);
                    clickAddMore(catCode);
                }
            }
        }
    }

    /**
     * addBankPreferences
     *
     * @param user
     * @throws Exception
     */
    public AddChannelUser_pg5 addBankPreferences(User user) throws Exception {
        try{
            int counter = 0;
            for (String bankName : user.linkedBanks) {
                Bank bank = DataFactory.getBankFromAppData(bankName);

                if (bank.IsLiquidation) // continue if this is liquidation Bank
                    continue;

                String roleName = user.hasPayInstTypeLinked(bank.BankName, bank.ProviderName).ProfileName;
                InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, bank.ProviderName, bank.BankName);

                clickOnAddMore(counter, user.CategoryCode);

                // UI elements
                WebElement providerSelect = driver.findElement(By.name("bankCounterList[" + counter + "].providerSelected"));
                WebElement bankSelect = driver.findElement(By.name("bankCounterList[" + counter + "].paymentTypeSelected"));
                WebElement gradeSelect = driver.findElement(By.name("bankCounterList[" + counter + "].channelGradeSelected"));
                WebElement gRole = driver.findElement(By.name("bankCounterList[" + counter + "].groupRoleSelected"));
                WebElement customerID = driver.findElement(By.name("bankCounterList[" + counter + "].customerId"));
                WebElement tcpSelect = driver.findElement(By.name("bankCounterList[" + counter + "].tcpSelected"));
                WebElement accountNum = driver.findElement(By.name("bankCounterList[" + counter + "].accountNumber"));
                WebElement selPrimaryAccount = driver.findElement(By.name("bankCounterList[" + counter + "].primaryAccountSelected"));


                selectVisibleText(providerSelect, bank.ProviderName, "Provider");
                Thread.sleep(Constants.MAX_WAIT_TIME);
                selectVisibleText(bankSelect, bank.BankName, "Bank");
                Thread.sleep(Constants.MAX_WAIT_TIME);
                selectVisibleText(gradeSelect, user.GradeName, "GradeName");
                Thread.sleep(Constants.MAX_WAIT_TIME);
                selectVisibleText(gRole, roleName, "Mobile Role Name");
                Thread.sleep(Constants.MAX_WAIT_TIME);
                selectVisibleText(tcpSelect, insTcp.ProfileName, "Instrument TCP");
                Thread.sleep(Constants.MAX_WAIT_TIME);

                /*
                Set The Customer ID, set with the default Bank
                */
                if (!AppConfig.isBankAccLinkViaMsisdn) {
                    String custId = DataFactory.getRandomNumberAsString(9);
                    setText(customerID, custId, "Customer Id");
                    Thread.sleep(Constants.MAX_WAIT_TIME);
                    if (bank.IsDefaultBank && DataFactory.isProviderDefault(bank.ProviderName)) {
                        user.setDefaultCustId(custId); // update User Object
                    } else if (!bank.IsTrust && !bank.IsLiquidation) {
                        user.setNonDefaultCustId(custId); // update User Object
                    }
                }

                /*
                Set The account number, set with the default Bank
                */
                if (!AppConfig.isBankAccLinkViaMsisdn) {
                    String accNum = DataFactory.getRandomNumberAsString(9);
                    setText(accountNum, accNum, "Account Number");
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    if (bank.IsDefaultBank && DataFactory.isProviderDefault(bank.ProviderName)) {
                        user.setDefaultAccNum(accNum); // update the User Object
                    }
                }

                if (bank.IsDefaultBank)
                    selectVisibleText(selPrimaryAccount, "Yes", "Primary Account");
                else
                    selectVisibleText(selPrimaryAccount, "No", "Primary Account");

                Thread.sleep(Constants.MAX_WAIT_TIME);
                counter++;
            }
            Utils.captureScreen(pageInfo);
        }catch (Exception e){
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }

        return this;
    }

    public AddChannelUser_pg5 addLiquidationBankPreferences(User user, boolean... isModifyLiquidationData) throws Exception {
        try {
            boolean isModified = isModifyLiquidationData.length > 0 ? isModifyLiquidationData[0] : false;

            int countLiquidation = 0;
            for (String bankName : user.linkedBanks) {
                Bank bank = DataFactory.getBankFromAppData(bankName);
                // continue if this is Not liquidation Bank
                if (!bank.IsLiquidation)
                    continue;

                if (isModified)
                    modifyLiquidationclick();
                else
                    liquidationclick();

                WebElement liqAccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + countLiquidation + "]"));
                WebElement liqBranchName = driver.findElement(By.name("liquidationBranchName1[" + countLiquidation + "]"));
                WebElement liqAccountHolderName = driver.findElement(By.name("liquidationAccHolderName1[" + countLiquidation + "]"));
                WebElement tradingName = driver.findElement(By.name("liquidationTradingName1[" + countLiquidation + "]"));
                WebElement providerSelect = driver.findElement(By.id("liquidationProvider" + countLiquidation + ""));
                WebElement bankSelect = driver.findElement(By.id("liquidationBankListId" + countLiquidation + ""));

                selectVisibleText(providerSelect, bank.ProviderName, "Provider");
                selectVisibleText(bankSelect, bank.BankName, "Bank");
                String liqacnum = "" + DataFactory.getRandomNumber(9);
                if (DataFactory.isProviderDefault(bank.ProviderName))
                    user.setLiquidationBankAccountNumber(liqacnum);

                setText(liqAccountNum, liqacnum, "liq Account Number");
                String liqbranchname = "LiqBranch" + DataFactory.getRandomNumber(4);
                if (DataFactory.isProviderDefault(bank.ProviderName))
                    user.setLiquidationBankBranchName(liqbranchname);

                setText(liqBranchName, liqbranchname, "liq Branch Name");
                String liqaccountholdername = "LiqAccount" + DataFactory.getRandomNumber(2);
                if (DataFactory.isProviderDefault(bank.ProviderName))
                    user.setAccountHolderName(liqaccountholdername);

                setText(liqAccountHolderName, liqaccountholdername, "liq Account Holder Name");
                String freqtradingname = "LiqBranchAccount" + DataFactory.getRandomNumber(4);
                if (DataFactory.isProviderDefault(bank.ProviderName))
                    user.setFrequencyTradingName(freqtradingname);

                setText(tradingName, freqtradingname, "Frequency Trading Name");
                countLiquidation++;
            }

            selectLiquidationFrequency(user.LiquidationFrequency, user.LiquidationDay);
            Utils.captureScreen(pageInfo);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }

        return this;
    }

    public AddChannelUser_pg5 ClickOnAddMoreToRegistrMorWallets() throws Exception {
        Thread.sleep(1200);
        clickOnElement(btnAddMore, "btnAddMore");
        return this;

    }

    public void liquidationclick() throws Exception {
        liquidation.click();
    }

    public void modifyLiquidationclick() throws Exception {
        modifyLiquidation.click();
    }

    public void selectLiquidationFrequency(String frequency, String day) throws Exception {
        driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='" + frequency + "']")).click();
        pageInfo.info(frequency + " is selected");
        if (frequency.equals("Monthly")) {
            Select sel = new Select(driver.findElement(By.id("dateOfMonth")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
        if (frequency.equals("Weekly")) {
            Select sel = new Select(driver.findElement(By.id("dayOfWeek")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
        if (frequency.equals("Fortnightly")) {
            Select sel = new Select(driver.findElement(By.id("fortnightlyVar")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
    }

    public AddChannelUser_pg5 frequencyFieldsPresent(String Frequency) throws Exception {
        WebElement freqtype = driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='" + Frequency + "']"));
        freqtype.isDisplayed();
        pageInfo.info(Frequency + " is displayed");
        return this;
    }

    public AddChannelUser_pg5 addvalues(String liqacnum, String liqbranchname, String liqaccountholdername, String freqtradingname) {

        liquidationBankAccountNo.sendKeys(liqacnum);
        pageInfo.info(liqacnum + " is liquidationaccountnumber");

        liquidationBranchName.sendKeys(liqbranchname);
        pageInfo.info(liqbranchname + " is liquidationbranchname");

        liquidationAccHolderName.sendKeys(liqaccountholdername);
        pageInfo.info(liqaccountholdername + " is liquidationaccountholdername");

        liquidationTradingName.sendKeys(freqtradingname);
        pageInfo.info(freqtradingname + " is frequencytradingname");
        return this;

    }

    public boolean verifyBankNamePresentInBankDdown(String bankName) {
        List<String> bankList;
        Select select = new Select(bankNameDdown);
        bankList = fl.getOptionText(select);
        if (bankList.contains(bankName)) {
            return true;
        }

        return false;
    }

    public boolean verifyBankNamesPresentInBankDdown(List<String> bankNames) {
        List<String> bankList;
        Select select = new Select(bankNameDdown);
        bankList = fl.getOptionText(select);

        for (String bankName : bankNames) {
            if (!bankList.contains(bankName)) {
                pageInfo.info("Bank " + bankName + " is not present in Dropdown.");
                return false;
            }
        }
        return true;
    }

    public void selectWeekly(String dayofweek) throws Exception {
        driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Weekly']")).click();
        pageInfo.info("Weekly is selected");
        Select sel = new Select(driver.findElement(By.id("dayOfWeek")));
        sel.selectByVisibleText(dayofweek);
        pageInfo.info(dayofweek + " is selected");
    }

    public void selectFortnightly(String day) throws Exception {
        driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Fortnightly']")).click();
        pageInfo.info("Fortnightly is selected");
        Select sel = new Select(driver.findElement(By.id("fortnightlyVar")));
        sel.selectByVisibleText(day);
        pageInfo.info(day + " is selected");
    }

    public void selectMonthly(String dateOfMonth) throws Exception {
        driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='Monthly']")).click();
        pageInfo.info("Monthly is selected");
        Select sel = new Select(driver.findElement(By.id("dateOfMonth")));
        sel.selectByVisibleText(dateOfMonth);
        pageInfo.info(dateOfMonth + " is selected");
    }
}
