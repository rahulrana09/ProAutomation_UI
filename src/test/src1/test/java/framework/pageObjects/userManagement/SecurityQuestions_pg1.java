package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SecurityQuestions_pg1 extends PageInit {

    @FindBy(id = "question")
    private WebElement question;
    @FindBy(id = "ans")
    private WebElement ans;
    @FindBy(id = "Next_Button")
    private WebElement nextBtn;
    @FindBy(id = "//button[.='Submit']")
    private WebElement submitBtn;
    @FindBy(id = "PINMANAGE_ALL")
    private WebElement pinManagement;
    @FindBy(id = "PINMANAGE_PINRESET")
    private WebElement resetPIN;

    public SecurityQuestions_pg1(ExtentTest t1) {
        super(t1);
    }

    public static SecurityQuestions_pg1 init(ExtentTest t1) {
        return new SecurityQuestions_pg1(t1);
    }

    public void setAnswer(String answer) {
        setText(ans, answer, "Answer");
    }

    public String getQuestion() {
        return question.getText();
    }

    public void clickNext() {
        clickOnElement(nextBtn, "Next Button");
    }

    public void clickSubmit() {
        clickOnElement(submitBtn, "Submit Button");
    }

    public void checkResetPINLink() {
        try {
            fl.contentFrame();
            fl.clickLink("PINMANAGE_ALL");
            if (resetPIN.isDisplayed()) {
                pageInfo.pass("Reset PIN link is displayed");
            } else {
                pageInfo.fail("Reset PIN link is not displayed");
            }
        } catch (Exception e) {
            pageInfo.fail("PIN Managment link is not displayed");
        }
    }

    public void checkPINManagementLink() throws Exception {
        try {
            fl.contentFrame();
            resetPIN.isDisplayed();
            pageInfo.fail("PIN Managment link is displayed");
        } catch (Exception e) {
            pageInfo.pass("PIN Managment link is not displayed");
        }
    }
}
