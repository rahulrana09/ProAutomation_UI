package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by amarnath.vb on 5/29/2017.
 */
public class approvePseudoCategory_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /************************************************************
     * Pseudo ApproveCategory PAGE 1
     *************************************************************/

    @FindBy(id = "addPseudo_save_namePrefix")
    private WebElement pseudoNamePrefix;
    @FindBy(id = "_button_approve")
    private WebElement approvebtn;

    public static approvePseudoCategory_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        approvePseudoCategory_pg1 page = PageFactory.initElements(driver, approvePseudoCategory_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void Navigate() throws Exception {
        fl.leftNavigation("PSEUDO2_ALL", "PSEUDO2_PSEUDO_CAT_APPROVAL");
    }

    public void selectParticularCategory(String cat) throws Exception {
        driver.findElement(By.xpath("(//tr[td[contains(text(),'" + cat + "')]]/td/a)[1]")).click();
        approvebtn.click();
    }
}
