package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class ApproveOperatorUser_pg1 extends PageInit {


    @FindBy(id = "loadUser_showSystemParty_msisdnWeb")
    WebElement loginID;
    @FindBy(id = "loadUser_showSystemParty_button_submit")
    WebElement submitButton;
    @FindBy(id = "loadUser_loadSystemParty_submit")
    WebElement submitButtonPage2;
    @FindBy(id = "loadUser_loadSystemParty_approve")
    WebElement approveButton;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    @FindBy(id = "loadUser_loadSystemParty_back")
    WebElement back;
    //==============================used in p1=====================//
    @FindBy(id = "loadUser_loadSystemParty_reject")
    private WebElement reject;

    public ApproveOperatorUser_pg1(ExtentTest t1) {
        super(t1);
    }

    /**
     * INIT
     *
     * @return
     */
    public static ApproveOperatorUser_pg1 init(ExtentTest t1) {
        return new ApproveOperatorUser_pg1(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */
    public void Navigate() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_PTY_ASUA", "Approve Operator User");

    }

    public void NavigateBPA() throws Exception {
        fl.leftNavigation("BPAMGT_ALL", "BPAMGT_EPTY_ASUA");
    }

    public void loginID_SetText(String text) {
        setText(loginID, text, "loginID");
    }

    public void clickSubmitPage1() {
        clickOnElement(submitButton, "submitButton");
    }

    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    public void clickSubmitPage2() {
        clickOnElement(submitButtonPage2, "submitButtonPage2");

    }

    public void clickApprovePage3() {
        clickOnElement(approveButton, "approveButton");
    }

    public void clickReject() {
        clickOnElement(reject, "reject");
    }

    public void clickOnBackButton() {
        clickOnElement(back, "back");
    }


    public void checkSubmitButtonIsAvailable() {
        Utils.checkElementPresent("loadUser_showSystemParty_msisdnWeb", Constants.FIND_ELEMENT_BY_ID);
    }
}
