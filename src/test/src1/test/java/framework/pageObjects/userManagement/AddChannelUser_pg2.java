package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by rahul.rana on 5/7/2017.
 */
public class AddChannelUser_pg2 extends PageInit {

    @FindBy(id = "ownerId")
    public WebElement OwnerName;
    @FindBy(id = "parentId")
    public WebElement ParentCategory;
    @FindBy(id = "userNameId")
    public WebElement FirstName;
    @FindBy(id = "parentNameId")
    public WebElement Parent;
    /***************************************************************************************
     * Page Objects
     ****************************************************************************************/
    @FindBy(id = "domainCodeList")
    private WebElement Domain;
    @FindBy(id = "categoryCodeList")
    private WebElement Category;
    @FindBy(id = "zoneId")
    private WebElement Zone;
    @FindBy(id = "geoId")
    private WebElement Geography;
    @FindBy(id = "confirm2_confirmAddChannelUser_button_next")
    private WebElement Next;
    @FindBy(id = "confirm_msisdn")
    private WebElement MSISDN;
    /**
     * NAVIGATION
     */
    @FindBy(id = "confirm_button_next")
    @CacheLookup
    private WebElement nextUserInfo;
    @FindBy(id = "childOwnerId")
    @CacheLookup
    private WebElement childOwnerName;
    @FindBy(id = "childParentNameId")
    @CacheLookup
    private WebElement childParentName;
    @FindBy(id = "editCH_assignGroupRole_button_next")
    @CacheLookup
    private WebElement modifyHierarchyNextBtn;

    public AddChannelUser_pg2(ExtentTest t1) {
        super(t1);
    }

    public static AddChannelUser_pg2 init(ExtentTest t1) {
        return new AddChannelUser_pg2(t1);
    }

    public boolean isHierarchyPageOpen() {
        return fl.elementIsDisplayed(Domain);
    }

    /**
     * Select User's Domain Name
     *
     * @param domainName
     */
    public void selectDomain(String domainName) {
        selectVisibleText(Domain, domainName, "Domain");
    }

    /**
     * Select User's Category Name
     *
     * @param category
     */
    public void selectCategory(String category) {
        selectVisibleText(Category, category, "Category");
    }

    /**
     * Select User's Owner Name
     *
     * @param owner
     * @throws Exception
     */
    public void selectOwnerName(String owner) throws Exception {
        selectVisibleText(OwnerName, owner, "OwnerName");
    }

    /**
     * Select User's Parent Category Name
     *
     * @param category
     */
    public void selectParentCategory(String category) {
        selectVisibleText(ParentCategory, category, "ParentCategory");
    }

    /**
     * Set First Name
     *
     * @param name
     */
    public void setFirstName(String name) {
        setText(FirstName, name, "FirstName");
    }

    public void selectParentName(String parentName) throws Exception {
        clickOnElement(Parent, "Parent");
        selectVisibleText(Parent, parentName, "Parent Name");
    }

    /**
     * Select Zone
     *
     * @param zone - Zone Name
     */
    public void selectZone(String zone) {
        if (zone == null) {
            // TODO this logic need to be re visited
            Select sel = new Select(Zone);
            if (sel.getOptions().size() > 1) {
                selectIndex(Zone, 3, "Zone");
            } else {
                selectIndex(Zone, 0, "Zone");
            }
        } else {
            selectVisibleText(Zone, zone, "Zone");
        }
    }

    /**
     * Select Geography
     *
     * @param geoName
     */
    public void selectGeography(String geoName) throws Exception {
        clickOnElement(Geography, "Geography");
        Thread.sleep(2000);
        if (geoName == null) {
            Select sel = new Select(Geography);
            if (sel.getOptions().size() > 1) {
                selectIndex(Geography, 1, "Geography");
            } else {
                selectIndex(Geography, 0, "Geography");
            }
        } else {
            selectVisibleText(Geography, geoName, "Geography");
        }
        if (ConfigInput.isInternetExplorer) {
            clickOnElement(Zone, "Handling ie inconsistency");
        }

    }

    /**
     * Click Next Button
     *
     * @return
     */
    public void clickNext() {
        if (fl.elementIsDisplayed(Next))
            clickOnElement(Next, "Next");
        else if (fl.elementIsDisplayed(modifyHierarchyNextBtn))
            clickOnElement(modifyHierarchyNextBtn, "modifyHierarchyNextBtn");
    }

    public void clickNextUserDetail() {
        clickOnElement(nextUserInfo, "nextUserInfo");
    }

    public void selectChildOwnerName(String text) {
        if (fl.elementIsDisplayed(childOwnerName)) {
            selectVisibleText(childOwnerName, text, "childOwnerName");
        } else {
            pageInfo.info("childOwnerName element is not available");
        }

    }

    public void selectChildParentName(String text) {
        if (fl.elementIsDisplayed(childParentName)) {
            selectVisibleText(childParentName, text, "childParentName");
        } else {
            pageInfo.info("childParentName element is not available");
        }
    }

    public void changeMSISDN(String msisdn) {
        this.MSISDN.clear();
        this.MSISDN.sendKeys(msisdn);
        clickNextUserDetail();
        pageInfo.info("Set First Name: " + msisdn);
    }

    public List<String> getCategoryNamesFromCategoryDropdown() {
        return fl.getOptionText(Category);
    }
}
