package framework.pageObjects.userManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ModifyOperatorUser_page3 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_updateButt")
    WebElement confirmButton;
    @FindBy(id = "confirmEditCh_confirmUpdateSystemParty_deleteButt")
    WebElement delConfirmButton;
    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;

    /**
     * INIT
     *
     * @return
     */
    public static ModifyOperatorUser_page3 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        //Change it for different page
        ModifyOperatorUser_page3 page = PageFactory.initElements(driver, ModifyOperatorUser_page3.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void confirmButton_Click() {
        confirmButton.click();
    }


    public String getActionMessage() {
        try {
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return ErrorMessage.getText();
        }
    }

    public void delConfirmButton_Click() {
        delConfirmButton.click();
    }

    /**
     * get error message
     *
     * @return
     */
    public String getErrorMessage() {
        try {
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

}
