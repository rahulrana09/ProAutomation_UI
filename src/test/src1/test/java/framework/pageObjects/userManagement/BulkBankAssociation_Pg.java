package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by ravindra.dumpa on 12/12/2017.
 */
public class BulkBankAssociation_Pg {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(xpath = "//*[contains(text(),'Download a File template')]//img")
    private static WebElement DownloadImage;
    @FindBy(id = "submitButton")
    private static WebElement SubmitCsv;
    @FindBy(xpath = "//*[contains(text(),'Download Error Log File')]//img")
    private static WebElement logFileImage;
    @FindBy(xpath = "//a[contains(text(),'Show list of codes')]")
    private static WebElement listOfCodes;
    @FindBy(name = "bulkUploadFile")
    private WebElement Upload;

    public static BulkBankAssociation_Pg init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        BulkBankAssociation_Pg page = PageFactory.initElements(driver, BulkBankAssociation_Pg.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public static void clickOnStartDownload() throws Exception {
        if (AppConfig.isUserTypeAllowed) {
            pageInfo.info("Deleting existing file with prefix - BulkUserBankAssociation");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserBankAssociation"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            DownloadImage.click();
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            pageInfo.info("Clicked on Download Image!");
        } else {
            if (!AppConfig.isUserTypeAllowed) {
                pageInfo.info("Deleting existing file with prefix - BulkUserBankAssociationEconet");
                FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserBankAssociationEconet"); // is hardcoded can be Generic TODO
                Thread.sleep(5000);
                DownloadImage.click();
                pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
                pageInfo.info("Clicked on Download Image!");
            }
        }
    }

    public BulkBankAssociation_Pg navBulkBankAssociation_Pg() throws Exception {
        fl.leftNavigation("SUBSCRIBER_ALL", "SUBSCRIBER_SUB_BULK_BANK_ASSOC");
        pageInfo.info("Navigate to Subscriber Management > Bulk bank Association");
        return this;
    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public void navBulkUserRegistrationApproval() throws Exception {
        fl.leftNavigation("BULK_ALL", "BULK_BULK_AP");
        pageInfo.info("Navigate to Bulk channel user Registration Approval Page");
    }

    public BulkBankAssociation_Pg submitCsv() {
        SubmitCsv.click();
        pageInfo.info("Click on Submit!");
        return this;
    }

    public BulkBankAssociation_Pg uploadFile(String file) {
        Upload.sendKeys(file);
        pageInfo.info("Upload File : " + file);
        return this;
    }

    public BulkBankAssociation_Pg clickOnListOfCodes() {
        listOfCodes.click();
        pageInfo.info("clicked on list of codes  ");
        return this;
    }

    public BulkBankAssociation_Pg verifyListOfCodesPage() {
        Set<String> windowhandles = driver.getWindowHandles();
        String parentWh = driver.getWindowHandle();
        Iterator<String> itr = windowhandles.iterator();
        while (itr.hasNext()) {
            driver.switchTo().window(itr.next());
            String title = driver.getTitle();
            if (title.contains("ID Name Mapping page")) {
                break;
            }
        }

        try {
            driver.findElement(By.xpath("//*[contains(text(),'" + DataFactory.getDefaultBankNameForDefaultProvider() + "']"));
            pageInfo.info("verified default bank");
        } catch (Exception e) {
            pageInfo.info(DataFactory.getDefaultBankNameForDefaultProvider() + "default bank is not there");
        }

        driver.switchTo().window(parentWh);
        return this;
    }

    public void clickOnStartDownloadLogFile() throws Exception {
        pageInfo.info("Deleting existing file with prefix - BulkUserBankAssociationLogs");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserBankAssociationLogs"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        logFileImage.click();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        pageInfo.info("Clicked on Download Image!");
    }

    public void checkLogFileDetails(String msisdn) throws Exception {
        Thread.sleep(500);
        try {
            File dir = new File(FilePath.dirFileDownloads);
            String logFile = "";
            String[] types = {"log"};
            Collection<File> collection = FileUtils.listFiles(dir, types, true);
            for (File s : collection) {
                if (s.getName().contains("BulkUserRegistrationLogs")) {
                    logFile = s.getName();
                    break;
                }
            }
            File file = new File(FilePath.dirFileDownloads + "/" + logFile);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {"2", "The given MSISDN" + msisdn + "is already registered in the system as different user"};
            //we know that the log will start from 9th line so moving to 9th line
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex == 9) {
                    //reading the 3rd line for log
                    for (int i = 0; i < matcherArray.length; i++) {
                        //replacing all the spaces in the log file and then comparing
                        if (line.replaceAll("\\s+", "").contains(matcherArray[i].replaceAll("\\s", ""))) {
                            pageInfo.pass("found text as " + matcherArray[i]);
                        } else {
                            success = false;
                            pageInfo.fail("couldn't match the details of the log file as desired");
                            break;
                        }
                    }
                    if (success) {
                        pageInfo.pass("log file contents matched as desired");
                    }
                    //now we don't want to read further
                    break;
                }
                lineIndex++;
            }
            s.close();
        } catch (IOException ioex) {
            // handle exception...
        }

    }


}