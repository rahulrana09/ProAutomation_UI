package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Approveadduser {


    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /************************************************************
     * Pseudo ApproveCategory PAGE 1
     *************************************************************/

    @FindBy(id = "addPseudo_addApproveConfirm_button_confirm")
    private WebElement confirmbutton;

    public static framework.pageObjects.userManagement.Approveadduser init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        framework.pageObjects.userManagement.Approveadduser page = PageFactory.initElements(driver, framework.pageObjects.userManagement.Approveadduser.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void Navigate() throws Exception {
        fl.leftNavigation("PSEUDO2_ALL", "PSEUDO2_PSEUDO_ADD2");
    }

    public void selectParticularCategory(String cat) throws Exception {
        driver.findElement(By.xpath("(//tr[td[contains(text(),'" + cat + "')]]/td/a)[1]")).click();
        confirmbutton.click();
    }

    public String getUserID() throws Exception {
        String userid = driver.findElement(By.xpath("//div[@id='content']//tr[2]/td[1]")).getText();
        return userid;
    }
}


