package framework.pageObjects.userManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;


public class LiquidationDetailsConfirmPage extends PageInit {


    public LiquidationDetailsConfirmPage(ExtentTest t1) {
        super(t1);
    }

    public static LiquidationDetailsConfirmPage init(ExtentTest t1) {
        return new LiquidationDetailsConfirmPage(t1);
    }


    /**
     * INIT
     *
     * @return
     */


    public void verifyHeaders() {
        ArrayList fields = new ArrayList();
        fields.add("MFS Provider");
        fields.add("Liquidation Bank Name");
        fields.add("Liquidation Bank Account Number");
        fields.add("Liquidation Bank Branch Name");
        fields.add("Account Holder Name");
        fields.add("Frequency Trading Name");

        for (int i = 1, j = 0; i < 7; i++, j++) {

            String elementText = "//table[@class ='wwFormTableC']/tbody[2]/tr[1]/td[" + i + "]";
            WebElement elem = driver.findElement(By.xpath(elementText));
            boolean isElementExist = elem.isDisplayed();
            Assert.assertTrue(isElementExist);
            System.out.println(fields.get(j) + " is Present");
        }
    }

    public void verifyHeadersDetails(String Provider, String Bankid, String LiquidationBankAccountNumber, String LiquidationBankBranchName, String AccountHolderName, String FrequencyTradingName) throws Exception {

        ArrayList fields = new ArrayList();
        fields.add(Provider);
        fields.add(Bankid);
        fields.add(LiquidationBankAccountNumber);
        fields.add(LiquidationBankBranchName);
        fields.add(AccountHolderName);
        fields.add(FrequencyTradingName);

        for (int i = 0; i < 2; i++) {
            String elementText = "//table[@class = 'wwFormTableC']/tbody[2]/tr[3]/td/select/option[text()='" + fields.get(i) + "']";
            String elem = driver.findElement(By.xpath(elementText)).getText();
            Assert.assertEquals(elem, fields.get(i));
        }

        for (int i = 2; i < 6; i++) {
            String elementText = "//table[@class = 'wwFormTableC']/tbody[2]/tr[3]/td/input[@value='" + fields.get(i) + "']";
            String elem = driver.findElement(By.xpath(elementText)).getAttribute("value");
            Assert.assertEquals(elem, fields.get(i));
        }
    }

}
