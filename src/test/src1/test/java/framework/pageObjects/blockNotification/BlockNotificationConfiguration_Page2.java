package framework.pageObjects.blockNotification;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BlockNotificationConfiguration_Page2 extends PageInit {

    @FindBy(name = "sourceSystem")
    private WebElement selectSourceValue;

    @FindBy(xpath = "//input[@value ='Submit']")
    WebElement submitButton;

    @FindBy(name = "serviceType")
    WebElement selectServiceValue;

    @FindBy(id = "notificationTemplate")
    WebElement selectTemplateValue;

    @FindBy(xpath = "//input[@value = 'Save']")
    WebElement saveButton;


    public BlockNotificationConfiguration_Page2(ExtentTest t1) {
        super(t1);
    }

    public static BlockNotificationConfiguration_Page2 init(ExtentTest t1) {
        return new BlockNotificationConfiguration_Page2(t1);
    }

    public BlockNotificationConfiguration_Page2 navigateToBlockNotification() throws Exception {
        navigateTo("BLCKNOTIFY_ALL", "BLCKNOTIFY_BLOCK_NOTIFYCONF", "Block Notification Configuration");
        return this;
    }

    public BlockNotificationConfiguration_Page2 selectSourceName(String txt){
        selectVisibleText(selectSourceValue,txt,"selectSourceValue");
            return this;

    }

    public BlockNotificationConfiguration_Page2 clickSubmit(){
        clickOnElement(submitButton,"submitButton");
        return this;
    }

    public BlockNotificationConfiguration_Page2 selectServiceType(String txt){
        selectVisibleText(selectServiceValue,txt,"selectServiceValue");
        return this;
    }

    public BlockNotificationConfiguration_Page2 selectNotificationTemplate(String[] txt){
        for(int i=0 ; i< txt.length; i++) {
            driver.findElement(By.xpath("//input[@type='checkbox' and @value='" + txt[i] + "']")).click();
        }
        return this;
    }

    public BlockNotificationConfiguration_Page2 clickSave() {
        clickOnElement(saveButton, "saveButton");
        return this;
    }


    public BlockNotificationConfiguration_Page2 accept(){
        driver.switchTo().alert().accept();
        return this;
    }

    public BlockNotificationConfiguration_Page2 dismiss(){
        driver.switchTo().alert().dismiss();
        return this;
    }

}



