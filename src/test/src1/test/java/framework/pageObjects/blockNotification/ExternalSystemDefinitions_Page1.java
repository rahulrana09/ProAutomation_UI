package framework.pageObjects.blockNotification;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ExternalSystemDefinitions_Page1 extends PageInit {


    @FindBy(name = "sourceSystemName")
    WebElement sourceSystemName;

    @FindBy(xpath = "//input[@value = 'Save']")
    WebElement saveButton;

    @FindBy(xpath = "//input[@value = 'Modify Details']")
    WebElement modifyDetailsButton;

    @FindBy(xpath = "//*[@type='radio']")
    private WebElement radioBtn;

    @FindBy(xpath = "//input[@value = 'Modify']")
    private WebElement modifyButton;

    @FindBy(name = "newSourceName")
    private WebElement newSourceName;

    @FindBy(xpath = "//input[@value = 'Delete']")
    WebElement deleteButton;


    public ExternalSystemDefinitions_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ExternalSystemDefinitions_Page1 init(ExtentTest t1) {
        return new ExternalSystemDefinitions_Page1(t1);
    }

    public ExternalSystemDefinitions_Page1 navigateToBlockNotification() throws Exception {
        navigateTo("BLCKNOTIFY_ALL", "BLCKNOTIFY_BLCK_NOTIFY", "External System Definitions");
        return this;
    }

    public ExternalSystemDefinitions_Page1 enterSourceName(String sourceName) {
        setText(sourceSystemName, sourceName, "Source Name");
        return this;
    }



    public ExternalSystemDefinitions_Page1 clickSubmit() {
        clickOnElement(saveButton, "saveButton");
        return this;
    }

    public ExternalSystemDefinitions_Page1 clickModifyDetails() {
        clickOnElement(modifyDetailsButton, "modifyDetailsButton");
        return this;
    }

    public void radioBtn_Click() {
        clickOnElement(radioBtn, "Radio Button");
    }

    public ExternalSystemDefinitions_Page1 radioBtn_ClickBasedOnText(String txt) {
        radioBtn = driver.findElement(By.xpath("//label[.='" + txt + "']/../input[@type='radio']"));
        clickOnElement(radioBtn, "Radio Button");
        return this;
    }

    public ExternalSystemDefinitions_Page1 clickDelete() {
        clickOnElement(deleteButton, "deleteButton");
        return this;
    }

    public ExternalSystemDefinitions_Page1 clickModify() {
        clickOnElement(modifyButton, "modifyButton");
        return this;
    }

    public ExternalSystemDefinitions_Page1 enterNewSourceName(String sourceName) {
        setText(newSourceName, sourceName, "New Source Name");
        return this;
    }


}
