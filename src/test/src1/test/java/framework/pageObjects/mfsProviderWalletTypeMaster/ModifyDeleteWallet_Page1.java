/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: automation.team
 *  Date:
 *  Purpose: Modify and Delete Wallet PageObject
 */
package framework.pageObjects.mfsProviderWalletTypeMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.Wallet;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;


public class ModifyDeleteWallet_Page1 extends PageInit {

    //Page Objects
    @FindBy(name = "action:MfsModifyType_modify")
    private WebElement modifyButton1;
    @FindBy(name = "action:MfsModifyType_displayServicesForUpdate")
    private WebElement modifyButton2;
    @FindBy(id = "MfsModifyType_modify_defaultWalletType")
    private WebElement defaultWallet;
    @FindBy(id = "MfsModifyType_displayServicesForUpdate_checkAll")
    private WebElement checkAll;
    @FindBy(name = "action:MfsModifyType_confirmModify")
    private WebElement submit;
    @FindBy(id = "MfsModifyType_mfsProviderList_button_delete")
    private WebElement deleteButton;
    @FindBy(id = "MfsModifyType_delete_button_delete")
    private WebElement confirmDeleteButton;
    @FindBy(className = "wwFormTableC")
    private WebElement webtable;

    public ModifyDeleteWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    public void checkAllServices() {
        clickOnElement(checkAll, "Check All");
    }

    public void submitModification() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(5000);
        clickOnElement(submit, "Submit");
    }

    /**
     * Navigate to MFS Provider Wallet Type Master > Modify/Delete Wallet page
     *
     * @throws Exception
     */
    public void navigateToModifyDeleteWallet() throws Exception {
        navigateTo("MFSWTM_ALL", "MFSWTM_MFSMD", "Wallet Type Master > Modify/Delete Wallet");
    }

    public void selectProvider(String providerName) {
        WebElement radProvider = driver.findElement(By.xpath(".//*[text()='" + providerName + "']"));
        clickOnElement(radProvider, "Radio: " + providerName);
    }

    /**
     * Method to Select Wallet type using Text
     *
     * @param wallet
     * @throws Exception
     */
    public void selectDefaultWallet(String wallet) throws Exception {
        Thread.sleep(2000);
        selectVisibleText(defaultWallet, wallet, "Wallet Type");
    }

    public void selectPayInstrumentToUpdateMapping(List<Wallet> wallets) throws IOException {
        for (Wallet wallet : wallets) {
            try {
                WebElement elem = webtable.findElement(By.xpath(".//tr/td[contains(text(), '" + wallet.WalletName + "')]/parent::tr[1]/td/input[@type = 'checkbox']"));
                if (elem.isEnabled()) {
                    clickOnElement(elem, "Click on Payment Instrument for Modification: " + wallet.WalletName);
                } else {
                    pageInfo.info(wallet.WalletName + " is already selected");
                }
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                pageInfo.fail("Instrument Type :" + wallet.WalletName + " is not available - check UI");
                Utils.captureScreen(pageInfo);
            }
        }

    }

    /**
     * Method to Select Wallet type By Value
     *
     * @param walletID
     * @throws Exception
     */
    public void selectDefaultWalletByValue(String walletID) throws Exception {

        selectValue(defaultWallet, walletID, "Wallet Type");
    }

    /**
     * clickSubmit
     */
    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

    /**
     * clickCheckAll
     */
    public void clickCheckAll() {
        checkAll.click();
        pageInfo.info("Click on CheckAll ");
    }

    /**
     * selectProviderRadio
     *
     * @param providerName
     */
    public void selectProviderRadio(String providerName) {
        WebElement radio = DriverFactory.getDriver().findElement(By.xpath("//label[text()='" + providerName + "']"));
        radio.click();
        pageInfo.info("Selecting the radio having providerName: " + providerName);
    }

    /**
     * clickDeleteButton
     */
    public void clickDeleteButton() {
        deleteButton.click();
        pageInfo.info("Clicked on Delete Button..");
    }

    /**
     * clickConfirmDeleteButton
     */
    public void clickConfirmDeleteButton() {
        confirmDeleteButton.click();
        pageInfo.info("Clicked on Confirm Delete Button..");
    }

    public void unCheckAllServices() {
        checkAll.click();
        checkAll.click();
        pageInfo.info("Uncheck All Services");
    }

    public void clickOnmodify1() {
        clickOnElement(modifyButton1, "Modify Button 1");
    }

    public void clickOnModify2() {
        clickOnElement(modifyButton2, "Modify Button 2");
    }

    public void checkAllWalletType() throws IOException {
        List<WebElement> walletTypes = driver.findElements(By.xpath(".//*[@id='MfsModifyType_modify_check' and @type='checkbox']"));
        for (WebElement walletType : walletTypes) {
            if (!walletType.isSelected()) {
                walletType.click();
            }
        }
        pageInfo.info("Select Multiple Wallet Type");
        Utils.captureScreen(pageInfo);
    }

    public void clickOnCheckAll() {
        clickOnElement(checkAll, "Check All");
    }

    public List<WebElement> getAllWallets() {
        Select wallets = new Select(defaultWallet);
        return wallets.getOptions();
    }

    public void checkDefaultWalletTypeCheckBox(String walletID) {
        driver.findElement(By.xpath(".//input[contains(@value,'" + walletID + "')][@id='MfsModifyType_modify_check']")).click();
        pageInfo.info("Default Wallet type checked");
    }
}
