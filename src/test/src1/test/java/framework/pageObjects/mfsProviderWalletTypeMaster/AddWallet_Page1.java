package framework.pageObjects.mfsProviderWalletTypeMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by Dalia on 31-05-2017.
 */
public class AddWallet_Page1 extends PageInit {


    @FindBy(name = "action:MfsType_AddWallet")
    public WebElement Submit;
    @FindBy(id = "MfsType_AddWallet_button_submit")
    public WebElement submitBtn;
    //Page Objects
    @FindBy(name = "mfsProvider")
    private WebElement providerName;
    @FindBy(name = "defaultWalletType")
    private WebElement defaultWallet;

    public AddWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AddWallet_Page1 init(ExtentTest t1) throws Exception {
        return new AddWallet_Page1(t1);
    }

    /**
     * Navigate to MFS Provider Wallet Type Master > Add Wallet page
     *
     * @throws Exception
     */
    public void navigateToMFSProviderWalletTypeMaster() throws Exception {
        navigateTo("MFSWTM_ALL", "MFSWTM_MFSWTM01DM", "Wallet Type Master > Add Wallet Page");
    }

    //Select Provider
    public void selectProviderName(String providerName) throws Exception {
        selectVisibleText(this.providerName, providerName, "Provider Name");
    }

    //Select Default Wallet
    public void selectDefaultWallet(String wallet) throws Exception {
        selectValue(defaultWallet, wallet, "Default Wallet");
    }

    public void selectDefaultWalletByText(String wallet) throws Exception {
        selectVisibleText(defaultWallet, wallet, "Default Wallet");
    }

    public void checkWalletName(String walletName) {
        driver.findElement(By.xpath(".//*[text()='" + walletName + "']/../td/input")).click();
        pageInfo.info("Wallet name checked.");
    }

    public List<WebElement> getAllWallets() {
        Select wallets = new Select(defaultWallet);
        return wallets.getOptions();
    }

    public void clickOnSubmit() {
        clickOnElement(submitBtn, "Submit Button");
    }
}
