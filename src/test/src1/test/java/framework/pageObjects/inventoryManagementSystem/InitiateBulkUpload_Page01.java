package framework.pageObjects.inventoryManagementSystem;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/*
*       Nirupama MK
* */

public class InitiateBulkUpload_Page01 extends PageInit {
    /***************************************************************************
     *                      Page Objects
     ***************************************************************************/

    @FindBy(id = "IMS_MGMT_IMS_BULK_INIT")
    WebElement initiateBulkUpload;
    @FindBy(xpath = ".//*[@id='bulkDirDiv']//select")
    WebElement service;
    @FindBy(name = "uploadfile")
    WebElement uploadbtn;

   /* @FindBy(xpath = "//span[.='UPLOAD']")
    WebElement uploadbtn;*/
    @FindBy(id = "remarks")
    WebElement remark;
    @FindBy(xpath = ".//*[@id='bulkDirDiv']//div[2]/div/div/button")
    WebElement submitButton;
    @FindBy(xpath = "//*[@id='bulkDirDiv']/div/div/div/bulk-file-upload/section/form/div/div[1]/div[2]/span")
    WebElement message;
    @FindBy(xpath = "//*[@id='bulkDirDiv']/div/div/div/bulk-file-upload/section/form/div/div/div[2]")
    WebElement ErrorMsg;
    /***************************************************************************
     *                      Bulk Upload Dash Board page
     ***************************************************************************/

    @FindBy(xpath = "//span[.='Newest']")
    WebElement sortNewest;
    @FindBy(xpath = "//a[text()='Download status file']")
    WebElement downloadstatusFile;

    public InitiateBulkUpload_Page01(ExtentTest t1) {
        super(t1);
    }

    public static InitiateBulkUpload_Page01 init(ExtentTest t1) {
        return new InitiateBulkUpload_Page01(t1);
    }

    /***************************************************************************
     *                          Page Operations
     ***************************************************************************/

    public void navigateToInitiateBulkUploadLink() throws Exception {
        navigateTo("IMS_MGMT_ALL", "IMS_MGMT_IMS_BULK_INIT", "navigateToInitiateBulkUploadLink");
    }

    public void submitButton_Click() {
        clickOnElement(submitButton, "Submit");
    }

    public String getMessage() throws InterruptedException {
        String msg = message.getText();
        Thread.sleep(2000);
        return msg;
    }

    public String verifyErrorMessage() throws InterruptedException {
        Thread.sleep(2000);
        String msg = ErrorMsg.getText();
        Thread.sleep(3000);
        pageInfo.info("get error message");
        return msg;
    }

    public void serviceButton_Click() {
        clickOnElement(service, "Service");
    }

    public void service_SelectValue(String text) {
        selectValue(service, text, "Service Name");
    }

    public void remark_SetText(String text) {
        setTextUsingJs(remark, text, "Remark");
    }

    public boolean fileUpload(String filePath) throws Exception {
        uploadbtn.sendKeys(filePath);
        //clickOnElement(uploadbtn, "Upload");
//        setText(uploadbtn,filePath,"File Path");
        //Utils.uploadFile_02(filePath);
        Utils.captureScreen(pageInfo);
        Thread.sleep(1500);
        return false;
    }

    public void navigateToBulkUploadDashBoardLink() throws Exception {
        navigateTo("IMS_MGMT_ALL", "IMS_MGMT_IMS_BULK_DASHBOARD", "navigateToBulkUploadDashBoardLink");
    }

    public void sortNewest() {
        clickOnElement(sortNewest, "sortNewest");
    }

    public void selectServiceType(String service) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(0);
        selectValue(elem, service, "Service Type");
    }

    public void selectStatusFilter(Boolean isApproval) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(1);
        if (isApproval) {
            selectValue(elem, "SUCCEEDED", "Approval Status");
        } else {
            selectValue(elem, "FAILED", "Failed Status");
        }
    }

    public String downloadStatusFile() throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - bulk-upload");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(downloadstatusFile, "DownloadStatusFile");
            Utils.ieSaveDownloadUsingSikuli();
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            if (Utils.isFileDownloaded(oldFile, newFile)) {
                return FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
            }

        } catch (Exception e) {
            Assertion.markAsFailure("Failed to download the status file");
        }
        return null;
    }


}
