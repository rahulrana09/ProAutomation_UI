package framework.pageObjects;

import com.aventstack.extentreports.ExtentTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by amarnath.vb on 5/23/2017.
 */
public class homeScreen_pg extends PageInit {

    @FindBy(id = "PSEUDO_ALL")
    private WebElement PseudoUserLinkWHS;

    @FindBy(id = "PSEUDO_PSEUDO_CAT_MGMT")
    private WebElement PseudoCategoryManagementLink;

    @FindBy(id = "PSEUDO_PSEUDO_ADD")
    private WebElement AddPseudoUserLink;

    @FindBy(id = "PSEUDO_PSEUDO_MOD")
    private WebElement ModifyPseudoUserLink;

    @FindBy(id = "PSEUDO_PSEUDO_DEL")
    private WebElement DeletePseudoUserLink;

    @FindBy(id = "PSEUDO_PSEUDO_MODIFY_ROLES")
    private WebElement ModifyPseudoRolesLink;

    @FindBy(id = "PSEUDO_PSEUDO_MODIFY_TCP")
    private WebElement ModifyPseudoTCPLink;

    @FindBy(id = "PSEUDO_PSEUDO_GRP")
    private WebElement PseudoGroupRoleLink;

    /*
    * ---------------------------------------------- Modify Pseudo User ----------------------------------------------
    * */

    @FindBy(id = "modPseudo_modify_button_next")
    private WebElement modNext1;

    @FindBy(id = "modPseudo_modifyHierarchyConfirm_button_next")
    private WebElement modNext2;

    @FindBy(id = "modPseudo_modifyConfirm_button_confirm")
    private WebElement modConfirm;

    @FindBy(id = "categoryCodeList")
    private WebElement categoryDropdown;

    @FindBy(id = "parentId")
    private WebElement parentCategoryDropdown;

    @FindBy(id = "ownerId")
    private WebElement ownerCategoryDropdown;

    @FindBy(id = "msisdn")
    private WebElement msisdnTxtBox;

  /*
  * ------------------------------------------- methods of Modify pseudo User --------------------------------------
  * */
    /**
     * -------------------------------------- Channel Admin Pseudo User links ----------------------------------------
     */
    @FindBy(id = "PSEUDO2_ALL")
    private WebElement PseudoUserLink_CHADM;
    @FindBy(id = "PSEUDO2_PSEUDO_ADD2")
    private WebElement AddApprovePseudoUserLink_CHADM;
    @FindBy(id = "PSEUDO2_PSEUDO_MOD2")
    private WebElement ModifyApprovePseudoUserLink_CHADM;
    @FindBy(id = "PSEUDO2_PSEUDO_DELETE2")
    private WebElement DeleteApprovePseudoUserLink_CHADM;
    @FindBy(id = "PSEUDO2_PSEUDO_CAT_APPROVAL")
    private WebElement PseudoCategoryApprovalLink_CHADM;
    @FindBy(id = "_button_approve")
    private WebElement approveCategory;
    @FindBy(id = "addPseudo_addApproveConfirm_button_confirm")
    private WebElement approvePseudoUser;
    @FindBy(id = "PSEUDO_PSEUDO_MOD")
    private WebElement modPseudoUserlink;

//Pseudo User Modify Initiation Successfully for Msisdn : 7791457132


    public homeScreen_pg(ExtentTest t1) {
        super(t1);
    }

    public static void navigateToModPseudoUser() throws Exception {
        fl.leftNavigation("PSEUDO2_ALL", "PSEUDO_PSEUDO_MOD");
        pageInfo.info("Navigate to Modify Pseudo User Page");
    }

    public static homeScreen_pg init(ExtentTest t1) {
        return new homeScreen_pg(t1);
    }

    public static void navigateToApproveCategoryPage() throws Exception {
        fl.leftNavigation("PSEUDO2_ALL", "PSEUDO2_PSEUDO_CAT_APPROVAL");
        pageInfo.info("Navigate to Pseudo User Category Approval Page!");
    }

    public void setCategoryCodeList(String pseudoCategoryCode) {
        Select sel = new Select(categoryDropdown);
        fl.waitSelectOptions(sel);
        sel.selectByVisibleText(pseudoCategoryCode);
        pageInfo.info("Select Category Code:" + pseudoCategoryCode);
    }

    public void setPseudoParentId(String pseudoParentCategory) {
        Select sel = new Select(parentCategoryDropdown);
        fl.waitSelectOptions(sel);
        sel.selectByVisibleText(pseudoParentCategory);
        pageInfo.info("Select Parent Category Code:" + pseudoParentCategory);
    }

    public void setPseudoOwnerId(String ownerCategoryCode) {
        Select sel = new Select(ownerCategoryDropdown);
        fl.waitSelectOptions(sel);
        //sel.selectByVisibleText(ownerCategoryCode);
        sel.selectByIndex(1);
        pageInfo.info("Select Owner Category Code:" + sel.getFirstSelectedOption());
    }

    /*
    *
    * */

    public void setPseudoMSISDN(String msisdn) {
        msisdnTxtBox.sendKeys(msisdn);
        pageInfo.info("Set MSISDN: " + msisdn);
    }

    public void clickOnNextBtn1() throws Exception {
        modNext1.click();
        pageInfo.info("Clicked on Next Button");
    }

    public void clickOnNextBtn2() throws Exception {
        modNext2.click();
        pageInfo.info("Clicked on Next Button");
    }

    public void clickOnConfirm() throws Exception {
        modConfirm.click();
        pageInfo.info("Clicked on Confirm Button");
    }

    public void clickPseudoGroupRole() {
        PseudoGroupRoleLink.click();
    }

    public void clickAddPseudoUser() {
        AddPseudoUserLink.click();
    }

    /**
     * Navigate to Pseudo User Creation page
     *
     * @throws Exception
     **/


    public void navPseudoUserCreation() throws Exception {
        fl.leftNavigation("PSEUDO_ALL", "PSEUDO_ALL");
        pageInfo.info("Navigate to Pseudo User Creation Page!");
    }

    public void navAddPseudoUserPage() throws Exception {
        AddPseudoUserLink.click();
    }

    public void navigateToApprovePseudoUser() throws Exception {
        fl.leftNavigation("PSEUDO2_ALL", "PSEUDO2_PSEUDO_ADD2");
        pageInfo.info("Navigate to Pseudo User Add Approval Page!");

    }

    public void approveCategoryForPseudoUser(String categoryName) throws Exception {
        driver.findElement(By.xpath("//tbody/tr/td[contains(text(),'" + categoryName + "')]/following-sibling::td/a")).click();
        pageInfo.info("Select category Name" + categoryName);
        clickOnElement(approveCategory, "Approve");
    }

    public void approvePseudoUserAddition(String msisdn) throws Exception {
        driver.findElement(By.xpath("//tbody/tr/td[contains(text(),'" + msisdn + "')]/following-sibling::td/a")).click();
        pageInfo.info("Select category Name" + msisdn);
        clickOnElement(approvePseudoUser, "Approve");
    }


}