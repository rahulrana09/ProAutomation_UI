/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Prashant Kumar
 *  Date: 9/12/2017
*  Purpose: Page object of commissionDisbursement page 2
*/

package framework.pageObjects.commissionDisbursement;

import com.aventstack.extentreports.ExtentTest;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Commission_Disbursement_Page2 extends PageInit {

    @FindBy(id = "0")
    private WebElement Checkbox;
    @FindBy(id = "commissionDisbursementAction_input_button_approve")
    private WebElement Approve;
    @FindBy(id = "commissionDisbursementAction_input_button_back")
    private WebElement backButton;
    @FindBy(id = "DisbursementID0")
    private WebElement methodDDown;
    @FindBy(id = "WalletTypeId0")
    private WebElement walletTypeDDown;
    @FindBy(name = "action:commissionDisbursementAction_disburseSelected")
    private WebElement exportBtn;
    @FindBy(className = "wwFormTableC")
    private WebElement webTable;
    @FindBy(id = "DisbursementID0")
    private WebElement DisbursementMethod;
    @FindBy(id = "WalletTypeId0")
    private WebElement wallet;
    @FindBy(id = "LinkedName0")
    private WebElement bank;
    @FindBy(id = "LinkedAccountNumber0")
    private WebElement bankAccountNumber;

    public Commission_Disbursement_Page2(ExtentTest t1) {
        super(t1);
    }

    public Commission_Disbursement_Page2 verifyUsers() throws IOException {
        Checkbox.click();
        pageInfo.pass("User Found");
        pageInfo.addScreenCaptureFromPath(ScreenShot.TakeScreenshot());
        return this;
    }

    public Commission_Disbursement_Page2 verifyUsers(User u) throws IOException {
        String gui_MSISDN = driver.findElement(By.xpath("//tr/td[4]/label")).getText();
        String gui_UserName = driver.findElement(By.xpath("//tr/td[3]/label")).getText();

        Assertion.verifyEqual(gui_MSISDN, u.MSISDN, "Verify MSISDN", pageInfo);
        Assertion.verifyEqual(gui_UserName, u.FirstName, "Verify User Name", pageInfo);

        return this;
    }

    public Commission_Disbursement_Page2 selectCommission() {
        clickOnElement(Checkbox, "Commission");
        return this;
    }

    public void clickonApprove() {
        clickOnElement(exportBtn, "Approve Button");
    }

    public void backButtonClick() {
        clickOnElement(backButton, "Back Button");
    }

    public void selectMethodType(String methodType) {
        selectValue(methodDDown, methodType, "Payment Method");

    }

    public void selectWalletType(String walletType) {
        selectValue(walletTypeDDown, walletType, "Wallet Type");

    }

    public void selectBankName(String Bank) {
        selectValue(bank, Bank, "Bank Name");

    }

    public void selectBankAcc(String BankAccnum) {
        selectValue(bankAccountNumber, BankAccnum, "Bank Account Number");
    }


    public String getDisbursementMethod() {
        Select option = new Select(DisbursementMethod);
        String v = option.getFirstSelectedOption().getText();
        pageInfo.info("Check DisbursementMethod" + v);
        return v;
    }

    public String getWalletType() {
        Select option = new Select(wallet);
        String v = option.getFirstSelectedOption().getText();
        pageInfo.info("Check WalletType" + v);
        return v;
    }

    public Commission_Disbursement_Page2 ClickonExport() {
        clickOnElement(exportBtn, "Export Button");
        return this;
    }

    /**
     * download to CSV file upon clicking export button
     *
     * @return
     * @throws InterruptedException
     */
    public Commission_Disbursement_Page2 downloadCommissionDisbursement() throws InterruptedException {
        pageInfo.info("Deleting existing file with prefix - CommissionDisbursement");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_COMMISSION_DISBURSEMENT);
        Thread.sleep(5000L);
        ClickonExport();
        Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Click On Export button");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000L);
        Utils.ieSaveDownloadUsingSikuli();
        return this;
    }


    /**
     * Update amount in csv file for commission disbursement
     *
     * @param amt
     * @return
     * @throws Exception
     */
    public Commission_Disbursement_Page2 updateCsvWithNewAmount(String amt) throws Exception {
        File inputFile = new File(FilePath.fileCommissionDisbursement);
        // Read existing file
        CSVReader reader = new CSVReader(new FileReader(inputFile), ',');
        List<String[]> csvBody = reader.readAll();
        // get CSV row column and replace with by using row and column
        csvBody.get(1)[6] = amt; //Target replacement
        csvBody.get(1)[3] = DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletId;
        reader.close();
        // Write to CSV file which is open
        CSVWriter writer = new CSVWriter(new FileWriter(inputFile), ',', CSVWriter.NO_QUOTE_CHARACTER);
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
        return this;
    }

    public Commission_Disbursement_Page2 selectCommissionDisbursement(String msisdn) {

        driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='checkbox']")).click();
        pageInfo.info("Click on check box corresponding to user - " + msisdn);
        return this;
    }

    public String getCommissionamount(String msisdn) {

        String value = driver.findElement(By.xpath("//label[text()='" + msisdn + "']/../..//input[@type='text']")).getAttribute("value");
        pageInfo.info("Click on check box correcponding to user - " + msisdn);
        return value;
    }

    public List<WebElement> checkAmt() {

        List<WebElement> amount = driver.findElements(By.xpath("//table[@class='wwFormTableC']//tr[contains(@id,'rowID')]//td[5]//input[1]"));
        return amount;
    }

    public List<String> getAllUsers() throws Exception {

        List<String> usersList = new ArrayList<>();
        if (Assertion.isErrorInPage(pageInfo)) {
            if (Assertion.verifyErrorMessageContain("comm.error.disbursement.nouser", "No User with Commission Disbursemenet Available", pageInfo)) {
                return usersList; // empty list
            }
        } else {
            // get the user namse from table
            int rowCount = webTable.findElements(By.tagName("tr")).size();

            for (int i = 0; i < rowCount - 2; i++) {
                // fetch all users,
                usersList.add(driver.findElement(By.xpath("//label[@id='userName" + i + "']")).getText());
            }
        }
        return usersList;
    }
}