package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;


public class TransferRule_Pg1_test {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static WebDriverWait wait;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[1]/td[2]/span/img")
    WebElement ServiceType;

    // Page objects
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[2]/td[2]/span/img")
    WebElement SenderProvider;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[3]/td[2]/span/img")
    WebElement SenderDomain;

    /*
     * Page Objects
     */
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[4]/td[2]/span/img")
    WebElement SenderPayInst;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[5]/td[2]/span/img")
    WebElement SenderPayInstType;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[6]/td[2]/span/img")
    WebElement ReceiverProvider;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[7]/td[2]/span/img")
    WebElement ReceiverDomain;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[8]/td[2]/span/img")
    WebElement ReceiverInst;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[9]/td[2]/span/img")
    WebElement ReceiverInstType;
    @FindBy(id = "selectForm_button_submit")
    WebElement Submit;

    public static TransferRule_Pg1_test init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        wait = new WebDriverWait(driver, 10);
        TransferRule_Pg1_test page = PageFactory.initElements(driver, TransferRule_Pg1_test.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Navigate to Add TransferRule
     *
     * @throws Exception
     */
    public void navAddTransferRule() throws Exception {
        fl.leftNavigation("TRULES_ALL", "TRULES_T_RULES");
        Thread.sleep(6000);
        pageInfo.info("Navigate to Transfer Rule Pages");
    }

    /**
     * Refreshing Transfer Rule Page
     *
     * @throws Exception
     */
    public void refreshTransferRulePage() throws Exception {
        fl.leftNavigation("TRULES_ALL");
        Thread.sleep(3000);
        pageInfo.info("Refreshing Transfer Rule Pages");
    }

    /**
     * Submit The form
     */
    public void submit() {
        Submit.click();
        pageInfo.info("Submit the Transfer Rule Form");
    }

    /**
     * Select Service Type
     *
     * @param service
     * @throws Exception
     */
    public void selectServiceName(String service) throws Exception {
        clickImage(ServiceType);
        selectFromDojoCombo(service);
        Thread.sleep(2000);
        pageInfo.info("Select Service Type: " + service);
    }

    public void selectFromDojoCombo(WebElement elem, String value) throws InterruptedException {
        String searchString = value.substring(0, 1);
        elem.sendKeys(searchString);
        driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultname='" + value + "']")).click();
        Thread.sleep(2500);
    }

    /**
     * Select Provider
     *
     * @param provider
     * @return
     * @throws Exception
     */
/*
    public boolean selectSenderProvider(String provider) throws Exception {
        try {
            selectFromDojoCombo(SenderProvider, provider);
            pageInfo.info("Select Sender Provider: " + provider);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
*/
    public boolean selectSenderProvider(String provider) throws Exception {
        try {
            clickImage(SenderProvider);
            selectFromDojoCombo(provider, "2");
            Thread.sleep(2000);
            pageInfo.info("Select Sender Provider: " + provider);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * Select from Domain
     *
     * @param domain
     * @return
     * @throws Exception
     */
 /*   public boolean selectSenderDomain(String domain) throws Exception {
        try {
            selectFromDojoCombo(SenderDomain, domain);
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    public boolean selectSenderDomain(String domain) throws Exception {
        try {
            clickImage(SenderDomain);
            selectFromDojoCombo(domain, "3");
            Thread.sleep(2000);
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Sender Payment Instrument
     *
     * @param payInst
     * @throws Exception
     */
   /* public void selectSenderPayInstrument(String payInst) throws Exception {
        selectFromDojoCombo(SenderPayInst, payInst);
        pageInfo.info("Select Sender Payment Instrument: " + payInst);
    }
*/
    public void selectSenderPayInstrument(String payInst) throws Exception {
        clickImage(SenderPayInst);
        selectFromDojoCombo(payInst, "4");
        Thread.sleep(2000);
        pageInfo.info("Select Sender Payment Instrument: " + payInst);
    }

    /**
     * Select Snder Payment Instrument Type
     *
     * @param payInstType
     * @return
     * @throws Exception
     */
/*    public boolean selectSenderPayInstrumentType(String payInstType) throws Exception {
        try {
            selectFromDojoCombo(SenderPayInstType, payInstType);
            pageInfo.info("Select Sender Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    public boolean selectSenderPayInstrumentType(String payInstType) throws Exception {
        try {
            clickImage(SenderPayInstType);
            selectFromDojoCombo(payInstType, "5");
            Thread.sleep(2000);
            pageInfo.info("Select Sender Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Receiver Currency Provider
     *
     * @param provider
     * @throws Exception
     */
   /* public void selectReceiverProvider(String provider) throws Exception {
        selectFromDojoCombo(ReceiverProvider, provider);
        pageInfo.info("Select Receiver Provider: " + provider);
    }*/
    public void selectReceiverProvider(String provider) throws Exception {
        clickImage(ReceiverProvider);
        selectFromDojoCombo(provider, "6");
        Thread.sleep(2000);
        pageInfo.info("Select Receiver Provider: " + provider);
    }

    /**
     * Select Receiver Domain
     *
     * @param domain
     * @return
     * @throws Exception
     */
   /* public boolean selectReceiverDomain(String domain) throws Exception {
        try {
            selectFromDojoCombo(ReceiverDomain, domain);
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    public boolean selectReceiverDomain(String domain) throws Exception {
        try {
            clickImage(ReceiverDomain);
            selectFromDojoCombo(domain, "7");
            Thread.sleep(2000);
            pageInfo.info("Select Sender Domain: " + domain);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Receiver Payment Instrument
     *
     * @param payInst
     * @throws Exception
     */
   /* public void selectReceiverPayInstrument(String payInst) throws Exception {
        selectFromDojoCombo(ReceiverInst, payInst);
        pageInfo.info("Select Receiver Payment Instrument: " + payInst);
    }
*/
    public boolean selectReceiverPayInstrument(String payInst) throws Exception {
        try {

            clickImage(ReceiverInst);
            selectFromDojoCombo(payInst, "8");
            pageInfo.info("Select Receiver Payment Instrument: " + payInst);
            Thread.sleep(2000);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Receiver Payment Instrument Type
     *
     * @param payInstType
     * @return
     * @throws Exception
     */
   /* public boolean selectReceiverPayInstrumentType(String payInstType) throws Exception {
        try {
            selectFromDojoCombo(ReceiverInstType, payInstType);
            pageInfo.info("Select Receiver Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/
    public boolean selectReceiverPayInstrumentType(String payInstType) throws Exception {
        try {
            Thread.sleep(2000);
            clickImage(ReceiverInstType);
            selectFromDojoCombo(payInstType, "9");
            pageInfo.info("Select Receiver Payment Instrument Type: " + payInstType);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Select Div from the Combo Box
     *
     * @param option
     * @throws Exception
     * @deprecated
     */
    /*public void selectFromDojoCombo(String option, String index) throws Exception {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("/*//*[@id='page-home']/span[" + index + "]/div[@resultname='" + option + "']"))).click();
        Thread.sleep(1200);
    }*/
    public void selectFromDojoComboByValue(String option, String index) throws Exception {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//*[@id='page-home']/span[" + index + "]/div[@resultvalue='" + option + "']"))).click();
        Thread.sleep(1200);
    }


    /**
     * Select Div from the Combo Box
     *
     * @param option
     * @throws Exception
     */
    public void selectFromDojoCombo(String option, String index) throws Exception {
        WebElement result = driver.findElement(By.xpath("//*[@id='page-home']/span[" + index + "]/div[@resultname='" + option + "']"));
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", result);
    }

    public void selectFromDojoCombo(String option) throws Exception {
        WebElement result = driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultvalue='" + option + "']"));
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", result);
    }


    /**
     * Get all options of a combo box
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllServiceType() throws Exception {
        ServiceType.click();
        List<String> list = getAllOptions("1");
        Thread.sleep(300);
        return list;
    }

    /**
     * Get all options of a combo box
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllSenderProvider() throws Exception {
        SenderProvider.click();
        List<String> list = getAllOptions("2");
        Thread.sleep(300);
        return list;
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderDomain() {
        SenderDomain.click();
        return getAllOptions("3");
    }

    /**
     * Select First Sender Domain from the Dropdown Options
     *
     * @throws Exception
     */
    public void selectFirstSenderDomain() throws Exception {
        SenderDomain.click();
        List<String> list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
    }

    /**
     * Select First Receiver Domain from the Dropdown Options
     *
     * @throws Exception
     */
    public void selectFirstReceiverDomain() throws Exception {
        ReceiverDomain.click();
        List<String> list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
    }


    /**
     * Select First Payment Instrument Type available in UI for Sender
     *
     * @throws Exception
     */
    public void selectFirstSenderPayInstType() throws Exception {
        SenderPayInstType.click();
        List<String> list = getAllOptions("5");
        selectFromDojoCombo(list.get(0), "5");
    }

    /**
     * Select First Payment Instrument Type available in UI for Receiver
     *
     * @throws Exception
     */
    public void selectFirstreceiverPayInstType() throws Exception {
        ReceiverInstType.click();
        List<String> list = getAllOptions("9");
        selectFromDojoCombo(list.get(0), "9");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderPayInstrument() {
        SenderPayInst.click();
        return getAllOptions("4");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllSenderPayInstrumentType() {
        SenderPayInstType.click();
        return getAllOptions("5");
    }

    /**
     * Get all options of a combo box
     *
     * @return
     */
    public List<String> getAllReceiverProvider() {
        ReceiverProvider.click();
        return getAllOptions("6");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverDomain() {
        ReceiverDomain.click();
        return getAllOptions("7");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverPayInstrument() {
        ReceiverInst.click();
        return getAllOptions("8");
    }

    /**
     * Get All receiver Domain
     *
     * @return
     */
    public List<String> getAllReceiverPayInstrumentType() {
        ReceiverInstType.click();
        return getAllOptions("9");
    }

    /**
     * Get All Options
     *
     * @return
     */
    public List<String> getAllOptions(String index) {
        List<String> str = new ArrayList<String>();
        try {
            List<WebElement> subLink = driver.findElements(By.xpath("//*[@id='page-home']/span[" + index + "]/div"));

            for (WebElement sl : subLink) {
                str.add(sl.getAttribute("resultname"));
            }

            return str;
        } catch (NoSuchElementException e) {
            return null;
        }
    }


    public void selectfirstall() throws Exception {
        ServiceType.click();
        List<String> list = getAllOptions("1");
        selectFromDojoCombo(list.get(0), "1");
        SenderProvider.click();
        list = getAllOptions("2");
        selectFromDojoCombo(list.get(0), "2");
        SenderDomain.click();
        list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
        SenderPayInst.click();
        list = getAllOptions("4");
        selectFromDojoCombo(list.get(0), "4");
        SenderPayInstType.click();
        list = getAllOptions("5");
        selectFromDojoCombo(list.get(0), "5");
        ReceiverProvider.click();
        list = getAllOptions("6");
        selectFromDojoCombo(list.get(0), "6");
        ReceiverDomain.click();
        list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
        ReceiverInst.click();
        list = getAllOptions("8");
        selectFromDojoCombo(list.get(0), "8");
        ReceiverInstType.click();
        list = getAllOptions("9");
        selectFromDojoCombo(list.get(0), "9");
        submit();
    }


    public void selectfirstAll(String service, String payInst) throws Exception {
        List<String> list;
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[1]/td[2]/span/img")).click();
        //list = getAllOptions("1");
        selectFromDojoComboByValue(service, "1");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[2]/td[2]/span/img")).click();
        list = getAllOptions("2");
        selectFromDojoCombo(list.get(0), "2");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[3]/td[2]/span/img")).click();
        list = getAllOptions("3");
        selectFromDojoCombo(list.get(0), "3");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[4]/td[2]/span/img")).click();
        list = getAllOptions("4");
        selectFromDojoCombo(list.get(0), "4");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[5]/td[2]/span/img")).click();
        list = getAllOptions("5");
        selectFromDojoComboByValue(payInst, "5");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[6]/td[2]/span/img")).click();
        list = getAllOptions("6");
        selectFromDojoCombo(list.get(0), "6");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[7]/td[2]/span/img")).click();
        list = getAllOptions("7");
        selectFromDojoCombo(list.get(0), "7");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[8]/td[2]/span/img")).click();
        list = getAllOptions("8");
        selectFromDojoCombo(list.get(0), "8");
        driver.findElement(By.xpath("//*[@id='selectForm']/table/tbody/tr[9]/td[2]/span/img")).click();
        list = getAllOptions("9");
        selectFromDojoComboByValue(payInst, "9");
        submit();
    }

    /**
     * Click on image
     *
     * @param elem
     * @throws Exception
     */
    public void clickImage(WebElement elem) throws Exception {
        elem.click();
        Thread.sleep(500);
        elem.click();
        Thread.sleep(500);
        elem.click();
    }
}
