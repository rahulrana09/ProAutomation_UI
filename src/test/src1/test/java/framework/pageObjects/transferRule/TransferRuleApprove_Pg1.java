package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class TransferRuleApprove_Pg1 extends PageInit {


    @FindBy(className = "actionMessage")
    WebElement ActionMessage;
    // Page objects
    @FindBy(id = "trfRulesAppDetails_createTransferRule_button_submit")
    private WebElement Submit;

    public TransferRuleApprove_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static TransferRuleApprove_Pg1 init(ExtentTest t1) {
        return new TransferRuleApprove_Pg1(t1);
    }

    public void navApproveTransferRule() throws Exception {
        navigateTo("TRULES_ALL", "TRULES_T_RULAPP", "Transfer Rule Approval");
    }

    /**
     * Click on submit
     */
    public void clickSubmit() {
        clickOnElement(Submit, "Submit");
    }

    /**
     * Get Error message
     *
     * @throws Exception
     */
    public String getActionMessage() throws Exception {
        try {
            Thread.sleep(1000);
            return ActionMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

}
