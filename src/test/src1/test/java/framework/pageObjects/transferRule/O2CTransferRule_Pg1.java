package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;


public class O2CTransferRule_Pg1 extends PageInit {


    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    // Page objects
    @FindBy(id = "domainCode")
    private WebElement DomainName;
    @FindBy(id = "categoryCode")
    private WebElement CategoryName;
    @FindBy(id = "providerId")
    private WebElement CurrencyProvider;
    @FindBy(id = "paymentInstrumentId")
    private WebElement PaymentInstrument;
    @FindBy(id = "linkedWalletBankId")
    private WebElement PayInstrumentType;
    @FindBy(id = "firstLimit")
    private WebElement FirstLimit;
    @FindBy(id = "o2cTrf_confirm_button_submit")
    private WebElement Submit;
    @FindBy(id = "o2cTrf_save_button_confirm")
    private WebElement Confirm;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    public O2CTransferRule_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static O2CTransferRule_Pg1 init(ExtentTest t1) {
        return new O2CTransferRule_Pg1(t1);
    }

	/*public void assertO2CTruleCreation(){
        fl.waitWebElementVisible(ActionMessage);
		Assert.assertEquals(ActionMessage.getText(), en_US.TRANSFER_RULE_SUCCESSFULLY_CREATED, "Verify That O2C Transfer Rule is successfully created");
	}*/

    /**
     * Submit the Form
     *
     * @throws Exception
     */
    public void submit() throws Exception {
        clickOnElement(Submit, "Submit");
        Thread.sleep(1000);
    }

    /**
     * Confirm the Form
     */
    public void confirm() {
        clickOnElement(Confirm, "Confirm");
    }

    /**
     * Get Action Message
     *
     * @return
     */
    public String getActionMessage() {
        String msg = null;
        if (fl.elementIsDisplayed(ActionMessage)) {
            msg = ActionMessage.getText();
        } else if (fl.elementIsDisplayed(ErrorMessage)) {
            msg = ErrorMessage.getText();
        }
        return msg;
    }

    /**
     * Select Domain Name
     *
     * @param domainName
     * @throws Exception
     */
    public void selectDomainName(String domainName) throws Exception {
        selectVisibleText(DomainName, domainName, "DomainName");
    }

    /**
     * Get all domain names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllDomainName() throws Exception {
        Select sel = new Select(DomainName);
        List<WebElement> we = sel.getOptions();
        List<String> ls = new ArrayList<String>();
        for (WebElement a : we) {
            if (!a.getText().equals("Select")) {
                ls.add(a.getText());
            }
        }
        return ls;
    }

    /**
     * Select Category Name
     *
     * @param categoryName
     */
    public void selectCategoryName(String categoryName) {
        selectVisibleText(CategoryName, categoryName, "CategoryName");
    }

    public void selectCategoryCode(String code) {
        try {
            Thread.sleep(1200);
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectValue(CategoryName, code, "CategoryCode");
    }

    /**
     * Get all Category names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllCategoryName() throws Exception {
        return fl.getOptionValues(CategoryName);
    }

    /**
     * Select Payment Instrument
     *
     * @param payInstrument
     * @throws Exception
     */
    public void selectPayInstrument(String payInstrument) throws Exception {
        selectVisibleText(PaymentInstrument, payInstrument, "PaymentInstrument");
    }

    /**
     * Get all Pay Instruments names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllPayInstrument() throws Exception {
        Select sel = new Select(PaymentInstrument);
        List<WebElement> we = sel.getOptions();
        List<String> ls = new ArrayList<String>();
        for (WebElement a : we) {
            if (!a.getText().equals("Select")) {
                ls.add(a.getText());
            }
        }
        return ls;
    }

    /**
     * Get First approval limit
     *
     * @return
     * @throws Exception
     */
    public String getFirstApproveLimit() throws Exception {
        Thread.sleep(ConfigInput.additionalWaitTime);
        return FirstLimit.getAttribute("value");
    }

    /**
     * Set Approval limit
     *
     * @param limit
     */
    public void setFirstApprovalLimit(String limit) {
        setText(FirstLimit, limit, "FirstLimit");
    }

    public void resetFirstApprovalLimit() {
        FirstLimit.clear();
    }

    /**
     * Select Payment Instrument Type
     */
    public void selectPayInstrumentType(String walletName) {
        String walletId = DataFactory.getWalletId(walletName.trim());
        selectValue(PayInstrumentType, walletId, walletName);
    }

    /**
     * Get all Pay Instruments type from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllPayInstrumentsType() throws Exception {
        Thread.sleep(2500);
        return fl.getOptionText(PayInstrumentType);
    }

    /**
     * Select Currency Provider
     *
     * @param provider
     * @throws Exception
     */
    public void selectCurrencyProvider(String provider) throws Exception {
        selectVisibleText(CurrencyProvider, provider, "CurrencyProvider");
    }

    public void selectProviderCode(String providerCode) throws Exception {
        selectValue(CurrencyProvider, providerCode, "CurrencyProvider Code");
    }

    /**
     * Get all Category names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllCurrencyProvider() throws Exception {
        return fl.getOptionValues(CurrencyProvider);
    }

    /**
     * Navigate to Add TransferRule
     *
     * @throws Exception
     */
    public void navAddTransferRule() throws Exception {
        navigateTo("TRULES_ALL", "TRULES_O2C_TR", "O2C Transfer Rule Page");
    }

    /**
     * Select the specific Payment Instrument
     *
     * @throws InterruptedException
     */
    public void selectPaymentInstrument() throws InterruptedException {
        Select sel = new Select(PaymentInstrument);
        PaymentInstrument.click();
        Thread.sleep(2000);

        if (sel.getOptions().size() > 1) {
            selectIndex(PaymentInstrument, 1, "PaymentInstrument");
        } else {
            selectIndex(PaymentInstrument, 0, "PaymentInstrument");
        }
    }

}
