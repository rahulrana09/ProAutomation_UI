package framework.pageObjects.transferRule;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class TransferRule_Pg2 extends PageInit {

    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;
    /**
     * Get Error message
     *
     * @throws Exception
     */
    @FindBy(id = "trRule_confirmCoU_statusId")
    WebElement status;
    //v5.0 id is different
    @FindBy(id = "payerCategoryCode")
    private WebElement FCategory;
    @FindBy(id = "payeeCategoryCode")
    private WebElement TCategory;
    // Page objects
    @FindBy(name = "payerCategoryCode")
    private WebElement FromCategory;
    @FindBy(name = "payeeCategoryCode")
    private WebElement ToCategory;
    @FindBy(id = "payerGradeCode")
    private WebElement FromGrade;
    @FindBy(id = "payeeGradeCode")
    private WebElement toGrade;
    @FindBy(id = "trList_transferRuleAddChecks_trfrules_label_addtrfrule")
    private WebElement AddTransferRule;
    @FindBys({@FindBy(xpath = "//form[@id='trList_transferRuleAddChecks']/table/tbody/tr/td[2])[position()>10 and position()<last()-1]")})
    private List<WebElement> transferRuleIDs;


    public TransferRule_Pg2(ExtentTest t1) {
        super(t1);
    }

    public static TransferRule_Pg2 init(ExtentTest t1) {
        return new TransferRule_Pg2(t1);
    }

    public void ClickonEdit() {
        driver.findElement(By.xpath("//*[@id='trList_transferRuleAddChecks']/table/tbody/tr[12]/td[9]/a")).click();
        pageInfo.info("Click on Edit Button");
    }

    public void ClickEdit(String ruleID) {
        driver.findElement(By.xpath("//td[text()='" + ruleID + "']/..//td/a[text()='Edit']")).click();
        pageInfo.info("Click on Edit Button");
    }

    public void ClickonView() {
        driver.findElement(By.xpath("//*[@id='trList_transferRuleAddChecks']/table/tbody/tr[12]/td[8]/a")).click();
        pageInfo.info("Click on View Button");
    }

    public void ClickView(String ruleID) {
        driver.findElement(By.xpath("//td[text()='" + ruleID + "']/..//td/a[text()='View']")).click();
        pageInfo.info("Click on View Button");
    }

    public void ClickonDelete() {
        driver.findElement(By.xpath("//*[@id='trList_transferRuleAddChecks']/table/tbody/tr[12]/td[10]/a")).click();
        pageInfo.info("Click on Edit Button");
    }

    public String getErrorMessage() throws Exception {
        try {
            Thread.sleep(1200);
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }

    /**
     * Get Error message
     *
     * @throws Exception
     */

    public void selectStatus(String text) throws Exception {
        selectVisibleText(status, text, "Status");
    }

    /**
     * Add Transfer rule
     */
    public void submit() {
        clickOnElement(AddTransferRule, "Add Transfer Rule");
    }

    /**
     * Select From Category
     *
     * @param category
     * @throws Exception
     */
    public void selectFromCategory(String category) throws Exception {
        if (!ConfigInput.isCoreRelease) {
            selectVisibleText(FromCategory, category, "From Category");
        } else {
            selectVisibleText(FCategory, category, "From Category");
        }
    }

    public void selectFromCategoryByValue(String category) throws Exception {
        if (!ConfigInput.isCoreRelease) {
            selectValue(FromCategory, category, "From Category");
        } else {
            selectValue(FCategory, category, "From Category");
        }
    }

    /**
     * Get all From Category names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllFromCategory() throws Exception {
        return fl.getOptionText(FromCategory);
    }

    /**
     * Select To Category
     *
     * @param category
     * @throws Exception
     */
    public void selectToCategory(String category) throws Exception {
        if (!ConfigInput.isCoreRelease) {
            selectVisibleText(ToCategory, category, "To Category");
        } else {
            selectVisibleText(TCategory, category, "To Category");
        }
    }

    /**
     * Select To Category for Bill Payment
     *
     * @param category
     * @throws Exception
     */
    public void selectToCategoryForBillPayment(String category) throws Exception {
        if (!ConfigInput.isCoreRelease) {
            selectValue(ToCategory, category, "To Category");
        } else {
            selectValue(TCategory, category, "To Category");
        }
    }


    /**
     * Get all To Category names from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllToCategory() throws Exception {
        return fl.getOptionText(ToCategory);
    }

    public TransferRule_Pg3 clickOnEdit(String ruleId) {
        WebElement elem = driver.findElement(By.xpath("//tr/td[contains(text(), '" + ruleId + "')]/parent::tr/td/a[text() = 'Edit']"));
        clickOnElement(elem, "Edit: " + ruleId);
        return new TransferRule_Pg3(pageInfo);
    }


    public String fetchRuleID() {
        WebElement ruleId = driver.findElement(By.xpath("//*[@id='trList_transferRuleAddChecks']/table/tbody/tr[12]/td[2]"));
        return ruleId.getText();
    }

    public void clickDelete(String id) {
        driver.findElement(By.xpath("//td[text()='" + id + "']/..//td[10]/a")).click();
        pageInfo.info("Deleted Transfer Rule: " + id);
    }

    public List<WebElement> fetchRuleIDs() {
        return transferRuleIDs;
    }

    public void selectFromGrade(String grade) throws Exception {
        Select sel = new Select(FromGrade);
        sel.selectByValue(grade);
        pageInfo.info("Selected From Grade: " + grade);
    }

    public void selectToGrade(String grade) throws Exception {
        Select sel = new Select(toGrade);
        sel.selectByValue(grade);
        pageInfo.info("Selected To Grade: " + grade);
    }

}
