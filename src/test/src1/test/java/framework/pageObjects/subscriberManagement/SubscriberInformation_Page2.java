package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by navin.pramanik on 31-08-2017.
 */
public class SubscriberInformation_Page2 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static String MainWindow;
    @FindBy(id = "userInfo_viewSubscriberInfo_userMap_msisdn")
    WebElement msisdnLabel;


    // Page objects
    @FindBy(id = "userInfo_viewSubscriberInfo_userMap_userName1")
    WebElement userNameLabel;
    @FindBy(xpath = "//a[contains(@href,'viewSubscriberInfo')]")
    WebElement subsInfoLionk;
    @FindBy(xpath = "//input[contains(@onclick,'window.close')]")
    WebElement closeButton;
    @FindBy(xpath = "//a[contains(@href,'viewSelfInfo')]")
    WebElement V5_subsSelfInfoLink;

    public static SubscriberInformation_Page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        SubscriberInformation_Page2 page = PageFactory.initElements(driver, SubscriberInformation_Page2.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void clickSubsInfoLink() {
        subsInfoLionk.click();
        pageInfo.info("Clicked on Subscriber Info Link!");
    }

    public String getMSISDNLabelText() {
        return msisdnLabel.getText();
    }

    public String getUsernameText() {
        return userNameLabel.getText();
    }

    public void switchToWindow() {

        MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
            }
        }
    }

    public void switchToMainWindow() throws Exception {
        driver.switchTo().window(MainWindow);
        fl.contentFrame();
    }

    public void clickCloseButton() {
        closeButton.click();
        pageInfo.info("Clicked on Close Button");
    }


}
