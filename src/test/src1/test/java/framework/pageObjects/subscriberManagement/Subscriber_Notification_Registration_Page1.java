package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by shubham.kumar1 on 7/27/2017.
 */
public class Subscriber_Notification_Registration_Page1 extends PageInit {

    @FindBy(xpath = "//*[@id='SelectForm_accountNo']")
    private static WebElement AccountNumber;
    @FindBy(xpath = "//*[@id='SelectForm_mobileNo']")
    private static WebElement MSISDN;
    @FindBy(xpath = "//*[@id='SelectForm_submit']")
    private static WebElement submitButton;
    @FindBy(xpath = "//*[@id='companyCode']")
    private static WebElement companyDropDown;
    @FindBy(id = "sel")
    private WebElement provider;

    public Subscriber_Notification_Registration_Page1(ExtentTest t1) {
        super(t1);
    }

    public static Subscriber_Notification_Registration_Page1 init(ExtentTest t1) {
        return new Subscriber_Notification_Registration_Page1(t1);
    }

    /**
     * Navigate to Operator User Creation
     *
     * @throws Exception
     */
    public Subscriber_Notification_Registration_Page1 navSubscriberManagementNotificationRegistration() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_NOTIFREG", "Notification Registration");
        return this;
    }

    public void setAccountNumber(String accNo) {
        setText(AccountNumber, accNo, "AccountNumber");
    }

    public void setMSISDN(String msisdn) {
        setText(MSISDN, msisdn, "MSISDN");
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    public void selectCompanyName() throws Exception {
        selectIndex(companyDropDown, 1, "CompanyName");
    }

    public void setMFSProvider(String value) {
        selectValue(provider, value, "MFS provider");
    }

    public void setCompanyName(String billerCode) {
        selectValue(companyDropDown, billerCode, "Bill Company");
    }

    public void subscriberBillNotificationRegistration(String provider, String billerCode, String accountNum, String msisdn) throws Exception {
        try {
            navSubscriberManagementNotificationRegistration();
            setMFSProvider(provider);
            setCompanyName(billerCode);
            setAccountNumber(accountNum);
            setMSISDN(msisdn);
            clickSubmit();
        } catch (Exception e) {
            e.printStackTrace();
            Utils.captureScreen(pageInfo);
        }

    }
}
