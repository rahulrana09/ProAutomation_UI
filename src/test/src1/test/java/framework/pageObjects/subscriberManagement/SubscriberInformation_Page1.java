/*
*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
*  This software is the sole property of Comviva
*  and is protected by copyright law and international
*  treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of
*  it may result in severe civil and criminal penalties
*  and will be prosecuted to the maximum extent possible
*  under the law. Comviva reserves all rights not
*  expressly granted. You may not reverse engineer, decompile,
*  or disassemble the software, except and only to the
*  extent that such activity is expressly permitted
*  by applicable law notwithstanding this limitation.
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
*  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
*  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
*  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
*  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
*  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*  Author Name: Automation Team
*  Date: 31-08-2017.
*  Purpose: Selenium test cases
*/
package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by navin.pramanik on
 */
public class SubscriberInformation_Page1 extends PageInit {

    // Page object providerDdown
    @FindBy(id = "partyProviderListSel")
    private WebElement providerDdown;

    @FindBy(id = "partyPaymentTypeList")
    private WebElement paymentInstumentDdown;

    @FindBy(id = "walletListId")
    private WebElement walletTypeDdown;
    @FindBy(id = "accessId")
    private WebElement msisdnTbox;
    @FindBy(xpath = "//input[contains(@id,'button_submit')]")
    private WebElement submitButton;

    public SubscriberInformation_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Subscriber Information Page
     *
     * @throws Exception
     */
    public SubscriberInformation_Page1 navigateToSubsInfo() throws Exception {
        fl.leftNavigation("SUBSINFO_ALL", "SUBSINFO_SUBS_INF");
        pageInfo.info("Navigate to  >  Subscriber Information Page");
        return this;
    }


    public SubscriberInformation_Page1 setMSISDN(String msisdn) {
        msisdnTbox.sendKeys(msisdn);
        pageInfo.info("set MSISDN : " + msisdn);
        return this;
    }

    public SubscriberInformation_Page1 selectProvider(String providerID) {
        Select sel = new Select(providerDdown);
        sel.selectByValue(providerID);
        pageInfo.info("Select Provider ID:" + providerID);
        return this;
    }

    public SubscriberInformation_Page1 selectPaymentInstrument(String payInst) {
        Select sel = new Select(paymentInstumentDdown);
        sel.selectByVisibleText(payInst);
        pageInfo.info("Select Payment Instrument:" + payInst);
        return this;
    }

    public SubscriberInformation_Page1 selectWalletType(String walletType) {
        Select sel = new Select(walletTypeDdown);
        sel.selectByVisibleText(walletType);
        pageInfo.info("Select Wallet Type:" + walletType);
        return this;
    }

    public SubscriberInformation_Page1 clickSubmit() {
        submitButton.click();
        pageInfo.info("Clicked on Submit Button !");
        return this; // change this if constructor of another page has to be returnedd
    }
}
