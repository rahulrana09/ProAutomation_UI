package framework.pageObjects.subscriberManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class ModifySubscriber_page4 extends PageInit {

    @FindBy(name = "action:subsRegistrationUpdateServiceBean_confirm")
    WebElement confirmButton;

    @FindBy(id = "subsRegistrationUpdateServiceBean_updateWallet_button_next")
    WebElement V5_confirmButton;

    @FindBy(name = "action:subsRegistrationUpdateServiceBean_updateBank")
    WebElement V5_confirmAgain;

    @FindBy(id = "subsRegistrationUpdateServiceBean_save_button_confirm")
    WebElement finalConfirmButton;

    @FindBy(xpath = "//input[@id='subsRegistrationUpdateServiceBean_save_button_confirm' and @value = 'Confirm']")
    WebElement V5_finalConfirmButton;

    @FindBy(xpath = "//input[@id='subsRegistrationUpdateServiceBean_view_submit' and @value = 'Add More']")
    WebElement addMoreButton;

    @FindBy(id = "subsRegistrationUpdateServiceBean_save_button_confirm")
    WebElement V5_nextFinalConfirmButton;

    @FindBys({@FindBy(xpath = "(//form[@id='subsRegistrationUpdateServiceBean_back2']/table[2]/tbody/tr/td[2])[position()>1]")})
    List<WebElement> selectedWallettype;

    @FindBy(id = "walletTypeID0")
    WebElement linkedBank;

    @FindBy(id = "subsRegistrationUpdateServiceBean_view_bankCounterList_0__customerId")
    WebElement customerId;

    @FindBy(id = "subsRegistrationUpdateServiceBean_view_bankCounterList_0__accountNumber")
    WebElement accountNo;

    @FindBy(id = "delete2")
    WebElement delete;

    @FindBy(xpath = "//*[contains(text(),'SAVINGCLUB')]/following::td[7]/select")
    WebElement Status;

    @FindBy(id = "subsRegistrationUpdateServiceBean_view_bankCounterList_0__statusSelected")
    WebElement Status1;

    @FindBy(id = "subsRegistrationUpdateServiceBean_view_bankCounterList_1__statusSelected")
    WebElement Status2;

    @FindBys({@FindBy(xpath = "(//form[@id='subsRegistrationUpdateServiceBean_view']/table[1]/tbody/tr/td[7])[position()>1]")})
    private List<WebElement> selectedAccountType;


    public ModifySubscriber_page4(ExtentTest t1) {
        super(t1);
    }

    public static ModifySubscriber_page4 init(ExtentTest t1) {
        return new ModifySubscriber_page4(t1);
    }

    public void clickOnDelete() {
        delete.click();
    }

    public void confirmButton_Click() {
        if (ConfigInput.isCoreRelease) {
            V5_confirmButton.click();
        } else {
            confirmButton.click();
        }
    }

    public ModifySubscriber_page4 clickOnConfirmPg4() {
        if (fl.elementIsDisplayed(V5_confirmButton)) {
            clickOnElement(V5_confirmButton, "V5_confirmButton");
        } else {
            clickOnElement(confirmButton, "confirmButton");
        }
        return this;
    }

    public void clickFinalConfirm() {
        Utils.putThreadSleep(Constants.TWO_SECONDS);

        if (fl.elementIsDisplayed(V5_finalConfirmButton)) {
            V5_finalConfirmButton.sendKeys(Keys.ENTER);
            pageInfo.info("Clicked on Final Page Confirm Button");
        } else {
            clickOnElement(finalConfirmButton, "Final Page Confirm Button");
        }
    }

    public void finalConfirm1() {
        finalConfirmButton.click();
        pageInfo.info("Click on confirm button");
    }

    public void confirm() {
        confirmButton.click();
        pageInfo.info("Click on confirm button");
    }

    public void confirmButton() {
        confirmButton.click();
    }

    public void addMoreBank() {
        addMoreButton.click();
    }

    public ModifySubscriber_page4 clickSubmitPg4() {
        //V5_confirmAgain.click();
        Utils.putThreadSleep(2000);
        clickOnElement(V5_confirmAgain, "Next Button");
        //pageInfo.info("Clicked on Confirm next Button");
        return this;
    }

    public String getCustomerId(int num) {
        String cusId = driver.findElement(By.id("subsRegistrationUpdateServiceBean_view_bankCounterList_" + num + "__customerId")).getAttribute("value");
        return cusId;
    }

    public String getLinkedBank(int num) {
        Select sel = new Select(driver.findElement(By.id("walletTypeID" + num)));
        String bankLinked = sel.getFirstSelectedOption().getText();
        return bankLinked;
    }

    public String getAccNo(int num) {
        String accNo = driver.findElement(By.id("subsRegistrationUpdateServiceBean_view_bankCounterList_" + num + "__accountNumber")).getAttribute("value");
        return accNo;
    }

    public String getPrimaryAccount(int num) {
        WebElement primaryAcc = driver.findElement(By.id("subsRegistrationUpdateServiceBean_view_bankCounterList_" + num + "__primaryAccountSelected"));
        Select sel = new Select(primaryAcc);
        String value = sel.getFirstSelectedOption().getText();
        return value;
    }

    public void nextFinalConfirm_click() {
        V5_nextFinalConfirmButton.sendKeys(Keys.ENTER);
        pageInfo.info("Clicked on nextFinalConfirm Button");
    }

    public void updateWalletStatus(String status, String walletName) {
        for (int i = 0; i < selectedWallettype.size(); i++) {
            Select sel = new Select(driver.findElement(By.name("counterList[" + i + "].paymentTypeSelected")));
            if (sel.getFirstSelectedOption().getText().equalsIgnoreCase(walletName)) {
                Select stat = new Select(driver.findElement(By.name("counterList[" + i + "].statusSelected")));
                stat.selectByVisibleText(status);
                pageInfo.info("selected status as " + status);
            }
        }
    }

    public void selectWalletStatus(String walletName,String statusValue) {
        ////select/option[@value='18']/following::select[contains(@name,'statusSelected')]
        WebElement el = driver.findElement(By.xpath("//*[contains(text(),'"+walletName+"')]/following::select[contains(@name,'statusSelected')]"));
        selectValue(el, statusValue, "Wallet Status");
    }

    public void selectStatus() {
        selectVisibleText(Status, "Deleted", "Status");
    }

    public void selectLinkedBank1Status() {
        selectVisibleText(Status1, "Deleted", "Status");
    }

    public void selectLinkedBank2Status() {
        selectVisibleText(Status2, "Deleted", "Status");
    }

    public void modifyStatusOfBank(String status, String accountNumber) throws Exception {

        for (int i = 0; i < selectedAccountType.size(); i++) {
            String accNo = driver.findElement(By.name("bankCounterList[" + i + "].accountNumber")).getAttribute("value");
            if (accNo.equalsIgnoreCase(accountNumber)) {
                Select stat = new Select(driver.findElement(By.name("bankCounterList[" + i + "].statusSelected")));
                stat.selectByVisibleText(status);
                pageInfo.info("selected status as " + status);
            }
        }
    }

}
