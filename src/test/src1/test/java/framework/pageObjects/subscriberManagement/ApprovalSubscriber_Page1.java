package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by shruti.gupta on 11-07-2017.
 */
public class ApprovalSubscriber_Page1 extends PageInit {

    // Page objects Approve subscriber details
    @FindBy(id = "action_userName")
    WebElement firstName;
    @FindBy(id = "action_lastName")
    WebElement lastName;
    @FindBy(id = "action_msisdn")
    WebElement mobNum;
    @FindBy(id = "action_externalCode")
    WebElement idNum;
    @FindBy(id = "action_button_close")
    WebElement buttonClose;

    public void clickOnCloseButton(){
        clickOnElement(buttonClose, "Button Close");
    }

    @FindBy(name = "approve")
    WebElement buttonApprove;
    @FindBy(id = "subsRegistrationAddChecker_addSubsApproveOrRejectConfirm_rejectReason")
    WebElement reason;
    @FindBy(id = "subsRegistrationAddChecker_addSubsApproveOrReject_action_submit")
    WebElement btnConfirmApproval;

    @FindBy(xpath = "//*[@id='subsRegistrationAddChecker_addSubsApproveOrRejectConfirm']/table/tbody/tr[2]/td[6]/a")
    WebElement ViewApprovalLink;

    //Page Objects Add subscriber Confirm
    @FindBy(id = "subsRegistrationServiceBean_confirm_userName")
    WebElement confirmFirstName;
    @FindBy(id = "subsRegistrationServiceBean_confirm_lastName")
    WebElement confirmLastName;
    @FindBy(id = "subsRegistrationServiceBean_confirm_msisdn")
    WebElement confirmMobNum;
    @FindBy(id = "subsRegistrationServiceBean_confirm_externalCode")
    WebElement confirmIdNum;
    @FindBy(id = "subsRegistrationServiceBean_confirm_button_confirm")
    WebElement confirmBtnClose;

    public void clickOnConfirmClose() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(2000);
        clickOnElement(confirmBtnClose, "Confirm Close");
    }


    public ApprovalSubscriber_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ApprovalSubscriber_Page1 init(ExtentTest t1) {
        return new ApprovalSubscriber_Page1(t1);
    }

    public String getFirstName(boolean isApprove) {
        if (isApprove)
            return firstName.getText();
        else
            return confirmFirstName.getText();
    }

    public String getLastName(boolean isApprove) {
        if (isApprove)
            return lastName.getText();
        else
            return confirmLastName.getText();
    }

    public String getMobileNumber(boolean isApprove) {
        if (isApprove)
            return mobNum.getText();
        else
            return confirmMobNum.getText();
    }

    public String getIdentificationNumber(boolean isApprove) {
        if (isApprove)
            return idNum.getText();
        else
            return confirmIdNum.getText();
    }
    public void clickApprove() {
        clickOnElement(buttonApprove, "buttonApprove");
    }

    public void confirmApproval() {
        clickOnElement(btnConfirmApproval, "btnConfirmApproval");
    }

    public void setReason(String text) {
        setText(reason, text, "reason");
    }

    public ApprovalSubscriber_Page1 navSubscriberApproval() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSADDAP", "Subscriber Approval");
        return this;
    }

    public void ViewApprovalLink() {
        clickOnElement(ViewApprovalLink, "ViewApprovalLink");
    }

    public ApprovalSubscriber_Page1 selectUsertoApprove(String msisdn) {
        WebElement userCheckBox = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
        clickOnElement(userCheckBox, "User with MSISDN:" + msisdn);
        return this;
    }

    public ApprovalSubscriber_Page1 viewUserApprovalDetails(String msisdn) {
        WebElement userCheckBox = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
        clickOnElement(userCheckBox, "User with MSISDN:" + msisdn);
        WebElement viewLink = driver.findElement(By.xpath("//td[contains(text(),'" + msisdn + "')]/..//a[contains(text(), 'View All Data')]"));
        clickOnElement(viewLink, "View All Details:" + msisdn);
        return this;
    }

    public List<WebElement> getAllLabels(String pageName) throws Exception {
        pageInfo.info("Getting all the Label on the: " + pageName);//[contains(text(),'Name prefix')]
        List<WebElement> labels = null;
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();
        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {

                // Switching to Child window
                driver.switchTo().window(ChildWindow);
                labels = DriverFactory.getDriver().findElements(By.xpath("//label[@class='label']"));
            }
        }
        // Switching to Parent window i.e Main Window.
        driver.switchTo().window(MainWindow);
        fl.contentFrame();
        return labels;
    }

    public ApprovalSubscriber_Page1 selectUserstoApprove(List<String> msisdnList) {
        for (String msisdn : msisdnList) {
            WebElement userCheckBox = driver.findElement(By.xpath("//tr/td[contains(text(),'" + msisdn + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
            clickOnElement(userCheckBox, "Clicked checkbox of User with MSISDN:" + msisdn);
        }

        return this;
    }


}
