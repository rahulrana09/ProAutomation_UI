package framework.pageObjects.subscriberManagement;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifySubscriber_page3 extends PageInit {

    @FindBy(name = "action:subsRegistrationUpdateServiceBean_updateWalletSub")
    WebElement nextpage;

    @FindBy(id = "_button_nextPage")
    WebElement V5_next_Page;

    @FindBy(css = "input[id='subsRegistrationUpdateServiceBean_back2_submit'][value='Back']")
    WebElement backButton;

    @FindBy(id = "_button_back")
    WebElement V5_back_Button;
    @FindBy(id = "subsRegistrationUpdateServiceBean_updateWallet_button_next")
    WebElement nextPage;
    @FindBy(id = "subsRegistrationUpdateServiceBean_updateWallet_button_back")
    WebElement backbtn;

    public ModifySubscriber_page3(ExtentTest t1) {
        super(t1);
    }

    public static ModifySubscriber_page3 init(ExtentTest t1) {
        return new ModifySubscriber_page3(t1);
    }

    public boolean isNextButtonShown() {
        return fl.elementIsDisplayed(nextpage);
    }

    public ModifySubscriber_page2 backButton_Click() {
        if (fl.elementIsDisplayed(V5_back_Button)) {
            clickOnElement(V5_back_Button, "'V5_back_Button'");
        } else {
            clickOnElement(backButton, "backButton");
        }
        return new ModifySubscriber_page2(pageInfo);
    }

    public ModifySubscriber_page4 clickOnNextPg3() {
        Utils.putThreadSleep(2000);
        if (fl.elementIsDisplayed(V5_next_Page)) {
            clickOnElement(V5_next_Page, "Page3 Next Button");
        } else {
            clickOnElement(nextpage, "Page3 Next Button");
        }
        return new ModifySubscriber_page4(pageInfo);
    }

    public ModifySubscriber_page4 nextPage() {
        clickOnElement(nextpage, "nextpage");
        return new ModifySubscriber_page4(pageInfo);
    }

    public void selectWebGroupRole(String roleName) {
        WebElement elem = driver.findElement(By.cssSelector("input[value='" + roleName + "']"));
        clickOnElement(elem, "Web Group Role: " + roleName);
    }

}
