package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class SubsBankAssociation_Page1 extends PageInit {

    /**
     * Page Objects
     */
    @FindBy(name = "msisdn")
    WebElement txtMsisdn;
    @FindBy(name = "bankTypeId")
    WebElement selBankTypeId;
    @FindBy(name = "selectedProviderId")
    WebElement selProvider;
    @FindBy(name = "submit")
    WebElement btnSubmit;
    @FindBy(name = "action:bankAdd_save")
    WebElement btnConfirm;


    public SubsBankAssociation_Page1(ExtentTest t1) {
        super(t1);
    }

    public SubsBankAssociation_Page1 setMsisdn(String text) {
        txtMsisdn.sendKeys(text);
        pageInfo.info("Set the Msisdn to - " + text);
        return this;
    }

    public SubsBankAssociation_Page1 selectBankType(String text) {
        Select sel = new Select(selBankTypeId);
        sel.selectByVisibleText(text);
        pageInfo.info("Set bank Type - " + text);
        return this;
    }

    public SubsBankAssociation_Page1 selectProvider(String text) {
        Select sel = new Select(selProvider);
        sel.selectByVisibleText(text);
        pageInfo.info("Set Provider - " + text);
        return this;
    }

    public SubsBankAssociation_Page1 clickSubmit() {
        btnSubmit.click();
        pageInfo.info("Click on Submit");
        return this;
    }

    public SubsBankAssociation_Page1 clickConfirm() {
        btnConfirm.click();
        pageInfo.info("Click on Confirm");
        return this;
    }

    /**
     * Common Methods
     */
    public SubsBankAssociation_Page1 navSubsBankAssociation() throws Exception {
        fl.leftNavigation("SUBSCRIBER_ALL", "SUBSCRIBER_SUBS_BNK_ASSOC");
        pageInfo.info("Navigate to Subscriber Management > Subscriber bank Association");
        return this;
    }

}
