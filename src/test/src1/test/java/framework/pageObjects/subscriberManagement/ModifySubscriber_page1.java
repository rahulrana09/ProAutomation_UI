package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifySubscriber_page1 extends PageInit {

    @FindBy(id = "_userNamePrefix")
    WebElement prefix;

    @FindBy(id = "_idType")
    WebElement idType;

    @FindBy(id = "_webPassword")
    WebElement newPassword;

    @FindBy(id = "_confWebPassword")
    WebElement confirmPassword;

    @FindBy(id = "subsRegistrationUpdateServiceBean_output_msisdn")
    WebElement msisdnTbox;

    @FindBy(id = "subsRegistrationUpdateServiceBean_output_button_submit")
    WebElement submit;

    @FindBy(id = "externalCode")
    WebElement identificationNumber;

    @FindBy(id = "_doc1")
    WebElement uploadIdentityProof;

    @FindBy(id = "_proofType1")
    WebElement selectIdentityProof;

    @FindBy(id = "_doc2")
    WebElement uploadAddressProof;

    @FindBy(id = "_proofType2")
    WebElement selectAddressProof;

    @FindBy(id = "_doc3")
    WebElement uploadPhotoProof;

    @FindBy(id = "_proofType3")
    WebElement selectPhotoProof;


    public ModifySubscriber_page1(ExtentTest t1) {
        super(t1);
    }

    public static ModifySubscriber_page1 init(ExtentTest t1) {
        return new ModifySubscriber_page1(t1);
    }

    public void selectPrefix() {
        selectIndex(prefix, 1, "Name Prefix");
    }

    public void selectIdType(String iDType) {
        selectVisibleText(idType,iDType,"ID Type");
    }

    public void selectIdType() {
        selectIndex(idType, 1, "ID Type");
    }

    public ModifySubscriber_page1 navSubscriberModification() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSMOD", "Subscriber Modification");
        return this;
    }

    public void uploadIdentityProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadIdentityProof, uploadFile, "uploadIdentityProof");
    }

    public void selectIdentityProof() {
        selectIndex(selectIdentityProof, 1, "selectIdentityProof");
    }

    public void uploadAddressProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadAddressProof, uploadFile, "uploadAddressProof");
    }

    public void selectAddressProof() {
        selectIndex(selectAddressProof, 2, "selectAddressProof");
    }

    public void uploadPhotoProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadPhotoProof, uploadFile, "uploadPhotoProof");
    }

    public void selectPhotoProof() {
        selectIndex(selectPhotoProof, 3, "selectPhotoProof");
    }

    public void setPassword(String pass) {
        setText(newPassword, pass, "newPassword");
    }

    public void setConfirmPassword(String pass) {
        setText(confirmPassword, pass, "confirmPassword");
    }

    public ModifySubscriber_page1 setidentificationNumber(String ExternalCode) {
        setText(identificationNumber, ExternalCode, "SUBS Identification Number");
        return this;
    }

    public ModifySubscriber_page1 setMSISDN(String msisdn) {
        setText(msisdnTbox, msisdn, "SUBS MSISDN");
        return this;
    }

    public ModifySubscriber_page2 clickOnSubmitPg1() {
        clickOnElement(submit, "Submit");
        return new ModifySubscriber_page2(pageInfo);
    }

}
