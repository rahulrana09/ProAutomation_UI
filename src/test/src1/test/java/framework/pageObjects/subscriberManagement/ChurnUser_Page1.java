/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 9/12/2017
 *  Purpose: Page Objects of ChurnUser Page1
 */
package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import tests.core.CR_B9.Suite_v5_ChurnRecord;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ChurnUser_Page1 extends PageInit {

    @FindBy(name = "providerId")
    private WebElement mfsProviderDdown;

    @FindBy(className = "wwFormTableC")
    private WebElement wTSettlement;

    @FindBy(xpath = "//*[@id=\"churnManagementAction_initiation\"]/table/tbody/tr[1]/td/a/img")
    private WebElement churnImageSampleFile;

    @FindBy(xpath = "//*[@id=\"submitButton\"]")
    private WebElement uploadBtn;

    @FindBy(name = "churnInitiationUpload")
    private WebElement btnUploadFile;

    @FindBy(xpath = "//*[@id=\"churnManagementAction_register\"]/table/tbody/tr[5]/td/a/img")
    private WebElement logFileImage;

    @FindBy(xpath = "//*[@id=\"failedCount\"]")
    private WebElement failCount;

    @FindBy(id = "churnManagementAction_register_currentbatchid")
    private WebElement batchId;

    @FindBy(xpath = "//*[@type=\"submit\"]")
    private WebElement submitBtn;

    @FindBy(xpath = "//*[@id=\"btnAdd\"]")
    private WebElement submitBtnForChurnReactivation;

    @FindBy(xpath = ".//*[@id=\'approvalForm\']/table/tbody/tr[1]/td/a/img")
    private WebElement LogApproveImage;

    @FindBy(xpath = "//input[@name=\'churnMsisdn\']")
    private WebElement churnMsisdnTextBox;

    @FindBy(id = "typeId")
    private WebElement userType;

    @FindBy(xpath = "//*[@id=\"typeId\"]")
    private WebElement Selectusertypedropdown;
    @FindBy(xpath = "//input[@id=\'churnSettlementTypeCHURN_SETTLE_REGISTERED_ACTIVE_ACCOUNT\']")
    private WebElement RegisterActiveSubscriberAccountradioBtn;
    @FindBy(xpath = "//input[@id=\'receiverMsisdn\']")
    private WebElement receiverMsisdnTextBox;
    @FindBy(xpath = "//input[@id=\'churnSettlementTypeCHURN_SETTLE_COMPLETE_REIMBURSEMENT\']")
    private WebElement completeReimbursementRadioBtn;
    @FindBy(xpath = "//input[@id=\'churnSettlementTypeCHURN_REACTIVATION\']")
    private WebElement churnReactivationRadioBtn;
    @FindBy(xpath = "(//input[@id=\'selectedArrayId\'])[last()]")
    private WebElement selectRecordCheckBox;
    @FindBy(xpath = "//input[@id=\'myForm_checkBack\']")
    private WebElement selectAllRecordCheckBox;
    @FindBy(xpath = "//input[@id=\'myForm_accept\']")
    private WebElement acceptBtn;
    @FindBy(xpath = "//input[@id=\'myForm_reject\']")
    private WebElement rejectBtn;
    @FindBy(xpath = "//input[@id=\'confirm\']")
    private WebElement confirmBtn;
    @FindBy(xpath = "(//input[@id=\'approvedBatchId\'])[last()]")
    private WebElement batchID;
    @FindBy(xpath = "//*[@id='churnreactive_confirmReactivateChannel_button_next']")
    private WebElement nextBtn;
    @FindBy(xpath = "//*[@id='churnreactive_saveChannelData_button_confirm']")
    private WebElement confirmforChurnReactivation;
    @FindBy(xpath = "//*[@id='churnreactive_loadUserData_button_confirm']")
    private WebElement confirmBtn1;
    @FindBy(xpath = "//*[@id='churnreactive_subsConfirm_button_confirm']")
    private WebElement subConfirmButton;
    @FindBy(xpath = "(//tbody[tr[td[contains(text(),'Batch Id')]]]//tr/td[3])[last()]")
    private WebElement batchId2;
    @FindBy(id = "typeId")
    private WebElement usrType;
    @FindBy(xpath = "//input[@id='approvalTypeCHURN_REJE']")
    private WebElement batchReject;
    @FindBy(className = "wwFormTableC")
    private WebElement webTable;
    @FindBy(id = "approvalTypeCHURN_BOTH")
    private WebElement radioApproveBySelection;
    @FindBy(xpath = "//input[@type=\"checkbox\"][@id=\"checkall\"]")
    private WebElement checkAllChurnApproval;
    @FindBy(xpath = "//table[@id='table']/tbody/tr/td[12]")
    private WebElement walletBalance;
    @FindBy(id = "approve")
    private WebElement approveBySelection;
    @FindBy(id = "approvalTypeCHURN_APPR")
    private WebElement radioBatchApprove;
    @FindBy(id = "approvalTypeCHURN_REJE")
    private WebElement radioBatchReject;
    @FindBy(id = "appchurn")
    private WebElement btnSubmit;

    public ChurnUser_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ChurnUser_Page1 init(ExtentTest t1) {
        return new ChurnUser_Page1(t1);
    }

    public List<String> selectusertypedropdown() {
        return fl.getOptionValues(Selectusertypedropdown);
    }

    /**
     * Navigate to User Churn Initiation
     *
     * @return current instance
     * @throws NoSuchElementException
     */
    public ChurnUser_Page1 navUserChurnInitiate() throws Exception {
        navigateTo("CHURNMGMT_ALL", "CHURNMGMT_CHURNMGMT_MAIN", "Churn Initiate");
        return this;
    }

    /**
     * selectProviderByValue
     *
     * @param providerID
     * @return
     */
    public ChurnUser_Page1 selectProviderByValue(String providerID) {
        Select prov = new Select(this.mfsProviderDdown);
        prov.selectByValue(providerID);
        pageInfo.info("Select Provider By Value: " + providerID);
        return this;
    }

    /**
     * navUserChurnApproval
     *
     * @return
     * @throws NoSuchElementException
     */
    public ChurnUser_Page1 navUserChurnApproval() throws Exception {
        navigateTo("CHURNMGMT_ALL", "CHURNMGMT_CHURN_APPROVE", "Churn Initiate Approve");
        return this;
    }

    public ChurnUser_Page1 selectBatchId(String batchId) {

        WebElement elem = driver.findElement(By.cssSelector("input[value='" + batchId + "'][type = 'radio']"));
        clickOnElement(elem, "Check Batch with Id:" + batchId);
        return this;
    }

    /**
     * downloadChurnSampleFile
     *
     * @return
     * @throws NoSuchElementException
     */
    public ChurnUser_Page1 downloadChurnSampleFile() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_CHURN_INITIATE);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_CHURN_INITIATE); // is hardcoded can be Generic TODO
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        churnImageSampleFile.click();
        pageInfo.info("Click On button Download");
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Utils.putThreadSleep(NumberConstants.SLEEP_3000);
        Utils.closeUntitledWindows();
        return this;
    }

    /**
     * setChurnUserMsisdn
     *
     * @param user
     * @return
     * @throws IOException
     */
    public ChurnUser_Page1 setChurnUserMsisdn(User user) throws IOException {
        Markup m = MarkupHelper.createLabel("setChurnUserMsisdn", ExtentColor.TEAL);
        pageInfo.info(m); // Method Start Marker

        String csvStr = (user.CategoryCode.equals(Constants.SUBSCRIBER)) ? user.MSISDN + ",,Y,N\n" : user.MSISDN + ",,N,Y\n";
        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(FilePath.fileChurnInitiation, true));
            e.newLine();
            e.write(csvStr);

            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pageInfo);
        }

        return this;
    }

    /**
     * setChurnUserMsisdn
     *
     * @param
     * @return
     * @throws IOException
     */
    public ChurnUser_Page1 setChurnUserMsisdn(String msisdn) throws IOException {
        Markup m = MarkupHelper.createLabel("setChurnUserMsisdn", ExtentColor.TEAL);
        pageInfo.info(m); // Method Start Marker

        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(FilePath.fileChurnInitiation, true));
            e.newLine();
            e.write(msisdn + ",,Y,N\n");

            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pageInfo);
        }

        return this;
    }

    public void checkForBatchApproval() {
        clickOnElement(radioBatchApprove, "Radio Batch Approval");
    }

    public void checkForApprovalBySelection() {
        clickOnElement(radioApproveBySelection, "Radio Approval By Selection");
    }

    public void checkAllApproval() {
        clickOnElement(checkAllChurnApproval, "Check all for approval");
    }

    public void approveBySelection() {
        clickOnElement(approveBySelection, "Check all for approval");
    }

    public String getWalletBalance() {
        return getElementText(walletBalance, "Wallet Balance ");
    }

    public void checkForBatchReject() {
        clickOnElement(radioBatchReject, "Radio Batch Reject");
    }

    public void clickOnSubmit() {
        clickOnElement(btnSubmit, "Submit");
    }


    /**
     * setUploadFile
     *
     * @param path
     */
    public void setUploadFile(String path) {
        this.btnUploadFile.sendKeys(new CharSequence[]{path});
        pageInfo.info("Set File for upload at Path - " + path);
    }

    public ChurnUser_Page1 setChurnUserMsisdnForSpecificUser(User usr) throws IOException {
        Markup m = MarkupHelper.createLabel("setChurnUserMsisdn", ExtentColor.TEAL);
        pageInfo.info(m); // Method Start Marker

        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(FilePath.fileChurnInitiation, true));
            e.newLine();
            if (usr.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                e.write(usr.MSISDN + ",");
                e.write("" + ",");
                e.write("Y" + ",");
                e.write("N");
            } else {
                e.write(usr.MSISDN + ",");
                e.write("" + ",");
                e.write("N" + ",");
                e.write("Y");
            }

            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pageInfo);
        }

        return this;
    }

    /**
     * checkTotalFiledCount
     *
     * @return
     * @throws NoSuchElementException
     */
    public ChurnUser_Page1 checkTotalFiledCount() throws Exception {
        String failedCount = this.failCount.getAttribute("value");
        if (failedCount.equalsIgnoreCase("1")) {
            pageInfo.pass("found the failed count as 1");
        } else {
            pageInfo.fail("found the failed count as" + failedCount);
        }

        return this;
    }

    /**
     * create a method to set user MSISDN and churn parameter
     */
    public ChurnUser_Page1 setChurnchannelUser(List<Suite_v5_ChurnRecord> churnRecords) throws IOException {
        Markup m = MarkupHelper.createLabel("setChurnUserMsisdn", ExtentColor.TEAL);
        pageInfo.info(m); // Method Start Marker

        try {
            BufferedWriter e = new BufferedWriter(new FileWriter(FilePath.fileChurnInitiation, true));
            e.newLine();
            for (Suite_v5_ChurnRecord churnRecord : churnRecords) {
                e.write(churnRecord.getUser().MSISDN);
                e.write(",,");
                e.write(churnRecord.isChurnSubs() ? "Y" : "N");
                e.write(",");
                e.write(churnRecord.isChurnChnl() ? "Y" : "N");
                e.newLine();
            }
            e.close();
        } catch (Exception var4) {
            Assertion.raiseExceptionAndContinue(var4, pageInfo);
        }

        return this;
    }

    /**
     * uploadChurnUserExcel
     *
     * @return
     * @throws NoSuchElementException
     */
    public ChurnUser_Page1 uploadChurnUserExcel() throws NoSuchElementException {
        setUploadFile(FilePath.fileChurnInitiation);
        uploadBtn.click();
        pageInfo.info("clicked the upload churn User button");
        return this;
    }

    public ChurnUser_Page1 checkTotalFiledCount0() throws Exception {
        String failedCount = this.failCount.getAttribute("value");
        if (failedCount.equalsIgnoreCase("0")) {
            pageInfo.pass("found the failed count as 0");
        } else {
            pageInfo.fail("found the failed count as" + failedCount);
        }

        return this;
    }

    public ChurnUser_Page1 clickOnSubmitBtn() throws Exception {
        clickOnElement(submitBtn, "Confirm Button");
        return this;
    }

    public ChurnUser_Page1 clickOnSubmitBtn_ChurnReactivation() throws Exception {
        this.submitBtnForChurnReactivation.click();
        pageInfo.info("clickOnSubmitBtn_ChurnReactivation");
        return this;
    }

    public void clickOnSubmitBtn1() throws Exception {
        clickOnElement(submitBtn, "Submit btn");
    }

    public int getChurnInitiateFailCount() throws Exception {
        return Integer.parseInt(failCount.getAttribute("value"));
    }

    public ChurnUser_Page1 navUserChurn_Settlement_Initiate() throws Exception {
        navigateTo("CHURNMGMT_ALL", "CHURNMGMT_CHURN_SETTLE_INIT", "Churn Settlement Initiate");
        return this;
    }

    public ChurnUser_Page1 navUserChurn_Settlement_Approval() throws Exception {
        navigateTo("CHURNMGMT_ALL", "CHURNMGMT_CHURN_SETTLE_APPROVE", "Churn Settlement Approval");
        return this;
    }

    public void enterChurnedMssidn(String churnedmssidn) {
        setText(churnMsisdnTextBox, churnedmssidn, "churnMsisdnTextBox");
        //setTextUsingJs(churnMsisdnTextBox,churnedmssidn,"churnMsisdnTextBox");
    }

    public void selectUserType(String userType) {
        setText(churnMsisdnTextBox, userType, "churnUserTypeDropDown");
    }

    /**
     * selectuserbyTEXT
     *
     * @param user
     * @return
     */
    public ChurnUser_Page1 selectusertypebytext(String user) {
        Select prov = new Select(this.Selectusertypedropdown);
        prov.selectByVisibleText(user);
        pageInfo.info("Select Provider By Text: " + user);
        return this;
    }

    public String getUserNameUI() throws IOException {
        return getTableDataUsingHeaderName(wTSettlement, "User Name");
    }

    public String getChurnedMSISDNUI() throws IOException {
        return getTableDataUsingHeaderName(wTSettlement, "Churned MSISDN");
    }

    public ChurnUser_Page1 setRegisterActiveSubscriberAccount() throws Exception {
        clickOnElement(RegisterActiveSubscriberAccountradioBtn, "RegisterActiveSubscriberAccountradioBtn");
        return this;
    }

    public ChurnUser_Page1 enterReceiverMsisdn(String receivermssidn) {
        setText(receiverMsisdnTextBox, receivermssidn, "receiverMsisdnTextBox");
        return this;
    }

    public boolean isSettlementPageOpen() {
        return fl.elementIsDisplayed(churnReactivationRadioBtn);
    }

    public ChurnUser_Page1 setcompleteReimbursement() throws Exception {
        clickOnElement(completeReimbursementRadioBtn, "completeReimbursementRadioBtn");
        return this;
    }

    public ChurnUser_Page1 setchurnReactivation() throws Exception {
        clickOnElement(churnReactivationRadioBtn, "churnReactivationRadioBtn");
        return this;
    }

    public ChurnUser_Page1 selectRecord(String msisdn) throws Exception {

        WebElement elem = driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr[1]/td/input[@type='checkbox']"));
        clickOnElement(elem, "Select record with MSISDN:" + msisdn);
        return this;
    }

    public ChurnUser_Page1 selectAllRecord() throws Exception {
        clickOnElement(selectAllRecordCheckBox, "selectAllRecordCheckBox");
        return this;
    }

    public ChurnUser_Page1 acceptRecord() throws Exception {
        clickOnElement(acceptBtn, "acceptBtn");
        return this;
    }

    public ChurnUser_Page1 rejectRecord() throws Exception {
        this.rejectBtn.click();
        pageInfo.info("clicked the reject button");
        return this;
    }

    public ChurnUser_Page1 rejectBatch() throws Exception {
        this.batchReject.click();
        pageInfo.info("checked the Batch reject button");
        return this;
    }


    public ChurnUser_Page1 confirmApproval() throws Exception {
        clickOnElement(confirmBtn, "confirmBtn");
        return this;
    }

    public ChurnUser_Page1 selectBatchID(String batchid) throws Exception {
        driver.findElement(By.xpath("//input[@type='radio' and @value='" + batchid + "']")).click();
        return this;
    }

    public ChurnUser_Page1 clickNext() throws Exception {
        clickOnElement(nextBtn, "clickNext");
        return this;
    }

    public ChurnUser_Page1 confirmChurnReactivation2() throws Exception {
        clickOnElement(confirmforChurnReactivation, "confirmChurnReactivation2");
        return this;
    }

    public ChurnUser_Page1 confirmChurnReactivation1() throws Exception {
        clickOnElement(confirmBtn1, "confirmChurnReactivation1");
        clickOnElement(subConfirmButton, "ConfirmSubReactivation");
        return this;
    }

    public ChurnUser_Page1 checkTotalFiledCount1() throws Exception {
        String failedCount = failCount.getAttribute("value");
        if (failedCount.equalsIgnoreCase("0")) {
            pageInfo.pass("found the failed count as 0");
        } else {
            pageInfo.fail("found the failed count as" + failedCount);
        }
        return this;
    }


    public ChurnUser_Page1 checkTotalFiledCount2() throws Exception {
        String failedCount = failCount.getAttribute("value");
        int count = Integer.parseInt(failedCount);
        if (count >= 0) {
            pageInfo.pass("found the failed count as 0" + failedCount);
        } else {
            pageInfo.fail("found the failed count as 0");
        }
        return this;
    }

    public String getbatchID1() throws Exception {
        String batchIdText = batchId2.getText();

        return batchIdText;
    }

    public String getbatchID() throws Exception {
        String batchIdText = batchId.getAttribute("value");
        return batchIdText;
    }

    public void selectSpecificBatchID(String batchid) throws Exception {
        driver.findElement(By.xpath("//input[@type='radio' and @value='" + batchid + "']")).click();
    }

    public ChurnUser_Page1 selectUsrType(String value) throws Exception {
        selectValue(usrType, value, "Select User type");
        return this;
    }

    /**
     * Check log file for Specific Message along with the Batch ID
     *
     * @param msisdn
     * @param batchId
     * @param message
     * @return
     * @throws Exception
     */
    public ChurnUser_Page1 checkLogFileSpecificMessage(String msisdn, String batchId, String message) throws Exception {

        try {
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "ChurnUserApprovalLogs");
            LogApproveImage.click();
            Thread.sleep(500);
            String churnLogFile1 = FilePath.dirFileDownloads + "/ChurnUserApprovalLogs.log";

            File file = new File(churnLogFile1);
            Scanner s = new Scanner(file);
            int lineIndex = 1;
            boolean success = true;
            String[] matcherArray = {msisdn, batchId, message};
            //we know that the log will start from 3rd line so moving to 3rd line
            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (lineIndex == 3) {
                    //reading the 3rd line for log
                    for (int i = 0; i < matcherArray.length; i++) {
                        if (line.contains(matcherArray[i])) {
                            pageInfo.pass("found text as " + matcherArray[i]);
                        } else {
                            success = false;
                            pageInfo.fail("couldn't match the details of the log file as desired");
                            break;
                        }
                    }
                    if (success) {
                        pageInfo.pass("log file contents matched as desired");
                    }
                    //now we don't want to read further
                    break;
                }
                lineIndex++;
            }
            s.close();
        } catch (IOException ioex) {
            // handle exception...
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }

    /**
     * @param msisdn
     * @param msgCode
     * @return
     * @throws Exception
     */
    public ChurnUser_Page1 checkLogFileSpecificMessage(String msisdn, String msgCode, String... params) throws Exception {
        logFileImage.click();
        Utils.putThreadSleep(NumberConstants.SLEEP_1000);
        try {
            String batchIdText = this.batchId.getAttribute("value");
            String churnLogFile = FilePath.dirFileDownloads + Constants.FILEPREFIX_CHURN_LOG + batchIdText + ".log";
            File file = new File(churnLogFile);
            Scanner s = new Scanner(file);

            boolean isVerified = false;
            boolean isFound = false;
            String expectedMsg = MessageReader.getMessage(msgCode, null);


            while (s.hasNextLine()) {
                String line = s.nextLine();
                if (line.contains(msisdn)) {
                    isFound = true;
                    String code[][] = {{"Verify Log File", "Churn Initiation"}, {"Actual", line.toString()}, {"Expected MSISDN", msisdn}, {"Expected Message", expectedMsg}};
                    Markup m = MarkupHelper.createTable(code);
                    if (line.contains(expectedMsg)) {
                        pageInfo.pass(m);
                    } else {
                        pageInfo.fail(m);
                        Assertion.markAsFailure("Failed to verify Churn Log File.");
                    }

                    if (params.length > 1) {
                        for (String data : params) {
                            if (!line.contains(data)) {
                                pageInfo.fail("Fail to find the data:" + data + " in Log Entry:" + line.toString());
                                Assertion.markAsFailure("Failed to verify Churn Log File.");
                            } else {
                                pageInfo.pass("Successfully found the data: '" + data + "' in Log Entry:" + line.toString());
                            }
                        }
                    }
                    break;
                }
            }
            if (!isFound) {
                pageInfo.fail("No Data for msisdn: " + msisdn + " & message: " + expectedMsg);
            }
            s.close();
        } catch (IOException ioex) {
            System.err.println("IO Exception Occurred" + ioex);
        } finally {
            Utils.closeUntitledWindows();
        }
        return this;
    }
}

