package framework.pageObjects.subscriberManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.Partner;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by Dalia on 06-06-2017.
 */
public class AddSubscriber_Page1 extends PageInit {


    @FindBy(name = "idNo")
    public WebElement idNumber;
    // Page objects
    @FindBy(name = "userNamePrefix")
    WebElement namePrefix;
    @FindBy(id = "subsRegistrationServiceBean_userName")
    WebElement firstName;
    @FindBy(id = "subsRegistrationServiceBean_lastName")
    WebElement lastName;
    @FindBy(name = "nationality")
    WebElement nationality;
    @FindBy(id = "msisdn")
    WebElement MSISDN;
    @FindBy(id = "externalCode")
    WebElement identificationNumber;
    @FindBy(name = "inputDob")
    WebElement dateOfBirth;
    @FindBy(name = "dojo.partyDateOfBirth")
    WebElement birthDate;
    @FindBy(id = "subsRegistrationServiceBean_gender")
    WebElement gender;
    @FindBy(id = "subsRegistrationServiceBean_registrationType")
    WebElement registrationType;
    @FindBy(id = "subsRegistrationServiceBean_idIssuePlace")
    WebElement idIssuePlace;
    @FindBy(id = "subsRegistrationServiceBean_idIssueCountry")
    WebElement idIssueCountry;
    @FindBy(id = "subsRegistrationServiceBean_residenceCountry")
    WebElement residenceCountry;
    @FindBy(name = "dojo.dateOfIssueOfId")
    WebElement idIssueDate;
    @FindBy(name = "dateOfExpiryOfId")
    WebElement idExpiryDate;
    @FindBy(id = "subsRegistrationServiceBean_city")
    WebElement city;
    @FindBy(id = "subsRegistrationServiceBean_postalCode")
    WebElement postalCode;
    @FindBy(id = "subsRegistrationServiceBean_employerName")
    WebElement employerName;
    @FindBy(id = "subsRegistrationServiceBean_localeCode")
    WebElement language;
    @FindBy(id = "webLoginId")
    WebElement loginId;
    @FindBy(id = "subsRegistrationServiceBean_webPassword")
    WebElement newPassword;
    @FindBy(id = "subsRegistrationServiceBean_confWebPassword")
    WebElement confirmPassword;
    @FindBy(name = "doc1")
    WebElement uploadIdentityProof;
    @FindBy(name = "proofType1")
    WebElement selectIdentityProof;
    @FindBy(name = "doc2")
    WebElement uploadAddressProof;
    @FindBy(name = "proofType2")
    WebElement selectAddressProof;
    @FindBy(name = "doc3")
    WebElement uploadPhotoProof;
    @FindBy(name = "proofType3")
    WebElement selectPhotoProof;

    /*  @FindBy(id = "subsRegistrationServiceBean_button_next")
      WebElement next;*/
    @FindBy(name = "region")
    WebElement region;
    @FindBy(name = "action:subsRegistrationServiceBean_output")
    WebElement next;
    @FindBy(name = "action:subsRegistrationUpdateServiceBean_updateWallet")
    WebElement nextModify;
    @FindBy(id = "checkExtCodeAvail")
    WebElement checkIdentificationNumber;

    @FindBy(name = "idType")
    WebElement idType;

    @FindBy(name = "birthPlace")
    WebElement txtBirthPlace;

    @FindBy(name = "address1")
    WebElement txtAddress1;

    @FindBy(name = "maritalStatus")
    WebElement maritalStatus;

    @FindBy(name = "district")
    WebElement txtDistrict;

    @FindBy(id = "availExtCode")
    WebElement v5_DuplicateExtCode;

    @FindBy(id = "subsRegistrationServiceBean_email")
    WebElement emailIDTbox;
    @FindBy(id = "isIdExpires")
    WebElement expireCheckBox;
    @FindBy(id = "availMSISDN")
    WebElement checkAvailMsisdnMsg;
    @FindBy(id = "subsRegistrationServiceBean_contactPerson")
    WebElement contactPersonTxtField;
    @FindBy(id = "availExtCode")
    WebElement errorMsg;
    @FindBy(id = "_residenceCountry")
    WebElement residenceCountryMod;
    @FindBy(id = "isIMTEnabled")
    WebElement isIMTEnabled;

    // ------------------- METHODS -----------------------------------------------------------------------
    @FindBy(id = "subsRegistrationServiceBean_imtidType")
    WebElement IMTIdType;
    @FindBy(id = "subsRegistrationServiceBean_idNo")
    WebElement IMTIdNumber;
    @FindBy(id = "isWUServiceEnabled")
    WebElement isWUServiceEnabled;
    @FindBy(id = "isMoneyGramSevicesEnabled")
    WebElement isMoneyGramSevicesEnabled;
    @FindBy(id = "subsRegistrationServiceBean_birthCountry")
    WebElement birthCountry;
    @FindBy(id = "subsRegistrationServiceBean_passportIssueCountry")
    WebElement passportIssueCountry;
    @FindBy(id = "subsRegistrationServiceBean_passportIssueCity")
    WebElement passportIssueCity;
    @FindBy(id = "subsRegistrationServiceBean_occupation")
    WebElement occupation;
    @FindBy(name = "dateOfPassportIssue")
    WebElement passportIssueDate;
    @FindBy(name = "button.check")
    private WebElement checkMsisdnAvailableButton;

    public AddSubscriber_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AddSubscriber_Page1 init(ExtentTest t1) {
        return new AddSubscriber_Page1(t1);
    }

    /*
    Negative Test
     */
    public String getMsgTextDuplicateIdeIdentificationNumExist() {
        if (fl.elementIsDisplayed(v5_DuplicateExtCode)) {
            return v5_DuplicateExtCode.getText();
        } else {
            return "AUT MSG: Message for Duplicate Identification number is not displayed";
        }
    }

    public String getErrorMsg() {
        String msg = errorMsg.getText();
        return msg;
    }

    public void setContactPerson(String contactPerson) {
        setText(contactPersonTxtField, contactPerson, "ContactPerson");
    }

    public AddSubscriber_Page1 checkIsIDExpire(boolean check) {
        boolean isChecked = expireCheckBox.isSelected();
        if(check){
            // make sure is id expire is checked
            if(!isChecked){
                clickOnElement(expireCheckBox, "Check Id Expiry Checkbox");
            }
        }else{
            // make sure that is id expire is not checked
            if(isChecked){
                clickOnElement(expireCheckBox, "UnCheck Id Expiry Checkbox");
            }
        }
        return this;
    }

    public AddSubscriber_Page1 navAddSubscriber() throws Exception {
        navigateTo("SUBSCRIBER_ALL", "SUBSCRIBER_SUBSADD", "Subscriber Management >  Add Subscriber Page");
        return this;
    }

    public AddSubscriber_Page1 clickCheckIdentification() {
        clickOnElement(checkIdentificationNumber, "checkIdentificationNumber");
        return this;
    }

    public void setDistrict(String text) {
        setText(txtDistrict, text, "District");
    }

    public void selectPrefix() {
        selectIndex(namePrefix, 1, "Name Prefix");
    }

    public void setFirstName(String text) {
        setText(firstName, text, "First Name");
    }

    public void setLastName(String text) {
        setText(lastName, text, "Last Name");
    }

    public void setMobileNumber(String msisdn) {
        setText(MSISDN, msisdn, "MSISDN");
    }

    public String getSelectedCountryOfResidence() {
        Select sel = new Select(residenceCountry);
        return sel.getFirstSelectedOption().getText().trim();
    }

    public String getSelectedIDType() {
        Select sel = new Select(idType);
        return sel.getFirstSelectedOption().getText().trim();
    }

    public AddSubscriber_Page1 setIdentificationNumber(String idenNum) {
        setText(identificationNumber, idenNum, "Identification Number");
        return this;
    }

    public void setDateOfBirth(String dob) {
        if (ConfigInput.isCoreRelease) {
//            setDate(birthDate, dob, "Date of Birth");
            setText(birthDate,dob,"Date of Birth");
        } else {
            setDate(dateOfBirth, dob, "Date of Birth");
        }
    }

    public void selectGender() {
        selectIndex(gender, 1, "Gender");
    }

    public void selectRegistrationType(String regType) {
        selectValue(registrationType, regType, "Registration Type");
    }

    public void setPlaceOfIssueId(String text) {
        setText(idIssuePlace, text, "idIssuePlace");
    }

    public void selectNationality() {
        selectIndex(nationality, 2, "Nationality");
    }

    public void selectIssueCountry() {
        selectIndex(idIssueCountry, 2, "idIssueCountry");
    }

    public void selectResidenceCountry() {
        selectIndex(residenceCountry, 2, "residenceCountry");
    }

    public void selectThirdPartyOption(List<Partner> partnerList) {
        try {
            if (!partnerList.isEmpty()) {
                for (Partner p1 : partnerList) {
                    if (driver.findElements(By.cssSelector(".checkboxLabel")).size() > 0) {
                        for (WebElement element : driver.findElements(By.cssSelector(".checkboxLabel"))) {
                            if (p1.partnerName.equalsIgnoreCase((element.getText())))
                                element.click();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setIssueDate(String issueDate) {
        setDate(idIssueDate, issueDate, "issue Date");
    }

    public void setExpiryDate(String expDate) {
        setDate(idExpiryDate, expDate, "idExpiryDate");
    }

    public void setCity(String text) {
        setText(city, text, "City");
    }

    public void setPostalCode(String code) {
        setText(postalCode, code, "postalCode"); //for registration
    }

    public void setEmployerName(String empName) {
        setText(employerName, empName, "employerName");
    }

    public void selectLanguage(String lang) {
        selectVisibleText(language, lang, "language");
    }

    public void setWebLoginId(String text) {
        setText(loginId, text, "loginId");
    }

    public void setPassword(String pass) {
        setText(newPassword, pass, "newPassword");
    }

    public void setConfirmPassword(String pass) {
        setText(confirmPassword, pass, "confirmPassword");
    }

    public void uploadIdentityProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadIdentityProof, uploadFile, "uploadIdentityProof");
    }

    public void selectIdentityProof() {
        selectIndex(selectIdentityProof, 1, "selectIdentityProof");
    }

    public void uploadAddressProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadAddressProof, uploadFile, "uploadAddressProof");
    }

    public void selectAddressProof() {
        selectIndex(selectAddressProof, 2, "selectAddressProof");
    }

    public void uploadPhotoProof(String uploadFile) throws Exception {
        Thread.sleep(800);
        setText(uploadPhotoProof, uploadFile, "uploadPhotoProof");
    }

    public void selectPhotoProof() {
        selectIndex(selectPhotoProof, 3, "selectPhotoProof");
    }

    public void selectRegion() {
        selectIndex(region, 1, "region");
    }

    public void clickNext() throws Exception {
        Utils.captureScreen(pageInfo);
        Utils.putThreadSleep(NumberConstants.SLEEP_2000);
        clickOnElement(next, "Next Button");
        Utils.putThreadSleep(NumberConstants.SLEEP_2000);
    }

    public void selectIdType() {
        selectIndex(idType, 2, "idType");
    }

    public List<String> getIdTypeOptionsValues() {
        return fl.getOptionValues(idType);
    }

    public AddSubscriber_Page1 setIdNumber(String text) {
        setText(idNumber, text, "idNumber");
        return this;
    }

    public void setBirthPlace(String text) {
        setText(txtBirthPlace, text, "txtBirthPlace");
    }

    public void setAddress1(String text) {
        setText(txtAddress1, text, "txtAddress1");
    }

    public void selectMaritalStatus() {
        selectIndex(maritalStatus, 2, "maritalStatus");
    }

    public void modifyClickNext() {
        clickOnElement(nextModify, "Next Subscriber Modification Initiate");
    }

    public List<WebElement> getAllLabels(String pageName) {
        pageInfo.info("Getting all the Label on the: " + pageName);//[contains(text(),'Name prefix')]
        List<WebElement> Labels = DriverFactory.getDriver().findElements(By.xpath("//table[@class = 'wwFormTableC']/tbody/tr/td/label"));
        // List<ArrayList> TabText=DriverFactory.getDriver().findElements(By.xpath("//td[@class='tabtextbold']"));
        return Labels;
    }

    public void setEmail(String email) {
        setText(emailIDTbox, email, "Email ID");
    }

    public void clickOnCheckMsisdnAvailabilityButton() {
        clickOnElement(checkMsisdnAvailableButton, "Next Subscriber Modification Initiate");
    }

    public String getAvailableMsisdnText() {
        return checkAvailMsisdnMsg.getText();
    }

    public AddSubscriber_Page1 setIsIMTEnabled(boolean isImtEnabled) {
        if (isImtEnabled) {
            if (!isIMTEnabled.isSelected()) {
                clickOnElement(isIMTEnabled, "IMT Enabled");
            }
        } else if (isIMTEnabled.isSelected()) {
            clickOnElement(isIMTEnabled, "IMT Enabled");
        }
        return this;
    }

    public void selectIMTIdType() {
        selectIndex(IMTIdType, 1, "IMT ID TYPE"); //for registration
    }

    public void setImtIdNumber(String idNumber) {
        setText(IMTIdNumber, idNumber, "IMT ID Number"); //for registration
    }

    public void isWUServiceEnabled(boolean serviceEnabled) {
        if (serviceEnabled) {
            if (!isWUServiceEnabled.isSelected()) {
                clickOnElement(isWUServiceEnabled, "WU Service");
            }
        } else if (isWUServiceEnabled.isSelected()) {
            clickOnElement(isWUServiceEnabled, "WU Service");
        }
    }

    public void isMoneyGramSevicesEnabled(boolean serviceEnabled) {
        if (serviceEnabled) {
            if (!isMoneyGramSevicesEnabled.isSelected()) {
                clickOnElement(isMoneyGramSevicesEnabled, "Money Gram Service");
            }
        } else if (isMoneyGramSevicesEnabled.isSelected()) {
            clickOnElement(isMoneyGramSevicesEnabled, "Money Gram Service");
        }
    }

    public void selectBirthCountry() {
        selectIndex(birthCountry, 2, "Birth Country");
    }

    public void selectPassportIssueCountry() {
        selectIndex(passportIssueCountry, 2, "Passport Issue Country");
    }

    public void selectPassportIssueCity(String city) {
        setText(passportIssueCity, city, "Passport Issue City");
    }

    public void setOccupation(String ocupation) {
        setText(occupation, ocupation, "Set Occupation");
    }

    public void setPassportIssueDate(String date) {
        setDate(passportIssueDate, date, "Passport Issue Date");
    }
}
