/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Page Of Master
*/
package framework.pageObjects.master;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class NetworkStatus_page1 extends PageInit {

    @FindBy(id = "netStatus_updateNetworkStatus_button_confirm")
    WebElement confirmBtn;
    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(id = "netStatus_networkConfirm_statusCheckBox_0_")
    private WebElement statusBox;

    @FindBy(id = "netStatus_networkConfirm_languageMessageList_0_")
    private WebElement englishLangBox;

    @FindBy(id = "netStatus_networkConfirm_languageMessageList_1_")
    private WebElement frenchLangBox;

    @FindBy(xpath = "//*[@onclick='submitForm()']")
    private WebElement save;
    @FindBy(name = "button.back")
    private WebElement backBtn;
    @FindBy(name = "button.reset")
    private WebElement resetBtn;

    public NetworkStatus_page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Method to Navigate to Master Page
     *
     * @throws Exception
     */
    public void navigateToLink() throws Exception {
        fl.leftNavigation("MMASTER_ALL", "MMASTER_MNT_STS");
        pageInfo.info("Navigate to Master Link");
    }

    /**
     * Method to set English Message
     *
     * @param text
     */
    public void setEnglishLangStatusBox(String text) {
        englishLangBox.clear();
        englishLangBox.sendKeys(text);
        pageInfo.info("Set value in English status field : " + text);
    }

    public String getEnglishLangStatusValue() {
        return englishLangBox.getAttribute("value");
    }

    /**
     * Method to get English Message
     *
     * @return
     */
    public String getEnglishStatusBoxText() {
        return englishLangBox.getText();
    }

    /**
     * Method to get French Message
     *
     * @return
     */
    public String getFrenchStatusBoxText() {
        return frenchLangBox.getText();
    }

    /**
     * Method to set French Language message
     *
     * @param text
     */
    public void setFrenchLangBox(String text) {
        frenchLangBox.clear();
        frenchLangBox.sendKeys(text);
        pageInfo.info("Set value in French status field : " + text);
    }

    /**
     * Method to click On Save Button
     */
    public void clickSaveButton() {
        save.click();
        pageInfo.info("Clicked on Save Button");
    }

    /**
     * Method to click on Confirm Button
     */
    public void clickConfirmButton() {
        if (ConfigInput.isConfirm) {
            confirmBtn.click();
            pageInfo.info("Clicked on Confirm Button");
        }
    }

    /**
     * Method to click on Back Button
     */
    public void clickBackBtn() {
        backBtn.click();
        pageInfo.info("Clicked on back Button");
    }

    /**
     * Method to click on reset button
     */
    public void clickResetButton() {
        resetBtn.click();
        pageInfo.info("Clicked on Reset Button");
    }

    public void statusBox_Click() {
        statusBox.click();
        pageInfo.info("Clicked on Status box");
    }
}
