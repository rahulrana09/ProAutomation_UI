package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddInstrumentTCP_pg2 extends PageInit {


    /*
     * Page Objects
	 */
    @FindBy(id = "user_min_balance_add")
    private WebElement UserMinBalance;
    @FindBy(id = "user_maximum_balance_add")
    private WebElement UserMaxBalance;
    @FindBy(id = "minimum_transaction_amount_add")
    private WebElement MinTransactionAmt;
    @FindBy(id = "maximum_transaction_amount_add")
    private WebElement MaxTransactionAmt;
    @FindBy(id = "maxPctTrnsAllwd_add")
    private WebElement MaxTransactionAllowed;
    @FindBy(id = "count[0]")
    private WebElement count;
    @FindBy(id = "count[1]")
    private WebElement amount;
    @FindBy(id = "add_instrument_next")
    private WebElement btnNext;
    @FindBy(css = "[id='add_instrument_next'][type='submit']")
    private WebElement btnConfirm;

    public AddInstrumentTCP_pg2(ExtentTest t1) {
        super(t1);
    }

    public static AddInstrumentTCP_pg2 init(ExtentTest t1) {
        return new AddInstrumentTCP_pg2(t1);
    }

    /**
     * Fill All the Text Fields
     */
    public void fillAllTextField() {
        // S E T   T H R E S H O L D   F O R   D E B I T   T R A N S A C T I O N
        List<WebElement> we = driver.findElements(By.xpath("//*[@class='threshhold_container']/div/div/div[1]/descendant::input[@thresholdtype='Per Transaction']"));
        for (WebElement i : we) {
            if (i.isDisplayed()) {
                i.clear();
                i.sendKeys(Constants.MAX_THRESHOLD);
            }
        }

        // S E T   T H R E S H O L D   F O R   C R E D I T   T R A N S A C T I O N
        List<WebElement> we2 = driver.findElements(By.xpath("//*[@class='threshhold_container']/div/div/div[3]/descendant::input[@thresholdtype='Per Transaction']"));
        for (WebElement j : we2) {
            if (j.isDisplayed()) {
                j.clear();
                j.sendKeys(Constants.MAX_THRESHOLD);
            }
        }

        //send specific values
        if (UserMinBalance.isDisplayed()) {
            setText(UserMinBalance, Constants.USR_MIN_BALANCE, "USR_MIN_BALACE");
        }

        if (UserMaxBalance.isDisplayed()) {
            setText(UserMaxBalance, Constants.USR_MAX_BALACE, "USR_MAX_BALACE");
        }

        if (MinTransactionAmt.isDisplayed()) {
            setText(MinTransactionAmt, Constants.MIN_TRANSACTION_AMT, "MIN_TRANSACTION_AMT");
        }

        if (MaxTransactionAmt.isDisplayed()) {
            setText(MaxTransactionAmt, Constants.MAX_TRANSACTION_AMT, "MAX_TRANSACTION_AMT");
        }

        if (MaxTransactionAllowed.isDisplayed()) {
            setText(MaxTransactionAllowed, Constants.MAX_TRANSACTION_ALLOWED, "MAX_TRANSACTION_ALLOWED");
        }
    }


    public void extraAssertionsForSmoke() throws IOException {
        Assertion.verifyEqual(MaxTransactionAllowed.isDisplayed(), true, "Assert Element Max Transaction Percent Allowed ", pageInfo, true);
        Assertion.verifyEqual(MaxTransactionAmt.isDisplayed(), true, "Assert Element MaxTransactionAmt", pageInfo, false);
        Assertion.verifyEqual(MinTransactionAmt.isDisplayed(), true, "Assert Element MinTransactionAmt", pageInfo, false);
        Assertion.verifyEqual(UserMaxBalance.isDisplayed(), true, "Assert Element UserMaxBalance", pageInfo, false);
        Assertion.verifyEqual(UserMinBalance.isDisplayed(), true, "Assert Element UserMinBalance", pageInfo, false);
        Assertion.verifyEqual(count.isDisplayed(), true, "Assert Element count", pageInfo, false);
        Assertion.verifyEqual(amount.isDisplayed(), true, "Assert Element amount", pageInfo, false);
    }

    /**
     * Click on Next
     *
     * @throws Exception
     */
    public void Next() throws Exception {
        clickOnElement(btnNext, "Next");
    }


    /**
     * Clicking on confirm
     *
     * @throws Exception
     */
    public void Confirm() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(3000);
        clickOnElementUsingJs(btnConfirm, "Confirm");
    }
}
