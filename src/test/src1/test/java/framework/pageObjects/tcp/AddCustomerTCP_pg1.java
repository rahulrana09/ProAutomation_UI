package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.CustomerTCP;
import framework.entity.InstrumentTCP;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddCustomerTCP_pg1 extends PageInit {

    @FindBy(id = "edit_customer_next")
    public WebElement editButton;

    @FindBy(id = "provider")
    public WebElement provider;

    @FindBy(id = "add_action_message")
    public WebElement addActionMessage;
    @FindBy(id = "action_message")
    public WebElement approveMessage;
    @FindBy(xpath = "//tbody[@id = 'addDetails_ALLPAYER']/tr/td/input[@thresholdtype = 'week' and @class = 'counts check']")
    WebElement cumulative_count_per_week;
    /*
     * Page Objects
	 */
    @FindBy(xpath = "//*[@id='add_new']/div/button")
    private WebElement AddNew;
    @FindBy(id = "domain")
    private WebElement DomainName;
    @FindBy(id = "category")
    private WebElement CategoryName;
    @FindBy(id = "registrationType")
    private WebElement RegType;
    @FindBy(id = "profile_name")
    private WebElement ProfileName;
    @FindBy(id = "description")
    private WebElement Description;
    @FindBy(id = "add_customer")
    private WebElement AddCustomerTCP;
    @FindBy(xpath = "//*[@id='addnew_modal']/div/div/div[2]/div[2]/div[2]/div/button")
    private WebElement Cancel;
    @FindBy(id = "add_action_message")
    private WebElement ActionMessage;
    @FindBy(className = "error_message")
    private WebElement ErrorMessage;
    @FindBy(xpath = "//*[@id=\"addnew_modal\"]/div/div/div[2]/div[1]/li")
    private WebElement ModalErrorMessage;
    @FindBy(id = "myTable")
    private WebElement myTable;

    public AddCustomerTCP_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AddCustomerTCP_pg1 init(ExtentTest t1) {
        return new AddCustomerTCP_pg1(t1);
    }

    /**
     * Cancel The action
     *
     * @throws Exception
     */
    public void cancelAddTCP() throws Exception {
        if (fl.elementIsDisplayed(Cancel)) {
            clickOnElement(Cancel, "Cancel");
            Thread.sleep(1200);
        } else {
            pageInfo.info("Cancel is not available");
        }
    }

    public void selectProviderName(String name){
        selectVisibleText(provider, name, "Currency Provider");
    }
    /**
     * Set the profile Name
     *
     * @param name
     */
    public void setProfileName(String name) {
        setText(ProfileName, name, "ProfileName");
    }

    /**
     * Set Description
     *
     * @param desc
     */
    public void setDescription(String desc) {
        setText(Description, desc, "Description");
    }

    /**
     * Add Customer TCP
     *
     * @throws Exception
     */
    public void Submit() throws Exception {
        clickOnElement(AddCustomerTCP, "AddCustomerTCP");
    }

    /**
     * Add initiate new Customer TCP
     *
     * @throws Exception
     */
    public void addInitiateCustomerTCP() throws Exception {
        cancelAddTCP();
        clickOnElement(AddNew, "AddCustomerTCP");
        Thread.sleep(1000);
    }

    /**
     * Select Domain Name
     *
     * @param domainName
     */
    public void selectDomainName(String domainName) throws InterruptedException {
        selectVisibleText(DomainName, domainName, "DomainName");
    }

    /**
     * Get all domain names from the UI
     *
     * @return
     */
    public List<String> getAllDomainNames() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(DomainName);
    }

    /**
     * Select Category Name
     *
     * @param catName
     */
    public void selectCategoryName(String catName) throws InterruptedException {
        selectVisibleText(CategoryName, catName, "CategoryName");
    }

    /**
     * Get all Category names from the UI
     *
     * @return
     * @throws InterruptedException
     */
    public List<String> getAllCategoryNames() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(CategoryName);
    }

    /**
     * Select Registration Type Name
     *
     * @param regType
     */
    public void selectRegType(String regType) throws Exception {
        selectVisibleText(RegType, regType, "RegType");
    }

    public void selectRegTypeByValue(String regTypeVal) throws Exception {
        selectValue(RegType, regTypeVal, "RegType");
    }

    public void clickTCPViewLink(String tcpName) throws Exception {
        driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[6]/a")).click();
        pageInfo.info("Click on TCP view link of Profile :" + tcpName);
    }

    /**
     * Get all Registration types from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllRegTypes() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(RegType);
    }

    /**
     * Navigate to Add Customer TCP Page
     *
     * @throws Exception
     */
    public void navAddCustomerTcp() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_USER", "Add Customer TCP");
    }

    /**
     * Check Error Message
     *
     * @return
     */
    public String checkErrorMessage() {
        try {
            if (ErrorMessage.isDisplayed()) {
                pageInfo.info(ErrorMessage.getText());
                return ErrorMessage.getText();
            }
        } catch (NoSuchElementException e) {
            pageInfo.info("No Error Found");
        }
        return null;
    }

    public String getErrorMessage() throws Exception {
        if (ErrorMessage.isDisplayed()) {
            Thread.sleep(1000);
            return ErrorMessage.getText();
        } else {
            return null;
        }
    }

    public String getModalErrorMessage() throws Exception {
        WebDriver driver = DriverFactory.getDriver();
        if (driver.findElements(By.xpath("//*[@id=\"addnew_modal\"]/div/div/div[2]/div[1]/li")).size() > 0) {
            Thread.sleep(1300);
            return ModalErrorMessage.getText();
        }
        return null;
    }

    //=====================used in p1 tc===========================//

    public String getActionMessage() throws Exception {
        WebDriver driver = DriverFactory.getDriver();
        wait.until(ExpectedConditions.visibilityOf(ActionMessage));
        if (driver.findElements(By.id("add_action_message")).size() > 0) {

            return ActionMessage.getText();
        }
        return null;
    }

    /**
     * Check for Pending approval
     *
     * @return
     */
    public boolean checkForPendingApprovals() {
        try {
            if (ErrorMessage.getText().equals(MessageReader.getMessage("trfProfileDao.error.CustProfileInitiated", null))) {
                pageInfo.info(ErrorMessage.getText());
                return true;
            }
        } catch (NoSuchElementException e) {
            pageInfo.info("Proceed to Approve Customer TCP");
        }
        return false;
    }

    /**
     * Assert Customer TCP is successfully created
     *
     * @param tcp
     * @return
     */
    public boolean assertTCPCreation(CustomerTCP tcp) throws Exception {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 30);
        String expected = MessageReader
                .getDynamicMessage("tcprofile.success.initiateCustTransferProfile", tcp.ProfileName, tcp.DomainName, tcp.CategoryName);

        String actual = wait.until(ExpectedConditions.visibilityOf(ActionMessage)).getText();

        return Assertion.verifyEqual(actual, expected, "Verify " + tcp.ProfileName + " is Successfully Created", pageInfo);
    }

    /**
     * Get Existing TCP Map
     *
     * @return
     */
    public Map<String, CustomerTCP> getExistingCustomerTCPmap() throws IOException {
        Markup m = MarkupHelper.createLabel("getExistingCustomerTCPmap", ExtentColor.BLUE);
        pageInfo.info(m);

        Map<String, CustomerTCP> map = new HashMap<String, CustomerTCP>();
        if (!fl.elementIsDisplayed(myTable)) {
            return map;
        }
        List<WebElement> tableData = myTable.findElements(By.tagName("tr"));

        for (WebElement row : tableData) {
            String tcpName = row.findElement(By.xpath("td[2]")).getText();
            String domainCategory = row.findElement(By.xpath("td[3]")).getText();
            String regType = row.findElement(By.xpath("td[4]")).getText();
            String provider = row.findElement(By.xpath("td[6]")).getText();

            CustomerTCP tcp = new CustomerTCP(tcpName, domainCategory, regType, provider);
            map.put(tcp.ProfileName, tcp);
        }
        Utils.captureScreen(pageInfo);
        return map;
    }

    public void clickEditButton() {
        clickOnElement(editButton, "Edit TCP");
    }

    public WebElement availableOperations(String operationName) {
        pageInfo.info("Verifying all Operations Section");
        return driver.findElement
                (By.xpath("//tr[td[@class = 'name']]/td[@class = 'operations']/a/abbr[@title = '" + operationName + "']"));
    }

    public WebElement availableFields(String fieldName) {
        return driver.findElement(By.xpath("//table[@class = 'edit_form']/tbody/tr/td/label[contains(text(),'" + fieldName + "')]"));
    }

    public void set_cumulative_count_per_week_debit() {
        pageInfo.info("set cumulative count per week debit");
        String count = cumulative_count_per_week.getAttribute("value");
        cumulative_count_per_week.clear();
        cumulative_count_per_week.sendKeys(count);
    }


    public void clickOnCopyCustomerTCP(String tcpName) throws Exception {
        WebElement copyLink = driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[9]/a"));
        pageInfo.info("Click on TCP Copy link of Profile :" + tcpName);
        clickOnElement(copyLink, "CopyCustomerTCP Link");
        Thread.sleep(Constants.TWO_SECONDS);
    }

    public void clickOnDeleteCustomerTCP(String tcpName) throws Exception {
        WebElement deleteLink = driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[8]/a"));
        pageInfo.info("Click on TCP Delete link of Profile :" + tcpName);
        clickOnElement(deleteLink, "DeleteCustomerTCP Link");
        Thread.sleep(Constants.TWO_SECONDS);
    }


    public void fillPageOneDetails(CustomerTCP tcp) throws Exception {
        navAddCustomerTcp();
        addInitiateCustomerTCP();
        selectDomainName(tcp.DomainName);
        selectCategoryName(tcp.CategoryName);
        selectRegTypeByValue(tcp.RegType);
        setProfileName(tcp.ProfileName);
        setDescription(tcp.ProfileName);
        Submit();
    }
}
