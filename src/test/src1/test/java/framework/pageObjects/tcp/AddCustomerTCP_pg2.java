package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddCustomerTCP_pg2 extends PageInit {

    @FindBy(id = "add_customer_confirm")
    public WebElement Confirm;

    @FindBy(className = "main-form-content")
    public WebElement mainFormContent;

    @FindBy(id = "add_customer_next")
    private WebElement Next;

    @FindBy(xpath = "//div[@class='success_image' and @id='add_action_message']")
    private WebElement ActionMessage;

    public AddCustomerTCP_pg2(ExtentTest t1) {
        super(t1);
    }

    public static AddCustomerTCP_pg2 init(ExtentTest t1) {
        return new AddCustomerTCP_pg2(t1);
    }

    public void submit() {
        Utils.scrollToBottomOfPage();
        clickOnElement(Next, "Btn Next");
    }

    public void confirm() throws Exception {
        Utils.scrollToBottomOfPage();
        clickOnElement(Confirm, "Btn Confirm");
    }

    /**
     * Fill All the Thresholds
     */
    public void fillAlltheTextField() {
        pageInfo.info("Filling the Thresholds");
        wait.until(ExpectedConditions.elementToBeClickable(mainFormContent));
        List<WebElement> we = mainFormContent.findElements(By.xpath("//tbody//input"));
        for (WebElement a : we) {
            if (a.isDisplayed()) {
                setText(a, Constants.MAX_THRESHOLD, "Threshold");
            }
        }
    }
}
