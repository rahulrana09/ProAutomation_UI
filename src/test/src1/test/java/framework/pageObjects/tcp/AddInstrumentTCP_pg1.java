package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.InstrumentTCP;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rahul.rana on 5/9/2017.
 */
public class AddInstrumentTCP_pg1 extends PageInit {

    /*
     * Page Objects
     */
    @FindBy(xpath = "//*[@id='add_new']/div/button")
    private WebElement AddNew;

    @FindBy(id = "provider")
    private WebElement Provider;

    @FindBy(id = "domain")
    private WebElement DomainName;

    @FindBy(id = "category")
    private WebElement CategoryName;

    @FindBy(id = "grade")
    private WebElement Grade;

    @FindBy(id = "payment_instrument")
    private WebElement PaymentInstrument;

    @FindBy(id = "wallet_type")
    private WebElement WalletType;

    @FindBy(id = "profile_name")
    private WebElement ProfileName;

    @FindBy(id = "description")
    private WebElement Description;

    @FindBy(id = "add_instrument")
    private WebElement AddInstrumentTCP;

    @FindBy(id = "add_modal_error_msg")
    private WebElement ErrorMessage;

    @FindBy(xpath = "//*[@id='add_instrument_form']/div/div[2]/div/button")
    private WebElement Cancel;

    @FindBy(id = "main_success_msg")
    private WebElement ActionMessage;

    @FindBy(xpath = "//*[@id=\"addnew_modal\"]/div/div/div[2]/div[1]/li")
    private WebElement ModalErrorMessage;

    @FindBy(id = "myTable")
    private WebElement myTable;


    public AddInstrumentTCP_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AddInstrumentTCP_pg1 init(ExtentTest t1) {
        return new AddInstrumentTCP_pg1(t1);
    }

    /**
     * Navigate to Add Customer TCP Page
     *
     * @throws Exception
     */
    public void navAddInstrumentTcp() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_INSTRMENT", "Add Instrument TCP");
        Utils.putThreadSleep(6000);
    }

    /**
     * Cancel The action
     *
     * @throws Exception
     */
    public void cancelAddTCP() throws Exception {
        if (fl.elementIsDisplayed(Cancel)) {
            clickOnElement(Cancel, "Cancel");
            Thread.sleep(1200);
        } else {
            pageInfo.info("Cancel is not available");
        }
    }

    /**
     * Get the Error message
     *
     * @return
     * @throws Exception
     */
    public String getErrorMessage() throws Exception {
        Thread.sleep(1500);
        return ErrorMessage.getText();
    }

    /**
     * Set the profile Name
     *
     * @param name
     */
    public void setProfileName(String name) {
        setText(ProfileName, name, "ProfileName");
    }

    /**
     * Set Description
     *
     * @param desc
     */
    public void setDescription(String desc) {
        setText(Description, desc, "Description");
    }

    /**
     * Add Customer TCP
     *
     * @throws Exception
     */
    public void Submit() throws Exception {
        clickOnElement(AddInstrumentTCP, "AddInstrumentTCP");
        Thread.sleep(1000);
    }

    /**
     * Add initiate new Customer TCP
     *
     * @throws Exception
     */
    public void addInitiateInstrumentTCP() throws Exception {
        clickOnElement(AddNew, "AddNew");
        Thread.sleep(1000);
    }

    /**
     * Open Add Instrument TCP Modal
     *
     * @throws Exception
     */
    public void openCreateTCP() throws Exception {
        cancelAddTCP();
        Utils.scrollToBottomOfPage();
        clickOnElement(AddNew, "AddNew");
        Thread.sleep(Constants.MAX_WAIT_TIME);
    }

    /**
     * Select Grade Name
     *
     * @throws Exception
     */
    public void selectProvider(String provider) throws Exception {
        selectVisibleText(Provider, provider, "Provider");
    }

    /**
     * Select Domain Name
     *
     * @param domainName
     * @throws Exception
     */
    public void selectDomainName(String domainName) throws Exception {
        selectVisibleText(DomainName, domainName, "DomainName");
    }

    /**
     * Get all domain names from the UI
     *
     * @return
     * @throws InterruptedException
     */
    public List<String> getAllDomainNames() throws InterruptedException {
        Thread.sleep(1200);
        return fl.getOptionText(DomainName);
    }

    /**
     * @param catName
     * @throws Exception Select Category Name
     * @throws
     */
    public void selectCategoryName(String catName) throws Exception {
        selectVisibleText(CategoryName, catName, "CategoryName");
    }

    /**
     * Get all Category names from the UI
     *
     * @return
     * @throws InterruptedException
     */
    public List<String> getAllCategoryNames() throws InterruptedException {
        Thread.sleep(1500);
        return fl.getOptionText(CategoryName);
    }

    /**
     * Select Grade Name
     *
     * @throws Exception
     */
    public void selectGradeName(String grade) throws Exception {
        selectVisibleText(Grade, grade, "GradeName");
    }

    /**
     * Get all Grade Name from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllGradeName() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(Grade);
    }

    /**
     * Select Wallet Type
     *
     * @throws Exception
     */
    public void selectWalletType(String walletType) throws Exception {
        selectVisibleText(WalletType, walletType, "WalletType");
    }

    /**
     * Get all Wallet Type from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllWalletType() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(WalletType);
    }

    /**
     * Select Payment Instrument
     *
     * @throws Exception
     */
    public void selectPaymentIstrument(String payInstrument) throws Exception {
        selectVisibleText(PaymentInstrument, payInstrument, "PaymentInstrument");
    }

    /**
     * Get all Payment Instrument from the UI
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllPaymentInstrument() throws Exception {
        Thread.sleep(1500);
        return fl.getOptionText(PaymentInstrument);
    }

    /**
     * Check Error Message
     *
     * @return
     */
    public boolean checkErrorMessage() {
        try {
            if (ErrorMessage.getText().equals(MessageReader.getMessage("trfProfileDao.error.CustProfileExists", null))) {
                pageInfo.info(ErrorMessage.getText());
                return true;
            }
        } catch (NoSuchElementException e) {
            pageInfo.info("Proceed to Create Customer TCPs");
        }
        return false;
    }

    public String getModalErrorMessage() throws Exception {
        Thread.sleep(1000);
        return ModalErrorMessage.getText();
    }

    /**
     * Check for Pending approval
     *
     * @return
     */
    public boolean checkForPendingApprovals() {
        try {
            if (ErrorMessage.getText().equals(MessageReader.getMessage("trfProfileDao.error.CustProfileInitiated", null))) {
                pageInfo.info(ErrorMessage.getText());
                return true;
            }
        } catch (NoSuchElementException e) {
            pageInfo.info("Proceed to Approve Customer TCP");
        }
        return false;
    }

    /**
     * Assert Customer TCP is successfully created
     *
     * @param tcp
     * @return
     * @throws Exception
     */
    public boolean assertTCPCreation(InstrumentTCP tcp) throws Exception {
        String expected = MessageReader
                .getDynamicMessage("tcprofile.success.initiateInstTransferProfile", tcp.ProfileName, tcp.DomainName, tcp.CategoryName);

        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 100);

        String actual = null;
        try {
            actual = wait.until(ExpectedConditions.visibilityOf(ActionMessage)).getText();
        } catch (Exception e) {
            Utils.captureScreen(pageInfo);
            actual = wait.until(ExpectedConditions.visibilityOf(ActionMessage)).getText();
        }

        if (Assertion.verifyEqual(actual, expected, "Verify " + tcp.ProfileName + " is Successfully Created", pageInfo)) {
            return true;
        }

        return false;
    }

    /**
     * Get Existing TCP Map
     *
     * @return
     */
    public Map<String, InstrumentTCP> getExistingInstrumentTCPmap() throws IOException {
        Markup m = MarkupHelper.createLabel("getExistingInstrumentTCPmap", ExtentColor.BLUE);
        pageInfo.info(m);

        Map<String, InstrumentTCP> map = new HashMap<String, InstrumentTCP>();

        if (!fl.elementIsDisplayed(myTable)) {
            pageInfo.info("No Instrument TCPs are yet Created in the sysytem!");
            Utils.captureScreen(pageInfo);
            return map;
        }

        List<WebElement> tableData = myTable.findElements(By.tagName("tr"));

        for (WebElement row : tableData) {
            String tcpName = row.findElement(By.xpath("td[2]")).getText();
            String domainCategory = row.findElement(By.xpath("td[3]")).getText();
            String instrumenType = row.findElement(By.xpath("td[4]")).getText();
            String gradeName = row.findElement(By.xpath("td[5]")).getText();
            String providerName = row.findElement(By.xpath("td[6]")).getText();

            InstrumentTCP tcp = new InstrumentTCP(domainCategory, instrumenType, gradeName, providerName, tcpName);
            map.put(tcp.ProfileName, tcp);
        }
        Utils.captureScreen(pageInfo);
        return map;
    }

    public void clickTCPViewLink(String tcpName) {
        clickOnElement(driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[8]/a")), "TCP View Link");
    }

    public void clickTCPEditLink(String tcpName) {
        clickOnElement(driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[9]/a")), "TCP Edit Link");
    }

    public void clickTCPDeleteLink(String tcpName) {
        clickOnElement(driver.findElement(By.xpath("//td[contains(text(),'" + tcpName + "')]/ancestor::tr[1]/td[10]/a")), "TCP Delete Link");
    }


    public void fillPageOneDetails(InstrumentTCP tcp) throws Exception {
        navAddInstrumentTcp();
        openCreateTCP();
        selectProvider(tcp.CurrencyProvider);
        selectDomainName(tcp.DomainName);
        selectCategoryName(tcp.CategoryName);
        selectGradeName(tcp.GradeName);
        selectPaymentIstrument(tcp.PaymentInstrument);
        selectWalletType(tcp.InstrumentType);
        setProfileName(tcp.ProfileName);
        setDescription(tcp.ProfileName);
        Submit();
    }


}
