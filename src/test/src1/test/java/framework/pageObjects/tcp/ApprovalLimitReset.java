package framework.pageObjects.tcp;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DateAndTime;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ApprovalLimitReset extends PageInit {


    /*
     * Page Objects
     */
    @FindBy(id = "enumId")
    public WebElement userType;

    @FindBy(id = "msisdn")
    public WebElement mobileNumber;

    @FindBy(name = "fromDate")
    private WebElement txtFromDate;

    @FindBy(name = "toDate")
    private WebElement txtToDate;

    @FindBy(id = "tcpApprovalLimitReset_submitAppr()_button_submit")
    private WebElement submit;

    @FindBy(id = "tcpApprovalLimitReset_submitConfirm()_button_submit")
    private WebElement confirm;

    @FindBy(id = "tcpApprovalLimitReset_approve()_button_submit")
    private WebElement approve;

    @FindBy(id = "tcpApprovalLimitReset_approve()_button_reject")
    private WebElement reject;


    public ApprovalLimitReset(ExtentTest t1) {
        super(t1);
    }

    public static ApprovalLimitReset init(ExtentTest t1) {
        return new ApprovalLimitReset(t1);
    }

    public void selectFromDojoCombo(String option) throws Exception {
        WebElement result = driver.findElement(By.xpath("//*[@id='page-home']/span/div[@resultvalue='" + option + "']"));
        Thread.sleep(1000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", result);
    }

    public void clickImage(WebElement elem) throws Exception {
        elem.click();
        Thread.sleep(500);
        elem.click();
        Thread.sleep(500);
        elem.click();
    }

    public void selectUserType(String user) throws Exception {
        Select sel = new Select(userType);
        sel.selectByVisibleText(user);
        pageInfo.info("Selected UserType: " + user);
    }

    public void enterMobilerNumber(String msisdn) throws Exception {
        mobileNumber.sendKeys(msisdn);
        pageInfo.info("Entered Mobile Number: " + msisdn);
    }

    public void selectFromDate(int day) throws Exception {
        String sDate = new DateAndTime().getDate(day);
        setDate(txtFromDate, sDate, "txtFromDate");
    }

    public void selectToDate(int day) throws Exception {
        String sDate = new DateAndTime().getDate(day);
        setDate(txtToDate, sDate, "txtToDate");
    }

    public void clickOnSubmit() throws Exception {
        submit.click();
        pageInfo.info("Submit Button is Clicked");
    }

    public void clickOnCheckBox(String loginId) throws Exception {
        driver.findElement(By.xpath(".//*[@id='tcpApprovalLimitReset_submitConfirm()']/table/tbody/tr[2]/td[contains(text(),'" + loginId + "')]/../td[1]/input"));
        pageInfo.info("Check Box is Clicked");
    }

    public void clickOnConfirm() throws Exception {
        confirm.click();
        pageInfo.info("Confirm Button is Clicked");
    }

    public void clickOnApprove() throws Exception {
        approve.click();
        pageInfo.info("Approve Button is Clicked");
    }

    public void clickOnReject() throws Exception {
        reject.click();
        pageInfo.info("Reject Button is Clicked");
    }

    /**
     * Navigate to Initiate Limit Reset
     *
     * @throws Exception
     */


    public void navApprovalLimitReset() throws Exception {
        navigateTo("TCPROFILE_ALL", "TCPROFILE_TCP_APPRLIMITRESET", "Initiate Limit Reset");
    }
}




