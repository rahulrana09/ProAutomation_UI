package framework.pageObjects.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PinManage extends PageInit {

    //Page Objects
    @FindBy(id = "add_new")
    private WebElement addNewRule;

    @FindBy(id = "add_customer_next")
    private WebElement submit;

    @FindBy(name = "maxAccBalance")
    private WebElement maxValue;


    public PinManage(ExtentTest t1) {
        super(t1);
    }

    public static PinManage init(ExtentTest t1) {
        return new PinManage(t1);
    }

    /**
     * Navigate methods
     */
    public void navLink() throws Exception {
        navigateTo("PINMANAGE_ALL", "PINMANAGE_RESETRULE", "Pin Management");
    }

    public void clickOnEdit() throws Exception {
        WebElement edit = driver.findElement(By.xpath("//span[@class='glyphicon glyphicon-edit']"));
        clickOnElement(edit, "Edit");

    }

    public void clickOnMaxValue(String value) {
        maxValue.clear();
        setText(maxValue, value, "Maximum Account Value");
    }

    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }
}
