package framework.pageObjects.pinManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeleteQuestions_pg1 extends PageInit {

    @FindBy(xpath = "//form[@name='fileUploadForm']/table/tbody/tr[1]/td/a/img")
    private WebElement downloadImg;

    @FindBy(xpath = "//button[.='Delete']")
    private WebElement deleteBtn;

    public DeleteQuestions_pg1(ExtentTest t1) {
        super(t1);
    }

    public static DeleteQuestions_pg1 init(ExtentTest t1) {
        return new DeleteQuestions_pg1(t1);
    }

    public void navDeleteQuestions() throws Exception {
        navigateTo("PINMANAGE_ALL", "PINMANAGE_DELQUEST", "Delete Questions to Master List");
    }

    public void selectSpecificQuestion(String question) {
        driver.findElement(By.xpath("//td[text()='" + question + "']/..//input[@type='checkbox']")).click();
        pageInfo.info("Selected the question: " + question);
    }

    public void clickDeleteBtn() throws Exception {
        clickOnElement(deleteBtn, "");
    }

}
