package framework.pageObjects.geoManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Properties;

/**
 * Created by amarnath.vb on 5/31/2017.
 */
public class geoManagement_pg1 extends Properties {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /******************************************************
     * GEO ADD ZONE PAGE
     */
    @FindBy(id = "geoAdd_save_grphDomainCode")
    public WebElement GeoDomainCode;
    @FindBy(id = "geoAdd_save_grphDomainName")
    public WebElement GeoDomainName;
    @FindBy(id = "geoAdd_save_grphDomainShortName")
    public WebElement GeoDomainShortName;
    @FindBy(id = "geoAdd_save_description")
    public WebElement GeoDescription;
    @FindBy(id = "geoAdd_save_btnAdd")
    public WebElement geoAddSave;
    @FindBy(id = "geoAdd_save_back")
    public WebElement geoAddSaveBack;
    /***************************************************
     * GEO Add zone confirm page
     */

    @FindBy(id = "geoAdd_confirm_btnAdd")
    public WebElement geoZoneAddConfirm;
    @FindBy(id = "geoAdd_confirm_btnCncl")
    public WebElement geoZoneAddConfirmCancel;
    @FindBy(id = "geoAdd_confirm_btnAddBack")
    public WebElement geoZoneAddConfirmBack;
    /*********************************
     * GEO ADD AREA - PAGE1
     */
    @FindBy(id = "geoMgmt_displayDomainList__parentTypeValues_0_")
    public WebElement geoAreaAddZone;
    @FindBy(id = "geoMgmt_displayDomainList_submitButton")
    public WebElement btnSybmitAreaCreation;
    @FindBy(id = "geoMgmt_addModifyDelete_btnAdd")
    public WebElement areaSubmit;
    @FindBy(id = "geoMgmt_addModifyDelete_btnBack")
    public WebElement areaAddBack;
    @FindBy(id = "geoAdd_save_grphDomainCode")
    public WebElement areaDomainCode;
    @FindBy(id = "geoAdd_save_grphDomainName")
    public WebElement areaDomainName;
    @FindBy(id = "geoAdd_save_grphDomainShortName")
    public WebElement areaDomainShortName;
    @FindBy(id = "geoAdd_save_description")
    public WebElement areaDescription;
    @FindBy(id = "geoAdd_save_btnAdd")
    public WebElement areaAddSave;
    @FindBy(id = "geoAdd_save_back")
    public WebElement AreaBack;
    @FindBy(id = "geoAdd_confirm_btnAdd")
    public WebElement AreaAddConfirm;
    @FindBy(id = "geoAdd_confirm_btnCncl")
    public WebElement AreaAddConfirmCancel;
    @FindBy(id = "geoAdd_confirm_btnAddBack")
    public WebElement AreaAddConfirmBack;
    /*****************************************
     * Geo Management Page Objects- PAGE-1
     ******************************************/

    @FindBy(id = "GEOMASTER_ALL")
    private WebElement geoManagementLink;
    @FindBy(id = "GEOMASTER_GRDOM001")
    private WebElement AddGeographyLink;
    @FindBy(id = "geoMgmt_showParentSearch_grphDomainType")
    private WebElement GeoDomainType;
    @FindBy(xpath = ".//*[@id='geoMgmt_showParentSearch']/table/tbody/tr[3]/td/input")
    private WebElement GeoSubmit;
    @FindBy(id = "geoMgmt_addModifyDelete_btnAdd")
    private WebElement GeoAdd;
    @FindBy(xpath = ".//*[@id='geoMgmt_addModifyDelete_btnModify']")
    private WebElement GeoUpdate;
    @FindBy(xpath = ".//*[@id='geoMgmt_addModifyDelete_btnModify']")
    private WebElement GeoDelete;
    @FindBy(id = "geoMgmt_addModifyDelete_btnBack")
    private WebElement GeoBack;

    public static geoManagement_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        geoManagement_pg1 page = PageFactory.initElements(driver, geoManagement_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;

    }

    public void setGeoDomainCode(String text) {
        GeoDomainCode.sendKeys(text);
        pageInfo.info("Set Domain Code - " + text);
    }

    public void setGeoDomainName(String text) {
        GeoDomainName.sendKeys(text);
        pageInfo.info("Set Domain Name - " + text);
    }

    public void setGeoDomainShortName(String text) {
        GeoDomainShortName.sendKeys(text);
        pageInfo.info("Set Domain Short Name - " + text);
    }

    public void setGeoDomainDescription(String text) {
        GeoDescription.sendKeys(text);
        pageInfo.info("Set Domain Description - " + text);
    }

    public void clickAddSave() {
        geoAddSave.click();
        pageInfo.info("Click On Save");
    }

    public void clickConfirm() {
        geoZoneAddConfirm.click();
        pageInfo.info("Click On Confirm");
    }

    public void clickAddArea() {
        btnSybmitAreaCreation.click();
        pageInfo.info("Click on Submit");
    }

    public void setZoneNameForArea(String text) {
        geoAreaAddZone.sendKeys(text);
        pageInfo.info("Set Zone Name For Area to be added - " + text);
    }

    public void addGeoArea() {
        areaSubmit.click();
        pageInfo.info("Click on Add geo Area");
    }
    //assertion: Geographical domain is added successfully

    public void navGeoManagement() throws Exception {
        fl.leftNavigation("GEOMASTER_ALL", "GEOMASTER_GRDOM001");
        pageInfo.info("Navigate to Geo Management Page");
    }

    public void navAddGeography() {
        AddGeographyLink.click();
    }

    public void setGeoDomainTypeZone() {
        Select sel = new Select(GeoDomainType);
        sel.selectByIndex(1);
        pageInfo.info("Select Zone at index 1!");
    }

    public void setGeoDomainTypeArea() {
        Select sel = new Select(GeoDomainType);
        sel.selectByIndex(2);
        pageInfo.info("Select Area at index 2!");
    }

    public void clickGeoSubmit() {
        GeoSubmit.click();
    }

    public void AddInitiate() {
        GeoAdd.click();
    }
}
