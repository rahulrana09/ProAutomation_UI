/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 07-Jan-2018
*  Purpose: Page of CCE  Enquiry
*/

package framework.pageObjects.CCE;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by surya.dhal on 1/7/2018.
 */
public class CustomerCareExecutiveEnquiryPage1 extends PageInit {
    private static String MainWindow;
    @FindBy(id = "user")
    private WebElement userTypeDDown;
    @FindBy(id = "filter")
    private WebElement accountIdentifierDDown;
    @FindBy(id = "filter_data")
    private WebElement filterDataTBox;
    @FindBy(id = "global_search_btn")
    private WebElement submitBtn;
    @FindBy(id = "consumer_status")
    private WebElement consumerStatusLabel;
    @FindBy(xpath = ".//*[contains(@href,'transaction_details')]")
    private WebElement transactionDetailsTab;
    @FindBy(id = "grade_name")
    private WebElement gradeName;
    @FindBy(id = "balance")
    private WebElement balance;
    @FindBy(xpath = ".//*[contains(@href,'account_information')]")
    private WebElement accountInformationTab;
    @FindBy(xpath = ".//*[contains(@href,'sva_account_information')]")
    private WebElement svaAccountInformation;
    @FindBy(xpath = ".//*[contains(@bank_info_account_information')]")
    private WebElement bankInfoAccountInformationLink;
    @FindBy(id = "bank_tranfer_amount")
    private WebElement accountID;
    @FindBy(xpath = ".//*[contains(@href,'reset_credentials')]")
    private WebElement resetCredentialsTab;
    @FindBy(id = "reset_pin_reset")
    private WebElement resetPinLink;
    @FindBy(name = "reset")
    private WebElement mobileResetPinRadioBtn;
    @FindBy(name = "pin")
    private WebElement mPinRadioBtn;
    @FindBy(id = "reset_pin")
    private WebElement resetPinSubmitBtn;
    @FindBy(id = "reset_employee_pin")
    private WebElement resetEmployeePinSubmitBtn;
    @FindBy(id = "web_password_reset_radio")
    private WebElement resetWebPasswordRadioBtn;
    @FindBy(id = "reset_password")
    private WebElement resetWebPasswordSubmitBtn;
    @FindBy(id = "employeeRole")
    private WebElement resetEmployeePinLink;
    @FindBy(xpath = ".//*[contains(@href,'Audit')]")
    private WebElement auditTab;


    public CustomerCareExecutiveEnquiryPage1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Method to navigate CCE Enquiry page.
     *
     * @throws Exception
     */
    public void navCustomerCareExecutiveEnquiry() throws Exception {
        fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_CCE_ENQUIRY");
        pageInfo.info("Navigate to Customer Care Executive Enquiry Page");
    }

    /**
     * Method to select user Type from Dropdown
     *
     * @param user
     */
    public void selectUserType(String user) throws ElementNotVisibleException {
        Select userType = new Select(userTypeDDown);
        userType.selectByValue(user);
        pageInfo.info("Select User Type : " + user);
    }

    /**
     * Method to select Identifier Type
     *
     * @param identifier
     */
    public void selectAccountIdentifier(String identifier) throws ElementNotVisibleException {
        Select identifierType = new Select(accountIdentifierDDown);
        identifierType.selectByValue(identifier);
        pageInfo.info("Select Account Identifier : " + identifier);
    }

    /**
     * Method to enter Filter Data(e.g MSISDN,AccountID etc)
     *
     * @param data
     */
    public void setFilterData(String data) throws ElementNotVisibleException {
        filterDataTBox.sendKeys(data);
        pageInfo.info("Enter Filter Data : " + data);
    }

    /**
     * Method to click on Submit button
     *
     * @throws ElementNotVisibleException
     */
    public void clickOnSubmitButton() throws ElementNotVisibleException {
        submitBtn.click();
        pageInfo.info("Click On Submit button");
    }

    /**
     * Method to handle Window
     */
    public void handleWindow() {
        MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                pageInfo.info("Switching to Child Window..");
            }
        }
        // Switching to Parent window i.e Main Window.
        //driver.switchTo().window(MainWindow);
    }

    /**
     * Method to switch to main Window
     */
    public void switchToMainWindow() {
        driver.switchTo().window(MainWindow);
        pageInfo.info("Switch to main Window");
    }

    /**
     * Methdo to get Status of User from GUI
     *
     * @return
     */
    public String getConsumerStatus() throws ElementNotVisibleException {
        String consumerStatus = consumerStatusLabel.getText();
        return consumerStatus;
    }

    /**
     * Method to close Child Window
     */
    public void closeChildWindow() {
        driver.close();
        pageInfo.info("Closing Child Window.");
    }

    /**
     * Method to Navigate to Transaction Tab
     */
    public void clickOnTransactionTab() {
        transactionDetailsTab.click();
        pageInfo.info("Clicked On Transaction Tab");
    }

    /**
     * Method to navigate to Account Information Tab
     */
    public void clickOnAccountInformationTab() {
        accountInformationTab.click();
        pageInfo.info("Clicked On Account Information Tab");
    }

    /**
     * Method to fetch Grade from GUI
     */
    public String getGrade() {
        String grade = gradeName.getText();
        pageInfo.info("Grade Of User : " + grade);
        return grade;
    }

    /**
     * Method to navigate to Credentials Tab
     */
    public void clickOnResetCredentialsTab() {
        resetCredentialsTab.click();
        pageInfo.info("Clicked On Reset Credentials Tab");
    }

    public void clickOnResetPINLink() {
        resetPinLink.click();
        pageInfo.info("Navigate to Seset PIN link");
    }


    public void selectMobileResetPinRadioButton() {
        mobileResetPinRadioBtn.click();
        pageInfo.info("Clicked On Mobile Reset Pin Radio Button");
    }

    public void selectMPINRadioButton() {
        mPinRadioBtn.click();
        pageInfo.info("Clicked On M-Pin Radio Button");
    }

    public void clickOnResetPINSubmitButton() {
        resetPinSubmitBtn.click();
        pageInfo.info("Clicked On Reset Pin Submit Button");
    }

    public void clickOnResetEmployeePINSubmitButton() {
        resetEmployeePinSubmitBtn.click();
        pageInfo.info("Clicked On Reset Employee Pin Submit Button");
    }

    public void selectWebPasswordResetRadioButton() {
        resetWebPasswordSubmitBtn.click();
        pageInfo.info("Clicked On Password Reset Radio Button");
    }

    public void clickWebPasswordSubmitButton() {
        resetWebPasswordSubmitBtn.click();
        pageInfo.info("Clicked On Reset Web Password Submit Button");
    }

    public void clickOnAuditTab() {
        auditTab.click();
        pageInfo.info("Clicked On Audit Tab");
    }


    public void clickOnBankInfoLink() {
        bankInfoAccountInformationLink.click();
        pageInfo.info("Clicked On Bank Info link");
    }

    public String getBalance() {
        String userBalance = balance.getText();
        userBalance = userBalance.split(" ")[1].trim();
        pageInfo.info("Balance of User : " + userBalance);
        return userBalance;
    }

    public String getAccountID() {
        String accountId = accountID.getText();
        return accountId;
    }

    public void clickOnResetEmployeePINLink() {
        resetEmployeePinLink.click();
        pageInfo.info("Clicked On Reset Employee Pin Link");
    }

    public String getNotificationMessage() {
        String message = driver.findElement(By.xpath(".//*[@id='reset_pin_failure']/div/div/div[2]/p")).getText();
        driver.findElement(By.xpath("//button[text() = 'OK']")).click();
        return message;
    }
}
