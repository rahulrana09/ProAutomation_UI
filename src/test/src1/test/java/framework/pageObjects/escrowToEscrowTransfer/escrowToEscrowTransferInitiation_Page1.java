package framework.pageObjects.escrowToEscrowTransfer;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dalia on 09-03-2018.
 */
public class escrowToEscrowTransferInitiation_Page1 extends PageInit {

    //Page Objects
    @FindBy(id = "mfsProvidersList")
    WebElement providerName;
    @FindBy(id = "fromBankId")
    WebElement fromBankName;
    @FindBy(id = "toBankId")
    WebElement toBankName;
    @FindBy(id = "escrowInit_confirmInitiate_refNumber")
    WebElement referenceNumber;
    @FindBy(id = "escrowInit_confirmInitiate__requestedQuantity")
    WebElement amount;
    @FindBy(id = "escrowInit_confirmInitiate_button_submit")
    WebElement submit;
    @FindBy(id = "stockButt")
    WebElement confirm;
    @FindBy(id = "escrowInit_confirmInitiate_back")
    WebElement back;
    @FindBy(id = "frombalance")
    WebElement fromBal;
    @FindBy(name = "tobalance")
    WebElement toBal;

    public escrowToEscrowTransferInitiation_Page1(ExtentTest t1) {
        super(t1);
    }

    public static escrowToEscrowTransferInitiation_Page1 init(ExtentTest t1) {
        return new escrowToEscrowTransferInitiation_Page1(t1);
    }

    /**
     * Navigate to Stock Management >  Escrow To Escrow Transfer Initiation page
     *
     * @throws Exception
     */
    public escrowToEscrowTransferInitiation_Page1 navigateToEscrowToEscrowTransferInitiationPage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_ESC_TRFDM", "Stock Management >  Escrow To Escrow Transfer");
        return this;
    }

    public escrowToEscrowTransferInitiation_Page1 selectProviderName(String text) throws Exception {
        selectVisibleText(providerName, text, "Provider Name");
        return this;
    }

    public void setReferenceNumber(String refNo) throws Exception {
        setText(referenceNumber, refNo, "Reference Num");
    }

    public void setAmount(String text) throws Exception {
        setText(amount, text, "Amount");
    }

    public void selectFromBank(String bankName) throws Exception {
        selectVisibleText(fromBankName, bankName, "From Bank");
    }

    public void selectToBank(String bankName) throws Exception {
        selectVisibleText(toBankName, bankName, "To Bank");
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit, "Submit");
    }

    //Click Confirm button
    public void clickConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    //Click Back button
    public void clickBack() {
        clickOnElement(back, "Back");
    }

    public String fetchFromBal() throws Exception {
        Thread.sleep(2000);
        String bal = fromBal.getAttribute("value").trim();
        pageInfo.info("From Bank balance: " + bal);
        return bal;

    }

    public String fetchToBal() throws Exception {
        Thread.sleep(2000);
        String bal = toBal.getAttribute("value").trim();
        pageInfo.info("To Bank balance: " + bal);
        return bal;

    }

    public String getSelectedMFSProvider() throws Exception {
        Select sel = new Select(this.providerName);
        pageInfo.info("Entered MFS Provider " + sel.getFirstSelectedOption().getText());
        return sel.getFirstSelectedOption().getText();
    }

    public String getSelectedFromBank() throws Exception {
        Select sel = new Select(fromBankName);
        pageInfo.info("Selected From Bank: " + sel.getFirstSelectedOption().getText());
        return sel.getFirstSelectedOption().getText();
    }

    public String getSelectedToBank() throws Exception {
        Select sel = new Select(toBankName);
        pageInfo.info("Selected To Bank: " + sel.getFirstSelectedOption().getText());
        return sel.getFirstSelectedOption().getText();
    }

    public String getRefNo() throws Exception {
        Thread.sleep(2000);
        pageInfo.info("Entered Reference Number: " + referenceNumber.getAttribute("value"));
        return referenceNumber.getAttribute("value");
    }

    public String getReqAmt() throws Exception {
        pageInfo.info("Entered requested Number: " + amount.getAttribute("value"));
        return amount.getAttribute("value");
    }

    public List<String> listOfFromBank() throws Exception {
        Select sel = new Select(fromBankName);
        List<WebElement> allOptions = sel.getOptions();
        ArrayList<String> options = new ArrayList<String>();

        for (WebElement from : allOptions) {
            String bankName = from.getText();
            options.add(bankName);
        }
        pageInfo.info("Get the list of From Bank options");
        return options;
    }

    public List<String> listOfToBank() throws Exception {
        Select sel = new Select(toBankName);
        List<WebElement> allOptions = sel.getOptions();
        ArrayList<String> options = new ArrayList<String>();

        for (WebElement from : allOptions) {
            String bankName = from.getText();
            options.add(bankName);
        }
        pageInfo.info("Get the list of To Bank options");
        return options;
    }

}
