package framework.pageObjects.escrowToEscrowTransfer;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dalia on 13-03-2018.
 */
public class escrowToEscrowTransferApproval_Page1 extends PageInit {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    //Page Objects
    @FindBy(id = "escrowApprove_displayTransactionDetails_button_submit")
    WebElement submit;
    @FindBy(id = "escrowApprove_displayTransactionDetails_button_approve")
    WebElement approve;
    @FindBy(id = "escrowApprove_displayTransactionDetails_button_reject")
    WebElement reject;
    @FindBy(id = "escrowApprove_displayTransactionDetails_remark")
    WebElement remarks;
    @FindBys({@FindBy(xpath = "(//form[@id='escrowApprove_displayTransactionDetails']/table/tbody/tr/td[2])[position()>1]")})
    private List<WebElement> txnIdList;
    @FindBys({@FindBy(xpath = "(//form[@id='escrowApprove_displayTransactionDetails']/table/tbody/tr/td[6])[position()>1]")})
    private List<WebElement> userIdList;


    public escrowToEscrowTransferApproval_Page1(ExtentTest test) {
        super(test);
    }

    public static escrowToEscrowTransferApproval_Page1 init(ExtentTest test) {
        return new escrowToEscrowTransferApproval_Page1(test);
    }

    /**
     * Navigate to Stock Management >  Escrow To Escrow Transfer Approval page
     *
     * @throws Exception
     */

    public void navigateToEscrowToEscrowTransferApprovalPage() throws Exception {
        navigateTo("STOCK_ALL", "STOCK_ESC_TRF_APPDM", "Escrow To Escrow Approve");
    }

    //Select transaction ID
    public void selectRadioButton(String transactionId) {
        DriverFactory.getDriver().findElement(By.xpath(".//*[@value='" + transactionId + "']")).click();
    }

    //Click Submit button
    public void clickSubmit() {
        clickOnElement(submit,"Submit Button");
    }

    //Click Approve button
    public void clickApprove() {
        clickOnElement(approve,"Approve Button");
    }

    public void clickReject() {
        clickOnElement(reject, "Reject Button");
    }

    public void enterRemarks(String remark) {
        setTextUsingJs(remarks, remark, "Remerks");

    }

    public List<String> listOftxnIds() throws Exception {
        List<String> txnIds = new ArrayList<String>();
        for (WebElement txn : txnIdList) {
            txnIds.add(txn.getText());
        }
        return txnIds;
    }

    public List<String> listOfuserIds() throws Exception {
        List<String> userIds = new ArrayList<String>();
        for (WebElement usr : userIdList) {
            userIds.add(usr.getText());
        }
        return userIds;
    }
}
