/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Dalia
 *  Date: 31-May-2017
 *  Purpose: ModifyDeleteBank_Page Object
 */

package framework.pageObjects.mfsProviderBankTypeMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dalia on 31-05-2017.
 */
public class ModifyDeleteBank_Page1 extends PageInit {


    //Page Objects
    @FindBy(id = "MfsBankMapping_mfsProviderListForBankMapping_button_modify")
    private WebElement modifyButton1;

    @FindBy(id = "MfsBankMapping_modifyBankMapping_defaultBankType")
    private WebElement DefaultBank;

    @FindBy(id = "MfsBankMapping_modifyBankMapping_button_modify")
    private WebElement modifyButton2;

    @FindBy(id = "MfsBankMapping_displayServicesForEditBankMapping_checkAll")
    private WebElement checkAll;

    @FindBy(id = "MfsBankMapping_displayServicesForEditBankMapping_button_submit")
    private WebElement Submit;

    @FindBy(id = "MfsBankMapping_mfsProviderListForBankMapping_button_delete")
    private WebElement deleteButton;

    @FindBy(id = "MfsBankMapping_deleteBankMapping_button_delete")
    private WebElement confirmDeleteButton;

    @FindBy(id = "MfsBankMapping_modifyBankMapping")
    private WebElement webTableBank;


    public ModifyDeleteBank_Page1(ExtentTest t1) {
        super(t1);
    }

    public void initiateModify() {
        clickOnElement(modifyButton1, "Initiate Modify");
    }

    public void initiateModify2() {
        clickOnElement(modifyButton2, "Complete Modify");
    }

    public void checkAllServices() {
        clickOnElement(checkAll, "Check All");
    }

    public void submitModify() throws InterruptedException {
        Utils.scrollToBottomOfPage();
        Thread.sleep(5000);
        clickOnElement(Submit, "Submit");
    }

    public void checkBank(String bankName) {
        WebElement checkBox = webTableBank.findElement(By.xpath(".//tr/td[contains(text(),'" + bankName + "')]/ancestor::tr[1]/td//input[@type='checkbox']"));

        if (!checkBox.isSelected()) {
            clickOnElement(checkBox, "Checkbox:" + bankName);
        }
    }

    /**
     * Navigate to MFS Provider Bank Type Master > Modify/Delete Bank page
     *
     * @throws Exception
     */
    public void navigateToModifyDeleteBank() throws Exception {
        navigateTo("MFSBTM_ALL", "MFSBTM_MFSBMD", "Bank Type Master > Modify/Delete Bank");
    }

    //Select Default Bank
    public void selectDefaultBankName(String bankName) throws Exception {
        selectVisibleText(DefaultBank, bankName, "DefaultBank");
    }

    public void selectProviderRadio(String providerID) {
        WebElement radio = DriverFactory.getDriver().findElement(By.xpath("//input[@type='radio' and @value='" + providerID + "']"));
        clickOnElement(radio, "Provider");
    }

    public void clickDeleteButton() {
        clickOnElement(deleteButton, "Delete");
    }

    public void clickConfirmDeleteButton() {
        clickOnElement(confirmDeleteButton, "Confirm Delete");
    }

    public String selectOtherBankName(String defaultBank) {
        Select banks = new Select(DefaultBank);
        String otherBank = null;
        List<WebElement> allBanks = new ArrayList<>();
        allBanks = banks.getOptions();
        for (WebElement bank : allBanks) {
            if (!defaultBank.equalsIgnoreCase(bank.getText())) {
                bank.click();
                otherBank = bank.getText();
                break;
            }
        }
        return otherBank;
    }

    public void unCheckBank(String bankID) {
        try {
            if (driver.findElement(By.xpath(".//*[@id='MfsBankMapping_modifyBankMapping_checkBank' and @value = '" + bankID + "']")).isSelected()) {
                driver.findElement(By.xpath(".//*[@id='MfsBankMapping_modifyBankMapping_checkBank' and @value = '" + bankID + "']")).click();
            }
            pageInfo.info("Bank Name is unchecked.");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
