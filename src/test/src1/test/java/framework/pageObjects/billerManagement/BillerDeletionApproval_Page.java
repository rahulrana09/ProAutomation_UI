package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerDeletionApproval_Page extends PageInit {

    @FindBy(id = "autoform3_button_approve")
    WebElement approveButton;


    public BillerDeletionApproval_Page(ExtentTest t1) {
        super(t1);
    }

    public void navBillerDeletion() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CDAP");
        pageInfo.info("Navigate to Biller Deletion Approval Page!");
    }

    public void clickApprovalLink(String billerCode) {
        WebElement approveLink = DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td/a[contains(@href,'Approve')]"));
        approveLink.click();
        pageInfo.info("Clicked on Approval Link..");
    }

    public void clickApproveButton() {
        approveButton.click();
        pageInfo.info("Click on Approve button .");
    }

}
