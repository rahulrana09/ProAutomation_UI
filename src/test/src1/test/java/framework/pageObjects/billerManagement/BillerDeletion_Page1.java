package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerDeletion_Page1 extends PageInit {

    @FindBy(id = "deleteMerchant_input_button_delete")
    WebElement deleteButton;
    @FindBy(id = "autoform3_button_next")
    WebElement btnNext;
    @FindBy(id = "autoform3_button_submit")
    WebElement btnSubmit;

    public BillerDeletion_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navBillerDeletion() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CDEL");
        pageInfo.info("Navigate to Biller Deletion Page!");
    }

    public void selectBillerToDelete(String billerCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td/input")).click();
        pageInfo.info("Selecting biller having code : " + billerCode);
    }


    public void clickDeleteButton() {
        deleteButton.click();
        pageInfo.info("Click on Delete button.");
    }


}
