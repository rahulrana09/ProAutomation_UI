/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 2-Jan-2018
 *  Purpose: Biller Registration Page Objects
 */
package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class BillerRegistrationPage1 extends PageInit {

    @FindBy(id = "addMerchant_input_button_add")
    private WebElement btnAddMerchant;

    @FindBy(id = "autoform3_loginId")
    private WebElement txtloginid;

    @FindBy(id = "autoform3_password")
    private WebElement txtPassword;

    @FindBy(id = "autoform3_confirmPassword")
    private WebElement txtConfirmPassword;

    @FindBy(id = "autoform3_userName")
    private WebElement txtBillerName;

    @FindBy(id = "autoform3_contactName")
    private WebElement ContactName;

    @FindBy(id = "userCode")
    private WebElement txtBillerCode;

    @FindBy(id = "autoform3_email")
    private WebElement txtEmail;

    @FindBy(id = "enumId")
    private WebElement selServiceLevel;
    @FindBy(id = "regreq")
    private WebElement chBoxIsRegistrationRequired;
    @FindBy(id = "autoform3_merCategoryCode")
    private WebElement selMerchantCatCode;
    @FindBy(id = "regConfirmInterval")
    private WebElement txtBilPaymentActivationInterval;
    @FindBy(id = "autoform3_paidBillNotf")
    private WebElement txtPayedBillNotificationFrequency;
    @FindBy(id = "autoform3_autoBillDelete")
    private WebElement txtAutoBilDeletionfequency;
    @FindBy(id = "processType")
    private WebElement selProccesType;
    @FindBy(id = "billerType")
    private WebElement selBillerType;
    @FindBy(id = "billAmount")
    private WebElement selbillAmountType;
    @FindBy(id = "partpaysubtype")
    private WebElement selPartPaySubType;
    @FindBy(id = "bankPaymentEffTo")
    private WebElement selPaymenteffected;
    @FindBy(id = "autoform3_checkmfs")
    private WebElement ckBoxProvider;
    @FindBy(name = "categoryCodeaa")
    private WebElement selTcp;
    @FindBy(id = "autoform3_button_next")
    private WebElement btnNext;
    @FindBy(name = "action:addMerchant_save")
    private WebElement btnSubmit;
    @FindBy(id = "addMerchant_assignBank_submit")
    private WebElement bankSubmit;

    @FindBy(id = "gradeCode")
    private WebElement selGradeName;

    @FindBy(id = "supportsOnlineTransReversal")
    private WebElement chkSupportOnlineReversal;

    public void setOnlineReversalState(boolean state){
        if(state && chkSupportOnlineReversal.isSelected())
            pageInfo.info("Online transaction reversal is already enabled");
        else if(state && !chkSupportOnlineReversal.isSelected())
            clickOnElement(chkSupportOnlineReversal, "Enable Support Online txn reversal");

        if(!state && !chkSupportOnlineReversal.isSelected())
            pageInfo.info("Online transaction reversal is already disable");
        else if(!state && chkSupportOnlineReversal.isSelected())
            clickOnElement(chkSupportOnlineReversal, "Disable Support Online txn reversal");
    }

    public void selectBillerGradeName(String gradeName){
        selectVisibleText(selGradeName, gradeName, "Grade Name");
    }


    public void selectProviderAndTcp(String providerId, int index){
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[name='checkmfs'][value='"+providerId+"']"))).click();
        pageInfo.info("Select provider: "+ providerId);
        String tcpId = index + providerId + "seltcp";
        WebElement selTcp = wait.until(ExpectedConditions.elementToBeClickable(By.id(tcpId)));
        selectIndex(selTcp, 1, "Select TCP");

    }
    public BillerRegistrationPage1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Biller Registration Page
     *
     * @throws NoSuchElementException NoSuchElementException
     */
    public BillerRegistrationPage1 navBillerRegistration() throws Exception {
        navigateTo("UTIL_ALL", "UTIL_UTL_CREG", "Biller Registration");
        return this;
    }

    /**
     * clickAddNewMerchant
     */
    public BillerRegistrationPage1 clickAddNewMerchant() throws InterruptedException {
        Thread.sleep(3000);
        clickOnElement(btnAddMerchant, "btnAddMerchant");
        return this;
    }

    /**
     * setLoginId
     *
     * @param loginId
     */
    public void setLoginId(String loginId) {
        setText(txtloginid, loginId, "Login Id");
    }

    /**
     * setPassword
     *
     * @param pass Password
     */
    public void setPassword(String pass) {
        wait.until(ExpectedConditions.elementToBeClickable(txtPassword));
        setText(txtPassword, pass, "txtPassword");
    }

    /**
     * selectBillerType
     *
     * @param billerType
     */
    public void selectBillerType(String billerType) {
        selectVisibleText(selBillerType, billerType, "BillerType");
    }

    public void selectBillerType() {
        selectIndex(selBillerType, 1, "selBillerType");
    }

    /**
     * selectProcessType
     *
     * @param processType ProcessType
     */
    public void selectProcessType(String processType) {
        selectValue(selProccesType, processType, "selProcessType");
    }

    /**
     * setAutoBillDeleteFrequency
     *
     * @param delFreq Bill Delete Frequency
     */
    public void setAutoBillDeleteFrequency(String delFreq) {
        setText(txtAutoBilDeletionfequency, delFreq, "txtAutoBilDeletionfequency");
    }

    /**
     * setPayerBillNotificationFrequency
     *
     * @param notifFreq BillNotificationFrequency
     */
    public void setPayerBillNotificationFrequency(String notifFreq) {
        setText(txtPayedBillNotificationFrequency, notifFreq, "txtPayedBillNotificationFrequency");
    }

    /**
     * setRegistrationConfirmInterval
     *
     * @param interval registration confirm interval
     */
    public void setRegistrationConfirmInterval(String interval) {
        setText(txtBilPaymentActivationInterval, interval, "txtBilPaymentActivationInterval");
    }

    /**
     * selectMerchantCategory
     *
     * @param catCode Merchant Category Code
     */
    public void selectMerchantCategory(String catCode) {
        //selectVisibleText(selMerchantCatCode, catCode, "selMerchantCatCode");
        Select select = new Select(selMerchantCatCode);
        select.selectByVisibleText(catCode);
        pageInfo.info("Select Biller Category:" + catCode);
    }

    /**
     * checkIfRegistrationRequired
     */
    public void checkIfRegistrationRequired() {
        clickOnElement(chBoxIsRegistrationRequired, "chBoxIsRegistrationRequired");
    }

    /**
     * selectServiceLevel
     *
     * @param serviceLvl Service Level
     */
    public void selectServiceLevel(String serviceLvl) {
        selectValue(selServiceLevel, serviceLvl, "selServiceLevel");
    }

    /**
     * setMerchantEmail
     *
     * @param email Email
     */
    public void setMerchantEmail(String email) {
        setText(txtEmail, email, "txtEmail");
    }

    /**
     * setMerchantCode
     *
     * @param code
     */
    public void setMerchantCode(String code) {
        setText(txtBillerCode, code, "txtBillerCode");
    }

    /**
     * setMerchantName
     *
     * @param name Merchant Name
     */
    public void setMerchantName(String name) {
        setText(txtBillerName, name, "txtBillerName");
    }

    /**
     * @param name
     */
    public void setContactName(String name) {
        setText(ContactName, name, "ContactName");
    }

    /**
     * setConfirmPassword
     *
     * @param pass Confirm Password
     */
    public void setConfirmPassword(String pass) {
        setText(txtConfirmPassword, pass, "txtConfirmPassword");
    }

    /**
     * Click on button Submit
     */
    public void clickButtonSubmit() {
        //JavascriptExecutor executor = (JavascriptExecutor)driver;
        //executor.executeScript("arguments[0].click();", btnSubmit);
        //actions.moveToElement(btnSubmit).perform();
        //actions.click(btnSubmit).perform();
        clickOnElement(btnSubmit, "btnSubmit");
        Utils.putThreadSleep(5000);

    }

    public void clickBankSubmit() {
        clickOnElement(bankSubmit, "bankSubmit");
    }

    /**
     * clickButtonNext
     */
    public void clickButtonNext() {
        clickOnElement(btnNext, "btnNext");
    }

    /**
     * selectMerchantTcp
     *
     * @param tcp Transfer Control Profile
     */
    public void selectMerchantTcp(String tcp) {
        selectVisibleText(selTcp, tcp, "selTcp");
    }

    /**
     * selectMerchantTcpByIndex
     */
    public void selectMerchantTcpByIndex() {
        Select sel = new Select(selTcp);
        if (sel.getOptions().size() > 1)
            sel.selectByIndex(1);
        else
            sel.selectByIndex(0);
        pageInfo.info("select Transfer Control Profile - ");
    }

    /**
     * checkProvider
     */
    public void checkProvider() {
        clickOnElement(ckBoxProvider, "ckBoxProvider");
    }

    /**
     * selectPaymentEffectedTo
     *
     * @param paymentEffectedTo Payment Effected To
     */
    public void selectPaymentEffectedTo(String paymentEffectedTo) {
        selectValue(selPaymenteffected, paymentEffectedTo, "selPaymenteffected");
    }

    /**
     * selectPartPaymentSubType
     *
     * @param subType Part Payment Sub Type
     */
    public void selectPartPaymentSubType(String subType) {
        selectVisibleText(selPartPaySubType, subType, "selPartPaySubType");
    }

    /**
     * selectBillAmountType
     *
     * @param amount Amount
     */
    public void selectBillAmountType(String amount) {
        selectVisibleText(selbillAmountType, amount, "selbillAmountType");
    }


    /**
     *
     * @return
     */
    public List<String> getAllValuesFromServiceLevelDropdown(){
        return fl.getOptionValues(selServiceLevel);
    }


}
