package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class SubsBillerAssociation_pg1  extends PageInit{
    @FindBy(name = "providerId")
    WebElement selCurrencyProvider;
    @FindBy(name = "msisdn")
    WebElement txtSubsMsisdn;
    @FindBy(id = "billerRegVal_input_button_submit")
    WebElement btnSubmit;
    @FindBy(id = "billerRegVal_billerRegNext_button_submit")
    WebElement btnSubmitConfirmAssociation;

    public SubsBillerAssociation_pg1(ExtentTest t1) {
        super(t1);
    }

    public static SubsBillerAssociation_pg1 init(ExtentTest t1) throws Exception {
        return new SubsBillerAssociation_pg1(t1);
    }

    public SubsBillerAssociation_pg1 setSubscriberMsisdn(String text) {
        setText(txtSubsMsisdn, text, "Sub Msisdn");
        return this;
    }

    public SubsBillerAssociation_pg1 selectProvider(String text) {
        selectVisibleText(selCurrencyProvider, text, "Provider");
        return this;
    }

    public SubsBillerAssociation_pg1 navAddBillerValidation() throws Exception {
        navigateTo("UTIL_ALL", "UTIL_UTL_BILREG", "Navigate To subscriber Biller Associatioin");
        return this;
    }

    public SubsBillerAssociation_pg1 clickOnSubmit() {
        clickOnElement(btnSubmit, "Submit");
        return this;
    }

    public SubsBillerAssociation_pg1 clickOnSubmitConfirmAssociation() throws Exception {
        clickOnElement(btnSubmitConfirmAssociation, "Confirm");
        return this;
    }

    public SubsBillerAssociation_pg1 checkBiller(String bcode) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/input[@type='checkbox']")).click();
        pageInfo.info("Click on check box correcponding to Biller Code - " + bcode);
        return this;
    }

    public SubsBillerAssociation_pg1 setAccountNumber(String bcode, String accNumber) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/input[@type='text']")).sendKeys(accNumber);
        pageInfo.info("Set Bill Number as - " + accNumber);
        return this;
    }
}
