package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class BillPay_pg1 extends PageInit {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static BillPay_pg1 page;

//    public static BillPay_pg1 init(ExtentTest t1) throws Exception {
//        pageInfo = t1;
//        driver = DriverFactory.getDriver();
//        fl = new FunctionLibrary(driver);
//        page = PageFactory.initElements(driver, BillPay_pg1.class);
//        return page;
//    }
    @FindBy(xpath = "//select[@name ='billingMode']")
    WebElement ProcessType;
    @FindBy(xpath = "//select[@name ='serviceName']")
    WebElement services;
    @FindBy(xpath = "//select[@name ='billerCode']")
    WebElement billerCode;
    @FindBy(id = "billerAccountNo")
    WebElement billeraccountNo;
    @FindBy(id = "billPayment_input_amount")
    WebElement billamt;
    @FindBy(id = "billPayment_input_refIdOrOthrMsisdn")
    WebElement msisdn;
    @FindBy(id = "billPayment_input_button_submit")
    WebElement submit;
    @FindBy(xpath = "//select[@name ='providerName']")
    WebElement provider;
    @FindBy(xpath = "//select[@name ='paymentMethodType']")
    WebElement paymentMethod;
    @FindBy(xpath = "//select[@name ='WalletTypeId']")
    WebElement Wallettype;
    @FindBy(id = "billPayment_confirm_button_confirm")
    WebElement confirm;

    public BillPay_pg1(ExtentTest t1) {
        super(t1);
    }

    public static BillPay_pg1 init(ExtentTest t1) {
        return new BillPay_pg1(t1);
    }

    public BillPay_pg1 navBillPayment() throws Exception {
        fl.leftNavigation("BP_ALL", "BP_BP_RET");
        pageInfo.info("Navigate to BillPay");
        return this;
    }

    public void selectProcessType(String status) {
        ProcessType.click();
        Select stat1 = new Select(ProcessType);
        stat1.selectByValue(status);
        pageInfo.info("Select Status :" + status);
    }

    public void selectServices(String status) {
        services.click();
        Select stat1 = new Select(services);
        stat1.selectByValue(status);
        pageInfo.info("Select Status :" + status);
    }

    public void selectBillerCode(String billercode) {
        billerCode.click();
        Select stat1 = new Select(billerCode);
        stat1.selectByValue(billercode);
        pageInfo.info("Select Status :" + billercode);
    }

    public void setBilleraccountNo(String text) {
        billeraccountNo.sendKeys(text);
        pageInfo.info("Set biller account Number - " + text);
    }

    public void setBillPaymentAmt(String text) {
        billamt.sendKeys(text);
        pageInfo.info("Set bill amountr - " + text);
    }

    public void setMsisdn(String text) {
        msisdn.sendKeys(text);
        pageInfo.info("Set Msisdn - " + text);
    }

    public void submit_click() {
        submit.click();
        pageInfo.info("clicked on submit button");
    }

    public void selectProvider(String type) {
        provider.click();
        Select stat1 = new Select(provider);
        stat1.selectByVisibleText(type);
        pageInfo.info("Select Status :" + type);
    }

    public void selectpaymentInstrument(String type) {
        paymentMethod.click();
        Select stat1 = new Select(paymentMethod);
        stat1.selectByValue(type);
        pageInfo.info("Select Status :" + type);
    }

    public void selectWalletType(String type) {
        Wallettype.click();
        Select stat1 = new Select(Wallettype);
        stat1.selectByValue(type);
        pageInfo.info("Select Wallet :" + type);
    }

    public void confirm_click() {
        confirm.click();
        pageInfo.info("clicked on confirm button");
    }

}
