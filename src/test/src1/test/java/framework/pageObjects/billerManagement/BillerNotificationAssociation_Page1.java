package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerNotificationAssociation_Page1 extends PageInit {


    @FindBy(id = "notification_input_add")
    WebElement addButton;
    @FindBy(id = "notification_input_update")
    WebElement updateButton;

    public BillerNotificationAssociation_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navBillerNotificationAssociation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_NOTF");
        pageInfo.info("Navigate to Notification Association Page!");
    }

    public void selectNotificationToUpdate(String notifnName) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + notifnName + "')]/ancestor::tr[1]/td/input")).click();
        pageInfo.info("Selecting Notification having code : " + notifnName);
    }

   /* public void selectNotificationToUpdate(String billValue){
        DriverFactory.getDriver().findElement(By.xpath("//input[@type='radio' and @value='"+billValue+"']")).click();
        pageInfo.info("Selecting Notification having code : "+billValue);
    }*/


    public void clickAddButton() {
        addButton.click();
        pageInfo.info("Click on Add button");
    }

    public void clickUpdateButton() {
        updateButton.click();
        pageInfo.info("Click on Update button");
    }


}
