package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerModification_Page1 extends PageInit {

    @FindBy(id = "modifyMerchant_input_button_update")
    WebElement updateButton;


    public BillerModification_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navBillerModification() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CMOD");
        pageInfo.info("Navigate to Biller Modification Page!");
    }

    public void selectBillerToModify(String billerCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td/input")).click();
        pageInfo.info("Selecting biller having code : " + billerCode);
    }


    public void clickUpdateButton() {
        updateButton.click();
        pageInfo.info("Click on Update button");
    }


}
