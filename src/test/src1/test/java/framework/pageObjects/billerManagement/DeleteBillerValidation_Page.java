package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DeleteBillerValidation_Page extends PageInit {

    @FindBy(id = "deleteBillerVal_input_button_deleteValidations")
    WebElement deleteValidationBtn;
    @FindBy(id = "deleteBillerVal_viewValidations_submit")
    WebElement confirmButton;

    public DeleteBillerValidation_Page(ExtentTest t1) {
        super(t1);
    }

    public void navDeleteBillerValidation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CDVAL");
        pageInfo.info("Navigate to Delete Biller Validation");
    }

    public void selectBillerToDeleteValidation(String bCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + bCode + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        pageInfo.info("Click on Radio button corresponding to Biller Code - " + bCode);
    }


    public void clickDeleteValidationBtn() {
        deleteValidationBtn.click();
        pageInfo.info("Clicked on Delete Validation Button");
    }

    public void clickConfirmBtn() {
        confirmButton.click();
        pageInfo.info("Clicked on Confirm Button");
    }


}
