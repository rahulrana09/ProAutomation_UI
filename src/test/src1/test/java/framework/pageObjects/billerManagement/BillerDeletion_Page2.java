package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerDeletion_Page2 extends PageInit {

    @FindBy(id = "autoform3_deletionRemark")
    WebElement remarkTextArea;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirmButton;

    public BillerDeletion_Page2(ExtentTest t1) {
        super(t1);
    }

    public void setRemarks(String remarks) {
        remarkTextArea.sendKeys(remarks);
        pageInfo.info("Set remarks : " + remarks);
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }


}
