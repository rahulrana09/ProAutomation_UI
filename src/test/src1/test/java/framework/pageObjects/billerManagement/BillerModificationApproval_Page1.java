package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerModificationApproval_Page1 extends PageInit {

    @FindBy(id = "autoform3_button_approve")
    WebElement approveButton;


    public BillerModificationApproval_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navBillerModificationApproval() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CMODAP");
        pageInfo.info("Navigate to Biller Modification Page!");
    }

    public void selectBillerToModifyApprove(String billerCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td[7]/a")).click();
        pageInfo.info("Selecting biller having code : " + billerCode);
    }


    public void clickApproveButton() {
        approveButton.click();
        pageInfo.info("Click on Update button");
    }


}
