package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class BillerSuspendInitiate_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "suspendMerchant_input_button_suspend")
    WebElement Add;
    @FindBy(id = "autoform3_button_confirm")
    WebElement suspend;

    public static BillerSuspendInitiate_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        BillerSuspendInitiate_pg1 page = PageFactory.initElements(driver, BillerSuspendInitiate_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void NavigateToLink() {

        try {
            fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CSUS");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("link not found");
        }


    }

    public void selectuser(String bcode) {

        driver.findElement(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        Add.click();

    }

    public void confirm() {

        suspend.click();

    }


}
