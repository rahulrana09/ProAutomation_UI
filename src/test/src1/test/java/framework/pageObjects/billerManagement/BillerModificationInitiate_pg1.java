package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class BillerModificationInitiate_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "modifyMerchant_input_button_update")
    WebElement Add;
    @FindBy(id = "autoform3_loginId")
    WebElement loginid;
    @FindBy(id = "autoform3_password")
    WebElement pass;
    @FindBy(id = "autoform3_confirmPassword")
    WebElement confirmpass;
    @FindBy(id = "autoform3_userName")
    WebElement uname;
    @FindBy(id = "userCode")
    WebElement ucode;
    @FindBy(id = "autoform3_email")
    WebElement uemail;
    @FindBy(id = "enumId")
    WebElement serv;
    @FindBy(id = "regreq")
    WebElement approval;
    @FindBy(id = "autoform3_merCategoryCode")
    WebElement ctname;
    @FindBy(id = "regConfirmInterval")
    WebElement billamount;
    @FindBy(id = "processType")
    WebElement procces;
    @FindBy(id = "billerType")
    WebElement billertype;
    @FindBy(id = "billAmount")
    WebElement billeramount;
    @FindBy(id = "partpaysubtype")
    WebElement subtype;
    @FindBy(id = "bankPaymentEffTo")
    WebElement paymenteffected;
    @FindBy(id = "autoform3_checkmfs")
    WebElement mfs;
    @FindBy(id = "0101seltcp")
    WebElement categoryCode;
    @FindBy(id = "autoform3_button_next")
    WebElement next;
    @FindBy(id = "autoform3_autoBillDelete")
    WebElement feq;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirm;

    public static BillerModificationInitiate_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        BillerModificationInitiate_pg1 page = PageFactory.initElements(driver, BillerModificationInitiate_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /*
     *
     * @FindBy(id = "autoform3_button_next") WebElement change;
     */
    public void NavigateToLink() {

        try {
            fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CMOD");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("link not found");
        }


    }


    public void clickonupdate(String text) {

        driver.findElement(By.xpath("//tr/td[contains(text(),'" + text + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
        Add.click();

    }

    public void logindetail(String login, String password) {
        loginid.clear();
        pass.clear();
        confirmpass.clear();
        loginid.sendKeys(login);
        pass.sendKeys(password);
        confirmpass.sendKeys(password);
    }

    public void adddvalues(String name, String code, String email, String fe) {

        loginid.clear();
        pass.clear();
        confirmpass.clear();
        uname.clear();
        //ucode.clear();
        uemail.clear();

        uname.sendKeys(name);
        //ucode.sendKeys(code);
        uemail.sendKeys(email);
        feq.sendKeys("");

    }

    public void Is_pproval_Req() throws InterruptedException {

        Thread.sleep(1000);
        approval.click();
        Thread.sleep(1000);
        billamount.clear();
        billamount.sendKeys("50");

    }

    public void selectdata(String servicelevel, String catname, String bprocc, String btype, String bamount, String bsubtype, String payment) throws InterruptedException {
        Select option = new Select(ctname);
        option.selectByVisibleText(catname);
        Select option2 = new Select(procces);
        option2.selectByVisibleText(bprocc);
        Select option3 = new Select(billertype);
        option3.selectByVisibleText(btype);
        Select option4 = new Select(billeramount);
        option4.selectByVisibleText(bamount);
        Select option5 = new Select(subtype);
        option5.selectByVisibleText(bsubtype);
        paymenteffected.click();
        // Thread.sleep(1000);
        Select option6 = new Select(paymenteffected);
        option6.selectByVisibleText(payment);

        Select option7 = new Select(serv);
        option7.selectByVisibleText(servicelevel);

    }

    public void walletandtype(String cattype) {
        if (!mfs.isSelected())
            mfs.click();

        Select option = new Select(categoryCode);
        option.selectByVisibleText(cattype);

    }

    public void next() {

        next.click();

    }

    public void confirms() {

        confirm.click();


    }
}
