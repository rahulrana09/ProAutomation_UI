package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class BillerRegisteration_pg2 extends PageInit {

    @FindBy(name = "action:addMerchant_addMoreLiquidationBank")
    WebElement addLiquidationBank;
    @FindBy(css = "[id='addMerchant_addMoreLiquidationBank_submit'][value='Submit']")
    WebElement btnSubmit;

    public BillerRegisteration_pg2(ExtentTest t1) {
        super(t1);
    }

    public void liquidationclick() throws Exception {
        clickOnElement(addLiquidationBank, "addLiquidationBank");
    }

    public void clickButtonSubmit() {
        clickOnElement(btnSubmit, "Button Submit");
    }

    public boolean isLiquidationFieldDisplayed() {
        return fl.elementIsDisplayed(addLiquidationBank);

    }

    public void selectLiquidationFrequency(String frequency, String day) throws Exception {
        driver.findElement(By.xpath("//tbody[@id='liquidationDetails']//td[2]/input[@value='" + frequency + "']")).click();
        pageInfo.info(frequency + " is selected");
        if (frequency.equals("Monthly")) {
            Select sel = new Select(driver.findElement(By.id("dateOfMonth")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
        if (frequency.equals("Weekly")) {
            Select sel = new Select(driver.findElement(By.id("dayOfWeek")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
        if (frequency.equals("Fortnightly")) {
            Select sel = new Select(driver.findElement(By.id("fortnightlyVar")));
            sel.selectByVisibleText(day);
            pageInfo.info(day + " is selected");
        }
    }

    public void addBankForBiller(Biller biller, boolean... isDefault) throws Exception {
        boolean isDefaultBank = isDefault.length > 0 ? isDefault[0] : false;

        List<String> liqProviderList = new ArrayList<>();
        if (isDefaultBank) {
            liqProviderList.add(DataFactory.getDefaultProvider().ProviderName);
        } else {
            liqProviderList = DataFactory.getAllProviderNames();
        }

        for (int i = 0; i < liqProviderList.size(); i++) {

            List<String> liquidationBankList = DataFactory.getAllLiquidationBanksLinkedToProvider(liqProviderList.get(i));

            liquidationclick();

            WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + i + "]"));
            WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[" + i + "]"));
            WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[" + i + "]"));
            WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[" + i + "]"));

            // UI elements


            WebElement providerSelect = driver.findElement(By.id("liquidationProvider" + i + ""));
            selectVisibleText(providerSelect, liqProviderList.get(i), "Provider");


            WebElement bankSelect = driver.findElement(By.id("liquidationBankListId" + i + ""));
            selectVisibleText(bankSelect, liquidationBankList.get(0), "Bank");


            String liqacnum = "" + DataFactory.getRandomNumber(9);
            if (liqProviderList.get(i).equals(DataFactory.getDefaultProvider().ProviderName)) {
                biller.setLiquidationBankAccountNumber(liqacnum);
            }
            setText(liqaccountNum, liqacnum, "liqaccountNum");


            String liqbranchname = "LiqBranch" + DataFactory.getRandomNumber(4);
            if (liqProviderList.get(i).equals(DataFactory.getDefaultProvider().ProviderName)) {
                biller.setLiquidationBankBranchName(liqbranchname);
            }
            setText(liqbranchnam, liqbranchname, "liquidationbranchname");


            String liqaccountholdername = "LiqAccount" + DataFactory.getRandomNumber(2);
            if (liqProviderList.get(i).equals(DataFactory.getDefaultProvider().ProviderName)) {
                biller.setAccountHolderName(liqaccountholdername);
            }
            setText(liqaccountholdernam, liqaccountholdername, "liquidationaccountholdername");


            String freqtradingname = "LiqBranchAccount" + DataFactory.getRandomNumber(4);
            if (liqProviderList.get(i).equals(DataFactory.getDefaultProvider().ProviderName)) {
                biller.setFrequencyTradingName(freqtradingname);
            }
            setText(freqtradingnam, freqtradingname, "frequencytradingname");

        }
        selectLiquidationFrequency(biller.LiquidationFrequency, biller.LiquidationDay);
    }

}
