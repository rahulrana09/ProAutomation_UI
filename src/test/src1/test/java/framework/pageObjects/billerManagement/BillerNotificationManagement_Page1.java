package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerNotificationManagement_Page1 extends PageInit {


    @FindBy(id = "addSystemNotification_input_add")
    WebElement addButton;
    @FindBy(id = "addSystemNotification_input_update")
    WebElement updateButton;

    public BillerNotificationManagement_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navBillerNotificationManagement() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_NOT");
        pageInfo.info("Navigate to Biller Notification Management Page!");
    }

    public void selectNotificationToUpdate(String billValue) {
        DriverFactory.getDriver().findElement(By.xpath("//input[@type='radio' and @value='" + billValue + "']")).click();
        pageInfo.info("Selecting Notification having code : " + billValue);
    }


    public void clickAddButton() {
        addButton.click();
        pageInfo.info("Click on Add button");
    }

    public void clickUpdateButton() {
        updateButton.click();
        pageInfo.info("Click on Update button");
    }


}
