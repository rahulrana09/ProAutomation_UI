package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerActivation_Page extends PageInit {

    @FindBy(id = "activateMerchant_input_button_activate")
    WebElement activateButton;
    @FindBy(id = "activateMerchant_inputAcivate_button_activate")
    WebElement initiateActivateButton;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirmButton;
    @FindBy(id = "autoform3_button_approve")
    WebElement approveButton;
    @FindBy(id = "activateMerchant_approveActivation_button_approve")
    WebElement approveActivateButton;

    public BillerActivation_Page(ExtentTest t1) {
        super(t1);
    }

    public void navBillerActivation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CACT");
        pageInfo.info("Navigate to Biller Activation Page!");
    }

    public void navBillerApprovalActivation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CACTAPRDM");
        pageInfo.info("Navigate to Biller Approval Activation Page!");
    }

    public void selectBillerToActivate(String billerCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td/input")).click();
        pageInfo.info("Selecting biller having code : " + billerCode);
    }


    public void clickActivateButton() {
        initiateActivateButton.click();
        pageInfo.info("Click on Activate button");
    }

    public void clickInitiateActivateButton() {
        initiateActivateButton.click();
        pageInfo.info("Click on Activate button");
    }

    public void clickApproveActivateButton() {
        approveActivateButton.click();
        pageInfo.info("Click on Activate button");
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }

    public void clickApproveButton() {
        approveButton.click();
        pageInfo.info("Click on Confirm button");
    }

}
