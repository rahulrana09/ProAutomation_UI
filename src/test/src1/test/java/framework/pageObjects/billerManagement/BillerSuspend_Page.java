package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class BillerSuspend_Page extends PageInit {


    @FindBy(id = "activateMerchant_inputSuspend_button_suspend")
    WebElement suspendButton;
    @FindBy(id = "activateMerchant_inputSuspend_button_suspend")
    WebElement suspendInitiateButton;
    @FindBy(id = "autoform3_button_confirm")
    WebElement confirmButton;
    @FindBy(id = "suspendMerchant_approveSuspend_button_approve")
    WebElement approve;
    @FindBy(id = "autoform3_button_approve")
    WebElement confirmApproveButton;

    public BillerSuspend_Page(ExtentTest t1) {
        super(t1);
    }

    public void navBillerSuspension() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CSUS");
        pageInfo.info("Navigate to Biller Suspension Page!");
    }

    public void navBillerSuspensionForApproval() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CSUSAPRDM");
        pageInfo.info("Navigate to Biller Suspension Page!");
    }

    public void selectBillerToSuspend(String billerCode) {
        DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + billerCode + "')]/ancestor::tr[1]/td/input")).click();
        pageInfo.info("Selecting biller having code : " + billerCode);
    }


    public void clickSuspendButton() {
        suspendButton.click();
        pageInfo.info("Click on Suspend button");
    }

    public void clickSuspendInitiateButton() {
        suspendInitiateButton.click();
        pageInfo.info("Click on Suspend Initiate button");
    }

    public void clickApproveButton() {
        approve.click();
        pageInfo.info("Click on Approve button");
    }

    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }

    public void clickConfirmApproveButton() {
        confirmApproveButton.click();
        pageInfo.info("Click on Confirm Approve button");
    }


}
