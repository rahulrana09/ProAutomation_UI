package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DeleteSubsBillerAssociation_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static DeleteSubsBillerAssociation_pg1 page;
    @FindBy(name = "providerId")
    WebElement selCurrencyProvider;
    @FindBy(name = "msisdn")
    WebElement txtSubsMsisdn;
    @FindBy(id = "delbillerReg_delbillRegNext_button_submit")
    WebElement btnSubmit;
    @FindBy(id = "delbillerReg_deleteBiller_submit")
    WebElement btnDelete;

    public static DeleteSubsBillerAssociation_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        page = PageFactory.initElements(driver, DeleteSubsBillerAssociation_pg1.class);
        fl = FunctionLibrary.init(t1);
        return page;
    }

    public DeleteSubsBillerAssociation_pg1 navDeleteSubsBillerAssociation() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_DBLREG");
        pageInfo.info("Navigate to Delete Subscriber Biller Association");
        return this;
    }

    public DeleteSubsBillerAssociation_pg1 selectProvider(String text) {
        Select sel = new Select(selCurrencyProvider);
        sel.selectByValue(text);
        pageInfo.info("Select Currency Provider - " + text);
        return this;
    }

    public DeleteSubsBillerAssociation_pg1 setSubscriberMsisdn(String text) {
        txtSubsMsisdn.sendKeys(text);
        pageInfo.info("Set the Subscriber MSISDN as - " + text);
        return this;
    }

    public DeleteSubsBillerAssociation_pg1 clickOnSubmit() {
        btnSubmit.click();
        pageInfo.info("Click on Button Submit");
        return this;
    }

    public DeleteSubsBillerAssociation_pg1 clickDeleteBiller() {
        btnDelete.click();
        pageInfo.info("Click on Button Delete");
        return this;
    }

    public DeleteSubsBillerAssociation_pg1 checkBill(String billCode) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + billCode + "')]/ancestor::tr[1]/td/input[@type='checkbox']")).click();
        pageInfo.info("Click on check box correcponding to Bill Acc Number - " + billCode);
        return this;
    }
}
