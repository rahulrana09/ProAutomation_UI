package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class BillerNotificationAssociation_Page2 extends PageInit {


    @FindBy(id = "notification_addNotification_inputNotificationCompanyCode")
    WebElement billerDdown;
    @FindBy(id = "notification_addNotification_inputNotificationName")
    WebElement notificationNameTbox;
    @FindBy(id = "notification_addNotification_inputNotificationType")
    WebElement notificationTypeDdown;
    @FindBy(id = "notification_addNotification_submit")
    WebElement submitButton;
    @FindBy(id = "notification_confirmNotification_confirm")
    WebElement confirmButton;
    @FindBy(id = "notification_updateNotification_statusId")
    WebElement updateStatusDdown;

    ///////////////////////////
    @FindBy(id = "notification_updateNotification_update")
    WebElement updateButton;
    @FindBy(id = "notification_updateNotification_inputNotificationName")
    WebElement updateNotificationName;
    @FindBy(id = "notification_confirmUpdateNotificationDetail_confirm")
    WebElement updateConfirmButton;

    public BillerNotificationAssociation_Page2(ExtentTest t1) {
        super(t1);
    }

    public void clickSubmitButton() {
        submitButton.click();
        pageInfo.info("Click on Submit button");
    }

    public void selectBiller(String biller) {
        Select select = new Select(billerDdown);
        select.selectByVisibleText(biller);
        pageInfo.info("Select Biller  : " + biller);
    }

    public void setNotificationName(String name) {
        notificationNameTbox.sendKeys(name);
        pageInfo.info("Set Notification name : " + name);
    }

    public void selectNotificationType(String notifnValue) {
        Select select = new Select(notificationTypeDdown);
        select.selectByValue(notifnValue);
        pageInfo.info("Select Biller  : " + notifnValue);
    }


    public void clickConfirmButton() {
        confirmButton.click();
        pageInfo.info("Click on Confirm button");
    }


    // Update Page Operations

    public void setUpdatedNotificationName(String name) {
        updateNotificationName.clear();
        updateNotificationName.sendKeys(name);
        pageInfo.info("Set Updated Notification name : " + name);
    }

    public void selectStatusByValue(String status) {
        Select select = new Select(updateStatusDdown);
        select.selectByValue(status);
        pageInfo.info("Select Status by value : " + status);
    }

    public void clickUpdateSubmitButton() {
        updateButton.click();
        pageInfo.info("Click on Submit button");
    }

    public void clickUpdateConfirmButton() {
        updateConfirmButton.click();
        pageInfo.info("Click on Submit button");
    }


}
