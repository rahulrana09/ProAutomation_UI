package framework.pageObjects.billerManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by rahul.rana on 9/19/2017.
 */
public class BillerApproveInitiate_pg1 extends PageInit {

    @FindBy(id = "autoform3_button_approve")
    WebElement btnApproveBiller;
    @FindBy(id = "autoform3_button_reject")
    WebElement rejectBillerButton;

    public BillerApproveInitiate_pg1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Biller Approve Initiate Page
     *
     * @throws NoSuchElementException when Link not found
     */
    public void navBillerApproveInitiate() throws Exception {
        fl.leftNavigation("UTIL_ALL", "UTIL_UTL_CAPP");
        pageInfo.info("Navigate to Biller Approve Initiate Page");
    }


    /**
     * click On Approve Or Reject
     *
     * @param bcode     Biller Code
     * @param isApprove Pass true if want to Approve and false if want to Reject
     */
    public void clickOnApproveOrReject(String bcode, boolean isApprove) {
        List<WebElement> links = driver.findElements(By.xpath("//tr/td[contains(text(),'" + bcode + "')]/ancestor::tr[1]/td/a"));
        if (isApprove) {
            links.get(0).click();
            Utils.putThreadSleep(3000);
            pageInfo.info("click on Approve Biller having biller code - " + bcode);
        } else {
            links.get(1).click();
            pageInfo.info("click on Reject Biller having biller code - " + bcode);
        }
    }

    /**
     * Click On Approve Button
     */
    public void clickApproveBiller() throws InterruptedException {
        clickOnElement(btnApproveBiller, "Approve Button");
    }

    public void clickRejectBillerButton() {
        clickOnElement(rejectBillerButton, "Reject Button");
    }
}
