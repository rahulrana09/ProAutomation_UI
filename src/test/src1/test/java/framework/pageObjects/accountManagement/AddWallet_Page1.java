package framework.pageObjects.accountManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class AddWallet_Page1 extends PageInit {


    //Page Objects
    @FindBy(name = "walletName")
    private WebElement walletNameTBox;

    @FindBy(name = "button.save")
    private WebElement saveButton;


    public AddWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate methods
     */
    public void navAddWalletLink() throws Exception {
        navigateTo("MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_ADD", "Multiple Wallet Management > Add Wallet");
    }

    public void setWalletName(String walletName) throws Exception {
        setText(walletNameTBox, walletName, "Wallet Name");
    }

    public void clickOnSaveButton() throws Exception {
        clickOnElement(saveButton, "Save Button");
    }

}
