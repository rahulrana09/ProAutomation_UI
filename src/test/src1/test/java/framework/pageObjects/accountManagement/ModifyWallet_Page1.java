package framework.pageObjects.accountManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class ModifyWallet_Page1 extends PageInit {


    @FindBy(id = "multipleWalletMgmtModify_input_submit")
    private WebElement updateButton;

    @FindBy(name = "updatedWalletName")
    private WebElement walletNameTBox;

    @FindBy(id = "multipleWalletMgmtModify_detailsOfSelectedWallet_button_save")
    private WebElement saveButton;


    public ModifyWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate methods
     */
    public void navModifyWalletLink() throws Exception {
        navigateTo("MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_MOD", "Multiple Wallet Management > Modify Wallet");
    }

    public void selectRadioButton(String walletID) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + walletID + "')]/ancestor::tr[1]/td/input[@type='radio']")).click();
    }

    public void clickOnUpdateButton() throws Exception {
        clickOnElement(updateButton, "Update Button");
    }

    public void setWalletName(String walletName) throws Exception {
        setText(walletNameTBox, walletName, "Wallet Name");
    }

    public void clickOnSaveButton() throws Exception {
        clickOnElement(saveButton, "Save Button");
    }

}
