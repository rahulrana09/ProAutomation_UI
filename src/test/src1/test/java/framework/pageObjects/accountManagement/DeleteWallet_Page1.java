package framework.pageObjects.accountManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class DeleteWallet_Page1 extends PageInit {
    @FindBy(id = "multipleWalletMgmtDelete_input_submit")
    private WebElement deleteButton;
    @FindBy(id = "multipleWalletMgmtDelete_detailsOfSelectedWallet_button_confirm")
    private WebElement confirmButton;

    public DeleteWallet_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navDeleteWalletLink() throws Exception {
        navigateTo("MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_DEL", "Multiple Wallet Management > Delete Wallet");
    }

    public boolean selectRadioButton(String walletID) {
        boolean found = false;
        List<WebElement> radioButton = driver.findElements(By.xpath("//tr/td[contains(text(),'" + walletID + "')]/ancestor::tr[1]/td/input[@type='radio']"));
        if (radioButton.size() > 0) {
            radioButton.get(0).click();
            found = true;
        }
        return found;
    }

    public void clickOnDeleteButton() {
        clickOnElement(deleteButton, "Delete Button");
    }

    public void clickOnConfirmButton() {
        clickOnElement(confirmButton, "Confirm Button");
    }
}
