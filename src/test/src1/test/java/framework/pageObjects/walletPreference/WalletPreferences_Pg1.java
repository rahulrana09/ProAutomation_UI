package framework.pageObjects.walletPreference;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.InstrumentTCP;
import framework.entity.User;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 8/17/2017.
 */
public class WalletPreferences_Pg1 extends PageInit {


    @FindBy(id = "domainCode")
    WebElement domainDdown;

    @FindBy(id = "preferenceType")
    WebElement selPrefType;

    @FindBy(id = "categoryCode")
    WebElement categoryDdown;

    @FindBy(id = "kycCode")
    WebElement RegType;

    @FindBy(name = "counterList[0].providerSelected")
    WebElement MFSProvider;

    @FindBy(id = "paymentMethodTypeId0")
    WebElement paymentInstrument;

    @FindBy(id = "walletTypeID0")
    WebElement walletTypeDdown;

    @FindBy(id = "grade0")
    WebElement gradeDdown;

    @FindBy(id = "walletRoles0")
    WebElement mobileRoleDdown;

    @FindBy(id = "tcp0")
    WebElement tcpDdown;

    @FindBy(name = "counterList[0].primaryAccountSelected")
    WebElement primaryAccDdown;

    @FindBy(name = "counterList[0].paymentTypeSelected")
    WebElement PayType;

    @FindBy(id = "susbcriberCatPreference_input_addMore")
    WebElement AddMore;

    @FindBy(id = "susbcriberCatPreference_addMoreWalletRows_addMore")
    WebElement AddMore1;

    @FindBy(id = "susbcriberCatPreference_input_submit")
    WebElement Next;

    @FindBy(id = "susbcriberCatPreference_createRows_submit")
    WebElement savingsClubNext;

    @FindBy(id = "susbcriberCatPreference_addMoreWalletRows_submit")
    WebElement Next1;
    @FindBy(id = "preferenceType")
    WebElement prefType;
    @FindBy(name = "preferenceType")
    private WebElement preferenceTypeDdown;

    public WalletPreferences_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static WalletPreferences_Pg1 init(ExtentTest t1) {
        return new WalletPreferences_Pg1(t1);
    }

    public void selectPreferenceTypeVyVal(String text) {
        selectValue(prefType, text, "Preference Type");
    }

    /**
     * Navigate to Wallet preferences
     *
     * @throws Exception
     */
    public void navMapWalletPreferences() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_CAT_PREFDM", "Map Wallet preferences");
    }

    /**
     * Click on Add More
     *
     * @throws Exception
     */
    public void clickAddMore() throws Exception {
        Thread.sleep(1200);
        if (fl.elementIsDisplayed(AddMore)) {
            clickOnElement(AddMore, "Add More");
        } else {
            clickOnElement(AddMore1, "Add More");
        }
    }

    /**
     * click on next
     */
    public void clickNext() {
        if (fl.elementIsDisplayed(Next)) {
            clickOnElement(Next, "Next");
        } else {
            clickOnElement(Next1, "Next");
        }
    }

    /**
     * select Domain Name
     *
     * @param domain
     * @throws Exception
     */
    public void selectDomainName(String domain) throws Exception {
        Utils.putThreadSleep(NumberConstants.SLEEP_2000);
        selectVisibleText(domainDdown, domain, "Domain");
    }

    /**
     * Select Categiory name
     *
     * @param category
     * @throws Exception
     */
    public void selectCategoryName(String category) throws Exception {
        selectVisibleText(categoryDdown, category, "Category");
    }

    /**
     * Select reg type
     *
     * @param reg
     * @throws Exception
     */
    public void selectRegType(String reg) throws Exception {
        selectVisibleText(RegType, reg, "Registration Type");
    }

    public void selectRegTypeByValue(String reg) throws Exception {
        selectValue(RegType, reg, "Registration Type");
    }

    public void selectPaymentInstrument(String paymntInstrument) throws Exception {
        if (fl.elementIsDisplayed(paymentInstrument))
            selectValue(paymentInstrument, paymntInstrument, "Payment Instrument");
    }

    public void selectProvider(String provider) throws Exception {
        selectVisibleText(MFSProvider, provider, "Provider");
    }

    public void selectWallet(String wallet) throws Exception {
        selectVisibleText(walletTypeDdown, wallet, "Wallet");
    }

    public void selectWalletType(String walletType) throws Exception {
        selectVisibleText(walletTypeDdown, walletType, "walletType");
    }

    public void selectWalletByValue(String walletID) throws Exception {
        selectValue(walletTypeDdown, walletID, "Wallet");
    }

    public void selectRegTypeByVal(String reg) throws Exception {
        selectValue(RegType, reg, "Registration Type");
    }

    public void selectGrade(String grade) throws Exception {
        selectVisibleText(gradeDdown, grade, "GradeName");
    }

    public void selectMobileRoleByIndex() throws Exception {
        Select sel = new Select(mobileRoleDdown);
        if (sel.getOptions().size() > 1) {
            selectIndex(mobileRoleDdown, 1, "Mobile Role");
        } else {
            selectIndex(mobileRoleDdown, 0, "Mobile Role");
        }
    }

    public void selectMobileRole(String mobRole) {
        selectValue(mobileRoleDdown, mobRole, "Mobile Role");
    }

    public void selectTCP(String TCP) {
        selectVisibleText(tcpDdown, TCP, "TCP");
    }

    public void selectTCPByIndex() throws Exception {
        Select sel = new Select(tcpDdown);
        fl.waitSelectOptions(sel);
        if (sel.getOptions().size() > 1) {
            selectIndex(tcpDdown, 1, "TCP");
        } else {
            selectIndex(tcpDdown, 0, "TCP");
        }
    }

    public void selectPrimaryAccount(boolean isPrimary) throws Exception {
        if (isPrimary) {
            selectVisibleText(primaryAccDdown, "Yes", "Primary Account");
        } else {
            selectVisibleText(primaryAccDdown, "No", "Primary Account");
        }

    }


    /**
     * Configure Payment Instrument
     * @param counter
     * @param provider
     * @param gradeName
     * @param instTypeCode
     * @param setIsPrimary
     * @param mobileRole
     * @param isWallet
     * @throws Exception
     */
    public void configurePaymentInstrumentMapping(int counter, String provider, String gradeName, String instTypeCode, boolean setIsPrimary, String mobileRole, boolean isWallet) throws Exception {

        try {
            if (counter > 0) {
                clickAddMore();
            }
            WebElement providerSelected = driver.findElement(By.name("counterList[" + counter + "].providerSelected"));
            WebElement paymentTypeSelected = driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"));
            WebElement channelGradeSelected = driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"));
            WebElement tcpSelected = driver.findElement(By.name("counterList[" + counter + "].tcpSelected"));
            WebElement selMobileRole = driver.findElement(By.name("counterList[" + counter + "].groupRoleSelected"));

            selectVisibleText(providerSelected, provider, "providerSelected");

            if (ConfigInput.isCoreRelease) {
                Utils.putThreadSleep(NumberConstants.SLEEP_2000);
                WebElement paymentMethodTypeId = driver.findElement(By.name("counterList[" + counter + "].paymentMethodTypeId"));
                if (isWallet) {
                    selectValue(paymentMethodTypeId, "WALLET", "Payment Method Type");
                } else {
                    selectValue(paymentMethodTypeId, "BANK", "Payment Method Type");
                }
            }

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectValue(paymentTypeSelected, instTypeCode, "paymentTypeSelected");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectVisibleText(channelGradeSelected, gradeName, "channelGradeSelected");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectVisibleText(selMobileRole, mobileRole, "Mobile Role");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectIndex(tcpSelected, 1, "tcpSelected");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            if (fl.elementIsDisplayed(primaryAccDdown)) {
                WebElement primaryAccountSelected = driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected"));

                if (setIsPrimary)
                    selectVisibleText(primaryAccountSelected, "Yes", "primaryAccountSelected");
                else
                    selectVisibleText(primaryAccountSelected, "No", "primaryAccountSelected");

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }

    }

    public int configureSVAWalletPref(int counter,
                                      String provider,
                                      String gradeName,
                                      String instTypeCode,
                                      boolean setIsPrimary,
                                      boolean isBank,
                                      String roleName) throws Exception {
        try {
            if (counter > 1) {
                clickAddMore();
            }

            WebElement providerSelected = driver.findElement(By.name("counterList[" + counter + "].providerSelected"));
            WebElement paymentTypeSelected = driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"));
            WebElement channelGradeSelected = driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"));
            WebElement tcpSelected = driver.findElement(By.name("counterList[" + counter + "].tcpSelected"));
            WebElement primaryAccountSelected = driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected"));
            WebElement paymentMethodTypeId = driver.findElement(By.name("counterList[" + counter + "].paymentMethodTypeId"));
            WebElement mobileRole = driver.findElement(By.name("counterList[" + counter + "].groupRoleSelected"));

            if (counter > 1)
                selectVisibleText(providerSelected, provider, "providerSelected");

            if (isBank) {
                selectValue(paymentMethodTypeId, "BANK", "paymentMethodTypeId");
            } else {
                selectValue(paymentMethodTypeId, "WALLET", "paymentMethodTypeId");
            }

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectValue(paymentTypeSelected, instTypeCode, "paymentTypeSelected");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectVisibleText(channelGradeSelected, gradeName, "channelGradeSelected");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectVisibleText(mobileRole, roleName, "Mobile RoleName");
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            selectIndex(tcpSelected, 1, "tcpSelected");


            if (setIsPrimary)
                selectVisibleText(primaryAccountSelected, "Yes", "primaryAccountSelected");
            else
                selectVisibleText(primaryAccountSelected, "No", "primaryAccountSelected");

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }
        return counter;
    }

    public void selectPreferenceType(String preftype) throws Exception {
        selectValue(prefType, preftype, "Preference Type");
    }

    public void checkElementEnabledOrDesabled() throws IOException {
        if (!fl.elementIsDisplayed(RegType)) {
            pageInfo.pass("Registration Type is Disabled.");
        } else {
            pageInfo.fail("Registration Type is Enabled.");
            List<String> values = fl.getOptions(RegType);
            for (String val : values) {
                pageInfo.info("Values present in Registration Type Drop down is : " + val);
            }
        }
        Utils.captureScreen(pageInfo);
    }


    public void clickNextForSavingsClub() {
        if (fl.elementIsDisplayed(savingsClubNext)) {
            clickOnElement(savingsClubNext, "Next");
        }
    }


}
