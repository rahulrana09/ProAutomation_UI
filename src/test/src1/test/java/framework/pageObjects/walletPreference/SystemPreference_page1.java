package framework.pageObjects.walletPreference;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Dalia on 15-09-2017.
 */
public class SystemPreference_page1 extends PageInit {

    @FindBy(xpath = "//table[@class = 'wwFormTableC']")
    WebElement element;
    @FindBy(id = "_submit")
    private WebElement submit;
    @FindBy(id = "preferences_modify_submit")
    private WebElement perferenceModifySubmit;
    @FindBy(id = "grouprole_forward_button_update")
    private WebElement V5_update;
    @FindBy(xpath = "//td[contains(text(),'Normal')]/..//input[@type='radio']")
    private WebElement V5_groupRoleName;
    @FindBy(xpath = "//input[@value='Delete']")
    private WebElement V5_deleteButton;
    @FindBy(id = "grouprole_modify_button_update")
    private WebElement V5_update_GroupRole_Modification;


    public SystemPreference_page1(ExtentTest t) {
        super(t);
    }

    public static SystemPreference_page1 init(ExtentTest t) {
        return new SystemPreference_page1(t);
    }

    public void navigateToSystemPreferencePage() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_PREF001", "Preferences >  System Preferences Page");
    }

    public void navigateTGroupRoleManagementPage() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_GRP_ROL", "Preferences >  Group Role");
    }

    public void V5_updateButton() {
        clickOnElement(V5_update, "V5_update");
    }

    public void V5_deleteButton() {
        clickOnElement(V5_deleteButton, "V5_delete");
    }

    public void V5_grouproleradioButton() {
        clickOnElement(V5_groupRoleName, "V5_groupRoleName");
    }

    public void clickSubmit() {
        Utils.scrollToBottomOfPage();
        clickOnElement(submit, "submit");
    }

    //=============used in p1=============//

    public void clickPreferenceModifySubmit() {
        clickOnElement(perferenceModifySubmit, "perferenceModifySubmit");
        Utils.putThreadSleep(5000);
    }

    public void V5_update_GroupRole_Modification() {
        clickOnElement(V5_update_GroupRole_Modification, "V5_update_GroupRole_Modification");
    }

    public void uncheckArole(String roleCode) {
        WebElement checkBox = driver.findElement(By.xpath("//input[@value='" + roleCode + "' and @type='checkbox']"));
        clickOnElement(checkBox, "RoleCode:" + roleCode);
    }

    public boolean isUpdatedPreferenceExists(String preference) {
        return isStringPresentInWebTable(element, preference);
    }

    public List<WebElement> isPreferenceListExists() {
        List<WebElement> preference = driver.findElements(By.xpath("//table[@class = 'wwFormTableC']/tbody/tr/td[2]"));

        return preference;
    }

    public void clickAndUpdatePreference(String preference, String value) {
        driver.findElement(By.xpath("//input[@value='" + preference + "']")).click();
        WebElement element = driver.findElement(By.xpath("//input[@value='" + preference + "']/ancestor::tr[1]/td/input[@type='text']"));
        element.clear();
        element.sendKeys(value);
    }
}
