/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Page for perform CashIN
*/
package framework.pageObjects.cashInCashOut;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class CashOut_page1 extends PageInit {

    @FindBy(id = "cashOutPay_updateDetail")
    WebElement tableTransaction;
    @FindBy(id = "channelProviderList")
    WebElement channelUserprovider;
    @FindBy(id = "channelWalletList")
    WebElement channelUserwallet;
    @FindBy(name = "paymentMethodNumber")
    WebElement subscriberMsisdn;
    @FindBy(id = "partyProviderListSel")
    WebElement provider;
    @FindBy(id = "partyWalletListSel")
    WebElement subscriberWallet;
    @FindBy(id = "cashOutWeb_confirm_amount")
    WebElement amountTextBox;
    @FindBy(id = "cashOutWeb_confirm_idNumber")
    WebElement identificationNumber;
    @FindBy(id = "cashOutWeb_confirm_referenceNumber")
    WebElement paymentId;
    @FindBy(id = "cashOutWeb_confirm_button_submit")
    WebElement submit;
    @FindBy(id = "cashButt")
    WebElement confirm;
    @FindBy(id = "serviceTypeId")
    WebElement serviceType;
    @FindBy(id = "cashOutPay_cashOutPayment_button_next")
    WebElement nextBtn;
    @FindBy(id = "cashOutPay_updateDetail_blacklist_button_confirm")
    WebElement confirmApproval;
    @FindBy(id = "cashOutPay_updateDetail_button_reject")
    WebElement rejectApproval;
    @FindBy(id = "channelProviderList")
    private WebElement channelUserProvider;
    @FindBy(id = "channelWalletList")
    private WebElement channelUserWallet;
    @FindBy(name = "paymentMethodNumber")
    private WebElement subscriberMSISDN;
    @FindBy(id = "partyProviderListSel")
    private WebElement providerList;
    @FindBy(id = "cashOutWeb_confirm_button_submit")
    private WebElement submitBtn;


    public CashOut_page1(ExtentTest t1) {
        super(t1);
    }

    public static CashOut_page1 init(ExtentTest t1) {
        return new CashOut_page1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("MTRANS_ALL", "MTRANS_COUT_WEB", "Cash Out Transaction Approval Page");
    }

    public void subscriberWallet_SelectText(String text) {
        selectVisibleText(subscriberWallet, text, "subscriberWallet");
    }

    public void channelUserprovider_SelectText(String text) {
        selectVisibleText(channelUserprovider, text, "channelUserprovider");
    }

    public void channelUserwallet_SelectText(String text) {
        selectVisibleText(channelUserwallet, text, "channelUserwallet");
    }

    public void channelUserprovider_SelectValue(String text) {
        selectValue(channelUserprovider, text, "channelUserprovider");
    }

    public void subscriberMSISDNSetText(String msisdn) {
        setText(subscriberMSISDN, msisdn, "subscriberMSISDN");
    }

    public void channelUserwallet_SelectValue(String text) {
        selectValue(channelUserwallet, text, "channelUserwallet");
    }

    public void provider_SelectText(String text) {
        provider.click();
        selectVisibleText(provider, text, "provider");
    }

    public void amountSetText(String amount) {
        setText(amountTextBox, amount, "amountTextBox");
    }

    public void subscriberMsisdn_SetText(String text) {
        setText(subscriberMsisdn, text, "MSISDN");
    }

    public void subscriberMsisdn_SelectValue(String text) {
        selectValue(subscriberMsisdn, text, "subscriberMsisdn");
    }

    public void identificationNumberSetText(String text) {
        setText(identificationNumber, text, "identificationNumber");
    }

    public void provider_SelectValue(String text) {
        selectValue(provider, text, "provider");
    }

    public void subscriberWallet_SelectValue(String text) {
        selectValue(subscriberWallet, text, "subscriberWallet");
    }

    public void amount_SetText(String text) {
        setText(amountTextBox, text, "amountTextBox");
    }

    public void identificationNumber_SetText(String text) {
        setText(identificationNumber, text, "identificationNumber");
    }

    public void paymentId_SetText(String text) {
        setText(paymentId, text, "paymentId");
    }

    public void submit_Click() {
        clickOnElement(submit, "Submit");
    }

    public void confirm_Click() {
        clickOnElement(confirm, "Confirm");
    }

    public CashOut_page1 navigateToTransactionsApproval() throws Exception {
        navigateTo("MTRANS_ALL", "MTRANS_COUT_DISP", "Transaction Approval");
        return this;
    }

    public CashOut_page1 select_ServiceName(String text) throws Exception {
        selectValue(serviceType, "CASHOUT", "Service Type");
        clickOnElement(nextBtn, "Next Button");
        return this;
    }

    public CashOut_page1 selectTransactionForAction(String transactionId, boolean isApprove) throws Exception {
        wait.until(ExpectedConditions.elementToBeClickable(tableTransaction.findElement(By.xpath("//input[contains(@value,'" + transactionId + "')]")))).click();

        if (isApprove) {
            clickOnElement(confirmApproval, "Confirm Approval");
        } else {
            clickOnElement(rejectApproval, "Reject Approval");
        }
        return this;
    }

    public boolean isTransactionAvailable(String transactionId) throws Exception {
        return isStringPresentInWebTable(tableTransaction, transactionId);
    }


}
