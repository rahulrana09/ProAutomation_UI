/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Page for perform CashIN
*/

package framework.pageObjects.cashInCashOut;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class CashIn_page1 extends PageInit {


    @FindBy(id = "requestorMsisdn")
    public WebElement senderMSISDN;
    @FindBy(id = "channelProviderList")
    private WebElement channelUserProvider;
    @FindBy(id = "channelWalletList")
    private WebElement channelUserWallet;
    @FindBy(name = "paymentMethodNumber")
    private WebElement SubscriberMSISDN;
    @FindBy(id = "partyProviderListSel")
    private WebElement providerList;
    @FindBy(id = "partyWalletListSel")
    private WebElement SubscriberWallet;
    @FindBy(id = "selectForm_amount")
    private WebElement amountTBox;
    @FindBy(id = "selectForm_idNumber")
    private WebElement identificationNumber;
    @FindBy(id = "selectForm_referenceNumber")
    private WebElement paymentNoTBox;
    @FindBy(id = "requestorName")
    private WebElement senderName;

    @FindBy(id = "selectForm_button_submit")
    private WebElement submitButton;

    @FindBy(id = "cashButt")
    private WebElement confirmButton;

    @FindBy(id = "cashInWeb_makePayment_button_back")
    private WebElement backButton;


    public CashIn_page1(ExtentTest t1) {
        super(t1);
    }

    public static CashIn_page1 init(ExtentTest t1) {
        return new CashIn_page1(t1);
    }

    public CashIn_page1 navigateToLink() throws Exception {
        navigateTo("MTRANS_ALL", "MTRANS_CIN_WEB", "Cash In Link");
        return this;
    }

    /**
     * Page Method to Select User Provider
     *
     * @param provider
     * @return
     */
    public CashIn_page1 channelUserProviderSelectText(String provider) {
        selectVisibleText(channelUserProvider, provider, "channelUserProvider");
        return this;
    }

    /**
     * Method to Select wallet value
     *
     * @param wallet
     * @return
     */
    public CashIn_page1 subscriberWalletSelectText(String wallet) {
        selectVisibleText(SubscriberWallet, wallet, "SubscriberWallet");
        return this;
    }

    /**
     * Method to Select wallet value
     *
     * @param msisdn
     * @return
     */
    public CashIn_page1 subscriberMSISDNSetText(String msisdn) {
        setText(SubscriberMSISDN, msisdn, "SubscriberMSISDN");
        return this;
    }

    /**
     * Method to Select Provider by value
     *
     * @param provider
     * @return
     */
    public CashIn_page1 selectPartyProviderName(String provider) {
        clickOnElement(providerList, "providerList");
        selectVisibleText(providerList, provider, "providerList");
        return this;
    }

    /**
     * method to Set Identification Number
     *
     * @param IDNumber
     * @return
     */
    public CashIn_page1 identificationNumberSetText(String IDNumber) {
        setText(identificationNumber, IDNumber, "identificationNumber");
        return this;
    }

    /**
     * method to Set Sender name
     *
     * @param sender
     * @return
     */
    public CashIn_page1 senderNameSetText(String sender) {
        setText(senderName, sender, "senderName");
        return this;
    }

    /**
     * method to Set payment Number
     *
     * @param paymentNo
     * @return
     */
    public CashIn_page1 paymentNoSetText(String paymentNo) {
        setText(paymentNoTBox, paymentNo, "paymentNoTBox");
        return this;
    }

    /**
     * method to Set Sender MSISDN
     *
     * @param msisdn
     * @return
     */
    public CashIn_page1 senderMSISDNSetText(String msisdn) {
        if (Utils.checkElementPresent("requestorMsisdn", Constants.FIND_ELEMENT_BY_ID)) {
            setText(senderMSISDN, msisdn, "senderMSISDN");
        }
        return this;
    }

    /**
     * method to Select Channel User Wallet
     *
     * @param wallet
     * @return
     */
    public CashIn_page1 channelUserWalletSelectText(String wallet) {
        selectVisibleText(channelUserWallet, wallet, "channelUserWallet");
        return this;
    }

    /**
     * method to Select Channel User Wallet
     *
     * @param amount
     * @return
     */
    public CashIn_page1 amountSetText(String amount) {
        setText(amountTBox, amount, "amountTBox");
        return this;
    }

    /**
     * Method to click on Submit button
     *
     * @return
     */
    public CashIn_page1 clickSubmit() {
        clickOnElement(submitButton, "submitButton");
        return this;
    }

    /**
     * Method to click on Confirm button
     *
     * @return
     */
    public CashIn_page1 clickConfirmButton() {
        if (ConfigInput.isConfirm) {
            confirmButton.click();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            pageInfo.info("Click on Confirm Button.");
        }
        return this;
    }

    /**
     * Method to click on back button
     *
     * @return
     */
    public void clickBackButton() {
        clickOnElement(backButton, "backButton");
        Utils.putThreadSleep(2000);
    }

    /**
     * Method to check element disabled or not
     *
     * @return
     */
    public boolean checkElementDisabled() {
        if (channelUserProvider.isEnabled()) {
            return true;
        } else
            return false;
    }

    public CashIn_page1 providerSelectValue(String provider) {
        providerList.click();
        Select s_channelUserProvider = new Select(providerList);
        s_channelUserProvider.selectByVisibleText(provider);
        pageInfo.info("Select Provider : " + provider);
        return this;
    }
}
