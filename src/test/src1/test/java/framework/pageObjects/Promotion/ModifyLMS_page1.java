package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ModifyLMS_page1 extends PageInit {

    public ModifyLMS_page1(ExtentTest t1) {
        super(t1);
    }

    public static ModifyLMS_page1 init(ExtentTest t1) {

        return new ModifyLMS_page1(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */

    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_MC_PRO", "Modify");
    }

    public void selectprofile(String profilename) {
        String xpath = "//*[contains(text(), '" + profilename + "')]/following::a[contains(text(), 'Modify')]";
        WebElement profile = driver.findElement(By.xpath(xpath));
        profile.click();
        pageInfo.info("SELECT LMS PROFILE " + profilename);
        driver.switchTo().alert().accept();
        pageInfo.info("Accept Alert Message");

    }

}
