package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ResumeLMS_Page extends PageInit {

    @FindBy(id = "modify_saveConfirm_button_confirm")
    WebElement confirm;

    public ResumeLMS_Page(ExtentTest t1) {
        super(t1);
    }

    public static ResumeLMS_Page init(ExtentTest t1) {
        return new ResumeLMS_Page(t1);
    }

    public void clickonConfirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void navigateToResumePromotion() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_MC_PRO", "Resume");
    }

    public void selectprofile(String profilename) {
        String xpath = "//*[contains(text(), '" + profilename + "')]/following::a[contains(text(), 'Resume')]";
        WebElement profile = driver.findElement(By.xpath(xpath));
        profile.click();
        pageInfo.info("Select LMS profile To resumeLMSProfile it ");
        Assertion.alertAssertion("promotion.confirmation.resume", pageInfo);
    }

}
