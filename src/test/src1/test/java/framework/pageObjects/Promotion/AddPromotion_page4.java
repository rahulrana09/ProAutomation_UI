package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AddPromotion_page4 extends PageInit {


    @FindBy(id = "create_save_button_confirm")
    WebElement confirm;

    public AddPromotion_page4(ExtentTest t1) {
        super(t1);
    }

    public static AddPromotion_page4 init(ExtentTest t1) {
        return new AddPromotion_page4(t1);
    }

    public void clickOnConfrim() {
        clickOnElement(confirm, "Confirm");
    }


}
