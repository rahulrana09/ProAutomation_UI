package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class ApproveLMS_page1 extends PageInit {

    public ApproveLMS_page1(ExtentTest t1) {
        super(t1);
    }

    public static ApproveLMS_page1 init(ExtentTest t1) {
        return new ApproveLMS_page1(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */

    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_AC_PRO", "Approve LMS");
    }

    public void selectProfile(String profilename) {
        String xpath = "//*[contains(text(), '" + profilename + "')]/following::a";
        WebElement profile = driver.findElement(By.xpath(xpath));
        profile.click();
        pageInfo.info("Select Profile");
    }

}
