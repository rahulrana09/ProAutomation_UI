package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyLMS_page4 extends PageInit {
    @FindBy(id = "selectForm_button_save")
    WebElement next;
    @FindBy(id = "delete2")
    WebElement delete;
    @FindBy(id = "modify_save_button_confirm")
    WebElement modify;
    public ModifyLMS_page4(ExtentTest t1) {
        super(t1);
    }

    public static ModifyLMS_page4 init(ExtentTest t1) {

        return new ModifyLMS_page4(t1);
    }

    public void clickmodify() {
        clickOnElement(modify, "MODIFY");
    }

    public void clickonsave() {
        clickOnElement(next, "Next");
    }

    public void clickondelete() {
        clickOnElement(delete, "Delete");
    }

}
