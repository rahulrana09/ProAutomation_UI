package framework.pageObjects.Promotion;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ApproveLMS_page2 extends PageInit {

    @FindBy(id = "_button_approve")
    WebElement approve;

    public ApproveLMS_page2(ExtentTest t1) {
        super(t1);
    }

    public static ApproveLMS_page2 init(ExtentTest t1) {
        return new ApproveLMS_page2(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */

    public void clickOnApprove() {
        clickOnElement(approve, "Approve button");
    }

}
