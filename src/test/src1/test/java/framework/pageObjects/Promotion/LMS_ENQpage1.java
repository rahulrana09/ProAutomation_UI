package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by prashant.kumar on 11/3/2017.
 */
public class LMS_ENQpage1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "userType")
    WebElement UserType;
    @FindBy(id = "accessId")
    WebElement Msisdn;
    @FindBy(name = "partyProviderName")
    WebElement Provider;
    @FindBy(id = "userLoyaltySelfEnquiry_authenticateUser_partyProviderName")
    WebElement SelfProvider;
    @FindBy(id = "userLoyaltyEnquiry_showLoyaltyBalance_button_submit")
    WebElement Submit;
    @FindBy(id = "userLoyaltySelfEnquiry_authenticateUser_button_submit")
    WebElement SelfSubmit;
    @FindBy(id = "userLoyaltySelfEnquiry_showLoyaltyBalance_password")
    WebElement Password;
    @FindBy(id = "userLoyaltySelfEnquiry_showLoyaltyBalance_button_submit")
    WebElement PasswordSubmit;

    public static LMS_ENQpage1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        LMS_ENQpage1 page = PageFactory.initElements(driver, LMS_ENQpage1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public LMS_ENQpage1 NavigateToLink() throws Exception {
        fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_ENQ_LUS");
        pageInfo.info("Navigate to  Channel User/Subscriber Loyalty Enquiry  Link");
        return this;
    }

    public LMS_ENQpage1 NavigateToSelfBalanceEnquiryLink() throws Exception {
        fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_ENQ_LUSC");
        pageInfo.info("Navigate to  Loyalty Self Balance Enquiry");
        return this;
    }


    public LMS_ENQpage1 selectUserType(String userType) {
        Select option = new Select(UserType);
        option.selectByValue(userType);
        pageInfo.info("Select User Type");
        return this;
    }

    public LMS_ENQpage1 setMsisdn(String value) {
        Msisdn.sendKeys(value);
        pageInfo.info("Enter user no " + value);
        return this;
    }

    public LMS_ENQpage1 selectProvider()  {
        Provider.click();
        pageInfo.info("Select Provider ID");
        return this;
    }

    public LMS_ENQpage1 selectSelfProvider() throws InterruptedException {

        Thread.sleep(1000);
        Select option = new Select(SelfProvider);
        option.selectByValue(DataFactory.getDefaultProvider().ProviderId);
        pageInfo.info("Select Provider ID");

        return this;
    }

    public LMS_ENQpage1 clickOnSubmitButton() {
        Submit.click();
        pageInfo.info("Click on submit button");
        return this;
    }

    public LMS_ENQpage1 clickOnSelfSubmitButton() {
        SelfSubmit.click();
        pageInfo.info("Click on submit button");
        return this;
    }

    public LMS_ENQpage1 clickOnPassword(String password) {
        Password.sendKeys(password);
        pageInfo.info("Click on Password");
        return this;
    }

    public LMS_ENQpage1 clickOnPasswordSubmitButton() {
        PasswordSubmit.click();
        pageInfo.info("Click on Passwordsubmit button");
        return this;
    }

    public void Verify() {
        //String

        pageInfo.info("");
    }


}
