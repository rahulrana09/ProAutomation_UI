package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.List;


public class ViewPromotion_page extends PageInit {

    @FindBy(id = "applicableFromStr")
    WebElement from;
    @FindBy(id = "applicableToStr")
    WebElement to;


    public ViewPromotion_page(ExtentTest t1) {
        super(t1);
    }

    public static ViewPromotion_page init(ExtentTest t1) {
        return new ViewPromotion_page(t1);
    }

    /**
     * navigate for the Network Admin Page
     *
     * @throws Exception
     */


    public void NavigateNA() throws Exception {
        fl.leftNavigation("PROMOTION_ALL", "PROMOTION_VIEW_PRO");
        pageInfo.info("Navigate To Link ");
    }


    public void sendTodate(String text) {
        removeto();
        to.clear();
        to.sendKeys(text);
        pageInfo.info("Enter To date");
    }


    public void sendFromdate(String text) {
        removefrom();
        from.clear();
        from.sendKeys(text);
        pageInfo.info("Enter From date");
    }


    public void clickonnextbutton() throws InterruptedException, IOException {
        driver.findElement(By.id("create_show_btnSearch")).click();
        pageInfo.info("Click on search button");
        Thread.sleep(2000);
        List<WebElement> profile = driver.findElements(By.xpath("//*[@id='content']/form/table/tbody/tr[2]/td[2]"));
        List<WebElement> version = driver.findElements(By.xpath("//*[@id='content']/form/table/tbody/tr[2]/td[3]"));
        pageInfo.info("No of LMS version Found" + version.size());
        Boolean isFound = false;
        for (WebElement e : version) {
            if (e.getText().equalsIgnoreCase("2")) {
                isFound = true;
            }
        }
        if (isFound) {
            pageInfo.pass("Second Version is Found");
        } else {
            pageInfo.fail("Second Version is Not Found");
        }
        Assertion.attachScreenShotAndLogs(pageInfo);

    }

    public void removefrom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('applicableFromStr').removeAttribute('readonly');");
    }


    public void removeto() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('applicableToStr').removeAttribute('readonly');");
    }

}
