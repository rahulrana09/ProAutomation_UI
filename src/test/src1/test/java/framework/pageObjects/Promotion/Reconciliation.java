package framework.pageObjects.Promotion;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Reconciliation extends PageInit {


    @FindBy(xpath = "//label[contains(text(),'Total Reward Account Stock (A):')]/../..//label[@id='reconciliation_loadReconciliation_totalRewardStock']")
    WebElement RAstock;
    @FindBy(xpath = "//label[contains(text(),'Total Loyalty Stock based')]/../..//label[@id='reconciliation_loadReconciliation_totalLoyaltyStock']")
    WebElement conversionFac;
    @FindBy(xpath = "(//label[contains(text(),'Loyalty Points for IND09')]/../..//label[@id='reconciliation_loadReconciliation_balanceIND09'])[1]")
    WebElement IND09bal;
    @FindBy(xpath = "(//td[@class='tableft'])[last()]")
    WebElement reconciliationResult;
    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_providerName")
    WebElement provider;
    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_button_submit")
    WebElement clickSubmit;
    @FindBy(id = "reconciliation_loadReconciliation_reconciliationMap_netBalance")
    WebElement summaryOfBalance;

    public Reconciliation(ExtentTest t1) {
        super(t1);
    }

    public static Reconciliation init(ExtentTest t1) {
        pageInfo = t1;
        return new Reconciliation(t1);
    }

    public Reconciliation clickSubmit() {
        clickOnElement(clickSubmit, "Submit Button");
        return this;
    }

    public String getReconciliationBalance() {
        return summaryOfBalance.getText();
    }

    public boolean viewReconciliation() {
        return summaryOfBalance.isDisplayed();
    }

    public Reconciliation selectProvider(String providerID) {
        selectValue(provider, providerID, "provider");
        return this;
    }


    public void NavigateNA() throws Exception {
        navigateTo("PROMOTION_ALL", "PROMOTION_RECON_PRODM", "Reconciliation");
    }

    public Reconciliation navReconciliation() throws Exception {
        navigateTo("MNREC_ALL", "MNREC_MN_REC", "Reconciliation");
        return this;
    }

    public String convertedAmount() throws Exception {
        String convertedAmount = conversionFac.getText();
        pageInfo.info("Loyalty stock based on conversion factor is " + convertedAmount);
        return convertedAmount;
    }

    public String loyaltyStock() throws Exception {
        String raStock = RAstock.getText();
        pageInfo.info("Total Loyalty stock " + raStock);
        return raStock;
    }

    public String ind09Balance() throws Exception {
        String ind09Bal = IND09bal.getText();
        pageInfo.info("Balance in IND09 wallet is " + ind09Bal);
        return ind09Bal;
    }

    public String result() throws Exception {
        String value = reconciliationResult.getText();
        String mismatchResult = value.split("Reconciled ")[1].trim();
        pageInfo.info("reconciliation result " + mismatchResult);
        return mismatchResult;
    }
}
