package framework.pageObjects.bulkUserRegnAndModification;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 25-Oct-17.
 */
public class BulkUserRegnAndModification_Page1 extends PageInit {

    @FindBy(id = "uplFile")
    private WebElement uploadFileWebElement;

    @FindBy(id = "submitButton")
    private WebElement submitButton;

    @FindBy(xpath = "//a[contains(@href,'javaScript:openLogFile()')]")
    private WebElement imgLogDownload;

    public BulkUserRegnAndModification_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToBulkUsrRegnAndModifn() throws Exception {
        fl.leftNavigation("BULK_ALL", "BULK_BLK_CHUSR");
        pageInfo.info("Navigate to Bulk User Registration and Modification Page");
    }

    public void clickSubmit() {
        submitButton.click();
        pageInfo.info("Clicked on Submit Button");
    }

    public void uploadFile(String fileName) {
        uploadFileWebElement.sendKeys(fileName);
        pageInfo.info("Uploading file :" + fileName);
    }


    public void downloadErrorLogs() throws Exception {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_USER_REGISTER);
        try {
           // FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_USER_REGISTER);
            imgLogDownload.click();
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Click On image Download template");
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(2000);
            //Added by Navin to capture the log file in Report
            Utils.attachFileAsExtentLog(FilePath.dirFileDownloads+Utils.getLatestFilefromDir(FilePath.dirFileDownloads),
                    pageInfo);

        } finally {
            Utils.closeUntitledWindows();
        }

    }

}
