package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class GroupRole_Page1 extends PageInit {


    /***************************************************************************
     * Page Objects
     ***************************************************************************/
    @FindBy(name = "domainCode")
    public WebElement DomainName;

    @FindBy(name = "categoryCode")
    public WebElement CategoryName;

    @FindBy(name = "gradeCode")
    public WebElement GradeName;

    @FindBy(id = "webGroupRole1")
    public WebElement RadioWebGroupRole;

    @FindBy(id = "grouprole_forward_role2")
    public WebElement RadioMobileGroupRole;

    @FindBy(id = "grouprole_forward_submit")
    public WebElement Submit;

    @FindBy(id = "grouproleview_forward_submit")
    public WebElement viewSubmit;
    @FindBy(id = "grouprole_forward_submit")
    private WebElement V5_submit;

    public GroupRole_Page1(ExtentTest t1) {
        super(t1);
    }

    public static GroupRole_Page1 init(ExtentTest t1) {
        return new GroupRole_Page1(t1);
    }

    public static void verifySectionOnGroupRoleManagementPage() throws IOException {

        boolean domCodePresent = Utils.checkElementPresent("domCode", Constants.FIND_ELEMENT_BY_ID);
        boolean catCodePresent = Utils.checkElementPresent("catCode", Constants.FIND_ELEMENT_BY_ID);
        boolean webGroupRole1Present = Utils.checkElementPresent("webGroupRole1", Constants.FIND_ELEMENT_BY_ID);
        boolean grouprole_forward_role2Present = Utils.checkElementPresent("grouprole_forward_role2", Constants.FIND_ELEMENT_BY_ID);


        Assertion.verifyEqual(domCodePresent, true, "Checking Presence of - Domain Code drop down", pageInfo);
        Assertion.verifyEqual(catCodePresent, true, "Checking Presence of - Category Code drop down", pageInfo);
        Assertion.verifyEqual(webGroupRole1Present, true, "Checking Presence of - Web Group Role ", pageInfo);
        Assertion.verifyEqual(grouprole_forward_role2Present, true, "Checking Presence of - Mobile Group Role ", pageInfo);

    }

    public void clickOnSubmitView() {
        clickOnElement(viewSubmit, "viewSubmit");
    }

    public void V5_submit() {
        clickOnElement(V5_submit, "V5_submit");
    }

    /**
     * Navigate to Add Web Group role Page
     *
     * @throws Exception
     */
    public GroupRole_Page1 navAddWebGroupRole() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_GRP_ROL", "Group Role Page");
        return this;
    }

    /**
     * Select Domain Name
     *
     * @param domainName
     */
    public GroupRole_Page1 selectDomainName(String domainName) {
        selectVisibleText(DomainName, domainName, "DomainName");
        return this;
    }

    /**
     * Select Category Name
     *
     * @param categoryName
     */
    public GroupRole_Page1 selectCategoryName(String categoryName) {
        selectVisibleText(CategoryName, categoryName, "CategoryName");
        return this;
    }

    /**
     * Select Grade Name
     *
     * @param gradeName
     */
    public GroupRole_Page1 selectGradeName(String gradeName) {
        selectVisibleText(GradeName, gradeName, "GradeName");
        return this;
    }

    /**
     * Select Grade Name By Index
     */
    public GroupRole_Page1 selectGradeNameByIndex() {
        selectIndex(GradeName, 0, "GradeName");
        return this;
    }

    /**
     * Check Web Group Role
     *
     * @return
     */
    public boolean checkWebGroupRole() {
        if (RadioWebGroupRole.isEnabled()) {
            clickOnElement(RadioWebGroupRole, "Radio WebGroupRole");
            return true;
        } else {
            pageInfo.info("WebRole is not enabled");
            return false;
        }
    }

    /**
     * Check Mobile Group Role
     *
     * @return
     */
    public boolean checkMobileGroupRole() {
        if (RadioMobileGroupRole.isEnabled()) {
            clickOnElement(RadioMobileGroupRole, "RadioMobileGroupRole");
            return true;
        } else {
            pageInfo.info("Mobile Role is not enabled");
            return false;
        }
    }

    /**
     * Submit
     */
    public void submit() {
        clickOnElement(Submit, "Submit");
    }

    /**
     * Get all the domain names from the UI
     */
    public List<String> getAllDomainNames() {
        return fl.getOptionText(DomainName);
    }

    /**
     * Get all the category names from the UI
     */
    public List<String> getAllCategoryNames() {
        return fl.getOptionText(CategoryName);
    }

    /**
     * Get all the Grade names from the UI
     */
    public List<String> getAllGradeNames() {
        return fl.getOptionText(GradeName);

    }
}
