package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.MobileGroupRole;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by rahul.rana on 5/8/2017.
 */
public class GroupRole_Page2 extends PageInit {

    /*
     * Page Objects
     */
    @FindBy(id = "grouprole_forward_button_add")
    private WebElement Add;
    @FindBy(id = "grouprole_forward_button_update")
    private WebElement Update;
    @FindBy(name = "action:grouprole_duplicate")
    private WebElement btnDuplicate;
    @FindBy(id = "grouprole_forward_button_view")
    private WebElement ViewDetails;
    @FindBy(className = "wwFormTableC")
    private WebElement webTable;
    @FindBy(css = "input[type='button'][value='Delete']")
    private WebElement btnDelete;
    @FindBy(id = "grouprole_forward_domainName")
    private WebElement DomainNameValue;
    @FindBy(id = "grouprole_forward_categoryName")
    private WebElement CategoryName;

    public GroupRole_Page2(ExtentTest t1) {
        super(t1);
    }

    public static GroupRole_Page2 init(ExtentTest t1) {
        return new GroupRole_Page2(t1);
    }

    /**
     * Verify If Role already Exists
     *
     * @param webRole
     * @return index of web group role
     */
    public boolean isRoleExists(String webRole) {
        Utils.putThreadSleep(Constants.TWO_SECONDS);

        return isStringPresentInWebTable(webTable, webRole);
    }

    /**
     * Verify If Role already Exists
     *
     * @param webRole
     * @return index of web group role
     */
    public int verifyRoleExists(String webRole) {
        int rowCount = driver.findElements(By.xpath("//*[@id='grouprole_forward']/table/tbody/tr")).size();

        // iterate through all the cells
        for (int i = 3; i <= rowCount - 1; i++) {
            String roleName = driver.findElement(By.xpath("//*[@id='grouprole_forward']/table/tbody/tr[" + i + "]/td[2]")).getText();
            if (roleName.trim().equals(webRole.trim())) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Initiate Add New Web Group Role
     */
    public void initiateAddGroupRole() {
        clickOnElement(Add, "Add Role");
        pageInfo.info("Initiating Add New Group Role");
    }

    public String getSubcriberRoleName() {
        return driver.findElement(By.xpath("//*[@class='wwFormTableC']//tr[4]/td[2]")).getText();
    }

    /**
     * Initiate Update Existing web Group role
     */
    public void initiateUpdateGroupRole(int index) {
        webTable.findElement(By.xpath(".//tr[" + index + "]/td[1]/input")).click();
        clickOnElement(Update, "Update Role");
        pageInfo.info("Initiating Updating existing Group Role");
    }

    public boolean isMobileRoleExist(String roleName) {
        if (driver.findElements(By.xpath("//tr/td[contains(text(), '" + roleName + "')]")).size() > 0)
            return true;

        return false;
    }

    public void selectWebRoleByIndex(int groupRoleIndex) {
        driver.findElement(By.xpath("//*[@id='grouprole_forward']/table/tbody/tr[" + groupRoleIndex + "]/td[1]/input")).click();
        pageInfo.info("Selected WebRole with Index:" + groupRoleIndex);
    }

    public void initiateUpdateGroupRole1(String walletType, String provider) {
        driver.findElement(By.xpath("//td[contains(text(),'" + walletType + "')]/preceding-sibling::td[contains(text(),'" + provider + "')]/../td[contains(text(),'Mob')]/..//input[@type='radio']")).click();
        Update.click();
        pageInfo.info("Initiating Updating existing Group Role");
    }

    public void initiateUpdateGroupRole(String roleName) throws Exception {
        selectWebRole(roleName);
        Thread.sleep(ConfigInput.contentSwitchSync);
        clickOnElement(Update, "Update Role");
    }

    public void initiateDuplicateGroupRole(String roleName) throws Exception {
        selectWebRole(roleName);
        Thread.sleep(1500);
        clickOnElement(btnDuplicate, "Duplicate Role");
    }

    public void clickOnView() {
        clickOnElement(ViewDetails, "View Web Group Role Button.");
    }

    public boolean selectWebRole(String roleName) throws Exception {
        try {
            WebElement elem = driver.findElement(By.xpath(".//tr/td[contains(text(),'" + roleName + "')]/ancestor::tr[1]/td/input[@type='radio']"));
            clickOnElement(elem, "Radio: " + roleName);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }

    }

    public void clickOnDelete() {
        clickOnElement(btnDelete, "Button Delete");
        driver.switchTo().alert().accept();
    }

    public void clickOnDeleteWithoutAlertHandle() {
        clickOnElement(btnDelete, "Button Delete");
    }

    public void uncheckArole(String roleCode) {
        WebElement checkBox = driver.findElement(By.cssSelector("input[type='checkbox'][value='" + roleCode + "']"));
        if (checkBox.isSelected()) {
            checkBox.click();
        }
        pageInfo.info("Make sure that the Role is unchecked - " + roleCode);
    }

    public void uncheckAroleByCode(String[] roleCodes) {
        for(String roleCode : roleCodes){
            WebElement checkBox = driver.findElement(By.cssSelector("input[type='checkbox'][value='" + roleCode + "']"));
            if (checkBox.isSelected()) {
                checkBox.click();
            }
            pageInfo.info("Make sure that the Role is unchecked - " + roleCode);
        }

    }

    public void checkArole(String roleCode) {
        WebElement checkBox = driver.findElement(By.cssSelector("input[type='checkbox'][value='" + roleCode + "']"));
        if (!checkBox.isSelected()) {
            checkBox.click();
        }
        pageInfo.info("Make sure that the Role is unchecked - " + roleCode);
    }

    /**
     * Get Existing Mobile Role Map
     *
     * @return
     */
    public Map<String, MobileGroupRole> getExistingMobileGroupRole() throws IOException {
        Map<String, MobileGroupRole> map = new HashMap<String, MobileGroupRole>();
        try {

            Utils.putThreadSleep(NumberConstants.SLEEP_5000);
            int rowCount = getTableRowCount(webTable);

            if (isStringPresentInWebTable(webTable, MessageReader.getMessage("systemUser.blanklist.label", null))) {
                pageInfo.info("No Records Found");
                return map;  // return the empty map Since it is being used further by the system for mobile role creation
            }

            // iterate through all the cells
            for (int i = 4; i < rowCount; i++) {
                String roleName = webTable.findElement(By.xpath(".//tr[" + i + "]/td[2]")).getText();
                String roleCode = webTable.findElement(By.xpath(".//tr[" + i + "]/td[3]")).getText();
                String status = webTable.findElement(By.xpath(".//tr[" + i + "]/td[4]")).getText();
                String provider = webTable.findElement(By.xpath(".//tr[" + i + "]/td[5]")).getText();
                String payInst = webTable.findElement(By.xpath(".//tr[" + i + "]/td[6]")).getText();
                String payInstType = webTable.findElement(By.xpath(".//tr[" + i + "]/td[7]")).getText();

                if (!roleName.substring(0, 3).equals("Mob")) { // only consider mobile role which start from 'Mob'
                    continue;
                }

                MobileGroupRole mobileRole = new MobileGroupRole(roleName, roleCode, status, provider, payInst, payInstType);
                map.put(roleName, mobileRole);


            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
        }
        return map;
    }

    public String getDomainNameValue() {
        String DomainName = DomainNameValue.getText();
        return DomainName;
    }

    public String getCategoryName() {
        String CatName = CategoryName.getText();
        return CatName;
    }

    /**
     * Generic Method to verify label
     *
     * @throws IOException
     */
    public void verifyIfLabelExists(String labelName, ExtentTest t1) throws IOException {
        try {
            DriverFactory.getDriver()
                    .findElement(By.xpath("//*[contains(text(),'" + labelName + "')]")).isDisplayed();
            t1.pass("Successfuly verified the Label for element - " + labelName);
        } catch (Exception e) {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }

    /**
     * Generic Method to verify buttons
     *
     * @throws IOException
     */
    public void verifyIfButtonExists(String btnNAme, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.cssSelector("input[type='submit'][value='" + btnNAme + "']")).isDisplayed()) {
            t1.pass("Successfully verified the " + btnNAme + " button is present");
        } else {
            t1.fail("Failed to verify the " + btnNAme + " button is NOT present");
            Utils.captureScreen(t1);
        }
    }

    public void verifyIfLabelDisabledOrEnabled(String name, ExtentTest t1) throws IOException {
        WebElement labels = DriverFactory.getDriver()
                .findElement(By.xpath("//input[@name='" + name + "']"));

        if (labels.getAttribute("disabled").equals("disabled")) {
            t1.pass("Successfully verified the Field for element " + name + " is in non editable mode");
        } else {
            t1.fail("Field " + name + " is in editable mode");
            Utils.captureScreen(t1);
        }
    }

    public void DeleteConfirmation(ExtentTest t1) throws Exception {
        driver.findElement(By.cssSelector("input[type='button'][value='Delete']")).click();
        String msg = driver.switchTo().alert().getText();
        System.out.println(msg);
        pageInfo.info("Delete Confirmation pop up");
        Assertion.verifyContains(msg, "Do you really want to delete the selected profile?", "Confirm", t1);
        driver.switchTo().alert().accept();

    }

    public void verifyRoleOrServiceNamesWithCheckBox() throws IOException {

        Utils.putThreadSleep(NumberConstants.SLEEP_1000);

        boolean checkAllCheckBoxPresent = Utils.checkElementPresent("//input[@type='checkbox'][@value='0']", Constants.FIND_ELEMENT_BY_XPATH, pageInfo);
        boolean tabcenterPresent = Utils.checkElementPresent("tabcenter", Constants.FIND_ELEMENT_BY_CLASS, pageInfo);
        boolean grouprole_add_checkPresent = Utils.checkElementPresent("grouprole_add_check", Constants.FIND_ELEMENT_BY_ID, pageInfo);

        Assertion.verifyEqual(checkAllCheckBoxPresent, true, "Check All option is available.", pageInfo);
        Assertion.verifyEqual(tabcenterPresent, true, "Services Are available to select.", pageInfo);
        Assertion.verifyEqual(grouprole_add_checkPresent, true, "Check Boxes with respect to Services are exists for Selection", pageInfo);
    }
}
