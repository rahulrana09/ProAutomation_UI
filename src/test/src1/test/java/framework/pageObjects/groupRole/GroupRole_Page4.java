package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class GroupRole_Page4 extends PageInit {

    @FindBy(css = "input[type='checkbox'][value='0']")
    public WebElement SelectAllRoles;
    /*
     * Page Objects
	 */
    @FindBy(id = "grouprole_add_groupRoleCode")
    private WebElement GroupRoleCode;
    @FindBy(id = "grouprole_add_groupRoleName")
    private WebElement GroupRoleName;
    @FindBy(id = "selectForm_button_submit")
    private WebElement MobileGroupRoleSubmit;
    @FindBy(id = "selectForm_groupRoleCode")
    private WebElement MobileGroupRoleCode;
    @FindBy(id = "selectForm_groupRoleName")
    private WebElement MobileGroupRoleName;
    @FindBy(id = "selectForm_button_add")
    private WebElement MobileGroupRoleAdd;
    @FindBy(id = "grouprole_add_button_add")
    private WebElement AddGroupRole;
    @FindBy(id = "grouprole_modify_button_update")
    private WebElement UpdateGroupRole;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;

    @FindBy(className = "errorMessage")
    private WebElement ErrorMessage;

    @FindBy(id = "grouprole_duplicate_groupRoleCode")
    private WebElement duplicateGroupRoleCode;

    @FindBy(id = "grouprole_duplicate_groupRoleName")
    private WebElement duplicateGroupRoleName;

    @FindBy(id = "grouprole_duplicate_button_save")
    private WebElement btnSaveDuplicateRole;


    public GroupRole_Page4(ExtentTest t1) {
        super(t1);
    }

    public static GroupRole_Page4 init(ExtentTest t1) {
        return new GroupRole_Page4(t1);
    }

    /**
     * setGroupRoleName
     *
     * @param name
     */
    public void setGroupRoleName(String name) {
        setText(GroupRoleName, name, "GroupRoleName");
    }

    public void setDuplicateGroupRoleName(String name) {
        setText(duplicateGroupRoleName, name, "duplicateGroupRoleName");
    }

    public void setDuplicateGroupRoleCode(String name) {
        setText(duplicateGroupRoleCode, name, "duplicateGroupRoleCode");
    }

    /**
     * setGroupRoleCode
     *
     * @param code
     */
    public void setGroupRoleCode(String code) {
        setText(GroupRoleCode, code, "GroupRoleCode");
    }

    /**
     * setMobileGroupRoleCode
     *
     * @param code
     */
    public void setMobileGroupRoleCode(String code) {
        setText(MobileGroupRoleCode, code, "MobileGroupRoleCode");
    }

    /**
     * setMobileGroupRoleName
     *
     * @param name
     */
    public void setMobileGroupRoleName(String name) {
        setText(MobileGroupRoleName, name, "MobileGroupRoleName");
    }

    /**
     * Check Specific Role
     *
     * @param applicableRoles
     */
    public void checkRole(List<String> applicableRoles) throws IOException {
        clickOnElement(SelectAllRoles, "select all");
        clickOnElement(SelectAllRoles, "Reset selection");

        if(applicableRoles.contains("CHECK_ALL")) {
            clickOnElement(SelectAllRoles, "Select all role");
        }else {
            for (String roleCode : applicableRoles) {
                try {
                    if (!roleCode.equals("")) {
                        WebElement elem = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("input[type=checkbox][value='" + roleCode + "']"))));
                        Utils.scrollToAnElement(elem);
                        if(!elem.isSelected())
                            clickOnElement(elem, "Mobile Role with id: "+ roleCode);
                    }
                } catch (Exception e) {
                    pageInfo.warning("Group role code " + roleCode + " Not found ");
                }
            }
        }
        Utils.captureScreen(pageInfo);
    }

    public void selectUserRoles(List<String> applicableRoles) throws IOException {
        // check the specified roles
        if (applicableRoles.contains("CHECK_ALL")) {
            pageInfo.info("Select All Permissions!");
            checkAllRoles();
        } else {
            pageInfo.info("Select Specific Permissions!");
            checkRole(applicableRoles);
        }
    }



    /**
     * Check all roles
     */
    public void checkAllRoles() {
        clickOnElement(SelectAllRoles, "SelectAllRoles");
    }

    /**
     * Add Group Role
     */
    public void addGroupRole() {
        clickOnElement(AddGroupRole, "AddGroupRole");
    }

    public void addDuplicatedGroupRole() {
        clickOnElement(btnSaveDuplicateRole, "btnSaveDuplicateRole");
    }

    /**
     * Update Group Role
     */
    public void updateGroupRole() {
        clickOnElement(UpdateGroupRole, "UpdateGroupRole");
    }

    /**
     * Add Mobile Group Role
     */
    public void addMobileGroupRole() throws Exception {
        Utils.scrollToBottomOfPage();
        Thread.sleep(2000);
        clickOnElement(MobileGroupRoleAdd, "MobileGroupRoleAdd");
    }

    /**
     * Assert that the group Role is successfully Created
     * TODO - language en_US has to be configurable
     */
    public String assertCreatedMessage() throws Exception {
        Thread.sleep(1200);
        Assert.assertEquals(ActionMessage.getText(), MessageReader.getMessage("grouprole.added", null),
                "Verify That Group Role is successfully Created");
        return ActionMessage.getText();
    }

    /**
     * Assert that the group Role is successfully Updated
     */
    public String assertUpdateMessage() {
        Assert.assertEquals(ActionMessage.getText(), MessageReader.getMessage("grouprole.updated", null), "Verify That Group Role is successfully Updated");
        return ActionMessage.getText();
    }

    /**
     * Get the Action Message
     *
     * @return
     * @throws Exception
     */
    public String getActionMessage() throws Exception {
        try {
            Thread.sleep(5000);
            return wait.until(ExpectedConditions.elementToBeClickable(ActionMessage)).getText();
        } catch (NoSuchElementException e) {
            return null;
        }
    }
}
