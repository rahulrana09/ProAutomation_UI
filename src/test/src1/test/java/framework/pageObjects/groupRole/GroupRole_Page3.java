package framework.pageObjects.groupRole;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.List;

/**
 * Created by rahul.rana on 5/8/2017.
 */
public class GroupRole_Page3 extends PageInit {


    /*
     * Page Objects
	 */
    @FindBy(id = "mfsProvidersList")
    private WebElement CurrencyProvider;

    @FindBy(id = "paymentInstrument")
    private WebElement PaymentInstrument;

    @FindBy(id = "walletOrBank")
    private WebElement WalletOrBank;

    @FindBy(id = "selectForm_button_submit")
    private WebElement Submit;

    @FindBy(className = "errorMessage")
    private WebElement ErrorMessage;

    public GroupRole_Page3(ExtentTest t1) {
        super(t1);
    }

    public static GroupRole_Page3 init(ExtentTest t1) {
        return new GroupRole_Page3(t1);
    }

    public static void verifyElementsOnAddMobileRolePage() throws IOException {
        boolean mfsProvidersListPresent = Utils.checkElementPresent("mfsProvidersList", Constants.FIND_ELEMENT_BY_ID);
        boolean paymentInstrumentPresent = Utils.checkElementPresent("paymentInstrument", Constants.FIND_ELEMENT_BY_ID);
        boolean walletOrBankPresent = Utils.checkElementPresent("walletOrBank", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(mfsProvidersListPresent, true, "Checking Presence of - MFS Provider", pageInfo);
        Assertion.verifyEqual(paymentInstrumentPresent, true, "Checking Presence of - Payment Instrument", pageInfo);
        Assertion.verifyEqual(walletOrBankPresent, true, "Checking Presence of - Wallet Type/Linked Banks", pageInfo);
    }

    /**
     * Select Currency Provider
     *
     * @param provider
     */
    public void selectCurrencyProvider(String provider) {
        selectVisibleText(CurrencyProvider, provider, "Provider Name");
    }

    /**
     * Select Payment Instrument
     *
     * @param paymentInstrument
     */
    public void selectPaymentInstrument(String paymentInstrument) {
        selectVisibleText(PaymentInstrument, paymentInstrument, "Payment instrument");
    }

    /**
     * Select Payment Instrument By value
     *
     * @param paymentInstrument
     */
    public void selectPaymentInstrumentByValue(String paymentInstrument) {
        selectValue(PaymentInstrument, paymentInstrument, "Payment instrument");
        try {
            Thread.sleep(Constants.THREAD_SLEEP_1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Select Wallet Or Bank
     *
     * @param walletOrBank
     */
    public void selectWalletOrBank(String walletOrBank) {
        selectVisibleText(WalletOrBank, walletOrBank, "Payment Instrument Type");
    }

    /**
     * Select Wallet Or Bank By Value
     *
     * @param walletOrBank
     */
    public void selectWalletOrBankByValue(String walletOrBank) {
        selectValue(WalletOrBank, walletOrBank, "Payment type");
    }

    /**
     * Submit
     */
    public void Submit() {
        clickOnElement(Submit, "Submit Button");
    }

    /**
     * Check Error Message
     *
     * @return
     */
    public boolean checkErrorMessage() {
        try {
            //Assert.assertEquals(ErrorMessage.getText(), MessageReader.getMessage("grouprole.error.unique.already.created", null));
            if (ErrorMessage.getText().equals(MessageReader.getMessage("grouprole.error.unique.already.created", null)))
                return true;
        } catch (NoSuchElementException e) {
            pageInfo.info("Proceed to Mobile Group Role Creation");
        }
        return false;
    }

    /**
     * Get all currency Providers
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllCurrencyProviders() throws Exception {
        return fl.getOptionText(CurrencyProvider);
    }

    /**
     * Get All Payment Instruments
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllPaymentInstrument() throws Exception {
        return fl.getOptionText(PaymentInstrument);
    }

    /**
     * Get all Wallet or Bank Available
     *
     * @return
     * @throws Exception
     */
    public List<String> getAllWalletOrBank() throws Exception {
        Thread.sleep(2500);
        return fl.getOptionText(WalletOrBank);
    }
}
