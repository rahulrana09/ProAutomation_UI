package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ApproveClub_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static ApproveClub_Page1 page;
    @FindBy(name = "approve")
    WebElement btnApprove;
    @FindBy(name = "reject")
    WebElement btnReject;
    @FindBy(name = "rejectReason")
    WebElement txtRejectReason;
    @FindBy(xpath = "//input[@type = 'submit' and @value = 'Submit']")
    WebElement btnConfirm;

    public static ApproveClub_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, ApproveClub_Page1.class);
    }

    public ApproveClub_Page1 navApproveSavingClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_APP_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  Approve Club Addition");
        return this;
    }

    public ApproveClub_Page1 navModifyApproveClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_MODAPP_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  Approve Club Modification");
        return this;
    }

    public ApproveClub_Page1 selectClub(String clubName) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + clubName + "')]/ancestor::tr[1]/td/input[@type='checkbox']")).click();
        pageInfo.info("Click on Checkbox corresponding to  - " + clubName);
        return this;
    }

    public ApproveClub_Page1 approveClub() {
        btnApprove.click();
        pageInfo.info("Click on Approve");
        return this;
    }

    public ApproveClub_Page1 rejectClub() {
        btnReject.click();
        pageInfo.info("Click on Reject");
        return this;
    }

    public ApproveClub_Page1 setRejectReason(String text) {
        txtRejectReason.sendKeys(text);
        pageInfo.info("Set Reason for Reject - " + text);
        return this;
    }

    public ApproveClub_Page1 confirmAction() {
        btnConfirm.click();
        pageInfo.info("Click on Submit Confirm");
        return this;
    }


}
