package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class ModifyClub_Page1 extends PageInit {
    @FindBy(name = "clubId")
    private WebElement txtClubId;
    @FindBy(name = "chMsisdn")
    private WebElement txtCmMsisdn;
    @FindBy(name = "clubname")
    private WebElement txtClubNameInitials;
    @FindBy(name = "selectedClubName")
    private WebElement selClubName;
    @FindBy(id = "clubRegisterationServiceBean_modifyClub_button_submit")
    private WebElement btnSubmit;

    public ModifyClub_Page1(ExtentTest t1) {
        super(t1);
    }

    public ModifyClub_Page1 navModifySavingClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_MOD_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  Modify Club");
        return this;
    }

    public ModifyClub_Page1 setClubId(String text) {
        txtClubId.sendKeys(text);
        pageInfo.info("Set Club id - " + text);
        return this;
    }

    public ModifyClub_Page1 setCMMsisdn(String text) {
        txtCmMsisdn.sendKeys(text);
        pageInfo.info("Set CM Msisdn as - " + text);
        return this;
    }

    public ModifyClub_Page1 setClubNameInitials(String text) {
        txtClubNameInitials.sendKeys(text);
        pageInfo.info("set Club Initials - " + text);
        return this;
    }

    public ModifyClub_Page1 selectClubName(String text) throws InterruptedException {
        selClubName.click();
        Select sel = new Select(selClubName);
        fl.waitSelectOptions(sel);
        Thread.sleep(3000);
        sel.selectByVisibleText(text);
        pageInfo.info("set Club Name - " + text);
        return this;
    }

    public ModifyClub_Page1 clickSubmit() {
        btnSubmit.click();
        pageInfo.info("click on Submit");
        return this;
    }
}
