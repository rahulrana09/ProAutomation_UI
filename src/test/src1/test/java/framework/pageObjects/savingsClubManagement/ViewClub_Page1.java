package framework.pageObjects.savingsClubManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class ViewClub_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    private static ViewClub_Page1 page;

    public static ViewClub_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, ViewClub_Page1.class);
    }


    public ViewClub_Page1 navViewSavingClub() throws Exception {
        fl.leftNavigation("CLUBS_ALL", "CLUBS_VIEW_CLUB");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Savings Club Management >  View Club List");
        return this;
    }

    public String getClubCurrentStatus(String cmMsisdn) throws IOException {
        pageInfo.info("Get Club current Status for  - " + cmMsisdn);
        Utils.captureScreen(pageInfo);
        return driver.findElement(By.xpath("//tr/td[contains(text(),'" + cmMsisdn + "')]/ancestor::tr[1]/td[5]")).getText();
    }

    public ViewClub_Page1 clickOnViewClub(String clubId) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + clubId + "')]/ancestor::tr[1]/td/a")).click();
        return this;
    }

    /**
     * Get Members currenty Status
     *
     * @param msisdn
     * @return
     */
    public HashMap<String, String> getUserCurrentStatus(String msisdn) throws Exception {
        String MainWindow = driver.getWindowHandle();
        Set<String> s1 = driver.getWindowHandles();
        HashMap<String, String> userStatusMap = new HashMap<>();
        String status = null, isApprover = null;
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String ChildWindow = i1.next();

            if (!MainWindow.equalsIgnoreCase(ChildWindow)) {
                driver.switchTo().window(ChildWindow);
                driver.manage().window().maximize();
                status = driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr/td[6]")).getText();
                isApprover = driver.findElement(By.xpath("//tr/td[contains(text(), '" + msisdn + "')]/ancestor::tr/td[4]")).getText();
                userStatusMap.put("status", status);
                userStatusMap.put("approver", isApprover);

                Utils.captureScreen(pageInfo);
                driver.close(); // close the window
                driver.switchTo().window(MainWindow);
                fl.contentFrame();
                return userStatusMap;
            }
        }
        return null;
    }

    public String getCurrentStatus(String cmMsisdn) {
        return driver.findElement(By.xpath("//tr/td[contains(text(),'" + cmMsisdn + "')]/ancestor::tr[1]/td[5]")).getText();
    }


}