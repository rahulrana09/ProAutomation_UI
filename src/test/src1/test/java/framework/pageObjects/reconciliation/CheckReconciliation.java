/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 28-Dec-2017
*  Purpose: PageObject Of Reconciliation
*/
package framework.pageObjects.reconciliation;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 12/28/2017.
 */
public class CheckReconciliation extends PageInit {


    @FindBy(id = "reconciliation_loadReconciliation_reconciliationMap_netBalance")
    private WebElement netBalance;
    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_providerName")
    private WebElement mfsProviderDDown;
    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_button_submit")
    private WebElement submit;
    @FindBy(xpath = "//*[@id='reconciliation_loadReconciliation']/table/tbody/tr[17]/td[2]")
    private WebElement onTheFlyOnHoldLbl;
    @FindBy(xpath = "//td[contains(text(),\"f)Churn\")]/following-sibling::td[@class=\"tabcol\"]")
    private WebElement churn;
    @FindBy(xpath = ".//*[@id='reconciliation_loadReconciliation_reconciliationMap_ftbalance']")
    private WebElement onTheFlyBalanceLbl;

    public CheckReconciliation(ExtentTest t1) {
        super(t1);
    }

    public void navigateToLink() throws Exception {
        fl.leftNavigation("MNREC_ALL", "MNREC_MN_REC");
        pageInfo.info("Navigate to reconciliation Link");
    }

    public String getReconBalance() throws InterruptedException {
        String reconBalance = netBalance.getText();
        Utils.scrollToAnElement(netBalance);
        pageInfo.info("Summary of balances : " + reconBalance);
        return reconBalance;
    }

    public String getReconChurn() throws InterruptedException {
        String val = churn.getText();
        Utils.scrollToAnElement(churn);
        pageInfo.info("Churn Balance: " + val);
        return val;
    }


    /**
     * Methdo to select MFS Provider
     *
     * @param mfsProvider
     */
    public void selectMFSProvider(String mfsProvider) {
        selectValue(mfsProviderDDown, mfsProvider, "MFS Provider");
    }

    /**
     * method to get On the fly balance
     *
     * @return
     */
    public String getOnTheFlyBalance() {
        return onTheFlyBalanceLbl.getText();
    }

    /**
     * method to get On the fly On Hold
     *
     * @return
     */
    public String getOnTheFlyOnHold() {
        return onTheFlyOnHoldLbl.getText();
    }

    /**
     * Method to click On Submit Button
     */
    public void clickSubmit() {
        clickOnElement(submit, "Submit Button");
    }

}
