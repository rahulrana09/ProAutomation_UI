package framework.pageObjects.reconciliation;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class Reconciliation_Page extends PageInit {


    public Reconciliation_Page(ExtentTest t1) {
        super(t1);
    }


    /*
     *   ################# Elements Of ReconCilation Page #################
     *
     */
    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_providerName")
    private WebElement selectDropDown;

    @FindBy(id = "reconciliation_loadReconciliationMultiCurr_button_submit")
    private WebElement submitBtn;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']//td[@class='tableft']")
    private WebElement reconciledResult;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr[1]/td[1]")
    private WebElement stockCreatedBy1;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr[2]/td[1]")
    private WebElement stockCreatedBy2;

    @FindBy(xpath = "//label[@class='label'][.='Total Stock (A):']")
    private WebElement totalstockfield;

    @FindBy(xpath = "//label[@id='reconciliation_loadReconciliation_reconciliationMap_balanceCredit']")
    private WebElement totalStockValue;

    @FindBy(xpath = "//label[@class='label'][.='Bank Balance (B):']")
    private WebElement bankBalanceField;

    @FindBy(xpath = "//label[@id='reconciliation_loadReconciliation_reconciliationMap_balanceCredit']")
    private WebElement bankBalanceValue;

    @FindBy(xpath = "//label[@class='label'][.='On The Fly Balance(C):']")
    private WebElement onTheFlyField;

    @FindBy(xpath = "//label[@id='reconciliation_loadReconciliation_reconciliationMap_ftbalance']")
    private WebElement onTheFlyValue;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr/td[contains(text(), 'Channel User Balance (F01)')]")
    private WebElement channelUserBalanceField;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr/td[contains(text(), 'Channel User Balance (F01)')]/../td[2]")
    private WebElement channelUserBalanceValue;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr[7]/td[1]")
    private WebElement serviceChargeAndCommission;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr/td[contains(text(), 'Tax (E01)')]")
    private WebElement TaxField;

    @FindBy(xpath = "//form[@id='reconciliation_loadReconciliation']/table/tbody/tr/td[contains(text(), 'Tax (E01)')]/../td[2]")
    private WebElement TaxValue;



 /*
 *  ------------------------------------------   Navigation methods   ------------------------------------------------
 * */

    public Reconciliation_Page navToReconciliationLink() throws Exception {
        navigateTo("MNREC_ALL", "MNREC_MN_REC", "Reconciliation Page");
        return this;
    }

    public void navToReconciliationPage() throws Exception {
        navToReconciliationLink();
        setProvider();
        selectSubmit();
    }

    public Reconciliation_Page setProvider() throws Exception {
        Select sel = new Select(selectDropDown);
        sel.selectByIndex(1);
        pageInfo.info("Selected MFS Provider");
        return this;
    }

    public Reconciliation_Page setProviderByText(String provider) throws Exception {
        selectVisibleText(selectDropDown,provider,"Provider ");
        return this;
    }

    public Reconciliation_Page selectSubmit() {
        clickOnElement(submitBtn, "Submit Button");
        return this;
    }

    public String reconciledResult() throws Exception {
        Thread.sleep(1000);
        pageInfo.info("Reconciled Page");
        return reconciledResult.getText();
    }


/*
*  --------------------------------------  Field Validation in Reconcilation Screen -----------------------------------
* */

    public Reconciliation_Page validateFieldsInReconcilationScreen() throws Exception {
        Markup m = MarkupHelper.createLabel("ReconciliationScreen", ExtentColor.TEAL);
        pageInfo.info(m);
        try {
            navToReconciliationLink();
            setProvider();
            selectSubmit();
            stockCreatedBy1.isDisplayed();
            pageInfo.info(stockCreatedBy1.getText() + " Field Present");

            totalstockfield.isDisplayed();
            pageInfo.info(totalstockfield.getText() + " :: " + totalStockValue.getText());

            bankBalanceField.isDisplayed();
            pageInfo.info(bankBalanceField.getText() + " :: " + bankBalanceValue.getText());

            onTheFlyField.isDisplayed();
            pageInfo.info(onTheFlyField.getText() + " :: " + onTheFlyValue.getText());

            serviceChargeAndCommission.isDisplayed();
            pageInfo.info(serviceChargeAndCommission.getText() + " Field Present");

            channelUserBalanceField.isDisplayed();
            pageInfo.info(channelUserBalanceField.getText() + " :: " + channelUserBalanceValue.getText());

            TaxField.isDisplayed();
            pageInfo.info(TaxField.getText() + " :: " + TaxValue.getText());


        } catch (Exception e) {
            pageInfo.info("....Field not Present....");
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, pageInfo);
        }

        return this;
    }
}

