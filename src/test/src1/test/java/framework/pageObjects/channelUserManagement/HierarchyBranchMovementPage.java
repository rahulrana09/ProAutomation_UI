package framework.pageObjects.channelUserManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 12/11/2018.
 */
public class HierarchyBranchMovementPage extends PageInit {


    /**
     * H I E R A R C H Y * B R A N C H *  M OV E M E N T *  W E B *  E L E M E N T S
     */
    @FindBy(name = "msisdnForHierarchy")
    private WebElement hierarchyUserMsisdn;
    @FindBy(id = "selectForm_button_submit")
    private WebElement hierarchyMsisdnSubmit;
    @FindBy(id = "ownerId")
    private WebElement hierarchyToOwner;
    @FindBy(id = "parentId")
    private WebElement hierarchyToParents;
    @FindBy(id = "userNameId")
    private WebElement hierarchyUserNameId;
    @FindBy(xpath = "//*[@id='selectForm']/table/tbody/tr[6]/td[3]/img")
    private WebElement hierarchyUserNameSearchParent;
    @FindBy(name = "action:hryMoveCH_loadHierarchyMovePageDisplay")
    private WebElement hierarchyNextPage;
    @FindBy(id = "selectForm_button_confirm")
    private WebElement Confirm;
    @FindBy(id = "selectForm_balanceCheckRequired")
    private WebElement balanceCheck;
    @FindBy(id = "parentNameId")
    private WebElement hierarchyParentName;

    public HierarchyBranchMovementPage(ExtentTest t1) {
        super(t1);
    }

    /**
     * H I E R A R C H Y * B R A N C H *  M OV E M E N T *  M E T H O D S
     */

    /**
     * @throws Exception
     */
    public void navHierarchyMovement() throws Exception {
        navigateTo("CHUMGMT_ALL", "CHUMGMT_HRY_MOV", "Channel User Hierarchy Movement");
    }


    public void setMsisdnforBranchMovement(String msisdn) {
        setText(hierarchyUserMsisdn, msisdn, "hierarchyUserMsisdn");
    }

    public void selectOwnerNameforBranchMovement(String ownerPartialName) {
        selectVisibleText(hierarchyToOwner, ownerPartialName, "hierarchyToOwner");
    }

    public void selectOwnerNameforBranchMovementByValue(String ownerUserId) {
        selectValue(hierarchyToOwner, ownerUserId, "hierarchyToOwner");
    }

    public void selectParentCategory(String category) {
        selectVisibleText(hierarchyToParents, category, "hierarchyToParents");
    }

    public void setParentName(String parentPartialName) throws InterruptedException {
        setText(hierarchyUserNameId, "%", "hierarchyUserNameId");
        clickOnElement(hierarchyUserNameSearchParent, "hierarchyUserNameSearchParent");
        selectVisibleText(hierarchyParentName, parentPartialName, "hierarchyParentName");
    }

    public void submitBranchMovementPageOne() {
        clickOnElement(hierarchyNextPage, "hierarchyNextPage");
    }

    public void clickonConfirm() {
        clickOnElement(Confirm, "Confirm");
    }

    public void clickonBalanceCheck() {
        clickOnElement(balanceCheck, "BalanceCheck");
    }

    public void submitforBranchMovement() {
        clickOnElement(hierarchyMsisdnSubmit, "hierarchyMsisdnSubmit");
    }


}
