package framework.pageObjects.gradeManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class GradeDeletePage extends PageInit {

    @FindBy(id = "deleteGrades_delete_submit")
    WebElement deleteButton;


    /*
      ****************************************************************
      *      P A G E   O B J E C T S
      ****************************************************************
     */
    @FindBy(id = "delconfirm_submit")
    WebElement confirmButton;
    @FindBy(name = "action:deleteGrades_displayForDeleteBack")
    WebElement backButton;

    public GradeDeletePage(ExtentTest t1) {
        super(t1);
    }

    /**
     * For navigation to Grade Delete
     *
     * @throws NoSuchElementException Throws NoSuchElementException when Link not found
     */
    public void navigateToLink() throws Exception {
        navigateTo("CHGRADES_ALL", "CHGRADES_DELETEGRADES_TR", "Grade Delete");
    }


    /**
     * To select the Grade to Delete
     *
     * @param gradeCode Pass the Grade
     */
    public void selectGrade(String gradeCode) {
        WebElement option = driver.findElement(By.xpath("//*[@value='" + gradeCode + "' ][ @type='checkbox']"));
        clickOnElement(option, "radio having the Grade :" + gradeCode);
    }

    /**
     * Click On Delete Button
     */
    public void clickDeleteButton() {
        clickOnElement(deleteButton, "Delete Button");
    }

    /**
     * Click On Confirm Button
     */
    public void confirmDelete() {
        clickOnElement(confirmButton, "Confirm Button");
    }

    /**
     * Click On Back Button
     */
    public void clickBackButton() {
        clickOnElement(backButton, "Back Button");
    }

    public boolean isBackButtonDisplayed() {
        return fl.elementIsDisplayed(backButton);
    }

    public boolean isConfirmButtonDisplayed() {
        return fl.elementIsDisplayed(confirmButton);
    }

    public boolean isDeleteButtonDisplayed() {
        return fl.elementIsDisplayed(deleteButton);
    }


}