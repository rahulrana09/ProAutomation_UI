package framework.pageObjects.gradeManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class GradeAdd_Page1 extends PageInit {

    @FindBy(xpath = ".//input[@id='grades_addGrades_submit' and @value = 'Add More Grades']")
    public WebElement addmoregrade;
    @FindBy(id = "grades_addGrades_domainCode")
    WebElement domain;
    @FindBy(id = "grades_addGrades_categoryCode")
    WebElement category;
    @FindBy(id = "grades_addGrades_submit")
    WebElement add1;
    @FindBy(id = "grades_addGrades_gradeList_0__gradeCode")
    WebElement code;
    @FindBy(id = "grades_addGrades_gradeList_0__gradeName")
    WebElement name;
    @FindBy(name = "action:grades_saveGrades")
    WebElement save;
    @FindBy(name = "action:grades_saveGradesConfirm")
    WebElement confirm;
    @FindBy(name = "action:grades_displayGrades")
    WebElement back;

    public GradeAdd_Page1(ExtentTest t1) {
        super(t1);
    }

    public static GradeAdd_Page1 init(ExtentTest t1) {
        return new GradeAdd_Page1(t1);
    }

    public void navigateToLink() throws Exception {
        navigateTo("CHGRADES_ALL", "CHGRADES_CHGRADES_TR", "Add Grade Page");
    }


    public void selectdomain(String val) {
        selectVisibleText(domain, val, "Domain Name");
    }

    public void selectdomainByValue(String val) {
        selectValue(domain, val, "Domain Name");
    }

    public void selectcategory(String val) {
        selectVisibleText(category, val, "Category Name");
    }

    public void selectcategoryByValue(String val) {
        selectValue(category, val, "Category");
    }

    public boolean addgrade() {
        clickOnElement(add1, "Add button");
        return add1.isEnabled();
    }

    public void gradecode(String val) {
        setText(code, val, "Grade Code");
    }

    public void gradename(String val) {
        setText(name, val, "Grade Name");
    }


    public void clickSaveButton() {
        clickOnElement(save, "Save Button");
    }

    public void confirm() {
        clickOnElement(confirm, "Confirm");
    }

    public void back() {
        clickOnElement(back, "Back");
    }

    public boolean getAddMoreButtonStatus() {
        return addmoregrade.isEnabled();
    }

    public boolean getSaveButtonStatus() {
        return save.isEnabled();
    }

    public boolean getBackButtonStatus() {
        return back.isEnabled();
    }

    public boolean getConfirmButtonStatus() {
        return confirm.isEnabled();
    }
}
