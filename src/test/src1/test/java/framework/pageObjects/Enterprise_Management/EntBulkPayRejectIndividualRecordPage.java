package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.pageObjects.enterpriseManagement.BulkPaymentPage1;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static java.awt.SystemColor.text;

public class EntBulkPayRejectIndividualRecordPage extends PageInit{

    @FindBy(xpath = "//span[@class='download_template']/a")
    WebElement entRejectRecordTemplateFile;

    @FindBy(xpath = "//input[@id='file-upload1']")
    private WebElement entRejectRecordFilUupload;

    @FindBy(css = ".uploader_details.cursor_default")
    WebElement txtUploadInfo;

    @FindBy(id = "remarks")
    private WebElement remarks;

    @FindBy(css = ".alert.alert-success")
    WebElement successMessage;

    @FindBy(xpath = "//*[@type='submit']")
    private WebElement Submit;

    public EntBulkPayRejectIndividualRecordPage(ExtentTest t1) {
        super(t1);
    }

    public static EntBulkPayRejectIndividualRecordPage init(ExtentTest t1) {
        return new EntBulkPayRejectIndividualRecordPage(t1);
    }

    public EntBulkPayRejectIndividualRecordPage NavigateToLink() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_REJECT_BULK_ENT_PAY", "Approve entBulk Pay L1");
        return this;
    }

    public EntBulkPayRejectIndividualRecordPage EnterRemarks(String remark) {
        clickOnElement(remarks, "Remarks");
        setText(remarks, remark, "Remark");
        return this;
    }

    public String getActionMessage() throws Exception {
        Thread.sleep(2500);
        if (fl.elementIsDisplayed(successMessage)) {
            return successMessage.getText();
        } else {
            return null;
        }
    }

    public EntBulkPayRejectIndividualRecordPage ClickOnSubmit() {
        clickOnElement(Submit, "Submit");
        Utils.putThreadSleep(5000);
        return this;
    }

    public boolean downloadTemplateBulkCSV(String prefix) throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - " + prefix);
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, prefix);
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(entRejectRecordTemplateFile, "Reject record file template");
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            return Utils.isFileDownloaded(oldFile, newFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean uploadFile(String filePath) throws Exception {
        entRejectRecordFilUupload.sendKeys(filePath);
        pageInfo.info("Uploading file" + filePath);
        Utils.captureScreen(pageInfo);
        Thread.sleep(1500);
        if (fl.elementIsDisplayed(txtUploadInfo)) {
            if (txtUploadInfo.getText().toLowerCase().contains("initiated")) {
                pageInfo.pass("Successfully uploaded the file:" + text);
                return true;
            } else {
                pageInfo.fail("Failed to uploaded the file:" + text);
                return false;
            }
        } else {
            pageInfo.fail("Failed to uploaded the file:" + text);
        }
        return false;
    }


}
