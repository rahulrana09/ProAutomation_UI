//package framework.pageObjects.Enterprise_Management;
//
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.markuputils.ExtentColor;
//import com.aventstack.extentreports.markuputils.Markup;
//import com.aventstack.extentreports.markuputils.MarkupHelper;
//import framework.pageObjects.PageInit;
//import framework.util.common.Assertion;
//import framework.util.common.Utils;
//import framework.util.globalConstant.FilePath;
//import framework.util.globalVars.GlobalData;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.ui.Select;
//import org.testng.Assert;
//import java.util.ArrayList;
//import java.util.List;
//import java.awt.*;
//import java.awt.datatransfer.StringSelection;
//import java.awt.event.KeyEvent;
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.FileReader;
//import java.io.FileWriter;
//
//
//public class BulkPayment_page1New extends PageInit {
//
//    @FindBy(xpath = "//span[contains(@class,'upload')]")
//    private WebElement uploadfile;
//
//    @FindBy(id = "remarks")
//    private WebElement remarks;
//
//    @FindBy(name = "action")
//    private WebElement Submit;
//
//    @FindBy(xpath = "//span[@class='download_template']/a")
//    WebElement bulkSampleFile;
//
//    @FindBy(xpath = "(//div[@class='alert alert-success']//span)[2]")
//    WebElement message;
//
//    @FindBy(xpath = "(//input[@name='item.name'])[2]")
//    WebElement name;
//
//    @FindBy(xpath = "(//input[@name='item.name'])[1]")
//    WebElement nation_id;
//
//    @FindBy(xpath = "//span//a[@class='text_decoration']")
//    WebElement Error;
//
//    @FindBy(xpath = "//select")
//    WebElement serviceName;
//
//    @FindBy(xpath = "//span[.='UPLOAD']")
//    WebElement uploadbtn;
//
//
//    public BulkPayment_page1New ClickOnSubmit() throws Exception {
//        clickOnElement(Submit, "Submit");
//        return this;
//    }
//
//    public BulkPayment_page1New EnterRemarks() {
//        clickOnElement(remarks, "Remark");
//        setText(remarks, "Remark", "remarks");
//        return this;
//    }
//
//    public void alerthandle() {
//        driver.switchTo().alert().accept();
//        pageInfo.info("Alert is accepted");
//    }
//
//    public BulkPayment_page1New NavigateToApp1Link() throws Exception {
//        navigateTo("PAYROLL_ALL", "PAYROLL_SAL_BUP", "Add Bulk Payee");
//        return this;
//
//    }
//
//    public String getBatchId() throws Exception {
//        return driver.findElement(By.xpath("//button[@class='close']/following-sibling::span")).getText();
//    }
//
//    public void VerifyCSVFile(List status) throws Exception {
//        pageInfo.info("Verifying the Error Massage");
//        try {
//            ArrayList fields = new ArrayList();
//            //Header i.e 1st Row
//            fields.add("Mobile Number*");
//            fields.add("Amount*");
//            fields.add("First Name");
//            fields.add("Last Name");
//            fields.add("National Id");
//            fields.add("ID Number");
//            fields.add("Status");
//            fields.add("ServiceRequestStatus");
//            fields.add("ErrorCode");
//            fields.add("ErrorMessage");
//            String ErrorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
//            BufferedReader br = new BufferedReader(new FileReader(ErrorFile));
//            String line = "";
//            int row = 0;
//            List<String> records = new ArrayList<>();
//
//            //Getting the number of rows in the file and will store it in records
//            while ((line = br.readLine()) != null) {
//                records.add(line);
//                row++;
//            }
//            System.out.println("Number of lines in records:" + records.size());
//            String[] subString = records.get(0).split(",");
//
//            //Verifying the header of the file i.e row 0
//            for (int i = 0; i < subString.length; i++) {
//                Assert.assertEquals(subString[i], fields.get(i));
//                System.out.println("Header " + i + " verified ");
//            }
//
//            ////Verifying the other row of the file part from row 0
//            int index = 0;
//            for (int i = 1; i < records.size(); i++) {
//                System.out.println(" Substring:" + i);
//                String[] subStrings = records.get(i).split(",");
//                for (int k = 0; k < subStrings.length; k++) {
//                    System.out.print(subStrings[k] + ",");
//                }
//                for (int o = 6; o < subStrings.length; o++) {
//                    Assert.assertEquals(subStrings[o], status.get(index));
//                    index++;
//                }
//                pageInfo.pass("Passed");
//                pageInfo.info("Row " + i + " Verified ");
//
//            }
//        } catch (Exception e) {
//            pageInfo.fail("Failed");
//            pageInfo.info("Records not verified ");
//            e.printStackTrace();
//        }
//    }
//
//    public BulkPayment_page1New VerifyErrorMassage(String ActualErrorFilePrefix, String ExpectedErrorFilePrefix) throws Exception {
//        pageInfo.info("Verify Two csv files are equal or not:");
//        try {
//            String ActualErrorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, ActualErrorFilePrefix);
//            BufferedReader ActualFileReader = new BufferedReader(new FileReader(ActualErrorFile));
//            String ExpectedErrorFile = FilePath.fetchFilesForPathByPrefix(FilePath.FILE_EXPECTED_PATH, ExpectedErrorFilePrefix);
//            BufferedReader ExpectedFileReader = new BufferedReader(new FileReader(ExpectedErrorFile));
//
//            //AreEqual flag value will be set to "false" whenever content of file is different
//            boolean areEqual = true;
//
//            //lineNum flag will hold the count the no. of lines
//            int lineNum = 1;
//
//            //Read the lines of both the file and store it in respective variables
//            String line1 = ActualFileReader.readLine();
//            System.out.println(line1);
//            String line2 = ExpectedFileReader.readLine();
//            System.out.println(line2);
//
//            //If any of the line1 or line2 is null,then assign false and break the loop,if both are not null then compare using equalsIgnoreCase()
//            while (line1 != null || line2 != null) {
//                if (line1 == null || line2 == null) {
//                    areEqual = false;
//                    break;
//                } else if (!line1.equalsIgnoreCase((line2))) {
//                    areEqual = false;
//                    break;
//                }
//                line1 = ActualFileReader.readLine();
//                line2 = ExpectedFileReader.readLine();
//                System.out.println(lineNum);
//                lineNum++;
//            }
//
//            if (areEqual) {
//                pageInfo.pass("passed");
//                Assert.assertEquals(areEqual, true);
//            } else {
//                pageInfo.fail("Failed");
//                Assert.assertEquals(areEqual, false);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return this;
//    }
//
//    public String verfiy_allertmessage() {
//        fl.waitWebElementVisible(message);
//        String msg = message.getText();
//        pageInfo.info("get message");
//        return msg;
//    }
//
//    public void selectName() {
//        name.click();
//        pageInfo.info("Name is selected Mandatory");
//    }
//
//
//    public void selectNationalid() {
//        nation_id.click();
//        pageInfo.info("National Id is selected as Mandatory");
//    }
//    public BulkPayment_page1New(ExtentTest t1) {
//        super(t1);
//    }
//
//
//    public BulkPayment_page1New ClickSubmit() {
//        clickOnElement(Submit, "Submit");
//        return this;
//    }
//
//}
