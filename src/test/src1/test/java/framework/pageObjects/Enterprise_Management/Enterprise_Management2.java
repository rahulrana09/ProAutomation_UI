package framework.pageObjects.Enterprise_Management;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class Enterprise_Management2 {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pageInfo;
    private static Enterprise_Management2 page;
    @FindBy(id = "addEmployee_addConfirm_employeeCode")
    private WebElement UniqueCode;
    @FindBy(id = "addEmployee_addConfirm_button_add" +
            "")
    private WebElement Submit;
    @FindBy(id = "addEmployee_add_button_confirm")
    private WebElement Confirm;

    public static Enterprise_Management2 init(ExtentTest t1) {
        pageInfo = t1;
        if (page == null) {
            driver = DriverFactory.getDriver();
            page = PageFactory.initElements(driver, Enterprise_Management2.class);
            fl = new FunctionLibrary(driver);
        }
        return page;
    }

    public Enterprise_Management2 EnterUniqueCode(String value) {
        UniqueCode.sendKeys(value);
        pageInfo.info("Enter Unique Code Number" + value);
        return this;
    }

    public Enterprise_Management2 ClickOnSubmit() {
        Submit.click();
        pageInfo.info("Click on Submit button");
        return this;
    }

    public void ClickOnConfirm() {
        Confirm.click();
        pageInfo.info("Click on Confirm button");
    }
}
