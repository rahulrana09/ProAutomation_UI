package framework.pageObjects.bulkPayoutTool;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class BulkPayoutDashboard_page1 extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(xpath = "//span[.='Newest']")
    WebElement sortNewest;

    @FindBy(xpath = "(//span[@class='lh collapse_btnPD1'])[1]")
    WebElement entry;

    @FindBy(xpath = "//span[.='Success']/following-sibling::span")
    WebElement successCount;

    @FindBy(xpath = "//span[.='Failed']/following-sibling::span")
    WebElement failCount;

    @FindBy(xpath = "//a[@class='download_failed_trans']")
    WebElement downloadfailedTrans;


    @FindBy(xpath = "//a[text()='Download status file']")
    WebElement downloadstatusFile;

    @FindBy(xpath = "//span[@class='primary_color_dark bulkID']")
    WebElement batchId;

    @FindBy(xpath = "(//span[contains(text(),'Number of entries')]/span)[1]")
    WebElement noOfEntries;


    public BulkPayoutDashboard_page1(ExtentTest t1) {
        super(t1);
    }

    public static BulkPayoutDashboard_page1 init(ExtentTest t1) {
        return new BulkPayoutDashboard_page1(t1);
    }

    /***************************************************************************
     * ############################ Page Operations ############################
     ***************************************************************************/

    public void navigateToLink() throws Exception {
        navigateTo("BULKPAY_ALL", "BULKPAY_BULK_DASHBOARD", "Bulk Payout Dashboard");
    }

    public void sortNewest() {
        clickOnElement(sortNewest, "sortNewest");
    }

    public void clickOnEntry() {
        clickOnElement(entry, "Latest Entry");
    }

    public String fetchSuccessCount() {
        return wait.until(ExpectedConditions.visibilityOf(successCount)).getText();
    }

    public String fetchFailedCount() {
        return wait.until(ExpectedConditions.elementToBeClickable(failCount)).getText();
    }

    public void selectServiceType(String service) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(0);
        selectVisibleText(elem, service, "Service Type");
    }

    public void selectStatusFilter(Boolean isApproval) {
        WebElement elem = driver.findElements(By.xpath("//select")).get(1);
        if (isApproval) {
            selectValue(elem, "SUCCEEDED", "Approval Status");
        } else {
            selectValue(elem, "FAILED", "Approval Status");
        }
    }

    public void clickOnDownloadFailedTransLink() throws InterruptedException, IOException {

        pageInfo.info("Deleting existing file with prefix - bulk-upload");
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload"); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        downloadfailedTrans.click();
        pageInfo.info("Click on the download Failed Transaction link");

        //String errorFile = FilePath.dirFileDownloads;
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
        String errorFile = FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
        System.out.println(errorFile);

        //String splitBy = ",";
        String message = "";

        ArrayList<String> al = new ArrayList<String>();
        //BufferedReader br = new BufferedReader(new FileReader(errorFile));
        Scanner s = new Scanner(new File(errorFile));
        System.out.println("read the file");
        /*String line = br.readLine();
        String[] b=new String[line.length()-1];*/
        System.out.println("read the line");

        while (s.hasNext()) {
            message = s.next();
            String[] b = message.split("ErrorMessage");
            System.out.println(b[b.length - 1]);
        }
    }

    public String downloadStatusFile() throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - bulk-upload");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload-"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(downloadstatusFile, "DownloadStatusFile");
            Utils.ieSaveDownloadUsingSikuli();
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            if (Utils.isFileDownloaded(oldFile, newFile)) {
                return FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-upload");
            }

        } catch (Exception e) {
            Assertion.markAsFailure("Failed to download the status file");
        }
        return null;
    }

    public String getbatchID() {
        String id = batchId.getText();
        String[] v = id.split("-");

        pageInfo.info("batch Id is " + id);
        return v[0].toString();

    }

    public String getNoOfEntries() {
        String entries = noOfEntries.getText();
        return entries;
    }
}
