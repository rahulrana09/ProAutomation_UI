package framework.pageObjects.ambiguousTransaction;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

/**
 * Created by rahul.rana on 9/6/2017.
 */
public class AmbiguousTransaction_pg1 extends PageInit {

    @FindBy(id = "partyType")
    private WebElement selPartyType;

    @FindBy(id = "partyId")
    private WebElement selPartyName;

    @FindBy(name = "fromDateStr")
    private WebElement txtFromDate;

    @FindBy(name = "toDateStr")
    private WebElement txtToDate;

    @FindBy(css = "input[value='Download']")
    private WebElement btnDownload;
    @FindBy(xpath = "//*[@id='ambiguousTransaction_input']/table/tbody/tr[10]/td/a/img")
    private WebElement imgDownloadTemplate;
    @FindBy(xpath = "//td[contains(text(),'Download Log File')]/a")
    private WebElement imgDownloadLog;
    @FindBy(name = "ambiguousTXNUpload")
    private WebElement btnUploadFile;
    @FindBy(xpath = "//input[@value = 'Upload']")
    private WebElement btnUpload;


    public AmbiguousTransaction_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AmbiguousTransaction_pg1 init(ExtentTest t1) {
        return new AmbiguousTransaction_pg1(t1);
    }

    public void navAmbiguousTransaction() throws Exception {
        navigateTo("AMBGTXN_ALL", "AMBGTXN_AMBGTXN_UPLOAD", "Ambiguous Txn Page");
    }

    public void selectPartyType(String text) {
        selectIndex(selPartyType, 0, "Selecting index 0, as this select box is inconsistent");
        selectVisibleText(selPartyType, text, "selPartyType");
    }

    public void selectPartyName(String text) {
        selectVisibleText(selPartyName, text, "selPartyName");
    }

    public void selectFromDate(int day) throws Exception {
        String sDate = new DateAndTime().getDate(day);
        setDate(txtFromDate, sDate, "txtFromDate");

//        new Actions(driver).sendKeys(txtFromDate, sDate).perform();
    }

    public void selectToDate(int day) throws Exception {
        String sDate = new DateAndTime().getDate(day);
        //setDate(txtToDate, sDate, "txtToDate");
        new Actions(driver).sendKeys(txtToDate, sDate).build().perform();
    }

    public void downloadAmbiguousTransactionFile() throws InterruptedException {
        pageInfo.info("Deleting existing file with prefix - " + Constants.FILEPREFIX_BULK_AMBIGUOUS_TXN);
        FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, Constants.FILEPREFIX_BULK_AMBIGUOUS_TXN); // is hardcoded can be Generic TODO
        Thread.sleep(5000);
        clickOnElement(btnDownload, "btnDownload");
        //Utils.ieSaveDownloadUsingSikuli();
        pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
        Thread.sleep(5000);
    }

    public void clickImgDownloadTemplate() {
        clickOnElement(imgDownloadTemplate, "imgDownloadTemplate");
    }

    public void clickImgDownloadLog() throws InterruptedException {
        clickOnElement(imgDownloadLog, "imgDownloadLog");
        Thread.sleep(4000); // wait for file to be downloaded
    }

    public void setUploadFile(String path) {
        setText(btnUploadFile, path, "btnUploadFile");
    }

    public void clickBtnUpload() {
        clickOnElement(btnUpload, "btnUpload");
    }


}
