package framework.pageObjects.globalSearch;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class GlobalSearch_page2 extends PageInit {

    /****************************************
     *       P A G E  -  O B J E C T S
     ****************************************/

    @FindBy(name = "action:globalSearch_nextPage")
    WebElement nextPageButton;
    @FindBy(id = "globalSearch_back")
    WebElement detailsBackButton;
    @FindBy(id = "_back")
    WebElement resultPageBackButton;
    @FindBy(name = "action:globalSearch_previousPage")
    WebElement previousPageButton;
    @FindBy(id = "globalSearch_submit")
    WebElement finalSubmitButton;
    @FindBy(name = "button.reset")
    WebElement resetButton;


    public GlobalSearch_page2(ExtentTest test) {
        super(test);
    }

    /****************************************
     *     P A G E  -  O P E R A T I O N S
     ****************************************/

    public void navigateToLink() throws Exception {
        fl.leftNavigation("PARTY_ALL", "PARTY_RP_GOS");
        pageInfo.info("Navigate to Global Search");
    }

    public void clickOnNextPageButton() {
        nextPageButton.click();
        pageInfo.info("Clicked on NextButton button");
    }


    public void clickFinalSubmit() {
        finalSubmitButton.click();
        pageInfo.info("Clicked on Final Submit button");
    }

    public void clickOnPreviousPageButton() {
        previousPageButton.click();
        pageInfo.info("Clicked on Previous Page button");
    }

    public void clickResultPageBackButton() {
        resultPageBackButton.click();
        pageInfo.info("Clicked on Result Page Back button");
    }

    public void clickUserDetailsPageBackButton() {
        detailsBackButton.click();
        pageInfo.info("Clicked on User Details Page Back button");
    }

    public void selectRadioButton(String msisdn) {

        String userID = MobiquityGUIQueries.fetchUserIDFromUsers(msisdn);
        if (Utils.checkElementPresent(".//input[@type='radio' and @value = '" + userID + "']", Constants.FIND_ELEMENT_BY_XPATH)) {
            driver.findElement(By.xpath(".//input[@type='radio' and @value = '" + userID + "']")).click();
        } else if (Utils.checkElementPresent("action:globalSearch_nextPage", Constants.FIND_ELEMENT_BY_NAME)) {
            nextPageButton.click();
            driver.findElement(By.xpath(".//input[@type='radio' and @value = '" + userID + "']")).click();
        }
        pageInfo.info("Selecting the user.");

    }

    public void isSubmit1ButtonDisplayed() {
        if (Utils.checkElementPresent("globalSearch_loadData_button_submit", Constants.FIND_ELEMENT_BY_ID)) {
            pageInfo.pass("Successfully navigate to back page.");
        }
    }

    public void isSubmit2ButtonDisplayed() {
        if (Utils.checkElementPresent("globalSearch_submit", Constants.FIND_ELEMENT_BY_ID)) {
            pageInfo.pass("Successfully navigate to back page.");
        }
    }
}
