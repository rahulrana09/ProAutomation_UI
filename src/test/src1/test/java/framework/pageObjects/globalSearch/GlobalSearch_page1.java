package framework.pageObjects.globalSearch;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class GlobalSearch_page1 extends PageInit {

    /****************************************
     *       P A G E  -  O B J E C T S
     ****************************************/

    @FindBy(id = "globalSearch_loadData_button_submit")
    WebElement submitButton;
    @FindBy(id = "globalSearch_loadData_loginId")
    WebElement loginIdTbox;
    @FindBy(id = "globalSearch_loadData_msisdn")
    WebElement msisdnTbox;
    @FindBy(id = "globalSearch_loadData_firstName")
    WebElement firstNameTbox;
    @FindBy(id = "globalSearch_loadData_lastName")
    WebElement lastNameTbox;
    @FindBy(name = "dojo.dateOfCreation")
    WebElement creationTBox;
    @FindBy(name = "dojo.dateOfModification")
    WebElement modificationTBox;
    @FindBy(name = "dojo.dateOfLastTransaction")
    WebElement lastTransactionTBox;
    @FindBy(name = "dojo.dateOfLastConnection")
    WebElement lastLoginTBox;
    @FindBy(name = "button.reset")
    WebElement resetButton;
    //For Verification Elements
    @FindBy(xpath = "//*[@id='_systemparty_label_msisdnWeb']")
    WebElement usrLoginIDLabel;
    @FindBy(xpath = "//*[@id='_systemparty_label_userName']")
    WebElement usrFirstNameLabel;
    @FindBy(xpath = "//*[@id='_systemparty_label_lastName']")
    WebElement usrLastNameLabel;
    @FindBy(xpath = "//*[@id='_systemparty_label_msisdnUser']")
    WebElement usrMSISDNLabel;

    public GlobalSearch_page1(ExtentTest test) {
        super(test);
    }

    /****************************************
     *     P A G E  -  O P E R A T I O N S
     ****************************************/

    public void navigateToLink() throws Exception {
        navigateTo("PARTY_ALL", "PARTY_RP_GOS", "Global Search");
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    public void clickResetButton() {
        clickOnElement(resetButton, "Reset");
    }

    public void setLoginID(String val) {
        setText(loginIdTbox, val, "Login Id");
    }

    public void setmsisdn(String val) {
        setText(msisdnTbox, val, "MSISDN");
    }

    public String getMSISDN() {
        return msisdnTbox.getText();
    }

    public void setCreatedOn(String val) {
        setText(creationTBox, val, "Creation On");
    }

    public void setModifiedOn(String val) {
        setText(modificationTBox, val, "Modification ON");
    }

    public void setLastTransaction(String val) {
        setText(lastTransactionTBox, val, "Last Transaction");
    }

    public void setLastLogin(String val) {
        setText(lastLoginTBox, val, "Last Login");
    }

    //For verification methods
    public String getMSISDNText() {
        return usrMSISDNLabel.getText();
    }

    public String getLoginIDText() {
        return usrLoginIDLabel.getText();
    }

    public String getFirstName() {
        return usrFirstNameLabel.getText();
    }

    public void setFirstName(String val) {
        setText(firstNameTbox, val, "First Name");
    }

    public String getLastName() {
        return usrLastNameLabel.getText();
    }

    public void setLastName(String val) {
        setText(lastNameTbox, val, "Last name");
    }

    public void setSearchType(String searchType, String searchData){
        if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LOGINID))
            setLoginID(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_MSISDN))
            setmsisdn(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_FIRSTNAME))
            setFirstName(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LASTNAME))
            setLastName(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_CREATION))
            setCreatedOn(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION))
            setModifiedOn(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_TRANSACTION))
            setLastTransaction(searchData);
        else if (searchType.equalsIgnoreCase(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN))
            setLastLogin(searchData);
    }
}
