package framework.pageObjects.nonFinancialServices;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


/**
 * Created by rahul.rana on 7/4/2017.
 * Modified by automation.team
 */
public class EmailNotificationConfig_Page extends PageInit {

    @FindBy(id = "_update")
    WebElement updateButton;
    @FindBy(id = "_emailCheck")
    WebElement emailCheckBox;

    public EmailNotificationConfig_Page(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Change Pin Page
     *
     * @throws Exception
     */
    public EmailNotificationConfig_Page navigateToLink() throws Exception {
        fl.leftNavigation("NTF_CONFIG_ALL", "NTF_CONFIG_ALL");
        pageInfo.info("Navigate to Email Notification Configuration Page");

        return this;
    }

    public int getAllSMSCheckBoxCount() {
        List<WebElement> boxes = DriverFactory.getDriver().findElements(By.xpath("//input[@type='checkbox' and @checked='checked']"));
        return boxes.size();
    }

    public void checkValue() {
        if (emailCheckBox.isSelected()) {
            pageInfo.info("Checkbox is clicked..");
        } else {
            emailCheckBox.click();
        }
    }

    public void updateSmsEmailService(String serviceType, boolean sms, boolean email) throws Exception {

        WebElement SMS = driver.findElement(By.xpath("//input[@value = '" + serviceType + "-SMS" + "']"));
        WebElement EMAIL = driver.findElement(By.xpath("//input[@value = '" + serviceType + "-EMAIL" + "']"));

        if (sms && !email) {

            if (!SMS.isSelected()) {
                SMS.click();
                pageInfo.info("Selected SMS Service For Service Type: " + serviceType);
            }

            if (EMAIL.isSelected()) {
                EMAIL.click();
                pageInfo.info("Deselected EMAIL Service For Service Type: " + serviceType);
            }

        } else if (email && !sms) {

            if (!EMAIL.isSelected()) {
                EMAIL.click();
                pageInfo.info("Selected EMAIL Service For Service Type: " + serviceType);
            }

            if (SMS.isSelected()) {
                SMS.click();
                pageInfo.info("Deselected SMS Service For Service Type: " + serviceType);
            }

        } else if (sms && email) {

            if (!SMS.isSelected()) {
                SMS.click();
                pageInfo.info("Selected SMS Service For Service Type: " + serviceType);
            }

            if (!EMAIL.isSelected()) {
                EMAIL.click();
                pageInfo.info("Selected EMAIL Service For Service Type: " + serviceType);
            }

        } else {

            if (SMS.isSelected()) {
                SMS.click();
                pageInfo.info("Deselected SMS Service For Service Type: " + serviceType);
            }

            if (EMAIL.isSelected()) {
                EMAIL.click();
                pageInfo.info("Deselected EMAIL Service For Service Type: " + serviceType);
            }
        }
        Utils.scrollUntilElementIsVisible(SMS);
        Utils.captureScreen(pageInfo);
        clickUpdateButton();
        Thread.sleep(Constants.TWO_SECONDS);
        if (ConfigInput.isAssert) {
            Assertion.verifyContains(Assertion.getActionMessage(), "Number of notifictaions modified",
                    "Notification  Updated", pageInfo, true);
        }
    }


    public void clickUpdateButton() {
        updateButton.click();
        pageInfo.info("Clicked on Update Button");
    }
}
