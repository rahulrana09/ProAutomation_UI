package framework.pageObjects.menuManager;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import gherkin.lexer.Th;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

public class MenuServlet extends PageInit {

    @FindBy(xpath="//*[@id='mobileNO']")
    private WebElement mobileNumber;

    @FindBy(xpath="//*[@id='userInput']")
    private WebElement userInput;

    @FindBy(xpath="//input[@onclick='getOutPutXML();']")
    private WebElement goButton;

    @FindBy(xpath="//*[@id='outputMSG']")
    private WebElement outputMsg;

    public MenuServlet(ExtentTest t1) {
        super(t1);
    }

    public static MenuServlet init(ExtentTest t1) {
        return new MenuServlet(t1);
    }

    public void enterMobileNumber(String msisdn){
        pageInfo.info("Trying to enter msisdn: "+msisdn);
        mobileNumber.clear();
        mobileNumber.sendKeys(msisdn);
    }

    public void enterInput(String value){
        pageInfo.info("Trying to enter input: "+value);
        userInput.clear();
        userInput.sendKeys(value);
    }

    public void clickGoButton() throws InterruptedException{
        clickOnElement(goButton,"GO Button");
        Thread.sleep(2000);
    }

    public String getOutput() throws InterruptedException {
        pageInfo.info("Trying to get output message from servlet.");
        String output = outputMsg.getText();
        pageInfo.info("OUTPUT as : "+output);
        Thread.sleep(2000);
        return output;
    }
}
