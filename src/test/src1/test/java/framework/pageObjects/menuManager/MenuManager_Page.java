package framework.pageObjects.menuManager;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;
import java.util.List;

public class MenuManager_Page extends PageInit {

    @FindBy(id = "idGatherInputLink")
    private WebElement gatherInput;

    @FindBy(xpath = "//td/div[@id='headerTextAreaDiv']/textarea[@id='headerTextValue']")
    private WebElement headerTextArea;

    @FindBy(xpath = "//img[@id='saveHeaderTextId']")
    private WebElement saveHeaderText;

    @FindBy(xpath = "//div[@id='headerEntireTextAreaDiv']//img[@id='editLanguageId']")
    private WebElement editLanguage;

    @FindBy(xpath = "//button[@class='popupFooterBtn'][@onclick='saveAddMenuLanguage()']")
    private WebElement saveBtn;

    @FindBy(id = "labelName")
    private WebElement labelName;

    @FindBy(xpath = "//button[@id='editSaveGatherInput']")
    private WebElement saveeditMenu;

    @FindBy(xpath = "//button[@id='saveGatherInput']")
    private WebElement saveMenu;


    @FindBy(xpath = "//img[@title='Define in Other Language']")
    private WebElement defineInOthLang;

    @FindBy(xpath = "//button[text()='Field Name']")
    private WebElement fieldName;

    @FindBy(xpath = "//input[@placeholder='Enter Field Name']")
    private WebElement enterfieldname;

    @FindBy(xpath = "//button[@onclick='savePlaceholder();']")
    private WebElement savePlaceHolder;

    @FindBy(xpath="//button[@onclick[contains(.,'validateAndForwardG')]]")
    private WebElement nextBtn;

    @FindBy(xpath = "//*[@onclick='submitForApprovals();']")
    private WebElement submitForApprovalBtn;

    @FindBy(xpath = "//img[@src='../images/calendar.png']")
    private WebElement selectCalender;

    @FindBy(xpath = "//td[@class='day active']")
    private WebElement selectActiveDate;

    @FindBy(xpath = "//textarea[@id='remarks']")
    private WebElement remarks;

    @FindBy(xpath = "//button[@onclick='submitForApprovals1();']")
    private WebElement sendforapproval;

    @FindBy(xpath = "//div[@class='modal fade in']//button[@data-dismiss='modal']")
    private WebElement closeWindow;

    @FindBy(xpath="//td//button[@id='idAssSubMenuLink2']")
    private WebElement staticMenuList;

    @FindBy(xpath="//*[@onclick='discardAll();']")
    private WebElement discardAll;

    @FindBy(xpath="//*[@id='alertify-ok']")
    private WebElement alertOKBtn;

    @FindBy(xpath="//*[@id='takeRegEx']")
    private WebElement validationBtn;

    @FindBy(xpath="//*[@id='ErrorMsgId']")
    private WebElement errorMessageBtn;

    @FindBy(xpath="//*[@id='gatherInputPrefixId']")
    private WebElement prefixBtn;

    @FindBy(xpath="//*[@id='inputValidationCondtionDropDown']")
    private WebElement validationCheck;

    @FindBy(xpath="//*[@id='inputFirstValueId']")
    private WebElement valueForValidation;

    @FindBy(xpath="//*[@id='inputValErrorId']")
    private WebElement errorMsgForValidation;

    @FindBy(xpath="//*[@id='regExTable']//*[@onclick='addGatherInputValidationRow()']")
    private WebElement addValidation;

    @FindBy(xpath="//*[@id='saveInputValId']")
    private WebElement saveInput;

    @FindBy(xpath="//*[@onclick='closeRegEx();']")
    private WebElement closeRegex;

    @FindBy(xpath="//*[@id='otherLangValErrMsgId']")
    private WebElement validateErrMsgLang;

    @FindBy(xpath="//*[@id='gatherErrorMsgId']")
    private WebElement enterErrorMsg;

    @FindBy(xpath="//*[@id='errMsgTable']//..//*[@id='editLanguageId']")
    private WebElement editLangForErrMsg;

    @FindBy(xpath="//*[@id='errorMsgDiv']//*[@onclick='saveErrorMsg();']")
    private WebElement saveErrorMsgBtn;

    @FindBy(xpath="//*[@id='prefixId']")
    private WebElement prefixRemoval;

    @FindBy(xpath="//*[@id='gatherPrefixDiv']//*[@onclick='savePrefix();']")
    private WebElement savePrefixBtn;

    public MenuManager_Page(ExtentTest t1) {
        super(t1);
    }

    public static MenuManager_Page init(ExtentTest t1) {
        return new MenuManager_Page(t1);
    }


    public void selectCategory(String category) {
        pageInfo.info("Trying to select Category: "+category);
        driver.findElement(By.xpath("//button[text()='" + category.toUpperCase() + "']")).click();
    }

    public void clickGatheruserInput() {
        pageInfo.info("Trying to click Gather User Input link.");
        gatherInput.click();
    }

    public void enterText(String text) throws InterruptedException {
        Thread.sleep(2000);
        pageInfo.info("Enter header text: "+text);
        headerTextArea.clear();
        headerTextArea.sendKeys(text);
    }

    public void saveHeaderText() {
        pageInfo.info("Trying to save header.");
        saveHeaderText.click();
    }

    public void editLanguage() {
        pageInfo.info("Trying to click edit language.");
        editLanguage.click();
    }

    public void enterInOtherLanguage(String textMsg) {
        pageInfo.info("Trying to enter text message for other languages: "+textMsg);
        List<WebElement> lang = driver.findElements(By.xpath("//table[@id='addMenuLanguageTable']//input[@class='form-control'][@type='text']"));
        for (WebElement x : lang) {
            x.clear();
            x.sendKeys(textMsg);
        }
    }

    public void clickOnSaveBtn() {
        pageInfo.info("Trying to click save button.");
        saveBtn.click();
    }

    public void enterMenuLabel(String text) {
        pageInfo.info("Trying to enter menu label: "+text);
        labelName.sendKeys(text);
    }

    public void saveMenu() {
        pageInfo.info("Trying to click save button.");
        try {
            saveMenu.click();
        } catch (Exception e) {
            try {
                saveeditMenu.click();
            } catch (Exception e1) {
                pageInfo.info("Element not found");
            }
        }
    }

    public void clickDefineInOtherlang() {
        pageInfo.info("Trying to click other language for menu label");
        defineInOthLang.click();
    }

    public void clickFieldNameBtn() {
        pageInfo.info("Trying to click field name");
        fieldName.click();
    }

    public void enterFieldName(String text) {
        pageInfo.info("Trying to enter field name as :"+text);
        enterfieldname.clear();
        enterfieldname.sendKeys(text);
    }

    public void clickSavePlaceholder() {
        pageInfo.info("Trying to click save for place holder");
        savePlaceHolder.click();
    }

    public void clickSubmitForApproval() {
        pageInfo.info("Trying to click link 'Submit For Appproval'.");
        submitForApprovalBtn.click();
    }

    public void clickCalenderIcon() {
        pageInfo.info("Trying to click Calendar Icon.");
        selectCalender.click();
    }

    public void clickToSelectActiveDate() {
        pageInfo.info("Trying to select active date in calendar");
        selectActiveDate.click();
    }

    public void enterRemarks(String text) {
        pageInfo.info("Trying to enter remarks: "+text);
        remarks.clear();
        remarks.sendKeys(text);
    }

    public void clickSubmitBtnToApprove() {
        pageInfo.info("Trying to click submit button for sending menu for approval.");
        sendforapproval.click();
    }

    public void closepopupwindow() {
        pageInfo.info("Trying to close the pop-up window");
        closeWindow.click();
    }

    public void clickNextBtn(){
        pageInfo.info("Trying to click Next button.");
        nextBtn.click();
    }

    public void clickAssociateStaticMenuList(){
        pageInfo.info("Trying to click Associate Static Menu List link");
        clickOnElement(staticMenuList,"Associate Static Menu link");
    }

    public void clickDiscardAllLink() throws InterruptedException{
        clickOnElement(discardAll,"Discard All button");
        Thread.sleep(3000);
        pageInfo.info("Trying to click OK button.");
        clickOnElement(alertOKBtn,"Alert OK button");
        Thread.sleep(3000);
    }

    public void selectDate(int days){
        if(days==0)
            clickToSelectActiveDate();
        else{
            pageInfo.info("Trying to select future date in calendar");
            WebElement fdate = driver.findElement(By.xpath("//td[@class='day active']/following-sibling::td["+days+"]"));
            clickOnElement(fdate,"Future date");
        }
    }

    public void clickValidationBtn(){
        clickOnElement(validationBtn,"Validation Button");
    }

    public void selectValidationcheck(String value){
        Select valCheck = new Select(validationCheck);
        valCheck.selectByValue(value);
    }

    public void enterValueforValidation(String Value){
        pageInfo.info("Trying to Enter validation value:"+Value);
        valueForValidation.sendKeys(Value);
    }

    public void enterErrorMessagForValidation(String errorMsg){
        pageInfo.info("Trying to Enter error message for validation:"+errorMsg);
        errorMsgForValidation.sendKeys(errorMsg);
    }

    public void clickErrorMessageBtn(){
        clickOnElement(errorMessageBtn,"Error Message Button");
    }

    public void enterErrorMessage(String text){
        pageInfo.info("Trying to enter error message:"+text);
        enterErrorMsg.sendKeys(text);
    }

    public void clickPrefixBtn(){
        clickOnElement(prefixBtn,"Prefix Button");
    }

    public void clickButtonForLangInValidation(){
        clickOnElement(validateErrMsgLang,"Define in Other Language");
    }

    public void clickSaveInput(){
        clickOnElement(saveInput,"Save Input");
    }

    public void clickCloseBtnForValidation(){
        clickOnElement(closeRegex,"Close Button");
    }

    public void clickErrorMsgEditLang(){
        clickOnElement(editLangForErrMsg,"Edit language for Error Message");
    }

    public void clickSaveErrorMessageBtn(){
        clickOnElement(saveErrorMsgBtn,"Save Error Message Button");
    }

    public void enterPrefixRemoval(String text){
        prefixRemoval.clear();
        prefixRemoval.sendKeys(text);
    }

    public void clickSavePrefixButton(){
        clickOnElement(savePrefixBtn,"Save Prefix Button");
    }
}
