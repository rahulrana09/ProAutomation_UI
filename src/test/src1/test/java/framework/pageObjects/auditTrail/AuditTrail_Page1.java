package framework.pageObjects.auditTrail;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;


public class AuditTrail_Page1 extends PageInit {

    @FindBy(id = "sel")
    WebElement domainName;

    @FindBy(name = "categoryCode")
    WebElement categoryName;

    @FindBy(name = "dojo.fromDateField")
    WebElement fromDate;

    @FindBy(name = "dojo.toDateField")
    WebElement toDate;

    @FindBy(id = "auditTrail_input_button_submit")
    WebElement submitButton;

    @FindBy(name = "button.download")
    private WebElement downloadButton;

    @FindBy(id = "table")
    WebElement webTable;

    public AuditTrail_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AuditTrail_Page1 init(ExtentTest t1) {
        return new AuditTrail_Page1(t1);
    }

    // navigate to link
    public AuditTrail_Page1 navigateToLink() throws Exception {
        navigateTo("AUDITTRAIL_ALL", "AUDITTRAIL_AUD_TRAIL", "Audit Trail");
        return this;
    }

    public AuditTrail_Page1 domainName_SelectIndex(int index) {
        selectIndex(domainName, index, "domainName");
        return this;
    }

    public AuditTrail_Page1 domainName_SelectText(String text) {
        selectVisibleText(domainName, text, "domainName");
        return this;
    }

    public AuditTrail_Page1 selectDomainByValue(String val) throws Exception {
        selectValue(domainName, val, "domainName");
        Thread.sleep(1200);
        return this;
    }

    public AuditTrail_Page1 selectCategoryByValue(String text) {
        selectValue(categoryName, text, "categoryName");
        return this;
    }

    public AuditTrail_Page1 categoryName_SelectIndex(int index) {
        selectIndex(categoryName, index, "categoryName");
        return this;
    }

    public AuditTrail_Page1 categoryName_SelectText(String text) {
        selectVisibleText(categoryName, text, "categoryName");
        return this;
    }

    public AuditTrail_Page1 fromDateSetText(String text) {
        setDate(fromDate, text, "fromDate");
        return this;
    }

    public AuditTrail_Page1 toDateSetText(String text) {
        setDate(toDate, text, "toDate");
        return this;
    }

    public AuditTrail_Page1 submitButtonClick() {
        clickOnElement(submitButton, "submitButton");
        return this;
    }

    public boolean downloadButtonClick() throws Exception {
        String oldfile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        clickOnElement(downloadButton, "downloadButton");
        if (ConfigInput.isInternetExplorer) {
            Utils.ieSaveDownloadUsingSikuli();
        }
        String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        return Utils.isFileDownloaded(oldfile, newFile);
    }

    public boolean checkIfTxnIdIsAvailable(String txnId) {
        return isStringPresentInWebTable(webTable, txnId);
    }

    //============================used in p1=======================//
    public WebElement verifyFieldAudit(String fieldName) {
        return driver.findElement(By.xpath("//table[@id= 'table']/thead/tr/th[contains(text(),'" + fieldName + "')]"));
    }

    public List<WebElement> checkTXNid() {
        List<WebElement> txnIDs = driver.findElements(By.xpath("//table[@id ='table']/tbody/tr/td[1]"));

        return txnIDs;
    }
}
