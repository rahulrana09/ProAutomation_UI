package framework.pageObjects.auditTrail;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


public class AdminTrail_Page1 extends PageInit {

   /* private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;

    public static AdminTrail_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        AdminTrail_Page1 page = PageFactory.initElements(driver, AdminTrail_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }*/

    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/

    @FindBy(id = "sel")
    WebElement DomainName;
    @FindBy(xpath = "//*[@name='categoryCode']")
    WebElement CatName;
    @FindBy(xpath = "//*[@name='dojo.fromDateField']")
    WebElement FromDate;
    @FindBy(xpath = "//*[@name='dojo.toDateField']")
    WebElement ToDate;
    @FindBy(id = "adminTrail_input_button_submit")
    WebElement Submit;
    @FindBy(id = "displ_button_download")
    WebElement downloadButton;
    @FindBy(id = "table")
    WebElement webTable;

    public AdminTrail_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AdminTrail_Page1 init(ExtentTest t1) {
        return new AdminTrail_Page1(t1);
    }

    /***************************************************************************
     * ############################ Page Operations #############################
     ***************************************************************************/

    public void navigateToLink() throws Exception {
        navigateTo("AUDITTRAIL_ALL", "AUDITTRAIL_ADM_TRAIL", "Admin Trail");
    }

    public void domainName_SelectIndex(int index) throws Exception {
        Select s_domainName = new Select(DomainName);
        s_domainName.selectByIndex(index);
        Thread.sleep(2000);
    }

    public void domainName_SelectText(String text) throws Exception {
        Select s_domainName = new Select(DomainName);
        s_domainName.selectByVisibleText(text);
        pageInfo.info("Select domain as: " + text);
        Thread.sleep(2000);
    }

    public void selectDomainByValue(String val) throws Exception {
        Select s_domainName = new Select(DomainName);
        s_domainName.selectByValue(val);
        pageInfo.info("Select domain as: " + val);
        Thread.sleep(2000);
    }

    public void categoryName_SelectIndex(int index) {
        Select s_categoeryName = new Select(CatName);
        s_categoeryName.selectByIndex(index);
    }

    public void categoryName_SelectText(String text) {
        Select s_categoeryName = new Select(CatName);
        s_categoeryName.selectByVisibleText(text);
        pageInfo.info("Select Category as: " + text);
    }

    public void selectCategoryByValue(String text) {
        Select s_categoeryName = new Select(CatName);
        s_categoeryName.selectByValue(text);
        pageInfo.info("Select Category as: " + text);
    }

    public void fromDateSetText(String text) {
        FromDate.clear();
        FromDate.sendKeys(text);
        pageInfo.info("Select From Date: " + text);

    }

    public void toDateSetText(String text) {
        ToDate.clear();
        ToDate.sendKeys(text);
        pageInfo.info("Select To Date: " + text);
    }

    public void submitButtonClick() {
        Submit.click();
        pageInfo.info("Click on Submit Button");

    }

    //=============================used in p1=========================//

    public boolean downloadButtonClick() throws Exception {
        String oldfile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        clickOnElement(downloadButton, "downloadButton");
        if (ConfigInput.isInternetExplorer) {
            Utils.ieSaveDownloadUsingSikuli();
        }
        String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
        return Utils.isFileDownloaded(oldfile, newFile);
    }

    public boolean checkIfUsernameIsAvailable(String txnId) {
        return isStringPresentInWebTable(webTable, txnId);
    }

    public List<WebElement> getTableHeaders() {
        List<WebElement> list = driver.findElements(By.xpath(".//*[contains(@class,'tabhead')]"));
        return list;
    }

}
