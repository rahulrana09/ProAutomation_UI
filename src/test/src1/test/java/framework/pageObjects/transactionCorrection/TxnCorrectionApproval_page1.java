package framework.pageObjects.transactionCorrection;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;

public class TxnCorrectionApproval_page1 extends PageInit {


    @FindBy(name = "button.submit")
    public WebElement submitButton;

    @FindBy(xpath = "//input[contains(@id,'button_approve')]")
    public WebElement approveButton;

    @FindBy(xpath = "//input[contains(@id,'button_reject')]")
    public WebElement rejectButton;

    @FindBy(className = "wwFormTableC")
    private WebElement appTable;


    public TxnCorrectionApproval_page1(ExtentTest t1) {
        super(t1);
    }

    public static TxnCorrectionApproval_page1 init(ExtentTest t1) {
        return new TxnCorrectionApproval_page1(t1);
    }

    public void navigateToTxnCorrApproval() throws Exception {
        navigateTo("TXN_CORR_ALL", "TXN_CORR_TXN_CORRAPP", "Transaction correction Approval");
    }

    public void selectTransactionId(String transactionId) {
        WebElement elem = wait.until(ExpectedConditions
                .elementToBeClickable(driver.findElement(By.xpath("//*[@value='" + transactionId + "' and @type='radio']"))));

        clickOnElement(elem, "Radio: " + transactionId);
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    public void clickApproveButton() {
        clickOnElement(approveButton, "Approve Button");
    }

    public void clickRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }

    public String getTransactionIdUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Transaction ID");
    }

    public String getTransferAmountUI() throws IOException {
        String amt = getTableDataUsingHeaderName(appTable, "Transfer Value");
        return amt.split(" ")[0].trim();
    }

    public String getActualTransactionIdUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Actual Transaction Id");
    }

    public String getServiceTypeUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Actual Transaction Service Type");
    }

    public String getPayerMsisdnUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Payer MSISDN");
    }

    public String getPayerDomainUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Payer Domain");
    }

    public String getPayeeMsisdnUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Payee MSISDN");
    }

    public String getPayeeDomainUI() throws IOException {
        return getTableDataUsingHeaderName(appTable, "Payee Domain");
    }
}
