package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ServiceCharge_Pg1 extends PageInit {


    // Page objects
    @FindBy(id = "profileName")
    public WebElement profileName;

    @FindBy(id = "shortCode")
    WebElement ShortCode;

    @FindBy(name = "dojo.applicableFromStr")
    WebElement Date;

    @FindBy(id = "selectMFSSenderProviderId")
    WebElement SenderProvider;

    @FindBy(id = "senderPaymentInstrumentId")
    WebElement SenderInstrument;

    @FindBy(id = "senderLinkedBanksOrWalletTypesId")
    WebElement SenderInstrumentType;

    @FindBy(id = "selectMFSReceiverProviderId")
    WebElement ReceiverProvider;

    @FindBy(id = "receiverPaymentInstrumentId")
    WebElement ReceiverInstrument;

    @FindBy(id = "receiverLinkedBanksOrWalletTypesId")
    WebElement ReceiverInstrumentType;

    @FindBy(id = "paymentTypeServicesId")
    WebElement ServiceType;

    @FindBy(name = "Next")
    WebElement Next;

    @FindBy(className = "errorMessage")
    WebElement ErrorMessage;

    @FindBy(className = "actionMessage")
    WebElement ActionMessage;


    public ServiceCharge_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static ServiceCharge_Pg1 init(ExtentTest t1) {
        return new ServiceCharge_Pg1(t1);
    }

    public ServiceCharge_Pg2 clickNext() {
        clickOnElement(Next, "Next");
        return new ServiceCharge_Pg2(pageInfo);
    }

    public ServiceCharge_Pg1 setShortCode(String code) {
        setText(ShortCode, code, "ShortCode");
        return this;
    }

    public ServiceCharge_Pg1 setDate(String date) {
        setText(Date, date, "Date");
        return this;
    }

    public ServiceCharge_Pg1 selectSenderProvider(String provider) throws Exception {
        selectVisibleText(SenderProvider, provider, "SenderProvider");
        return this;
    }

    public ServiceCharge_Pg1 selectSenderInstrument(String instrumentName) throws InterruptedException {
        selectVisibleText(SenderInstrument, instrumentName, "SenderInstrument");
        return this;
    }

    public ServiceCharge_Pg1 selectSenderInstrumentType(String instrumentType) throws InterruptedException {
        SenderInstrumentType.click();
        Thread.sleep(2000);
        selectVisibleText(SenderInstrumentType, instrumentType, "SenderInstrumentType");
        return this;
    }

    public ServiceCharge_Pg1 selectReceiverProvider(String provider) {
        selectVisibleText(ReceiverProvider, provider, "ReceiverProvider");
        return this;
    }

    public ServiceCharge_Pg1 selectReceiverInstrument(String instrumentName) {
        selectVisibleText(ReceiverInstrument, instrumentName, "ReceiverInstrument");
        return this;
    }

    public ServiceCharge_Pg1 selectReceiverInstrumentType(String instrumentType) {
        selectVisibleText(ReceiverInstrumentType, instrumentType, "ReceiverInstrumentType");
        return this;
    }

    public ServiceCharge_Pg1 selectServiceType(String service) throws Exception {
        Thread.sleep(1200);
        selectValue(ServiceType, service, "ServiceType");
        return this;
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public ServiceCharge_Pg1 navAddServiceCharge() throws Exception {
        navigateTo("SERVCHARG_ALL", "SERVCHARG_SVC_ADD", "Add Service Charge");
        return this;
    }

    public WebElement getProfileName() {
        return profileName;
    }

    public ServiceCharge_Pg1 setProfileName(String name) {
        setText(profileName, name, "profileName");
        return this;
    }
}
