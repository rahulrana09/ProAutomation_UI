package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 10/24/2018.
 */
public class NonFinancialServiceChargeCalculator_Page1 extends PageInit {

    @FindBy(id = "selectCategoryId")
    private WebElement categoryDDown;
    @FindBy(id = "selectMFSSenderProviderId")
    private WebElement senderProviderDDown;
    @FindBy(id = "senderPaymentInstrumentId")
    private WebElement senderInstrumentDDown;
    @FindBy(id = "senderLinkedBanksOrWalletTypesId")
    private WebElement senderInstrumentTypeDDown;
    @FindBy(id = "paymentTypeServicesId")
    private WebElement serviceTypeDDown;
    @FindBy(id = "selectMFSReceiverProviderId")
    private WebElement receiverProviderId;
    @FindBy(id = "selectSenderGradeId")
    private WebElement senderGradeDDown;
    @FindBy(id = "chargingCalc_loadVersionListCalc_submit")
    private WebElement submitButton;

    public NonFinancialServiceChargeCalculator_Page1(ExtentTest t1) {
        super(t1);
    }

    public static NonFinancialServiceChargeCalculator_Page1 init(ExtentTest t1) {
        return new NonFinancialServiceChargeCalculator_Page1(t1);
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public NonFinancialServiceChargeCalculator_Page1 navigateServiceChargeCalculator() throws Exception {
        navigateTo("NONFIN_ALL", "NONFIN_CHARGENON_CALC", "View Non Financial Service Charge");
        return this;
    }

    public NonFinancialServiceChargeCalculator_Page1 selectSenderProvider(String provider) throws Exception {
        selectVisibleText(senderProviderDDown, provider, "SenderProvider");
        return this;
    }

    public NonFinancialServiceChargeCalculator_Page1 selectSenderInstrument(String instrumentName) throws InterruptedException {
        selectVisibleText(senderInstrumentDDown, instrumentName, "SenderInstrument");
        return this;
    }

    public NonFinancialServiceChargeCalculator_Page1 selectSenderInstrumentType(String instrumentType) throws InterruptedException {
        selectVisibleText(senderInstrumentTypeDDown, instrumentType, "SenderInstrumentType");
        return this;
    }

    /**
     * This method is used to select Service Type
     *
     * @param service
     * @return
     * @throws Exception
     */
    public NonFinancialServiceChargeCalculator_Page1 selectServiceType(String service) throws Exception {
        Thread.sleep(1200);
        selectValue(serviceTypeDDown, service, "ServiceType");
        return this;
    }

    /**
     * This method is used  to click on Submit Button..
     *
     * @return
     */
    public NonFinancialServiceChargeCalculator_Page1 clickSubmit() {
        clickOnElement(submitButton, "Submit");
        return new NonFinancialServiceChargeCalculator_Page1(pageInfo);
    }

    /**
     * This method is used to select Sender Grade.
     *
     * @param gradeCode
     */
    public void selectSenderGrade(String gradeCode) {
        selectValue(senderGradeDDown, gradeCode, "Sender Grade");
    }

    public void selectCategory(String categoryCode) {
        selectValue(categoryDDown, categoryCode, "Category Code");
    }
}
