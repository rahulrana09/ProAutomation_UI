package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by rahul.rana on 6/22/2017.
 */
public class ServiceChargeDeleteModify_Pg1 extends PageInit {


    @FindBy(id = "selectServSetID")
    private WebElement SelectServicCharge;

    @FindBy(id = "confirm_loadDetail_delete")
    private WebElement ConfirmDelete;

    @FindBy(id = "confirm_loadDetail_update")
    private WebElement BtnUpdate;

    @FindBy(id = "confirmModify_confirmModify_servChrgMap_serviceChargeName")
    private WebElement ModifyServiceName;

    @FindBy(id = "confirmModify_confirmModify_servChrgMap_shortCode")
    private WebElement ModifyServiceId;

    @FindBy(name = "dojo.applicableFromStr")
    private WebElement ModifyServiceApplicableDate;

    @FindBy(name = "applicableFromStr")
    private WebElement ModifyServiceApplicableDateHidden;

    @FindBy(id = "confirmModify_confirmModify_button_save")
    private WebElement SaveModify;

    @FindBy(id = "confirmModify_modify_button_confirm")
    private WebElement ConfirmModify;


    public ServiceChargeDeleteModify_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static ServiceChargeDeleteModify_Pg1 init(ExtentTest t1) {
        return new ServiceChargeDeleteModify_Pg1(t1);
    }

    public ServiceChargeModify_Pg2 clickUpdate() {
        clickOnElement(BtnUpdate, "Button Update");
        return new ServiceChargeModify_Pg2(pageInfo);
    }

    public void modifyProfileName(String name) {
        setText(ModifyServiceName, name, "Modify ProfileName");
    }

    public void modifyProfileId(String id) {
        setText(ModifyServiceId, id, "Modify Service Id");
    }

    public void modifyApplicableDate(String date) {
        setText(ModifyServiceApplicableDate, date, "Applicable Date");
    }

    public void clickSaveModify() {
        clickOnElement(SaveModify, "Save Modify");
    }

    public ServiceChargeDeleteModify_Pg1 selectServiceCharge(String sName) {
        Select sel = new Select(SelectServicCharge);
        fl.selectOptionByPartialText(sel, sName);
        pageInfo.info("Select Service Charge:" + sName);
        return this;
    }

    public int getActiveVersionCount(String sName) {
        int counter = 0;
        Select sel = new Select(SelectServicCharge);
        List<WebElement> options = sel.getOptions();
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i).getText().contains(sName)) {
                counter++;
            }
        }

        return counter;
    }

    public void clickConfirmUpdate() {
        clickOnElement(ConfirmModify, "Confirm Modify");
    }

    public ServiceChargeDeleteModify_Pg1 navModifyDeleteServiceCharge() throws Exception {
        navigateTo("SERVCHARG_ALL", "SERVCHARG_SVC_MOD", "Modify /Delete Service Charge");
        return this;
    }

    public void clickConfirmDelete() {
        clickOnElement(ConfirmDelete, "Confirm Delete");
    }

    public WebElement getSelectServicCharge() {
        return SelectServicCharge;
    }

    public void setSelectServicCharge(WebElement selectServicCharge) {
        SelectServicCharge = selectServicCharge;
    }

    public WebElement getConfirmDelete() {
        return ConfirmDelete;
    }

    public void setConfirmDelete(WebElement confirmDelete) {
        ConfirmDelete = confirmDelete;
    }

    public WebElement getBtnUpdate() {
        return BtnUpdate;
    }

    public void setBtnUpdate(WebElement btnUpdate) {
        BtnUpdate = btnUpdate;
    }

    public WebElement getModifyServiceName() {
        return ModifyServiceName;
    }

    public void setModifyServiceName(WebElement modifyServiceName) {
        ModifyServiceName = modifyServiceName;
    }

    public WebElement getModifyServiceId() {
        return ModifyServiceId;
    }

    public void setModifyServiceId(WebElement modifyServiceId) {
        ModifyServiceId = modifyServiceId;
    }

    public WebElement getModifyServiceApplicableDate() {
        return ModifyServiceApplicableDate;
    }

    public void setModifyServiceApplicableDate(WebElement modifyServiceApplicableDate) {
        ModifyServiceApplicableDate = modifyServiceApplicableDate;
    }

    public WebElement getModifyServiceApplicableDateHidden() {
        return ModifyServiceApplicableDateHidden;
    }

    public void setModifyServiceApplicableDateHidden(WebElement modifyServiceApplicableDateHidden) {
        ModifyServiceApplicableDateHidden = modifyServiceApplicableDateHidden;
    }

    public WebElement getSaveModify() {
        return SaveModify;
    }

    public void setSaveModify(WebElement saveModify) {
        SaveModify = saveModify;
    }

    public WebElement getConfirmModify() {
        return ConfirmModify;
    }

    public void setConfirmModify(WebElement confirmModify) {
        ConfirmModify = confirmModify;
    }
}
