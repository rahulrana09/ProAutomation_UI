package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by gurudatta.praharaj on 6/14/2018.
 */
public class ModifyNewSubscriberCommRule extends PageInit {

    @FindBy(id = "selectCommRuleId")
    private WebElement service;
    @FindBy(id = "subsComRule_loadSubsCommDetail_update")
    private WebElement update;
    @FindBy(id = "subsComRule_loadSubsCommDetail_view")
    private WebElement modify;

    @FindBy(id = "subsComRule_confirmSubsCommissionRule_commRuleName")
    private WebElement subCommRule;

    public ModifyNewSubscriberCommRule(ExtentTest test) {
        super(test);
    }

    public static ModifyNewSubscriberCommRule init(ExtentTest test) {
        return new ModifyNewSubscriberCommRule(test);
    }

    public ModifyNewSubscriberCommRule navToModifyNewSubCommRule() throws Exception {
        navigateTo("SERVCHARG_ALL", "SERVCHARG_SVC_SUBCOM", "Modify New Sub Comm Rule");
        return this;
    }

    public ModifyNewSubscriberCommRule selectService(String value) {
        selectValue(service, value, "Service Name");
        return this;
    }

    public ModifyNewSubscriberCommRule clickUpdate() {
        clickOnElement(update, "Update Button");
        return this;
    }

    public ModifyNewSubscriberCommRule viewDetails() {
        clickOnElement(modify, "View Details Button");
        return this;
    }

    public String getSubCommRule(){
        return getElementAttributeValue(subCommRule,"value","Subscriber Commission Rule");
    }
}
