package framework.pageObjects.serviceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class NewSubsCommissionRule_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    // Page objects
    @FindBy(name = "commRuleName")
    WebElement commRuleNameTbox;
    @FindBy(name = "count")
    WebElement countTbox;
    @FindBy(name = "validityDays")
    WebElement validityDaysTbox;
    @FindBy(name = "roleList1")
    WebElement checkAll;
    @FindBy(name = "dojo.applicableFromStr")
    WebElement Date;
    @FindBy(xpath = "//input[contains(@id,'button_save')]")
    WebElement saveButton;

    public static NewSubsCommissionRule_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NewSubsCommissionRule_Page1 page = PageFactory.initElements(driver, NewSubsCommissionRule_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }



    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public void navigateToSubsCommRule() throws Exception {
        fl.leftNavigation("SERVCHARG_ALL", "SERVCHARG_SVC_ADDSUBCOM");
        pageInfo.info("Navigate to New Subscriber Commission Rule Page");
    }


    /**
     * Set the Profile name
     *
     * @param name
     */
    public void setCommRuleName(String name) {
        commRuleNameTbox.clear();
        commRuleNameTbox.sendKeys(name);
        pageInfo.info("Set Commission Rule Name: " + name);
    }

    public void setCount(String count) {
        countTbox.clear();
        countTbox.sendKeys(count);
        pageInfo.info("Set Count: " + count);
    }

    public void setValidityDays(String days) {
        validityDaysTbox.clear();
        validityDaysTbox.sendKeys(days);
        pageInfo.info("Set Validity Days: " + days);
    }

    /**
     * Set Date
     *
     * @param date
     */
    public void setDate(String date) {
        Date.clear();
        Date.sendKeys(date);
        pageInfo.info("Set Date: " + date);
    }


    /**
     * Click on save
     */
    public void clickSaveButton() {
        saveButton.click();
        pageInfo.info("Clicked on Save Button");
    }


    /**
     * Click on check all
     */
    public void clickCheckAll() {
        checkAll.click();
        pageInfo.info("Clicked on Check All ");
    }

    public void clickParticularService(String value){
        driver.findElement(By.xpath("//input[@type='checkbox'][@name='checkboxforserv'][@value='"+value+"']")).click();
    }

    public ArrayList<String> getServicesOnPage(){
        List<WebElement> services = driver.findElements(By.xpath("//input[@type='checkbox'][@name='checkboxforserv']"));
        ArrayList<String> serviceCh = new ArrayList<>();
        for(WebElement x:services){
            serviceCh.add(x.getAttribute("value"));
        }
        return serviceCh;
    }

}
