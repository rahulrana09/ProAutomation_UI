package framework.pageObjects;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StatusMessage {

    //Page Objects
    @FindBy(className = "actionMessage")
    static WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    static WebElement ErrorMessage;
    @FindBy(className = "error_Message")
    static WebElement ErrorMessage1;
    private static WebDriver driver;
    private static WalletPreferences_Pg1 page;

    public static StatusMessage init(ExtentTest t1) {
        driver = DriverFactory.getDriver();
        return PageFactory.initElements(driver, StatusMessage.class);
    }

}
