/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Automation Team
 *  Date: 26-May-2017
 *  Purpose: Page Object
 */
package framework.pageObjects.bankMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.NumberConstants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DeleteBank_Page1 extends PageInit {

    @FindBy(name = "bankId")
    private WebElement selectBank;
    @FindBy(name = "submit")
    private WebElement btnSubmitPg1;
    @FindBy(id = "bankMasterActionDetete_finalDeleteBank_submit")
    private WebElement btnSubmitPg2;

    public DeleteBank_Page1(ExtentTest t1) {
        super(t1);
    }

    public DeleteBank_Page1 navDeleteBank() throws Exception {
        fl.leftNavigation("BANK_ALL", "BANK_BANKDELETE");
        pageInfo.info("Navigate Bank Master > Delete Bank");
        return this;
    }

    /**
     * getBankOptionsfromSelect
     *
     * @return list of Banks from Dropdown
     */
    public List<String> getBankOptionsfromSelect() {
        Utils.putThreadSleep(NumberConstants.SLEEP_2000);
        return fl.getOptionText(selectBank);
    }

    /**
     * selectBankForDeletion
     *
     * @param bank bank name
     * @return Current instance
     */
    public DeleteBank_Page1 selectBankForDeletion(String bank) {
        selectVisibleText(selectBank, bank.toUpperCase(), "Bank Name dropdown");
        return this;
    }

    /**
     * clickSubmitPg1
     *
     * @return Current instance
     */
    public DeleteBank_Page1 clickSubmitPg1() {
        clickOnElement(btnSubmitPg1, "Submit Button Page 1");
        return this;
    }

    /**
     * clickSubmitPg2
     *
     * @return
     */
    public DeleteBank_Page1 clickSubmitPg2() {
        Utils.putThreadSleep(2000);
        clickOnElement(btnSubmitPg2, "Submit Button Page 2");
        return this;
    }


}
