/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Automation Team
 *  Date: 26-May-2017
 *  Purpose: Page Object
 */
package framework.pageObjects.bankMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyServiceProviderBankAccount_Pg1 extends PageInit {

    @FindBy(id = "optBankAccModify_initModifyOptBankAcc_bankId")
    private WebElement BankNameDdown;
    //Page Object
    @FindBy(id = "optBankAccModify_initModifyOptBankAcc_submit")
    private WebElement submitButton;
    @FindBy(id = "optBankAccModify_getAllAssociatedOptBankAccounts_submit")
    private WebElement modifyButton;
    @FindBy(id = "optBankAccModify_initModConfirmOptBankAccount_submit")
    private WebElement confirmButton;

    /**
     * Constructor
     *
     * @param t1 Extent Test
     */
    public ModifyServiceProviderBankAccount_Pg1(ExtentTest t1) {
        super(t1);
    }

    /**
     * navModifyServiceProviderBankAc
     *
     * @throws NoSuchElementException NoSuchElementException
     */
    public void navModifyServiceProviderBankAc() throws Exception {
        navigateTo("BANK_ALL", "BANK_OPT_BANK_ACC_MODDM", "Modify Service Provider");
    }

    //Enter Bank Name
    public void selectBank(String bankName) throws Exception {
        selectVisibleText(BankNameDdown, bankName, "BankNameDdown");
    }


    /**
     * clickSubmit
     */
    public void clickSubmit() {
        clickOnElement(submitButton, "submitButton");
    }

    /**
     * clickOnModify
     */
    public void clickOnModify() {
        clickOnElement(modifyButton, "modifyButton");
    }


    /**
     * clickConfirmComplete
     */
    public void clickOnConfirm() {
        clickOnElement(confirmButton, "confirmButton");
    }
}
