/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: Automation Team
 *  Date: 26-May-2017
 *  Purpose: ModifyDeleteBank_Page Object
 */
package framework.pageObjects.bankMaster;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;


/**
 * Created by Dalia on 26-05-2017.
 */
public class AddBank_Page1 extends PageInit {

    @FindBy(name = "bankId")
    public WebElement BankId;
    @FindBy(name = "action:MfsBankMapping_AddBankMapping")
    WebElement submit;
    //Page Objects
    @FindBy(name = "providerId")
    private WebElement ProviderName;
    @FindBy(name = "bankName")
    private WebElement BankName;
    @FindBy(name = "poolAccountNo")
    private WebElement PoolAccountNumber;
    @FindBy(name = "bankType")
    private WebElement BankType;
    @FindBy(id = "defaultRoutingBank")
    private WebElement selDefaultRoutingBank;
    @FindBy(name = "poolAccType")
    private WebElement PoolAccountType;
    @FindBy(name = "cbsType")
    private WebElement CBSType;
    @FindBy(id = "submitButton")
    private WebElement AddBankSubmit;
    @FindBy(id = "optBankAccAdd_initConfirmOptBankAccount_submit")
    private WebElement AddMoreBtn;
    @FindBy(name = "bulkUploadFile")
    private WebElement uploadBranchCsvBtn;
    @FindBy(className = "actionMessage")
    private WebElement ActionMessage;
    @FindBy(className = "errorMessage")
    private WebElement ErrorMessage;
    @FindBy(name = "defaultBankType")
    private WebElement defaultBank;

    public AddBank_Page1(ExtentTest t1) {
        super(t1);
    }

    public static AddBank_Page1 init(ExtentTest t1) {
        return new AddBank_Page1(t1);
    }

    /**
     * Get error Message
     *
     * @return
     */
    public String getErrorMessage() {
        try {
            return ErrorMessage.getText();
        } catch (NoSuchElementException e) {
            return null;
        }

    }

    public void clickOnSubmit() {
        clickOnElement(AddBankSubmit, "AddBankSubmit");
    }

    public void uploadBranchCSV(String filepath) {
        setText(uploadBranchCsvBtn, filepath, "uploadBranchCsv");
    }

    /**
     * Navigate to Add Bank
     *
     * @throws Exception
     */
    public AddBank_Page1 navAddBank() throws Exception {
        navigateTo("BANK_ALL", "BANK_BANK_ADD", "Add Bank");
        return this;
    }

    //Select Provider
    public AddBank_Page1 selectProviderName(String providerName) throws Exception {
        selectVisibleText(ProviderName, providerName, "ProviderName");
        return this;
    }

    //Enter Bank Name
    public void setBankName(String bankName) throws Exception {
        setText(BankName, bankName, "BankName");
    }

    public AddBank_Page1 setBankId(String bankId) throws Exception {
        setText(BankId, bankId, "Bank Id");
        return this;
    }

    public AddBank_Page1 selectBankType(String bankType) throws Exception {
        selectValue(BankType, bankType, "BankType");
        Thread.sleep(4000);
        return this;
    }

    public AddBank_Page1 selectDefaultRoutingBank(String trustBankId) throws Exception {
        Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
        selectValue(selDefaultRoutingBank, trustBankId, "Default Routing Bank");
        return this;
    }

    public WebElement getSelectDefaultRoutingBank() {
        return selDefaultRoutingBank;
    }

    public List<String> getListOfBanksInDefaultRoutingDropdown() {
        return fl.getOptionText(selDefaultRoutingBank);
    }

    //Enter Pool Account Number
    public AddBank_Page1 setPoolAccountNumber(String accNum) throws Exception {
        setText(PoolAccountNumber, accNum, "Pool account Number");
        return this;
    }

    //Select Pool Account Type
    public AddBank_Page1 selectPoolAccountType(String accType) throws Exception {
        selectVisibleText(PoolAccountType, accType, "Pool Account Type");
        return this;
    }

    public void selectCBSType(String cbsType) throws Exception {
        selectValue(CBSType, cbsType, "CBS Type");
    }

    //Select CBS Type
    public AddBank_Page1 selectCBSTypeV5(String cbsType) throws NoSuchElementException {
        WebElement element = DriverFactory.getDriver().findElements(By.name("cbsType")).get(1);
        element.click();
        new Select(element).selectByValue(cbsType);
        pageInfo.info("Select CBS Type: " + cbsType);
        return this;
    }

    public AddBank_Page1 navMFSProviderBankTypeMaster() throws Exception {
        navigateTo("MFSBTM_ALL", "MFSBTM_MFSBTM01DM", "Bank Type Master > Add Bank Pages");
        return this;
    }

    //Select Default Bank
    public AddBank_Page1 selectDefaultBankName(String bankName) throws Exception {
        selectVisibleText(defaultBank, bankName, "Default Bank");
        return this;
    }

    //Click Submit button
    public AddBank_Page1 clickSubmit() {
        clickOnElement(submit, "Submit");
        return this;
    }

    //Verify PoolAccountTypeField
    public boolean isdisppoolaccounttype() throws Exception {
        return PoolAccountType.isDisplayed();
    }

    //Verify CBSTypeField
    public boolean isdispcbstype() throws Exception {
        boolean res = CBSType.isDisplayed();
        return res;
    }

    public void clickAddBankSubmit() {
        clickOnElement(AddBankSubmit, "Add Bank Submit");
    }

}
