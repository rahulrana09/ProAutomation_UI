package framework.pageObjects.preferences;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;

/**
 * Created by navin.pramanik on 9/9/2017.
 */
public class SystemPreferences_Page1 extends PageInit {


    @FindBy(name = "submit")
    WebElement submitButton;

    @FindBy(xpath = "//input[@name='rest']")
    WebElement resetButton;

    @FindBy(id = "preferences_modify_submit")
    WebElement finalSubmitButton;

    @FindBy(xpath = "//td[text()='CURRENCY_FACTOR']/..//td[text()='Y']")
    WebElement currencyFactor;

    public SystemPreferences_Page1(ExtentTest t1) {
        super(t1);
    }

    public void navigateToSystemPreference() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_PREF001", "System Preferences");
    }

    public void selectCheckBox(String code) {
        WebElement checkBox = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@value,'" + code + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
        if (checkBox.isEnabled()) {
            if (checkBox.isSelected()) {
                pageInfo.info("Check box having code:" + code + " is selected");
            } else {
                clickOnElement(checkBox, "checkBox: " + code);
            }
        } else {
            pageInfo.info("Check box is not enabled..");
        }
    }

    public Boolean checkSystemPref(String code) throws IOException {
        try {
            Assertion.attachScreenShotAndLogs(pageInfo);
            WebElement checkBox = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@value,'" + code + "')]/ancestor::tr[1]/td[1]/input[@type='checkbox']"));
            Actions mouseOver = new Actions(driver);
            mouseOver.moveToElement(checkBox);
            if (checkBox.isEnabled()) {
                pageInfo.pass("System Preference is found  and its Editable");
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pageInfo);
            return false;
        }

    }

    public void enterValueToTextBox(String code, String valueToSet) {
        WebElement textBox = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@value,'" + code + "')]/ancestor::tr[1]/td[7]/input[@type='text']"));
        if (textBox.isEnabled()) {
            setText(textBox, valueToSet, "Set Value for code: " + code);
        } else {
            pageInfo.info("Text Box is not enabled");
        }
    }

    public String fetchTextBoxValue(String code) {
        WebElement textBox = DriverFactory.getDriver().findElement(By.xpath("//*[contains(@value,'" + code + "')]/ancestor::tr[1]/td[7]/input[@type='text']"));
        String textValue = textBox.getAttribute("value");
        return textValue;
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "submitButton");
    }

    public void clickReset() {
        clickOnElement(resetButton, "resetButton");
    }

    public void clickFinalSubmit() {
        clickOnElement(finalSubmitButton, "finalSubmitButton");
    }

    public void checkCurrencyFactor() {

        if (currencyFactor.getText().equalsIgnoreCase("Y")) {

            pageInfo.info("CURRENCY_FACTOR is present in the system prefernces");

        } else {
            pageInfo.info("CURRENCY_FACTOR is not present in the system prefernces");
        }
    }

}
