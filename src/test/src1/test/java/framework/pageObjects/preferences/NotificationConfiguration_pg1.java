package framework.pageObjects.preferences;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by navin.pramanik on 9/9/2017.
 */
public class NotificationConfiguration_pg1 extends PageInit {

    @FindBy(id = "serviceForm_bearerSMS")
    public WebElement SMSRadio;
    @FindBy(id = "serviceForm_bearerEMAIL")
    public WebElement emailRadio;
    @FindBy(id = "serviceCode")
    public WebElement serviceCode;
    @FindBy(id = "messageCode")
    public WebElement messageCode;
    @FindBy(id = "languageCode")
    public WebElement languageDdown;
    @FindBy(id = "transactionDataCode")
    public WebElement transactionDataCode;
    @FindBy(id = "addTxnButton")
    public WebElement addButton;
    @FindBy(id = "messageText")
    public WebElement messageTextArea;
    @FindBy(name = "button.submit")
    public WebElement submitButton;
    @FindBy(id = "emailsubject")
    public WebElement txtEmailSubject;

    public NotificationConfiguration_pg1(ExtentTest t1) {
        super(t1);
    }

    public NotificationConfiguration_pg1 clickSmsConfigRadio() {
        clickOnElement(SMSRadio, "SMS radio");
        return this;
    }

    public List<String> getTransactionDataCode() {
        return fl.getOptionValues(transactionDataCode);
    }

    public String getMessageTextArea() throws Exception {
        Thread.sleep(Constants.TWO_SECONDS);
        return messageTextArea.getAttribute("value");
    }

    public String getEmailSubject() throws Exception {
        Thread.sleep(Constants.TWO_SECONDS);
        return txtEmailSubject.getAttribute("value");
    }

    public void setEmailSubject(String text) {
        setText(txtEmailSubject, text, "Email Subject");
    }

    public NotificationConfiguration_pg1 clickEmailRadioBtn() {
        clickOnElement(emailRadio, "emailRadio");
        return this;
    }

    public void clicksubmitBtn_email() {
        clickOnElement(submitButton, "submitBtn_email");
    }

    public void selectServiceForEmailByValue() {
        selectValue(serviceCode, "Transaction correction initiaton", "serviceDropDown");
    }

    public void selectServiceForEmailByValue(String service) {
        selectValue(serviceCode, service, "serviceDropDown");
    }

    public void clickAddBtn_email() {
        clickOnElement(addButton, "addBttn_Email");
    }

    public void selectParametersForEmail() {
        Select serSelect = new Select(transactionDataCode);
        serSelect.selectByIndex(1);
        pageInfo.info("Selected Parameter from Dropdown");
    }

    public NotificationConfiguration_pg1 navigateToSMSConfig() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_SMS_CRDM", "Preferences > SMS Configuration");
        return this;
    }

    public NotificationConfiguration_pg1 navigateToEmailConfig() throws Exception {
        navigateTo("MPREF_ALL", "MPREF_SMS_CRDM", "Preferences > SMS Configuration");
        Thread.sleep(Constants.MAX_WAIT_TIME);
        clickEmailRadioBtn();
        return this;
    }

    public void selectServiceByValue(String code) {
        selectValue(serviceCode, code, "Service Code");
    }

    public void selectMessageByValue(String message) {
        selectValue(messageCode, message, "messageCode");
    }

    public void selectLanguage() {
        selectValue(languageDdown, Constants.SMSC, "Language");
    }

    public void selectTransactionDataByText() {
        Select serSelect = new Select(transactionDataCode);
        serSelect.selectByIndex(1);
        pageInfo.info("Transaction Data Selected");
    }

    public void selectTransactionCodeByValue(String value) {
        selectValue(transactionDataCode, value, "Transaction Data Dynamic field");
    }

    public void setMessageText(String message) {
        setText(messageTextArea, message, "Message text area");
    }

    public void clearMessageTextArea() {
        messageTextArea.clear();
        pageInfo.info("Cleared the SMS message text area.");
    }

    public String fetchMessage() {
        String textValue = messageTextArea.getAttribute("value");
        return textValue;
    }

    public String fetchMessage_email() {
        String textValue = messageTextArea.getAttribute("value");
        return textValue;
    }

    public void setMessageText_email(String message) {
        setText(messageTextArea, message, "Message Text area");
    }

    public String EnterStaticMessage(String[] text) {
        messageTextArea.clear();
        messageTextArea.sendKeys(text);
        String textValue = messageTextArea.getAttribute("value");
        return textValue;
    }

    public String EnterStaticMessage_email(String[] text) {
        messageTextArea.clear();
        messageTextArea.sendKeys(text);
        String textValue = messageTextArea.getAttribute("value");
        return textValue;
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    public void clickAddButton() {
        clickOnElement(addButton, "Add Button");
    }

}
