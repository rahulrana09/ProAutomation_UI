package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class CustomerCarePortal_page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "user")
    private WebElement userType;
    @FindBy(id = "filter")
    private WebElement filterUsing;
    @FindBy(name = "filter_data")
    private WebElement filter_data;
    @FindBy(id = "global_search_btn")
    private WebElement submit;
    @FindBy(xpath = "//*[@id=\"global_search\"]/div/div/div/ul/li[2]/a")
    private WebElement transactionsTab;
    @FindBy(id = "search_role")
    private WebElement transactionSearchInputBox;
    @FindBy(id = "search_transaction_id")
    private WebElement searchTransactionButton;
    @FindBy(id = "search_by_transaction_id_initiate_btn")
    private WebElement initiateTransactionReversal;
    @FindBy(id = "Service_charge")
    private WebElement reverseServiceCharge;
    @FindBy(id = "Commission")
    private WebElement reverseCommission;
    @FindBy(id = "transaction_correction_selection_btn")
    private WebElement initiateTxnReversalBtn;
    @FindBy(id = "initiateTxnCorr")
    private WebElement confirmTxnReversalBtn;

    public static CustomerCarePortal_page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        CustomerCarePortal_page1 page = PageFactory.initElements(driver, CustomerCarePortal_page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public CustomerCarePortal_page1 selectUserType(String user) {
        Select sel = new Select(userType);
        sel.selectByVisibleText(user);
        pageInfo.info("Selecting user as - " + user);
        return this;
    }

    public CustomerCarePortal_page1 selectFilterOption(String filter) {
        Select sel = new Select(filterUsing);
        sel.selectByVisibleText(filter);
        pageInfo.info("Selecting user using - " + filter);
        return this;
    }

    public void clickOnSubmit() {

        submit.click();
        pageInfo.info("clicked on submit");
    }


    //Methods --> Actions
    public CustomerCarePortal_page1 navigateToCCEPortal() {
        try {
            fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_CCE_ENQUIRY");
            pageInfo.info("Navigate to CCE Portal");
            Thread.sleep(60000);

        } catch (Exception e) {
            pageInfo.error("CCE Portal not found.");
        }
        return this;
    }


    public CustomerCarePortal_page1 searchSubscriberTransactionBasedOnTxnID(String msisdn, String txnId) throws Exception {

        selectUserType("SUBSCRIBER");
        selectFilterOption("Mobile Number");
        filter_data.sendKeys(msisdn);
        clickOnSubmit();
        transactionsTab.click();
        performTransactionCorrectionBasedOnTxnId(txnId);
        return this;


    }

    public CustomerCarePortal_page1 searchChannelUserTransactionBasedOnTxnID(String msisdn, String txnId) throws Exception {

        selectUserType("CHANNEL");
        selectFilterOption("Mobile Number");
        filter_data.sendKeys(msisdn);
        clickOnSubmit();
        transactionsTab.click();
        performTransactionCorrectionBasedOnTxnId(txnId);
        return this;

    }

    public void performTransactionCorrectionBasedOnTxnId(String txnId) throws Exception {
        transactionSearchInputBox.sendKeys(txnId);
        pageInfo.info("txn id to be searched is: " + txnId);

        searchTransactionButton.click();
        pageInfo.info("clicked on search transaction button");

        initiateTransactionReversal.click();
        pageInfo.info("clicked on initiate transaction reversal");

        reverseServiceCharge.click();
        pageInfo.info("clicked on reverseServiceCharge");

        reverseCommission.click();
        pageInfo.info("clicked on reverseCommission");

        initiateTxnReversalBtn.click();
        pageInfo.info("clicked on initiateTxnReversalBtn");

        confirmTxnReversalBtn.click();
        pageInfo.info("clicked on confirmTxnReversalBtn");

        //handle the alert box that appears in the transaction

        Alert alert = driver.switchTo().alert();
        alert.accept();

    }
}
