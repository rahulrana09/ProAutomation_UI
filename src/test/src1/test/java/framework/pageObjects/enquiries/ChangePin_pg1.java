package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by rahul.rana on 7/4/2017.
 * Modified by automation.team
 */
public class ChangePin_pg1 extends PageInit{

    public ChangePin_pg1(ExtentTest t1) {
        super(t1);
    }




    @FindBy(id = "userType1")
    private WebElement selUserType;

    @FindBy(id = "msisdn")
    WebElement msisdnWebElement;

    @FindBy(id = "resetPin")
    WebElement pinType;

    @FindBy(id = "bearer")
    WebElement bearer;

    @FindBy(id = "sendReset_displayUserDetails_button_sendReset")
    WebElement resetButton;

    @FindBy(name = "button.submit")
    WebElement submit;


    /**
     * Navigate to Change Pin Page
     *
     * @throws Exception
     */
    public void navResetPin() throws Exception {
        fl.leftNavigation("ENQUIRY_ALL", "ENQUIRY_SR_PIN01");
        pageInfo.info("Navigate to Change Pin Page");
    }


    public void EnterUserNumber(String num) {
        msisdnWebElement.clear();
        msisdnWebElement.sendKeys(num);
        pageInfo.info("Enter MSISDN: " + num);
    }

    public void selectusertype(String text) {
        Select option = new Select(selUserType);
        option.selectByVisibleText(text);
        pageInfo.info("Select user Type:" + text);
    }

    public void selectUserTypeByValue(String text) {
        Select option = new Select(selUserType);
        option.selectByValue(text);
        pageInfo.info("Select user Type:" + text);
    }

    public void selectPinByValue(String pType) {
        selectValue(pinType,pType,"PIN Type");
    }

    public void selectPINtypeByindex() {
        Select option = new Select(pinType);
        option.selectByIndex(1);
        pageInfo.info("Select PIN type at index 1!");
    }

    public void selectBearerType(String text) {
        Select option = new Select(bearer);
        option.selectByVisibleText(text);
    }

    public void clickonSubmit() {
        clickOnElement(submit,"Submit Button");
    }

    public void clickResetButton() {
        clickOnElement(resetButton,"Reset Button");
    }
}
