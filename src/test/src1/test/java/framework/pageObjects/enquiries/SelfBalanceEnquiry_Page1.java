package framework.pageObjects.enquiries;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by surya.dhal on 6/11/2018.
 */
public class SelfBalanceEnquiry_Page1 extends PageInit {

    @FindBy(id = "selectMFSSenderProviderId")
    private WebElement mfsProviderDDown;
    @FindBy(id = "senderPaymentInstrumentId")
    private WebElement paymentInstrumentDDown;
    @FindBy(id = "senderLinkedBanksOrWalletTypesId")
    private WebElement walletTypeDDown;
    @FindBy(name = "button.next")
    private WebElement nextButton;
    @FindBy(id = "channelBalEnq_loadBalance_password")
    private WebElement passwordTBox;
    @FindBy(name = "button.submit")
    private WebElement submitButton;
    @FindBy(id = "channelBalEnq_input_balance")
    private WebElement balanceLabel;

    public SelfBalanceEnquiry_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to SelfBalanceEnquiryPage
     *
     * @throws Exception
     */
    public void navigateToSelfBalanceEnquiryPage() throws Exception {
        navigateTo("ENQUIRY_ALL", "ENQUIRY_ENQ_USC", "Self Enquiry");
    }



    public void selectMFSProvider(String mfsProvider) {
        selectValue(mfsProviderDDown, mfsProvider, "MFS Provider");
    }

    public void selectPaymentInstrument(String paymentInstrument) {
        selectValue(paymentInstrumentDDown, paymentInstrument, "Payment Instrument");
    }

    public void selectWalletType(String walletType) {
        selectValue(walletTypeDDown, walletType, "Wallet Type");
    }

    public void clickOnNextButton() {
        clickOnElement(nextButton, "Next Button");
    }

    public void enterPassword(String password) {
        setText(passwordTBox, password, "Password");
    }

    public void clickOnSubmit() {
        clickOnElement(submitButton, "Submit Button");
    }

    public Double getUserBalance() {
        Double balance = Double.valueOf(balanceLabel.getText().split(" ")[0].trim());
        return balance;
    }


}
