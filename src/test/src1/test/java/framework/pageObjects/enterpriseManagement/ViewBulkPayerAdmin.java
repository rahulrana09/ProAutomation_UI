/*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name:
 *  Date: 06-09-2017
 *  Purpose: Selenium test cases
 */
package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ViewBulkPayerAdmin extends PageInit {


    @FindBy(id = "viewopt_loadSystemPartyView_partyTypeId")
    protected WebElement UserTypeDdown;

    @FindBy(id = "viewopt_loadSystemPartyView_userName")
    protected WebElement NameTbox;

    @FindBy(id = "viewopt_loadSystemPartyView_button_submit")
    protected WebElement SubmitBtn;

    @FindBy(id = "viewopt_input_msisdnUser")
    protected WebElement MSISDNLabel;

    @FindBy(id = "viewopt_input_userName")
    protected WebElement FirstNameLabel;

    @FindBy(id = "viewopt_input_msisdnWeb")
    protected WebElement LoginIdLabel;


    public ViewBulkPayerAdmin(ExtentTest t1) {
        super(t1);
    }

    public static ViewBulkPayerAdmin init(ExtentTest t1) {
        return new ViewBulkPayerAdmin(t1);
    }

    /**
     * navigateToLink - Navigate to link  View Bulk Payer Admin page!
     *
     * @return current Page Instance
     * @throws Exception
     */
    public ViewBulkPayerAdmin navigateToLink() throws Exception {
        navigateTo("BPAMGT_ALL", "BPAMGT_EPTY_VSU", "Buld Payer Admin");
        return this;

    }

    /**
     * selectType - To Select User type
     *
     * @return current Page Instance
     */
    public ViewBulkPayerAdmin selectType(String usrType) {
        selectValue(UserTypeDdown, usrType, "UserTypeDdown");
        return this;
    }

    /**
     * @return current Page instance
     */
    public ViewBulkPayerAdmin clickOnSubmit() {
        clickOnElement(SubmitBtn, "SubmitBtn");
        return this;
    }

    /**
     * Method to Enter Name of the User
     *
     * @param value
     * @return current Page instance
     */
    public ViewBulkPayerAdmin enterName(String value) {
        setText(NameTbox, value, "NameTbox");
        return this;
    }

    /**
     * To return the MSISDN Label Text
     *
     * @return
     */
    public String getMSISDNLabelText() {
        return MSISDNLabel.getText();
    }

    /**
     * To return the Login ID Label Text
     *
     * @return LoginIdLabelText
     */
    public String getLoginIdLabelText() {
        return LoginIdLabel.getText();
    }

    /**
     * To return the Name Label Text
     *
     * @return NameLabelText
     */
    public String getNameLabelText() {
        return FirstNameLabel.getText();
    }


}
