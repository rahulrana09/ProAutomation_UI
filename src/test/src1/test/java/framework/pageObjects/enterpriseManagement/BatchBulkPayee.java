package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class BatchBulkPayee extends PageInit {

    @FindBy(id = "bulkEmployeeReg_confirmEmployeeUpload_bulkUploadFile")
    protected WebElement UploadFileWebElement;

    @FindBy(id = "bulkEmployeeReg_confirmEmployeeUpload_button_submit")
    protected WebElement SubmitBtn;

    @FindBy(id = "bulkEmployeeReg_addEmployees_button_confirm")
    protected WebElement ConfirmBtn;


    public BatchBulkPayee(ExtentTest t1) {
        super(t1);
    }

    public static BatchBulkPayee init(ExtentTest t1) {
        return new BatchBulkPayee(t1);
    }

    /**
     * Navigate to Link
     *
     * @return instance
     * @throws Exception
     */
    public BatchBulkPayee navigateToLink() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_BULK_EMP", "Payroll Bulk Employee");
        return this;
    }

    /**
     * Method to upload File
     *
     * @param fileName File To Upload
     * @return current instance
     */
    public BatchBulkPayee uploadFile(String fileName) {
        setText(UploadFileWebElement, fileName, "UploadFileWebElement");
        return this;
    }

    /**
     * To click on Submit Button
     *
     * @return BatchBulkPayee instance
     */
    public BatchBulkPayee clickOnSubmit() {
        clickOnElement(SubmitBtn, "SubmitBtn");
        return this;
    }

    /**
     * To click on Confirm Button
     */
    public void clickOnConfirm() {
        clickOnElement(ConfirmBtn, "ConfirmBtn");
    }
}
