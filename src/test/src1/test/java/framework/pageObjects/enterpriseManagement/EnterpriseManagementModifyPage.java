package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class EnterpriseManagementModifyPage extends PageInit {

    private static FunctionLibrary fl;
    private static WebDriver driver;
    private static ExtentTest pageInfo;
    private static EnterpriseManagementModifyPage page;

//    public static EnterpriseManagementModifyPage init(ExtentTest t1) {
//        pageInfo = t1;
//        if(page == null){
//             driver = DriverFactory.getDriver();
//            page = PageFactory.initElements(driver, EnterpriseManagementModifyPage.class);
//            fl = new FunctionLibrary(driver);
//        }
//        return page;
//    }

    @FindBy(name = "employeeCode")
    private WebElement UniqueCode;

    @FindBy(id = "modifyEmployee_modifyConfirm_button_modify")
    private WebElement Submit;

    @FindBy(name = "button.confirm")
    private WebElement Confirm;

    public EnterpriseManagementModifyPage(ExtentTest test) {
        super(test);
    }

    public static EnterpriseManagementModifyPage init(ExtentTest test) {
        return new EnterpriseManagementModifyPage(test);
    }

    public EnterpriseManagementModifyPage EnterUniqueCode(String value) {
        setText(UniqueCode, value, "Employee Unique Code: " + value + "");
        return this;
    }

    public EnterpriseManagementModifyPage ClickOnModify() {
        clickOnElement(Submit, "Click on Modify button");
        return this;
    }

    public void ClickOnConfirm() {
        clickOnElement(Confirm, "Click on Confirm button");
    }
}
