package framework.pageObjects.enterpriseManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.FilePath;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import static java.awt.SystemColor.text;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class BulkPaymentPage1 extends PageInit {

    @FindBy(css = ".uploader_details.cursor_default")
    WebElement txtUploadInfo;
    @FindBy(xpath = "//span[.='UPLOAD']")
    WebElement fileuploadbtn;
    @FindBy(xpath = "(//div[@class='alert alert-danger']//span)[2]")
    WebElement errorMsg;
    @FindBy(css = ".alert.alert-success")
    WebElement message;
    @FindBy(xpath = "//select")
    WebElement serviceName;
    @FindBy(xpath = "//span[@class='download_template']/a")
    WebElement bulkSampleFile;
    @FindBy(xpath = "(//input[@name='item.name'])[2]")
    public WebElement name;
    @FindBy(xpath = "(//input[@name='item.name'])[1]")
    public WebElement nation_id;
    @FindBy(id = "remarks")
    private WebElement remarks;
    @FindBy(xpath = "//*[@type='submit']")
    private WebElement Submit;
    @FindBy(xpath = ".//*[@class='truncate upload_title']")
    private WebElement uploadBtn;

    @FindBy(partialLinkText = "Download error")
    private WebElement downloadstatusFile;
    @FindBy(xpath = "//input[@id='file-upload1']")
    private WebElement upload;

    public BulkPaymentPage1(ExtentTest t1) {
        super(t1);
    }

    public static BulkPaymentPage1 init(ExtentTest t1) {
        return new BulkPaymentPage1(t1);
    }

    public String getActionMessage() throws Exception {
        Thread.sleep(2500);
        if (fl.elementIsDisplayed(message)) {
            return message.getText();
        } else {
            return null;
        }
    }

    public void uploadFile(String filepath) {
        upload.sendKeys(filepath);
        pageInfo.info("Uploading file" + filepath);
    }

    public boolean UploadFile(String filePath) throws Exception {
        uploadFile(filePath);
        Utils.captureScreen(pageInfo);
        Thread.sleep(1500);
        if (fl.elementIsDisplayed(txtUploadInfo)) {
            if (txtUploadInfo.getText().toLowerCase().contains("initiated")) {
                pageInfo.pass("Successfully uploaded the file:" + text);
                return true;
            } else {
                pageInfo.fail("Failed to uploaded the file:" + text);
                return false;
            }
        } else {
            pageInfo.fail("Failed to uploaded the file:" + text);
        }
        return false;
    }

    public String verfiy_Errormessage() {
        wait.until(ExpectedConditions.visibilityOf(errorMsg));
        String msg = errorMsg.getText();
        pageInfo.info("get error message");
        return msg;
    }

    public BulkPaymentPage1 fileUpload(String text) throws AWTException, InterruptedException {

        fileuploadbtn.click();
        Thread.sleep(1000);
        StringSelection s = new StringSelection(text);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(s, null);
        Robot r = new Robot();
        Thread.sleep(2000);
        r.keyPress(KeyEvent.VK_CONTROL);
        r.keyPress(KeyEvent.VK_V);
        r.keyRelease(KeyEvent.VK_CONTROL);
        r.keyRelease(KeyEvent.VK_V);
        Thread.sleep(2000);
        r.keyPress(KeyEvent.VK_ENTER);
        r.keyRelease(KeyEvent.VK_ENTER);
        //uploadbtn.sendKeys(text);
        pageInfo.info("Uploading file: " + text);
        return this;
    }

    public void alerthandle() {
        driver.switchTo().alert().accept();
        pageInfo.info("Alert is accepted");
    }

    public BulkPaymentPage1 ClickOnSubmit() {
        clickOnElement(Submit, "Submit");
        Utils.putThreadSleep(5000);
        return this;
    }

    public BulkPaymentPage1 EnterRemarks(String remark) {
        clickOnElement(remarks, "Remarks");
        setText(remarks, remark, "Remark");
        return this;
    }

    public BulkPaymentPage1 serviceSelectText(String text) throws Exception {
        Thread.sleep(2000);
        selectValue(serviceName, text, "serviceName");
        return this;
    }

    public BulkPaymentPage1 serviceSelectTextByVisbleText(String text) throws Exception {
        Thread.sleep(2000);
        selectVisibleText(serviceName, text, "serviceName");
        return this;
    }

    public void checkNameAsMandatory() {
        clickOnElement(name, "Checkbox Name");
    }

    public void checkNationalIdAsMandatory() {
        clickOnElement(nation_id, "Checkbox National Id");
    }

    public String getBatchId() throws Exception {
        return driver.findElement(By.xpath("//button[@class='close']/following-sibling::span")).getText();
    }

    public BulkPaymentPage1 NavigateToLink() throws Exception {
        navigateTo("PAYROLL_ALL", "PAYROLL_SAL_BUP", "Add Bulk Payee");
        return this;
    }

    public boolean downloadTemplateBulkCSV(String prefix) throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix - " + prefix);
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, prefix);
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(bulkSampleFile, "bulkSampleFile");
            Utils.ieSaveDownloadUsingSikuli();
            pageInfo.info("Downloading file at location " + FilePath.dirFileDownloads);
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            return Utils.isFileDownloaded(oldFile, newFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String downloadErrorLogFile() throws Exception {
        try {
            pageInfo.info("Deleting existing file with prefix : bulk-error-");
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-error-"); // is hardcoded can be Generic TODO
            Thread.sleep(5000);
            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            clickOnElement(downloadstatusFile, "DownloadStatusFile");
            Utils.ieSaveDownloadUsingSikuli();
            Thread.sleep(5000);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            if (Utils.isFileDownloaded(oldFile, newFile)) {
                return FilePath.fetchFilesForPathByPrefix(FilePath.dirFileDownloads, "bulk-error-");
            }

        } catch (Exception e) {
            Assertion.markAsFailure("Failed to download the Error Log file");
        }
        return null;
    }

    public String verfiy_allertmessage() {
        fl.waitWebElementVisible(message);
        String msg = message.getText();
        pageInfo.info("get message");
        return msg;
    }

    public String getErrorString() {
        return wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector(".alert.alert-danger")))).getText();
    }
}
