package framework.pageObjects.InverseC2C;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InverseC2C_page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    /***************************************************************************
     * Page Objects
     ***************************************************************************/
    @FindBy(id = "channelProviderList")
    private WebElement payeeProvider;
    @FindBy(id = "channelWalletList")
    private WebElement payeeWallet;
    @FindBy(id = "sel")
    private WebElement domainName;
    @FindBy(id = "ops1")
    private WebElement payerCategory;
    @FindBy(id = "accessIdId")
    private WebElement payerMSISDN;
    @FindBy(xpath = "//select[contains(@id,'otherProviderListSel')]")
    private WebElement payerProvider;
    @FindBy(xpath = "//select[contains(@id,'otherWalletListId')]")
    private WebElement payerWallet;
    @FindBy(id = "inverseC2cTrf_userDetails_button_submit")
    private WebElement submitbtn;

    public static InverseC2C_page1 init(ExtentTest t1) {
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        pageInfo = t1;
        return PageFactory.initElements(driver, InverseC2C_page1.class);
    }

    public InverseC2C_page1 navInversec2c() throws Exception {
        fl.leftNavigation("INVC2C_ALL", "INVC2C_INC2C_TR");
        pageInfo.info("Navigate to Inverse C2C Page");
        return this;
    }

    public void selectDomain(String val) {
        Select option = new Select(domainName);
        option.selectByVisibleText(val);
        pageInfo.info("Select Domain as: " + val);
    }

    public void selectPayerCategory(String val) {
        Select option = new Select(payerCategory);
        option.selectByVisibleText(val);
        pageInfo.info("Select payerCategory as: " + val);
    }

    public void enterPayerMSISDN(String val) {
        payerMSISDN.sendKeys(val);
        pageInfo.info("enter MSISDN as: " + val);
    }

    public void selectPayerProvider(String val) {
        payeeProvider.click();
        Select option = new Select(payerProvider);
        option.selectByVisibleText(val);
        pageInfo.info("Select payerProvider as: " + val);
    }

    public void selectPayerWallet(String val) {
        payeeWallet.click();
        Select option = new Select(payerWallet);
        option.selectByVisibleText(val);
        pageInfo.info("Select payerWallet as: " + val);
    }

    public void clickSubmit() {
        submitbtn.click();
        pageInfo.info("Clicked on submit button");
    }

}
