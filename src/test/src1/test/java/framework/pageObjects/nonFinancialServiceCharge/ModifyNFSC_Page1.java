package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;


public class ModifyNFSC_Page1 extends PageInit {

    @FindBy(id = "selectServSetID")
    WebElement profile;

    @FindBy(id = "chargingMod_loadNonFinancialDetail_delete")
    WebElement DELETE;

    @FindBy(id = "chargingMod_loadNonFinancialDetail_update")
    WebElement update;


    public ModifyNFSC_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page1 init(ExtentTest t1) {
        return new ModifyNFSC_Page1(t1);
    }

    public void NavigateToLink() {
        try {
            fl.leftNavigation("NONFIN_ALL", "NONFIN_CHARGENON_MOD");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("link not found");
        }
    }

    public ModifyNFSC_Page1 navigateToModifyDeleteNFSC() throws Exception {
        navigateTo("NONFIN_ALL", "NONFIN_CHARGENON_MOD", "Non Financial Service Charge");
        return this;
    }

    public boolean selectprofile(String val) {
        try {
            Select option = new Select(profile);
            option.selectByVisibleText(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ModifyNFSC_Page1 selectServiceCharge(String sChargeName) {
        Select option = new Select(profile);
        fl.selectOptionByPartialText(option, sChargeName);
        pageInfo.info("Select Service Charge Profie :" + sChargeName);
        return this;
    }

    public ModifyNFSC_Page1 selectServiceChargebyValue(String sChargeID) {
        selectValue(profile, sChargeID, "Select Service Charge Profie :" + sChargeID);
        return this;
    }

    /**
     * Select top profile form the list
     *
     * @return
     */
    public boolean selectTopProfile() {
        try {
            Select option = new Select(profile);
            option.selectByIndex(1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean delete() {
        try {
            DELETE.click();
            Thread.sleep(2000);
            driver.switchTo().alert().dismiss();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ModifyNFSC_Page1 clickDeleteButton() {
        clickOnElement(DELETE, "DELETE");
        return this;
    }

    public void acceptAlert(boolean accept) {
        if (accept) {
            driver.switchTo().alert().accept();
            pageInfo.info("Accepting the alert..");
        } else {
            driver.switchTo().alert().dismiss();
            pageInfo.info("Rejecting the alert..");
        }
    }

    public ModifyNFSC_Page1 clickUpdateButton() {
        clickOnElement(update, "update");
        return this;
    }

    public boolean update() {
        try {
            clickOnElement(update, "update");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public int getActiveVersionCount(String sName) {
        int counter = 0;
        Select sel = new Select(profile);
        List<WebElement> options = sel.getOptions();
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i).getText().contains(sName)) {
                counter++;
            }
        }
        return counter;
    }

}
