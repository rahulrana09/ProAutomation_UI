package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyNFSC_Page6 extends PageInit {


    @FindBy(id = "chargingMod_modifyConfirm_modifyDetails")
    WebElement confirm;

    public ModifyNFSC_Page6(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page6 init(ExtentTest t1) {
        return new ModifyNFSC_Page6(t1);
    }

    public void clickOnConfirm() {
        clickOnElement(confirm, "Confirm");
    }

}


	

