package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dalia on 29-05-2017.
 */
public class NFSCApprove_pg1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;

    public static NFSCApprove_pg1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NFSCApprove_pg1 page = PageFactory.initElements(driver, NFSCApprove_pg1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public void navApproveNFSC() throws Exception {
        fl.leftNavigation("NONFIN_ALL", "NONFIN_CHARGENON_APP");
        pageInfo.info("Navigate to Approve Service Charge Page");
    }

    public List<String> getListOfServicesPendingForApproval() {
        List<String> services = new LinkedList<>();
        List<WebElement> elements = driver.findElements(By.xpath("//tr[@class = 'tableData']/td[2]"));

        for (WebElement element : elements) {
            services.add(element.getText());
        }
        return services;
    }

    public String getStatusOfTheServicePendingForApproval(String serviceName) {
        String xPath = "//tr[@class = 'tableData']/td[contains(text(),'" + serviceName + "')]/../td[3]";

        return driver.findElement(By.xpath(xPath)).getText();
    }

    public void verifyApproveAndRejectButtonDisplayForTheService(String serviceName, ExtentTest test) throws Exception {
        String xpath = "//tr[@class = 'tableData']/td[contains(text(),'" + serviceName + "')]/following-sibling::td/a";

        List<WebElement> elements = driver.findElements(By.xpath(xpath));

        for (WebElement element : elements) {
            Assertion.verifyEqual(element.isDisplayed(), true,
                    element.getText() + " Button Visible", test);
        }
    }
}
