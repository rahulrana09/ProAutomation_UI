package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ModifyNFSC_Page3 extends PageInit {

    @FindBy(id = "commnAmt")
    WebElement amount;

    @FindBy(id = "chargingMod_modifyCommissionScreen_next")
    WebElement next;

    @FindBy(id = "chargingMod_modifyCommissionScreen_back")
    WebElement back;


    public ModifyNFSC_Page3(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page3 init(ExtentTest t1) {
        return new ModifyNFSC_Page3(t1);
    }

    public ModifyNFSC_Page4 NEXT() {
        clickOnElement(next, "Next");
        return ModifyNFSC_Page4.init(pageInfo);
    }

    public ModifyNFSC_Page3 BACK() {
        clickOnElement(back, "Next");
        return this;
    }

    public ModifyNFSC_Page3 comission(String val) {
        setText(amount, val, "Commission");
        return this;
    }


}
