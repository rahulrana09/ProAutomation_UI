package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class SuspendNFSC_Page1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "chargingSus_listView_button_save")
    WebElement save;
    @FindBy(id = "chargingSus_saveSuspend_button_confirm")
    WebElement confirm;
    @FindBy(xpath = "//*[@id='chargingSus_listView']/table/tbody/tr[2]/td[2]")
    WebElement topProfileName;

    public static SuspendNFSC_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        SuspendNFSC_Page1 page = PageFactory.initElements(driver, SuspendNFSC_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public void NavigateToLink() {
        try {
            fl.leftNavigation("NONFIN_ALL", "NONFIN_CHARGENON_SUS");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Assert.fail("link not found");
        }
    }

    public void navigateToSuspendNFSC() throws Exception {
        fl.leftNavigation("NONFIN_ALL", "NONFIN_CHARGENON_SUS");
        pageInfo.info("Navigating to Suspend NFSC");
    }

    /**
     * This will return the name of service charge present at the top
     *
     * @return
     */
    public String topProfileName_GetText() {

        return topProfileName.getText();
    }


    public boolean profile(String val) {
        try {
            WebElement profile = driver.findElement(By.xpath("//td[contains(text(),'" + val + "')]/following::*[@type='checkbox']"));
            if (profile.isSelected()) {
                profile.click();
            } else {
                System.out.println("profile is already suspened");
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean save() {
        try {
            save.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public boolean confirm() {
        try {
            confirm.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
