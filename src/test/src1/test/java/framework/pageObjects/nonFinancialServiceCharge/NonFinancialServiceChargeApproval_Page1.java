package common.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Dalia on 29-05-2017.
 */
public class NonFinancialServiceChargeApproval_Page1 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;

    public static NonFinancialServiceChargeApproval_Page1 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceChargeApproval_Page1 page = PageFactory.initElements(driver, NonFinancialServiceChargeApproval_Page1.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    /**
     * Navigate to Add Service Charge
     *
     * @throws Exception
     */
    public void navigateToApprovalNonFinancialServiceCharge() throws Exception {
        fl.leftNavigation("NONFIN_CHARGENON_APP", "NONFIN_CHARGENON_APP");
        pageInfo.info("Navigate to Approve Service Charge Page");
    }
}
