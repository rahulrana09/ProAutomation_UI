package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class NonFinancialServiceChargeView_Page2 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "chargingView_loadNonFinancialDetail_submit")
    WebElement submit;

    public static NonFinancialServiceChargeView_Page2 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceChargeView_Page2 page = PageFactory.initElements(driver, NonFinancialServiceChargeView_Page2.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public boolean submit() {
        try {
            submit.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String fetchProfileName() {

        return null;
    }

}
