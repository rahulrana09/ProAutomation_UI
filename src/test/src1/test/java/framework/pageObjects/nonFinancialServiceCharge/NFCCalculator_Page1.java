package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by gurudatta.praharaj on 6/12/2018.
 */
public class NFCCalculator_Page1 extends PageInit {

    @FindBy(id = "selectCategoryId")
    private WebElement selectCategory;
    @FindBy(id = "selectSenderGradeId")
    private WebElement selectSenderGrade;
    @FindBy(id = "selectMFSSenderProviderId")
    private WebElement senderMFSProvider;
    @FindBy(id = "senderPaymentInstrumentId")
    private WebElement paymentInstrument;
    @FindBy(id = "senderLinkedBanksOrWalletTypesId")
    private WebElement instrument;
    @FindBy(id = "paymentTypeServicesId")
    private WebElement service;
    @FindBy(id = "selectMFSReceiverProviderId")
    private WebElement receiverMFSProvider;
    @FindBy(id = "chargingCalc_loadVersionListCalc_submit")
    private WebElement submitButton1;
    @FindBy(id = "chargingCalc_loadNonFinancialDetail_submit")
    private WebElement submitButton2;

    public NFCCalculator_Page1(ExtentTest test) {
        super(test);
    }

    public static NFCCalculator_Page1 init(ExtentTest test) {
        return new NFCCalculator_Page1(test);
    }

    public NFCCalculator_Page1 navToNFCcalculatorPage() throws Exception {
        navigateTo("NONFIN_ALL", "NONFIN_CHARGENON_CALC", "NFC Calculator");
        return this;
    }

    public NFCCalculator_Page1 selectCategory(String category) {
        selectValue(selectCategory, category, "Category Type");
        return this;
    }

    public NFCCalculator_Page1 selectSenderGrade(String senderGrade) {
        selectValue(selectSenderGrade, senderGrade, "Sender Grade");
        return this;
    }

    public NFCCalculator_Page1 setSenderMFSProvider(String providerID) {
        selectValue(senderMFSProvider, providerID, "Sender Provider ID");
        return this;
    }

    public NFCCalculator_Page1 setSenderPaymentInstrument(String instrumentType) {
        selectValue(paymentInstrument, instrumentType, "Sender Instrument Type");
        return this;
    }

    public NFCCalculator_Page1 selectInstrumentType(String instrumentType) {
        selectValue(instrument, instrumentType, "Instrument Type");
        return this;
    }

    public NFCCalculator_Page1 selectService(String serviceType) {
        selectValue(service, serviceType, "Service Type");
        return this;
    }

    public NFCCalculator_Page1 setReceiverMFSProvider(String providerID) {
        selectValue(receiverMFSProvider, providerID, "Receiver ID");
        return this;
    }

    public NFCCalculator_Page1 clickSubmitButton1() {
        clickOnElement(submitButton1, "Submit Button");
        return this;
    }

    public void clickSubmitButton2() {
        clickOnElement(submitButton2, "Submit Button");
    }
}
