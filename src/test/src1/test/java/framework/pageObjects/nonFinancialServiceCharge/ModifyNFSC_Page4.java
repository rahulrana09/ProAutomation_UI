package framework.pageObjects.nonFinancialServiceCharge;


import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class ModifyNFSC_Page4 extends PageInit {


    @FindBy(id = "chargingMod_modifyBonusScreen_amount1")
    WebElement amount;

    @FindBy(id = "chargingMod_modifyBonusScreen_dacId1")
    WebElement accountid;

    @FindBy(id = "chargingMod_modifyBonusScreen_next")
    WebElement next;

    @FindBy(id = "chargingMod_modifyBonusScreen_back")
    WebElement back;


    public ModifyNFSC_Page4(ExtentTest t1) {
        super(t1);
    }

    public static ModifyNFSC_Page4 init(ExtentTest t1) {
        return new ModifyNFSC_Page4(t1);
    }

    public ModifyNFSC_Page5 NEXT() {
        clickOnElement(next, "Next To Page 5");
        return ModifyNFSC_Page5.init(pageInfo);
    }

    public boolean BACK() {
        try {
            back.click();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean accountid(String val) {
        try {
            Select option = new Select(accountid);
            //tion.
            option.selectByVisibleText(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean bonus(String val) {
        try {
            amount.clear();
            amount.sendKeys(val);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
