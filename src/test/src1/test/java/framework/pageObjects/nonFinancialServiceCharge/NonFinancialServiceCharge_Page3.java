package framework.pageObjects.nonFinancialServiceCharge;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Dalia on 29-05-2017.
 */
public class NonFinancialServiceCharge_Page3 {
    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    @FindBy(id = "confirmAddNon_goBonusScreen_next")
    WebElement btnNextToAddTax;

    public static NonFinancialServiceCharge_Page3 init(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        NonFinancialServiceCharge_Page3 page = PageFactory.initElements(driver, NonFinancialServiceCharge_Page3.class);
        fl = new FunctionLibrary(driver);
        return page;
    }

    public NonFinancialServiceCharge_Page4 clickNextToAddTaxPage() throws Exception {
        btnNextToAddTax.click();
        pageInfo.info("Click to next Page To add Tax");
        return NonFinancialServiceCharge_Page4.init(pageInfo);
    }

}
