package framework.pageObjects.autoDebit;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class AutoDebit_SI_pg1 extends PageInit {
    /*
     *     ----------------------------------------- Page Objects ----------------------------------------
     * */
    @FindBy(id = "msisdn")
    WebElement customerMSISDN;
    @FindBy(id = "msisdnId")
    WebElement customerMSISDNID;
    @FindBy(id = "enableAuto_confirmAutoDebit_amount")
    WebElement maxAmtForDebit;
    @FindBy(id = "enableAuto_confirmAutoDebit_button_submit")
    WebElement submitBtn;
    @FindBy(xpath = "//select[@name ='providerName']")
    WebElement provider;
    @FindBy(id = "accountNumberSelected")
    WebElement accountNumber;
    @FindBy(id = "billerTypeSelected")
    WebElement benificiary;
    @FindBy(id = "disableAuto_disableconfirm_button_submit")
    WebElement disableconfirmBtn;
    @FindBy(id = "msisdn")
    WebElement subscriberMsisdn;
    @FindBy(id = "partyProviderList")
    WebElement subProviderIDdropDown;

/*
*    ----------------------    Elements Of Enable Standing Instructions   --------------------------
* */
    @FindBy(id = "paymentMethodDetailList")
    WebElement subWalletDropDown;
    @FindBy(id = "benMsisdn")
    WebElement BeneficiaryMsisdn;
    @FindBy(id = "benProviderList")
    WebElement BeneficiaryProviderIDdropDown;
    @FindBy(id = "benPymtMethodDetailList")
    WebElement BeneficiaryWalletDropDown;
    @FindBy(id = "amount")
    WebElement amountTxtBox;
    @FindBy(id = "enableSI_forwardPage_button_submit")
    WebElement EnableStandingInsSubmitBtn;
    @FindBy(id = "discustmsisdn")
    WebElement subMsisdn_dis;
    @FindBy(id = "disbenmsisdn")
    WebElement beneficiaryMsisdnDropDown;


/*
*    -----------------------    Elements Of Disable Standing Instructions   --------------------------
* */
    @FindBy(id = "disableSI_disableforward_button_submit")
    WebElement disableStandingInsSubmitBtn;
    @FindBy(id = "ViewAutoSI_benificiarydetails_viewbenificiarySI")
    WebElement viewBeneficiarySIRadioButton;
    @FindBy(id = "ViewAutoSI_benificiarydetails_button_submit")
    WebElement viewADSISubmitButton;


 /*
 *     ---------------------  Details to fill Enable Standing Instruction Page   ----------------------------
 * */
    @FindBy(className = "wwFormTableC")
    WebElement webTableStandingInstruction;
    @FindBy(id = "providerListSel")
    private WebElement providerName;

    public AutoDebit_SI_pg1(ExtentTest t1) {
        super(t1);
    }

    public static AutoDebit_SI_pg1 init(ExtentTest t1) throws Exception {
        return new AutoDebit_SI_pg1(t1);
    }

    public void navEnableStandingInstruction() throws Exception {
        navigateTo("AUTODEBIT_ALL", "AUTODEBIT_AUTO_ENASI", "Navigate to enable Standing Instruction");
    }

    public void setsubscriberMsisdn_en_st_ins(User sub) throws Exception {
        subscriberMsisdn.sendKeys(sub.MSISDN);
        pageInfo.info("Entered Subscriber Msisdn" + sub.MSISDN);
    }

    public void setsubProviderID() throws Exception {
        subProviderIDdropDown.click();
           /* Select sel = new Select(subProviderIDdropDown);
            Thread.sleep(1000);
            sel.selectByValue("101");*/
        pageInfo.info("Selected Subscriber Provider ID");
    }

    public void setsubWallet() throws Exception {
        Select sel = new Select(subWalletDropDown);
        sel.selectByIndex(0);
        pageInfo.info("Selected Subscriber Wallet");
    }

    public void setBenProviderID() throws Exception {
        BeneficiaryProviderIDdropDown.click();
           /* Select sel = new Select(BeneficiaryProviderIDdropDown);
            sel.selectByValue("101");*/
        pageInfo.info("Selected Beneficiary Provider ID");
    }

    public void setBenWallet() throws Exception {
        Select sel = new Select(BeneficiaryWalletDropDown);
        sel.selectByIndex(0);
        pageInfo.info("Selected Beneficiary Wallet");
    }

 /*
 *     ---------------------------  Details to fill Disable Standing Instruction Page   ----------------------------
 * */

    public void setAmount(String amt) throws Exception {
        amountTxtBox.sendKeys(amt);
        pageInfo.info("Entered Amount To Be Remitted");
    }

    public void clickOnSubmit() throws Exception {
        EnableStandingInsSubmitBtn.click();
        pageInfo.info("Clicked on submit Button");
    }

    public AutoDebit_SI_pg1 enableStandingInstructionsDetails(User sub, User ben, String amount) throws Exception {
        try {
            navEnableStandingInstruction();
            Thread.sleep(300);
            setsubscriberMsisdn_en_st_ins(sub);
            setsubProviderID();
            setBeneficiaryMsisdn(ben);
            Thread.sleep(3000);
            setBenProviderID();
            setAmount(amount);
            Thread.sleep(2000);
            clickOnSubmit();
            return this;
        } catch (Exception e) {
            pageInfo.error("Failed to enable Standing Instructions...");
            e.printStackTrace();
        }
        return this;
    }

    public void navDisableStandingInstruction() throws Exception {
        navigateTo("AUTODEBIT_ALL", "AUTODEBIT_AUTO_DISSI", "Navigate to disable Standing Instruction");
    }

    public void setSubMsisdn_dis_std_ins(User sub) throws Exception {
        subMsisdn_dis.sendKeys(sub.MSISDN);
        pageInfo.info("Entered Beneficiary Msisdn");
    }
 /*
 *     ---------------------------------------   Enable and Disable Auto Debit --------------------------------------
 * */

    public void setBenMsisdn_dis_std_ins(User ben) throws Exception {
        beneficiaryMsisdnDropDown.click();
        Select sel = new Select(beneficiaryMsisdnDropDown);
        sel.selectByValue(ben.MSISDN);
        pageInfo.info("Entered Beneficiary Msisdn");
    }

    public void clickOnSubmitBtn_dis_std_ins() throws Exception {
        disableStandingInsSubmitBtn.click();
        pageInfo.info("Clicked On Submit Button");
    }

    public AutoDebit_SI_pg1 disableStandingInstructionsDetails(User sub, User ben) throws Exception {
        try {
            navDisableStandingInstruction();
            setSubMsisdn_dis_std_ins(sub);
            Thread.sleep(3000);
            setBenMsisdn_dis_std_ins(ben);
            Thread.sleep(3000);
            clickOnSubmitBtn_dis_std_ins();
        } catch (Exception e) {
            pageInfo.error("Failed to disable Standing Instructions...");
            e.printStackTrace();
        }
        return this;
    }

    public AutoDebit_SI_pg1 navEnableAutoDebit() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_EAD");
        pageInfo.info("Navigate to EnableAutoDebit");
        return this;
    }

    public AutoDebit_SI_pg1 navDisableAutoDebit() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_DIS");
        pageInfo.info("Navigate to EnableAutoDebit");
        return this;
    }

    public void setCustomerMSISDN(String text) {
        customerMSISDN.sendKeys(text);
        pageInfo.info("Set Category's Description - " + text);
    }

    public void setMaxAmtForDebit(String text) {
        maxAmtForDebit.sendKeys(text);
        pageInfo.info("Set Category's Description - " + text);
    }

    public void confirm() {
        submitBtn.click();
        pageInfo.info("Click On Confirm Category!");
    }

    public void selectProvider(String status) {
        provider.click();
        Select stat1 = new Select(provider);
        stat1.selectByVisibleText(status);
        pageInfo.info("Select Status :" + status);
    }

    public void selectAccountNumber(String accno) {
        accountNumber.click();
        Select stat2 = new Select(accountNumber);
        stat2.selectByValue(accno);
        //pageInfo.info("Select Status :" + status);
    }

    // *** View List Of Beneficiaries ***

    public void setCustomerMSISDNID(String text) {
        customerMSISDNID.sendKeys(text);
        pageInfo.info("Set Category's Description - " + text);
    }

    public void selectbenificiary(String bCode) {
        benificiary.click();
        Select stat3 = new Select(benificiary);
        stat3.selectByValue(bCode);
        pageInfo.info("Select Status :benificiary" + bCode);
    }

    public void clickDisableconfirmBtn() {
        disableconfirmBtn.click();
        pageInfo.info("Click On disableconfirmBtn");
    }

    public void navigateToViewListOfBeneficiariesForADSI() throws Exception {
        navigateTo("AUTODEBIT_ALL", "AUTODEBIT_AUTO_DEBVIEW", "Navigate to Auto Debit >  View List of Beneficiaries for Auto Debit/SI page");
    }

    public String getBeneficiaryMsisdn() throws IOException {
        return getTableDataUsingHeaderName(webTableStandingInstruction, "Beneficiary MSISDN");
    }

    public void setBeneficiaryMsisdn(User ben) throws Exception {
        BeneficiaryMsisdn.sendKeys(ben.MSISDN);
        pageInfo.info("Entered Beneficiary Msisdn" + ben.MSISDN);
    }

    public String getDebitAmount() throws IOException {
        return getTableDataUsingHeaderName(webTableStandingInstruction, "Debit Amount");
    }


    public void selectProviderName() throws Exception {
        providerName.click();
        pageInfo.info("Selected Subscriber Provider ID");
    }

    public void selectRadioButtonViewBeneficiarySI() {
        clickOnElement(viewBeneficiarySIRadioButton, "viewBeneficiarySIRadioButton");
    }

    public void clickViewADSISubmitButton() {
        clickOnElement(viewADSISubmitButton, "viewADSISubmitButton");
    }

    public AutoDebit_SI_pg1 viewListOfStandingInstructionBeneficiary(User sub) throws Exception {
        try {
            navigateToViewListOfBeneficiariesForADSI();
            Thread.sleep(2000);
            selectRadioButtonViewBeneficiarySI();
            setCustomerMSISDNID(sub.MSISDN);
            selectProviderName();
            clickViewADSISubmitButton();
            Thread.sleep(3000);
            return this;
        } catch (Exception e) {
            pageInfo.error("Failed to view list of beneficiaries for Standing Instructions...");
            e.printStackTrace();
        }
        return this;
    }

}