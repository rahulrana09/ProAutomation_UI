package framework.pageObjects.autoDebit;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AutoDebitEnableApproval_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    //Page Objects
    @FindBy(id = "approve")
    private WebElement approveBtn;
    @FindBy(id = "reject")
    private WebElement rejectBtn;
    @FindBy(id = "rejectId")
    private WebElement rejectReason;

    public static AutoDebitEnableApproval_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, AutoDebitEnableApproval_pg1.class);
    }

    public void navAutoDebitApproval() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_EADAP");
        Thread.sleep(1000);
        pageInfo.info("Navigate to autoDebit Enable Approval Page");
    }

    public void clickCheckbox(String Msisdn) throws Exception {
        driver.findElement(By.xpath("//label[.='" + Msisdn + "']/../../..//input[@id='singleCheck']")).click();
        pageInfo.info("Click on the check box for autoDebit initiated MSISDN");
    }

    public void clickApprove() throws Exception {
        approveBtn.click();
        pageInfo.info("Click on the Approve button");
    }

    public void clickReject() throws Exception {
        rejectBtn.click();
        pageInfo.info("Click on the Reject button");
    }

    public void selectRejectReason(String reason) throws Exception {
        Select sel = new Select(rejectReason);
        sel.selectByVisibleText(reason);
        pageInfo.info("Select a Reject reason");
    }

}
