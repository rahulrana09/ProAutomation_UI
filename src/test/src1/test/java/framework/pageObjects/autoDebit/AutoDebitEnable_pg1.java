package framework.pageObjects.autoDebit;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AutoDebitEnable_pg1 {

    private static FunctionLibrary fl;
    private static ExtentTest pageInfo;
    private static WebDriver driver;
    //Page Objects
    @FindBy(id = "msisdn")
    private WebElement cusMsisdn;
    @FindBy(id = "providerSelected")
    private WebElement providerId;
    @FindBy(id = "walletIdSelected")
    private WebElement wallet;
    @FindBy(id = "billerTypeSelected")
    private WebElement beneficiaryDetails;
    @FindBy(name = "accountNumber")
    private WebElement accountNo;
    @FindBy(id = "enableAuto_confirmAutoDebit_amount")
    private WebElement amount;
    @FindBy(id = "enableAuto_confirmAutoDebit_button_submit")
    private WebElement submit;

    public static AutoDebitEnable_pg1 init(ExtentTest t1) throws Exception {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
        return PageFactory.initElements(driver, AutoDebitEnable_pg1.class);
    }

    public void navAutoDebitEnable() throws Exception {
        fl.leftNavigation("AUTODEBIT_ALL", "AUTODEBIT_AUTO_EAD");
        Thread.sleep(1000);
        pageInfo.info("Navigate to autoDebit Enable Approval Page");
    }

    public void enterMsisdn(String Msisdn) {
        cusMsisdn.sendKeys(Msisdn);
        pageInfo.info("Entered customer msisdn " + Msisdn);
    }

    public void selectProviderId(String providerName) {
        providerId.click();
        Select sel = new Select(providerId);
        sel.selectByVisibleText(providerName);
        pageInfo.info("Selected Provider " + providerName);
    }

    public void selectWallet(String walletType) {
        Select sel = new Select(wallet);
        sel.selectByVisibleText(walletType);
        pageInfo.info("Selected wallet " + walletType);
    }

    public void selectBeneficiary(String code) {
        Select sel = new Select(beneficiaryDetails);
        sel.selectByValue(code);
        pageInfo.info("Selected Beneficiary " + code);
    }

    public void selectAccountNumber(String acc) {
        accountNo.click();
        Select sel = new Select(accountNo);
        sel.selectByValue(acc);
        pageInfo.info("Selected Account Number " + acc);
    }

    public void enterAmount(String amt) {
        amount.sendKeys(amt);
        pageInfo.info("Amount entered " + amt);
    }

    public void clickSubmit() {
        submit.click();
        pageInfo.info("Submit button clicked");
    }
}
