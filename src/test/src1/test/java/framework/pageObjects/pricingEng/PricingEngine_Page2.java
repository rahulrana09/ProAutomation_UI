package framework.pageObjects.pricingEng;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalConstant.NumberConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;


public class PricingEngine_Page2 extends PageInit {

    @FindBy(id = "charge-rule-0-charge-statement-0-charge-payer")
    private WebElement payerType;
    @FindBy(id = "charge-rule-0-name")
    private WebElement ruleNameTBox;
    @FindBy(id = "charge-rule-0-charge-statement-0-pricing-factor-name-0")
    private WebElement maxOf;
    @FindBy(xpath = "//*[contains(@id,'start-date')]")
    private WebElement startDateTBox;
    @FindBy(xpath = "//*[contains(@id,'end-date')]")
    private WebElement endDateTBox;
    @FindBy(id = "ruleBased-min-charge")
    private WebElement minServiceChargeTBox;
    @FindBy(id = "ruleBased-max-charge")
    private WebElement maxServiceChargeTBox;
    @FindBy(xpath = "//*[contains(@data-bind,'Add New Condition')]")
    private WebElement newConditionDDown;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li/div/div[2]/div[2]/div[1]/div[2]/div/div/div[1]/div/div/ul[2]/li/div[1]/div[2]/div/ul/li[1]/div[2]/div/div/div/div[2]/div/div[1]/div[2]/div/div[1]/div/div[1]/div[1]/div/div[1]")
    private WebElement senderGrade;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li/div/div[2]/div[2]/div[1]/div[2]/div/div/div[1]/div/div/ul[2]/li/div[1]/div[2]/div/ul/li[2]/div[2]/div/div/div/div[2]/div/div[1]/div[2]/div/div[1]/div/div[1]/div[1]/div/div[1]")
    private WebElement receiverGrade;
    @FindBy(xpath = "//select[contains(@data-bind,'chargePayerOptions')]")
    private WebElement chargePayerDDown;
    @FindBy(xpath = "//select[contains(@id,'charge-payer-product')]")
    private WebElement svaTypeDDown;
    @FindBy(xpath = "//select[contains(@data-bind,'chargeReceiverOptions')]")
    private WebElement chargePayeeDDown;
    @FindBy(xpath = "//input[contains(@id,'flat-pricing-percentage')]")
    private WebElement percentagePricing;
    @FindBy(xpath = "//input[contains(@id,'flat-pricing-fixed')]")
    private WebElement fixedPricing;
    @FindBy(xpath = "//span[contains(@data-bind,'successMessage')]")
    private WebElement successMessage;
    @FindBy(xpath = "//a[contains(@data-bind,'apply')]")
    private WebElement finalSubmit;
    @FindBy(xpath = "//a[contains(@data-bind,'openSubmitDialog')]")
    private WebElement openSubmitDialog;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/div/div[1]/div/div[1]/div/div[1]/div[2]/select")
    private WebElement senderRole;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div[2]/select")
    private WebElement hierarchy;
    @FindBy(xpath = "//*[@id='charge-rules']/div[6]/ul/li[1]/div/div[2]/div[2]/div[1]/div[1]/div/div/div/div[3]/div/div[1]/div/div[1]/div[1]/div/div[1]/input")
    private WebElement grade;
    @FindBy(xpath = "//select[contains(@id,'charge-receiver-product')]")
    private WebElement svaTypeDDownPayee;

    @FindBy(xpath = "//div[@class=\"composite-condition-header-with-simple-conditions\"]/ul/li[1]//div[@class='selectize-input items not-full has-options']")
    private WebElement senderGradeForTaxation;

    @FindBy(xpath = "//div[@class=\"composite-condition-header-with-simple-conditions\"]/ul/li[2]//div[@class=\"selectize-input items not-full has-options\"]")
    private WebElement receiverGradeForTaxation;

    @FindBy(id="charge-rule-0-charge-statement-0-flat-pricing-percentage")
    private WebElement percentValue;

    @FindBy(id="charge-rule-0-charge-statement-0-flat-pricing-fixed-amount")
    private WebElement fixedAmount;

    @FindBy(xpath="//div[@class=\"composite-condition-header-with-simple-conditions\"]/ul/li[1]/div[2]/div/div/div/div/div[3]//div/div/div/div/div/div/select")
    private WebElement SenderCategoryForTaxationRule;

    @FindBy(xpath="//div[@class=\"composite-condition-header-with-simple-conditions\"]/ul/li[2]//div[@class=\"col s3\"][2]//select[@class=\"browser-default control\"][1]")
    private WebElement ReceiverCategoryForTaxationRule;


    public PricingEngine_Page2(ExtentTest t1) {
        super(t1);
    }

    public static PricingEngine_Page2 init(ExtentTest t1) {
        return new PricingEngine_Page2(t1);
    }

    public void navToAddService() throws Exception {
        WebElement element = driver.findElement(By.xpath("//a[@data-bind='click: addNewChargeRule']"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
        pageInfo.info("Add Button is clicked");
    }

    public void selectPayerType(String PayerType) throws Exception {

        Select sel = new Select(payerType);
        sel.selectByVisibleText(PayerType);
        pageInfo.info("Selected Type: " + PayerType);
    }

    public void SelectmaxOf()
    {
        selectVisibleText(maxOf,"Sum of","Drop down");
    }

    public void selectSenderRole() {
        selectValue(newConditionDDown, "sender.roleWithGrade", "Sender Role");
    }

    public void selectReceiverRole() {
        selectValue(newConditionDDown, "receiver.roleWithGrade", "Receiver Role");
    }

    public String enterRoleName(String serviceChargeName) {
        setText(ruleNameTBox, serviceChargeName, "Role Name");
        return serviceChargeName;
    }

    public void enterStartDate() {
        String startDate = DateAndTime.init(pageInfo).getDate("YYYY-MM-dd", 0);
        setText(startDateTBox, startDate, "Start Date");
        clickOnElement(startDateTBox, "Start Date");
    }

    public void enterEndDate() {
        String endDate = DateAndTime.init(pageInfo).getDate("YYYY-MM-dd", 21);
        setText(endDateTBox, endDate, "End Date");
        clickOnElement(endDateTBox, "End Date");
    }

    public void enterMinimumCharge(String amount) {
        setText(minServiceChargeTBox, amount, "Minimum Service Charge");
    }

    public void enterMaximumCharge(String amount) {
        setText(maxServiceChargeTBox, amount, "Maximum Service Charge");
    }

    public void selectSenderCategory(String category) {
        WebElement senderCategory = driver.findElements(By.xpath("//*[@id=\"charge-rules\"]/div[6]/ul/li/div/div[2]/div[2]/div[1]/div[2]/div/div/div[1]/div/div/ul[2]/li/div[1]/div[2]/div/ul/li/div[2]/div/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/div[2]/select")).get(0);
        selectValue(senderCategory, category, "Sender category");
    }


    public void selectReceiverCategory(String category) {
        WebElement receiverCategory = driver.findElements(By.xpath("//*[@id=\"charge-rules\"]/div[6]/ul/li/div/div[2]/div[2]/div[1]/div[2]/div/div/div[1]/div/div/ul[2]/li/div[1]/div[2]/div/ul/li/div[2]/div/div/div/div[1]/div[3]/div/div/div[1]/div/div[1]/div[2]/select")).get(1);
        selectValue(receiverCategory, category, "Receiver category");
    }

    public void selectSenderGrade(String gradeCode) {
        clickOnElement(senderGrade, "Sender Grade");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@data-value='" + gradeCode + "']")));
    }


    public void selectReceiverGrade(String gradeCode) {
        clickOnElement(receiverGrade, "Receiver Grade");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@data-value='" + gradeCode + "']")));
    }




    public void selectSenderCategoryForTaxationRule(String category) {
        selectVisibleText(SenderCategoryForTaxationRule,category,"Sender category");
    }

    public void selectReceiverCategoryForTaxationRule(String category) {
        selectVisibleText(ReceiverCategoryForTaxationRule,category,"Receiver category");
    }

    public void selectSenderGradeForTaxation(String gradeCode) {
        clickOnElement(senderGradeForTaxation, "Sender Grade");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@data-value='" + gradeCode + "']")));
    }

    public void selectReceiverGradeForTaxation(String gradeCode) {
        clickOnElement(receiverGradeForTaxation, "Receiver Grade");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@data-value='" + gradeCode + "']")));
    }

    public void selectChargePayer() {
        selectVisibleText(chargePayerDDown, "Sender", "Who Pays");
    }

    public void selectChargePayee() {
        selectVisibleText(chargePayeeDDown, "Service Provider", "Whom to Pay");
    }

    public void enterPricingPercentage(String pricingType, String amount) {
        setText(fixedPricing, amount, "Percentage Charge");
    }
    public void setpercentValue(String amount)
    {
        setText(percentValue, amount,"Percentage Value");
    }

    public void setFixedAmount(String amount)
    {
        setText(fixedAmount, amount,"Percentage Value");
    }

    public void savePolicy() throws InterruptedException {
        Thread.sleep(3000);
        actions.moveToElement(driver.findElement(By.xpath("//div/a[@data-tooltip='Save Draft']"))).build().perform();
        WebElement draft = driver.findElement(By.xpath("//a[@data-bind='click: openSubmitDialog']"));
        actions.moveToElement(draft).perform();
        actions.click(draft).perform();
    }

    public PricingEngine_Page2 clickOnSubmitForApprovalLink() {
        Utils.putThreadSleep(NumberConstants.SLEEP_5000);
        actions.moveToElement(driver.findElement(By.xpath("//div/a[@data-tooltip='Save Draft']"))).build().perform();
        WebElement draft = driver.findElement(By.xpath("//a[@data-bind='click: openSubmitDialog']"));
        actions.moveToElement(draft).click().build().perform();
        pageInfo.info("Clicked on Submit for Approval.");
        //actions.click(draft).perform();

        return this;
    }

    public void clickSubmit() throws InterruptedException {
        Utils.putThreadSleep(NumberConstants.SLEEP_5000);
        WebElement submit = driver.findElement(By.xpath("//a[contains(@data-bind,'submit')]"));
        submit.click();
        Utils.putThreadSleep(NumberConstants.SLEEP_5000);
        //new WebDriverWait(DriverFactory.getDriver(), Constants.EXPLICIT_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(finalSubmit));
        //finalSubmit.click();
    }

    public String getSuccessMessage() throws InterruptedException {
        return successMessage.getText();
    }

    public void selectSVAType(String walletId) {
        selectValue(svaTypeDDown, walletId, "SVA Type");
    }

    public void selectSenderRoleComm(String roleSender) {
        selectValue(senderRole, roleSender, "Sender Role");
    }

    public void selectHierachyComm(String hierarchyRole) {
        selectValue(hierarchy, hierarchyRole, "Hierarchy");
    }

    public void selectGrade(String gradeCode) {
        clickOnElement(grade, "Grade");
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath("//*[@data-value='" + gradeCode + "']")));
    }

    public void selectChargePayerCom() {
        selectVisibleText(chargePayerDDown, "Service Provider", "Who Pays");
    }

    public void selectChargePayeeCom(String value) {
        selectVisibleText(chargePayeeDDown, value, "Whom to Pay");
    }

    public void selectSVACom(String value) {
        selectValue(svaTypeDDownPayee, value, "SVA Type");

    }

    public void selectSVACommission() {
        selectIndex(svaTypeDDownPayee, 2, "SVA Type");
    }

    public void clickClonesubmit() throws InterruptedException {
        WebElement clone = driver.findElement(By.xpath("//a[contains(@data-bind,'clone')][contains(@class,'submit-for-approval')]"));
        clone.click();
        Thread.sleep(3000);
    }

    public void discardPolicy() throws InterruptedException {
        WebElement draft = driver.findElement(By.xpath("//a[contains(@data-bind,'openDiscardDraftDialog')]"));
        actions.moveToElement(draft).perform();
        actions.click(draft).perform();
    }

    public void clickBtnToDiscard() throws InterruptedException {
        WebElement discard = driver.findElement(By.xpath("//a[contains(@data-bind,'discardDraft')]"));
        discard.click();
        Thread.sleep(5000);
    }

    public void revertToLastSaved() throws InterruptedException {
        pageInfo.info("Trying to click Revert to Last Saved");
        WebElement draft = driver.findElement(By.xpath("//a[contains(@data-bind,'openRevertDialog')]"));
        actions.moveToElement(draft).perform();
        actions.click(draft).perform();
        pageInfo.info("Revert to Last Saved clicked");
    }

    public void clickBtnToRevert() throws InterruptedException {
        WebElement revert = driver.findElement(By.xpath("//a[contains(@data-bind,'revert')]"));
        revert.click();
        pageInfo.info("Revert to Last Saved confirmed");
        Thread.sleep(5000);
    }

    public String getPricingFixedValue() {
        pageInfo.info("Trying to get the fixed pricing value");
        String value = fixedPricing.getAttribute("value").toString();
        pageInfo.info("Fixed pricing value:" + value);
        return value;
    }
}
