package framework.pageObjects.pricingEng;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class RejectedPolicies_Page extends PageInit {

    /**
     * W E B   E L E M E N T S
     */

    @FindBy(xpath = "//div[@class='apply-changes']/a")
    private WebElement resolveLink;
    @FindBy(xpath = "//a[@data-bind='click: showRemarks']")
    private WebElement viewFeedbackLink;
    @FindBy(xpath = "//a[@data-bind='click: applyProposalChangesAsDraft']")
    private WebElement confirmLinkOfResolvePolicy;
    @FindBy(xpath = "//modal-action modal-close waves-effect center")
    private WebElement cancelLinkOfResolvePolicy;
    @FindBy(xpath = "//pre[@data-bind='text: remarks']")
    private WebElement approverRemarksList;

    public RejectedPolicies_Page(ExtentTest t1) {
        super(t1);
    }

    public RejectedPolicies_Page clickOnConfirmLinkOfResolvePolicy() {

        return this;
    }

    public RejectedPolicies_Page clickOnResolveLink(String serviceName) {
        WebElement element = driver.findElement(By.xpath("//span[contains(text(),'" + serviceName + "')]//following::div[@class='apply-changes'][1]"));
        clickOnElement(element, "Resolve Link");
        return this;
    }

    public RejectedPolicies_Page clickOnViewFeedbackLink() {
        return this;
    }

    public RejectedPolicies_Page clickOnCancelLinkOfResolvePolicy() {
        return this;
    }


}
