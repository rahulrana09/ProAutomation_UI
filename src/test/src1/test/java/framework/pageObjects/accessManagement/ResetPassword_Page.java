package framework.pageObjects.accessManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResetPassword_Page extends PageInit {


    //Page Objects
    @FindBy(id = "sendResetPass_displayUserDetails_userType")
    private WebElement userTypeWebElement;

    @FindBy(id = "sendResetPass_displayUserDetails_accessId")
    private WebElement loginID;

    @FindBy(id = "sendResetPass_displayUserDetails_button_submit")
    private WebElement submitBtn;

    @FindBy(id = "sendResetPass_displayUserDetails_button_sendReset")
    private WebElement resetBtn;


    public ResetPassword_Page(ExtentTest t1) {
        super(t1);
    }

    public static ResetPassword_Page init(ExtentTest t1) {
        return new ResetPassword_Page(t1);
    }

    /**
     * Navigate methods
     */
    public void navResetPasswordLink() throws Exception {
        navigateTo("ACCESS_MAN_ALL", "ACCESS_MAN_SR_PASS", "Access Management > Reset Password");
    }

    public void setLoginId(String loginId) throws Exception {
        setText(loginID, loginId, "loginID");

    }

    public void setUserType(String userType) throws Exception {
        selectVisibleText(userTypeWebElement, userType, "User Type");
    }

    public void setUserTypeByValue(String userType) throws Exception {
        selectValue(userTypeWebElement, userType, "User Type");
    }

    public void clickSubmit() {
        clickOnElement(submitBtn, "Submit");
    }

    public void clickReset() {
        clickOnElement(resetBtn, "Reset");

    }
}
