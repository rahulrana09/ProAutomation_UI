package framework.pageObjects.operatorManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Nirupama MK on 17/7/2018.
 */
public class OperatorManagementPage extends PageInit {

    @FindBy(id = "operator_checkAddedOperator_operatorName")
    private WebElement operatorNameTextField;
    @FindBy(id = "operator_checkAddedOperator_button_add")
    private WebElement addBtn;

    /*
    * ----------------------------------------- Add Operator ---------------------------------------
    */
    @FindBy(name = "button.reset")
    private WebElement resetBtn;
    @FindBy(id = "gradeCode")
    private WebElement gradeCodeDropDown;
    @FindBy(id = "operator_verifyAddOperator_smscId")
    private WebElement smscDropDown;
    @FindBy(id = "operator_verifyAddOperator_topupId")
    private WebElement topUpDropDown;
    @FindBy(linkText = "Denominations")
    private WebElement rechargeRadioBtn;
    @FindBy(id = "operator_verifyAddOperator_series")
    private WebElement rechargeDenominationsSupportedTextBox;
    @FindBy(id = "operator_verifyAddOperator_minRange")
    private WebElement rechargeMinAmountForFlexiRecharge;
    @FindBy(id = "operator_verifyAddOperator_maxRange")
    private WebElement rechargeMaxAmountForFlexiRecharge;
    @FindBy(xpath = "//*[@value='101'][1]")
    private WebElement ProviderMFS1checkBox;
    @FindBy(xpath = "//*[@value='102'][1]")
    private WebElement ProviderMFS2checkBox;
    @FindBy(id = "operator_verifyAddOperator_contactIdType")
    private WebElement idTypeDropDown;
    @FindBy(id = "operator_verifyAddOperator_idNo")
    private WebElement idNumber;
    @FindBy(id = "operator_verifyAddOperator_button_add")
    private WebElement addBtn2;
    @FindBy(id = "operator_verifyAddOperator_back")
    private WebElement bckBtn2;
    @FindBy(id = "//input[@type='reset']")
    private WebElement resetBtn2;
    @FindBy(id = "operator_confirmAddOperator_back")
    private WebElement bckBtn3;
    @FindBy(id = "operator_confirmAddOperator_button_confirm")
    private WebElement confirmBtn;
    @FindBy(id = "loadOperator_selectOperator_operatorId")
    private WebElement mod_operatorNameDropDown;
    @FindBy(id = "loadOperator_selectOperator_button_submit")
    private WebElement mod_submitBtn;
    @FindBy(id = "loadOperator_selectOperator_button_reset")
    private WebElement mod_resetBtn1;
    @FindBy(id = "loadOperator_verifyOperator_button_update")
    private WebElement mod_UpdateBtn;
    @FindBy(id = "loadOperator_modifyOperator_button_confirm")
    private WebElement mod_ConfirmBtn;
    @FindBy(id = "loadOperator_verifyOperator_button_reset")
    private WebElement mod_resetBtn2;
    @FindBy(id = "loadOperator_verifyOperator_interfaceId")
    private WebElement mod_interfaceId;
    @FindBy(id = "loadOperator1_delOptChecker_delete")
    private WebElement deleteOperatorBtn;
    @FindBy(id = "loadOperator1_deleteOperator_button_confirm")
    private WebElement del_ConfirmBtn;


    //Added By Deepthi --Multi Country Recharge Operator
    @FindBy(id = "operator_verifyAddOperator_loginId")
    private WebElement loginId;
    @FindBy(id="operator_verifyAddOperator_password")
    private WebElement password;
    @FindBy(id="operator_verifyAddOperator_msisdn")
    private WebElement msisdn;
    @FindBy (id="operator_verifyAddOperator_pin")
    private WebElement pin;
    @FindBy (id="operator_verifyAddOperator_externalCode")
    private WebElement externalCode;
    @FindBy(id="url")
    private WebElement urlDropDown;
    @FindBy(id="country")
    private WebElement countryDropDown;
    @FindBy(id="interfaceName")
    private WebElement interfaceDropDown;
    @FindBy(id="operator_verifyAddOperator_interfaceAlias")
    private WebElement interfaceAlias;

    public void setLoginId(String login){
        setText(loginId,login,"Login Text box");
    }

    public void setPassword(String password1){
        setText(password,password1,"Password Text box");
    }

    public void setMsisdn(String msisdn1){
        setText(msisdn,msisdn1,"Msisdn Text box");
    }

    public void setPin(String pin1){
        setText(pin,pin1,"Pin Text box");
    }

    public void setExternalCode(String externalCode1){
        setText(externalCode,externalCode1,"External Code Text box");
    }

    public void setUrlDropDown(String url) {
        selectValue(urlDropDown, url, "url");
    }

    public void setCountryDropDown(String country) {
        selectValue(countryDropDown, country, "country");
    }

    public void setInterfaceDropDown(String interfaceName) {selectValue(interfaceDropDown, interfaceName, "interface"); }

    public void setInterfaceAlias(String interfaceAlias1){
        setText(interfaceAlias,interfaceAlias1,"Interface Alias Text box");
    }

    public OperatorManagementPage(ExtentTest t1) {
        super(t1);
    }

    public static OperatorManagementPage init(ExtentTest t1) {
        return new OperatorManagementPage(t1);
    }

    public void navigateToAddOperatorLink() throws Exception {
        navigateTo("OPPMGM_ALL", "OPPMGM_OPPADD",
                "Operator Management > Add Operator Pages");
    }

    public void setOperatorName(String operatorName) throws Exception {
        operatorNameTextField.sendKeys(operatorName);
        pageInfo.info("operator Name is: " + operatorName);
    }

    public void clickAdd() {
        clickOnElement(addBtn, "add Button");
    }

    public void clickReset() {
        clickOnElement(resetBtn, "Reset Button");
    }

    public void setoperatorNameTextField(String operatorName) {
        setText(operatorNameTextField, operatorName, "operatorName");
    }

    public void setgradeCode(String gradeCode) {
        selectValue(gradeCodeDropDown, gradeCode, "gradeCode");
    }

    public void setsmsc(String smsc) {
        selectValue(smscDropDown, smsc, "smsc");
    }

    public void settopUp(String topUp) {
        selectValue(topUpDropDown, topUp, "topUp");
    }

    public void selectRechargingOptionsRadio(String RechargingOptions) {
        if (RechargingOptions.equalsIgnoreCase(Constants.RECHARGING_OPTIONS_DENOMINATIONS)) {

            WebElement radio = DriverFactory.getDriver().findElement(By.
                    id("operator_verifyAddOperator_rechargeOptionIdDenominations"));
            clickOnElement(radio, "RechargingOptions");

        } else if (RechargingOptions.equalsIgnoreCase(Constants.RECHARGING_OPTIONS_FLEXI_RECHARGE)) {
            WebElement radio = DriverFactory.getDriver().findElement(By.

                    id("operator_verifyAddOperator_rechargeOptionIdFlexi Recharge"));
            clickOnElement(radio, "RechargingOptions");
        } else {
            pageInfo.info("Please Select the Recharge Option");
        }
    }

    public void setrechargeDenominationsSupported(String rechargeDenominationsSupported) {
        setText(rechargeDenominationsSupportedTextBox, rechargeDenominationsSupported, "rechargeDenominationsSupported");
    }

    public void setrechargeMinAmountForFlexiRecharge(String minAmount) {
        setText(rechargeMinAmountForFlexiRecharge, minAmount, "minAmount");
    }

    /*
    * ----------------------------------------- Modify Operator ---------------------------------------
    */

    public void setrechargeMaxAmountForFlexiRecharge(String maxAmount) {
        setText(rechargeMaxAmountForFlexiRecharge, maxAmount, "maxAmount");
    }

    public void selectProvider(String providerID) {
        WebElement checkBox = DriverFactory.getDriver().findElement(By.xpath("//*[@value=" + providerID + "][1]"));
        clickOnElement(checkBox, "providerID");
    }

    public void setIdType(String IdType) {
        selectValue(idTypeDropDown, IdType, "IdType");
    }

    public void setIdNumber(String IdNumber) throws Exception {
        idNumber.sendKeys(IdNumber);
        pageInfo.info("Id Number is: " + IdNumber);
    }

    public void addBtn2() {
        clickOnElement(addBtn2, "addBtn");
    }

    public void bckBtn2() {
        clickOnElement(bckBtn2, "bckBtn2");
    }

    public void resetBtn2() {
        clickOnElement(resetBtn2, "resetBtn2");
    }

    public void bckBtn3() {
        clickOnElement(bckBtn3, "bckBtn3");
    }

    public void confirmBtn() {
        clickOnElement(confirmBtn, "confirmBtn");
    }

    public void navigateToModifyOperatorLink() throws Exception {
        navigateTo("OPPMGM_ALL", "OPPMGM_MODOP",
                "Operator Management > Modify Operator Pages");
    }

    public void selectOperatorName(String operatorName) {
        selectVisibleText(mod_operatorNameDropDown, operatorName, "operatorName");
    }

    public String interfaceId() {
        String value = "" + DataFactory.getRandomNumber(9) + DataFactory.getRandomNumber(3);
        setText(mod_interfaceId, value, "interfaceId");
        return value;
    }

    public void clickOnMod_submit() {
        clickOnElement(mod_submitBtn, "submitBtn");
    }

    public void clickOnMod_reset1() {
        clickOnElement(mod_resetBtn1, "resetBtn");
    }

    public void mod_UpdateBtn() {
        clickOnElement(mod_UpdateBtn, "updateBtn");
    }


    /*
    * ----------------------------------------- Delete Operator ---------------------------------------
    */

    public void mod_ConfirmBtn() {
        clickOnElement(mod_ConfirmBtn, "ConfirmBtn");
    }

    public void clickOnMod_reset2() {
        clickOnElement(mod_resetBtn2, "resetBtn");
    }

    public void navigateToDeleteOperatorLink() throws Exception {
        navigateTo("OPPMGM_ALL", "OPPMGM_DELOP",
                "Operator Management > Delete Operator Pages");
    }

    public void clickOnDeleteOperator() {
        clickOnElement(deleteOperatorBtn, "deleteOperatorBtn");
    }

    public void clickOnDeleteConfirm() {
        clickOnElement(del_ConfirmBtn, "del_ConfirmBtn");
    }

    /*
     * ----------------------------------------- MultiCountry Recharge Operator ---------------------------------------
     */



}