package framework.pageObjects.channelEnquiry;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by navin.pramanik on 9/8/2017.
 */
public class ChannelEnquiry_Page1 extends PageInit {

    @FindBy(id = "o2cEnq_loadDetails_button_submit")
    WebElement submitButton;
    @FindBy(name = "txnId")
    private WebElement txnIdTbox;
    @FindBy(id = "o2cEnq_view_button_submit")
    private WebElement finalSubmit;

    @FindBy(id = "providerListSel")
    private WebElement mfsProviderDropDown;

    @FindBy(id = "paymentMethodId")
    private WebElement pymntInstrumentDropDown;

    @FindBy(id = "wallet")
    private WebElement walletTypeDropDown;

    @FindBy(id = "o2cEnq_loadDetails_msisdn")
    private WebElement MobNumTxtField;

    @FindBy(id = "o2cEnq_view_button_back")
    private WebElement backBtn;

    @FindBy(id = "//input[@id='o2cEnq_view_button_back']")
    private WebElement backBtn1;


    //Using Mobile Number

    @FindBy(id = "providerListSel")
    private WebElement mfsProvider;

    @FindBy(id = "paymentMethodId")
    private WebElement paymentInstrument;

    @FindBy(id = "wallet")
    private WebElement walletType;

    @FindBy(id = "o2cEnq_loadDetails_msisdn")
    private WebElement msisdn;

    //Using date Range

    @FindBy(id = "providerListSel1")
    private WebElement mfsProvider1;

    @FindBy(id = "paymentMethodId1")
    private WebElement paymentInstrument1;

    @FindBy(id = "walletOrBank1")
    private WebElement walletType1;

    @FindBy(name = "dojo.inputFromDate")
    private WebElement fromDate;

    @FindBy(name = "dojo.inputToDate")
    private WebElement toDate;

    @FindBy(name = "status")
    private WebElement txnStatus;


    public ChannelEnquiry_Page1(ExtentTest t1) {
        super(t1);
    }

    public static ChannelEnquiry_Page1 init(ExtentTest t1) {
        return new ChannelEnquiry_Page1(t1);
    }

    /**
     * Navigate to Channnel or Subscriber Enquiry
     *
     * @throws Exception
     */

    public void navChannelEnquiry() throws Exception {
        navigateTo("MCHENQ_ALL", "MCHENQ_O2CENQ01", "Channel Enquiry");
    }

    public void clickOnSubmit() {
        clickOnElement(submitButton, "submitButton");
    }

    public void clickOnBack() {
        clickOnElement(backBtn, "backBtn");
    }

    public void clickOnBack1() {
        clickOnElement(backBtn, "backBtn1");
    }

    public void clickOnFinalSubmit() {
        clickOnElement(finalSubmit, "finalSubmit");
    }

    public void setTxnId(String tid) {
        setText(txnIdTbox, tid, "txnIdTbox");
    }

    public void selectTxnID(String tid) {
        WebElement tidRadio = driver.findElement(By.xpath("//*[@type='radio' and @value='" + tid + "']"));
        clickOnElement(tidRadio, "TID Radio: " + tid);
    }

    public String extractTxnStatus(String tid) {
        String status = DriverFactory.getDriver().findElement(By.xpath("//tr/td[contains(text(),'" + tid + "')]/ancestor::tr[1]/td[7]")).getText();
        return status;
    }

    public void setMFSprovider() throws Exception {
        selectValue(mfsProviderDropDown, DataFactory.getDefaultProvider().ProviderId, "Provider");
    }

    public void setPaymentInstrument() throws Exception {
        selectIndex(pymntInstrumentDropDown, 1, "pymntInstrumentDropDown");
    }

    public void setWalletType(String WalletName) throws Exception {
        selectVisibleText(walletTypeDropDown, WalletName, "walletTypeDropDown");
    }

    public List<String> getWalletOptions() {
        return fl.getOptionText(walletTypeDropDown);
    }

    public void clickOnWalletDropdown() {
        clickOnElement(walletTypeDropDown, "walletTypeDropDown");
    }

    public void setMobNum(String MSISDN) throws Exception {
        setText(MobNumTxtField, MSISDN, "MobNumTxtField");
    }

    public void setMobNum1(String MSISDN) throws Exception {
        setText(MobNumTxtField, MSISDN, "MobNumTxtField");
    }

    /**
     * Select MFS Provider
     *
     * @param provider
     * @throws Exception
     */
    public void selectMFSProvider(String provider) throws Exception {
        selectValue(mfsProvider, provider, "mfsProvider");
    }

    public void selectMFSProvider1(String provider) throws Exception {
        selectValue(mfsProvider1, provider, "mfsProvider1");
    }

    public void clickFromDateImg() {
        //driver.findElement(By.xpath("//input[@name=\"inputFromDate\"]//following-sibling::img")).click();
        //driver.findElement(By.xpath("//tr[@dojoattachpoint=\"calendarWeekTemplate\"]/td[@djdatevalue=\"1525372200000\"][@class=\"currentMonth\"]")).click();
        pageInfo.info("Clicking on the date image");
    }

    /**
     * Select Payment Instrument
     *
     * @param payInstrument
     * @throws Exception
     */
    public void selectPayInstrument(String payInstrument) throws Exception {
        selectValue(paymentInstrument, payInstrument, "paymentInstrument");
    }

    public void selectPayInstrument1(String payInstrument) throws Exception {
        selectValue(paymentInstrument1, payInstrument, "paymentInstrument");
    }

    /**
     * Select Payment Instrument Type
     */
    public void selectPayInstrumentType(String payInstrumentType) {
        selectValue(walletType, payInstrumentType, "walletType");
    }

    public void selectPayInstrumentType1(String payInstrumentType) {
        selectValue(walletType1, payInstrumentType, "walletType");
    }

    public void setMSISDN(String msisdnNumber) {
        setText(msisdn, msisdnNumber, "msisdn");
    }

    public void setFromDate(String fromdate) {
        setText(fromDate, fromdate, "fromDate");
    }

    public void setToDate(String date) {
        setText(toDate, date, "toDate");
    }

    public void selectTxnStatus(String status) {
        selectValue(txnStatus, status, "txnStatus");
    }

    public void clickOnBackButton() throws InterruptedException {
        clickOnElement(backBtn, "backBtn");
    }
}
