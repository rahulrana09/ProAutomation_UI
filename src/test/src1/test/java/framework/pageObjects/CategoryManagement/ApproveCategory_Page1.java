package framework.pageObjects.CategoryManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.dbManagement.MobiquityGUIQueries;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ApproveCategory_Page1 extends PageInit {

    @FindBy(id = "domainCode")
    private WebElement domainCodeDDown;

    //Page Objects
    @FindBy(id = "categoryCode")
    private WebElement parentCategoryDDown;
    @FindBy(id = "newCategoryCode")
    private WebElement categoryCode;
    @FindBy(id = "catapp_label_button_approve")
    private WebElement approveButton;
    @FindBy(id = "catapp_label_button_reject")
    private WebElement rejectButton;

    public ApproveCategory_Page1(ExtentTest t1) {
        super(t1);
    }

    /**
     * Mathod to Navigate to Category Approval Page.
     *
     * @throws Exception
     */
    public void navAddCategoryApprovalPage() throws Exception {
        navigateTo("CAT_APPRL_ALL", "CAT_APPRL_ADD_CAT_APPROVAL", "Approve Category");
    }

    /**
     * Method to select Domain
     *
     * @param val
     * @return
     */
    public void selectDomain(String val) {
        selectVisibleText(domainCodeDDown, val, "domain Code");
    }

    /**
     * Method to select Parent Category
     *
     * @param val
     * @return
     */
    public void selectParentCategory(String val) {
        selectVisibleText(parentCategoryDDown, val, "Parent CategoryName");
    }

    /**
     * Method to select Category name
     *
     * @param val
     * @return
     */
    public void selectCategoryName(String val) {
        selectVisibleText(categoryCode, val, "Category Name");
    }

    /**
     * Method to click on approve button
     *
     * @return
     */
    public void clickOnApproveButton() {
        clickOnElement(approveButton, "Approve Button");
    }

    public void clickOnRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }

    /**
     * Method to click On Reject Button
     *
     * @return
     */
    public void clickRejectButton() {
        clickOnElement(rejectButton, "Reject Button");
    }

    /**
     * Method to select Domain.
     *
     * @param domainCode
     */
    public void selectDomainByValue(String domainCode) {
        selectValue(domainCodeDDown, domainCode.toUpperCase(), "Domain Name");
    }

    /**
     * Method to Select Parent Category.
     *
     * @param domainCode
     */
    public void selectParentCategoryByValue(String domainCode) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String parentCategory = MobiquityGUIQueries.fetchParentCategoryCode(domainCode.toUpperCase());
        if (parentCategory != null) {
            selectValue(parentCategoryDDown, parentCategory, "Parent Category");
        } else {
            selectIndex(parentCategoryDDown, 1, "Parent category");
        }
    }

    /**
     * Method to select Category Name.
     *
     * @param categoryName
     */
    public void selectCategoryNameByValue(String categoryName) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        selectValue(categoryCode, categoryName, "Category Name");
    }
}
