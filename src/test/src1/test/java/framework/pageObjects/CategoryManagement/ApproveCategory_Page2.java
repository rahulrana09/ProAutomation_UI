package framework.pageObjects.CategoryManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.util.List;

public class ApproveCategory_Page2 extends PageInit {


    @FindBy(id = "wallet_button_next")
    private WebElement next;

    //Page Objects
    @FindBy(id = "addcategoryapproval_getGroupRolesForCat_button_submit")
    private WebElement submit;
    @FindBy(name = "action:addcategoryapproval_save")
    private WebElement submitButton;
    @FindBy(className = "wwFormTableC")
    private WebElement webTable;

    public ApproveCategory_Page2(ExtentTest t1) {
        super(t1);
    }


    //Navigating Methods

    public void clickOnWalletApproval() throws Exception {
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorWalletServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorWalletBearerCheck'])[3]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payeeWalletServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payeeWalletBearerCheck'])[3]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payerWalletServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payerWalletBearerCheck'])[3]")).click();
        next.click();
    }

    public void clickOnBankApproval() throws Exception {
        driver.findElement(By.xpath("(//input[@id='wallet_payerBankServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payerBankBearerCheck'])[3]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payeeBankServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_payeeBankBearerCheck'])[3]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorBankServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorBankBearerCheck'])[3]")).click();
        next.click();
    }

    public void clickOnInitiatorApproval() throws Exception {
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorServiceCheck'])[1]")).click();
        driver.findElement(By.xpath("(//input[@id='wallet_initiatorBearerCheck'])[3]")).click();
        next.click();
    }

    public void clickOnAllServicesApproval() throws Exception {
        driver.findElement(By.xpath("(//input[@name = 'check' and @type = 'checkbox'])[1]")).click();
        submit.click();
    }

    /**
     * Method to select Payer Service Lists.
     */
    public void selectAllCheckBox() {
        List<WebElement> checkBoxes = driver.findElements(By.xpath(".//*[@type='checkbox']"));
        for (WebElement checkBox : checkBoxes) {
            wait.until(ExpectedConditions.elementToBeClickable(checkBox)).click();
        }
        pageInfo.info("All Check boxes Selected.");
    }

    public void selectSingleServiceWithBearer() throws IOException {
        int rows = webTable.findElements(By.tagName("tr")).size();
        // exclude first and last row and first column
        for (int i = 2; i < rows; i++) {
            webTable.findElement(By.xpath(".//tr[" + i + "]/td[2]/input[@type='checkbox']")).click();
            webTable.findElement(By.xpath(".//tr[" + i + "]/td[3]/input[@type='checkbox']")).click();
            Utils.captureScreen(pageInfo);
        }
    }

    /**
     * Method to click on Next button
     */
    public void clickOnNextButton() {
        clickOnElement(next, "Next Button");
    }

    /**
     * Method to click on check all button
     */
    public void clickCheckAll() {
        driver.findElement(By.xpath("//input[@type='checkbox' and @value='0']")).click();
        pageInfo.info("Web Role Selected.");
    }

    /**
     * Method to click on Submit button
     */
    public void clickFinalSubmit() {
        clickOnElement(submitButton, "Submit Button");
    }
}
