package framework.pageObjects.CategoryManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CategoryModification_Page1 extends PageInit {

    @FindBy(id = "catapp_button_submit")
    private WebElement submitbtn;
    //Page Objects
    @FindBy(id = "catapp_button_back")
    private WebElement back;

    public CategoryModification_Page1(ExtentTest t1) {
        super(t1);
    }


    //Navigate Methods

    public void navCategoryManagementLink() throws Exception {
        fl.leftNavigation("CATADD_ALL", "CATADD_MODIFY_CAT");
        Thread.sleep(1000);
        pageInfo.info("Navigate to Category Modification Page");
    }

    public void selectParticularCategoryToModify(String cat) throws Exception {
        driver.findElement(By.xpath("//tr[td[contains(text(),'" + cat + "')]]/td/a")).click();
        submitbtn.click();
    }

    /**
     * Method to click On Modify Link.
     *
     * @param childCategory
     */
    public void clickOnModify(String childCategory) {
        driver.findElement(By.xpath("//tr/td[contains(text(),'" + childCategory + "')]/ancestor::tr[1]/td/a")).click();
        pageInfo.info("Click On Modify.");
    }

    /**
     * Method to click on Submit Button.
     */
    public void clickOnSubmitButton() {
        clickOnElement(submitbtn, "Submit Button");
    }

    /**
     * Method to click on Back Button.
     */
    public void clickOnBackButton() {
        clickOnElement(back, "back button");
    }

    /**
     * Method to Check Back Button Functionality.
     */
    public void checkNavigateBackOrNot() {
        if (!fl.elementIsDisplayed(submitbtn))
            pageInfo.info("Successfully Navigated to Back Page.");
        else
            pageInfo.info("Failed To Navigate Back Page.");
    }

}
