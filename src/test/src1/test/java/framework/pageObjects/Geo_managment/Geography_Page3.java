package framework.pageObjects.Geo_managment;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class Geography_Page3 extends PageInit {


    @FindBy(id = "geoMgmt_addModifyDelete_btnAdd")
    private WebElement Add;
    @FindBy(id = "geoMgmt_addModifyDelete_btnBack")
    private WebElement backMain;
    @FindBy(id = "geoAdd_save_back")
    private WebElement addBack;
    @FindBy(id = "geoAdd_confirm_btnAddBack")
    private WebElement addBackConfirm;
    @FindBy(id = "geoModify_save_btnBack")
    private WebElement updateBack;
    @FindBy(id = "geoAdd_save_grphDomainCode")
    private WebElement geocode;
    @FindBy(id = "geoAdd_save_grphDomainName")
    private WebElement geoname;
    @FindBy(id = "geoAdd_save_grphDomainShortName")
    private WebElement shortname;
    @FindBy(id = "geoAdd_save_description")
    private WebElement descr;
    @FindBy(id = "geoAdd_save_btnAdd")
    private WebElement Add2;
    @FindBy(xpath = "//input[@name=\"btnAdd\"][@value=\"Confirm\"]")
    private WebElement confirm;
    @FindBy(id = "geoMgmt_displayDomainList__parentTypeValues_0_")
    private WebElement search;
    @FindBy(id = "geoMgmt_displayDomainList_submitButton")
    private WebElement Submit2;
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_networkDescription\"]")
    private WebElement networkNameConfirm;


    //Confirm Page Elements
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_parentDomainName\"]")
    private WebElement parentGeoNameConfirm;
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_networkCode\"]")
    private WebElement parentShortNameConfirm;
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_grphDomainName\"]")
    private WebElement nameConfirm;
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_grphDomainShortName\"]")
    private WebElement shortNameConfirm;
    @FindBy(xpath = "//label[@id=\"geoAdd_confirm_statusDescription\"]")
    private WebElement statusConfirm;
    @FindBy(id = "geoAdd_confirm_btnCncl")
    private WebElement addCancel;
    @FindBy(id = "geoAdd_confirm_networkCode")
    private WebElement confirmPageNetworkShortName;
    @FindBy(id = "geoMgmt_addModifyDelete_networkDescription")
    private WebElement networkNameInit;


    //Initialisation Page Elements
    @FindBy(id = "geoMgmt_addModifyDelete_parentDomainName")
    private WebElement parentNameInit;
    @FindBy(id = "geoMgmt_addModifyDelete_parentDomainType")
    private WebElement parentDomainTypeInit;
    @FindBy(id = "geoMgmt_addModifyDelete_networkCode")
    private WebElement networkShortName;
    @FindBy(id = "geoAdd_confirm_grphDomainName")
    private WebElement confirmPageDomainName;
    @FindBy(id = "geoAdd_confirm_grphDomainCode")
    private WebElement confirmPageDomainCode;
    @FindBy(id = "geoAdd_confirm_grphDomainShortName")
    private WebElement confirmPageDomainShortName;
    @FindBy(id = "geoAdd_confirm_description")
    private WebElement confirmPageDescription;

    public Geography_Page3(ExtentTest t1) {
        super(t1);
    }






 /*   @FindBy(id = "geoModify_confirm_btnModify")
    private WebElement confirm;
*/

    public Geography_Page3 add() {
        clickOnElement(Add, "Click on Add Button");
        return this;
    }

    public Geography_Page3 adddvalues(String geocodevalue, String geonamevale, String SNvalue, String descvalue) {
        pageInfo.info("Enter Geo code" + geocodevalue);
        geocode.clear();
        geocode.sendKeys(geocodevalue);
        pageInfo.info("Enter Geo name" + geonamevale);
        geoname.clear();
        geoname.sendKeys(geonamevale);
        pageInfo.info(" Enter Short name" + SNvalue);
        shortname.clear();
        shortname.sendKeys(SNvalue);
        pageInfo.info("Enter Description" + descvalue);
        descr.clear();
        descr.sendKeys(descvalue);
        return this;
    }

    public Geography_Page3 addgeo() {
        clickOnElement(Add2, "Add Button");
        return this;
    }

    public Geography_Page3 confirmbutton() {
        if (ConfigInput.isConfirm) {
            clickOnElement(confirm, "Click on Confirm button");
        }
        return this;
    }


    public Geography_Page3 selectzone(String value1) {
        setText(search, value1, "Enter Zone name" + value1);
        clickOnElement(Submit2, "click on Submit button");
        return this;
    }

    public Geography_Page3 submitBtn2() {
        clickOnElement(Submit2, "click on Submit button");
        pageInfo.info("click on Submit button");
        return this;
    }


    public Geography_Page3 selectZone(String zoneName) {
        WebElement radio = driver.findElement(By.xpath("//td[contains(text(),'" + zoneName + "')]/ancestor::tr[1]/td/input[@type='radio']"));
        clickOnElement(radio, "Select Zone");
        return this;
    }

    public Geography_Page3 clickMainBack() {
        clickOnElement(backMain, "Click on the Back button");
        return this;
    }

    public Geography_Page3 clickBackAdd() {
        clickOnElement(addBack, "Click on the Back button");
        return this;
    }

    public Geography_Page3 clickBackAddConfirm() {
        clickOnElement(addBackConfirm, "Click on the Back button");
        return this;
    }

    public Geography_Page3 clickBackUpdate() {
        clickOnElement(updateBack, "Click on the Back button");
        return this;
    }

    public boolean checkGeoPageAdd() {
        if (Add2.isDisplayed()) {
            pageInfo.info("Back to the Add Geography Management page");
            return true;
        }
        return false;
    }

    public boolean networkNameVisible() {
        if (networkNameConfirm.isDisplayed()) {
            pageInfo.info("Network Name is visible");
            return true;
        }
        return false;
    }

    public boolean confirmNetworkShortNameVisible() {
        return fl.elementIsDisplayed(confirmPageNetworkShortName);
    }

    public boolean parentGeoNameVisible() {
        if (parentGeoNameConfirm.isDisplayed()) {
            pageInfo.info("Parent Geo Name is visible");
            return true;
        }
        return false;
    }

    public boolean parentShortNameVisible() {
        if (parentShortNameConfirm.isDisplayed()) {
            pageInfo.info("Parent Short Name is visible");
            return true;
        }
        return false;
    }

    public boolean nameVisible() {
        return fl.elementIsDisplayed(nameConfirm);
    }

    public boolean shortNameVisible() {
        return fl.elementIsDisplayed(shortNameConfirm);
    }


    public boolean statusVisible() {
        return fl.elementIsDisplayed(statusConfirm);
    }

    public boolean backVisible() {
        return fl.elementIsDisplayed(addBackConfirm);
    }

    public boolean confirmVisible() {
        return fl.elementIsDisplayed(confirm);
    }

    public boolean cancelVisible() {
        return fl.elementIsDisplayed(addCancel);
    }

    public boolean fieldCheck(String name, String sName, String Desc) throws InterruptedException {
        if (geoname.getText().equalsIgnoreCase(name)) {
            pageInfo.info("Domain Name Entered " + name);
            if (shortname.getText().equalsIgnoreCase(sName)) {
                pageInfo.info("Short Name Entered " + sName);
                if (descr.getText().equalsIgnoreCase(Desc)) {
                    pageInfo.info("Description Entered " + Desc);
                    return true;
                }
            }
        }
        return false;
    }

    public String getGeoNameGui() {
        return geoname.getAttribute("value");
    }

    public String getShortNameGui() {
        return shortname.getAttribute("value");
    }

    public String getDescriptionGui() {
        return descr.getAttribute("value");
    }

    //Initialisation Page
    public boolean networkNameInitVisible() {
        return fl.elementIsDisplayed(networkNameInit);
    }

    public boolean parentDomNameInitVisible() {
        return fl.elementIsDisplayed(parentNameInit);
    }

    public boolean parentDomTypeInitVisible() {
        return fl.elementIsDisplayed(parentNameInit);
    }

    public String getNetworkShortName() {
        return getElementText(networkShortName, "Network Short Name");
    }

    public String getConfirmPageDomainName() {
        return getElementText(confirmPageDomainName, "confirmPageDomainName");
    }

    public String getConfirmPageDomainCode() {
        return getElementText(confirmPageDomainCode, "confirmPageDomainCode");
    }

    public String getConfirmPageDescription() {
        return getElementText(confirmPageDescription, "confirmPageDescription");
    }

    public String getConfirmPageDomainShortName() {
        return getElementText(confirmPageDomainShortName, "confirmPageDomainShortName");
    }


}
