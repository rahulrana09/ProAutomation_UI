package framework.pageObjects.Geo_managment;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.NumberConstants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.Iterator;
import java.util.Set;


public class Geography_Page1 extends PageInit {

    //First Page Elements
    @FindBy(id = "geoMgmt_addModifyDelete_networkDescription")
    public WebElement networkNameInit;
    @FindBy(id = "geoMgmt_addModifyDelete_parentDomainName")
    public WebElement parentNameInit;
    @FindBy(id = "geoMgmt_addModifyDelete_parentDomainType")
    public WebElement parentDomainTypeInit;
    @FindBy(id = "geoMgmt_addModifyDelete_networkCode")
    public WebElement networkShortNameInit;
    @FindBy(xpath = "//td[@class='tabhead'][2]")
    public WebElement domainNameInit;
    @FindBy(xpath = "//td[@class='tabhead'][3]")
    public WebElement domainShortNameInit;
    @FindBy(xpath = "//td[@class='tabhead'][3]")
    public WebElement statusInit;
    @FindBy(id = "geoMgmt_showParentSearch_grphDomainType")
    private WebElement geotype;
    @FindBy(xpath = "//*[@id='geoMgmt_showParentSearch']/table/tbody//td/input[@type='button']")
    private WebElement Submit;
    @FindBy(xpath = "//input[@name=\"grphDomainName\"]")
    private WebElement GeoDomName;
    @FindBy(xpath = "//input[@name=\"grphDomainShortName\"]")
    private WebElement GeoShortName;
    @FindBy(id = "geoModify_save_description")
    private WebElement GeoDesc;
    @FindBy(xpath = "//span[text()=\" View Geographical Domain \"]")
    private WebElement geopage2;
    @FindBy(xpath = "//img[@src=\"/CoreWeb/mjsp/common/images/search.gif\"]")
    private WebElement iconImg;
    @FindBy(name = "parentDomainCode")
    private WebElement childWindowZoneDdown;
    @FindBy(id = "geoMgmt_loadParentList_btnSearch")
    private WebElement childWindowSubmitBtn;
    @FindBy(id = "geoMgmt_displayDomainList__parentTypeValues_0_")
    private WebElement zoneTbox;

    public Geography_Page1(ExtentTest t1) {
        super(t1);
    }

    public Geography_Page1 navigateToLink() throws Exception {
        navigateTo("GEOMASTER_ALL", "GEOMASTER_GRDOM001", "Geography Management");
        return this;
    }


    public Geography_Page1 selectgeo(String value1) {
        selectVisibleText(geotype, value1, "Geography Type");
        return this;
    }

    public String getSelectedGeo() {
        return new Select(geotype).getFirstSelectedOption().getAttribute("text");
    }


    public Geography_Page3 clicksubmit() {
        clickOnElement(Submit, "Click on Submit Button");
        return new Geography_Page3(pageInfo);
    }

    public boolean checkGeoPage2() {
        return fl.elementIsDisplayed(geopage2);
    }

    public boolean checkGeoMain() {
        return fl.elementIsDisplayed(geotype);
    }

    public boolean checkIconImg() {
        return fl.elementIsDisplayed(iconImg);
    }

    public void clickIconImg() {
        clickOnElement(iconImg, "Icon Image");
    }


    public void selectZoneFromChildWindow(String zoneCode) {


        setZoneText("%");
        String mainWindow = driver.getWindowHandle();
        clickIconImg();


        Set<String> s1 = driver.getWindowHandles();
        Iterator<String> i1 = s1.iterator();

        while (i1.hasNext()) {
            String childWindow = i1.next();

            if (!mainWindow.equalsIgnoreCase(childWindow)) {
                driver.switchTo().window(childWindow);
                driver.manage().window().maximize();
                pageInfo.info("Switching to Child Window..");

                selectValue(childWindowZoneDdown, zoneCode, "Zone Dropdown");
                clickOnElement(childWindowSubmitBtn, "Child Window Submit Button");

            }


            driver.switchTo().window(mainWindow);
            try {
                fl.checkFrame();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //driver.switchTo().activeElement();
        }
    }

    public void setZoneText(String text) {
        setText(zoneTbox, text, "Zone");
    }

    public String getZoneTextFromTextBox() {
        Utils.putThreadSleep(NumberConstants.SLEEP_2000);
        return zoneTbox.getAttribute("value");
    }


}
