/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 27-Nov-2017
*  Purpose: Page2 of C2C
*/

package framework.pageObjects.c2c;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ChannelToChannelTransfer_page2 extends PageInit {


    /***************************************************************************
     * ############################ Page Objects #############################
     ***************************************************************************/


    @FindBy(id = "amount")
    private WebElement amountTBox;

    @FindBy(id = "c2cTrf_confirm_paymentId")
    private WebElement paymentIDTBox;

    @FindBy(id = "c2cTrf_confirm_button_submit")
    private WebElement submitBtn;

    //Page 3 also combined
    @FindBy(id = "c2cTrf_confirm_c2c")
    private WebElement confirmBtn;

    @FindBy(name = "action:c2cTrf_back")
    private WebElement backBtn;


    public ChannelToChannelTransfer_page2(ExtentTest t1) {
        super(t1);
    }

    public static ChannelToChannelTransfer_page2 init(ExtentTest t1) {
        return new ChannelToChannelTransfer_page2(t1);
    }

    /**
     * Method to Set Amount
     *
     * @param amount
     */
    public void amountSetText(String amount) {
        amountTBox.clear();
        amountTBox.sendKeys(amount);
        pageInfo.info("Set amount :" + amount);
    }

    /**
     * Method to set Payment ID
     *
     * @param paymentId
     */
    public void paymentIDSetText(String paymentId) {
        paymentIDTBox.clear();
        paymentIDTBox.sendKeys(paymentId);
        pageInfo.info("Set payment ID :" + paymentId);
    }

    /**
     * Method to click on Submit button
     */
    public void clickSubmitButton() {
        clickOnElement(submitBtn, "submitBtn");
    }

    /**
     * Method to click on Confirm Button
     */
    public void clickConfirmButton() {
        confirmBtn.click();
        pageInfo.info("Click on Confirm Button");
    }

    /**
     * Method to click on back button
     */
    public void clickBackButton() throws Exception {
        backBtn.click();
        Thread.sleep(Constants.WAIT_TIME);
        pageInfo.info("Click on back Button");
    }


}
