/*
*  COPYRIGHT: Comviva Technologies Pvt. Ltd.
*  This software is the sole property of Comviva
*  and is protected by copyright law and international
*  treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of
*  it may result in severe civil and criminal penalties
*  and will be prosecuted to the maximum extent possible
*  under the law. Comviva reserves all rights not
*  expressly granted. You may not reverse engineer, decompile,
*  or disassemble the software, except and only to the
*  extent that such activity is expressly permitted
*  by applicable law notwithstanding this limitation.
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
*  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
*  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
*  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
*  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
*  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
*  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*  Author Name: Automation Team
*  Date: 12-Dec-2017
*  Purpose: O2C Approval1 Page
*/
package framework.pageObjects.ownerToChannel;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Dalia on 02-06-2017.
 */
public class O2CApproval1_Page2 extends PageInit {

    //Page Objects
    @FindBy(id = "o2cApproval2_displayTransactionDetails_button_submit")
    private WebElement submit;
    @FindBy(id = "o2cApproval2_displayTransactionDetails_button_approve")
    private WebElement approve;

    public O2CApproval1_Page2(ExtentTest t1) {
        super(t1);
    }

    /**
     * Navigate to Owner to Channel Transfer Approval-2 Page
     *
     * @throws Exception
     */
    public void navigateToOwner2ChannelApproval2Page() throws Exception {
        fl.leftNavigation("O2CTRF_ALL", "O2CTRF_O2CAPP02");
        pageInfo.info("Navigate to Owner to Channel Transfer Approval-2 Page");
    }

    /**
     * Method to Click Submit button
     */
    public void clickSubmit() {
        submit.click();
        pageInfo.info("Click on Submit");
    }

    /**
     * Method to Click Approve button
     */
    public void clickApprove() {
        approve.click();
        pageInfo.info("Click on Approve");
    }
}
