package framework.pageObjects.kpi;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by navin.pramanik on 9/11/2017.
 */
public class KPI_Pg1 extends PageInit {

    @FindBy(id = "channelProviderList")
    WebElement providerDdown;

    @FindBy(id = "confirm_kpiType1")
    WebElement currentDateRadio;

    @FindBy(id = "confirm_kpiType2")
    WebElement previousDateRadio;

    @FindBy(id = "confirm_kpiType3")
    WebElement dateRangeRadio;

    @FindBy(name = "fromDateStr")
    WebElement fromDate;

    @FindBy(name = "toDateStr")
    WebElement toDate;

    @FindBy(id = "confirm_button_submit")
    WebElement submitButton;

    @FindBy(id = "kpi_result_back")
    WebElement backButton;


    public KPI_Pg1(ExtentTest t1) {
        super(t1);
    }

    public static KPI_Pg1 init(ExtentTest t1) {
        return new KPI_Pg1(t1);
    }

    //Page Operations
    public void navigateToLink() throws Exception {
        navigateTo("MKPI_ALL", "MKPI_ALL", "KPIManagement");
    }

    public void selectProviderByValue(String providerID) {
        selectValue(providerDdown, providerID, "Provider");
    }

    public void selectCurrentDateRadio() {
        clickOnElement(currentDateRadio, "Current Date");
    }

    public void selectPreviousDateRadio() {
        clickOnElement(previousDateRadio, "Previous Date");
    }

    public void selectDateRangeRadio() {
        clickOnElement(dateRangeRadio, "Date range");
    }

    public void setFromDate(String date) throws Exception {
        Utils.setValueUsingJs(fromDate, "");
        Utils.setValueUsingJs(fromDate, date);
        Thread.sleep(Constants.TWO_SECONDS);
        pageInfo.info("Set From Date: " + date);
    }

    public void setToDate(String date) throws Exception {
        Utils.setValueUsingJs(toDate, "");
        Utils.setValueUsingJs(toDate, date);
        Thread.sleep(Constants.TWO_SECONDS);
        pageInfo.info("Set To Date: " + date);
    }

    public void clickSubmit() {
        clickOnElement(submitButton, "Submit");
    }

    public void clickBack() {
        clickOnElement(backButton, "Back");
    }

}
