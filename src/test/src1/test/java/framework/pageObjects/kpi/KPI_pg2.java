package framework.pageObjects.kpi;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;


public class KPI_pg2 extends PageInit {


    @FindBy(className = "wwFormTableC")
    WebElement kpiTable;

    public KPI_pg2(ExtentTest t1) {
        super(t1);
    }

    public static KPI_pg2 init(ExtentTest t1) {
        return new KPI_pg2(t1);
    }

    /**
     * @param kpiId
     * @return
     * @throws IOException
     */
    public String getKPIResult(String kpiId) throws Exception {
        int index = getKpiIndex(kpiId);
        String result = null;
        if (index >= 1) {
            WebElement btnSubmit = kpiTable.findElement(By.xpath(".//tr[" + index + "]/td/input[@type='submit']"));
            clickOnElement(btnSubmit, "Submit for KPI Id: " + kpiId);
            try {
                Thread.sleep(Constants.TWO_SECONDS);
                actions.moveToElement(btnSubmit);
                Utils.scrollUntilElementIsVisible(kpiTable.findElement(By.xpath(".//tr[" + index + "]/td/input[@type='submit']")));
                Utils.captureScreen(pageInfo);
                Thread.sleep(Constants.MAX_WAIT_TIME);
                WebElement vCell = kpiTable.findElement(By.xpath(".//tr[" + index + "]/td[4]"));
                result = vCell.getText();
                pageInfo.info("KPI actual Value: " + result);
            } catch (InterruptedException e) {
                Assertion.raiseExceptionAndContinue(e, pageInfo);
            }

        }
        return result;
    }

    private int getKpiIndex(String kpiId) throws Exception {
        try {
            int rowCount = kpiTable.findElements(By.tagName("tr")).size();
            for (int i = 1; i <= rowCount; i++) {
                Thread.sleep(Constants.MAX_WAIT_TIME);
                if (kpiTable.findElement(By.xpath(".//tr[" + i + "]/td[1]")).getText().equals(kpiId)) {
                    return i;
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, pageInfo);
        }

        return -1;
    }

}
