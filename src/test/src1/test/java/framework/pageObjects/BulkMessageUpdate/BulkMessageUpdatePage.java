package framework.pageObjects.BulkMessageUpdate;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BulkMessageUpdatePage extends PageInit {

    @FindBy(id = "BULKMSG_ALL")
    public WebElement BulkMessageUpdate;

    @FindBy(xpath = "//img[contains(@src,'home.png')]")
    public WebElement download;

   @FindBy(id="bulkMsgUpdate_upload_bulkChoicesms")
   public WebElement smsRadioButton;

    @FindBy(id="bulkMsgUpdate_upload_button_submit")
    public WebElement submitButton;

    @FindBy(id="serviceId")
    public WebElement selectService;

    @FindBy(id="channelId")
    public WebElement channelType;

    @FindBy(id="languageId")
    public WebElement Language;

    @FindBy(xpath="//form[@id=\"bulkMsgUpdate_uploadNewMessage\"]/table/tbody/tr[2]/td/a")
    public WebElement DownloadMesageFile;

    @FindBy(id="bulkMsgUpdate_uploadNewMessage_bulkUploadFile")
    public WebElement chooseFileBtn;


    public BulkMessageUpdatePage(ExtentTest t1) {
        super(t1);
    }

    public BulkMessageUpdatePage navigateToBulkMessageUpdate() throws Exception {
        navigateTo("BULKMSG_ALL", "BULKMSG_ALL", "Bulk Message Update");
        return this;
    }
    public void checkSMS()
    {
        smsRadioButton.click();
    }
    public void DownloadMesageFile()
    {
        DownloadMesageFile.click();
    }
    public void clickSubmit()
    {
        submitButton.click();
    }
    public void selectService(String text)
    {
        selectVisibleText(selectService, text,"ServiceName");
    }

    public void selectChannelType(String text)
    {
        selectVisibleText(channelType, text,"Channel Type");
    }

    public void selectLanguage(String text)
    {
        selectVisibleText(Language,text,"language");
    }

    public BulkMessageUpdatePage uploadCurrentMessageFile(String filename) {
        setText(chooseFileBtn, filename, "Upload File");
        return this;
    }
}
