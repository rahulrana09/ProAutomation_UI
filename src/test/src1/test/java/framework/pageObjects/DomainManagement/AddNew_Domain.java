package framework.pageObjects.DomainManagement;

import com.aventstack.extentreports.ExtentTest;
import framework.pageObjects.PageInit;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddNew_Domain extends PageInit {

    //Page Objects
    @FindBy(id = "confirmAddDomain_viewDetail_domainName")
    private WebElement domainName;
    @FindBy(id = "confirmAddDomain_viewDetail_domainCode")
    private WebElement domainCode;
    @FindBy(name = "noOfCategories")
    private WebElement numOfCategory;
    @FindBy(id = "confirmAddDomain_viewDetail_button_submit")
    private WebElement submit;
    @FindBy(id = "confirmAddDomain_getNonFinancialSerForCat_button_submit")
    private WebElement submit1;

    public AddNew_Domain(ExtentTest test) {
        super(test);
    }

    public static AddNew_Domain init(ExtentTest test) {
        return new AddNew_Domain(test);
    }

    public WebElement getElementDomainName() {
        return domainName;
    }

    public WebElement getElementDomainCode() {
        return domainCode;
    }

    public WebElement getElementNumOfCategory() {
        return numOfCategory;
    }

    public void navDomainManagementLink() throws Exception {
        navigateTo("DOMMANAGE_ALL", "DOMMANAGE_DOM_ADD", "Add Domain Page");
    }

    public void setDomainName(String val) {
        setText(domainName, val, "Domain Name");
    }

    public void setDomainCode(String val) {
        setText(domainCode, val, "Domain Code");
    }

    public void setNumberOfCategory(String val) {
        setText(numOfCategory, val, "Number of Category");
    }

    public void submit() {
        clickOnElement(submit, "Submit Button");
    }

    public void submitf() {
        clickOnElement(submit1, "Submit Button");
        //clickOnElementUsingJs(submit1,"Submit Button");
    }

}
