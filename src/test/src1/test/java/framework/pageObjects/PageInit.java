package framework.pageObjects;

import com.aventstack.extentreports.ExtentTest;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.*;

/**
 * Created by Automation Team on 6/21/2017.
 */
public class PageInit {
    protected static WebDriver driver;
    protected static WebDriverWait wait;
    protected static ExtentTest pageInfo;
    protected static FunctionLibrary fl;
    protected static Actions actions;

    public PageInit(ExtentTest t1) {
        pageInfo = t1;
        driver = DriverFactory.getDriver();
        wait = new WebDriverWait(driver, Constants.EXPLICIT_WAIT_TIME);
        actions = new Actions(driver);
        fl = new FunctionLibrary(driver);
        PageFactory.initElements(driver, this);
    }

    public static Object[] fetchLabelTexts(String xpath, String... attribute) {
        List<WebElement> elements = driver.findElements(By.xpath(xpath));
        List<String> elementFieldText = new ArrayList<>();

        if (attribute.length > 0) {
            String value = attribute[0];

            for (int i = 0; i < elements.size(); i++) {
                elementFieldText.add(elements.get(i).getAttribute(value).trim());
            }
        } else {

            for (int i = 0; i < elements.size(); i++) {
                elementFieldText.add(elements.get(i).getText().trim());
            }
        }
        Object[] actualFieldText = elementFieldText.toArray();

        Arrays.sort(actualFieldText);

        return actualFieldText;
    }

    protected void logInfo(String info) {
        //System.out.println(info);
        pageInfo.info(info); // log for Extent Report
    }

    protected void setText(WebElement elem, String text, String elementName) {

        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem)).clear();
            Thread.sleep(ConfigInput.additionalWaitTime);
            elem.sendKeys(text);
            logInfo("set " + elementName + "s text as '" + text + "'");

        } catch (Exception e) {
            try {
                pageInfo.warning("Exception Occurred: " + e.getMessage());
                pageInfo.info("Retry Set Text");
                wait.until(ExpectedConditions.elementToBeClickable(elem)).clear();
                Thread.sleep(ConfigInput.additionalWaitTime);
                elem.sendKeys(text);
                logInfo("set " + elementName + "'s text as '" + text + "'");
            } catch (Exception e1) {
                pageInfo.fail("Failed to set " + text + " to " + elementName);
                pageInfo.error(e1.toString());
                e1.printStackTrace();
                try {
                    Assertion.raiseExceptionAndContinue(e1, pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected void setTextUsingJs(WebElement elem, String text, String elementName) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem)).clear();
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
            js.executeScript("arguments[0].value='" + text + "';", elem);
            Thread.sleep(ConfigInput.additionalWaitTime);
            logInfo("set " + elementName + "'s text as '" + text + "'");
        } catch (Exception e) {
            pageInfo.fail("Failed to set " + text + " for " + elementName);
            pageInfo.error(e.toString());
            e.printStackTrace();
            try {
                Assertion.raiseExceptionAndContinue(e, pageInfo);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    protected void selectVisibleText(WebElement elem, String text, String elementName) {
        Select sel = null;

        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            sel = new Select(elem);
            fl.waitSelectOptionText(sel, text);
            sel.selectByVisibleText(text);
            Thread.sleep(ConfigInput.additionalWaitTime);
            logInfo("select from " + elementName + ". Selected text '" + text + "'");
        } catch (Exception e) {
            try {
                sel = new Select(elem);
                pageInfo.warning("Exception Occurred: " + e.getMessage());
                pageInfo.info("Retry Selecting By Visible Text");
                fl.waitSelectOptionText(sel, text);
                sel.selectByVisibleText(text);
                Thread.sleep(ConfigInput.additionalWaitTime);
                logInfo("select from " + elementName + ". Selected text '" + text + "'");
            } catch (Exception e1) {
                try {
                    Utils.captureScreen(pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                pageInfo.fail("Failed to Select " + text + " from " + elementName);
                pageInfo.error(e1.toString());
                e1.printStackTrace();
                try {
                    elem.click();
                    Assertion.raiseExceptionAndStop(e1, pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected void selectValue(WebElement elem, String value, String elementName) {
        Select sel = null;
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            sel = new Select(elem);
            fl.waitSelectOptionValue(sel, value);
            sel.selectByValue(value);
            Thread.sleep(ConfigInput.additionalWaitTime);
            logInfo("Select from :" + elementName + " . Selected value : '" + value + "'");
        } catch (Exception e) {
            try {
                sel = new Select(elem);
                pageInfo.warning("Exception Occurred: " + e.getMessage());
                pageInfo.info("Retry Selecting By Value");
                fl.waitSelectOptionValue(sel, value);
                sel.selectByValue(value);
                Thread.sleep(ConfigInput.additionalWaitTime);
                logInfo("select from " + elementName + ". Selected value '" + value + "'");
            } catch (Exception e1) {
                pageInfo.fail("Failed to Select " + value + " from " + elementName);
                pageInfo.error(e1.toString());
                e1.printStackTrace();
                try {
                    elem.click();
                    Assertion.raiseExceptionAndStop(e1, pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected void selectIndex(WebElement elem, int index, String elementName) {
        Select sel = null;

        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            sel = new Select(elem);
            Thread.sleep(ConfigInput.additionalWaitTime);
            sel.selectByIndex(index);
            logInfo("Select from " + elementName + ". Selected index: '" + index + "'");
        } catch (Exception e) {
            try {
                sel = new Select(elem);
                pageInfo.warning("Exception Occurred: " + e.getMessage());
                pageInfo.info("Retry Selecting By Index");
                sel.selectByIndex(index);
                logInfo("Select from " + elementName + ". Selected index: '" + index + "'");
            } catch (Exception e1) {
                pageInfo.fail("Failed to Select " + index + " from " + elementName);
                pageInfo.error(e1.toString());
                e1.printStackTrace();
                try {
                    elem.click();
                    Assertion.raiseExceptionAndStop(e1, pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected String getElementText(WebElement elem, String elementName) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            String value = elem.getText();
            pageInfo.info(elementName + "'s value is: " + value);
            return value;
        } catch (Exception e) {
            pageInfo.warning(elementName + "element Not found ");
        }
        return "";
    }

    protected String getElementAttributeValue(WebElement element, String attributeValue, String elementName) {
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
            String attribute = element.getAttribute(attributeValue);
            pageInfo.info(elementName + "'s attribute value is: " + attribute);
            return attribute;
        } catch (Exception e) {
            pageInfo.warning(elementName + "element Not found ");
        }
        return null;
    }

    protected void clickOnElement(WebElement elem, String elementName) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            Thread.sleep(ConfigInput.additionalWaitTime);
            elem.click();
            logInfo("Click on " + elementName);
        } catch (Exception e) {
            try {
                Thread.sleep(ConfigInput.additionalWaitTime);
                if (e.getMessage().contains("unexpected alert open")) {
                    pageInfo.warning("Un Expected Alert has opened");
                    driver.switchTo().alert().accept();
                }
                e.printStackTrace();
                pageInfo.warning("Exception Occured: " + e.getMessage());
                pageInfo.info("Retry Clicking");
                actions.moveToElement(elem).perform();
                actions.click(elem).perform();
                logInfo("Click on " + elementName);

            } catch (Exception e1) {
                pageInfo.fail("Failed to Click on " + elementName);
                pageInfo.error(e1.toString());
                e1.printStackTrace();
                try {
                    Assertion.raiseExceptionAndContinue(e1, pageInfo);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    protected void clickOnElementUsingJs(WebElement elem, String elementName) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(elem));
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
            js.executeScript("arguments[0].click();", elem);
            Thread.sleep(ConfigInput.additionalWaitTime);
            logInfo("Click on " + elementName + " using Js");
        } catch (Exception e) {
            pageInfo.fail("Failed to Click on " + elementName);
            pageInfo.error(e.toString());
            e.printStackTrace();
        }
    }

    protected void setDate(WebElement elem, String value, String description) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getDriver();
            js.executeScript("arguments[0].value='" + value + "';", elem);
            Thread.sleep(ConfigInput.additionalWaitTime);
            logInfo("Set Date " + description + "Value as: " + value);
        } catch (Exception e) {
            pageInfo.fail("Failed to Set Date on " + description);
            pageInfo.error(e.toString());
            e.printStackTrace();
            try {
                Assertion.raiseExceptionAndContinue(e, pageInfo);
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

    }

    protected boolean isStringPresentInWebTable(WebElement table, String text) {
        if (table.findElements(By.xpath(".//tr/td[contains(text(), '" + text + "')]")).size() > 0)
            return true;
        else
            return false;
    }

    protected boolean isStringPresentInWebTableAsLabel(WebElement table, String text) {
        if (table.findElements(By.xpath(".//tr/td/label[contains(text(), '" + text + "')]")).size() > 0)
            return true;
        else
            return false;
    }

    protected void navigateTo(String parentPageCode, String childPageCode, String pageName) throws Exception {
        fl.leftNavigation(parentPageCode, childPageCode);
        pageInfo.info("Navigate to Page: " + pageName);
    }

    protected int getTableRowCount(WebElement table) {
        try {
            Thread.sleep(1500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table.findElements(By.tagName("tr")).size();
    }

    /**
     * Get web table data under a header
     * use when data is present immediately under the header column
     *
     * @param table
     * @param headerName
     * @return
     * @throws IOException
     */
    protected String getTableDataUsingHeaderName(WebElement table, String headerName) throws IOException {
        Utils.putThreadSleep(Constants.WAIT_TIME);
        int headerIndex = getHeaderIndex(table, headerName);
        Utils.putThreadSleep(Constants.WAIT_TIME);
        String text = null;
        if (headerIndex == 0) {
            pageInfo.fail("Failed to find the header in the webtable");
            Utils.captureScreen(pageInfo);
            return null;
        }

        if (table.findElements(By.tagName("th")).size() > 0) {
            text = table.findElement(By.xpath(".//tr/th[contains(text(), '" + headerName + "')]/parent::*/following::td[" + headerIndex + "]")).getText();
        } else {
            text = table.findElement(By.xpath(".//tr/td[contains(text(), '" + headerName + "')]/parent::*/following::td[" + headerIndex + "]")).getText();
        }
        return text;
    }

    /**
     * Get Table Data - this method is very useful to get a simple webtable data in a useful format
     *
     * @param table         [webelement] - table
     * @param primaryKey    [int] column index that has to referred as primary key
     * @param excludeRow    [int] list of rows that need to be excluded
     * @param excludeColumn [int] list of columns that need to be excluded
     * @return [HashMap<String, HashMap < String, String>>] table data
     * ex. List<Integer> excludeRow = new ArrayList<>(), excludeColumn = new ArrayList<>();
     * excludeRow.add(0); // exclude first Row
     * excludeRow.add(-1); // exclude last row
     * excludeColumn.add(0); // exclude first column
     * // usage
     * HashMap<String, HashMap<String, String>> data = getTableData(this.tableBarUser, 2, excludeRow, excludeColumn);
     */
    protected HashMap<String, HashMap<String, String>> getTableData(WebElement table, int primaryKey, List<Integer> excludeRow, List<Integer> excludeColumn) {
        HashMap<String, HashMap<String, String>> data = new HashMap<>();

        // get header list
        List<String> headerList = getHeaderList(table);

        // get number of rows
        int tableRow = table.findElements(By.tagName("tr")).size();

        // iterate for each row and get corresponding values for the headers
        // exclude rows - excludeRow
        // exclude columns - excludeColumn
        for (int j = 1; j < tableRow; j++) {// excluding first row assuming it as header
            int bottonRowIndex = j - tableRow;
            if (excludeRow.contains(j) || excludeRow.contains(bottonRowIndex)) {
                continue;
            }

            int row = j + 1;
            String key = table.findElement(By.xpath(".//tr[" + row + "]/td[" + primaryKey + "]")).getText().trim();
            HashMap<String, String> rowData = new HashMap<>();
            for (int i = 0; i < headerList.size(); i++) {
                int column = i + 1;

                if (excludeColumn.contains(i)) {
                    continue;
                }

                // start forming the hashmap
                String header = headerList.get(i);
                String cellVal = table.findElement(By.xpath(".//tr[" + row + "]/td[" + column + "]")).getText().trim().toString();
                rowData.put(header, cellVal);
            }
            data.put(key, rowData);
        }
        return data;
    }


    /**
     * Get header index
     *
     * @param table
     * @param headerName
     * @return
     */
    protected int getHeaderIndex(WebElement table, String headerName) {
        List<WebElement> headerList;

        if (table.findElements(By.tagName("th")).size() > 0) {
            headerList = table.findElement(By.xpath(".//tr/th[contains(text(), '" + headerName + "')]/parent::tr[1]")).findElements(By.tagName("th"));
        } else {
            headerList = table.findElement(By.xpath(".//tr/td[contains(text(), '" + headerName + "')]/parent::tr[1]")).findElements(By.tagName("td"));
        }

        int i = 1;
        for (WebElement elem : headerList) {
            if (elem.getText().trim().equals(headerName)) {
                return i;
            }
            i++;
        }
        return 0;
    }

    private List<String> getHeaderList(WebElement table) {
        List<WebElement> headerElements;
        List<String> headerList = new ArrayList<>();
        if (table.findElements(By.tagName("th")).size() > 0) {
            headerElements = table.findElements(By.tagName("th"));
        } else {
            headerElements = table.findElements(By.className("tabhead"));
        }

        for (WebElement header : headerElements) {
            headerList.add(header.getText());
        }
        return headerList;
    }

    protected boolean elemIsEnabled(WebElement elem) {
        try {
            return elem.isEnabled();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return false;
        }

    }

}