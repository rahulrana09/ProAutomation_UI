package framework.entity;


import framework.util.common.DataFactory;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class BillNotification {
    public String Days, BillType, Name, BillTypeValue;

    /**
     * Representation of Bill Notification
     */
    public BillNotification(String name, String billType, String days) {
        this.Name = name;
        this.BillType = billType;
        this.Days = days;
        this.BillTypeValue = billType + "_" + days;

    }

    public BillNotification(String billType) {
        this.Name = "AutoNot" + DataFactory.getRandomNumber(3);
        this.BillType = billType;
        this.Days = "" + DataFactory.getRandomNumber(3);
        this.BillTypeValue = this.BillType + "_" + this.Days;
    }


}

