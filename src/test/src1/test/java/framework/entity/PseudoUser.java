package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;

/**
 * Created by amarnath.vb on 5/29/2017.
 */
public class PseudoUser {

    public String PseudoCategoryName, PseudoCategoryCode, LoginId,
            Password, MSISDN, ExternalCode,
            FirstName, LastName, Prefix,
            ContactNum, ContactPerson, Division,
            Department,ERROR,Mpin,Tpin,PseudoGroupRole;

    public User parentUser;

    public boolean isCreated = false;

    public PseudoUser() {
        this.LoginId = "PS" + DataFactory.getRandomNumber(5);
        this.Password = ConfigInput.defaultPass;
        this.MSISDN = DataFactory.getAvailableMSISDN();
        this.ExternalCode = MSISDN + "abcd";
        this.FirstName = MSISDN;
        this.LastName = MSISDN;
        this.Prefix = "Mr.";
        this.ContactNum = MSISDN;
        this.ContactPerson = this.FirstName;
        this.Division = "MFS";
        this.Department = "Mobiquity";
        this.Mpin=ConfigInput.mPin;
        this.Tpin=ConfigInput.tPin;
    }

    public String getMpin() {
        return Mpin;
    }

    public void setMpin(String mpin) {
        Mpin = mpin;
    }

    public String getTpin() {
        return Tpin;
    }

    public void setTpin(String tpin) {
        Tpin = tpin;
    }

    public void setCategoryDetails(String cat) {
        this.PseudoCategoryCode = cat;
        this.PseudoCategoryName = cat;
    }

    public void setPseudoGroupRole(String role){
        this.PseudoGroupRole = role;
    }

    public void setError(String error) {
        this.ERROR = error;
    }

    public void setParentUser(User parent){
        this.parentUser = parent;
    }

    public void writeDataToExcel() throws Exception {
        String filePath = FilePath.fileTempUsers;
        int lastRow = ExcelUtil.getExcelLastRow(filePath, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 0, this.parentUser.LoginId, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 1, this.parentUser.MSISDN, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 2, this.LoginId, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 3, this.Password, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 4, this.MSISDN, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 5, this.ExternalCode, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 6, this.PseudoCategoryCode, 2);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 7, this.PseudoCategoryName, 2);


    }

    public void setIsCreated(){
        this.isCreated = true;
    }

}
