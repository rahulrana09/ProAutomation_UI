package framework.entity;

import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;

import java.math.BigDecimal;

public class SysWalletBalance {
    private BigDecimal reconBalance, payeeBalance, payerBalance;

    public SysWalletBalance(String payeeWalletId, String payerWalletId, String providerId) {
        this.reconBalance = MobiquityDBAssertionQueries.getReconBalance();
        this.payeeBalance = MobiquityGUIQueries
                .getBalanceSystemWallet(providerId, payeeWalletId);
        this.payerBalance = MobiquityGUIQueries
                .getBalanceSystemWallet(providerId, payerWalletId);
    }

    public BigDecimal getReconBalance() {
        return reconBalance;
    }

    public BigDecimal getPayeeBalance() {
        return payeeBalance;
    }

    public BigDecimal getPayerBalance() {
        return payerBalance;
    }
}
