package framework.entity;

import framework.util.HTMLParser;
import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.excelManagement.ExcelWriter;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.propertiesManagement.CITReportProperties;
import framework.util.propertiesManagement.MfsTestProperties;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.*;

/**
 * Created by navin.pramanik on 7/21/2017.
 *
 * @author navin.pramanik
 */
public class CITReport {

    public static String INPUTHTML_PATH;
    public static String CTDATEFORMAT_INTERNAL = "yyyy-MM-dd HH:mm:ss";
    private static String description = null;
    private static String uniqueID = null;
    private static CITReportProperties properties = CITReportProperties.getInstance();
    private String buildID, leadName, testFrameworkIP, testFrameworkName, testFrameworkSVNPath,
            testExecutionDateTime, productInterface, uniqueTestCaseID, testCaseDescription, testStatus, productName;

    public CITReport() {

        this.leadName = properties.getProperty("LeadName");
        this.productInterface = properties.getProperty("ProductCIinterface");
        this.testFrameworkName = CITReportProperties.getInstance().getProperty("TestFrameworkName");
        this.testFrameworkIP = CITReportProperties.getInstance().getProperty("TestFrameworkIP");
        this.testFrameworkSVNPath = CITReportProperties.getInstance().getProperty("TestFrameworkSVNPath");
        this.testExecutionDateTime = DataFactory.getCurrentDateTime();
        this.buildID = CITReportProperties.getInstance().getProperty("BUILDID");

    }

    public CITReport(String inputHTMLPath) {
        this();
        this.INPUTHTML_PATH = inputHTMLPath;

    }

    public static void generateCTReport() throws IOException {

        File directory = new File(FilePath.dirCITReport);

        if (!directory.exists()) {
            directory.mkdir();
        }

        Object[][] reporterObject = HTMLParser.generateReporterObject();

        ExcelWriter.writeTestCaseSheetNew(FilePath.dirCITReport, "TestCases", reporterObject);
    }

    public static void main(String args[]) {


        try {
            String path = "C:\\AutomationSuite\\Econet\\New\\dev setup\\mobiquityUI\\reports\\";
            String fileName = "PVG_P1_TEST_SUITE_08-04-2019_13-18.html";
            CITReport CIT = new CITReport(path + fileName);
            CIT.generateCTReport();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void readBuildID() throws Exception {

        String BuildIDPath = CITReportProperties.getInstance().getProperty("BuildIDPath");
        File fileName = new File(BuildIDPath);

        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            String line;
            String buildID = null;
            while ((line = br.readLine()) != null) {
                buildID = line.trim();
            }
            System.out.println("Build ID is -->" + buildID);
        } finally {
            br.close();
        }
    }

    public void setReportFilePath(String inputHTMLPath) {
        this.INPUTHTML_PATH = inputHTMLPath;
    }

    public String getTestStatus() {
        return this.testStatus;
    }

    public void setTestStatus(String status) {
        this.testStatus = status;
    }

    public String getLeadName() {
        return this.leadName;
    }

    public String getTestExecutionDateTime() {
        return this.testExecutionDateTime;
    }

    public String getTestFrameworkName() {
        return this.testFrameworkName;
    }

    public String getUniqueTestCaseID() {
        return this.uniqueTestCaseID;
    }

    public void setUniqueTestCaseID(String id) {
        this.uniqueTestCaseID = id;
    }

    public String getProductInterface() {
        return this.productInterface;
    }

    public String getTestFrameworkIP() {
        return this.testFrameworkIP;
    }

    public String getBuildID() {
        return this.buildID;
    }

    public String getTestFrameworkSVNPath() {
        return this.testFrameworkSVNPath;
    }

    public String getTestCaseDescription() {
        return this.testCaseDescription;
    }

    public void setTestCaseDescription(String desc) {
        this.testCaseDescription = desc;
    }

    public void generateCITReport() throws IOException, InvalidFormatException {

        try {

            //For creating Directory if not already there
            File directory = new File(FilePath.dirCITReport);

            if (!directory.exists()) {
                directory.mkdir();
            }
            //For CIT report Header
            ExcelUtil.writeCITReportFileHeader();

            if (MfsTestProperties.getInstance().getProperty("run.baseset.grouprole").equalsIgnoreCase("Yes")) {
                readWebGroupRoleFile();
                readMobileGroupRoleFile();
            }

            if (MfsTestProperties.getInstance().getProperty("run.baseset.customertcp").equalsIgnoreCase("Yes")) {
                readCustomerTCPFile();
            }

            if (MfsTestProperties.getInstance().getProperty("run.baseset.instrumenttcp").equalsIgnoreCase("Yes")) {
                readInstrumentTCPFile();
            }

            if (MfsTestProperties.getInstance().getProperty("run.baseset.operatoruser").equalsIgnoreCase("Yes")) {
                readOperatorUserFileandWriteToCIT();
            }

            if (MfsTestProperties.getInstance().getProperty("run.baseset.channeluser").equalsIgnoreCase("Yes")) {
                readChannelUserFileandWriteToCIT();
            }


        } catch (FileNotFoundException fe) {
            System.out.println("File not Found ... ");
        } catch (IOException ioe) {
            System.out.println("IO Exception... ");
        }

    }

    public void readWebGroupRoleFile() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileGroupRoles);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {
            String category = formatter.formatCellValue(sheet.getRow(i).getCell(1));
            String created = formatter.formatCellValue(sheet.getRow(i).getCell(8));
            String error = formatter.formatCellValue(sheet.getRow(i).getCell(9));

            uniqueID = "WEB_GroupRole_Add_" + i;
            description = "To verify that valid user should be able to add WEB Group role for " + category;
            setTestCaseDescription(description);
            setUniqueTestCaseID(uniqueID);

            if (created.equalsIgnoreCase("Y") && (error.equalsIgnoreCase("") || error.equals("null"))) {
                setTestStatus("Pass");
            } else {
                setTestStatus("Fail");
            }

            //Writing values to Excel
            writeDataToExcel();

        }
    }

    /**
     * The below method will read Mobile Group Role Output file
     * and Write the appropriate values to CIT Report file
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public void readMobileGroupRoleFile() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileMobileRoles);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {

            String created = formatter.formatCellValue(sheet.getRow(i).getCell(7));
            String payInstrumentType = formatter.formatCellValue(sheet.getRow(i).getCell(6));
            String payInstrument = formatter.formatCellValue(sheet.getRow(i).getCell(5));
            String grade = formatter.formatCellValue(sheet.getRow(i).getCell(3));

            uniqueID = "Mobile_GroupRole_Add_" + i;
            description = "To verify that valid user should be able to add Mobile Group role for " + grade
                    + " for Payment Instrument type " + payInstrumentType + " " + payInstrument;
            setTestCaseDescription(description);
            setUniqueTestCaseID(uniqueID);

            if (created.equalsIgnoreCase("ACTIVE")) {
                setTestStatus("Pass");
            } else {
                setTestStatus("Fail");
            }
            //Writing values to Excel
            writeDataToExcel();

        }
    }

    public void readCustomerTCPFile() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileConsumerTCP);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {

            String domain = formatter.formatCellValue(sheet.getRow(i).getCell(1));
            String category = formatter.formatCellValue(sheet.getRow(i).getCell(2));
            String created = formatter.formatCellValue(sheet.getRow(i).getCell(4));
            String regType = formatter.formatCellValue(sheet.getRow(i).getCell(3));
            String approved = formatter.formatCellValue(sheet.getRow(i).getCell(5));
            String error = formatter.formatCellValue(sheet.getRow(i).getCell(6));

            uniqueID = "CustomerTCP_" + i;
            description = "To verify that valid user should be able to Customer TCP for " + category
                    + " for Registration Type " + regType;

            setTestCaseDescription(description);
            setUniqueTestCaseID(uniqueID);

            if (!(error.equalsIgnoreCase("") || error.equals("null"))) {
                setTestStatus("Fail");
            } else {
                setTestStatus("Pass");
            }
            //Writing values to Excel
            writeDataToExcel();

        }
    }

    public void readInstrumentTCPFile() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileInstrumentTCP);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {

            String domain = formatter.formatCellValue(sheet.getRow(i).getCell(2));
            String category = formatter.formatCellValue(sheet.getRow(i).getCell(3));
            String grade = formatter.formatCellValue(sheet.getRow(i).getCell(4));
            String paymentInstrument = formatter.formatCellValue(sheet.getRow(i).getCell(6));
            String created = formatter.formatCellValue(sheet.getRow(i).getCell(7));
            String approved = formatter.formatCellValue(sheet.getRow(i).getCell(8));
            String error = formatter.formatCellValue(sheet.getRow(i).getCell(9));

            uniqueID = "InstrumentTCP_" + i;
            description = "To verify that valid user should be able to add Instrument TCP for " + category + " having Grade " + grade
                    + " for Payment Instrument " + paymentInstrument;

            setTestCaseDescription(description);
            setUniqueTestCaseID(uniqueID);

            if (!(error.equalsIgnoreCase("") || error.equals("null"))) {
                setTestStatus("Fail");
            } else {
                setTestStatus("Pass");
            }
            //Writing values to Excel
            writeDataToExcel();

        }
    }

    /**
     * readOperatorUserFileandWriteToCIT() method will Read Operator User File
     * and write the values to CIT Report File
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public void readOperatorUserFileandWriteToCIT() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileOperatorUsers);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();

        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {

            String category = formatter.formatCellValue(sheet.getRow(i).getCell(0));
            String created = formatter.formatCellValue(sheet.getRow(i).getCell(8));
            String approved = formatter.formatCellValue(sheet.getRow(i).getCell(9));
            String passwordChanged = formatter.formatCellValue(sheet.getRow(i).getCell(10));
            String error = formatter.formatCellValue(sheet.getRow(i).getCell(11));

            uniqueID = "OperatorUser_Creation_" + i;
            description = "To verify that valid user should be able to add " + category;

            if ((error.equalsIgnoreCase("") || error.equals("null"))) {

                if (created.equalsIgnoreCase("Y")) {
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }

                if (approved.equalsIgnoreCase("Y")) {
                    description = "To verify that valid user should be able to approve " + category;
                    uniqueID = "OperatorUser_Approval_" + i;
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }
                if (passwordChanged.equalsIgnoreCase("Y")) {
                    description = "To verify that " + category + " should be able to change First Time Password";
                    uniqueID = "OperatorUser_PasswordChange_" + i;
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }
            } else {
                setTestStatus("Fail");
                writeDataToExcel();
            }

        }
    }

    public void readChannelUserFileandWriteToCIT() throws IOException, InvalidFormatException {

        InputStream input = new FileInputStream(FilePath.fileChannelUsers);
        Workbook wb = WorkbookFactory.create(input);
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();
        /* get last used row */
        int lastRow = sheet.getLastRowNum();

        for (int i = 1; i < lastRow; i++) {
            String category = formatter.formatCellValue(sheet.getRow(i).getCell(0));
            String grade = formatter.formatCellValue(sheet.getRow(i).getCell(18));
            String created = formatter.formatCellValue(sheet.getRow(i).getCell(9));
            String approved = formatter.formatCellValue(sheet.getRow(i).getCell(10));
            String passwordChanged = formatter.formatCellValue(sheet.getRow(i).getCell(11));
            String error = formatter.formatCellValue(sheet.getRow(i).getCell(14));

            uniqueID = "ChannelUser_Creation_" + i;
            description = "To verify that valid user should be able to add " + category + " having grade " + grade;

            if ((error.equalsIgnoreCase("") || error.equals("null"))) {
                if (created.equalsIgnoreCase("Y")) {
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }

                if (approved.equalsIgnoreCase("Y")) {
                    description = "To verify that valid user should be able to approve " + grade;
                    uniqueID = "Channel_Approval_" + i;
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }
                if (passwordChanged.equalsIgnoreCase("Y")) {
                    description = "To verify that " + grade + " should be able to change First Time Password";
                    uniqueID = "Channel_PasswordChange_" + i;
                    setTestCaseDescription(description);
                    setUniqueTestCaseID(uniqueID);
                    setTestStatus("Pass");
                    writeDataToExcel();
                }
            } else {
                setTestStatus("Fail");
                writeDataToExcel();
            }


        }
    }

    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.CIT_REPORT_OUTPUT_FILE);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_0, this.buildID);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_1, this.leadName);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_2, this.testFrameworkIP);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_3, this.testFrameworkName);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_4, this.testFrameworkSVNPath);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_5, this.testExecutionDateTime);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_6, this.productInterface);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_7, this.uniqueTestCaseID);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_8, this.testCaseDescription);
        ExcelUtil.WriteDataToExcel(FilePath.CIT_REPORT_OUTPUT_FILE, lastRow, NumberConstants.CONST_9, this.testStatus);

    }


}
