package framework.entity;

import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalVars.GlobalData;

/**
 * Created by automation team on 3/13/2019.
 */
public class LMSProfile {
    public String profileName, fromDate, toDate, serviceCode, payerProvider, payeeProvider, payerPayInstId, payeePayInstId, payerPayInst, payeePayInst;
    public User payer, payee;

    public LMSProfile(User payer, User payee, String serviceCode, int fromDate, int toDate){
        this.payer = payer;
        this.payee = payee;
        this.serviceCode = serviceCode;
        this.profileName = "LP"+ DataFactory.getRandomNumberAsString(5);
        this.fromDate = new DateAndTime().getDate("dd-MM-yyyy", fromDate) + " 02:30:00 PM";
        this.toDate = new DateAndTime().getDate("dd-MM-yyyy", toDate) + " 02:30:00 PM";
        this.payerProvider = GlobalData.defaultProvider.ProviderId;
        this.payeeProvider = GlobalData.defaultProvider.ProviderId;
        this.payerPayInstId = GlobalData.defaultWallet.WalletId;
        this.payeePayInstId = GlobalData.defaultWallet.WalletId;
        this.payerPayInst = "WALLET";
        this.payeePayInst = "WALLET";
    }

    public void setPayerProvider(String payerProvider) {
        this.payerProvider = payerProvider;
    }

    public void setPayeeProvider(String payeeProvider) {
        this.payeeProvider = payeeProvider;
    }

    public void setPayerPayInstId(String payerPayInstId) {
        this.payerPayInstId = payerPayInstId;
    }

    public void setPayeePayInstId(String payeePayInstId) {
        this.payeePayInstId = payeePayInstId;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public void setPayerPayInst(String payerPayInst) {
        this.payerPayInst = payerPayInst;
    }

    public void setPayeePayInst(String payeePayInst) {
        this.payeePayInst = payeePayInst;
    }
}
