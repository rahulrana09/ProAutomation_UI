package framework.entity;

import framework.util.globalConstant.Constants;

/**
 * Created by Automation.team
 */
public class BillerCategory {
    public String BillerCategoryName, BillerCategoryCode, Description, Status,
            isCreated = "N", isUpdated = "N";


    /**
     * Constructor for Biller category
     *
     * @param catCode
     * @param catName
     * @param desc
     * @throws Exception
     */
    public BillerCategory(String catCode, String catName, String desc) {
        this.BillerCategoryCode = catCode;
        this.BillerCategoryName = catName;
        this.Description = desc;
    }


    /**
     * This will assisgn the category code to code, name and description as well
     *
     * @param catCode
     * @throws Exception
     */
    public BillerCategory(String catCode) {
        this.BillerCategoryCode = catCode.substring(0, 10);
        ;
        this.BillerCategoryName = catCode;
        this.Description = catCode;
    }

    /**
     * Set The Biller Category Name
     *
     * @param name
     */
    public void setBillerCategoryName(String name) {
        this.BillerCategoryName = name;
    }

    /**
     * To set the Biller Category Code
     *
     * @param code
     */
    public void setBillerCategoryCode(String code) {
        this.BillerCategoryCode = code;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public void setIsCreated() {
        this.isCreated = "Y";
        this.Status = Constants.STATUS_ACTIVE;
    }

    public void setIsUpdated() {
        this.isUpdated = "Y";
    }

    public void setStatus(String status) {
        this.Status = status;
    }

}
