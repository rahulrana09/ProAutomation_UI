package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul.rana on 7/28/2017.
 */
public class Biller {
    private static final Logger LOGGER = LoggerFactory.getLogger(Biller.class);
    public String ProviderName,
            ProviderId,
            CategoryCode,
            CategoryName,
            DomainName,
            DomainCode,
            GradeName,
            LoginId,
            Password,
            ConfirmPassword,
            AllowedIp,
            BillerName,
            BillerCode,
            Email,
            Address,
            ServiceLevel,
            PaidNotificationFrequency,
            BillerCategoryName,
            ProcessType,
            BillAmount,
            GradeCode,
            BillerType,
            PaymentEffectedTo,
            TCP = null,
            isCreated = "N",
            BillerCategoryCode,
            PaymentSubType,
            DeleteFrequency,
            Status,
            LiquidationFrequency,
            LiquidationDay,
            LiquidationBankAccountNumber,
            LiquidationBankBranchName,
            AccountHolderName,
            FrequencyTradingName,
            ContactName;

    public boolean supportOnlineTxnReversal = false;

    public void setOnlineTxnReversalStatus(boolean status) {
        this.supportOnlineTxnReversal = status;
    }

    public List<BillValidation> ValidationList = new ArrayList(); // this stores all the validation
    public List<CustomerBill> BillList = new ArrayList(); // this stores all the Bills

    /**
     * @deprecated
     * @param providerName
     * @param billerCategoryName
     * @param processType
     * @param serviceLevel
     * @throws Exception
     */
    public Biller(String providerName, String billerCategoryName, String processType, String serviceLevel) throws Exception {
        this.ProviderName = providerName;
        this.BillerCategoryName = billerCategoryName;
        this.ProcessType = processType;
        this.ServiceLevel = serviceLevel;

        setGeneralInformation(); // set generic Biller info

        this.PaidNotificationFrequency = "45";
        this.PaymentEffectedTo = Constants.BILLER_PAYMENT_EFFECTED_TO_WALLET;
        this.DeleteFrequency = Constants.BILLER_BILL_DELETION_FREQUENCY;
        this.LiquidationFrequency = "Daily";
        this.LiquidationDay = null;
        this.LiquidationBankAccountNumber = null;
        this.LiquidationBankBranchName = null;
        this.AccountHolderName = null;
        this.FrequencyTradingName = null;

        if (serviceLevel.equalsIgnoreCase(Constants.BILL_SERVICE_LEVEL_ADHOC)) {
            this.BillerType = BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE;
            this.BillAmount = BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE;
            this.PaymentSubType = BillerAttribute.SUB_TYPE_NOT_APPLICABLE;
        } else {
            this.BillerType = BillerAttribute.BILLER_TYPE_BILL_AVAILABLE;
            this.BillAmount = BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT;
            this.PaymentSubType = BillerAttribute.SUB_TYPE_NOT_APPLICABLE;
        }
    }

    /**
     * New Method for detailed biller creation
     *
     * @param providerName
     * @param billerCategoryName
     * @param processType
     * @param serviceLevel
     * @param billerType
     * @param billAmount
     * @param subType
     * @throws Exception
     */
    public Biller(String providerName, String billerCategoryName, String processType,
                  String serviceLevel, String billerType, String billAmount, String subType) throws Exception {

        this.ProviderName = providerName;
        setGeneralInformation(); // set generic Biller info
        this.BillerCategoryName = billerCategoryName;
        this.ProcessType = processType;
        this.ServiceLevel = serviceLevel;
        this.BillerType = billerType;
        this.BillAmount = billAmount;
        this.PaymentSubType = subType;
        assertBillerOnbordingIsValid();
    }


    private void setGeneralInformation() throws Exception {
        this.ProviderId = DataFactory.getProviderId(this.ProviderName);
        this.CategoryCode = Constants.BILL_COMPANY;
        this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
        this.DomainCode = DataFactory.getDomainName(this.CategoryCode);
        this.DomainName = DataFactory.getDomainName(this.CategoryCode);
        this.GradeName = DataFactory.getGradesForCategory(this.CategoryCode).get(0).GradeName;
        this.GradeCode = DataFactory.getGradeCode(this.GradeName);
        this.LoginId = this.CategoryCode + DataFactory.getRandomNumber(5);
        this.Password = ConfigInput.defaultPass;
        this.ConfirmPassword = ConfigInput.defaultPass;
        this.BillerName = this.LoginId;
        this.ContactName = this.LoginId;
        this.BillerCode = "" + DataFactory.getRandomNumber(4);
        this.Email = "biller@xyz.com";
        this.Address = "Gurgaon, India";
        this.PaymentEffectedTo = Constants.BILLER_PAYMENT_EFFECTED_TO_WALLET;
        this.PaidNotificationFrequency = "45";
        this.DeleteFrequency = Constants.BILLER_BILL_DELETION_FREQUENCY;
        this.LiquidationFrequency = "Daily";
        this.LiquidationDay = null;
        this.LiquidationBankAccountNumber = null;
        this.LiquidationBankBranchName = null;
        this.AccountHolderName = null;
        this.FrequencyTradingName = null;
        this.TCP = DataFactory.getInstrumentTCP(this.DomainName,
                this.CategoryName,
                this.GradeName,
                this.ProviderName,
                DataFactory.getDefaultWallet().WalletName).ProfileName;
        BillValidation defaultValidation = new BillValidation("DEFAULT", Constants.BILL_TYPE_ALPHANUMERIC, "1", "10", null, null, Constants.BILL_REFTYPE_REGISTRATION);
        addValidation(defaultValidation);
    }

    /**
     * For reading Data From excel
     *
     * @param billerCode
     * @param categoryCode
     * @param gradeName
     * @param loginId
     * @param password
     * @param providerName
     * @param billerCategoryName
     * @param tcp
     */
    public Biller(String billerCode, String categoryCode, String gradeName, String loginId, String password,
                  String providerName, String billerCategoryName, String tcp, String gradeCode, String status,
                  String serviceLevel, String processtype, String billerType, String billAmount, String paymentSubType) {
        this.BillerCode = billerCode;
        this.LoginId = loginId;
        this.Password = password;
        this.CategoryCode = categoryCode;
        this.DomainName = DataFactory.getDomainName(categoryCode);
        this.DomainCode = DataFactory.getDomainCode(categoryCode);
        this.GradeName = gradeName;
        this.ProviderName = providerName;
        this.ProviderId = DataFactory.getProviderId(providerName);
        this.BillerCategoryName = billerCategoryName;
        this.TCP = tcp;
        this.Status = status;
        this.GradeCode = gradeCode;
        this.ServiceLevel = serviceLevel;
        this.ProcessType = processtype;
        this.BillerType = billerType;
        this.BillAmount = billAmount;
        this.PaymentSubType = paymentSubType;

        BillValidation defaultValidation = new BillValidation("DEFAULT", Constants.BILL_TYPE_ALPHANUMERIC, "1", "10", null, null, Constants.BILL_REFTYPE_REGISTRATION);
        addValidation(defaultValidation);
    }

    public void setProviderName(String providerName) {
        this.ProviderName = providerName;
    }

    public void setProviderId(String providerId) {
        this.ProviderId = providerId;
    }

    public void setCategoryCode(String catCode) {
        this.CategoryCode = catCode;
    }

    public void setCategoryName(String catName) {
        this.CategoryName = catName;
    }

    public void setDomainName(String domainName) {
        this.DomainName = domainName;
    }

    public void setDomainCode(String domainCode) {
        this.DomainCode = domainCode;
    }

    public void setGradeName(String gradeName) {
        this.GradeName = gradeName;
    }

    public void setLoginId(String loginId) {
        this.LoginId = loginId;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public void setConfirmPassword(String password) {
        this.ConfirmPassword = password;
    }

    public void setAllowedIp(String allowedIp) {
        this.AllowedIp = allowedIp;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public void setAddress(String address) {
        this.Address = address;
    }

    public void setServiceLevel(String serviceLevel) {
        this.ServiceLevel = serviceLevel;
    }

    public void setPaidNotificationFrequency(String paidNotificationFrequency) {
        this.PaidNotificationFrequency = paidNotificationFrequency;
    }

    public void setBillerCategoryName(String billerCategoryName) {
        this.BillerCategoryName = billerCategoryName;
    }

    public void setProcessType(String processType) {
        this.ProcessType = processType;
    }

    public void setBillerType(String billerType) {
        this.BillerType = billerType;
    }

    public void setBillAmount(String billAmount) {
        this.BillAmount = billAmount;
    }

    public void setPaymentEffectedTo(String paymentEffectedTo) {
        this.PaymentEffectedTo = paymentEffectedTo;
    }

    public void setTCP(String TCP) {
        this.TCP = TCP;
    }

    public void setBillerCategoryCode(String billerCategoryCode) {
        this.BillerCategoryCode = billerCategoryCode;
    }

    public void setBillerName(String billerName) {
        this.BillerName = billerName;
    }

    public void setContactName(String billerName) {
        this.ContactName = billerName;
    }

    public void setBillerCode(String billerCode) {
        this.BillerCode = billerCode;
    }

    public void setPaymentSubType(String paymentSubType) {
        this.PaymentSubType = paymentSubType;
    }

    public void setDeleteFrequency(String deleteFrequency) {
        this.DeleteFrequency = deleteFrequency;
    }

    public void setLiquidationFrequency(String liquidationFrequency) {
        this.LiquidationFrequency = liquidationFrequency;

    }

    public void setLiquidationDay(String day) {
        this.LiquidationDay = day;

    }

    public String getLiquidationBankAccountNumber() {
        return LiquidationBankAccountNumber;
    }

    public void setLiquidationBankAccountNumber(String liquidationBankAccountNumber) {
        LiquidationBankAccountNumber = liquidationBankAccountNumber;
    }

    public String getFrequencyTradingName() {
        return FrequencyTradingName;
    }

    public void setFrequencyTradingName(String frequencyTradingName) {
        FrequencyTradingName = frequencyTradingName;
    }

    public String getLiquidationBankBranchName() {
        return LiquidationBankBranchName;
    }

    public void setLiquidationBankBranchName(String liquidationBankBranchName) {
        LiquidationBankBranchName = liquidationBankBranchName;
    }

    public String getAccountHolderName() {
        return AccountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        AccountHolderName = accountHolderName;
    }

    /**
     * Add validation
     *
     * @param validation
     */
    public void addValidation(BillValidation validation) {
        this.ValidationList.add(validation);
    }

    /**
     * Add Bill For Customer
     *
     * @param customerMsisdn
     * @param billAccNum
     */
    public void addBillForCustomer(String customerMsisdn, String billAccNum) {
        CustomerBill bill = new CustomerBill(this, customerMsisdn, billAccNum, "Test Bill");
        this.BillList.add(bill);
    }

    /**
     * Remove a bill associated with a Customer
     *
     * @param customerMsisdn
     * @param billAccNum
     */
    public void removeBillForCustomer(String customerMsisdn, String billAccNum) {
        for (CustomerBill bill : this.BillList) {
            if (bill.CustomerMsisdn.equals(customerMsisdn) && bill.BillAccNum.equals(billAccNum)) {
                this.BillList.remove(bill);
            }
        }
    }

    /**
     * Remove all Bills
     */
    public void removeAllBills() {
        try {
            for (CustomerBill bill : this.BillList) {
                this.BillList.remove(bill);
            }
        } catch (Exception e) {
        }

    }

    /**
     * Get a Bill not yet Associated with any customer
     *
     * @return
     */
    public CustomerBill getNonAssociatedBill() {
        for (CustomerBill bill : this.BillList) {
            if (!bill.isAlreadyAssociated) {
                return bill;
            }
        }
        return null;
    }

    // TODO -  Remove Biller Validation

    public CustomerBill getAssociatedBill() {
        for (CustomerBill bill : this.BillList) {
            if (bill.isAlreadyAssociated) {
                return bill;
            }
        }
        return null;
    }

    public void setIsCreated() {
        this.isCreated = "Y";
    }

    public boolean isSuspended() {
        if (this.Status.equals(Constants.STATUS_SUSPEND)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isBarred() {
        if (this.Status.equals(Constants.BAR_AS_SENDER) ||
                this.Status.equals(Constants.BAR_AS_RECIEVER) ||
                this.Status.equals(Constants.BAR_AS_BOTH)) {
            return true;
        } else {
            return false;
        }
    }

    public String getStatus() {
        return this.Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    /**
     * Write Data to excel
     */
    public void writeDataToExcel() throws IOException {
        ExcelUtil.writeTempBillerHeader();
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileTempBillers);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_0, this.BillerCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_1, this.LoginId);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_2, this.Password);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_3, this.CategoryCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_4, this.GradeName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_5, this.ProviderName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_6, this.BillerCategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_7, this.TCP);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_8, this.isCreated);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_9, this.Status);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_10, this.GradeCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_11, this.ServiceLevel);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, NumberConstants.CONST_12, this.ProcessType);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, 13, this.BillerType);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, 14, this.BillAmount);
        ExcelUtil.WriteDataToExcel(FilePath.fileTempBillers, lastRow, 15, this.PaymentSubType);
    }

    /**
     * Verify that Biller On-boarding Combination is valid
     */
    public void assertBillerOnbordingIsValid() {
        boolean isValid = false;

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_ADHOC) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_ONLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE) &&
                this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_ADHOC) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE) &&
                this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_STANDARD) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT) &&
                this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_STANDARD) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_PART_PAYMENT) &&
                (this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_UNDER_PAYMENT)
                        || this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_OVER_PAYMENT))) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_PREMIUM) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT) &&
                this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_PREMIUM) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_PART_PAYMENT) &&
                (this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_UNDER_PAYMENT)
                        || this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_OVER_PAYMENT))) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_DELUX) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_PART_PAYMENT) &&
                (this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_UNDER_PAYMENT)
                        || this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_OVER_PAYMENT))) {
            isValid = true;
        }

        if (this.ServiceLevel.equalsIgnoreCase(BillerAttribute.SERVICE_LEVEL_DELUX) &&
                this.ProcessType.equalsIgnoreCase(BillerAttribute.PROCESS_TYPE_OFFLINE) &&
                this.BillerType.equalsIgnoreCase(BillerAttribute.BILLER_TYPE_BILL_AVAILABLE) &&
                this.BillAmount.equalsIgnoreCase(BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT) &&
                this.PaymentSubType.equalsIgnoreCase(BillerAttribute.SUB_TYPE_NOT_APPLICABLE)) {
            isValid = true;
        }

        if (isValid) {
            LOGGER.info("Biller Onboarding Combination is valid");
        } else {
            LOGGER.info("Biller Onboarding Combination is not valid");
            Assert.assertFalse(true); // exit execution
        }
    }

}
