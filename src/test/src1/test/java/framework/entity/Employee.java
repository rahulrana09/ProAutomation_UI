package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;

public class Employee {

    public String FirstName, ID, LastName, UserMSISDN, ERROR;
    public boolean isCreated= false;

    public Employee(String userMsisdn, String userCategory) {
        this.UserMSISDN = userMsisdn;
        this.FirstName = "EMP" + userCategory + DataFactory.getRandomNumberAsString(3);
        this.LastName = "EmpLastName" + DataFactory.getRandomNumberAsString(3);
        this.ID = DataFactory.getRandomNumberAsString(1);
    }

    public void writeDataToExcel() throws Exception {
        String filePath = FilePath.fileTempUsers;
        int lastRow = ExcelUtil.getExcelLastRow(filePath, 3);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 0, this.UserMSISDN, 3);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 1, this.FirstName, 3);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 2, this.LastName, 3);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 3, this.ID, 3);
    }

    public void setIsCreated(){
        this.isCreated = true;
    }

    /*      Set Genereal Information
    * */
    public void setError(String error) {
        this.ERROR = error;
    }
}

