package framework.entity;

import framework.util.common.DataFactory;

public class Liquidation {

    public String LiquidationBankAccountNumber, LiquidationBankBranchName, AccountHolderName, FrequencyTradingName;


    public Liquidation() throws Exception {
        this.LiquidationBankAccountNumber = "" + DataFactory.getRandomNumber(9);
        this.LiquidationBankBranchName = "LiqBranch" + DataFactory.getRandomNumber(4);
        ;
        this.AccountHolderName = "LiqAccount" + DataFactory.getRandomNumber(3);
        this.FrequencyTradingName = "LiqBranchAccount" + DataFactory.getRandomNumber(4);

    }
}


