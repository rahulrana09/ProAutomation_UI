package framework.entity;

import framework.util.common.DataFactory;

public class SystemBankAccount {
    public String Type, BankName, BankId, WalletCode;

    public SystemBankAccount(String bankName, String walletCode) {
        this.Type = "Bank";
        this.BankName = bankName;
        this.BankId = DataFactory.getBankId(bankName);
        this.WalletCode = walletCode;
    }
}
