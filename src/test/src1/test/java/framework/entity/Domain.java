package framework.entity;

import framework.util.common.DataFactory;

public class Domain {


    public String domainCode;
    public String domainName;

    public Domain() {
        this.domainName = "NewDom" + DataFactory.getRandomNumber(3);
        this.domainCode = DataFactory.getRandomNumber(5) + "D";
    }

    public Domain(String domainCode,String domainName) {
        this.domainName = domainName;
        this.domainCode = domainCode;
    }

    public void setDomainCode(String Code) {
        this.domainCode = Code;
    }

    public void setDomainName(String Name) {
        this.domainName = Name;
    }
}

