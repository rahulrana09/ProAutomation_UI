package framework.entity;

import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rahul.rana on 5/28/2017.
 */
public class MobileGroupRole {
    public String ProfileName,
            ProfileCode,
            CategoryName,
            DomainName,
            DomainCode,
            CategoryCode,
            GradeName,
            RoleType,
            MobileProvider,
            MobilePayInst,
            MobilePayInstType,
            MobilePayInstName;

    public String MobileRoleStatus = "N";
    public List<String> ApplicableRoles = new ArrayList<>();
    public List<String> notApplicableRoles = new ArrayList<>();
    public boolean isCreated = false;

    /**
     * constructor Mobile Group Role
     *
     * @param categoryCode
     */
    public MobileGroupRole(String categoryCode, String gradeName) {
        this.ProfileName = Constants.MOBILE_ROLE_PREFIX + DataFactory.getRandomNumber(4);
        this.ProfileCode = "MOBC" + DataFactory.getRandomNumber(4);
        this.CategoryCode = categoryCode;
        this.GradeName = gradeName;
        this.RoleType = "MOB";
        setMobileRolesDetail();
    }

    public void setIsCreated(){
        this.isCreated = true;
        this.MobileRoleStatus = "ACTIVE";
    }

    public void setIsActive(){
        this.MobileRoleStatus = "ACTIVE";
    }



    public void addApplicableService(String serviceCode){
        this.ApplicableRoles.add(serviceCode);
    }

    public void removeApplicableService(String serviceCode){
        this.notApplicableRoles.add(serviceCode);
    }

    /**
     * Data representation of Mobile Group Role
     * UI table Data
     *
     * @param groupRoleName
     * @param status
     * @param provider
     * @param payInst
     * @param payInstType
     */
    public MobileGroupRole(String groupRoleName, String roleCode, String status, String provider, String payInst, String payInstType) {
        this.ProfileName = groupRoleName;
        this.ProfileCode = roleCode;
        this.MobileRoleStatus = status;
        this.MobileProvider = provider;
        this.MobilePayInst = payInst;
        this.MobilePayInstType = payInstType;
    }

    /**
     * Representation from Output Excel File
     *
     * @param roleName
     * @param domainName
     * @param categoryname
     * @param gradeName
     * @param provider
     * @param payInst
     * @param payInstType
     * @param status
     */
    public MobileGroupRole(String roleName, String roleCode, String domainName, String categoryname,
                           String gradeName, String provider, String payInst, String payInstType, String status) {
        this.ProfileName = roleName;
        this.ProfileCode = roleCode;
        this.DomainName = domainName;
        this.CategoryName = categoryname;
        this.GradeName = gradeName;
        this.MobileRoleStatus = status;
        this.MobileProvider = provider;
        this.MobilePayInst = payInst;
        this.MobilePayInstType = payInstType;
    }

    /**
     * Used in method fetchAvailableMobileGroupRoles Get Mobile roles from DB
     * @param catCode
     * @param gradeCode
     * @param roleName
     * @param roleCode
     * @param payInstrument
     * @param providerId
     * @param bankId
     * @param walletId
     */
    public MobileGroupRole(String catCode,
                           String gradeCode,
                           String roleName,
                           String roleCode,
                           String payInstrument,
                           String providerId,
                           String bankId,
                           String walletId) {
        this.ProfileName = roleName;
        this.ProfileCode = roleCode;
        this.MobileRoleStatus = "ACTIVE";
        this.MobileProvider = DataFactory.getProviderName(providerId);
        this.MobilePayInst = payInstrument;
        this.GradeName = DataFactory.getGradeName(gradeCode);
        this.CategoryCode = catCode;
        this.CategoryName = DataFactory.getCategoryName(catCode);
        this.DomainName = DataFactory.getDomainName(catCode);

        if(bankId != null && payInstrument.equalsIgnoreCase("bank")){
            this.MobilePayInstType = DataFactory.getBankName(bankId);
        }else if(walletId != null && payInstrument.equalsIgnoreCase("wallet")){
            this.MobilePayInstType = DataFactory.getWalletName(walletId);
        }


    }
    public void setMobileRoleName(String name) {
        this.ProfileName = name;
    }

    public void setMobileRoleCode(String code) {
        this.ProfileCode = code;
    }

    public void setRoleStatusasFail() {
        this.MobileRoleStatus = "FAIL";
    }

    public void setProvider(String provider) {
        this.MobileProvider = provider;
    }

    public void setPayInstrument(String payInst) {
        this.MobilePayInst = payInst;
    }

    public void setPayInstrumentType(String payInstType) {
        this.MobilePayInstType = payInstType;
    }

    public void setPayInstrumentDetails(String payInstId) {
        this.MobilePayInstType = payInstId;
        if(this.MobilePayInst.equals("BANK"))
            this.MobilePayInstName = DataFactory.getBankName(payInstId);
        else
            this.MobilePayInstName = DataFactory.getWalletName(payInstId);
    }

    public void setPayInstrumentDetailsUsingName(String payInstName) {
        this.MobilePayInstName = payInstName;
        if(this.MobilePayInst.equals("BANK"))
            this.MobilePayInstType = DataFactory.getBankId(payInstName);
        else
            this.MobilePayInstType = DataFactory.getWalletId(payInstName);
    }

    public void setMobileProvider(String mobileProvider) {
        this.MobileProvider = mobileProvider;
    }

    /**
     * setGroupRolesDetail
     */
    private void setMobileRolesDetail() {
        this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
        this.DomainName = DataFactory.getDomainName(this.CategoryCode);
        this.DomainCode = DataFactory.getDomainCode(this.CategoryCode);
    }

    /**
     * Write To Output Excel File
     */
    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileMobileRoles);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 0, "" + this.ProfileName);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 1, "" + this.ProfileCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 2, "" + this.DomainName);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 3, "" + this.CategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 4, "" + this.GradeName);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 5, "" + this.MobileProvider);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 6, "" + this.MobilePayInst);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 7, "" + this.MobilePayInstType);
        ExcelUtil.WriteDataToExcel(FilePath.fileMobileRoles, lastRow, 8, "" + this.MobileRoleStatus);
    }
}
