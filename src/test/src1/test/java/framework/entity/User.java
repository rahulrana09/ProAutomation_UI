/**
 * Copyright (C) 2008-2013 Comviva Technologies
 */

package framework.entity;

import framework.dataEntity.GradeDB;
import framework.dataEntity.Partner;
import framework.dataEntity.UsrBalance;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MfsTestProperties;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Automation team on 5/5/2017.
 */
public class User {
    public boolean isExisting = false, isSvaApprover;
    private static final Logger LOGGER = LoggerFactory.getLogger(User.class);

    public String CategoryCode,
            LoginId,
            FirstName,
            LastName,
            FullName,
            ExternalCode,
            Email,
            Type,
            Password,
            MSISDN,
            CategoryName,
            DomainName,
            DomainCode,
            ParentCategoryName,
            ParentCategoryCode,
            OwnerCategoryName,
            OwnerCategoryCode,
            GradeName,
            GradeCode,
            WebGroupRole,
            ERROR,
            REMARK,
            IDProof,
            Merchant,
            RegistrationType,
            EnterpriseLimit,
            LiquidationFrequency,
            LiquidationDay,
            PhotoProof,
            AddressProof,
            MerchantType,
            ZoneName,
            LiquidationBankAccountNumber,
            LiquidationBankBranchName,
            AccountHolderName,
            FrequencyTradingName,
            IssueDate,
            accNumOtional = null,
            custIdOptional = null,
            DefaultCustId = null,
            NonDefaultCustId = null,
            DefaultAccNum = null,
            isCreated = "N",
            isApproved = "N",
            isPwdReset = "N",
            PaymentType = null,
            PaymentTypeID = null,
            ProviderName = null,
            ProviderId = null,
            dbPaymentTypeId = null,
            DateOfBirth = null,
            RelationshipOfficer = null,
            IDNum = null,
            Address = null,
            City = null,
            District = "District",
            ExpiryDate = null,
            ContactPerson = null,
            ImtIdNumber = null;

    public List<Partner> thirdParties = new ArrayList<>();
    public boolean isIMTEnabled = true;
    private boolean supportOnlineTxnReversal = false;
    public UsrBalance CurrentBalance = null;
    public BigDecimal preBalance = null, postBalance = null;
    public User ParentUser, OwnerUser;
    public KinUser KinUser;
    public Geography Geography = null;
    public List<String> linkedBanks = new ArrayList<>();

    private boolean isIdExpires = false;
    private String status = "N";
    private String mPin = ConfigInput.mPin;
    private String tPin = ConfigInput.tPin;
    private List<MobileGroupRole> mRoles = new ArrayList<>();

    public void setOnlineTxnReversalStatus(boolean status) {
        this.supportOnlineTxnReversal = status;
    }

    public boolean isSupportOnlineTxnReversal(){
        return this.supportOnlineTxnReversal;
    }

    public boolean isIdExpires() {
        return isIdExpires;
    }

    public void setIdExpires(boolean idExpires) {
        isIdExpires = idExpires;
    }

    public String getmPin() {
        return mPin;
    }

    public void setmPin(String mPin) {
        this.mPin = mPin;
    }

    public String gettPin() {
        return tPin;
    }

    public void settPin(String tPin) {
        this.tPin = tPin;
    }

    public void addLinkedBanks(List<String> bankNames) {
        for (String bankName : bankNames) {
            if(!this.linkedBanks.contains(bankName)){
                linkedBanks.add(bankName);
            }
        }
    }

    /**
     * Constructor initialization
     *
     * @param categoryCode
     */
    public User(String categoryCode) throws Exception {
        this.CategoryCode = categoryCode;
        this.GradeName = null;
        this.WebGroupRole = DataFactory.getWebGroupRoleName(this.CategoryCode);
        setGeneralInformation();
        setUserDetails();
    }

    public void addMobileRole(MobileGroupRole role) {
        mRoles.add(role);
    }

    public void addThirdParty(Partner thirdParty) {
        thirdParties.add(thirdParty);
    }

    /**
     * User with an assigned Parent User
     *
     * @param categoryCode
     * @param parent       - User Object
     */
    public User(String categoryCode, User parent) throws Exception {
        this.CategoryCode = categoryCode;
        this.ParentUser = parent;
        this.WebGroupRole = DataFactory.getWebGroupRoleName(this.CategoryCode);
        setGeneralInformation();
        setUserDetails();
    }

    /**
     * User with specific Grade and Specific Role Name-  mainly used in Base Set user creation
     *
     * @param categoryCode
     * @param gradeName
     */
    public User(String categoryCode, String gradeName, String roleName) throws Exception {
        this.CategoryCode = categoryCode;
        this.GradeName = gradeName;
        this.WebGroupRole = roleName;
        setGeneralInformation();
        setUserDetails();
    }

    public User(String categoryCode, String gradeName) throws Exception {
        this.CategoryCode = categoryCode;
        this.GradeName = gradeName;
        this.WebGroupRole = DataFactory.getWebGroupRoleName(this.CategoryCode);
        setGeneralInformation();
        setUserDetails();
    }

    /**
     * Data Representation of User
     * This method make sure that existing user is fetched before creating a new user, Use the Migrated User Data
     *
     * @param categoryCode
     * @param gradeName
     * @param index        - Number Of user
     * @NOTE IN PROGRESS
     */
    public User(String categoryCode, String gradeName, String roleName, int index) throws Exception {
        this(categoryCode, gradeName, roleName);
        int resultSize = 0;
        ResultSet result = MobiquityGUIQueries.fetchNonOptUsers(categoryCode, gradeName, index);

        if (result != null) {
            result.beforeFirst();
            result.last();
            resultSize = result.getRow();
        }

        if (index <= resultSize && result != null) {
            result.absolute(index); // switch to that Index
            this.MSISDN = result.getString("MSISDN");
            this.LoginId = result.getString("LOGIN_ID");
            this.FirstName = result.getString("USER_NAME");
            this.ExternalCode = result.getString("EXTERNAL_CODE");
            this.LastName = result.getString("LAST_NAME");
            this.Password = ConfigInput.defaultPass;
            this.isExisting = true;
            setUserDetails();
        }
    }

    /**
     * Masking Operator User Object as User, used for Non Financial Service Charges
     *
     * @param optUser
     */
    public User(OperatorUser optUser) {
        this.LoginId = optUser.LoginId;
        this.Password = optUser.Password;
        this.DomainCode = Constants.OPERATOR;
        this.CategoryCode = Constants.OPERATOR;
        this.GradeCode = Constants.OPERATOR;
        this.DomainName = "Operator";
        this.CategoryName = "Operator";
        this.GradeName = "Operator";
    }

    public User(Biller biller) {
        this.LoginId = biller.LoginId;
        this.Password = biller.Password;
        this.CategoryCode = biller.CategoryCode;
        this.CategoryName = biller.CategoryName;
        this.DomainName = biller.DomainName;
        this.DomainCode = biller.DomainCode;
        this.GradeName = biller.GradeName;
        this.GradeCode = biller.GradeCode;
    }

    /**
     * Masking Channel User object, User for Financial Service Charge
     *
     * @param usr
     */
    public User(User usr) {
        this.PaymentType = usr.PaymentType;
        this.DomainName = usr.DomainName;
        this.DomainCode = usr.DomainCode;
        this.CategoryName = usr.CategoryName;
        this.CategoryCode = usr.CategoryCode;
        this.GradeName = usr.GradeName;
        this.GradeCode = usr.GradeCode;
    }

    /**
     * User Object when reading it from Output File (ChannelUser.xlsx)
     *
     * @param catCode
     * @param domCode
     * @param loginId
     * @param msisdn
     * @param password
     * @param webGroupRole
     * @param isPwdReset
     */
    public User(String catCode, String catName, String domCode, String domName, String loginId,
                String firstName, String msisdn, String password, String gradeCode, String gradeName,
                String webGroupRole, String isPwdReset, String defaultAccNum, String defaultCustId,
                String externalCode, String idNum, String status, String zoneName, String areaName, String merType, String mpin, String tpin) {
        this.DomainCode = domCode;
        this.DomainName = domName;
        this.CategoryCode = catCode;
        this.CategoryName = catName;
        this.LoginId = loginId;
        this.FirstName = firstName;
        this.LastName = firstName;
        this.MSISDN = msisdn;
        this.Password = password;
        this.GradeCode = gradeCode;
        this.WebGroupRole = webGroupRole;
        this.isPwdReset = isPwdReset;
        this.DefaultAccNum = defaultAccNum;
        this.DefaultCustId = defaultCustId;
        this.GradeName = gradeName;
        this.ExternalCode = externalCode;
        this.IDNum = idNum;
        this.status = status;
        this.Geography = new Geography();
        this.Geography.setZone(zoneName);
        this.Geography.setArea(areaName);
        this.MerchantType = merType;
        this.mPin = mpin;
        this.tPin = tpin;
    }

    /**
     * Constructor for Barred User
     * Created for System Test Cases
     *
     * @param domainCode
     * @param categoryCode
     * @param firstName
     * @param lastName
     * @param msisdn
     * @param loginId
     * @param password
     * @param isCreated
     * @param status
     * @param externalCode
     */
    public User(String domainCode, String categoryCode, String firstName, String lastName, String msisdn, String loginId, String password, String isCreated, String status, String externalCode) {
        this.DomainCode = domainCode;
        this.CategoryCode = categoryCode;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.MSISDN = msisdn;
        this.LoginId = loginId;
        this.Password = password;
        this.isCreated = isCreated;
        this.status = status;
        this.ExternalCode = externalCode;
    }

    public void setGradeName(String gradeName) {
        GradeName = gradeName;
    }

    /*
     * Set General Information
     */
    private void setGeneralInformation() throws Exception {
        this.MSISDN = DataFactory.getAvailableMSISDN();
        this.ExternalCode = DataFactory.getRandomNumber(MSISDN, AppConfig.identificationNumLength);
        this.IDNum = this.MSISDN + "abcd";
        this.LoginId = this.CategoryCode + MSISDN;
        this.FirstName = LoginId;
        this.LastName = LoginId;
        this.FullName = FirstName + " " + LastName + "(" + LoginId + ")";
        this.Email = LoginId + "@xyz.com";
        this.Password = ConfigInput.defaultPass;
        this.IDProof = FilePath.uploadFile;
        this.AddressProof = FilePath.uploadFile;
        this.PhotoProof = FilePath.uploadFile;
        this.DateOfBirth = new DateAndTime().getDate(-9660);
        this.Address = "221 Baker Street";
        this.City = "London";
        this.MerchantType = "ONLINE";
        this.Geography = GlobalData.autGeography;
        this.EnterpriseLimit = Constants.MAX_TRANSACTION_ALLOWED;
        this.LiquidationFrequency = "Daily";
        this.LiquidationDay = null;
        this.LiquidationBankAccountNumber = null;
        this.LiquidationBankBranchName = null;
        this.AccountHolderName = null;
        this.FrequencyTradingName = null;
        this.ExpiryDate = new DateAndTime().getDate(+10);
        this.IssueDate = new DateAndTime().getDate(-10);
        this.ContactPerson = LoginId;
        this.ImtIdNumber = DataFactory.getRandomNumber(MSISDN, AppConfig.identificationNumLength);
        this.mPin = ConfigInput.mPin;
        this.tPin = ConfigInput.tPin;
    }


    public void setLiquidationFrequency(String liquidationFrequency) {
        this.LiquidationFrequency = liquidationFrequency;
    }

    public void setLiquidationDay(String day) {
        this.LiquidationDay = day;
    }

    public String getLiquidationBankAccountNumber() {
        return LiquidationBankAccountNumber;
    }

    public void setLiquidationBankAccountNumber(String liquidationBankAccountNumber) {
        LiquidationBankAccountNumber = liquidationBankAccountNumber;
    }

    public String getFrequencyTradingName() {
        return FrequencyTradingName;
    }

    public void setFrequencyTradingName(String frequencyTradingName) {
        FrequencyTradingName = frequencyTradingName;
    }

    public String getLiquidationBankBranchName() {
        return LiquidationBankBranchName;
    }

    public void setLiquidationBankBranchName(String liquidationBankBranchName) {
        LiquidationBankBranchName = liquidationBankBranchName;
    }

    public String getAccountHolderName() {
        return AccountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        AccountHolderName = accountHolderName;
    }

    public void setIsCreated() {
        this.isApproved = "Y";
        this.isCreated = "Y";
        this.isPwdReset = "Y";
        this.status = Constants.STATUS_ACTIVE;
    }

    public void setIsPasswordChanged() {
        this.isPwdReset = "Y";
        this.status = Constants.STATUS_ACTIVE;
    }

    public String getFullName() {
        return FirstName + " " + FirstName + "(" + LoginId + ")";
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public String getPartialName() {
        return FirstName + " " + FirstName;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        this.LoginId = loginId;
    }

    public void setParentUser(User parent) {
        this.ParentUser = parent;
    }

    public void setOwnerUser(User owner) {
        this.OwnerUser = owner;
    }

    public void setMSISDN(String msisdn) {
        this.MSISDN = msisdn;
    }

    public void setMSISDN() {
        this.MSISDN = DataFactory.getMSISDNcounter();
    }

    public void setExternalCode(String extCode) {
        this.ExternalCode = extCode;
    }

    public void setAddress(String address) {
        this.Address = address;
    }

    public void setContactPerson(String contactPerson) {
        this.ContactPerson = contactPerson;
    }

    public void setError(String error) {
        this.ERROR = error;
    }

    public void setRemark(String remark) {
        this.REMARK = remark;
    }

    public void setPhotoProof(String filePath) {
        this.PhotoProof = filePath;
    }

    public void setRelationshipOfficer(String Rofc) {
        this.RelationshipOfficer = Rofc;
    }

    public String getDefaultAccNum() {
        return this.DefaultAccNum;
    }

    public void setDefaultAccNum(String accNum) {
        this.DefaultAccNum = accNum;
    }

    public void setDefaultCustId(String id) {
        this.DefaultCustId = id;
    }

    public void setNonDefaultCustId(String id) {
        this.NonDefaultCustId = id;
    }

    public void setKinUser(KinUser kinUser) {
        this.KinUser = kinUser;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public void setFirstAndLastName(String firstName, String lastName) {
        this.FirstName = firstName;
        this.LastName = lastName;
    }

    public void setFirstName(String firstName) {
        this.FirstName = firstName;
    }

    public void setLastName(String lastName) {
        this.LastName = lastName;
    }

    public void LastName(String firstName, String lastName) {
        this.FirstName = firstName;
        this.LastName = lastName;
    }

    public void setDateOfBirth(String date) {
        this.DateOfBirth = date;
    }

    public void setExpiryDate(String date) {
        this.ExpiryDate = date;
    }

    public void setIssueDate(String date) {
        this.IssueDate = date;
    }

    public void setCurrentBalance(UsrBalance balance) {
        this.CurrentBalance = balance;
    }

    public void setAsSvaApprover() {
        this.isSvaApprover = true;
    }

    public void setMerchantType(String type) {
        this.MerchantType = type;
    }

    public void setZone(String zone) {
        this.Geography.ZoneName = zone;
    }

    public void setRegistrationType(String type) {
        this.RegistrationType = type;
    }

    public void setEnterpriseLimit(String limit) {
        this.EnterpriseLimit = limit;
    }

    public void setWebRoleName(String roleName) {
        this.WebGroupRole = roleName;
    }

    public void setGeography(String geo) {
        this.Geography.AreaName = geo;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isBarred() {
        if (this.status.equals(Constants.BAR_AS_SENDER) ||
                this.status.equals(Constants.BAR_AS_RECIEVER) ||
                this.status.equals(Constants.BAR_AS_BOTH)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isSuspended() {
        if (this.status.equals(Constants.STATUS_SUSPEND)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isBlackListed() {
        if (this.status.equals(Constants.STATUS_BLACKLIST)) {
            return true;
        } else {
            return false;
        }
    }

    public void setProviderName(String providerName) {
        this.ProviderName = providerName;

    }

    public void setPreBalance(BigDecimal balance) {
        this.preBalance = balance;
    }

    public void setPostBalance(BigDecimal balance) {
        this.postBalance = balance;
    }

    public void setUserDetails() {
        try {
            this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
            this.DomainName = DataFactory.getDomainName(this.CategoryCode);
            this.DomainCode = DataFactory.getDomainCode(this.CategoryCode);

            // Set the parent Details
            this.ParentCategoryCode = DataFactory.getParentCategoryCode(CategoryCode);
            if (this.ParentCategoryCode != null) {
                this.ParentCategoryName = DataFactory.getCategoryName(this.ParentCategoryCode);

                // Get Owner Details
                this.OwnerCategoryCode = DataFactory.getParentCategoryCode(this.ParentCategoryCode);
                if (this.OwnerCategoryCode != null) {
                    this.OwnerCategoryName = DataFactory.getCategoryName(this.OwnerCategoryCode);
                }
            }

            if (this.OwnerCategoryCode != null && this.OwnerCategoryCode.equals(this.CategoryCode)) {
                // user is at root level of it's hierarchy, set parent, Owner as null
                this.ParentCategoryCode = null;
                this.OwnerCategoryCode = null;
            }

            /*
             * Set the grade details
             * If Grade Name is not mentioned then take the first grade from DB as Default
             */
            if (GradeName == null) {
                this.GradeCode = DataFactory.getGradesForCategory(CategoryCode).get(0).GradeCode;
                this.GradeName = DataFactory.getGradeName(this.GradeCode);
            } else {
                this.GradeCode = DataFactory.getGradeCode(this.GradeName);
            }

            isSvaApprover = false; // set false not a sva approver

            /*
             * If Parent user is not associated with object user and Parent Category Code is not Null
             *      Then get the Parent User from ChannelUsers.xlsx and assign as Parent
             *
             * If Owner user Category is not Null
             *      Then get the Owner User from ChannelUsers.xlsx and assign as Owner
             *
             * If user has two Level Hierarchy and Owner User is Defined
             *      Then, Owner user will be assigned as parent User
             */
            if (this.ParentUser == null && this.ParentCategoryCode != null) {
                User parent = DataFactory.getChannelUserWithCategory(this.ParentCategoryCode);
                this.setParentUser(parent);
            }

            if (this.OwnerCategoryCode != null) {
                User owner = DataFactory.getChannelUserWithCategory(this.OwnerCategoryCode);
                this.setOwnerUser(owner);
            }

            if (this.CategoryCode.equals(Constants.SUBSCRIBER)) {
                this.RegistrationType = Constants.REGTYPE_NO_KYC;
            } else {
                this.RegistrationType = Constants.REGTYPE_FULL_KYC;
            }

            // Add Banks from ConfigInput
            this.linkedBanks = DataFactory.getAllBankNamesForBaseSetUserCreation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method returns the role Name
     * if the user object is already assigned with a role with specific wallet and provider, it return the same
     * if no wallet is associated with User object, this method return the role name for wallet and provider combination, available in Appdata.
     * @param walletName
     * @param providerName
     * @return
     */
    public MobileGroupRole hasPayInstTypeLinked(String walletName, String providerName) {
        for (MobileGroupRole role : this.mRoles) {
            if (role.MobilePayInstName.equals(walletName) && role.MobileProvider.equals(providerName)) {
                return role;
            }
        }
        // if no roles are assigned then get default role available in App Data
        MobileGroupRole role = DataFactory.getMobileGroupRole(this.DomainName,
                this.CategoryName,
                this.GradeName,
                providerName,
                walletName);

        if(role == null)
            Assert.fail("Please check the AppData, MobileRoles.xlsx, failed to fetch Appropriate Mobile Role");

        return role;
    }

    public String getUserReimbType() {
        String reimbType = null;
        switch (this.CategoryCode) {
            case "SUBS":
                reimbType = Constants.USR_TYPE_SUBS;
                break;
            case "MER":
                reimbType = Constants.MERCHANT_REIMB;
                break;
            default:
                reimbType = Constants.CHANNEL_REIMB;
                break;
        }
        return reimbType;
    }

    public String getUserRole() {
        String userRole = null;
        switch (this.CategoryCode) {
            case "SUBS":
                userRole = Constants.CUSTOMER_REIMB;
                break;
            case "MER":
                userRole = Constants.MERCHANT_REIMB;
                break;
            default:
                userRole = Constants.CHANNEL_REIMB;
                break;
        }
        return userRole;
    }

    /**
     * writeDataToExcel
     * <p>
     * Write the User related data to Output excel file
     */
    public void writeDataToExcel(boolean... isTempUser) throws Exception {

        if (!this.isPwdReset.equalsIgnoreCase(Constants.STATUS_ACTIVE)) {
            LOGGER.info("Not writing the user, as it's not created completely " + this.LoginId);
            return;
        }

        String filePath;
        if (isTempUser.length > 0 && isTempUser[0]) {
            filePath = FilePath.fileTempUsers;
        } else {
            filePath = FilePath.fileChannelUsers;
        }
        int lastRow = ExcelUtil.getExcelLastRow(filePath);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 0, this.CategoryCode);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 1, this.DomainCode);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 2, this.FirstName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 3, this.LoginId);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 5, this.MSISDN);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 6, this.Password);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 7, this.GradeCode);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 8, this.WebGroupRole);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 9, this.isCreated);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 10, this.isApproved);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 11, this.isPwdReset);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 12, this.DefaultAccNum);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 13, this.DefaultCustId);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 14, this.ERROR);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 15, this.REMARK);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 16, this.CategoryName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 17, this.DomainName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 18, this.GradeName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 19, this.ExternalCode);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 20, this.IDNum);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 21, this.status);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 22, this.Geography.ZoneName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 23, this.Geography.AreaName);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 24, this.MerchantType);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 25, this.mPin);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 26, this.tPin);
        ExcelUtil.WriteDataToExcel(filePath, lastRow, 27, this.linkedBanks.toString());
    }
}
