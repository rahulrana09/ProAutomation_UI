package framework.entity;

import framework.util.common.DataFactory;

/**
 * Created by prashant.kumar on 8/8/2017.
 */
public class Geography {
    public String AreaName, AreaCode, AreaDescription, AreaShortName, ZoneName, ZoneCode, ZoneDescription, ZoneShortName;


    /**
     * Constructor
     */
    public Geography() {
        AreaName = "AUTAREA" + DataFactory.getRandomNumber(3);
        AreaCode = DataFactory.getRandomNumber(5) + "A";
        AreaShortName = "ATAN" + DataFactory.getRandomNumber(3);
        AreaDescription = AreaName;

        ZoneName = "AUTZONE" + DataFactory.getRandomNumber(3);
        ZoneShortName = "ATZN" + DataFactory.getRandomNumber(3);
        ZoneCode = DataFactory.getRandomNumber(5) + "Z";
        ZoneDescription = ZoneName;
    }

    /**
     * @param areaName
     * @param areaCode
     * @param areaShortName
     * @param areaDescription
     * @param zoneName
     * @param zoneShortName
     * @param zoneCode
     * @param zoneDescription
     */
    public Geography(String areaName, String areaCode, String areaShortName, String areaDescription,
                     String zoneName, String zoneShortName, String zoneCode, String zoneDescription) {

        this.AreaName = areaName;
        this.AreaCode = areaCode;
        this.AreaShortName = areaShortName;
        this.AreaDescription = areaDescription;

        this.ZoneName = zoneName;
        this.ZoneCode = zoneCode;
        this.ZoneShortName = zoneShortName;
        this.ZoneDescription = zoneDescription;

    }


    public String getAreaName() {
        return AreaName;
    }

    public void setAreaName(String areaName) {
        this.AreaName = areaName;
    }

    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        this.AreaCode = areaCode;
    }

    public String getAreaDescription() {
        return AreaDescription;
    }

    public void setAreaDescription(String areaDescription) {
        this.AreaDescription = areaDescription;
    }

    public String getAreaShortName() {
        return AreaShortName;
    }

    public void setAreaShortName(String areaShortName) {
        this.AreaShortName = areaShortName;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String zoneName) {
        ZoneName = zoneName;
    }

    public String getZoneCode() {
        return ZoneCode;
    }

    public void setZoneCode(String zoneCode) {
        this.ZoneCode = zoneCode;
    }

    public String getZoneDescription() {
        return ZoneDescription;
    }

    public void setZoneDescription(String zoneDescription) {
        this.ZoneDescription = zoneDescription;
    }

    public String getZoneShortName() {
        return this.ZoneShortName;
    }

    public void setZoneShortName(String zoneShortName) {
        this.ZoneShortName = zoneShortName;
    }

    public void setZone(String zoneName) {
        // todo set the rest of details
        this.ZoneName = zoneName;
    }

    public void setArea(String areaName) {
        // todo
        this.AreaName = areaName;
    }


}
