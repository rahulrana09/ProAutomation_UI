package framework.entity;

import framework.util.common.DataFactory;

public class MFSDomain {
    public String domainName, domainCode;
    public int numCategory;
    private boolean isCreated;

    public MFSDomain(int numOfCategory) {
        this.domainName = "DMN" + DataFactory.getRandomNumberAsString(3);
        this.domainCode = "DMC" + DataFactory.getRandomNumberAsString(3);
        this.numCategory = numOfCategory;
        this.isCreated = false;
    }

    public void setIsCreated() {
        this.isCreated = true;
    }

    public boolean getIsCreated() {
        return this.isCreated;
    }
}
