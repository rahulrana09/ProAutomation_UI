package framework.entity;

import framework.dataEntity.GradeDB;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created
 */
public class Subscriber {
    public boolean isExisting = false;

    public String CategoryCode, LoginId, FirstName, LastName, FullName, ExternalCode, Email,
            Password, MSISDN, CategoryName, DomainName, DomainCode, ParentCategoryName, ParentCategoryCode,
            OwnerCategoryName, OwnerCategoryCode, GradeName, GradeCode, WebGroupRole, ERROR, REMARK, IDProof, PhotoProof, AddressProof;

    public String DefaultCustId = null;
    public String DefaultAccNum = null;
    public String isCreated = "N";
    public String isApproved = "N";
    public String isPwdReset = "N";
    public String PaymentType = null;
    public String PaymentTypeID = null;
    public String ProviderName = null;
    public String ProviderId = null;
    public String dbPaymentTypeId = null;

    public User ParentUser, OwnerUser;

    /**
     * Constructor initialization
     *
     * @param categoryCode
     */
    public Subscriber(String categoryCode) throws Exception {
        this.CategoryCode = categoryCode;
        this.GradeName = null;
        this.WebGroupRole = DataFactory.getWebGroupRoleName(this.CategoryCode);
        setGeneralInformation();
        setUserDetails();
    }

    /**
     * User with an assigned Parent User
     *
     * @param categoryCode
     * @param parent       - User Object
     */
    public Subscriber(String categoryCode, User parent) throws Exception {
        this.CategoryCode = categoryCode;
        this.ParentUser = parent;
        this.WebGroupRole = DataFactory.getWebGroupRoleName(this.CategoryCode);
        setGeneralInformation();
        setUserDetails();
    }

    /**
     * User with specific Grade and Specific Role Name-  mainly used in Base Set user creation
     *
     * @param categoryCode
     * @param gradeName
     */
    public Subscriber(String categoryCode, String gradeName, String roleName) throws Exception {
        this.CategoryCode = categoryCode;
        this.GradeName = gradeName;
        this.WebGroupRole = roleName;
        setGeneralInformation();
        setUserDetails();
    }

    /**
     * Data Representation of User
     * This method make sure that existing user is fetched before creating a new user, Use the Migrated User Data
     *
     * @param categoryCode
     * @param gradeName
     * @param index        - Number Of user
     * @NOTE IN PROGRESS
     */
    public Subscriber(String categoryCode, String gradeName, String roleName, int index) {
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();
        this.CategoryCode = categoryCode;
        this.GradeName = gradeName;
        this.WebGroupRole = roleName;
        ResultSet result = dbHandle.fetchNonOptUsers(categoryCode, gradeName, index);
    }

    /**
     * Masking Operator User Object as User, used for Non Financial Service Charges
     *
     * @param optUser TODO - this need to be Completed
     */
    public Subscriber(OperatorUser optUser) {
        this.LoginId = optUser.LoginId;
        this.Password = optUser.Password;
        this.DomainCode = "OPT";
        this.CategoryCode = "OPT";
        this.GradeCode = "OPT";
        this.DomainName = "Operator";
        this.CategoryName = "Operator";
        this.GradeName = "Operator ";
    }

    /**
     * Masking Channel User object, User for Financial Service Charge
     *
     * @param usr
     */
    public Subscriber(User usr) {
        this.PaymentType = usr.PaymentType;
        this.DomainName = usr.DomainName;
        this.DomainCode = usr.DomainCode;
        this.CategoryName = usr.CategoryName;
        this.CategoryCode = usr.CategoryCode;
        this.GradeName = usr.GradeName;
        this.GradeCode = usr.GradeCode;
    }

    /**
     * User Object when reading it from Output File (ChannelUser.xlsx)
     *
     * @param catCode
     * @param domCode
     * @param loginId
     * @param msisdn
     * @param password
     * @param webGroupRole
     * @param isPwdReset
     */
    public Subscriber(String catCode, String catName, String domCode, String domName, String loginId,
                      String firstName, String msisdn, String password, String gradeCode, String gradeName,
                      String webGroupRole, String isPwdReset, String defaultAccNum, String defaultCustId) {
        this.DomainCode = domCode;
        this.DomainName = domName;
        this.CategoryCode = catCode;
        this.CategoryName = catName;
        this.LoginId = loginId;
        this.FirstName = firstName;
        this.LastName = firstName;
        this.MSISDN = msisdn;
        this.Password = password;
        this.GradeCode = gradeCode;
        this.WebGroupRole = webGroupRole;
        this.isPwdReset = isPwdReset;
        this.DefaultAccNum = defaultAccNum;
        this.DefaultCustId = defaultCustId;
        this.GradeName = gradeName;
    }

    /*
     * Set General Information
     */
    private void setGeneralInformation() {
        this.MSISDN = DataFactory.getAvailableMSISDN();
        this.ExternalCode = this.MSISDN + "1357";
        this.LoginId = this.CategoryCode + MSISDN;
        this.FirstName = LoginId;
        this.LastName = LoginId;
        this.FullName = FirstName + " " + LastName + "(" + LoginId + ")";
        this.Email = LoginId + "@xyz.com";
        this.Password = ConfigInput.defaultPass;
        this.IDProof = FilePath.uploadFile;
        this.AddressProof = FilePath.uploadFile;
        this.PhotoProof = FilePath.uploadFile;
    }

    public void setIsCreated() {
        this.isApproved = "Y";
        this.isCreated = "Y";
        this.isPwdReset = "Y";
    }

    public String getFullName() {
        return FirstName + " " + FirstName + "(" + LoginId + ")";
    }

    public String getPartialName() {
        return FirstName + " " + FirstName;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setParentUser(User parent) {
        this.ParentUser = parent;
    }

    public void setOwnerUser(User owner) {
        this.OwnerUser = owner;
    }

    public void setMSISDN(String msisdn) {
        this.MSISDN = msisdn;
    }

    public void setMSISDN() {
        this.MSISDN = DataFactory.getMSISDNcounter();
    }

    public void setExternalCode(String extCode) {
        this.ExternalCode = extCode;
    }

    public void setExternalCode() {
        this.ExternalCode = DataFactory.getMSISDNcounter() + "abc";
    }

    public void setError(String error) {
        this.ERROR = error;
    }

    public void setRemark(String remark) {
        this.REMARK = remark;
    }

    public void setPasswordIsReset() {
        this.isPwdReset = "Y";
    }

    public void setIdProof(String filePath) {
        this.IDProof = filePath;
    }

    public void setAddressProof(String filePath) {
        this.AddressProof = filePath;
    }

    public void setPhotoProof(String filePath) {
        this.PhotoProof = filePath;
    }

    public void setDefaultCustId(String id) {
        this.DefaultCustId = id;
    }

    public void setDefaultAccNum(String accNum) {
        this.DefaultAccNum = accNum;
    }

    public void setFirstAndLastName(String firstName, String lastName) {
        this.FirstName = firstName;
        this.LastName = lastName;
    }

    /**
     * setUserDetails
     */
    public void setUserDetails() {
        try {
            this.CategoryName = DataFactory.getCategoryName(this.CategoryCode);
            this.DomainName = DataFactory.getDomainName(this.CategoryCode);
            this.DomainCode = DataFactory.getDomainCode(this.CategoryCode);

            // Set the parent Details
            this.ParentCategoryCode = DataFactory.getParentCategoryCode(CategoryCode);
            if (this.ParentCategoryCode != null) {
                this.ParentCategoryName = DataFactory.getCategoryName(this.ParentCategoryCode);

                // Get Owner Details
                this.OwnerCategoryCode = DataFactory.getParentCategoryCode(this.ParentCategoryCode);
                if (this.OwnerCategoryCode != null) {
                    this.OwnerCategoryName = DataFactory.getCategoryName(this.OwnerCategoryCode);
                }
            }

            if (this.OwnerCategoryCode != null && this.OwnerCategoryCode.equals(this.CategoryCode)) {
                // user is at root level of it's hierarchy, set parent, Owner as null
                this.ParentCategoryCode = null;
                this.OwnerCategoryCode = null;
            }

			/*
             * Set the grade details
			 * If Grade Name is not mentioned then take the first grade from DB as Default
			 */
            List<GradeDB> grades = new ArrayList<>();
            if (GradeName == null) {
                grades = DataFactory.getGradesForCategory(CategoryCode);
                GradeName = grades.get(0).GradeName;
            }
            GradeCode = DataFactory.getGradeCode(GradeName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * writeDataToExcel
     * <p>
     * Write the User related data to Output excel file
     */
    public void writeDataToExcel() throws IOException {
        int lastRow = ExcelUtil.getExcelLastRow(FilePath.fileChannelUsers);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 0, this.CategoryCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 1, this.DomainCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 2, this.FirstName);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 3, this.LoginId);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 5, this.MSISDN);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 6, this.Password);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 7, this.GradeCode);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 8, this.WebGroupRole);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 9, this.isCreated);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 10, this.isApproved);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 11, this.isPwdReset);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 12, this.DefaultAccNum);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 13, this.DefaultCustId);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 14, this.ERROR);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 15, this.REMARK);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 16, this.CategoryName);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 17, this.DomainName);
        ExcelUtil.WriteDataToExcel(FilePath.fileChannelUsers, lastRow, 18, this.GradeName);
    }
}
