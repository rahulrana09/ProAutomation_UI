package framework.entity;

import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalVars.AppConfig;

/**
 * Created by ravindra.dumpa on 11/7/2017.
 */
public class KinUser {
    public String FirstName, MiddleName, LastName,
            RelationShip, Nationality, DateOfBirth,
            ContactNum, KinNumber, Age, IdNo,
            NationalityAPIVal, AgeAPIVal;

    public KinUser() throws Exception {
        this.FirstName = "Kin" + DataFactory.getRandomNumber(4);
        this.MiddleName = "MidName";
        this.LastName = "LastName";
        this.RelationShip = "Father";
        this.Nationality = "India";
        this.NationalityAPIVal = "IN";
        this.DateOfBirth = new DateAndTime().getDate(-19660);
        this.Age = "26/10/1980";
        this.AgeAPIVal = "26101980";
        this.ContactNum = DataFactory.getAvailableMSISDN();
        this.IdNo = DataFactory.getRandomNumber(this.ContactNum, AppConfig.identificationNumLength);
        this.KinNumber = DataFactory.getRandomNumberAsString(9) + DataFactory.getRandomNumberAsString(5);
    }

}
