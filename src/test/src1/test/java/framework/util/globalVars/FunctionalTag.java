package framework.util.globalVars;

/**
 * Created by rahul.rana on 7/23/2017.
 */
public class FunctionalTag {
    public static final String SAVING_CLUB = "SAVING_CLUB";
    public static final String ADD_BANK_TRUST_NONTRUST = "ADD_BANK_TRUST_NONTRUST";
    public static final String SUBS_REG_MOD = "SUBSCRIBER_REGISTRATION_MODIFICATION";
    public static final String SUBS_REG = "SUBSCRIBER_REGISTRATION";
    public static final String SUBS_DEL_ACC_CLOSURE = "SUBSCRIBER_DELETE_ACCOUNT_CLOSURE";
    public static final String SUBS_DEL_ACC_CLOSURE_AGENT = "SUBSCRIBER_DELETE_ACCOUNT_CLOSURE_AGENT";
    public static final String STANDING_INS = "STANDING_INSTRUCTIONS";


    // Suite Tag
    public static final String P1 = "P1_TESTs";
    public static final String SMOKE = "SMOKE_TESTs";
    public static final String MONEY_SMOKE = "MONEY_SMOKE";
    public static final String UAP = "UAP_TESTs";
    public static final String JIRA = "JIRA_TASK_TESTs";
    public static final String REGRESSION = "REGRESSION_TESTs";
    public static final String NEGATIVE_TEST = "NEGATIVE_TESTs";
    public static final String SYSTEM_TEST = "SYSTEM_TEST";
    public static final String OLD_SYSTEM_TEST = "OLD_SYSTEM_TEST";
    public static final String ECONET_UAT_5_0 = "Econet_5.0_UAT_Cases";
    public static final String ECONET_SIT_5_0 = "Econet_5.0_SIT_Cases";
    public static final String DESCOPED_5dot0 = "DESCOPE_5_0";
    public static final String UNSTABLE = "UNSTABLE";


    //For PVG Added
    public static final String PVG_SMOKE = "PVG_SMOKE_TEST_CASE";
    public static final String PVG_UAP = "PVG_UAP_TEST_CASE";
    public static final String PVG_SYSTEM = "PVG_SYSTEM_TEST_CASE";
    public static final String SECURITY_TEST = "SECURITY_TEST_CASE";
    public static final String PVG_P1 = "PVG_P1_TESTs";
    public static final String ESCAPEDEFECT = "ESCAPE_DEFECT_CASES";
    public static final String ECONET_5_0 = "ECONET_5_0_TEST_CASE";
    public static final String ECONET_SMOKE_CASE_5_0 = "ECONET_SMOKE_CASE_5.0";
    public static final String CRITICAL_CASES_TAG = "CRITICAL_CASES_TAG";
    public static final String WEB_MENU_MANAGER = "WEB_MENU_MANAGER_CASES";

    //Combining Multiple Tags
    public static final String PVG_SMOKE_AND_UAP = "PVG_SMOKE_TEST_CASE";
    public static final String PVG_SMOKE_UAP_SYSTEM = "PVG_SMOKE_UAP_AND_SYSTEM";


    // Functionality Tag

    public static final String ACCESS_MANAGEMENT = "Access_Management";
    public static final String AUTHENTICATION_MANAGEMENT = "Authentication_Management";
    public static final String ACCOUNT_MANAGEMENT = "Account Management";
    public static final String PIN_MANAGEMENT = "PIN Management";
    public static final String SVA = "SVA";
    public static final String CHURN = "Churn User";
    public static final String AUTO_DEBIT = "AUTO_DEBIT";
    public static final String MULTICURRENCYRAND = "RAND_CURRENCY";
    public static final String LIMIT_RESET = "Limit Reset";


    public static final String BANK_MASTER = "Suit_Bank_Master";
    public static final String BANK_CASH_OUT = "Suit_Bank_CASH_OUT";
    public static final String MFS_PROVIDER_BANK = "MFS_Provider_Bank_Type_Master";
    public static final String MFS_PROVIDER_WALLET = "MFS_Provider_Wallet_Type_Master";

    public static final String BARRING_MANAGEMENT = "Barring Management";
    public static final String IMT = "International Money Transfer";

    public static final String BILL_PAYMENT = "Bill_Payment";

    public static final String BILLER_REGISTRATION = "BillerRegistration";
    public static final String BILLER_MANAGEMENT = "Biller Management";
    public static final String BILLER_CATEGORY_MANAGEMENT = "BillerCategoryManagement";
    public static final String BILLER_VALIDATION_MANAGEMENT = "Biller Validation Management";
    public static final String BILL_NOTIFICATION_MANAGEMENT = "Bill Notification Management";
    public static final String BILL_NOTIFICATION_ASSOCIATION = "Bill Notification Association";
    public static final String SINGLE_MSISDN_MUL_USER = "Single MSISDN Multiple User Type";

    public static final String BLACKLIST_P2P = "Blacklist P2P";
    public static final String CUSTOMER_BLACK_LISTING = "Customer Black Listing";

    public static final String BULK_PAYOUT_TOOL = "Bulk Payout Tool";
    public static final String BULK_SUBSCRIBER_REGISTRATION = "Bulk Subscriber Registration";
    public static final String SUBS_REL_OFF_CAPTURE = "RelationShip officer";
    public static final String BULK_CHANNEL_USER_REGISTRATION_AND_MODIFICATION = "Bulk Channel User Registration and Modification";
    public static final String BULK_USER_DELETION = "Bulk User Deletion";

    public static final String CCE = "CCE";
    public static final String C2C = "C2C";
    public static final String O2C = "O2C";
    public static final String SAV = "SVA to Own Bank Account";
    public static final String OPERATER_USER = "Operator User";
    public static final String CASHIN = "CASH_IN";
    public static final String INVERSEC2C = "INVERSE_C2C";
    public static final String CASHINOTHERS = "CASH_IN_OTHERS";
    public static final String CASHOUT = "CASH_OUT";
    public static final String COMPLETECASHOUT = "COMPLETE_CASH_OUT";
    public static final String COMMISSION_DISBURSMENT = "Commission Disbursement Procedure";
    public static final String SINGLE_NATIONAL_ID_CONFIRGURATION = "Configuration for number of MSISDN for a Single National ID";

    public static final String EMAIL_NOTIFICATION = "E-mail_Notification_Configuration";

    public static final String ENQUIRIES = "Enquiries";
    public static final String CHANNEL_ENQUIRIES = "Channel Enquiry";


    public static final String ENTERPRISE_MANAGEMENT = "Enterprise Management";


    public static final String GLOBAL_SEARCH = "Global Search";
    public static final String KPI = "KPI";
    public static final String LMS = "LMS";
    public static final String TCP = "TCP";
    public static final String LOGIN = "Login Password Management";
    public static final String MASTER = "Master";
    public static final String NON_FINANCIAL_SERVICE_CHARGE = "Non-financial Service Charge";


    public static final String WALLET_PREFERENCES = "Wallet Preferences";
    public static final String SYSTEM_PREFERENCES = "System Preferenece";
    public static final String SMS_CONFIGURATION = "SMS Configuration";
    public static final String C = "";

    public static final String HIERARCHY_BRANCH_MOVEMENT = "Hierarchy Branch Movement";

    public static final String SERVICE_CHARGE_MANAGEMENT = "Service_Charge_Management";
    public static final String INVENTORY_MANAGEMENT = "Inventory Management for Merchants";
    public static final String SUBSCRIBER_MANAGEMENT = "Subscriber_Management";
    public static final String SUBSCRIBER_INFORMATION = "Subscriber_Information";
    public static final String SECRET_QUESTION_ANSWER_MANAGEMENT = "Secret_Question/Answer_Management_for_Self_PIN_Rest";
    public static final String REVERSE_LIMIT = "ReverseTheLimitWheneverTransactionIsReversed";
    public static final String BANK_SERVICES = "Banking Services for Agents and Merchants - Bank to Wallet, Wallet to Bank, Bank mini statement and bank balance enquiry";
    public static final String OPT_USER_MANAGEMENT = "Operator_Management";
    public static final String USER_MANAGEMENT = "User_Management";

    public static final String GROUP_ROLE_MANAGEMENT = "Group_Role_Management";
    public static final String GRADE_MANAGEMENT = "Grade_Management";
    public static final String TRANSACTIONS = "Transaction_Management";
    public static final String TRANSACTIONS_CORRECTION = "Suite_Transaction_Correction";
    public static final String TRANSFER_RULE = "Transfer_Rules";
    public static final String RECHARGE_SYSTEM_MANAGEMENT = "Recharge System Management";

    public static final String CHANNEL_USER_MANAGEMENT = "Channel_User_Management";
    public static final String CHANNEL_USER_AS_SUBS = "Single_MSISDN_Multiple_UserType";
    public static final String STOCK_MANAGEMENT = "Stock_Management";
    public static final String STOCK_TRANSFER = "STOCK_TRANSFER";
    public static final String STOCK_EA_TRANSFER = "STOCK_EA_TRANSFER";
    public static final String STOCK_IMT = "STOCK_IMT";
    public static final String STOCK_REIMBURSEMENT = "STOCK_REIMBURSEMENT";
    public static final String STOCK_WITHDRAWAL = "STOCK_WITHDRAWAL";
    public static final String AUDIT_TRAIL = "Audit Trail";
    public static final String ADMIN_TRAIL = "Admin_Trail";
    public static final String GEOGRAPHY_MANAGEMENT = "Geography_Management";

    public static final String RECONCILIATION = "Reconciliation";
    public static final String CATEGORY_MANAGEMENT = "Category_Management";
    public static final String DOMAIN_MANAGEMENT = "Domain_Management";


    public static final String ZONE_MANAGEMENT = "Zone_Management";
    public static final String PAYROLL_ENHANCEMENTS = "Payroll enhancements";
    public static final String PSEUDO_USER_MANAGEMENT = "Pseudo_User_Management";
    public static final String BLACKLIST_MANAGEMENT = "Blacklist_Management";
    public static final String NEW_SUBSCRIBER_GRADE = "New_Subscriber_Grade_Farmer";
    public static final String NEXT_OF_KIN_INFORMATION = "Next_of_KIN_Information";
    public static final String FORCED_PASSWORD_CHANGE_PROCEDURE = "Forced_Password_Change_Procedure";
    public static final String FBC_BANKING_SERVICES = "FBC_Banking_Services";
    public static final String ECOBANK_BANKING_SERVICES = "ECOBANK_Banking_Services";
    public static final String PAYBAY_MERCHANT_GRADE = "PayBay Merchant Grade";
    public static final String MULTIPLE_BANK_TRUST_ACCOUNT = "Multiple Bank Trust Account";
    public static final String WALLET_TO_BANK = "Wallet to Bank";


    //for Core Added
    public static final String P1CORE = "P1_CORE_TEST_CASES";
    public static final String v5_0 = "v_5_0";
    public static final String DEPENDENCY_TAG = "Dependent_Cases";


    public static final String PRICING_ENGINE = "Pricing Engine";
    public static final String BULK_SERVICE_THRESHOLD = "BULK_SERVICE_THRESHOLD";
    public static final String BULK_SMS_MODULE = "Bulk SMS Module";
    public static final String SUBSCRIBER_Bulk_Association = "Subscriber Bulk Association with bank";
    public static final String NMB = "NMB";
    public static final String UNCLAIMED_MONEY = "Unclaimed Money Processing";
    public static final String PROMOTION = "Rewards and Promotion Management";
    public static final String BULK_SUB_REG_MOD = "Bulk Subscriber Registration and Modification";
}
