package framework.util.globalVars;

import com.google.common.collect.Multimap;
import framework.dataEntity.*;
import framework.entity.*;

import java.util.*;

/**
 * Created by rahul.rana on 5/7/2017.
 * Use variable that shall be loaded once during the entire execution
 */
public class GlobalData {
    public static HashMap<String, DomainCategoryDB> domainCategoryMap;
    public static HashMap<String, List<GradeDB>> gradeCategoryDBMap;

    public static List<SuperAdmin> superAdmins;
    public static List<WebGroupRole> rnrDetails;
    public static List<CurrencyProvider> currencyProvider;
    public static List<Bank> banks;
    public static List<Wallet> wallet;
    public static List<ServiceList> serviceList;
    public static List<MobileGroupRole> mobileGroupRole;
    public static List<InstrumentTCP> instrumentTCPs;
    public static TransferRuleInput transferRuleInput;
    public static List<ConstantsInput> constantsInput;
    public static List<String> msisdnListDB;
    public static List<String> availableMsisdnList;
    public static List<String> regTypeListDB;
    public static Geography autGeography;
    public static CurrencyProvider defaultProvider;
    public static Wallet defaultWallet, normalWallet, commissionWallet, savingClubWallet;
    public static String defaultBankName;
    public static List<String> gradeList;
    public static List<Partner> partnerMap;
    public static List<String> exeCategoryGroup1 = new ArrayList<>();
    public static List<String> exeCategoryGroup2 = new ArrayList<>();
    public static List<String> exeCategoryGroup3 = new ArrayList<>();
    public static List<String> exeCategoryGroup4 = new ArrayList<>();
    public static List<String> tempCategoryGroup = new ArrayList<>();
    public static Map<String, String> prefMap = new LinkedHashMap<>();

}
