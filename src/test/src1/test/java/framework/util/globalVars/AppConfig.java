package framework.util.globalVars;

import framework.util.dbManagement.MobiquityGUIQueries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by rahul.rana on 7/10/2017.
 */
public class AppConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfig.class);

    public static String defaultPin;

    public static boolean isImtSendMoneyEnabled, isRemittanceRequired, isRandomPasswordAllowed, isRandomPinAllowed,
            isCommissionWalletRequired, isSubsMakerCheckerAllowed, isTwoStepRegistrationForSubs,
            useDefaultPin, isTPinAuthentication, isBankIdRequired, isAddMakerReqClub, isBankMandatoryForSavingsClub,
            isIdentificationNumRequired, isPassportRequired, isBankBranchCSVUploadRequired, isAgeLimitIsRequired,
            isBankPoolAccountRequired, isAccountClosureTwoStep, isUserTypeAllowed, isOptUserMsisdnieEditable,
            isMultipleRechargeOperatorRequired, isCustConfirmRequired, isSubsLoginAllowed, isBankRequiredForBiller,
            isSecurityQuestionOnLoginEnabled, isChannelUserAllowedAsSubscriber, isConsumerPortalRequired, isLinkStockCreateWithO2C,
            isBankAccLinkViaMsisdn;


    public static BigDecimal currencyFactor;

    public static int custAgeLimit, bankIdLength, identificationNumLength, poolAccountLength,
            minPoolAccountLength, msisdnLength, maxWebPasswordLength, minUserLengthNotInPass,
            maxGradePerCategory, maxWalletAddPerDay, minExternalCodeLength, externalCodeLength,
            minMsisdnLength, maxMsisdnLength,maxLoginIDLength;

    public static BigDecimal minSVCDepositAmount;
    public static String savingClubIdPrefix, defaultMaxNumApprover, defaultMaxNumMember, defaultMinNumApprover,
            defaultMinNumMember, minWebPasswordLength, minLoginIDLength, regSubsThreshold, domesticRemmittancePayID,
            defaultUsrPwd, clientSite, clubBankId;
    public static String defaultCurrency;
    private static Map<String, String> mtxprefMap;

    public static void init() {
        System.out.println("*** Started AppConfig.init ***\n");
        // Execution Flags Fetched from DB
        MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
        mtxprefMap = dbHandler.dbGetMtxPreferences();

        /**
         * P R E F E R E N C E   F L A G S
         */
        if (!ConfigInput.isCoreRelease) {
            isImtSendMoneyEnabled = getDefaultValAsBoolean("IS_IMT_SEND_MONEY_ENABLED");
        } else {
            isImtSendMoneyEnabled = true;
        }

        isRandomPasswordAllowed = getDefaultValAsBoolean("IS_RANDOM_PASS_ALLOW");
        isRandomPinAllowed = getDefaultValAsBoolean("IS_RANDOM_PIN_ALLOW");
        isRemittanceRequired = getDefaultValAsBoolean("IS_REMITTANCE_WALLET_REQUIRED");
        isCommissionWalletRequired = getDefaultValAsBoolean("IS_COMMISSION_WALLET_REQUIRED");
        isSubsMakerCheckerAllowed = getDefaultValAsBoolean("ADD_SUBS_MAKER_CHECKER_ALLOWED");
        useDefaultPin = getDefaultValAsBoolean("USE_DEFAULT_PIN");
        isTPinAuthentication = getDefaultValAsBoolean("ENABLE_TPIN_AUTHENTICATION");
        isTwoStepRegistrationForSubs = getDefaultValAsBoolean("TWO_STEP_SUBS_REG");
        isAgeLimitIsRequired = getDefaultValAsBoolean("IS_AGE_LIMIT_REQUIRE");
        minWebPasswordLength = mtxprefMap.get("WEB_PWD_MIN_LENGTH");
        minLoginIDLength = mtxprefMap.get("MIN_LOGINID_LENGTH");
        maxLoginIDLength = Integer.parseInt(mtxprefMap.get("MAX_LOGINID_LENGTH"));
        identificationNumLength = Integer.parseInt(mtxprefMap.get("EXTERNAL_CODE_LENGTH"));
        msisdnLength = Integer.parseInt(mtxprefMap.get("MAX_MSISDN_LENGTH"));
        isOptUserMsisdnieEditable = (mtxprefMap.get("IS_CHANGE_MSISDN_ALLOWED").equalsIgnoreCase("Y")) ? true : false;
        isUserTypeAllowed = getDefaultValAsBoolean("IS_USERTYPE_ALLOWED");
        regSubsThreshold = mtxprefMap.get("REGISTERINGSUB_THRESHOLD");
        domesticRemmittancePayID = mtxprefMap.get("DOMESTIC_REMIT_WALLET_PAYID");
        isCustConfirmRequired = getDefaultValAsBoolean("IS_CUSTOMER_CONFIRM_REQUIRED");
        isSubsLoginAllowed = getDefaultValAsBoolean("IS_CONSUMER_PORTAL_REQUIRED");
        isSecurityQuestionOnLoginEnabled = getDefaultValAsBoolean("USER_INPUTTED_QUESTIONS_ENABLED");
        isConsumerPortalRequired = getDefaultValAsBoolean("IS_CONSUMER_PORTAL_REQUIRED");
        isBankAccLinkViaMsisdn = getDefaultValAsBoolean("BANK_ACC_LINKING_VIA_MSISDN");
        custAgeLimit = Integer.parseInt(mtxprefMap.get("CUST_AGE_LIMIT"));

        // 5.0 Specific
        isIdentificationNumRequired = getDefaultValAsBoolean("IS_ID_REQUIRED");
        isPassportRequired = getDefaultValAsBoolean("PASSPORT_ALLOWED");
        isBankPoolAccountRequired = getDefaultValAsBoolean("IS_POOL_ACC_REQ");
        isMultipleRechargeOperatorRequired = (mtxprefMap.get("MULTI_OPERATOR_SUPPORT").equalsIgnoreCase("N")) ? true : false;
        isChannelUserAllowedAsSubscriber = getDefaultValAsBoolean("CHANNEL_USER_AS_SUBS_ALLOWED");
        isLinkStockCreateWithO2C = (mtxprefMap.get("LINK_STOCK_CREATION_WITH_O2C").equalsIgnoreCase("Y")) ? true : false;

        if (ConfigInput.isCoreRelease) {
            isBankMandatoryForSavingsClub = getDefaultValAsBoolean("IS_BANK_MANDATORY_FOR_SAVING_CLUB");
            isAddMakerReqClub = getDefaultValAsBoolean("ADD_MAKER_REQ_CLUB");
            isBankRequiredForBiller = getDefaultValAsBoolean("IS_BANK_REQUIRED");
            isBankIdRequired = getDefaultValAsBoolean("IS_BANKID_REQ");
            isBankBranchCSVUploadRequired = getDefaultValAsBoolean("IS_FILE_UPLOAD_REQ");

            // Saving Club
            savingClubIdPrefix = mtxprefMap.get("CLUB_ID_START_WITH");
            defaultMaxNumApprover = mtxprefMap.get("DEFAULT_MAX_NO_APPROVER");
            defaultMaxNumMember = mtxprefMap.get("DEFAULT_MAX_NO_MEMBER");
            defaultMinNumApprover = mtxprefMap.get("DEFAULT_MIN_NO_APPROVER");
            defaultMinNumMember = mtxprefMap.get("DEFAULT_MIN_NO_MEMBER");
            minSVCDepositAmount = new BigDecimal(mtxprefMap.get("SVC_MIN_DEPOSIT_AMOUNT"));
            clubBankId = mtxprefMap.get("CLUB_BANK_ID");
            maxWalletAddPerDay = Integer.parseInt(mtxprefMap.get("MAX_WLT_ADD_PERDAY"));
        }

        poolAccountLength = Integer.parseInt(mtxprefMap.get("POOL_ACC_NO_LEN"));
        minPoolAccountLength = Integer.parseInt(mtxprefMap.get("MIN_ACCOUNT_NO_LENGTH"));

        defaultCurrency = new String(mtxprefMap.get("DEFAULT_CURRENCY"));

        maxWebPasswordLength = Integer.parseInt(mtxprefMap.get("WEB_PWD_MAX_LENGTH"));
        minUserLengthNotInPass = Integer.parseInt(mtxprefMap.get("MIN_USER_LENGTH_NOT_IN_PASSWORD"));
        minExternalCodeLength = Integer.parseInt(mtxprefMap.get("MIN_EXT_CODE_LENGTH"));
        externalCodeLength = Integer.parseInt(mtxprefMap.get("EXTERNAL_CODE_LENGTH"));
        minMsisdnLength = Integer.parseInt(mtxprefMap.get("MIN_MSISDN_LENGTH"));
        maxMsisdnLength = Integer.parseInt(mtxprefMap.get("MAX_MSISDN_LENGTH"));


        /**
         * P R E F E R E N C E   V A L U E S
         */
        currencyFactor = new BigDecimal(mtxprefMap.get("CURRENCY_FACTOR"));
        maxGradePerCategory = Integer.parseInt(mtxprefMap.get("MAX_GRADES_PER_CATEGORY"));

        clientSite = new String(mtxprefMap.get("CLIENT_SITE"));

        isAccountClosureTwoStep = getDefaultValAsBoolean("ACCOUNT_CLOSURE_TWO_STEP");

        //get Default Pin
        if (useDefaultPin) {
            defaultPin = mtxprefMap.get("DEFAULT_RESET_PIN");
        } else {
            defaultPin = null; // if this value is set to null then User PIN has to be fetched using message notification
        }

        // get the Bank Id length
        if (isBankIdRequired) {
            bankIdLength = Integer.parseInt(mtxprefMap.get("BANK_ID_LENGTH"));
        }

        // get the default user password
        if (!isRandomPasswordAllowed) {
            defaultUsrPwd = mtxprefMap.get("DEFAULT_RESET_PWD");
        }

    }


    private static boolean getDefaultValAsBoolean(String preferenceCode) {

        try {
            String preferenceValue = mtxprefMap.get(preferenceCode);
            if (preferenceCode == null) {
                LOGGER.info("Preference code '" + preferenceCode + "' is not available in DataBase");
                return false;
            }

            if (preferenceValue.equalsIgnoreCase("y") || preferenceValue.equalsIgnoreCase("true")) {
                return true;
            }

        } catch (NullPointerException e) {
            return false;
        }

        return false;
    }
}
