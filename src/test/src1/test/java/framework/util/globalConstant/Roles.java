package framework.util.globalConstant;

import org.apache.poi.ss.usermodel.*;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;


/**
 * Created by navin.pramanik on 12/24/2018.
 * This class has all the constants for User Group Role Codes
 * The purpose is to follow a common standard while Coding and to follow DRY concept
 */
public class Roles {

    public static final String CHECK_ALL = "CHECK_ALL";
    public static final String ADD_CHANNEL_USER = "PTY_ACU";
    public static final String ADD_CHANNEL_USER_APPROVAL = "PTY_CHAPP2";
    public static final String MODIFY_CHANNEL_USER = "PTY_MCU";
    public static final String MODIFY_CHANNEL_USER_APPROVAL = "PTY_MCHAPP";
    public static final String VIEW_CHANNEL_USER = "PTY_VCU";
    public static final String DELETE_CHANNEL_USER = "PTY_DCU";
    public static final String DELETE_CHANNEL_USER_APPROVAL = "PTY_DCHAPP";
    public static final String SUSPEND_CHANNEL_USER = "PTY_SCU";
    public static final String SUSPEND_CHANNEL_USER_APPROVAL = "PTY_SCHAPP";
    public static final String RESUME_CHANNEL_USER = "PTY_RCU";
    public static final String RESUME_CHANNEL_USER_APPROVAL = "PTY_RCHAPP";
    public static final String HIERARCHY_BRANCH_MOVEMENT = "HRY_MOV";
    public static final String BULK_BANK_DISASSOCIATION = "BLK_BNK_DISOC";
    public static final String ADD_OPERATOR_USER = "PTY_ASU";
    public static final String APPROVE_OPERATOR_USER = "PTY_ASUA";
    public static final String GLOBAL_SEARCH = "RP_GOS";
    public static final String DELETE_OPERATOR_USER_APPROVAL = "PTY_ASUD";
    public static final String MODIFY_OPERATOR_USER = "PTY_MSU";
    public static final String SUSPEND_RESUME_SUBSCRIBER = "SR_USR";
    public static final String BAR_USER = "PTY_BLKL";
    public static final String VIEW_BARRED_USER = "PTY_VBLK";
    public static final String VIEW_SELF_DETAILS = "PTY_VSELF";
    public static final String VIEW_OPERATOR_DETAILS = "PTY_VSU";
    public static final String MODIFY_OPERATOR_USER_APPROVAL = "PTY_MSUAP";
    public static final String MANAGE_INSTRUMENT_LEVEL_TCP = "TCP_INSTRMENT";
    public static final String INSTRUMENT_LEVEL_TCP_APPROVAL = "TCP_INSTRAPPR";
    public static final String OPERATOR_TO_CHANNEL_TRANSFER_ENQUIRY = "O2C_ENQ";
    public static final String SYSTEM_PREFERENCES = "PREF001";
    public static final String GROUP_ROLE_MANAGEMENT = "GRP_ROL";
    public static final String VIEW_GROUP_ROLES = "GRP_ROL_VIEW";
    public static final String WALLET_PREFERENCES = "CAT_PREF";
    public static final String SMS_CONFIGURATION = "SMS_CR";
    public static final String BULK_USER_REGISTRATION_AND_MODIFICATION = "BLK_CHUSR";
    public static final String BULK_USER_APPROVAL = "BULK_AP";
    public static final String OWNER_TO_CHANNEL_TRANSFER_INITIATE = "O2C_INIT";
    public static final String OWNER_TO_CHANNEL_TRANSFER_APPROVAL1 = "O2C_APP1";
    public static final String OWNER_TO_CHANNEL_TRANSFER_APPROVAL2 = "O2C_APP2";
    public static final String TRANSFER_RULES = "T_RULES";
    public static final String O2C_TRANSFER_RULES = "O2C_TRULES";
    public static final String TRANSFER_RULE_APPROVAL = "T_RULESAPP";
    public static final String STOCK_INITIATION = "STOCK_INIT";
    public static final String STOCK_APPROVAL_1 = "STOCK_APP1";
    public static final String STOCK_APPROVAL_2 = "STOCK_APP2";
    public static final String STOCK_LIMIT = "STOCK_LIMIT";
    public static final String STOCK_TRANSFER_TO_EA_INITIATION = "STR_INIT";
    public static final String STOCK_TRANSFER_TO_EA_APPROVAL_1 = "STOCKTR_APP1";
    public static final String STOCK_ENQUIRY = "STOCK_ENQ";
    public static final String STOCK_TRANSFER_TO_EA_APPROVAL_2 = "STOCKTR_APP2";
    public static final String STOCK_REIMBURSEMENT_INITIATION = "STOCK_REINIT";
    public static final String STOCK_TRANSFER_LIMIT_FOR_EA = "STOCKTR_LIMIT";
    public static final String STOCK_REIMBURSEMENT_APPROVAL = "STOCK_REMB";
    public static final String STOCK_WITHDRAWAL = "STK_WITHDRAW";
    public static final String STOCK_WITHDRAWAL_APPROVAL = "STK_WITHDRAW_APP";
    //
    public static final String STOCK_TRANSFER_EA_ENQUIRY = "STOCKTR_ENQ";
    public static final String STOCK_REIMBURSEMENT_STATUS = "STOCK_REST";
    public static final String IMT_INITIATION = "STK_IMT_INI";
    public static final String IMT_APPROVAL_1 = "STK_IMT_APPROVAL1";
    public static final String STOCK_LIQUIDATION_APPROVAL_1 = "STK_LIQ_APPROVAL1";
    public static final String IMT_APPROVAL_2 = "STK_IMT_APPROVAL2";
    public static final String STOCK_TRANSFER_TO_RA_INITIATION = "STK_LOYALTY_INI";
    public static final String STOCK_TRANSFER_TO_RA_APPROVAL_1 = "STK_LOYALTY_APPR1";
    public static final String STOCK_TRANSFER_TO_RA_APPROVAL_2 = "STK_LOYALTY_APPR2";
    public static final String STOCK_TRANSFER_LIMIT_FOR_RA = "STK_LOYALTY_LMT";
    public static final String LOYALTY_STOCK_WITHDRAWAL = "STK_LOY_WITHDRAW";
    public static final String ESCROW_TO_ESCROW_TRANSFER_INITIATION = "ESCROW_INIT";
    public static final String STOCK_INITIATION_INTEREST_WALLET = "STOCK_INTERESTINIT";
    public static final String ESCROW_TO_ESCROW_TRANSFER_APPROVAL = "ESCROW_APP";
    public static final String STOCK_INTEREST_WALLET_APPROVAL_1 = "STOCK_INTERESTAPP1";
    public static final String STOCK_INTEREST_WALLET_APPROVAL_2 = "STOCK_INTERESTAPP2";
    public static final String SUBSCRIBER_BANK_ASSOCIATION = "SUBS_BNK_ASSOC";
    public static final String BULK_SUBSCRIBER_UPLOAD = "SUBBUREG";
    public static final String DELETE_SUBSCRIBER = "SUBSDEL";
    public static final String BULK_BANK_ASSOCIATION = "SUB_BULK_BANK_ASSOC";
    public static final String NOTIFICATION_REGISTRATION = "NOTF_REG";
    public static final String ADD_CUSTOMER_BLACK_LIST = "BLK_ABL";
    public static final String PRICING_POLICY_APPROVAL = "APRV_PRICING_POLICY";
    public static final String ACCESS_TO_CHARAGE_CALCULATOR = "CALCULATE_CHARGES";
    public static final String ADD_EDIT_DELETE_PRICING_POLICY_INITIATION = "SBMT_PRICING_POLICY";
    public static final String PRICING_ENGINE = "SHULKA_ACCESS";
    public static final String ADD_SERVICE_CHARGE = "SVC_ADD";
    public static final String MODIFY_DELETE_SERVICE_CHARGE = "SVC_MOD";
    public static final String VIEW_SERVICE_CHARGE = "SVC_VIEW";
    public static final String SUSPEND_RESUME_SERVICE_CHARGE = "SVC_SUS";
    public static final String SERVICE_CHARGE_CALCULATOR = "SVC_CALC";
    public static final String SERVICE_CHARGE_APPROVAL = "SVC_APP";
    public static final String MODIFY_NEW_SUBSCRIBER_COMMISSION_RULE = "SVC_SUBCOMRULE";
    public static final String NEW_SUBSCRIBER_COMMISSION_RULE = "SVC_ADDSUBCOM";
    public static final String ADD_NON_FINANCIAL_SERVICE_CHARGE = "CHARGENON";
    public static final String MODIFY_DELETE_NON_FINANCIAL_SERVICE_CHARGE = "CHARGENON_MOD";
    public static final String SUSPEND_RESUME_NON_FINANCIAL_SERVICE_CHARGE = "CHARGENON_SUS";
    public static final String VIEW_NON_FINANCIAL_SERVICE_CHARGE = "CHARGENON_VIEW";
    public static final String NON_FINANCIAL_SERVICE_CHARGE_APPROVAL = "CHARGENON_APP";
    public static final String NON_FINANCIAL_SERVICE_CHARGE_CALCULATOR = "CHARGENON_CALC";
    public static final String NETWORK_STATUS = "MNT_STS";
    public static final String BILLER_CATEGORY_MANAGEMENT = "UTL_MCAT";
    public static final String SUBSCRIBER_BILLER_ASSOCIATION = "UTL_BILREG";
    public static final String BILLER_REGISTRATION = "UTL_CREG";
    public static final String BILLER_REGISTRATION_APPROVAL = "UTL_CAPP";
    public static final String ADD_BILLER_VALIDATIONS = "UTL_CAVAL";
    public static final String MODIFY_BILLER_VALIDATIONS = "UTL_CMVAL";
    public static final String DELETE_BILLER_VALIDATIONS = "UTL_CDVAL";
    public static final String BULK_BILLER_ASSOCIATION = "UTL_BLKBILASSOC";
    public static final String BILLER_MODIFICATION = "UTL_CMOD";
    public static final String BILLER_DELETION_APPROVAL = "UTL_CDAP";
    public static final String BILLER_ACTIVATION_FROM_SUSPENSION = "UTL_CACT";
    public static final String BILLER_SUSPENSION = "UTL_CSUS";
    public static final String BILLER_DELETION_INITIATION = "UTL_CDEL";
    public static final String BILLER_NOTIFICATION_ASSOCIATION = "NOTIF_CR";
    public static final String BILL_UPLOAD = "BILLUP";
    public static final String BILLER_SUSPENSION_APPROVAL = "UTL_CSUSAPR";
    public static final String BILLER_ACTIVATION_APPROVAL = "UTL_CACTAPR";
    public static final String ADD_BILLER_VALIDATION_APPROVAL = "UTL_CBVALA";
    public static final String DELETE_BILLER_VALIDATION_APPROVAL = "UTL_CBVALD";
    public static final String BILLER_MODFICIATION_APPROVAL = "UTL_CMODAP";
    public static final String MODIFY_BILLER_VALIDATION_APPROVAL = "UTL_CBVALU";
    public static final String DELETE_SUBSCRIBER_BILLER_ASSOCIATION = "UTL_DBLREG";
    public static final String NOTIFICATION_MANAGEMENT = "UTL_NOT";
    public static final String ADD_GEOGRAPHY = "VIEWGRPHDOMAIN";
    public static final String AUDIT_TRAIL = "AUDIT_TRAIL";
    public static final String ADMIN_TRAIL = "ADMIN_TRAIL";
    public static final String KPI = "KPIR";
    public static final String CHANNEL_USER_SUBSCRIBER_ENQUIRY = "ENQ_US";
    public static final String RESET_PIN = "SR_PIN";
    public static final String TRANSACTION_DETAILS = "TXN_DETAILS";
    public static final String CHANNEL_USER_SUBSCRIBER_LOYALTY_ENQUIRY = "ENQ_LUS";
    public static final String CUSTOMER_CARE_EXECUTIVE = "CCE_ENQUIRY";
    public static final String RECONCILIATION = "MN_REC";
    public static final String RESET_PASSWORD = "SR_PASS";
    public static final String OPERATOR_MANAGEMENT = "OPPMG";
    public static final String ADD_OPERATOR = "OPPADD";
    public static final String MODIFY_OPERATOR = "MODOP";
    public static final String DELETE_OPERATOR = "DELOP";
    public static final String ADD_DOMAIN = "DOM_ADD";
    public static final String TRANSACTION_CORRECTION_INITIATION = "TXN_CORRECTION";
    public static final String TRANSACTION_CORRECTION_APPROVAL = "TXN_CORRAPP";
    public static final String DEBIT_CREDIT_TRANSACTION_CORRECTION = "CR_DR_TXN_CORR";
    public static final String NEW_BULK_PAYOUT_INITIATE = "BULK_INITIATE";
    public static final String NEW_BULK_PAYOUT_APPROVE = "NEW_BULK_APPROVE";
    public static final String NEW_BULK_PAYOUT_DASHBOARD = "BULK_DASHBOARD";
    public static final String COMMISSION_DISBURSEMENT = "COMMDIS_INITIATE";
    public static final String COMMISSION_WITHDRAWAL = "COM_WITHDRW";
    public static final String ADD_CATEGORY_APPROVAL = "CAT_APPRL";
    public static final String EMAIL_NOTIFICATION_CONFIGURATION = "NTF_CONFIG";
    public static final String NON_FINANCIAL_CHARGING_CONFIGURATION = "NONFINCONF";
    public static final String MODIFY_CHANNEL_CATEGORY_APPROVAL = "APPR_CHANNEL_CAT";
    public static final String UNCLAIMED_MONEY_PROCESSING = "UNCLAIMED_MONEY";
    public static final String SVA_TO_BANK_TRANSFER = "SVATOBANK";
    public static final String VIEW_MODIFY_STOP_DELETE_SCHEDULED_TRANSFERS = "VMSDSHTR";
    public static final String RESUME_SCHEDULED_TRANSFERS = "RESUMESHTR";
    public static final String ADD_CLUB = "ADD_CLUB";
    public static final String APPROVE_CLUB_ADDITION = "APP_CLUB";
    public static final String MODIFY_CLUB = "MOD_CLUB";
    public static final String APPROVE_CLUB_MODIFICATION = "MODAPP_CLUB";
    public static final String DELETE_CLUB = "DEL_CLUB";
    public static final String APPROVE_CLUB_DELETION = "DELAPP_CLUB";
    public static final String VIEW_CLUB_LIST = "VIEW_CLUB";
    public static final String TRANSFER_CHAIRMAN_RIGHTS_INITIATE = "TRF_CLUB_ADM";
    public static final String TRANSFER_CHAIRMAN_RIGHTS_APPROVAL = "APPTRF_CLUB_ADM";
    public static final String CLUB_REPORT = "CLUB_VIEW_REPORT";
    public static final String TRANSFER_RIGHTS_HISTORY_REPORT = "CLUB_TRF_REPORT";
    public static final String ADD_QUESTIONS_TO_MASTER_LIST = "ADDQUEST";
    public static final String DELETE_QUESTIONS_FROM_MASTER_LIST = "DELQUEST";
    public static final String MANAGE_SELF_PIN_RESET_RULES = "RESETRULE";
    public static final String CUSTOMER_SELF_PIN_RESET_STATISTICS = "RSPRPT";
    public static final String CREATE_PROMOTION = "C_ROLE";
    public static final String MODIFY_PROMOTION = "MC_ROLE";
    public static final String VIEW_PROMOTION = "VIEW_PRO";
    public static final String EXTERNAL_INTERFACE_GATEWAY = "EIG_SSO";
    public static final String EIG_CHECKER = "EIG_CHECKER";
    public static final String EIG_MAKER = "EIG_MAKER";
    public static final String MOBIQUITY_REPORTING_SUITE = "PENT_REP";
    public static final String MENU_MANAGEMENT = "MENU_MGMT_SSO";
    public static final String MENU_CREATOR = "MENU_CREATOR";
    public static final String MENU_APPROVER = "MENU_APPROVER";
    public static final String IDENTICAL_TRANSACTION_CRITERION = "IDENT_TXN_CRT";
    public static final String SERVICES_SELECTION = "SUADM_SUBCOM";
    public static final String ADD_WALLET = "MLTPLWALLT_ADD";
    public static final String VIEW_WALLET = "MLTPLWALLT_VIEW";
    public static final String MODIFY_WALLET = "MLTPLWALLT_MOD";
    public static final String DELETE_WALLET = "MLTPLWALLT_DEL";
    public static final String BULK_USER_DELETION = "BULKUSRDELR";
    public static final String GENERAL_LEDGER = "GL_MAIN";
    public static final String CHART_OF_ACCOUNTS = "GL_INIT";
    public static final String GL_ASSOCIATION = "GL_MAP";
    public static final String GL_ASSOCIATION_APPROVAL = "GL_MAPAPPR";
    public static final String MODIFY_GL_ASSOCIATION = "GL_MODMAP";
    public static final String MODIFY_GL_ASSOCIATION_APPROVAL = "GL_MODMAPAPPR";
    public static final String DELTA_UPLOAD = "GL_UPLOAD";
    public static final String VIEW_REPORTS = "GL_CHACC";
    public static final String GL_TO_GL_TRANSFER = "GL_TRANSFER";
    public static final String GL_TO_GL_TRANSFER_APPROVAL = "GL_TRANSFER_APPR";
    public static final String INTEREST_MANAGEMENT = "Interest_MAIN";
    public static final String ADD_INTEREST_PROFILE = "Interest_INIT";
    public static final String ADD_INTEREST_PROFILE_APPROVAL = "Interest_APPROVE";
    public static final String VIEW_INTEREST_PROFILE = "Interest_VIEW";
    public static final String INTEREST_PROFILE_ASSOCIATION = "Interest_DEF";
    public static final String INTEREST_PROFILE_ASSOCIATION_APPROVAL = "Interest_DEFAPPR";
    public static final String MODIFY_INTEREST_PROFILE = "Interest_MOD";
    public static final String MODIFY_INTEREST_PROFILE_APPROVAL = "Int_MODAPPROVE";
    public static final String ACCOUNT_LEVEL_OVERRIDE = "Int_EXCEPTION";
    public static final String ACCOUNT_LEVEL_OVERRIDE_APPROVAL = "Int_ExceptionAppr";
    public static final String DELETE_INTEREST_PROFILE = "Interest_DELETE";
    public static final String DELETE_INTEREST_PROFILE_APPROVAL = "Interest_DELAPPROVE";
    public static final String DISBURSE_SUSPENDED_INTEREST = "Int_SUSP";
    public static final String DISBURSE_SUSPENDED_INTEREST_APPROVAL = "Int_SUSPAPPR";
    public static final String HOLIDAY_CALENDAR = "Int_HOLIDAY";
    public static final String CHURN_INITIATION = "CHURNMGMT_MAIN";
    public static final String CHURN_APPROVAL = "CHURN_APPROVE";
    public static final String CHURN_ENQUIRY = "CHURN_ENQUIRY";
    public static final String CHURN_SETTLEMENT_INITIATE = "CHURN_SETTLE_INIT";
    public static final String CHURN_SETTLEMENT_APPROVAL = "CHURN_SETTLE_APPROVE";
    public static final String BULK_SERVICE_LEVEL_THRESHOLD = "BLK_SVC_UPL";
    public static final String AMBIGUOUS_TRANSACTION = "AMBGTXN_UPLOAD";
    public static final String BULK_MESSAGE_UPDATE = "MSG_UPDT";
    public static final String INITIALIZE_BATCH = "BULK_REG_INIT";
    public static final String BULK_APPROVAL = "BULK_REG_APP";
    public static final String MY_BATCHES = "BULK_MYBATCH_REG";
    public static final String BANK_ACCOUNT_REGISTRATION = "BANK_ACC_REG";
    public static final String BANK_ACCOUNT_DEREGISTRATION = "BANK_ACC_DREG";
    public static final String NFT_BENEFICIARY = "NFTBEN";
    public static final String ADD_BENEFICIARY = "NFTBEN_ADD";
    public static final String MODIFY_BENEFICIARY = "NFTBEN_MOD";
    public static final String DELETE_BENEFICIARY = "NFTBEN_DEL";
    public static final String ADD_MODIFY_DELETE_BANK_ACCOUNTS_APPROVAL = "BNK_APR";
    public static final String SELF_BALANCE_ENQUIRY = "ENQ_USC";
    public static final String ADD_CATEGORY = "ADD_CAT";
    public static final String CATEGORY_MODIFICATION = "MODIFY_CAT";
    public static final String ADD_APPROVE_PSEUDO_USER = "PSEUDO_ADD2";
    public static final String MODIFY_APPROVE_PSEUDO_USER = "PSEUDO_MOD2";
    public static final String DELETE_APPROVE_PSEUDO_USER = "PSEUDO_DELETE2";
    public static final String PSEUDO_CATEGORY_APPROVAL = "PSEUDO_CAT_APPROVAL";
    public static final String MODIFY_SUBSCRIBER = "SUBSMOD";
    public static final String ENABLE_AUTO_DEBIT = "AUTOEAD";
    public static final String DISABLE_AUTO_DEBIT = "AUTODIS";
    public static final String ENABLE_STANDING_INSTRUCTION = "AUTOENASI";
    public static final String DISABLE_STANDING_INSTRUCTION = "AUTODISSI";
    public static final String VIEW_LIST_OF_BENEFICIARIES_FOR_AUTO_DEBIT_SI = "AUTODEBVIEW";
    public static final String REDEMPTION_SCREEN = "CCE_ROLE";
    public static final String REDEMPTION_CORRECTION_INITIATE = "RED_CORR";
    public static final String REDEMPTION_CORRECTION_APPROVAL = "RED_CORAP";
    public static final String ENTERPRISE_PAYMENT_INITIATION = "SAL_BUP";
    public static final String MODIFY_BULK_PAYEE = "MOD_EMP";
    public static final String DELETE_BULK_PAYEE = "DEL_EMP";
    public static final String BULK_PAYEE_REGISTRATION = "BULK_EMP";
    public static final String ADD_BULK_PAYEE = "ADD_EMP";
    public static final String ENTERPRISE_PAYMENT_APPROVAL_1 = "SAL_AP1";
    public static final String ENTERPRISE_PAYMENT_APPROVAL_2 = "SAL_AP2";
    public static final String ENTERPRISE_PAYMENT_DASHBOARD = "BULKPAY_DASHBOARD";
    public static final String ADD_BULK_PAYER_ADMIN = "BPOPPADD";
    public static final String ADD_BULK_PAYER_ADMIN_APPROVAL = "BPOPPADDA";
    public static final String MODIFY_BULK_PAYER_ADMIN = "BPMODOP";
    public static final String MODIFY_BULK_PAYER_ADMIN_APPROVAL = "BPMODOPA";
    public static final String DELETE_BULK_PAYER_ADMIN_APPROVAL = "BPDELOPA";
    public static final String VIEW_SELF_DETAILS_BULK_PAYER = "BPVW_SLF";
    public static final String VIEW_BULK_PAYER_ADMIN = "BPVW_DT";
    public static final String P2P_TRANSFER = "P2P";
    public static final String VIEW_WALLET_PREFERENCES = "KYC001";
    public static final String ADD_SUBSCRIBER = "SUBSADD";
    public static final String ADD_SUBSCRIBER_APPROVAL = "SUBSADDAP";
    public static final String MODIFY_SUBSCRIBER_APPROVAL = "SUBSMODAP";
    public static final String DELETE_SUBSCRIBER_BY_AGENT = "SUBSDELBYAGT";
    public static final String SUBSCRIBER_INFORMATION = "SUBS_INF";
    public static final String CASH_IN = "CIN_WEB";
    public static final String CASH_OUT = "COUT_WEB";
    public static final String AGENT_CASH_OUT = "AGCOUT_WEB";
    public static final String TRANSACTIONS_APPROVAL = "COUT_DISP";
    public static final String LOYALTY_SELF_BALANCE_ENQUIRY = "ENQ_LUSC";
    public static final String CHANNEL_TO_CHANNEL_TRANSFER = "C2C";
    public static final String BULK_PSEUDO_USER_REGISTRATION = "BLK_PSEUDOUSR";
    public static final String BULK_PSEUDO_USER_APPROVAL = "BULK_PSEUDOAP";
    public static final String INVERSE_CHANNEL_TO_CHANNEL_TRANSFER = "INVC2C";
    public static final String OUTWARD_REMITTANCE_INITIATION = "SM_INIT";
    public static final String OUTWARD_REMITTANCE_APPROVAL = "SM_APP";
    public static final String PSEUDO_CATEGORY_MANAGEMENT = "PSEUDO_CAT_MGMT";
    public static final String ADD_PSEUDO_USER = "PSEUDO_ADD";
    public static final String MODIFY_PSEUDO_USER = "PSEUDO_MOD";
    public static final String DELETE_PSEUDO_USER = "PSEUDO_DEL";
    public static final String MODIFY_PSEUDO_ROLES = "PSEUDO_MODIFY_ROLES";
    public static final String MODIFY_PSEUDO_TCP = "PSEUDO_MODIFY_TCP";
    public static final String PSEUDO_GROUP_ROLE = "PSEUDO_GRP";
    public static final String VIEW_PSEUDO_USER = "PSEUDO_VIEW";
    public static final String BILL_PAYMENT = "BP_RETR";
    public static final String SELF_REIMBURSEMENT = "REMB_MNT";
    public static final String EMPLOYEE_GROUP_ROLE = "EMP_GROLE";
    public static final String BANK_BALANCE_ENQUIRY = "BR_BAL";
    public static final String CHANNEL_USER_HELP = "CHELP001";
    public static final String INITIATE_BULK_UPLOAD = "IMS_BULK_INIT";
    public static final String APPROVE_BULK_UPLOAD = "IMS_BULK_APPROVE";
    public static final String BULK_UPLOAD_DASHBOARD = "IMS_BULK_DASHBOARD";
    public static final String AUTO_DEBIT_ENABLE_APPROVAL = "AUTOEADAP";

    /**
     * SUPERADMIN ROLES
     */
    public static final String ADD_GRADES = "ADD_GRADES";
    public static final String MODIFY_GRADES = "MODIFY_GRADES";
    public static final String DELETE_GRADES = "DELETE_GRADES";


    public static void main(String[] args) {

        try {

            //Read RnR
            InputStream inp = new FileInputStream(FilePath.fileConfigInput);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);

            Row r = sheet.getRow(6);

            DataFormatter formatter = new DataFormatter();

            Iterator<Row> iterator = sheet.iterator();
            while (iterator.hasNext()) {
                Row nextRow = iterator.next();
                String roleCode = formatter.formatCellValue(nextRow.getCell(0));
                String roleName = formatter.formatCellValue(nextRow.getCell(1));

                //Below line will generate constants
                System.out.println("public static final String " + roleName.replaceAll("\\s", "_").toUpperCase() + " = \"" + roleCode + "\";");
            }

        } catch (Exception e) {
            System.out.println("Exception Occured");
        }
    }
}