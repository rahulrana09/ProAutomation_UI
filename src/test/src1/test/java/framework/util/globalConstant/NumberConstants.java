/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: Automation Team
 *  Date: 13-12-2017
 *  Purpose: Selenium Test Cases
 */

package framework.util.globalConstant;

public class NumberConstants {


    public static final int CONST_0 = 0;
    public static final int CONST_1 = 1;
    public static final int CONST_2 = 2;
    public static final int CONST_3 = 3;
    public static final int CONST_4 = 4;
    public static final int CONST_5 = 5;
    public static final int CONST_6 = 6;
    public static final int CONST_7 = 7;
    public static final int CONST_8 = 8;
    public static final int CONST_9 = 9;
    public static final int CONST_10 = 10;
    public static final int CONST_11 = 11;
    public static final int CONST_12 = 12;

    /**
     * Selenium Constants
     */
    public static final int SLEEP_1000 = 1000;
    public static final int SLEEP_2000 = 2000;
    public static final int SLEEP_3000 = 3000;
    public static final int SLEEP_5000 = 5000;


}
