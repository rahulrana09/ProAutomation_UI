package framework.util.globalConstant;

public class TransactionType {
    /**
     * Transaction Codes
     */
    public final static String SUBS_BILLER_ASSOC_BY_RETAILER = "RBPREGAREQ";
    public final static String SUBS_BILLER_APPROVAL_BY_RETAILER = "RBPREGAPREQ";
    public final static String CHANGE_MPIN_CHANNEL_USR = "RCMPNREQ";
    public final static String CHANGE_TPIN_CHANNEL_USR = "RCPNREQ";
    public final static String CHANGE_MPIN_CUSTOMER_USR = "CCMPNREQ";
    public final static String CHANGE_TPIN_CUSTOMER_USR = "CCPNREQ";
    public final static String SUBSCRIBER_AQUISITION = "ACQREQ";
    public final static String DELETE_BILLER_REG_BY_RETAILER = "RBPREGDREQ";
    public final static String DELETE_BILLER_REG_BY_RETAILER_APPROVAL = "RBPREGDPREQ";
    public final static String JOIN_SAVING_CLUB = "JOINCLUB";
    public final static String DEPOSIT_SAVING_CLUB = "DEPOCLUB";
    public final static String DISBURSEMENT_INITIATION_CLUB = "SVCDSBINI";
    public final static String DISBURSEMENT_CONFIRMATION_CLUB = "SVCDSBINTER";
    public final static String RESIGN_SAVING_CLUB = "RESIGNCLUB";
    public final static String GET_SVC_LIST = "GETSVCLIST";
    public final static String GET_SVC_WALLET_DETAILS = "SVCWD";
    public final static String SELF_SUBSCRIBER_REGISTRATION = "CUSTREG";
    public final static String SELF_SUBSCRIBER_REGISTRATION_OTP = "GENOTPREQ";
    public final static String SUBSCRIBER_REG_AS400 = "REGST";
    public final static String ADD_SAVING_CLUB = "CRCLUB";
    public final static String SUBSCRIBER_REGISTRATION_CHANNEL_USER = "RSUBREG";
    public final static String WEB_CASHOUT = "RCORREQ";
    public final static String SETTLEMENT_INITIATION_CLUB= "SVCSETINI";
    public final static String SETTLEMENT_CONFIRMATION_CLUB= "SVCSETINTER";
    public final static String SUBS_ACCOUNT_CLOSURE = "RACRREQ";
    public final static String SVC_BALANCE_ENQUIRY = "CBALENQ";
    public final static String SVC_WITHDRAW_CLUB = "WITHCLUB";
    public final static String SVC_WITHDRAW_CLUB_APPROVAL = "WITHCLUBCFN";
    public final static String SVC_BANK_MINI_STATEMENT = "SVCMINIREQ";
    public static final String CUSTOMER_BNK_MINI_STATEMENT = "CMINIREQ";
    public final static String SVC_BANK_TO_WALLET = "SVCB2W";
    public final static String SVC_WALLET_TO_BANK = "SVCW2B";
    public final static String SVC_WALLET_TO_BANK_CONFIRMATION = "SVCW2BCFM";
    public final static String SVC_BANK_TO_WALLET_CONFIRMATION = "SVCB2WCFM";
    public final static String SVC_DISBURSE_STOCK = "SVCDSBMNY";
    public final static String SVC_DISBURSE_CONFIRM = "SVCDSBCFM";
    public final static String SVC_PENDING_TXN = "SVNPEDNTXN";
    public final static String SVC_BANK_BAL = "SVCBNKBAL";
    public final static String SVC_MINI_STATEMENT = "CWTXNHIS";
    public final static String GENERATE_OTP = "GENOTPREQ";
    public final static String Get_EMPLOYEEList = "ELREQ";
    public final static String Auto_Debit_Confirmation_by_Customer = "ADENAREQ5";
    public final static String Last_N_Transaction = "CLTREQ";
    public final static String Day_Txn_Summary = "RDTREQ";
    public final static String My_Account_Details = "RADREQ";
    public final static String Last_N_Pending_Transaction = "SMSTXNHIS";
    public final static String UserenquiryAPI = "ENQUIRYAPI";
    public final static String gradeNetworkAssociation = "GRNETREQ";

    public final static String SELF_REIMBURSEMENT = "SLFREIMB";
    public final static String UnBar_EMPLOYEE = "BLEREQ";
    public final static String BANK_CASH_OUT = "RBCOREQ";
    public final static String BANK_CASH_IN = "RBCIREQ";
    public final static String BANK_CASH_OUT_CONFIRM = "CBCODCREQ";
    public final static String BILLER_ASSOCIATION_SUBSCRIBER = "BPREGREQ";


    public final static String SVC_PROMOTE_DEMOTE = "CPDMEM";

    public final static String CREATE_EMPLOYEE = "EMPMNGMNT";
    public final static String CHANGE_EMPLOYEE_MPIN = "ERMPNREQ";
    public final static String CHANGE_EMPLOYEE_PIN = "ERPNREQ";
    public final static String SUSPEND_EMPLOYEE = "SPEREQ";
    public final static String RESUME_EMPLOYEE = "RAEREQ";
    public final static String AUTO_DEBIT_SUBS_SELFREQ = "CUSTADREQ";
    public final static String AUTO_DEBIT_DISABLE_BY_CUSTOMER = "ADDISREQ";
    public final static String AUTO_DEBIT_CONFIRMATION_BY_CUSTOMER = "ADENAREQ5";
    public final static String AUTO_DEBIT_ENABLE_BY_CHANNELUSER = "RCUSTADREQ";
    public final static String AUTHORIZATION_REQUEST = "AUTHREQ";
    public final static String AUTOMATIC_O2C = "REQAO2C";
    public final static String AUTOMATIC_REIMBURSEMENT = "REQAOPTW";
    public final static String CREATE_SHIFTBASED_EMPLOYEE = "ADEREQ";
    public final static String GET_SHIFTBASED_EMPLOYEE = "ELREQ";
    public final static String SUBS_ACCOUNT_CLOSURE_BY_AGENT = "CCRACRREQ";
    public final static String CREATE_EMPLOYE = "ADEREQ";
    public final static String CHANGE_EMPLOYE_MPIN = "ERMPNREQ";
    public final static String CHANGE_EMPLOYE_TPIN = "ERPNREQ";
    public final static String BALANCE_ENQUIRY = "RBEREQ";
    public final static String SUB_BALANCE_ENQUIRY = "CBEREQ";
    public final static String BALANCE_ENQ_BANK_CHANNELUSR = "RBALREQ";
    public final static String ENABLE_STANDARD_INSTRUCTIONS_CHANLUSER = "SICHAREQ";
    public final static String ENABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER = "SICUSTREQ";
    public final static String DISABLE_STANDARD_INSTRUCTIONS_SUBSCRIBER = "SIDISREQ";
    public final static String ADD_BENIFICIARY_BANK = "ADDBEN1";
    public final static String DELETE_BENIFICIARY = "DELBEN";
    public final static String FETCH_LIST_OF_CURRENCIES = "GETUSRCUR";
    public final static String CHANGE_CURRENCY_CODE = "CNGDEFCUR";
    public final static String DEFAULT_CURRENCY_CODE = "USERINFOREQ";
    public final static String SIM_SWAP = "UPDSIMSWAP";
    public final static String BARorUNBAR = "BARRINGREQ";
    public final static String BANK_REGISTRATION = "BANKREG";
    public final static String BANK_ASSOCIATION = "BNKATREG";
    public final static String BANK_DEREGISTRATION = "BKDREG";
    public final static String VALIDATE_mPIN = "CVMPINREQ";
    public final static String VIEW_LIST_OF_BENEFICIARY_AD_SI = "VIEWADSIREQ";
    public final static String P2P_SEND_MONEY_SUBS = "CTMREQ";
    public final static String VIEW_BILL = "CVMBREQ";
    public final static String GET_ANSWER_FOR_USER_QUESTIONS = "GETANSREQ";
    public static final String TRANSACTION_ENQUIRY = "TRANSREQ";
    public static final String DEBIT_MONEY_FROM_SVA = "DRSVA";
    public static final String CREDIT_MONEY_TO_WALLET = "CRSVA";
    public static final String RETAILER_BANK_REQ = "RETBKRQ";
    public static final String SUSPEND_RESUME_USER = "SUSRESREQ";
    public static final String DELETE_CUSTOMER = "DELCUSTREQ";

}
