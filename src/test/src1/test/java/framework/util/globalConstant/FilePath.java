package framework.util.globalConstant;

import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.propertiesManagement.CITReportProperties;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Automation Team on 5/7/2017.
 */
public class FilePath {
    public final static String basePath = new File("").getAbsolutePath();

    // APP DATA path
    public final static String dirAppData = basePath + "/src/test/resources/AppData/";
    public final static String dirTempData = basePath + "/src/test/resources/TempData/";
    public final static String dirImages = basePath + "/src/test/resources/Images/";
    public final static String dirTestData = basePath + "/src/test/resources/TestData/";

    // Dir paths
    public final static String dirFileDownloads = basePath + "\\downloads\\";
    public final static String dirFileUploads = basePath + "\\uploads\\";
    public final static String dirReports = basePath + "\\reports\\";
    public final static String dirSnapShot = dirReports + "capture\\";
    public final static String fileSimpleReport = dirReports + "suiteRunReport\\LastRunReport.xlsx";

    // Status log files
    public final static String dirFailSuitesPath = basePath + "/src/test/resources/Suites/_06_Fail/";
    public final static String dirFailTestRecord = basePath + "/src/test/resources/FailTestRecords/";
    public final static String dirDailyExecStatusLog = dirFailTestRecord + "DailyLog/";
    public final static String fileCurrentExecutionLog = basePath + "/src/test/resources/FailTestRecords/CCLog.csv";

    //CIT Report vars
    public final static String dirCITReport = System.getProperty("user.dir") + "\\CITReports\\";

    public final static String CIT_REPORT_OUTPUT_FILE = dirCITReport + CITReportProperties.getInstance().getProperty("BUILDID") + "_"
            + CITReportProperties.getInstance().getProperty("TestFrameworkName") + "_" +
            CITReportProperties.getInstance().getProperty("ProductInterface")
            + "_" + DataFactory.getCurrentDateTimeExcel() + ".xlsx";

    public final static String dirCatalinaLogs = System.getProperty("user.dir") + "\\reports\\logs\\";

    // App Data
    public final static String fileConfigInput = basePath + "/src/test/resources/Config/ConfigInput.xlsx";
    public final static String fileExecutionConstants = basePath + "/src/test/resources/Config/ExecutionConstants.xlsx";
    public final static String fileGroupRoles = dirAppData + "Grouproles.xlsx";
    public final static String fileMobileRoles = dirAppData + "MobileRole.xlsx";
    public final static String fileConsumerTCP = dirAppData + "CustomerTCP.xlsx";
    public final static String fileInstrumentTCP = dirAppData + "InstrumentTCP.xlsx";
    public final static String fileChannelUsers = dirAppData + "ChannelUsers.xlsx";
    public final static String fileOperatorUsers = dirAppData + "OperatorUsers.xlsx";
    public final static String fileLeafUsers = dirAppData + "Subscribers.xlsx";
    public final static String fileBaseSetInput = dirAppData + "BaseSet_Input.xlsx";

    // Temp Data
    public final static String fileTempBillers = dirTempData + "TempBiller.xlsx";
    public final static String fileTempSavingClub = dirTempData + "TempSavingClub.xlsx";
    public final static String fileTempUsers = dirTempData + "TempUsers.xlsx";
    public final static String fileTempOptUsers = dirTempData + "TempOperatorUsers.xlsx";
    // bat files
    public final static String fileTXDriver = basePath + "/src/test/resources/Config/dk.bat";
    // Downloaded Files
    public static String fileBulkAmbiguousTransaction = dirFileDownloads + Constants.FILEPREFIX_BULK_AMBIGUOUS_TXN + ".xls"; // at any instance only one will be available to make test simple
    public static String fileBulkAmbiguousTransactionLog = dirFileDownloads + Constants.FILEPREFIX_BULK_AMBIGUOUS_TXN_LOGS + ".xls"; // at any instance only one will be available to make test simple
    public static String fileBulkBillerRegistration = dirFileDownloads + Constants.FILEPREFIX_BULK_BILLER_ASSOC + ".csv";
    public static String fileBulkBillerRegistrationLog = dirFileDownloads + Constants.FILEPREFIX_BULK_BILLER_ASSOC_LOG + ".csv";
    public static String fileAddBranch = dirFileDownloads + "AddBranches.csv";
    public static String fileBulkSubReg = dirFileDownloads + Constants.BULK_SUB_REG + ".csv";
    public static String fileBulkChUserReg = dirFileDownloads + Constants.BULK_CH_USER_REG + ".csv";
    public static String fileBulkBankAssoc = dirFileDownloads + Constants.BULK_BANK_ASSOC + ".csv";
    public static String fileBulkBankAssocEconet = dirFileDownloads + Constants.BULK_BANK_ASSOC_ECONET + ".csv";
    public static String fileCommissionDisbursement = dirFileDownloads + Constants.FILEPREFIX_COMMISSION_DISBURSEMENT + ".csv";
    public static String fileChurnInitiation = dirFileDownloads + Constants.FILEPREFIX_CHURN_INITIATE + ".csv";
    public static String fileEntPaymentTemplate = dirFileDownloads + Constants.FILEPREFIX_ENT_PAYMENT + ".csv";
    public static String fileBulkBillPayTemplate = dirFileDownloads + Constants.FILEPREFIX_BILL_PAY_ENTERPRISE + ".csv";
    public static String fileBulkRechargeOtherTemplate = dirFileDownloads + Constants.FILEPREFIX_RECHARGE_OTHER_ENTERPRISE + ".csv";
    public static String fileBulkCashInTemplate = dirFileDownloads + Constants.FILEPREFIX_BULK_CASH_IN + ".csv";
    public static String fileBulkMerchPayTemplate = dirFileDownloads + Constants.FILEPREFIX_BULK_MERC_PAY + ".csv";
    public static String fileCurrentConfigProfile = dirFileDownloads + "BulkServiceLevelThreshold" + ".xls";
    public static String fileBulkStockLiquidationTemplate = dirFileDownloads + Constants.FILEPREFIX_BULK_STOCK_LIQUIDATION + ".csv";
    public static String fileBulkStockLiquidation = dirFileDownloads + Constants.FILEPREFIX_BULK_STOCK_LIQUIDATION_LOGS + ".csv";
    public static String fileEntIndividualRecordReject = dirFileDownloads + Constants.FILEPREFIX_ENT_INDIVIDUAL_REC_REJECT + ".csv";

    // Sikuli files
    public static String sikuliEditBox_01 = basePath + "/src/test/resources/Sikuli/text_file_01.png";
    public static String sikuliEditBox_02 = basePath + "/src/test/resources/Sikuli/text_file_02.png";
    public static String sikuliOpen_01 = basePath + "/src/test/resources/Sikuli/btn_open_01.png";
    public static String sikuliOpen_02 = basePath + "/src/test/resources/Sikuli/btn_open_02.png";
    public static String sikuliIeSaveBtn = basePath + "/src/test/resources/Sikuli/ieSave.png";
    // image files & path
    static File upf = new File(dirImages + "brand.png");
    public final static String uploadFile = upf.getAbsolutePath().toString();
    static File maxSizeFile = new File(dirImages + "image.png");
    public final static String maxSizeFilePath = maxSizeFile.getAbsolutePath().toString();
    static File doubleExtnFile = new File(dirImages + "image.check.png");
    public final static String doubleExtnFilePath = doubleExtnFile.getAbsolutePath().toString();

    /**
     * Delete Files at path with Prefix
     *
     * @param path
     * @param prefix
     * @return
     */
    public static boolean deleteFilesForPathByPrefix(final String path, final String prefix) {
        boolean success = true;
        try (DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(Paths.get(path), prefix + "*")) {
            for (final Path newDirectoryStreamItem : newDirectoryStream) {
                Files.delete(newDirectoryStreamItem);
            }
        } catch (final Exception e) {
            success = false;
            e.printStackTrace();
        }
        Utils.putThreadSleep(NumberConstants.SLEEP_5000); // TODO - need a generic Wait
        return success;
    }

    public static String fetchFilesForPathByPrefix(final String path, final String prefix) throws InterruptedException {
        String filepath = "";
        try (DirectoryStream<Path> newDirectoryStream = Files.newDirectoryStream(Paths.get(path), prefix + "*")) {
            for (final Path newDirectoryStreamItem : newDirectoryStream) {
                filepath = newDirectoryStreamItem.toString();
            }
        } catch (final Exception e) {

            e.printStackTrace();
        }
        Thread.sleep(5000); // TODO - need a generic Wait
        return filepath;
    }

    public static void main(String[] args) {
        System.out.println(basePath);
    }
}
