/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 29-DEC-2017
 *  Purpose: FOR NEGATIVE TEST ON BUTTONS
 */

package framework.util.globalConstant;

public class Button {

    public static final String SUBMIT = "SUBMIT";
    public static final String CONFIRM = "CONFIRM";
    public static final String CANCEL = "CANCEL";
    public static final String BACK = "BACK";
    public static final String CONFIRM_BACK = "CONFIRM_BACK";
    public static final String RESET = "RESET";
    public static final String SAVE = "SAVE";
    public static final String ADD = "ADD";

    public static final String ADD_MORE_GRADE = "ADD_MORE_GRADE";
    public static final String DELETE_GRADE = "DELETE_GRADE";

    public static final String REMOVE = "REMOVE";
    public static final String REMV_MORE_BANK_AC = "REMV_MORE_BANK_AC";
    public static final String ADD_MORE_BANK_AC = "ADD_MORE_BANK_AC";

}
