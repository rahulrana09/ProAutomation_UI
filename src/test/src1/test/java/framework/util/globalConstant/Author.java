package framework.util.globalConstant;

/**
 * Created by navin.pramanik on 9/14/2018.
 */
public class Author {


    /**
     * This class contains Constants for Author Names.
     * The main aim to create this class is to follow a common standard while assigning Author Names
     * Don't be confuse just use these Constants to mark Author name in Extent Report
     *
     * @TeamAutomation ... Happy Coding :)
     */
    public static final String RAHUL = "rahul.rana";
    public static final String NAVIN = "navin.pramanik";
    public static final String PRASHANT = "prashant.kumar";
    public static final String FEBIN = "febin.thomas";
    public static final String SURYA = "surya.dhal";
    public static final String KRISHAN = "krishan.chawla";
    public static final String SARASWATHI = "saraswathi.annamalai";
    public static final String NIRUPAMA = "nirupama.mk";
    public static final String PUSHPALATHA = "pushpalatha.pattabiraman";

}
