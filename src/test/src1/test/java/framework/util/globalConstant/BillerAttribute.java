package framework.util.globalConstant;

public class BillerAttribute {
    public static final String SERVICE_LEVEL_ADHOC = "SL4";
    public static final String SERVICE_LEVEL_STANDARD = "SL2";
    public static final String SERVICE_LEVEL_PREMIUM = "SL1";
    public static final String SERVICE_LEVEL_DELUX = "SL3";

    public static final String PROCESS_TYPE_ONLINE = "REAL";
    public static final String PROCESS_TYPE_OFFLINE = "P_BATCH";
    public static final String BILLER_TYPE_BILL_AVAILABLE = "Bill Available";
    public static final String BILLER_TYPE_BILL_UNAVAILABLE = "Bill Unavailable";

    public static final String BILL_AMOUNT_EXACT_PAYMENT = "Exact Payment";
    public static final String BILL_AMOUNT_PART_PAYMENT = "Part Payment";
    public static final String BILL_AMOUNT_NOT_APPLICABLE = "NA";

    public static final String SUB_TYPE_UNDER_PAYMENT = "Under Payment";
    public static final String SUB_TYPE_OVER_PAYMENT = "Over Payment";
    public static final String SUB_TYPE_NOT_APPLICABLE = "NA";
}