package framework.util.globalConstant;

/**
 * Created by navin.pramanik on 9/7/2018.
 */
public class DBStatus {

    public static final String ACTIVE = "Y";
    public static final String UPDATE_INITIATED = "UI";
    public static final String DELETE_INITIATED = "DI";
    public static final String ADD_INITIATED = "AI";
    public static final String SUSPENDED = "S";
    public static final String DELETED = "N";
    public static final String SUSPEND_INITIATED = "SI";
    public static final String BLACKLISTED = "BL";
    public static final String BARRED_AS_SENDER = "BS";
    public static final String BARRED_AS_RECEIVER = "BR";
    public static final String BARRED_AS_BOTH = "BB";

}
