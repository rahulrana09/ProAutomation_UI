package framework.util.globalConstant;

/**
 * Created by test.user on 3/19/2019.
 */
public enum UserType {

    SUBSCRIBER("SUBSCRIBER") ,
    CHANNEL("CHANNEL") ,
    MERCHANT("MERCHANT") ,
    OPERATOR("OPERATOR") ,
    TOPUPOPERATOR("TOPUPOPERATOR");

    private String value;
    UserType(String value){
        this.value=value;
    }
}
