/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: Automation Team
 *  Date: 13-12-2017
 *  Purpose: Selenium Test Cases
 */

package framework.util.globalConstant;

import java.math.BigDecimal;

public class Constants {

    public static int WAIT_TIME = 1000; // static wait , must not be less than 500 | 0.5 sec
    public static int MAX_WAIT_TIME = 2800; // Static wait, should not exceed 3000 | 3 sec
    public static int EXPLICIT_WAIT_TIME = 12;

    public static final String SVA_SCHEDULE_OCCURANCE = "2";
    public static final String SALARY_WALLET = "11";
    public static final String NORMAL_WALLET = "12";
    public static final String COMMISSION_WALLET = "13";
    public static final String SAVINGS_CLUB = "18";
    public static final String COMMISSION_WALLET_STRING = "Commission";
    public static final String NORMAL_WALLET_STRING = "Normal";
    public static final String SVA_AMOUNT = "20";
    /*
     * N E G A T I V E   T E S T
     */
    public static final String FIND_ELEMENT_BY_CLASS = "BY_CLASS";
    public static final String FIND_ELEMENT_BY_ID = "BY_ID";
    public static final String FIND_ELEMENT_BY_LINK = "BY_LINK";
    public static final String FIND_ELEMENT_BY_XPATH = "BY_XPATH";
    public static final String FIND_ELEMENT_BY_NAME = "BY_NAME";
    public static final String FIND_ELEMENT_BY_CSS = "BY_CSS";
    public static final String CONSTANT_LOGOUT = "LOGOUT";
    public static final String CONSTANT_LOGIN = "LOGIN";
    public static final String AUTOCOMPLETE_OFF = "off";
    /*
     *  N E G A T I V E   T E S T   C O N S T A N T S
     */
    public static final String NEGATIVE_AMOUNT_CONSTANT = "-100";
    public static final String SPECIAL_CHARACTER_CONSTANT = "@#)*$%$&!%@$$";
    public static final String ALPHABET_CONSTANT = "abcdefghijweivhj";
    public static final String ALPHANUMERIC_CONSTANT = "abcd1234";
    public static final String BLANK_CONSTANT = "";
    public static final String SPACE_CONSTANT = " ";
    public static final String VALUE_SELECT = "Select";
    public static final String INVALID_MSISDN_LENGTH = "123456";
    public static final String DECIMAL_SPECIAL = "100.909";
    /*
     *   C C E      P O R T A L
     */
    public static final String MOBILE_ROLE_PREFIX = "Mob";
    public static final String CCE_MOBILE_NUMBER = "Mobile Number";
    public static final String CCE_KYC_ID = "KYC ID";
    public static final String CCE_ACCOUNT_ID = "Account ID";
    public static final String CCE_AGENT_CODE = "Agent Code";
    /*
     * Selenium Constants
     */
    public static final int THREAD_SLEEP_1000 = 1000;
    //-------------------------------------------------------------------------------------
    public static final int TWO_SECONDS = 2000;
    public static final String GOLD_WHOLESALER = "GWS";
    public static final String SILVER_WHOLESALER = "SWS";
    public static final String GOLD_RETAILER = "GRT";
    public static final String SILVER_RETAILER = "SRT";
    public static final String GOLD_SUBSCRIBER = "SUBS";
    public static final String SILVER_SUBSCRIBER = "SSUBS";
    public static final String BILLER_GRADE = "HMERGRD";
    public static final String CASHIN_TRANS_ID_PREFIX = "CI";
    public static final String ACCOUNT_IDENTIFIER_MOBILE_NUMBER = "Mobile Number";
    public static final String ACCOUNT_IDENTIFIER_KYC_ID = "KYC ID";
    public static final String ID_TYPE_PASSPORT = "PASSPORT";
    public static final String ACCOUNT_IDENTIFIER_ACCOUNT_ID = "Account ID";
    //-------------------------------------------------------------------------------------
    public static final String ACCOUNT_IDENTIFIER_AGENT_CODE = "Agent Code";
    public static final String Category_SUBS = "Subscriber";
    public static final String USER_TYPE_PAYER = "PAYER";
    public static final String USER_TYPE_PAYEE = "PAYEE";
    public static final String USR_TYPE_SUBS = "SUBSCRIBER";
    public static final String USR_TYPE_CHANNEL = "CHANNEL";


    // Wallets
    //operator management
    public static final String GRADE_CODE = "MERCHANT";
    public static final String SMSC = "1";
    public static final String TOPUP = "1";
    public static final String RECHARGING_OPTIONS_FLEXI_RECHARGE = "Flexi Recharge";
    public static final String RECHARGING_OPTIONS_DENOMINATIONS = "Denominations";
    public static final String RECHARGING_DENOMINATIONS_SUPPORTED = "100";
    public static final String RECHARGING_FLEXI_MIN = "1";
    public static final String RECHARGING_FLEXI_MAX = "100";
    public static final String PREFERENCE_EXTERNAL_CODE = "2";
    public static final String EXTERNAL_CODE_DEFAULT = "1";
    public static final String PREFERENCE_SIM_SWAP_DURATION_1S = "1S";
    public static final String PREFERENCE_SIM_SWAP_DURATION_1h = "1h";
    public static final String TRANSACTION_DATA = "{AUTOMATION}";
    public static final String MOBILENUMBER = "Mobile Number";
    public static final String KYC_ID = "KYC ID";
    public static final String AccountID = "Account ID";
    public static final String SUSPEND = "Suspend";
    public static final String UNBAR = "UnBar";
    public static final String RESUME = "Resume";
    public static final String STATUS_ACCEPT = "0";
    public static final String STATUS_REJECT = "1";

    //Preference Type
    public static final String PREFERENCE_TYPE_USER_REGISTRATION = "USER_REGISTRATION";
    public static final String PREFERENCE_TYPE_BANK_ACCOUNT_LINKING = "BANK_ACCOUNT_LINKING";
    public static final String PREFERENCE_TYPE_AUTOMATIC_GRADE_CHANGE = "AUTOMATIC_GRADE_CHANGE";
    public static final String PREFERENCE_TYPE_SAVING_CLUB_CREATION = "SAV_CLUB_CREATION";
    // Bulk Payout service types
    public static final String BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT = "COMMISSION DISBURSEMENT";
    public static final String BULK_PAYOUT_SERVICE_CASHIN = "CASH IN";
    public static final String BULK_PAYOUT_SERVICE_CASHOUT = "CASH OUT";
    public static final String BULK_PAYOUT_SERVICE_P2P_SENDMONEY = "P2P - SEND MONEY";
    public static final String BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION = "Stock Liquidation";
    public static final String BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT = "Stock Reimbursement";
    public static final String BULK_PAYOUT_SERVICE_MER_PAY = "MERCHANT PAYMENT";

    //Security Question Answer
    public static final String ANSWER = "Test";
    public static final String COUNT = "Test";
    public static final String SUM = "Test";
    public static final String BULK_PAYOUT_ENTERPRISE_PAYMENT = "ENTERPRISE_PAYMENT";
    public static final String BULK_PAYOUT_BILL_PAY_ENTERPRISE = "BULK_BILLPAY_ENT";
    public static final String BULK_PAYOUT_MERCHANT_PAY_ENTERPRISE = "BULK_MERCHPAY_ENT";
    public static final String BULK_PAYOUT_RECHARGE_OTHER_ENTERPRISE = "BULK_RECHARGE_OTHER_ENT";
    public static final String ENTERPRISE_RECHARGE_OTHER = "RECHARGE - OTHERS";

    //Inventory Management System
    public static final String IMS_INVENTORY = "BULK_PRODUCT_INVENTORY";
    public static final String IMS_CATALOGUE = "BULK_PRODUCT_CATALOGUE";
    public static final String IMS_ALL = "ALL";

    //---------------------------------------------------------------------------------
    // Automation Prefix
    // create any bank or category grade with this prefix
    // During Base Set Creation any entiry starting with AUT_PREFIX will be excluded
    public static String AUT_PREFIX = "AUTX";
    public static String LANGUAGE1 = "1";
    public static String LANGUAGE2 = "2";

    /**
     * C A T E G O R I E S
     */
    public static String OPERATOR = "OPT";
    public static String BANK = "BNK";
    public static String SUPER_ADMIN = "SUADM";
    public static String NETWORK_ADMIN = "NWADM";
    public static String CHANNEL_ADMIN = "BCU";
    public static String WHOLESALER = "WHS";
    public static String WHS_CATEGORY_CODE = "DISTWS";
    public static String SPECIAL_SUPER_AGENT = "SSA";
    public static String RETAILER = "RT";
    public static String BANK_ADMIN = "BNKADM";
    public static String BANK_USER = "BNKUSR";
    public static String ENTERPRISE = "Enterprise";
    public static String REJECT_BULK_ENT_PAY = "REJECT_BULK_ENT_PAY";
    public static String BULK_PAYER_ADMIN = "ENTADM";
    public static String BULK_C2C = "C2C";
    public static String BULK_O2C = "O2C transfer";
    public static String CUSTOMER_CARE_EXE = "CCE";
    public static String SUBSCRIBER = "SUBS";
    public static String BILL_COMPANY = "WBILLMER";
    public static String VAULT_ADMIN = "VADM";
    public static String HEAD_MERCHANT = "HMER";
    public static String MERCHANT = "MER";
    public static String BILLER = "WBILLMER";
    public static String ZEBRA_MER = "MERCHANT";
    public static String INT_PARTNER = "INTPAT";
    public static String WHS = "Wholesaler";
    //Biller Related Constants
    public static String BILLER_BILL_PAYEE = "Payee";
    public static String BILL_SUBTYPE_OVERPAY = "AMT_MORE";
    /*
     * T H R E S H O L D S
     */
    public static String MIN_THRESHOLD = "999";
    public static String MAX_THRESHOLD = "99999";
    public static String USR_MIN_BALANCE = "1";
    public static String MIN_TRANSACTION_AMT = "1";
    public static String MAX_TRANSACTION_ALLOWED = "100";
    public static String MAX_TRANSACTION_AMT = "99999";
    public static String MAX_EA_TRANSACTION_AMT = "9999";
    public static BigDecimal MIN_EA_STOCK_AMT = new BigDecimal("500");
    public static String USR_MAX_BALACE = "99999";
    public static String STOCK_TRANSFERINDECIMAL1 = "499.5";
    public static String REMARKS = "Automation Remarks";
    public static String Stock_Statuslevel1 = "Transaction Initiated";
    public static String Stock_Statuslevel2 = "Transaction Approval1";
    // Service Charge Constants
    public static String SCHARGE_MAX_TXN_AMT = "4999";
    // SVA Transfer NOW
    public static String SVA_TRANSFER_NOW = "Transfer Now";
    public static String SVA_SCHEDULE_TRANSFER = "Schedule Later";
    public static String WALLET_101 = "101IND01";
    public static String WALLET_108 = "IND08";
    public static String WALLET_109 = "IND09";
    public static String WALLET_104 = "IND04";
    public static String SERVICE_CHARGE_APPROVE_CREATE = "Create";
    public static String SERVICE_CHARGE_APPROVE_MODIFY = "Modify";
    public static String SERVICE_CHARGE_APPROVE_DELETE = "Delete";
    public static String CREATE = "Create";
    public static String MODIFY = "Modify";
    public static String DELETE = "Delete";
    //Account Types
    public static String SAVINGS_ACCOUNT = "Saving Account";
    public static String CURRENT_ACCOUNT = "Current Account";
    public static String SAVINGS_ACCOUNT_TYPENO = "02";
    public static String CURRENT_ACCOUNT_TYPENO = "01";
    //Existence Status
    public static String ACTIVE = "Active";
    public static String SUSPENDED = "Suspended";
    /**
     * B I L L   P A Y M E N T
     */
    public static String MERCHANT_TYPE_ONLINE = "ONLINE";
    public static String MERCHANT_TYPE_OFFLINE = "OFFLINE";
    public static String BILLER_CATEGORY = "AUTBillerCategory";
    public static String BILL_PROCESS_TYPE_ONLINE = "REAL";
    public static String BILL_PROCESS_TYPE_OFFLINE = "P_BATCH";
    public static String AUT_BILLER_CATEGORY = "AUTBillCat";
    public static String BILL_SERVICE_LEVEL_BOTH = "SL3";
    public static String BILL_SERVICE_LEVEL_ADHOC = "SL4";
    public static String BILL_SERVICE_LEVEL_PREMIUM = "SL1";
    public static String BILL_SERVICE_LEVEL_STANDARD = "SL2";
    public static String BILL_TYPE_ALPHANUMERIC = "A";
    public static String BILL_TYPE_NUM_WITH_SPECIAL = "NS";
    public static String BILL_TYPE_ALPHA_CHAR = "AL";
    public static String BILL_TYPE_NUMERIC = "NM";
    public static String BILL_REFTYPE_CONSTANT = "C";
    public static String BILL_REFTYPE_DYNAMIC = "D";
    public static String BILL_REFTYPE_REGISTRATION = "R";
    public static String BILL_NOTIFICATION_TYPE_DUE_DATE_BEFORE = "BDD_B";
    public static String BILL_NOTIFICATION_TYPE_GENERATE_DATE_AFTER = "BGD_A";
    public static String BILLER_TYPE_BOTH = "Payment(Both)";
    public static String BILLER_TYPE_PRESENTMENT = "BILL_PRE";
    //public static String STATUS_PENDING = "QI";
    public static String BILLER_AMOUNT_PART = "Part Payments";
    public static String BILLER_EXACT_PAYMENT = "AMT_SAME";
    public static String BILLER_PAYMENT_EFFECTED_TO_WALLET = "WALLET";
    public static String BILLER_BILL_DELETION_FREQUENCY = "30";
    public static String BILLER_PAYMENT_SUBTYPE = "Both";
    public static String BILL_EXACT_PAYMENT = "SL1_AMT_SAME";
    /**
     * S E R V I C E   T Y P E
     */
    public static String STOCK_SERVICE_TYPE = "STOCK";
    public static String EASTOCK_SERVICE_TYPE = "STKTR2OCA";
    public static String REIMBURSEMENT_SERVICE_TYPE = "OPTW";
    public static String USER_TYPE_OPT_USER = "OPT_USERS";
    public static String BAR_CHANNEL_CONST = "CH_USERS";
    public static String USER_TYPE_CHANNEL = "CH_USERS";
    public static String USER_TYPE_SUBS = "USER_SUBS";
    public static String USER_TYPE_BILLER = "BILL_MER";
    /**
     * B A R R I N G   M A N A G E M E N T
     */
    public static String BAR_AS_SENDER = "BLK_PR_S";
    public static String BAR_AS_RECIEVER = "BLK_PR_R";
    public static String BAR_AS_BOTH = "BLK_PR_B";
    /**
     * C H U R N   M A N A G E M E N T
     */
    public static String CHURN_INITIATED = "BLK_PR_S";
    public static String CHURNED = "BLK_PR_R";
    /**
     * D O W N L O A D   F I L E   P R E F I X
     */
    public static String FILEPREFIX_BULK_BILLER_ASSOC = "bulkBillerAssociation";
    public static String FILEPREFIX_BULK_BILLER_ASSOC_LOG = "bulkBillerAssociation";
    //-------------------------------------------------------------------------------------
    public static String FILEPREFIX_BULK_AMBIGUOUS_TXN = "AmbiguousTransactions";
    public static String FILEPREFIX_BULK_AMBIGUOUS_TXN_LOGS = "AmbiguousTransactionsLogs";
    public static String FILEPREFIX_BULK_STOCK_LIQUIDATION_LOGS = "Stock_Liquidation";
    public static String FILEPREFIX_BULK_BILLER_ASSOCIATION = "bulkBillerAssociation";
    public static String FILEPREFIX_COMMISSION_DISBURSEMENT = "CommissionDisbursement";
    public static String FILEPREFIX_CHURN_INITIATE = "ChurnUserInitiation";
    public static String FILEPREFIX_CHURN_LOG = "ChurnUserInitiationLog_";
    public static String FILEPREFIX_ADD_MASTER_QUESTIONS_LIST = "MasterQuestionList";
    public static String FILEPREFIX_ADD_MASTER_QUESTIONS_LOG = "MasterQuestionLogs";
    public static String FILEPREFIX_BULK_USER_DELETION = "BulkUserDeletion";
    public static String FILEPREFIX_ENT_PAYMENT = "ENTERPRISE_PAYMENT-template";
    public static String FILEPREFIX_BILL_PAY_ENTERPRISE = "BULK_BILLPAY_ENT-template";
    public static String FILEPREFIX_RECHARGE_OTHER_ENTERPRISE = "BULK_RECHARGE_OTHER_ENT-template";
    public static String FILEPREFIX_BULK_MERCHANTPAY_ENT = "BULK_MERCHANTPAY_ENT-template";
    public static String FILEPREFIX_BULK_CASH_IN = "BULK_CASHIN-template";
    public static String FILEPREFIX_BULK_MERC_PAY = "BULK_MERCHPAY-template";
    public static String FILEPREFIX_BULK_STOCK_LIQUIDATION = "BULK_STKLIQREQ-template";
    public static String FILEPREFIX_ENT_INDIVIDUAL_REC_REJECT = "BULK_REJECT_RECORD-template";
    //Reimbursement Constants
    public static String OPERATOR_REIMB = "OPERATOR";
    public static String CHANNEL_REIMB = "CHANNEL";
    public static String SUBSCRIBER_REIMB = "CUSTOMER";
    public static String MERCHANT_REIMB = "MERCHANT";
    public static String CUSTOMER_REIMB = "CUSTOMER";
    public static String rechargeOperator = "TOPUPOPERATOR";
    //Transactions Constants
    public static String O2C_TRANS_AMOUNT = "1000";
    public static String C2C_TRANS_AMOUNT = "100";
    public static String CASHIN_TRANS_AMOUNT = "100";
    public static String STOCK_LIMIT_AMOUNT = "100";
    public static String EA_STOCK_LIMIT_AMOUNT = "100";
    public static String STOCK_TRANSFER_AMOUNT = "100";
    public static String STOCK_EA_TRANSFER_AMOUNT = "100";
    public static String STOCK_WITHDRAW_AMOUNT = "100";
    public static String MAX_O2C_AMOUNT = "1999";
    public static String MIN_O2C_AMOUNT = "99";
    public static String MAX_CASHIN_AMOUNT = "199";
    public static String MIN_CASHIN_AMOUNT = "99";
    public static String CASHIN_BUFFER = "20"; // Additional amount to be added when performing Cash in
    public static String MIN_CASHOUT_AMOUNT = "10";
    public static String STOCK_TRANSFER_LEVEL1_AMOUNT = "99";
    public static String minimun_AMOUNT = "5";
    //-------------------------------------------------------------------------------------
    public static String STOCK_TRANSFER_LEVEL2_AMOUNT = "101";
    public static String STOCK_TRANSFERINDECIMAL2 = "599.05";
    //-------------------------------------------------------------------------------------
    public static String REIMBURSEMENT_AMOUNT = "20";
    public static String CHANNEL_USERS_CONST = "CH_USERS";

    //-------------------------------------------------------------------------------------
    public static String SUBS_USERS_CONST = "USER_SUBS";
    public static String RESET_MPIN_CONST = "MPIN";
    public static String RESET_TPIN_CONST = "TPIN";
    //KYC Related Constants
    public static String REGTYPE_NO_KYC = "NO_KYC";
    public static String REGTYPE_FULL_KYC = "FULL_KYC";
    public static String REGTYPE_SUBS_INIT_US = "SUB_INI_US";
    public static String REGTYPE_SUBS_INIT_USSD = "SUB_INI_USSD";
    public static String REGTYPE_NO_KYC_TEXT = "No KYC";
    public static String BNK_LNKD_USR = "BNK_LNKD_USR";
    //Payment Instrument Constants
    public static String PAYINST_WALLET_CONST = "WALLET";
    public static String PAYINST_WALLET_CONST_MOBILE_ROLE = "Wallet";
    /**
     * D B  U S E R  S T A T U S
     */
    public static String STATUS_ACTIVE = "Y";
    public static String STATUS_DELETE = "N";
    public static String STATUS_PENDING = "QS";
    public static String STATUS_ADD_INITIATE = "AI";
    public static String STATUS_UPDATE_INITIATE = "UI";
    public static String STATUS_DELETE_INITIATE = "DI";
    public static String STATUS_SUSPEND = "S";
    public static String STATUS_SUSPEND_INITIATE = "SI";
    public static String STATUS_MODIFY_INITIATE_SUBS = "IM";
    public static String STATUS_MODIFY_INITIATE_CHANNEL = "MI";
    public static String STATUS_BLACKLIST = "BL";
    public static String BAR_TYPE_SENDER = "BS";
    public static String BAR_TYPE_RECEIVER = "BR";
    public static String BAR_TYPE_BOTH = "BB";
    public static String PAYINST_BANK_CONST = "BANK";
    public static String ADD_INITIATE_CONSTANT = "AI";
    public static String UPDATE_INITIATE_CONSTANT = "UI";
    public static String DELETE_INITIATE_CONSTANT = "DI";
    public static String AUT_STATUS_CHURN_INITIATE = "CHURN_INIT";
    public static String AUT_STATUS_CHURNED = "CHURNED";
    public static String AUT_STATUS_DELETE_INITIATE = "DEL_INIT";
    public static String AUT_STATUS_DELETED = "DELETED";
    public static String BULK_USER_STATUS_ADD = "A";
    public static String BULK_USER_STATUS_MODIFY = "M";
    public static String BULK_USER_STATUS_DELETE = "D";
    public static String FILEPREFIX_BULK_SUBS_REGISTER = "BulkCustomerRegistrationCore";
    public static String FILEPREFIX_BULK_USER_REGISTER = "BulkChannelRegistrationCore";
    /**
     * T R A N S A C T I O N   S T A T U S
     */
    public static String TXN_SUCCESS = "200";
    public static String TXN_STATUS_SUCCEEDED = "SUCCEEDED";
    // System Wallet id
    public static String WALLET_103 = "IND03";
    public static String WALLET_101_101 = "101IND01";
    /**
     * B A N K   C O N S T A N T S
     */
    public static String TEMP_BANK_TRUST = "AUTBANK"; // if a temp trust bank is to be created
    public static String BANK_CBS_SPARROW = "0001";
    public static String BANK_CBS_FLEX = "0002";
    public static String BANK_TYPE_TRUST = "TRUST";
    public static String BANK_TYPE_NON_TRUST = "NTRUST";
    public static String BANK_TYPE_LIQUIDATION = "LIQUIDATION";
    public static String ACCOUNT_TYPE_SAVING = "Saving Account";
    /*
     * G L O B A L      S E A R C H
     */
    public static String GLOBAL_SEARCH_USING_LOGINID = "LOGINID";
    public static String GLOBAL_SEARCH_USING_MSISDN = "MSISDN";
    public static String GLOBAL_SEARCH_USING_FIRSTNAME = "FIRSTNAME";
    public static String GLOBAL_SEARCH_USING_LASTNAME = "LASTNAME";

    // SMS Configuration
    public static String GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION = "DATEOFMODIFICATION";
    public static String GLOBAL_SEARCH_USING_LAST_TRANSACTION = "LASTTRANSACTION";
    public static String GLOBAL_SEARCH_USING_LAST_LOGIN = "LASTLOGIN";
    public static String GLOBAL_SEARCH_USING_DATE_OF_CREATION = "DATEOFCREATION";
    /*
     * S A V I N G S   C L U B
     */
    public static String CLUB_TYPE_BASIC = "Basic";
    public static String CLUB_TYPE_PREMIUM = "Premium";
    public static String STATUS_CLUB_ACTIVE = "Y";
    public static String CM_TRANSFER_REASON_DIED = "died";
    public static String CM_TRANSFER_REASON_RESIGNED = "resigned";
    public static String DISBURSEMENT_APPROVE = "1";
    public static String DISBURSEMENT_REJECT = "0";
    /**
     * A P P R O V A L   S T A T U S
     */
    public static String APPROVED = "A";
    public static String APPROVAL_INITIATED = "AI";
    /**
     * B U L K
     */
    public static String BULK_SUB_REG = "BulkSubRegId";
    public static String BULK_CH_USER_REG = "BulkUserRegId";
    /**
     * B U L K   B A N K A S S O C I A T I O N
     */
    public static String BULK_BANK_ASSOC = "BulkUserBankAssociation";
    public static String BULK_BANK_ASSOC_ECONET = "BulkUserBankAssociationEconet";
    public static String paymentID = "12345678";
    /**
     * SHOW DETAIL TYPE
     */

    public static String SHOW_USER_DETAILS = "showUserDetails";
    public static String SHOW_USER_ACCESS_DETAILS = "showUserAccessDetail";
    public static String SHOW_TRANS_CONTROL = "showTrnsControlUnits";
    public static String SHOW_USER_BALANCE = "showBalance";
    public static String NETWORK_STATUS = "Automation Status";
    public static String TXN_SUCCESS_STRING = "Transaction Success";
    public static String TXN_FAIL_STRING = "Transaction Failed";
    public static String TXN_STATUS_SUCCESS = "TS";
    public static String TXN_STATUS_INITIATED = "TI";
    public static String TXN_STATUS_FAIL = "TF";
    public static String TXN_STATUS_APPROVED1 = "A1";
    public static String TXN_STATUS_AMBIGUOUS = "TA";
    public static String O2C_SERVICE_TYPE = "O2C";
    public static String GRADE_ALL = "ALL";
    public static String GRADE_BANKSUBS = "BANKSUBS";
    public static String TXNSTATUS = "00066";
    public static String TRANSACTION_CORR_CASHIN = "CASHIN";
    public static String TRANSACTION_CORR_CASHOUT = "CASHOUT";

    public static String AUT_PSEUDO_CATEGORY = "AUTPsCat1";
    public static String AUT_DOMAIN ="AUTDOM";

}
