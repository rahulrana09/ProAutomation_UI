package framework.util.globalConstant;

/**
 * Created by rahul.rana on 5/24/2017.
 */
public class MessageCodes {
    public static final String O2C_PAYER_SUCCESS_MSG_CODE = null;
    public static final String O2C_PAYEE_SUCCESS_MSG_CODE = "1007";
    public static final String C2C_PAYER_SUCCESS_MSG_CODE = "9013";
    public static final String C2C_PAYEE_SUCCESS_MSG_CODE = "7004N";
    public static final String CASHIN_PAYER_SUCCESS_MSG_CODE = "1019";
    public static final String CASHIN_PAYEE_SUCCESS_MSG_CODE = "1002";

    public static final String CASHIN_PAYER_SUCCESS_MSG_CODE_ONATEL = "1002";
    public static final String CASHIN_PAYEE_SUCCESS_MSG_CODE_ONATEL = "1002_payee";

    public static final String C2C_PAYER_SUCCESS_MSG_CODE_ONATEL = "9050010";
    public static final String C2C_PAYEE_SUCCESS_MSG_CODE_ONATEL = "9050020";


    public static final String CASHOUT_PAYER_SUCCESS_MSG_CODE = "9012";
    public static final String CASHOUT_PAYEE_SUCCESS_MSG_CODE = "7005";

    public static final String CASHOUT_PAYER_SUCCESS_MSG_CODE_ONATEL = "9012_payer";
    public static final String CASHOUT_PAYEE_SUCCESS_MSG_CODE_ONATEL = "9012_payee";

    public static final String CASHOUT_INIT_PAYER_SUCCESS_MSG_CODE = "9054";
    public static final String CASHOUT_INIT_PAYER_SUCCESS_MSG_CODE_ONATEL = "9054_payer";

    public static final String CASHOUT_INIT_PAYEE_SUCCESS_MSG_CODE = "7005";//TODO
    public static final String CASHOUT_INIT_PAYEE_SUCCESS_MSG_CODE_ONATEL = "9054_payee";

    public static final String COMMISSION_DISBURSE_SUCCESS_MSG_CODE = "80001";

    public static final String SVA_TO_OWN_BANK_SUCCESS_MSG_CODE = "WALTOBANKON";

    public static final String COMMISSION_DIS_PAYER_SUCCESS_MSG_CODE = "80001";
    public static final String COMMISSION_DIS_PAYEE_SUCCESS_MSG_CODE = "null";
}
