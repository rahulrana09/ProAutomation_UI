/*
 * ******************************************************************************
 *  * COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  * <p>
 *  * This software is the sole property of Comviva and is protected by copyright
 *  * law and international treaty provisions. Unauthorized reproduction or
 *  * redistribution of this program, or any portion of it may result in severe
 *  * civil and criminal penalties and will be prosecuted to the maximum extent
 *  * possible under the law. Comviva reserves all rights not expressly granted.
 *  * You may not reverse engineer, decompile, or disassemble the software, except
 *  * and only to the extent that such activity is expressly permitted by
 *  * applicable law notwithstanding this limitation.
 *  * <p>
 *  * THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  * EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  * WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  * YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  * Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  * USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  * OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 */

package framework.util.propertiesManagement;


import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;

import static com.jayway.restassured.RestAssured.with;

public final class MfsTestUtils {

    public static final String MONEY_SERVER_URI_PROPERTY = "money.server.uri";
    public static final String MONEY_SERVER_OLD_URI_PROPERTY = "money.server.oldUri";
    public static final String EC_SEARCH_SERVER_URI_PROPERTY = "elastic.search.uri";
    public static final String EC_SEARCH_QUERY_BODY_PROPERTY = "elastic.search.queryBodyFile";
    public static final String MONEY_PROXY_URI_PROPERTY = "money.proxy.uri";
    public static final String COMPONENTS = "components";
    public static final String DEFAULT_EL_SEARCH_QUERY_BODY = "el_search_notify_query.json";
    public static final String NOTIFICATION_SIMULATOR_URI_PROPERTY = "notification.simulator.uri";
    public static final String API_MANAGER_URI_PROPERTY = "api.manager.uri";
    public static final String INTERNAL_API_SECURITY_PASSWORD_PROPERTY = "internal.api.security.user.password";
    public static final String INTERNAL_API_SECURITY_USERNAME_PROPERTY = "internal.api.security.user.name";
    public static final String MFS_TENANT_ID = "mfs.tenant.id";
    private static final MfsTestProperties properties = MfsTestProperties.getInstance();
    private static final ComponentsProperties componentProperties = ComponentsProperties.getInstance();

    static {
        properties.init();
        //componentProperties.init();
    }

    private MfsTestUtils() {
    }

    public static String elasticSearchBody() {
        final String bodyFile = properties.getProperty(EC_SEARCH_QUERY_BODY_PROPERTY,
                DEFAULT_EL_SEARCH_QUERY_BODY);
        final InputStream bodyStream = MfsTestUtils.class.getResourceAsStream("/" + bodyFile);
        if (bodyStream == null) {
            throw new IllegalArgumentException("Failed to load Elastic Search query body File: " + bodyFile);
        }
        try {
            return IOUtils.toString(bodyStream);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to load Elastic Search query body File: " + bodyFile, e);
        }
    }

    public static RequestSpecification elasticSerachUri() {
        final RestAssuredConfig restAssuredConfig = new RestAssuredConfig();
        RequestSpecification requestSpec = with().config(restAssuredConfig).baseUri(properties.getProperty(EC_SEARCH_SERVER_URI_PROPERTY));
        String proxyUri = properties.getProperty(MONEY_PROXY_URI_PROPERTY);
        if (StringUtils.isNotBlank(proxyUri)) {
            requestSpec.proxy(proxyUri);
        }
        return requestSpec.accept(ContentType.JSON).log().all();
    }

    public static RequestSpecification moneyOldUri() {

        RequestSpecification requestSpec = with().baseUri(properties.getProperty(MONEY_SERVER_OLD_URI_PROPERTY));
        String proxyUri = properties.getProperty(MONEY_PROXY_URI_PROPERTY);
        if (StringUtils.isNotBlank(proxyUri)) {
            requestSpec.proxy(proxyUri);
        }
        return requestSpec.accept(ContentType.JSON).log().all();
    }

    public static RequestSpecification moneyUri() {
        RequestSpecification requestSpec = with().baseUri(properties.getProperty(MONEY_SERVER_URI_PROPERTY));
        String proxyUri = properties.getProperty(MONEY_PROXY_URI_PROPERTY);
        if (StringUtils.isNotBlank(proxyUri)) {
            requestSpec.proxy(proxyUri);
        }
        return requestSpec.accept(ContentType.JSON).log().all();
    }

    public static RequestSpecification stubUri() {
        RequestSpecification requestSpec = with().baseUri(properties.getProperty(MONEY_SERVER_URI_PROPERTY));
        String proxyUri = properties.getProperty(MONEY_PROXY_URI_PROPERTY);
        if (StringUtils.isNotBlank(proxyUri)) {
            requestSpec.proxy(proxyUri);
        }
        return requestSpec.accept(ContentType.JSON).log().all();
    }

    public static String getTenantId() {
        return properties.getProperty(MFS_TENANT_ID);
    }

    public static RequestSpecification apiManagerUri() {
        RequestSpecification requestSpec = with().baseUri(properties.getProperty(API_MANAGER_URI_PROPERTY));
        String proxyUri = properties.getProperty(MONEY_PROXY_URI_PROPERTY);
        if (StringUtils.isNotBlank(proxyUri)) {
            requestSpec.proxy(proxyUri);
        }
        return requestSpec.accept(ContentType.JSON).log().all();
    }

    public static RequestSpecification moneyUriInternalApi() {
        return moneyUri().auth().preemptive().basic(getInternalApiUsername(), getInternalApiPassword());
    }

    public static RequestSpecification moneyUriStubApi() {
        return moneyUri().auth().preemptive().basic(getInternalApiUsername(), getInternalApiPassword());
    }

    public static RequestSpecification imsURI() {
        return moneyUri();
    }

    public static String getInternalApiPassword() {
        return properties.getProperty(INTERNAL_API_SECURITY_PASSWORD_PROPERTY);
    }

    public static String getInternalApiUsername() {
        return properties.getProperty(INTERNAL_API_SECURITY_USERNAME_PROPERTY);
    }

    public static String allcomponents() {
        //RequestSpecification requestSpec = with().baseUri(properties.getProperty(MONEY_SERVER_URI_PROPERTY));
        String components = componentProperties.getProperty(COMPONENTS);
        return components;
    }

    public static RequestSpecification notificationSimulatorUri() {
        RequestSpecification requestSpec = with().baseUri(properties.getProperty(NOTIFICATION_SIMULATOR_URI_PROPERTY));
        return requestSpec.accept(ContentType.JSON).log().all();
    }

}
