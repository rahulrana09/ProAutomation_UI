package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jayway.restassured.response.ValidatableResponse;
import framework.entity.User;
import framework.features.common.Login;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import framework.util.propertiesManagement.MessageReader;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Dalia on 30-05-2017.
 * Updated by automation team
 */
public class Assertion extends Throwable {

    public static SoftAssert sAssert;

    public static void markTestAsFailure(String message, ExtentTest t1) {
        t1.fail(message);
        try {
            t1.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.fail("Test failed stopping execution!");
    }

    public static void markAsFailure(String message) {
        getSoftAssert().fail(message);
    }

    public static SoftAssert getSoftAssert() {
        if (sAssert == null) {
            sAssert = new SoftAssert();
        }
        return sAssert;
    }

    public static void finalizeSoftAsserts() {
        try {
            getSoftAssert().assertAll();
        } catch (Exception e) {
            ;
        } finally {
            resetSoftAsserts();
        }
    }

    public static void resetSoftAsserts() {
        if (sAssert == null) {
            return;
        } else {
            sAssert = null;
        }
    }

    public static void failAndStopTest(String message, ExtentTest t1) {
        t1.fail(message);
        markAsFailure(message);
        Assert.fail(message);
    }

    /**
     * Get Action Message
     *
     * @return
     */
    public static String getActionMessage() throws NoSuchElementException {
        new WebDriverWait(DriverFactory.getDriver(), Constants.EXPLICIT_WAIT_TIME).until(ExpectedConditions.elementToBeClickable(DriverFactory.getDriver().findElement(By.className("actionMessage"))));
        return DriverFactory.getDriver().findElement(By.className("actionMessage")).getText();
    }

    public static void verifyResponseSucceeded(ValidatableResponse response, ExtentTest t1) throws IOException {
        t1.info("Message: " + response.extract().jsonPath().getString("message"));
        t1.info("TransactionId: " + response.extract().jsonPath().getString("transactionId"));
        verifyEqual(response.extract().jsonPath().getString("status"), "SUCCEEDED", "Verify response Succeeded", t1);
    }

    public static void verifyResponsePaused(ValidatableResponse response, ExtentTest t1) throws IOException {
        t1.info("Message: " + response.extract().jsonPath().getString("message"));
        t1.info("TransactionId: " + response.extract().jsonPath().getString("transactionId"));
        verifyEqual(response.extract().jsonPath().getString("status"), "PAUSED", "Verify response Succeeded", t1);
    }

    public static void verifyResponseFailed(ValidatableResponse response, ExtentTest t1) throws IOException {
        t1.info("Message: " + response.extract().jsonPath().getString("message"));
        t1.info("TransactionId: " + response.extract().jsonPath().getString("transactionId"));
        verifyEqual(response.extract().jsonPath().getString("status"), "FAILED", "Verify response Failed", t1);
    }


    public static List<String> getAllActionMessages() {
        List<String> actionList = new ArrayList<>();
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 12);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("actionMessage")));
            List<WebElement> actions = DriverFactory.getDriver().findElements(By.className("actionMessage"));
            for (int i = 0; i < actions.size(); i++) {
                actionList.add(actions.get(i).getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
            markAsFailure("Failed to get Action Message Message");
        }

        return actionList;
    }

    public static void alertAssertionDynamic(String msgCode, String para, ExtentTest pNode) {
        try {
            pNode.info("Check Alert");
            String msg = DriverFactory.getDriver().switchTo().alert().getText();
            DriverFactory.getDriver().switchTo().alert().accept();
            pNode.info("Accept Alert : " + msg);
            String expectedMsg = MessageReader.getDynamicMessage(msgCode, para);
            String code = null;
            Markup m = null;
            if (msg.contains(expectedMsg)) {
                code = "Verified Successfully - " + ":\nExpected: " + expectedMsg + "\nActual  : " + msg;
                m = MarkupHelper.createCodeBlock(code);
                pNode.pass(m);
                pNode.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            } else {
                code = "Verified Not Successfully - " + ":\nExpected: " + expectedMsg + "\nActual  : " + msg;
                m = MarkupHelper.createCodeBlock(code);
                pNode.fail(m);
                pNode.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            }
        } catch (Exception e) {
            pNode.fail("Alert Not Found");
        }

    }

    public static void alertAssertion(String msgCode, ExtentTest pNode) {
        try {

            Utils.putThreadSleep(NumberConstants.SLEEP_2000);
            pNode.info("Check Alert");
            String actual = DriverFactory.getDriver().switchTo().alert().getText();
            pNode.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshotWithoutDriver()).build());

            DriverFactory.getDriver().switchTo().alert().accept();
            pNode.info("Accept Alert : " + actual);
            String expectedMsg = MessageReader.getMessage(msgCode, null);

            Markup m = null;
            String code[][] = {{"Verify", "Alert Assertion"}, {"Actual", actual}, {"Expected", expectedMsg}};
            if (actual.contains(expectedMsg)) {
                //code = "Verified Successfully - " + ":\nExpected: " + expectedMsg + "\nActual : " + msg;
                m = MarkupHelper.createTable(code);
                pNode.pass(m);

            } else {
                //code = "Verification Failed - " + ":\nExpected: " + expectedMsg + "\nActual  : " + msg;
                m = MarkupHelper.createTable(code);
                pNode.fail(m);
            }
        } catch (Exception e) {
            pNode.fail("Alert Not Found...");
        }

    }

    public static List<String> getAllErrorMessages() throws NoSuchElementException {
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        List<String> errorList = new ArrayList<>();
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 5);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("errorMessage")));
            List<WebElement> actions = DriverFactory.getDriver().findElements(By.className("errorMessage"));
            for (int i = 0; i < actions.size(); i++) {
                errorList.add(actions.get(i).getText());
            }
        } catch (Exception e) {
            e.printStackTrace();
            markAsFailure("Exception: while fetching all Error message");
        }

        return errorList;
    }

    /**
     * returns empty list if no error is encoundtered
     *
     * @return
     * @throws NoSuchElementException
     */
    public static List<String> getAllExpectedErrorMessages() throws NoSuchElementException {
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        List<String> errorList = new ArrayList<>();
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 5);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("errorMessage")));
            List<WebElement> actions = DriverFactory.getDriver().findElements(By.className("errorMessage"));
            for (int i = 0; i < actions.size(); i++) {
                errorList.add(actions.get(i).getText());
            }
        } catch (Exception e) {
            ;
        }

        return errorList;
    }

    public static String getErrorMessage() throws Exception {
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        try {
            return DriverFactory.getDriver().findElement(By.className("errorMessage")).getText();
        } catch (Exception e) {
            e.printStackTrace();
            markAsFailure("Exception: while fetching Error message");
        }
        return null;
    }

    public static String expectErrorInPage() throws Exception {
        try {
            return DriverFactory.getDriver().findElement(By.className("errorMessage")).getText();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Verify Message Contains
     *
     * @param actual
     * @param expected
     * @param node
     * @throws IOException
     */
    public static boolean verifyMessageContain(String actual, String expected, String message, ExtentTest node, String... variation) throws IOException {
        String expectedMsg;
        if (variation.length > 0) {
            expectedMsg = MessageReader.getDynamicMessage(expected, variation);
        } else {
            expectedMsg = MessageReader.getMessage(expected, null);
        }
        String code[][] = {{"Verify", message}, {"Actual", actual}, {"Expected", expectedMsg}};
        Markup m = MarkupHelper.createTable(code);
        if (actual.contains(expectedMsg)) {
            node.pass(m);
            node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            return true;
        } else {
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            markAsFailure("Failure: Verify Message Contains! refer reports");
            return false;
        }
    }

    /**
     * Assert Message Contains - exit execution on failure
     *
     * @param actual
     * @param expected
     * @param node
     * @throws IOException
     */
    public static void assertMessageContain(String actual, String expected, String message, ExtentTest node) throws IOException {
        String expectedMsg = MessageReader.getMessage(expected, null);
        String code[][] = {{"Verify", message}, {"Actual", actual}, {"Expected", expectedMsg}};
        Markup m = MarkupHelper.createTable(code);
        if (actual.contains(expectedMsg)) {
            node.pass(m);
            node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } else {
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Assert.fail();
        }
    }

    /**
     * Verify That a Specific Comparison is Not true
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void verifyNotEqual(Object actual, Object expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);

        boolean isEqual = false;
        if (actual instanceof Integer && expected instanceof Integer) {
            isEqual = actual == expected;
        } else if (actual instanceof String && expected instanceof String) {
            isEqual = actual.equals(expected);
        }

        if (!isEqual) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    /**
     * Verify For Equal
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static boolean verifyEqual(Object actual, Object expected, String message, ExtentTest node, boolean... takeScreenShot) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);

        boolean isEqual = false;
        boolean takeSnap = takeScreenShot.length > 0 ? takeScreenShot[0] : false;
        if (actual instanceof BigDecimal && expected instanceof BigDecimal) {
            BigDecimal act = (BigDecimal) actual;
            BigDecimal exp = (BigDecimal) expected;
            isEqual = (act.compareTo(exp) == 0);
        } else if (actual instanceof Integer && expected instanceof Integer) {
            isEqual = actual == expected;
        } else if (actual instanceof String && expected instanceof String) {
            isEqual = actual.equals(expected);
        } else if (actual instanceof Boolean && expected instanceof Boolean) {
            isEqual = actual == expected;
        }

        if (isEqual) {
            node.pass(m);
            if (takeSnap)
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            return true;
        } else {
            node.fail(m);
            if (takeSnap)
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            markAsFailure("Failed to verify for equality!");
        }
        return false;
    }

    public static boolean isEqual(String actual, String expected) throws IOException {
        if(actual.equalsIgnoreCase(expected))
            return true;

        return false;
    }

    public static boolean assertEqual(Object actual, Object expected, String message, ExtentTest node, boolean... takeScreenShot) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isEqual = false;

        boolean takeSnap = takeScreenShot.length > 0 ? takeScreenShot[0] : true;
        if (actual instanceof BigDecimal && expected instanceof BigDecimal) {
            isEqual = actual.equals(expected);
        } else if (actual instanceof Integer && expected instanceof Integer) {
            isEqual = actual == expected;
        } else if (actual instanceof String && expected instanceof String) {
            isEqual = actual.equals(expected);
        } else if (actual instanceof Boolean && expected instanceof Boolean) {
            isEqual = actual == expected;
        }

        if (isEqual) {
            node.pass(m);

            if (takeSnap)
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            return true;
        } else {
            node.fail(m);

            if (takeSnap)
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            Assert.fail("Not Equal");// fail the test
        }
        getSoftAssert().assertEquals(actual, expected);
        return false;
    }

    public static boolean verifyContains(String actual, String expected, String message, ExtentTest node, boolean... takeScreenShot) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isContain = false;

        boolean captureScreen = (takeScreenShot.length > 0) ? takeScreenShot[0] : true;
        if (actual.contains(expected)) {
            isContain = true;
        }

        if (isContain) {
            node.pass(m);
            if (captureScreen)
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            return true;
        } else {
            node.fail(m);
            if (captureScreen)
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            markAsFailure("Failure: Verify Contain: Actual-" + actual + "; Expected-" + expected);
        }
        return false;
    }

    public static boolean verifyNotContains(String actual, String expected, String message, ExtentTest node, boolean... takeScreenShot) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isContain = false;

        boolean captureScreen = (takeScreenShot.length > 0) ? takeScreenShot[0] : true;
        if (actual.contains(expected)) {
            isContain = true;
        }

        if (!isContain) {
            node.pass(m);
            if (captureScreen)
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            return true;
        } else {
            node.fail(m);
            if (captureScreen)
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            markAsFailure("Failure: Verify Not Contain: Actual-" + actual + "; Expected-" + expected);
        }
        return false;
    }

    public static boolean verifyListContains(List actual, String expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify List Contains: ", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isContain = false;

        if (actual.contains(expected)) {
            isContain = true;
        }

        if (isContain) {
            node.pass(m);
            return true;
        } else {
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            markAsFailure("Failure: Verify List Contain: Actual-" + actual + "; Expected-" + expected);
        }
        return false;
    }

    public static boolean verifyListNotContains(List actual, String expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify List Not Contains: ", message}, {"Actual", actual.toString()}, {"Not Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isContain = false;

        if (actual.contains(expected)) {
            isContain = true;
        }

        if (isContain) {
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            return false;
        } else {
            node.pass(m);
            return true;
        }
    }

    public static void verifyIsGreater(int first, int second, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify", message}, {"First", String.valueOf(first)}, {"Second", String.valueOf(second)}};
        Markup m = MarkupHelper.createTable(code);

        if (first > second) {
            node.pass(m);
        } else {
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            if (ConfigInput.generateServerLog) {
                String filename = CMDExecutor.getCatalinaLog();
                node.log(Status.FAIL, "<b> <h6><font color='blue'>Application Log File Path:</b><a href='" + filename + "'><b><h6><font color='red'> " + filename + "</font></h6></b></a>");
            }
            markAsFailure("Failure: Refer reports for more details");
        }
    }

    /**
     * Assert Not Equal
     *
     * @param actual
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void assertNotEqual(Object actual, Object expected, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        boolean isEqual = false;
        if (actual instanceof Integer && expected instanceof Integer) {
            isEqual = actual == expected;
        } else if (actual instanceof String && expected instanceof String) {
            isEqual = actual.equals(expected);
        }
        if (!isEqual) {
            node.pass(m);
        } else {
            node.fail(m);
            if (ConfigInput.generateServerLog) {
                String filename = CMDExecutor.getCatalinaLog();
                node.log(Status.FAIL, "<b> <h6><font color='blue'>Application Log File Path:</b><a href='" + filename + "'><b><h6><font color='red'> " + filename + "</font></h6></b></a>");
            }
            Assert.fail();
        }
    }

    /**
     * Verify Action Message
     *
     * @param expected
     * @param message
     * @param node
     * @param variation - for dynamic message
     * @return
     * @throws Exception
     */
    public static Boolean verifyActionMessageContain(String expected, String message, ExtentTest node, String... variation) throws Exception {
        try {

            String actual = "";
            boolean isFound = false;
            String expectedMsg = null;

            if (variation.length > 0) {
                expectedMsg = MessageReader.getDynamicMessage(expected, variation);
            } else {
                expectedMsg = MessageReader.getMessage(expected, null);
            }

            List<String> actionMessages = getAllActionMessages();
            for (int i = 0; i < actionMessages.size(); i++) {
                actual = actual + " " + actionMessages.get(i);
                if (actionMessages.get(i).contains(expectedMsg)) {
                    isFound = true;
                    actual = actionMessages.get(i);
                    break;
                }
            }

            String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
            Markup m = MarkupHelper.createTable(code);

            if (isFound) {
                node.pass(m);
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                return true;
            } else {
                Login.resetLoginStatus();
                node.fail(m);
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                markAsFailure("Failure: Verify Action message Contain:- " + expectedMsg);
                return false;
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, node);
            e.printStackTrace();
        }
        return false;
    }

    public static void assertFail(Exception e, String msg, ExtentTest node) {
        Login.resetLoginStatus();
        e.printStackTrace();
        node.fail(msg);
        Assert.fail(msg);
    }


    /**
     * Assert Action Message - it's similar to Verify Action message in addition it just exit the run
     *
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static void assertActionMessageContain(String expected, String message, ExtentTest node) throws Exception {
        String actual = "";
        boolean isFound = false;

        String expectedMsg = MessageReader.getMessage(expected, null);
        List<String> actionMessages = getAllActionMessages();
        for (int i = 0; i < actionMessages.size(); i++) {
            actual = actual + " " + actionMessages.get(i);
            if (actionMessages.get(i).contains(expectedMsg)) {
                isFound = true;
                actual = actionMessages.get(i);
                break;
            }
        }

        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (isFound) {
            node.pass(m);
            node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } else {
            Login.resetLoginStatus();
            node.fail(m);
            node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Assert.fail("Failed to verify the action message contains: " + expectedMsg);
        }
    }

    /**
     * Verify Error Message
     *
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static Boolean verifyErrorMessageContain(String expected, String message, ExtentTest node, String... variation) throws Exception {

        String actual = "";
        boolean isFound = false;
        String expectedMsg = null;

        if (variation.length > 0) {
            expectedMsg = MessageReader.getDynamicMessage(expected, variation);
        } else {
            expectedMsg = MessageReader.getMessage(expected, null);
        }

        List<String> errorMessages = getAllErrorMessages();
        for (int i = 0; i < errorMessages.size(); i++) {
            actual = actual + " " + errorMessages.get(i);
            if (errorMessages.get(i).contains(expectedMsg)) {
                isFound = true;
                actual = errorMessages.get(i);
                break;
            }
        }

        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);


        if (isFound) {
            node.pass(m);
            node.pass("Passed Screenshot", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            return true;
        } else {
            node.fail(m);
            node.fail(message, MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            markAsFailure("Failure: Verify Error message Contain:- " + expectedMsg);
            return false;
        }
    }

    public static Boolean isErrorMessageExists(String expected, String message, ExtentTest node, String... variation) throws Exception {

        String actual = "";
        boolean isFound = false;
        String expectedMsg = null;

        if (variation.length > 0) {
            expectedMsg = MessageReader.getDynamicMessage(expected, variation);
        } else {
            expectedMsg = MessageReader.getMessage(expected, null);
        }

        List<String> errorMessages = getAllExpectedErrorMessages();
        if (errorMessages.size() == 0)
            return false;


        for (int i = 0; i < errorMessages.size(); i++) {
            actual = actual + " " + errorMessages.get(i);
            if (errorMessages.get(i).contains(expectedMsg)) {
                isFound = true;
                actual = errorMessages.get(i);
                break;
            }
        }

        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (isFound) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verify Error Message
     *
     * @param expected
     * @param message
     * @param node
     * @throws IOException
     */
    public static Boolean verifyDynamicErrorMessageContain(String expected, String message, ExtentTest node) throws Exception {
        String code = null;
        Markup m = null;
        String actual = "";
        boolean isFound = false;

        try {

            List<String> errorMessages = getAllErrorMessages();
            for (int i = 0; i < errorMessages.size(); i++) {
                actual = actual + " " + errorMessages.get(i);
                if (errorMessages.get(i).contains(expected)) {
                    isFound = true;
                    actual = errorMessages.get(i);
                    break;
                }
            }

            if (isFound) {
                code = "Verified Error Successfully - " + message + "\nExpected: " + expected + "\nActual  : " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                node.pass(m);
                return true;
            } else {
                code = "Verification of Error Fail - " + message + ":\nExpected: " + expected + "\nActual: " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.fail(m);
                node.fail(message, MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                markAsFailure("Failure: Refer reports for more details");
                return false;
            }
        } catch (IOException ioe) {
            System.err.print(ioe);

        }
        return false;
    }

    /**
     * Method to verify Dynamic Action Message
     *
     * @param expected
     * @param message
     * @param node
     * @return
     * @throws NoSuchElementException
     */
    public static Boolean verifyDynamicActionMessageContain(String expected, String message, ExtentTest node) throws Exception {
        String code = null;
        Markup m = null;
        String actual = "";
        boolean isFound = false;

        try {

            List<String> actionMessages = getAllActionMessages();
            for (int i = 0; i < actionMessages.size(); i++) {
                actual = actual + " " + actionMessages.get(i);
                if (actionMessages.get(i).contains(expected)) {
                    isFound = true;
                    actual = actionMessages.get(i);
                    break;
                }
            }

            if (isFound) {
                code = "Verified Error Successfully - " + message + "\nExpected: " + expected + "\nActual  : " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                node.pass(m);
                return true;
            } else {
                code = "Verification of Error Fail - " + message + ":\nExpected: " + expected + "\nActual: " + actual;
                m = MarkupHelper.createCodeBlock(code);
                node.fail(m);
                node.fail(message, MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
                markAsFailure("Failure: Refer reports for more details");
                return false;
            }
        } catch (IOException ioe) {
            System.err.print(ioe);

        }
        return false;
    }

    /**
     * Verify Message Equals
     *
     * @param expected
     * @param actual
     * @param node
     * @throws IOException
     */
    public static void verifyMessageEquals(String actual, String expected, String message, ExtentTest node) throws Exception {
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expected.toString()}};
        Markup m = MarkupHelper.createTable(code);
        String expectedMsg = MessageReader.getMessage(expected, null);
        if (actual.equals(expectedMsg)) {
            node.pass(m);
        } else {
            node.fail(m);
            node.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        }
    }

    public static void raiseExceptionAndStop(Exception e, ExtentTest node) throws IOException {
        node.fail(e);
        node.error("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        if (ConfigInput.generateServerLog) {
            String filename = CMDExecutor.getCatalinaLog();
            node.log(Status.FAIL, "<b> <h6><font color='blue'>Application Log File Path:</b><a href='" + filename + "'><b><h6><font color='red'> " + filename + "</font></h6></b></a>");
        }
        e.printStackTrace();
        Assert.fail("Error. Stopping the test execution. Message:- " + e.getMessage());
    }

    public static void raiseExceptionAndContinue(Exception e, ExtentTest node) throws IOException {
        e.printStackTrace();
        node.fail(e);
        node.error("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        if (ConfigInput.generateServerLog) {
            String filename = CMDExecutor.getCatalinaLog();
            node.log(Status.FAIL, "<b> <h6><font color='blue'>Application Log File Path:</b><a href='" + filename + "'><b><h6><font color='red'> " + filename + "</font></h6></b></a>");
        }
        markAsFailure("Error: Refer reports for more details. Message:- " + e.getMessage());
    }

    /**
     * Check if there is any error in the page
     *
     * @return
     */
    public static boolean isErrorInPage(ExtentTest node) throws IOException {
        Utils.captureScreen(node);
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 2);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.className("errorMessage")));
            return true;
        } catch (Exception e) {
            ;
        }

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.className("error_Message")));
            return true;
        } catch (Exception e) {
            ;
        }

        node.pass("No error in the Page!");
        return false;

    }

    /**
     * Assert if there is Error and Stop the execution
     *
     * @param node
     * @return
     * @throws IOException
     */
    public static void assertErrorInPage(ExtentTest node) throws IOException {
        Utils.captureScreen(node);
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 2);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.className("errorMessage")));
            node.fail("Error: Refer ScreenShot");
        } catch (Exception e) {
            ;
        }

        try {
            wait.until(ExpectedConditions.elementToBeClickable(By.className("error_Message")));
            node.fail("Error: Refer ScreenShot");
        } catch (Exception e) {
            ;
        }

        node.pass("No error in the Page!");
    }


    public static Boolean checkActionMessageContain(String expected, String message, ExtentTest node) throws Exception {
        String code = null;
        Markup m = null;
        String actual = "";
        boolean isFound = false;

        String expectedMsg = MessageReader.getMessage(expected, null);
        List<String> actionMessages = getAllActionMessages();
        for (int i = 0; i < actionMessages.size(); i++) {
            actual = actual + ", " + actionMessages.get(i);
            if (actionMessages.get(i).contains(expectedMsg)) {
                isFound = true;
                actual = actionMessages.get(i);
                break;
            }
        }

        if (isFound) {
            code = "Verified Successfully - " + message + ":\nExpected: " + expectedMsg + "\nActual  : " + actual;
            m = MarkupHelper.createCodeBlock(code);
            node.pass(m);
            return true;
        } else {
            return false;
        }
    }


    public static Boolean checkErrorMessageContain(String expected, String message, ExtentTest node) throws IOException {
        String code = null;
        Markup m = null;
        String actual = "";
        boolean isFound = false;

        String expectedMsg = MessageReader.getMessage(expected, null);
        List<String> actionMessages = getAllErrorMessages();
        for (int i = 0; i < actionMessages.size(); i++) {
            actual = actual + ", " + actionMessages.get(i);
            if (actionMessages.get(i).contains(expectedMsg)) {
                isFound = true;
                actual = actionMessages.get(i);
                break;
            }
        }

        if (isFound) {
            code = "Verified Successfully - " + message + ":\nExpected: " + expectedMsg + "\nActual  : " + actual;
            m = MarkupHelper.createCodeBlock(code);
            node.pass(m);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Log Info
     *
     * @param info
     */
    public static void logAsPass(String info, ExtentTest node) {
        Markup m = MarkupHelper.createCodeBlock(info);
        node.pass(m);
    }

    public static void logAsInfo(String info, ExtentTest node) {
        Markup m = MarkupHelper.createCodeBlock(info);
        node.info(m);
    }

    /**
     * For attaching Screenshot and Logs without failing the test case
     *
     * @param node
     * @throws IOException
     */
    public static void attachScreenShotAndLogs(ExtentTest node) throws IOException {
        node.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        if (ConfigInput.generateServerLog) {
            String filename = CMDExecutor.getCatalinaLog();
            node.log(Status.FAIL, "<b> <h6><font color='blue'>Application Log File Path:</b><a href='" + filename + "'><b><h6><font color='red'> " + filename + "</font></h6></b></a>");
        }
    }

    public static String checkErrorPresent() throws NoSuchElementException {
        Utils.putThreadSleep(Constants.THREAD_SLEEP_1000);
        try {
            return DriverFactory.getDriver().findElement(By.className("errorMessage")).getText();
        } catch (NoSuchElementException ne) {
            return null;
        }
    }


    public static boolean verifyArrayEquals(Object[] actual, Object[] expected, String message, ExtentTest node, boolean... takeScreenShot) throws IOException {

        // String expectedCode="Expected:\n",actualCode="Actual:\n";
        String expectedCode = "", actualCode = "";

        for (int i = 0; i < expected.length; i++) {
            expectedCode = expectedCode + (i + 1) + "." + expected[i] + "\n";
        }

        for (int i = 0; i < actual.length; i++) {
            actualCode = actualCode + (i + 1) + "." + actual[i] + "\n";
        }

       /* Markup m = MarkupHelper.createCodeBlock(expectedCode);
        Markup m1 = MarkupHelper.createCodeBlock(actualCode);*/

        String code[][] = {{"Verify", message}, {"Actual", actualCode.toString()}, {"Expected", expectedCode.toString()}};
        Markup m = MarkupHelper.createTable(code);

       /* node.info(m);
        node.info(m1);*/


        boolean captureScreen = (takeScreenShot.length > 0) ? takeScreenShot[0] : true;
        if (Arrays.equals(actual, expected)) {
            node.pass(m);
            if (captureScreen)
                node.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            return true;
        } else {
            node.fail(m);
            if (captureScreen)
                node.fail("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            markAsFailure("Failure: Refer reports for more details");
        }
        return false;
    }

    /**
     * Verify Account - Bank /Wallet is Debited
     *
     * @param preBalance
     * @param postBalance
     * @param message
     * @param node
     * @return
     * @throws IOException
     */
    public static boolean verifyAccountIsDebited(BigDecimal preBalance, BigDecimal postBalance, BigDecimal amount, String message, ExtentTest node) throws IOException {
        String amt = (amount != null) ? amount.toString() : "N/A";
        String code[][] = {{"Verify", message}, {"PreBalance", preBalance.toString()}, {"PostBalance", postBalance.toString()}, {"Debited Amount", amt}};
        Markup m = MarkupHelper.createTable(code);

        boolean isDebited = false, isExact = false;

        isDebited = preBalance.compareTo(postBalance) > 0;

        if (amount != null && isDebited) {
            if (preBalance.subtract(postBalance).compareTo(amount) == 0) {
                isExact = true;
            }
           /* // check for the (prebalance - post balance) must be less than or equal to amount in range of minimum 5%
            BigDecimal range = amount.multiply(new BigDecimal(0.05));
            BigDecimal actualAmount = preBalance.subtract(postBalance);

            isDebited = preBalance.compareTo(postBalance) > 0 &&
                    amount.subtract(actualAmount).compareTo(new BigDecimal(0)) >= 0 &&
                    amount.subtract(actualAmount).compareTo(range) <= 0;*/
        }

        if (isDebited) {
            node.pass(m);
            return true;
        } else {
            node.fail(m);
            markAsFailure("Failed to verify that the account is Debited!");
        }
        if (!isExact) {
            node.warning("The Debited amount is not exactly same as requested amount - Please check pre/post Balance");
        }
        return false;
    }

    public static boolean verifyAccountIsCredited(BigDecimal preBalance, BigDecimal postBalance, BigDecimal amount, String message, ExtentTest node) throws IOException {
        String amt = (amount != null) ? amount.toString() : "N/A";
        String code[][] = {{"Verify", message}, {"PreBalance", preBalance.toString()}, {"PostBalance", postBalance.toString()}, {"Credited Amount", amt}};
        Markup m = MarkupHelper.createTable(code);

        boolean isCredited = false, isExact = false;
        isCredited = preBalance.compareTo(postBalance) < 0;

        if (amount != null && isCredited) {
            if (postBalance.subtract(preBalance).compareTo(amount) == 0) {
                isExact = true;
            }
           /* // check for the (prebalance - post balance) must be less than or equal to amount in range of minimum 5%
            BigDecimal range = amount.multiply(new BigDecimal(0.05));
            BigDecimal actualAmount = postBalance.subtract(preBalance);

            isCredited = preBalance.compareTo(postBalance) < 0 &&
                    amount.subtract(actualAmount).compareTo(new BigDecimal(0)) >= 0 &&
                    amount.subtract(actualAmount).compareTo(range) <= 0;*/
        }

        if (isCredited) {
            node.pass(m);
            return true;
        } else {
            node.fail(m);
            markAsFailure("Failed to verify that the account is Credited!");
        }
        if (!isExact) {
            node.warning("The Credited amount is not exactly same as requested amount - Please check pre/post Balance");
        }
        return false;
    }

    /**
     * Verify that acount is debited with service charge, assuming the max % to be 10
     *
     * @param preBalance
     * @param postBalance
     * @param amount
     * @param message
     * @param node
     * @return
     * @throws IOException
     */
    public static boolean verifyAccountIsDebitedWithServiceCharge(BigDecimal preBalance, BigDecimal postBalance, BigDecimal amount, String message, ExtentTest node) throws IOException {
        String code[][] = {{"Verify", message}, {"PreBalance", preBalance.toString()}, {"PostBalance", postBalance.toString()}, {"Debited Amount", amount.toString()}};
        Markup m = MarkupHelper.createTable(code);

        boolean isDebited = false;

        isDebited = preBalance.compareTo(postBalance) > 0;

        if (amount != null && isDebited) {
            // post balance must be deduced by the fraction of transaction amount i.e. the service charge
            BigDecimal range = amount.multiply(new BigDecimal(0.10));

            isDebited = preBalance.compareTo(postBalance) > 0 &&
                    preBalance.subtract(postBalance).compareTo(new BigDecimal(0)) >= 0 &&
                    preBalance.subtract(postBalance).compareTo(range) <= 0;
        }

        if (isDebited) {
            node.pass(m);
            return true;
        } else {
            node.fail(m);
            markAsFailure("Failed to verify that the account is Debited!");
        }
        return false;
    }

    public static boolean verifyAccountIsNotAffected(BigDecimal preBalance, BigDecimal postBalance, String message, ExtentTest node) throws IOException {

        String code[][] = {{"Verify", message}, {"PreBalance", preBalance.toString()}, {"PostBalance", postBalance.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (preBalance.compareTo(postBalance) == 0) {
            node.pass(m);
            return true;
        } else {
            node.fail(m);
            markAsFailure("Failed to verify that the account is not credited/ debited!");
        }
        return false;
    }

    /**
     * Verify DB Message Contains
     *
     * @param expected
     * @param actual
     * @param node
     * @throws IOException
     */
    public static void verifyDBMessageContains(String actual, String expected, String message, ExtentTest node, Object... params) throws Exception {
        String expectedMsg = MessageReader.getDBMessage(expected, params);
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);

        if (actual.contains(expectedMsg)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    public static void verifyDBMessageContains(User obj, String expected, String message, ExtentTest node, Object... params) throws Exception {
        Markup m = MarkupHelper.createLabel("DB-Assertion-Start-Marker", ExtentColor.BLUE);
        node.info(m);
        Thread.sleep(ConfigInput.additionalWaitTime);
        String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(obj.MSISDN, "desc");

        if (ConfigInput.isDBMessageEncrypted) {
            dbMessage = Utils.decryptMessage(dbMessage);
        }
        String expectedMsg = MessageReader.getDBMessage(expected, params);
        String code[][] = {{"Verify", message}, {"Actual", dbMessage.toString()}, {"Expected", expectedMsg.toString()}};
        m = MarkupHelper.createTable(code);

        if (dbMessage.contains(expectedMsg)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }

    public static void verifyDBMessageEquals(String actual, String expected, String message, ExtentTest node, Object... params) throws Exception {
        String expectedMsg = MessageReader.getDBMessage(expected, params);
        String code[][] = {{"Verify", message}, {"Actual", actual.toString()}, {"Expected", expectedMsg.toString()}};
        Markup m = MarkupHelper.createTable(code);
        if (actual.equalsIgnoreCase(expectedMsg)) {
            node.pass(m);
        } else {
            node.fail(m);
        }
    }


    public static void verifyListsAreEqual(List actualList, List expectedList, String message, ExtentTest node) {
        String code[][] = {{"Verify", message}, {"Actual", actualList.toString()}, {"Expected", expectedList.toString()}};
        Markup m = MarkupHelper.createTable(code);
        if (expectedList.containsAll(actualList) && expectedList.size() == actualList.size()) {
            node.pass(m);
        } else {
            node.fail(m);
        }

    }

    public static void verifyListContains(List expectedList, List actualList, String message, ExtentTest node) {

        String code[][] = {{"Verify", message}, {"Actual", actualList.toString()}, {"Expected", expectedList.toString()}};
        Markup m = MarkupHelper.createTable(code);
        if (actualList.containsAll(expectedList)) {
            node.pass(m);
        } else {
            node.fail(m);
        }

    }

    public static void verifyErrorLogFileForMessage(String messageCode, ExtentTest t1) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(), 'Download Error Log File')]/ancestor::tr[1]/td/a"))).click();
            Thread.sleep(5000);
            String newFile = FilePath.dirFileDownloads + Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            List<String> txtFile = new ArrayList<>();
            File file = new File(newFile);
            Scanner s = new Scanner(file);

            String expectedMsg = MessageReader.getDynamicMessage(messageCode);
            boolean isFound = false;
            while (s.hasNextLine()) {
                String line = s.nextLine();
                txtFile.add(line);
                if (line.contains(expectedMsg)) {
                    isFound = true;
                    break;
                }
            }
            s.close();

            t1.info(txtFile.toString());
            if (isFound)
                t1.pass("Successfully verified the message" + expectedMsg);
            else
                t1.fail("Failed to find the message: " + expectedMsg);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

}
