package framework.util.common;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.features.common.Login;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;

/**
 * Created by navin.pramanik on 8/22/2017.
 */
public class AlertHandle {

    private static WebDriver driver;

    /**
     *
     */
    public static boolean checkAlert() {
        driver = DriverFactory.getDriver();
        try {
            driver.switchTo().alert().getText();
            return true;
        } catch (NoAlertPresentException na) {
            /*na.printStackTrace();*/
            return false;
        }
    }

    /**
     * @param node
     * @author : navin.pramanik
     */
    public static void acceptAlert(ExtentTest node) {
        driver = DriverFactory.getDriver();
        try {
            driver.switchTo().alert().accept();
            node.debug("Accepting alert..");
        } catch (NoAlertPresentException na) {
            na.printStackTrace();
            node.warning("Alert not present");
        }
    }

    /**
     * @param node
     */
    public static void rejectAlert(ExtentTest node) {
        try {
            driver = DriverFactory.getDriver();
            driver.switchTo().alert().dismiss();
            node.info("Rejecting alert..");
        } catch (NoAlertPresentException na) {
            na.printStackTrace();
            node.warning("Alert not present");
        }
    }

    /**
     * @param node
     * @param text
     * @author : navin.pramanik
     */
    public static void setAlertText(ExtentTest node, String text) {
        try {
            driver = DriverFactory.getDriver();
            driver.switchTo().alert().sendKeys(text);
        } catch (NoAlertPresentException na) {
            node.warning("Alert not present");
        }
    }


    /**
     * To Handle the Alert on Page
     *
     * @param accept Pass true if want to Accept or false to Reject
     * @param node   Pass the current Node of Extent Test For Logging purpose
     */
    public static void handleAlert(boolean accept, ExtentTest node) {
        driver = DriverFactory.getDriver();
        if (accept) {
            driver.switchTo().alert().accept();
            node.debug("Accepting the Alert...");
        } else {
            driver.switchTo().alert().dismiss();
            node.debug("Rejecting the Alert...");
        }
    }


    /**
     * @param node
     * @return
     */
    public static String getAlertText(ExtentTest node) {
        String text = null;
        try {
            driver = DriverFactory.getDriver();
            text = driver.switchTo().alert().getText();
            node.info("Alert Text: " + text);
        } catch (NoAlertPresentException na) {
            node.warning("Alert not present");
        }
        return text;
    }

    /**
     * @return
     */
    public static boolean isAlertPresent() {
        try {
            DriverFactory.getDriver().switchTo().alert();
            return true;
        }   // try
        catch (NoAlertPresentException Ex) {
            return false;
        }   // catch
    }


    public static void handleUnExpectedAlert(ExtentTest test) {
        driver = DriverFactory.getDriver();
        Markup markup;
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            markup = MarkupHelper.createLabel("Unexpected Alert opened with text:" + alertText, ExtentColor.RED);
            test.info(markup);
            alert.accept();
            Login.resetLoginStatus();
        } catch (Exception e) {
            System.out.println();
        }
    }

}
