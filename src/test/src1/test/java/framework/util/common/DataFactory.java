package framework.util.common;

//import com.comviva.common.DesEncryptor;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.common.collect.Multimap;
import framework.dataEntity.*;
import framework.entity.*;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MsisdnProperties;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

//import com.comviva.framework.DesEncryptor;

/**
 * Created by rahul.rana on 5/5/2017.
 */
public class DataFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataFactory.class);
    static BigInteger MSISDNCounter = new BigInteger(MsisdnProperties.getInstance().getProperty("web.currentmsisdn"));
    private static ExtentTest dFactory;
    private static InputStream rnrInpStream;
    private static Workbook rnrWb;

    /**
     * Load the RNR Sheet to the Global Variable rnrDetails
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadRNRSheet(ExtentTest t1) throws Exception {
        dFactory = t1; // initialize Extent test for Future use
        int rnrSheet = 0;

        MobiquityGUIQueries dbConn = new MobiquityGUIQueries();

        LOGGER.info("Loading RNR Sheet\n");

        // * Read the Excel *
        List<WebGroupRole> userRoleExcelList = new ArrayList<>();
        List<SuperAdmin> superAdmin = new ArrayList<>();

        rnrInpStream = new FileInputStream(FilePath.fileConfigInput);
        rnrWb = WorkbookFactory.create(rnrInpStream);
        Sheet sheet = rnrWb.getSheetAt(rnrSheet);

        /* get last used row */
        Row r = sheet.getRow(2); // third row is the Category Code
        int lastUsedColumn = r.getLastCellNum();

        DataFormatter formatter = new DataFormatter();

        for (int i = 2; i < lastUsedColumn; i++) {
            ArrayList<String> applicableRoles = new ArrayList<>();

            // * Get the Category Code, Role Name and Number of user per Category to be created *
            String categoryCode = formatter.formatCellValue(sheet.getRow(2).getCell(i));
            if (categoryCode.equalsIgnoreCase(""))
                continue;

            String roleName = formatter.formatCellValue(sheet.getRow(3).getCell(i));
            int numOfUser = Integer.parseInt(formatter.formatCellValue(sheet.getRow(4).getCell(i)));

            // * Get the Role Codes which are applicable for each Category Code - read from excel *
            Iterator<Row> iterator = sheet.iterator();
            while (iterator.hasNext()) {
                Row nextRow = (Row) iterator.next();
                if (nextRow.getRowNum() < 5)
                    continue;

                String isRoleEnabled = formatter.formatCellValue(nextRow.getCell(i));
                if (isRoleEnabled.equals("1")) {
                    String roleCode = formatter.formatCellValue(nextRow.getCell(0));
                    applicableRoles.add(roleCode);
                }
            }

            WebGroupRole webRole = new WebGroupRole(categoryCode, roleName, applicableRoles, numOfUser);
            userRoleExcelList.add(webRole); // Add it to Global List
        }
        GlobalData.rnrDetails = userRoleExcelList;
    }

    public static void loadSuperAdminRoles(ExtentTest t1) throws Exception {
        dFactory = t1; // initialize Extent test for Future use
        List<WebGroupRole> tRoles = new ArrayList<>();
        MobiquityGUIQueries dbConn = new MobiquityGUIQueries();
        LOGGER.info("Loading Superadmin roles from DB Sheet\n");
        List<SuperAdmin> superAdmin = new ArrayList<>();

        /**
         * Get SuperAdmin roles
         * Add admin user to the GlobalData.superAdmins
         */
        ArrayList<String> saRoles = dbConn.getSuperAdminRoles();

        if (saRoles.size() == 0) {
            // there is only one super admin, and there are no web roles defined
            SuperAdmin adminUser = new SuperAdmin(ConfigInput.superadminUserId, "NoRole");
            superAdmin.add(adminUser);
        } else {
            for (String roleName : saRoles) {
                ArrayList<String> saApplicableRoles = dbConn.getApplicableRoleCodes(roleName);
                WebGroupRole webRole = new WebGroupRole(Constants.SUPER_ADMIN, roleName, saApplicableRoles, 1);
                tRoles.add(webRole); // Add it to Global List

                String userName = dbConn.getSuperAdminName(roleName);
                SuperAdmin adminUser = new SuperAdmin(userName, roleName);
                superAdmin.add(adminUser);
            }
        }

        GlobalData.rnrDetails.addAll(tRoles);
        GlobalData.superAdmins = superAdmin;
    }

    /**
     * Get the Web Role From RNR Sheet
     * NOTE - this might not suffice the Maker Checked, for a more specific Role, role code need to be provided
     *
     * @param catCode
     * @return
     */
    public static String getWebRoleNameFromRnR(String catCode) {
        int count = GlobalData.rnrDetails.size();
        for (int i = 0; i < count; i++) {
            WebGroupRole role = GlobalData.rnrDetails.get(i);
            if (role.CategoryCode.equals(catCode)) {
                return role.RoleName;
            }
        }
        return null;

    }

    public static WebGroupRole getWebRoleFromRnR(String catCode) {
        int count = GlobalData.rnrDetails.size();
        for (int i = 0; i < count; i++) {
            WebGroupRole role = GlobalData.rnrDetails.get(i);
            if (role.CategoryCode.equals(catCode)) {
                return role;
            }
        }
        return null;
    }

    /**
     * Get Web Role from rnr, having web role name
     *
     * @param roleName
     * @return
     */
    public static WebGroupRole getWebRoleWithRoleName(String roleName) {
        int count = GlobalData.rnrDetails.size();
        for (int i = 0; i < count; i++) {
            WebGroupRole role = GlobalData.rnrDetails.get(i);
            if (role.RoleName.equals(roleName)) {
                return role;
            }
        }
        return null;
    }

    /************************************************************************************************************
     * DOMAIN CATEGORY RELATED METHODS
     *************************************************************************************************************/

    public static List<String> getAllCategoriesFromRnr() {
        List<String> categoryList = new ArrayList<>();
        for (WebGroupRole role : GlobalData.rnrDetails) {
            String categoryName = DataFactory.getCategoryName(role.CategoryCode);
            if (!categoryList.contains(categoryName))
                categoryList.add(categoryName);
        }
        return categoryList;
    }

    public static List<String> getAllCategoryCodesFromRnr() {
        List<String> categoryList = new ArrayList<>();
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (!categoryList.contains(role.CategoryCode))
                categoryList.add(role.CategoryCode);
        }
        return categoryList;
    }

    /**
     * Load Domain cAtegory
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadDomainCategoryDBMap() {
        try {
            MobiquityGUIQueries dbConn = new MobiquityGUIQueries();
            HashMap<String, DomainCategoryDB> categoryDomainMapDataList = new HashMap<>();

            // Get the Category and Domain details From DB
            LOGGER.info("Fetching Domain Category Mapping from DataBase...\n");

            ResultSet result = dbConn.dbFetchDomainCategoryMap();
            while (result.next()) {
                String CategoryName = result.getString("CATEGORY_NAME");
                String CategoryCode = result.getString("CATEGORY_CODE");
                String DomainName = result.getString("DOMAIN_NAME");
                String DomainCode = result.getString("DOMAIN_CODE");
                String ParentCategoryCode = result.getString("PARENT_CATEGORY_CODE");

                DomainCategoryDB objDomainCategory = new DomainCategoryDB(CategoryName,
                        CategoryCode,
                        DomainName,
                        DomainCode,
                        ParentCategoryCode);

                if (DomainCode.equals("OPT")) {
                    objDomainCategory.setDomainAsOperator();
                } else if (DomainCode.equals("SUBS")) {
                    objDomainCategory.setDomainAsLeaf();
                } else if (DomainCode.equals("Enterprise") || DomainCode.equals("INTLMER")) {
                    objDomainCategory.setDomainAsSpecial();
                } else {
                    objDomainCategory.setIsChannelDomain();
                }

                categoryDomainMapDataList.put(CategoryCode, objDomainCategory);
            }
            GlobalData.domainCategoryMap = categoryDomainMapDataList;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * getCategoryName
     *
     * @param categoryCode
     * @return -> Return Category Name
     */
    public static String getCategoryName(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return map.CategoryName;
    }

    public static String getDomainName(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return map.DomainName;
    }

    public static String getCategoryCode(String categoryName) {
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (role.CategoryName.equalsIgnoreCase(categoryName)) {
                return role.CategoryCode;
            }
        }
        return null;
    }

    public static List<String> getAllDomainNamesExceptOPT() {
        List<String> domainList = new ArrayList<>();
        for (WebGroupRole role : GlobalData.rnrDetails) {
            String domainName = DataFactory.getDomainName(role.CategoryCode);
            if (!domainName.equalsIgnoreCase(Constants.OPERATOR)) {
                domainList.add(domainName);
            }

        }
        return domainList;
    }

    public static String getDomainCode(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return map.DomainCode;
    }

    public static String getParentCategoryCode(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return map.ParentCategoryCode;
    }

    public static String getParentCategoryName(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return getCategoryName(map.ParentCategoryCode);
    }

    /************************************************************************************************************
     * END of DOMAIN CATEGORY RELATED METHODS
     *************************************************************************************************************/

    /************************************************************************************************************
     * CURRENCY PROVIDER RELATED METHODS
     *************************************************************************************************************/

    public static boolean isLeafDomain(String categoryCode) {
        DomainCategoryDB map = GlobalData.domainCategoryMap.get(categoryCode);
        return map.isLeafDomain;
    }

    /**
     * Load Currency Provider
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadCurrencyProvider() throws IOException, InvalidFormatException {
        int currencySheet = 1;
        LOGGER.info("*** Loading Currency Provider Sheet ***\n");
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();

        List<Bank> bankList = new ArrayList<>();
        List<CurrencyProvider> providerList = new ArrayList<>();

        // * Read the Excel *
        Sheet sheet = rnrWb.getSheetAt(currencySheet);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            String providerName = getCellValueString(nextRow.getCell(0));
            if (providerName.equalsIgnoreCase("")
                    || providerName.equalsIgnoreCase("providerName"))
                continue;

            String providerId = getCellValueString(nextRow.getCell(1));
            String currency = getCellValueString(nextRow.getCell(2));
            String bankName = getCellValueString(nextRow.getCell(3));
            String isDefaultProvider = getCellValueString(nextRow.getCell(4));
            Boolean isDefaultBank = Assertion.isEqual(getCellValueString(nextRow.getCell(5)), "true");
            Boolean isTrust = Assertion.isEqual(getCellValueString(nextRow.getCell(6)), "true");
            Boolean isLiquidation = Assertion.isEqual(getCellValueString(nextRow.getCell(7)), "true");
            String bankId = getCellValueString(nextRow.getCell(8));
            String poolId = getCellValueString(nextRow.getCell(9));
            Boolean isRequired = Assertion.isEqual(getCellValueString(nextRow.getCell(10)), "true");

            String bankIdDb = dbHandle.dbGetBankId(bankName);

            if (bankId.equalsIgnoreCase(""))
                bankId = bankIdDb;

            if (poolId.equalsIgnoreCase(""))
                poolId = DataFactory.getRandomNumberAsString(9);

            Bank bank = new Bank(providerName,
                    providerId,
                    bankName,
                    bankId,
                    poolId,
                    isTrust,
                    isLiquidation,
                    isDefaultBank,
                    isRequired
            );

            if (bankIdDb != null) {
                bank.setExisting(true);
            }

            bankList.add(bank); // add bank to global List

            // update the currency provider object
            CurrencyProvider cp = new CurrencyProvider(providerName,
                    providerId,
                    currency,
                    isDefaultProvider,
                    bank
            );

            providerList.add(cp); // add currency to global List
        }
        GlobalData.banks = bankList;
        GlobalData.currencyProvider = providerList;
    }

    /**
     * Get the Default bank associated with the Default Provider
     *
     * @return
     */
    public static CurrencyProvider getDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.IsDefault)
                return provider;
        }
        return null;
    }

    /**
     * Get Trust Bank Name
     *
     * @return
     */
    public static String getTrustBankName(String providerName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)
                    && provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankName;
            }
        }
        return null;
    }

    public static String getTrustBankid(String providerName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)
                    && provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankID;
            }
        }
        return null;
    }

    public static List<String> getTrustBankNameList(String providerName) {
        List<String> bankList = new ArrayList<>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)
                    && provider.Bank.IsTrust
                    && !bankList.contains(provider.Bank.BankName)) {
                bankList.add(provider.Bank.BankName);
            }
        }

        return bankList;
    }

    public static List<String> getTrustBankNameListForBankAdminCreation(String providerName) {
        List<String> bankList = new ArrayList<>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)
                    && provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet
                    && !bankList.contains(provider.Bank.BankName)) {
                bankList.add(provider.Bank.BankName);
            }
        }

        return bankList;
    }

    public static List<String> getAllLiquidationBanksLinkedToProvider(String providerName) {
        List<String> bankList = new ArrayList<String>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)
                    && provider.Bank.IsLiquidation
                    && !bankList.contains(provider.Bank.BankName)) {
                bankList.add(provider.Bank.BankName);
            }
        }

        return bankList;
    }

    public static List<CurrencyProvider> getProviderWithLiquidationBank() {
        List<CurrencyProvider> providerList = new ArrayList<>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.Bank.IsLiquidation
                    && provider.Bank.IsRequiredInBaseSet
                    && !providerList.contains(provider)) {
                providerList.add(provider);
            }
        }
        return providerList;
    }

    /**
     * Get Non Trust Bank Name
     *
     * @return
     */
    public static String getNonTrustBankName(String providerName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName)) {
                if (!provider.Bank.IsLiquidation
                        && !provider.Bank.IsTrust) {
                    return provider.Bank.BankName;
                }
            }
        }

        return null;
    }

    public static String getDefaultBankName(String providerId) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderId.equalsIgnoreCase(providerId)
                    && provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankName;
            }
        }

        return null;
    }

    public static String getDefaultLiquidationBankName(String providerId) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderId.equalsIgnoreCase(providerId)
                    && provider.IsDefault
                    && provider.Bank.IsRequiredInBaseSet
                    && provider.Bank.IsLiquidation) {
                return provider.Bank.BankName;
            }
        }
        return null;
    }

    public static String getDefaultBankId(String providerId) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderId.equalsIgnoreCase(providerId)
                    && provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankID;
            }
        }

        return null;
    }

    /**
     * Get List Of providers
     *
     * @return
     */
    public static List<String> getAllProviderNames() {
        List<String> providerList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!providerList.contains(provider.ProviderName)) { // make sure that only Distinct values are returned
                providerList.add(provider.ProviderName);
            }
        }
        return providerList;
    }

    public static List<String> getAllProviderId() {
        List<String> providerList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!providerList.contains(provider.ProviderId)) { // make sure that only Distinct values are returned
                providerList.add(provider.ProviderId);
            }
        }
        return providerList;
    }

    /**
     * Get the list of Banks Associated with a provider, except Liquidation Banks
     *
     * @param providerName
     * @return
     */
    public static List<String> getAllBankNamesLinkedToProvider(String providerName) {
        List<String> bankList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsLiquidation) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    public static List<String> getAllBankNamesForBaseSetUserCreation() {
        List<String> bankList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!bankList.contains(provider.Bank.BankName)
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    public static List<String> getAllBankNamesForMobileRoleCreation() {
        List<String> bankList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsLiquidation
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    public static List<String> getAllBankNamesForUserCreation(String providerName) {
        List<String> bankList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsLiquidation
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    public static Bank getBankFromAppData(String bankName) {
        for (Bank bank : GlobalData.banks) {
            if (bank.BankName.equalsIgnoreCase(bankName))
                return bank;
        }
        return null;
    }

    public static boolean isProviderDefault(String providerName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equalsIgnoreCase(providerName) && provider.IsDefault) {
                return true;
            }
        }
        return false;
    }

    public static List<String> getAllTrustBankNamesLinkedToProvider(String providerName) {
        List<String> bankList = new ArrayList<String>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }

        return bankList;
    }

    public static List<String> getAllNonTrustBankNamesLinkedToProvider(String providerName) {
        List<String> bankList = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsTrust
                    && !provider.Bank.IsLiquidation
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    /**
     * This is combined data From GlobalData.bankDB and Banks provided in Config Input
     *
     * @param providerName
     * @return
     */
    public static List<Bank> getAllTrustBanksLinkedToProvider(String providerName) {
        List<Bank> bankList = new ArrayList<Bank>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank);
            }
        }

        return bankList;
    }

    public static List<Bank> getAllNonTrustBanksLinkedToProvider(String providerName) {
        List<Bank> bankList = new ArrayList<Bank>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsLiquidation
                    && !provider.Bank.IsTrust
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank);
            }
        }
        return bankList;
    }

    public static List<Bank> getAllBanksLinkedToProviderForBankLinkMapping(String providerName) {
        List<Bank> bankList = new ArrayList<Bank>();
        List<String> bankNameList = new ArrayList<>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)
                    && !bankList.contains(provider.Bank.BankName)
                    && !provider.Bank.IsLiquidation
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                bankList.add(provider.Bank);
            }
        }
        return bankList;
    }


    /**
     * Get All payment Instruments
     *
     * @return
     */
    /*public static List<String> getAllPaymentInstruments(String providerName) {
        List<String> payInst = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if ((!payInst.contains(provider.BankName)) && provider.ProviderName.equalsIgnoreCase(providerName)) { // make sure that only Distinct values are returned
                payInst.add(provider.BankName);
            }
       }

        for (Wallet wallet : GlobalData.wallet) {
            if (!payInst.contains(wallet.WalletName) && wallet.IsActive) { // make sure that only Distinct values are returned
                payInst.add(wallet.WalletName);
            }
        }
        return payInst;
    }
*/

    /**
     * Get All payment Instruments
     *
     * @return
     */
    public static List<String> getAllPaymentInstruments(String providerName) {
        List<String> payInst = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!payInst.contains(provider.Bank.BankName)
                    && provider.ProviderName.equals(providerName)
                    && provider.Bank.IsRequiredInBaseSet) { // make sure that only Distinct values are returned
                payInst.add(provider.Bank.BankName);
            }
        }

        for (Wallet wallet : GlobalData.wallet) {
            if (!payInst.contains(wallet.WalletName) && wallet.IsActive) { // make sure that only Distinct values are returned
                payInst.add(wallet.WalletName);
            }
        }
        return payInst;
    }

    public static List<String> getAllPaymentInstrumentsForTcpCreation(String providerName) {
        List<String> payInst = new ArrayList<String>();
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!payInst.contains(provider.Bank.BankName)
                    && provider.ProviderName.equals(providerName)
                    && provider.Bank.IsRequiredInBaseSet
                    && !provider.Bank.IsLiquidation) { // make sure that only Distinct values are returned
                payInst.add(provider.Bank.BankName);
            }
        }

        for (Wallet wallet : GlobalData.wallet) {
            if (!payInst.contains(wallet.WalletName) && wallet.IsActive) { // make sure that only Distinct values are returned
                payInst.add(wallet.WalletName);
            }
        }
        return payInst;
    }


    /**
     * Get Payment Intstrument Type -  BANK/WALLET
     *
     * @param payInstrument
     * @return
     */
    public static String getPaymentInstrumentType(String payInstrument) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.Bank.BankName.equalsIgnoreCase(payInstrument)) {
                return "BANK";
            }
        }

        for (Wallet wallet : GlobalData.wallet) {
            if (payInstrument.equalsIgnoreCase(wallet.WalletName) && wallet.IsActive) {
                return "WALLET";
            }
        }
        return null;
    }

    /**
     * Get Provider Id
     *
     * @param providerName
     * @return
     */
    public static String getProviderId(String providerName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderName.equals(providerName)) {
                return provider.ProviderId;
            }
        }
        return null;
    }

    public static String getProviderName(String providerId) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.ProviderId.equals(providerId)) {
                return provider.ProviderName;
            }
        }
        return null;
    }

    public static String getProviderNameUsingBankName(String bankName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.Bank.BankName.equalsIgnoreCase(bankName)) {
                return provider.ProviderName;
            }
        }
        return null;
    }

    public static CurrencyProvider getNonDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!provider.IsDefault) {
                return provider;
            }
        }
        return null;
    }

    public static Bank getDefaultBankForDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank;
            }
        }
        return null;
    }

    public static String getDefaultBankNameForDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankName;
            }
        }
        return null;
    }

    public static String getDefaultBankIdForDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankID;
            }
        }
        return null;
    }

    public static String getDefaultBankIdForNonDefaultProvider() {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!provider.IsDefault
                    && provider.Bank.IsDefaultBank
                    && provider.Bank.IsRequiredInBaseSet) {
                return provider.Bank.BankID;
            }
        }
        return null;
    }

    /**
     * Get the Bank ID
     *
     * @param bankName
     * @return
     */
    public static String getBankId(String bankName) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.Bank.BankName.equals(bankName)) {
                return provider.Bank.BankID;
            }
        }
        return null;
    }

    public static String getBankName(String bankId) {
        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (provider.Bank.BankID.equals(bankId)) {
                return provider.Bank.BankName;
            }
        }
        return null;
    }

    /************************************************************************************************************
     * END of CURRENCY PROVIDER RELATED METHODS
     *************************************************************************************************************/

    /************************************************************************************************************
     * WALLET RELATED METHODS
     *************************************************************************************************************/

    /**
     * Get list of Banks from the Input Sheet
     *
     * @return
     */
    public static List<String> getAllBank() {
        List<String> bankList = new ArrayList<String>();

        for (CurrencyProvider provider : GlobalData.currencyProvider) {
            if (!bankList.contains(provider.Bank.BankID)
                    && provider.Bank.IsRequiredInBaseSet) {
                bankList.add(provider.Bank.BankName);
            }
        }
        return bankList;
    }

    /**
     * Load Wallet Mappings
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadWallet() throws IOException, InvalidFormatException {
        int walletSheet = 2;
        LOGGER.info("*** Loading Wallet Sheet ***\n");

        // * Read the Excel *
        List<Wallet> walletMap = new ArrayList<>();
        Sheet sheet = rnrWb.getSheetAt(walletSheet);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            String autWalletCode = getCellValueString(nextRow.getCell(0)); // user Defined , it must also be updated under Wallets.java class
            if (autWalletCode.equalsIgnoreCase("")
                    || autWalletCode.equalsIgnoreCase("AutomationWalletCode"))
                continue;

            String walletName = getCellValueString(nextRow.getCell(1));
            String isDefault = getCellValueString(nextRow.getCell(2));
            String walletId = getCellValueString(nextRow.getCell(3));
            String forChannelUser = getCellValueString(nextRow.getCell(4));
            String forSubscriber = getCellValueString(nextRow.getCell(5));
            String isSpecialWallet = getCellValueString(nextRow.getCell(6));
            String isActive = getCellValueString(nextRow.getCell(7));

            if (walletName.equalsIgnoreCase(""))
                continue;

            Wallet obj = new Wallet(autWalletCode, walletId, walletName, isDefault,
                    forChannelUser, forSubscriber, isSpecialWallet, isActive);
            walletMap.add(obj);
        }

        GlobalData.wallet = walletMap;
    }

    /**
     * Load Grade Mappings
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadGradesFromConfig() throws IOException, InvalidFormatException {
        int gradeSheet = 3;
        LOGGER.info("*** Loading Grade Sheet ***\n");

        // * Read the Excel *
        HashMap<String, List<GradeDB>> map = new HashMap<>();
        List<String> gradeNameList = new ArrayList<>();
        Sheet sheet = rnrWb.getSheetAt(gradeSheet);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            List<GradeDB> tempList = new ArrayList<>();

            String gradeName = getCellValueString(nextRow.getCell(0));
            if (gradeName.equalsIgnoreCase("GradeName")
                    || gradeName.equalsIgnoreCase(""))
                continue;

            String gradeCode = getCellValueString(nextRow.getCell(1));
            String categoryCode = getCellValueString(nextRow.getCell(2));
            String isInclude = getCellValueString(nextRow.getCell(3));

            if (isInclude.equalsIgnoreCase("yes")) {
                gradeNameList.add(gradeName);
                GradeDB objGrade = new GradeDB(gradeName, gradeCode, categoryCode);
                if (map.containsKey(categoryCode)) {
                    map.get(categoryCode).add(objGrade);
                } else {
                    tempList.add(objGrade);
                    map.put(categoryCode, tempList);
                }
            }
        }

        GlobalData.gradeCategoryDBMap = map;
        GlobalData.gradeList = gradeNameList;
    }


    /**
     * this is just to initialize for parallel execution
     */
    public static void initializeExecutionGroups() {
        GlobalData.exeCategoryGroup1.add(Constants.WHOLESALER);
        GlobalData.exeCategoryGroup1.add(Constants.SPECIAL_SUPER_AGENT);
        GlobalData.exeCategoryGroup1.add(Constants.RETAILER);

        GlobalData.exeCategoryGroup2.add(Constants.HEAD_MERCHANT);
        GlobalData.exeCategoryGroup2.add(Constants.MERCHANT);
        GlobalData.exeCategoryGroup2.add(Constants.SUBSCRIBER);

        GlobalData.exeCategoryGroup3.add(Constants.BILLER);
        GlobalData.exeCategoryGroup3.add(Constants.BULK_PAYER_ADMIN);

        GlobalData.exeCategoryGroup4.add(Constants.INT_PARTNER);
        GlobalData.exeCategoryGroup4.add(Constants.ENTERPRISE);
    }

    /**
     * Get the Wallet object based on the Wautomation Wallet Code
     * this Wallet code is common in both the Wallet Sheet of Config Input and in the Wallets.java class
     * this is introduced, as there are chances that a particular wallet ID can vary system to system
     * Hence, a code anchor was required to be implemented
     *
     * @param code
     * @return
     */
    public static Wallet getWalletUsingAutomationCode(String code) {
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.AutWalletCode.equalsIgnoreCase(code)) {
                return wallet;
            }
        }
        return null;
    }

    /**
     * Get Default Wallet Object
     *
     * @return
     */
    public static Wallet getDefaultWallet() {
        int count = GlobalData.wallet.size();
        for (int i = 0; i < count; i++) {
            Wallet wallet = GlobalData.wallet.get(i);
            if (wallet.IsDefault) {
                return wallet;
            }
        }
        return null;
    }

    /**
     * Get All Wallets
     *
     * @return
     */
    public static List<Wallet> getAllWallet() {
        List<Wallet> walletList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.IsActive) {
                walletList.add(wallet);
            }
        }
        return walletList;
    }

    public static List<String> getAllWalletNames() {
        List<String> walletList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    /**
     * Get the Wallet ID
     *
     * @param walletName
     * @return
     */
    public static String getWalletId(String walletName) {
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.WalletName.equals(walletName)) {
                return wallet.WalletId;
            }
        }
        return null;
    }

    public static String getWalletName(String id) {
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.WalletId.equals(id)) {
                return wallet.WalletName;
            }
        }
        return null;
    }

    /**
     * Get list of wallet associated with the channel User
     *
     * @return
     */
    public static List<String> getWalletForChannelUser() {
        List<String> walletList = new ArrayList<String>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForChannelUser && wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    /**
     * Exclude the Special Wallets for Channel User Creation Only
     *
     * @return
     */
    public static List<String> getWalletForChannelUserCreation() {
        List<String> walletList = new ArrayList<String>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForChannelUser && !wallet.IsSpecialWallet && wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    /**
     * Get List of Special Wallets
     *
     * @return
     */
    public static List<String> getSpecialWallet() {
        List<String> walletList = new ArrayList<String>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.IsSpecialWallet && wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    /************************************************************************************************************
     * END of WALLET RELATED METHODS
     *************************************************************************************************************/

    /************************************************************************************************************
     * EXECUTION CONSTANTS
     *************************************************************************************************************/

    /**
     * Exclude the Special Wallets for Subscriber User Creation Only
     *
     * @return
     */
    public static List<String> getWalletForSubscriberUserCreation() {
        List<String> walletList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForSubscriber && !wallet.IsSpecialWallet && wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    /**
     * Get all the valid wallets for a Subscriber
     *
     * @return
     */
    public static List<String> getWalletForSubscriber() {
        List<String> walletList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForSubscriber && wallet.IsActive) {
                walletList.add(wallet.WalletName);
            }
        }
        return walletList;
    }

    public static String[] getPayIdApplicableForSubs() {
        List<String> walletIdList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForSubscriber && wallet.IsActive) {
                walletIdList.add(wallet.WalletId);
            }
        }

        String[] payIdArr = new String[walletIdList.size()];
        payIdArr = walletIdList.toArray(payIdArr);
        return payIdArr;
    }

    public static String[] getPayIdApplicableForChannelUser() {
        List<String> walletIdList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForChannelUser && wallet.IsActive) {
                walletIdList.add(wallet.WalletId);
            }
        }

        String[] payIdArr = new String[walletIdList.size()];
        payIdArr = walletIdList.toArray(payIdArr);
        return payIdArr;
    }

    public static List<String> getPayInstrumentApplicableForSubs() {
        List<String> walletIdList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForSubscriber && wallet.IsActive) {
                walletIdList.add(wallet.WalletName);
            }
        }

        return walletIdList;
    }

    public static List<String> getPayInstrumentApplicableForChannelUser() {
        List<String> walletIdList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForChannelUser && wallet.IsActive) {
                walletIdList.add(wallet.WalletName);
            }
        }

        return walletIdList;
    }

    public static List<String> getNonDefaultPayIdExceptSpecialWallet() {
        List<String> walletIdList = new ArrayList<>();
        for (Wallet wallet : GlobalData.wallet) {
            if (wallet.ForSubscriber && wallet.IsActive && !wallet.IsDefault && !wallet.IsSpecialWallet) {
                walletIdList.add(wallet.WalletId);
            }
        }

        return walletIdList;
    }

    /************************************************************************************************************
     * LOAD MOBILE GROUP ROLE SHEET & RELATED METHODS
     *************************************************************************************************************/

    /**
     * Load the Service List excel to Global Variable GlobalData.serviceList
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadServiceList() throws IOException, InvalidFormatException {
        LOGGER.info("*** Loading Service List ***\n");
        int serviceSheet = 1;

        // * Read the Excel *
        List<ServiceList> serviceMap = new ArrayList<>();
        InputStream inp = new FileInputStream(FilePath.fileExecutionConstants);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(serviceSheet);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();

            String serviceName = getCellValueString(nextRow.getCell(0));
            if (serviceName.equalsIgnoreCase("")
                    || serviceName.equalsIgnoreCase("SERVICE_NAME"))
                continue;

            String serviceType = getCellValueString(nextRow.getCell(1));
            String payerPaymentType = getCellValueString(nextRow.getCell(2));
            String payeePaymentType = getCellValueString(nextRow.getCell(3));

            ServiceList obj = new ServiceList(serviceName, serviceType, payerPaymentType, payeePaymentType);
            serviceMap.add(obj);
        }
        GlobalData.serviceList = serviceMap;
    }

    /**
     * Load the Service List excel to Global Variable GlobalData.serviceList
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadMobileGroupRole() throws IOException, InvalidFormatException {
        // * Read the Excel *
        List<MobileGroupRole> mobileRoleMap = new ArrayList<>();
        InputStream inp = new FileInputStream(FilePath.fileMobileRoles);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            String profileName = getCellValueString(nextRow.getCell(0));
            if (profileName.equalsIgnoreCase("")
                    || profileName.equalsIgnoreCase("Profile Name"))
                continue;

            String profileCode = getCellValueString(nextRow.getCell(1));
            String domainName = getCellValueString(nextRow.getCell(2));
            String categoryName = getCellValueString(nextRow.getCell(3));
            String gradeName = getCellValueString(nextRow.getCell(4));
            String providerName = getCellValueString(nextRow.getCell(5));
            String payInst = getCellValueString(nextRow.getCell(6));
            String payInstType = getCellValueString(nextRow.getCell(7));
            String status = getCellValueString(nextRow.getCell(8));

            MobileGroupRole obj = new MobileGroupRole(profileName, profileCode, domainName, categoryName, gradeName, providerName, payInst, payInstType, status);
            mobileRoleMap.add(obj);

        }

        GlobalData.mobileGroupRole = mobileRoleMap;
    }

    /************************************************************************************************************
     * END MOBILE GROUP ROLE
     *************************************************************************************************************/

    /**
     * Get Mobile Group Role From Excel, loaded Global Variable
     *
     * @param domain
     * @param category
     * @param grade
     * @param provider
     * @param payInstType
     * @return
     */
    public static MobileGroupRole getMobileGroupRole(String domain, String category, String grade, String provider, String payInstType) {
        int count = GlobalData.mobileGroupRole.size();
        try {
            for (MobileGroupRole mRole : GlobalData.mobileGroupRole) {
                if (mRole.DomainName.equalsIgnoreCase(domain)
                        && mRole.CategoryName.equalsIgnoreCase(category)
                        && mRole.GradeName.equalsIgnoreCase(grade)
                        && mRole.MobileProvider.equalsIgnoreCase(provider)
                        && mRole.MobilePayInstType.equalsIgnoreCase(payInstType)
                        && mRole.MobileRoleStatus.equalsIgnoreCase("ACTIVE")) {
                    return mRole;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Assert.fail("Failed to get Mobile role for domain:" + domain + ", category: " + category + ", grade:" + grade + ", payInst:" + payInstType);
        return null;
    }

    public static String getMobileRoleCode(String mobileRoleName) {
        for (MobileGroupRole role : GlobalData.mobileGroupRole) {
            if (role.ProfileName.equalsIgnoreCase(mobileRoleName))
                return role.ProfileCode;
        }
        return null;
    }

    /**
     * Load the transfer Rule Input File
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadTransferRuleInput() throws IOException, InvalidFormatException {
        int transferRuleCommissionInputSheet = 0;
        LOGGER.info("*** Loading Transfer Rule Common Input***\n");
        DataFormatter formatter = new DataFormatter();

        // * Read the Excel *
        InputStream inp = new FileInputStream(FilePath.fileExecutionConstants);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(transferRuleCommissionInputSheet);

        String statusId = formatter.formatCellValue(sheet.getRow(1).getCell(0));
        String transferType = formatter.formatCellValue(sheet.getRow(1).getCell(1));
        String controlledTxLevel = formatter.formatCellValue(sheet.getRow(1).getCell(2));
        String fixedTxLevel = formatter.formatCellValue(sheet.getRow(1).getCell(3));
        String modifyStatusID = formatter.formatCellValue(sheet.getRow(1).getCell(4));
        String modifyTxType = formatter.formatCellValue(sheet.getRow(1).getCell(5));
        String modifyControlTxLevel = formatter.formatCellValue(sheet.getRow(1).getCell(6));
        String modifyFixedTxLevel = formatter.formatCellValue(sheet.getRow(1).getCell(7));
        String geoDomainCode = formatter.formatCellValue(sheet.getRow(1).getCell(8));

        TransferRuleInput obj = new TransferRuleInput(statusId, transferType, controlledTxLevel, fixedTxLevel,
                modifyStatusID, modifyTxType, modifyControlTxLevel, modifyFixedTxLevel, geoDomainCode);
        GlobalData.transferRuleInput = obj;
    }

    /**
     * Load the transfer Rule Input File
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadExecutionConstants() throws IOException, InvalidFormatException {
        int executionConstantSheet = 0;
        LOGGER.info("*** Loading Execution  ***\n");

        // * Read the Excel *
        InputStream inp = new FileInputStream(FilePath.fileExecutionConstants);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(executionConstantSheet);


    }

    /**
     * Get Service Info
     *
     * @return
     */
    public static ServiceList getServiceInfo(String servicetype) {
        int count = GlobalData.serviceList.size();
        for (int i = 0; i < count; i++) {
            ServiceList service = GlobalData.serviceList.get(i);
            if (service.ServcieType.equals(servicetype)) {
                return service;
            }
        }
        return null;
    }

    /**
     * read the excel output file Group Role.xlsx
     *
     * @return Map
     * @throws Exception
     */
    public static Map<String, WebGroupRole> getGroupRoleSet() throws Exception {
        Map<String, WebGroupRole> map = new HashMap<String, WebGroupRole>();
        InputStream inp = new FileInputStream(FilePath.fileGroupRoles);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            String domainName = getCellValueString(nextRow.getCell(0));
            if (domainName.equalsIgnoreCase("")
                    || domainName.equalsIgnoreCase("Domain"))
                continue;

            String categoryName = getCellValueString(nextRow.getCell(1));
            String roleName = getCellValueString(nextRow.getCell(3));
            String created = getCellValueString(nextRow.getCell(8));

            // get the service Charge Object
            WebGroupRole gRole = new WebGroupRole(domainName, categoryName, roleName, created);

            // Push it to the Map
            map.put(gRole.RoleName, gRole);
        }
        return map;
    }

    /**
     * Get Operator User Set From Excel Output File
     *
     * @return
     * @throws Exception
     */
    public static Map<String, OperatorUser> getOperatorUserSet(boolean... isAppData) throws Exception {
        Map<String, OperatorUser> map = new HashMap<String, OperatorUser>();

        InputStream inp = null;
        boolean isapp = isAppData.length > 0 ? isAppData[0] : true;
        if (isapp) {
            inp = new FileInputStream(FilePath.fileOperatorUsers);
        } else {
            inp = new FileInputStream(FilePath.fileTempOptUsers);
        }

        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        int counter = 0; // counter to skip the header
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (counter > 0) {
                String categoryCode = getCellValueString(nextRow.getCell(0));
                if (categoryCode.equalsIgnoreCase("")
                        || categoryCode.equalsIgnoreCase("CategoryCode"))
                    continue;

                String firstName = getCellValueString(nextRow.getCell(1));
                String msisdn = getCellValueString(nextRow.getCell(3));
                String bankName = getCellValueString(nextRow.getCell(4));
                String loginId = getCellValueString(nextRow.getCell(5));
                String password = getCellValueString(nextRow.getCell(6));
                String webGroupRole = getCellValueString(nextRow.getCell(7));
                String isPwdReset = getCellValueString(nextRow.getCell(10));
                String status = getCellValueString(nextRow.getCell(13));
                String parentUserName = getCellValueString(nextRow.getCell(14));

                if (!isPwdReset.equalsIgnoreCase("") && isPwdReset.equals("Y")) {
                    OperatorUser opUser = new OperatorUser(categoryCode, firstName, loginId, msisdn,
                            password, bankName, webGroupRole, isPwdReset, status, parentUserName);

                    map.put(opUser.LoginId, opUser);
                }
            }
            counter++;
        }
        return map;
    }

    /**
     * Get Operator User Set From Excel Output File
     *
     * @return
     * @throws Exception
     */
    public static Map<String, User> getChannelUserSet(Boolean isAppData) throws Exception {
        Map<String, User> map = new HashMap<String, User>();
        InputStream inp = null;

        if (isAppData) {
            inp = new FileInputStream(FilePath.fileChannelUsers);
        } else {
            inp = new FileInputStream(FilePath.fileTempUsers);
        }

        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        int counter = 0; // counter to skip the header
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (counter > 0) {

                String categoryCode = getCellValueString(nextRow.getCell(0));
                if (categoryCode.equalsIgnoreCase("")
                        || categoryCode.equalsIgnoreCase("CategoryCode"))
                    continue;

                String domainCode = getCellValueString(nextRow.getCell(1));
                String firstName = getCellValueString(nextRow.getCell(2));
                String loginId = getCellValueString(nextRow.getCell(3));
                String msisdn = getCellValueString(nextRow.getCell(5));
                String password = getCellValueString(nextRow.getCell(6));
                String gradeCode = getCellValueString(nextRow.getCell(7));
                String webGroupRole = getCellValueString(nextRow.getCell(8));
                String isPwdReset = getCellValueString(nextRow.getCell(11));
                String defaultAccNum = getCellValueString(nextRow.getCell(12));
                String defaultCustId = getCellValueString(nextRow.getCell(13));
                String categoryName = getCellValueString(nextRow.getCell(16));
                String domainName = getCellValueString(nextRow.getCell(17));
                String gradeName = getCellValueString(nextRow.getCell(18));
                String externalCode = getCellValueString(nextRow.getCell(19));
                String idNum = getCellValueString(nextRow.getCell(20));
                String status = getCellValueString(nextRow.getCell(21));
                String zoneName = getCellValueString(nextRow.getCell(22));
                String areaName = getCellValueString(nextRow.getCell(23));
                String merType = getCellValueString(nextRow.getCell(24));
                String mpin = getCellValueString(nextRow.getCell(25));
                String tpin = getCellValueString(nextRow.getCell(26));

                if (!isPwdReset.equalsIgnoreCase("") && isPwdReset.equals("Y")) {
                    // get the service Charge Object
                    User chUser = new User(categoryCode, categoryName, domainCode,
                            domainName, loginId, firstName,
                            msisdn, password, gradeCode,
                            gradeName, webGroupRole, isPwdReset,
                            defaultAccNum, defaultCustId,
                            externalCode, idNum, status, zoneName, areaName, merType, mpin, tpin
                    );

                    // Push it to the Map
                    map.put(chUser.LoginId, chUser);
                }
            }
            counter++;
        }
        return map;
    }

    /**
     * Get SuperAdmin with Specific Access
     *
     * @param rolecode
     * @return
     * @throws Exception
     */
    public static SuperAdmin getSuperAdminWithAccess(String rolecode) throws Exception {
        if (GlobalData.superAdmins.size() == 1) {
            return GlobalData.superAdmins.get(0); // it means there is no maker checker for Admin user
        }

        // find the Admin user with specific Role Assigned
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (role.ApplicableRoles.contains(rolecode) && role.CategoryCode.equals(Constants.SUPER_ADMIN)) {
                for (SuperAdmin admin : GlobalData.superAdmins) {
                    if (admin.WebGroupRole.equals(role.RoleName)) {
                        return admin;
                    }
                }
            }
        }

        // if no user is found raise error
        LOGGER.error("Error: SuperAdmin with role code '" + rolecode + "' is not available, Please check DB");
        return null;
    }

    /**
     * Get Operator User With Specific Role
     *
     * @param rolecode
     * @return
     * @throws Exception
     */
    public static OperatorUser getOperatorUserWithAccess(String rolecode) throws Exception {
        List<OperatorUser> userList = getOperatorUsersWithAccess(rolecode);
        if (userList.size() != 0) {
            return userList.get(0);
        }
        return null;
    }

    /**
     * Get Operator User
     *
     * @param rolecode
     * @param categoryCode
     * @return
     * @throws Exception
     */
    public static OperatorUser getOperatorUserWithAccess(String rolecode, String categoryCode) throws Exception {
        List<OperatorUser> userList = getOperatorUsersWithAccess(rolecode, categoryCode);
        if (userList.size() != 0) {
            return userList.get(0);
        }
        return null;
    }

    /**
     * Get Operator User Having a Specific Category
     *
     * @param categoryCode
     * @return
     * @throws Exception
     */
    public static OperatorUser getOperatorUserWithCategory(String categoryCode) throws Exception {
        List<OperatorUser> userList = getOperatorUserListWithCategory(categoryCode);
        if (userList.size() != 0) {
            return userList.get(0);
        }
        return null;
    }

    /**
     * Get Operator User With Specific Access
     *
     * @param rolecode
     * @return
     * @throws Exception
     */
    public static List<OperatorUser> getOperatorUsersWithAccess(String rolecode) throws Exception {
        List<OperatorUser> userList = new ArrayList<>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)) {
                for (OperatorUser usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y")) {
                        userList.add(usr);
                    }
                }
            }
        }

        // Check if one uers is present for all the roles are not
        /*if (userList.size() == 0) {
            String msg = "Error: User with role code '" + rolecode + "' is not available in Operator User Sheet, Please check";
            LOGGER.error(msg);
            Assertion.markAsFailure(msg);
            Assert.fail(msg);
        }*/

        // return List of specific permission Users
        return userList;
    }


    /**
     * Get Bank admin With specific Role access and Bank
     *
     * @param roleCode
     * @param bankName
     * @return
     * @throws Exception
     */
    public static OperatorUser getBankAdminWithAccessAndBank(String roleCode, String bankName) throws Exception {
        List<OperatorUser> userList = getBankAdminListWithAccessAndBank(roleCode, bankName);
        if (userList.size() != 0) {
            return userList.get(0);
        }
        return null;
    }

    public static List<OperatorUser> getBankAdminListWithAccessAndBank(String roleCode, String bankName) throws Exception {
        List<OperatorUser> userList = new ArrayList<>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole wRole : rnrData) {
            if (wRole.ApplicableRoles.contains(roleCode) && wRole.CategoryCode.equalsIgnoreCase(Constants.BANK_ADMIN)) {
                for (OperatorUser usr : map.values()) {
                    if (wRole.RoleName.equals(usr.WebGroupRole)
                            && usr.isPwdReset.equals("Y")
                            && usr.BankName.equalsIgnoreCase(bankName)) {
                        userList.add(usr);
                    }
                }
            }
        }

        // return List of specific permission Users
        return userList;
    }

    public static OperatorUser getBankUserWithAccessAndBank(String roleCode, String bankName) throws Exception {
        List<OperatorUser> userList = getBankUserListWithAccessAndBank(roleCode, bankName);
        if (userList.size() != 0) {
            return userList.get(0);
        }
        return null;
    }

    public static List<OperatorUser> getBankUserListWithAccessAndBank(String roleCode, String bankName) throws Exception {
        List<OperatorUser> userList = new ArrayList<>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole wRole : rnrData) {
            if (wRole.ApplicableRoles.contains(roleCode) && wRole.CategoryCode.equalsIgnoreCase(Constants.BANK_USER)) {
                for (OperatorUser usr : map.values()) {
                    if (wRole.RoleName.equals(usr.WebGroupRole)
                            && usr.isPwdReset.equals("Y")
                            && usr.BankName.equalsIgnoreCase(bankName)) {
                        userList.add(usr);
                    }
                }
            }
        }

        // return List of specific permission Users
        return userList;
    }


    /**
     * Get Operator User List
     *
     * @param rolecode     - access code or the Role code
     * @param categoryCode - Category Code
     * @return
     * @throws Exception
     */
    public static List<OperatorUser> getOperatorUsersWithAccess(String rolecode, String categoryCode) throws Exception {
        List<OperatorUser> userList = new ArrayList<OperatorUser>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific role
         */
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)
                    && rnrData.get(i).CategoryCode.equals(categoryCode)) {
                for (OperatorUser usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y")) {
                        userList.add(usr);
                    }
                }
            }
        }

        if (userList.size() == 0) {
            LOGGER.error("Error: User not found with category: " + categoryCode + " & role: " + rolecode);
            return null;
        }
        // return List of specific permission Users
        return userList;
    }


    /**
     * Get Bulk Payer Admin
     *
     * @param rolecode
     * @param parent
     * @param userNumber
     * @return
     * @throws Exception
     */
    public static OperatorUser getBulkPayerWithAccess(String rolecode, User parent, int... userNumber) throws Exception {
        List<OperatorUser> userList = getListOfBulkPayerAdmin(rolecode, parent);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            String roleName = getRoleNameFromRoleCode(rolecode, Constants.BULK_PAYER_ADMIN);
            try {
                ExtentTest t1 = dFactory.createNode("Base User Creation",
                        "Creating Bulk Payer Admin User With Parent:" + parent.LoginId + " & having role: " + rolecode);
                // Create the User with Specific Role
                OperatorUser newUser = new OperatorUser(Constants.BULK_PAYER_ADMIN);
                newUser.setWebGroupRole(roleName);

                OperatorUserManagement.init(t1)
                        .createBulkPayerAdmin(newUser, parent);

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();
                    return newUser;
                }
            } catch (Exception e) {
                Assertion.markAsFailure("Failed to create Bulk Payer Admin withhaving role: " + rolecode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }

    /**
     * Get list of bulk payer for a specific Enterprise User
     *
     * @param roleCode
     * @param entUser
     * @return
     * @throws Exception
     */
    private static List<OperatorUser> getListOfBulkPayerAdmin(String roleCode, User entUser) throws Exception {

        List<OperatorUser> userList = new ArrayList<>();
        Map<String, OperatorUser> map = getOperatorUserSet();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        for (int i = 0; i < rnrData.size(); i++) {
            if (!rnrData.get(i).ApplicableRoles.contains(roleCode))
                continue;

            for (OperatorUser usr : map.values()) {
                if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) &&
                        usr.isPwdReset.equals("Y") &&
                        usr.getParentUserName().equalsIgnoreCase(entUser.LoginId)) {

                    if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                        userList.add(usr);
                    } else {
                        //ExcelUtil.removeTempOperatorUser(usr);
                    }
                }
            }
        }


        return userList;
    }


    //------------------- G E T   C H A N N E L   U S E R ----------------------------------

    /**
     * Get Operator Users List having Category Code and Status as Active
     *
     * @param categoryCode
     * @return
     * @throws Exception
     */
    public static List<OperatorUser> getOperatorUserListWithCategory(String categoryCode) throws Exception {
        List<OperatorUser> userList = new ArrayList<OperatorUser>();
        Map<String, OperatorUser> map = getOperatorUserSet();

        /*
         * Fetch the List of operator users having specific cat code    */


        for (OperatorUser usr : map.values()) {
            if (usr.CategoryCode.equals(categoryCode) && usr.isPwdReset.equals("Y")) {
                userList.add(usr);
            }
        }

        // return List of specific permission Users
        if (userList.size() == 0) {
            LOGGER.error("Error: User not found with category: " + categoryCode);
        }
        return userList;
    }

    /**
     * Get Channel User with specific Category
     * get the user list from Appdata, if user is now available create a new User and return the same
     *
     * @param catCode    [String] rolecode
     * @param userNumber [optional]- say we need two users of same roleCode and Category but >> different Users
     *                   e.g. getChannelUserWithAccess("SUBS", 0); will fetch the 1st User
     *                   e.g. getChannelUserWithAccess("SUBS", 1); will fetch the 2nd User
     * @return [User] Mobiquity User
     * NOTE : userNumber starts from 0
     * @throws Exception
     */
    public static User getChannelUserWithCategory(String catCode, int... userNumber) throws Exception {

        List<User> userList = getChannelUserListWithCategory(catCode);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            try {
                System.out.println("Base Set user needed with category: " + catCode);
                ExtentTest t1 = dFactory.createNode("Base User Creation",
                        "Creating User With Category:" + catCode);
                // Create the User with Specific Role
                User newUser = new User(catCode);

                if (catCode.equals(Constants.SUBSCRIBER)) {
                    SubscriberManagement.init(t1)
                            .createBaseSetSubscriber(newUser);
                } else {
                    ChannelUserManagement.init(t1)
                            .createBaseSetChannelUser(newUser);
                }

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();

                    if (catCode.equals(Constants.SUBSCRIBER)) {
                        TransactionManagement.init(t1)
                                .makeSureLeafUserHasBalance(newUser);
                    } else {
                        TransactionManagement.init(t1)
                                .makeSureChannelUserHasBalance(newUser);
                    }
                    return newUser;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Assertion.markAsFailure("Failed to create User With Category:" + catCode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }


    public static User getSubscriberUser(int... userNo) throws Exception {
        if (userNo.length > 0) {
            return getChannelUserWithCategory(Constants.SUBSCRIBER, userNo[0]);
        } else {
            return getChannelUserWithCategory(Constants.SUBSCRIBER);
        }
    }

    /**
     * Get Channel User with Specific Role,
     * get the user list from Appdata, if user is now available create a new User and return the same
     *
     * @param rolecode   [String] rolecode
     * @param userNumber [optional] say we need two users of same roleCode but the different Users
     *                   e.g. getChannelUserWithAccess("ROLE_CODE", 0); will fetch the 1st User
     *                   e.g. getChannelUserWithAccess("ROLE_CODE", 1); will fetch the 2nd User
     * @return [User] Mobiquity User
     * NOTE : userNumber starts from 0
     * @throws Exception
     */
    public static User getChannelUserWithAccess(String rolecode, int... userNumber) throws Exception {
        List<User> userList = getChannelUserListWithAccess(rolecode);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            String catCode = getCategoryCodeFromRoleCode(rolecode);
            String roleName = getRoleNameFromRoleCode(rolecode);
            try {
                ExtentTest t1 = dFactory.createNode("BaseSet User Creation",
                        "Creating User With Category:" + catCode + " & having role: " + rolecode);
                // Create the User with Specific Role
                User newUser = new User(catCode);
                newUser.setWebRoleName(roleName);

                if (catCode.equals(Constants.SUBSCRIBER)) {
                    SubscriberManagement.init(t1)
                            .createBaseSetSubscriber(newUser);
                } else {
                    ChannelUserManagement.init(t1)
                            .createBaseSetChannelUser(newUser);
                }

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();

                    if (catCode.equals(Constants.SUBSCRIBER)) {
                        TransactionManagement.init(t1)
                                .makeSureLeafUserHasBalance(newUser);
                    } else {
                        TransactionManagement.init(t1)
                                .makeSureChannelUserHasBalance(newUser);
                    }

                    return newUser;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Assertion.markAsFailure("Failed to create User With Category:" + catCode + " & having role: " + rolecode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }

    /**
     * Get Channel User with Specific Role and Category
     * get the user list from Appdata, if user is now available create a new User and return the same
     *
     * @param rolecode   [String] rolecode
     * @param catCode    [String] rolecode
     * @param userNumber [optional]- say we need two users of same roleCode and Category but >> different Users
     *                   e.g. getChannelUserWithAccess("ROLE_CODE", "SUBS", 0); will fetch the 1st User
     *                   e.g. getChannelUserWithAccess("ROLE_CODE", "SUBS", 1); will fetch the 2nd User
     * @return [User] Mobiquity User
     * NOTE : userNumber starts from 0
     * @throws Exception
     */
    public static User getChannelUserWithAccess(String rolecode, String catCode, int... userNumber) throws Exception {
        List<User> userList = getChannelUserListWithAccess(rolecode, catCode);

        // if no user index is passed, set the default index to 0
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // if AppData Has users and count of available user is more than the requested index
        if (userList.size() > uNumber) {
            return userList.get(uNumber);
        }

        // Create a new User with requested Access and return the same
        try {
            String roleName = getRoleNameFromRoleCode(rolecode, catCode);
            ExtentTest t1 = dFactory.createNode("BaseSet User Creation",
                    "Creating User With Category:" + catCode + " & having role: " + rolecode);
            /*
            check for currect RNR
            check for valid category, is role applicable for the Category as per RNR sheet
             */
            List<String> catList = getCategoryListForRoleCode(rolecode);
            if (!catList.contains(catCode)) {
                Assertion.failAndStopTest("Please verify parameters, role is not mapped to category as per RNR", t1);
            }

            // Create the User with Specific Role and Category
            User newUser = new User(catCode);
            newUser.setWebRoleName(roleName);

            if (catCode.equals(Constants.SUBSCRIBER)) {
                SubscriberManagement.init(t1).createBaseSetSubscriber(newUser);
            } else {
                ChannelUserManagement.init(t1).createBaseSetChannelUser(newUser);
            }

            if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                newUser.writeDataToExcel();

                if (catCode.equals(Constants.SUBSCRIBER)) {
                    TransactionManagement.init(t1).makeSureLeafUserHasBalance(newUser);
                } else {
                    TransactionManagement.init(t1).makeSureChannelUserHasBalance(newUser);
                }
                return newUser;
            }
        } catch (Exception e) {
            Assertion.markAsFailure("Failed to create User With Category:" + catCode + " & having role: " + rolecode);
        }
        return null;
    }

    /**
     * Get Merchant User with ONLINE/OFFLINE merchant type
     * get the user list from Appdata, if user is now available create a new User and return the same
     *
     * @param catCode    [String] rolecode
     * @param userNumber [optional]- say we need two users of same roleCode and Category but >> different Users
     *                   e.g. getChannelUserWithAccess("MER", 0); will fetch the 1st User
     *                   e.g. getChannelUserWithAccess("MER", 1); will fetch the 2nd User
     * @return [User] Mobiquity User
     * NOTE : userNumber starts from 0
     * @throws Exception
     */
    public static User getMerchantUserWithOnlineOfflineType(String catCode, Boolean isOnline, int... userNumber) throws Exception {
        List<User> userList = getMerchantUserListWithCategoryAndStatus(catCode, isOnline);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;
        String merType = isOnline ? "ONLINE" : "OFFLINE";

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            try {
                ExtentTest t1 = dFactory.createNode("BaseSet User Creation",
                        "Creating Merchant With Category:" + catCode);
                // Create the User with Specific Role
                User newUser = new User(catCode);
                newUser.setMerchantType(merType);

                ChannelUserManagement.init(t1)
                        .createBaseSetChannelUser(newUser);

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();

                    TransactionManagement.init(t1)
                            .makeSureChannelUserHasBalance(newUser);

                    return newUser;
                }
            } catch (Exception e) {
                Assertion.markAsFailure("Failed to create User With Category:" + catCode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }

    /**
     * Get Channel User Specific Role
     *
     * @param rolecode - Role Code
     * @param grade    - Grade name
     * @return - first User with matching Role And Grade
     * @throws Exception
     */
    public static User getChannelUserWithAccessAndGrade(String rolecode, String grade, int... userNumber) throws Exception {
        List<User> userList = getChannelUserListWithAccessAndGrade(rolecode, grade);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            String roleName = getRoleNameFromRoleCode(rolecode);
            String catCode = getCategoryCodeFromGradeCode(grade);
            try {
                ExtentTest t1 = dFactory.createNode("Base User Creation",
                        "Creating User With Grade:" + grade + " & having role: " + rolecode);
                // Create the User with Specific Role and Category
                User newUser = new User(catCode);
                newUser.setWebRoleName(roleName);

                if (catCode.equals(Constants.SUBSCRIBER)) {

                } else {
                    ChannelUserManagement.init(t1)
                            .createBaseSetChannelUser(newUser);
                }

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();
                    if (catCode.equals(Constants.SUBSCRIBER)) {
                        TransactionManagement.init(t1)
                                .makeSureLeafUserHasBalance(newUser);
                    } else {
                        TransactionManagement.init(t1)
                                .makeSureChannelUserHasBalance(newUser);
                    }
                    return newUser;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Assertion.markAsFailure("Failed to create User With Grade Code:" + grade + " & having role: " + rolecode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }


    public static User getChannelUserWithOutAccess(String rolecode, ExtentTest pNode) throws Exception {
        List<User> userList = new ArrayList<User>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch Channel users doesn't having specific role
         */
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)) {
            } else {
                for (User usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y")) {
                        userList.add(usr);
                    }
                }
            }

        }

        // return specific  Users without permission
        if (userList.size() == 0) {
            LOGGER.error("Error: User not found with " + rolecode + ", please check ChannelUser.xlsx");
            pNode.log(Status.ERROR, "User not found without Access " + rolecode);
        }
        return userList.get(0);
    }


    public static OperatorUser getOptUserWithOutAccess(String rolecode, ExtentTest pNode) throws Exception {
        List<OperatorUser> userList = new ArrayList<OperatorUser>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet(true);

        /*
         * Fetch operator users doesn't having specific role
         */
        for (int i = 0; i < rnrData.size(); i++) {
            if (rnrData.get(i).ApplicableRoles.contains(rolecode)) {
            } else {
                for (OperatorUser usr : map.values()) {
                    if (rnrData.get(i).RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y")) {
                        userList.add(usr);
                    }
                }
            }

        }

        // return specific  Users without permission
        if (userList.size() == 0) {
            LOGGER.error("Error: User not found with " + rolecode + ", please check ChannelUser.xlsx");
            pNode.log(Status.ERROR, "User not found without Access " + rolecode);
        }
        return userList.get(0);
    }

    public static OperatorUser getBankAdminWithBank(String bankName) throws Exception {
        List<OperatorUser> userList = new ArrayList<OperatorUser>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, OperatorUser> map = getOperatorUserSet(true);

        /*
         * Fetch operator users doesn't having specific role
         */
        for (OperatorUser usr : map.values()) {
            if (usr.isPwdReset.equals("Y") && usr.BankName.equalsIgnoreCase(bankName) && usr.CategoryCode.equalsIgnoreCase(Constants.BANK_ADMIN)) {
                if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                    return usr;
                } else {
                    ExcelUtil.removeTempOperatorUser(usr);
                }
            }
        }

        // return specific  Users without permission
        if (userList.size() == 0) {
            ExtentTest t1 = dFactory.createNode("BaseSet User Creation",
                    "Creating BankAdmin with Bank: " + bankName);
            OperatorUser opt = new OperatorUser(Constants.BANK_ADMIN);
            opt.setBankName(bankName);
            OperatorUserManagement.init(t1).createAdmin(opt);
            opt.writeDataToExcel();
        }
        return null;
    }


    /**
     * It will return the Channel user and set Error if no user found
     * test parameter is  optional
     *
     * @param rolecode
     * @return List<User>
     * @throws Exception
     */
    private static List<User> getChannelUserListWithAccess(String rolecode) throws Exception {
        List<User> userList = new ArrayList<User>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole role : rnrData) {
            if (!role.ApplicableRoles.contains(rolecode))
                continue;

            for (User usr : map.values()) {
                if (role.RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equalsIgnoreCase("Y")) {
                    if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                        userList.add(usr);
                    } else {
                        ExcelUtil.deleteAppDataUser(usr.MSISDN);
                    }
                }
            }
        }

        return userList;
    }

    /**
     * Get Category Code from Role Code
     *
     * @param roleCode
     * @return
     */
    private static String getCategoryCodeFromRoleCode(String roleCode) {
        for (WebGroupRole role : GlobalData.rnrDetails) {

            if (role.ApplicableRoles.contains(roleCode)) {
                return role.CategoryCode;
            }
        }
        return null;
    }

    /**
     * Get list of categories whicha which has specific permission as per RNR Sheet
     *
     * @param roleCode
     * @return
     */
    private static List<String> getCategoryListForRoleCode(String roleCode) {
        List<String> categoryList = new ArrayList<>();
        for (WebGroupRole role : GlobalData.rnrDetails) {

            if (role.ApplicableRoles.contains(roleCode)) {
                categoryList.add(role.CategoryCode);
            }
        }
        return categoryList;
    }

    public static String getRoleNameFromRoleCode(String roleCode, String... category) {
        String catCode = category.length > 0 ? category[0] : null;
        for (WebGroupRole role : GlobalData.rnrDetails) {

            if (catCode != null && catCode.equalsIgnoreCase(role.CategoryCode) && role.ApplicableRoles.contains(roleCode)) {
                return role.RoleName;
            } else if (role.ApplicableRoles.contains(roleCode) && catCode == null) {
                return role.RoleName;
            }
        }
        return null;
    }

    private static String getCategoryCodeFromGradeCode(String gradeCode) {
        for (List<GradeDB> gradeList : GlobalData.gradeCategoryDBMap.values()) {
            for (GradeDB objGrade : gradeList) {
                if (objGrade.GradeCode.equals(gradeCode)) {
                    return objGrade.CategoryCode;
                }
            }

        }
        return null;
    }

    /**
     * It will return the Channel user and set Error if no user found
     * test parameter is  optional
     *
     * @param rolecode
     * @param categoryCode
     * @return List<User>
     * @throws Exception
     */
    private static List<User> getChannelUserListWithAccess(String rolecode, String categoryCode) throws Exception {
        List<User> userList = new ArrayList<User>();
        List<WebGroupRole> rnrData = GlobalData.rnrDetails;
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole role : rnrData) {
            if (!role.ApplicableRoles.contains(rolecode) || !role.CategoryCode.equals(categoryCode))
                continue;

            for (User usr : map.values()) {
                if (role.RoleName.equals(usr.WebGroupRole) && usr.isPwdReset.equals("Y")) {

                    if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                        userList.add(usr);
                    } else {
                        ExcelUtil.deleteAppDataUser(usr.MSISDN);
                    }
                }
            }
        }
        return userList;
    }

    /**
     * Get Channel user list with Category Names
     *
     * @return
     * @throws Exception
     */
    private static List<User> getChannelUserListWithCategory(String categoryCode) throws Exception {
        List<User> userList = new ArrayList<User>();
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (User usr : map.values()) {
            if (usr.isPwdReset.equals("Y") && usr.CategoryCode.equals(categoryCode)) {
                if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                    userList.add(usr);
                } else {
                    ExcelUtil.deleteAppDataUser(usr.MSISDN);
                }
            }
        }

        if (userList.size() == 0) {
            LOGGER.error("Error: Failed to find User with Category '" + categoryCode + "', Please check ChannelUser.xlsx");
        }
        // return List of specific permission Users
        return userList;
    }

    private static List<User> getMerchantUserListWithCategoryAndStatus(String categoryCode, Boolean isOnline) throws Exception {
        List<User> userList = new ArrayList<User>();
        Map<String, User> map = getChannelUserSet(true);
        String merchanType = isOnline ? "ONLINE" : "OFFLINE";

        /*
         * Fetch the List of Merchants users having specific role and merchant type online /offline
         */
        for (User usr : map.values()) {
            if (usr.isPwdReset.equals("Y") && usr.CategoryCode.equals(categoryCode) && usr.MerchantType != null && usr.MerchantType.equalsIgnoreCase(merchanType)) {
                if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                    userList.add(usr);
                } else {
                    ExcelUtil.deleteAppDataUser(usr.MSISDN);
                }
            }
        }

        if (userList.size() == 0) {
            LOGGER.error("Error: Failed to find User with Category '" + categoryCode + "', Please check ChannelUser.xlsx");
        }
        // return List of specific permission Users
        return userList;
    }

    /**
     * Get Channel User with Specifi Role
     *
     * @param rolecode
     * @param grade    {optional}
     * @return User
     * @throws Exception
     */
    private static List<User> getChannelUserListWithAccessAndGrade(String rolecode, String grade) throws Exception {
        List<User> userList = new ArrayList<User>();
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (role.DomainCode.equals(Constants.OPERATOR))
                continue; // skip for operator user Roles

            if (!role.ApplicableRoles.contains(rolecode))
                continue;

            for (User usr : map.values()) {
                if (!role.RoleName.equals(usr.WebGroupRole)
                        || !usr.isPwdReset.equalsIgnoreCase("Y")
                        || !usr.GradeName.equalsIgnoreCase(grade)) {
                    continue;
                }

                if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                    userList.add(usr);
                } else {
                    ExcelUtil.deleteAppDataUser(usr.MSISDN);
                }
            }
        }
        return userList;
    }

    /**
     * For Debugging purpose
     * Pass the MSISDN of channel user already ex
     *
     * @param msisdn
     * @return
     */
    public static User getUserUsingMsisdn(String msisdn) throws Exception {
        Map<String, User> map = getChannelUserSet(true);
        for (User usr : map.values()) {
            if (usr.MSISDN.equals(msisdn)) {
                return usr;
            }
        }
        return null;
    }


    /**
     * Get Role Name From The Group Role Sheet/ Rnr Sheet Base on the Migrated User or New User
     * If migrated users are to be used, there is no point of accessing the newly created or AppData
     * Group Roles, rnr group roles becomes the reference, which has to be filled manually before Execution
     *
     * @param catCode
     * @return
     * @throws Exception
     */
    public static String getWebGroupRoleName(String catCode) throws IOException {
        try {
            String catName = DataFactory.getCategoryName(catCode);
            Map<String, WebGroupRole> map = getGroupRoleSet();
            for (WebGroupRole role : map.values()) {
                if (role.CategoryName.equals(catName) && role.CREATED.equals("Y")) {
                    return role.RoleName;
                }
            }
        } catch (IOException ioe) {
            System.err.println("IO Exception occurred..");
        } catch (Exception ex) {
            System.err.println("Exception occured" + ex);
        }
        return null;
    }

    /********************************************************************************************
     * INSTRUMENT TCP & Methods
     **********************************************************************************************/

    /**
     * Get a Web Group Role Specific to a Category Code
     *
     * @param categoryCode
     * @return -  role which is existing as Base Setup
     */
    public static WebGroupRole getExistingWebRole(String categoryCode) {
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (role.CategoryCode.equals(categoryCode)) {
                return role;
            }
        }
        return null;
    }

    /**
     * read the excel, output filr Instrument TCP
     *
     * @return Map
     * @throws Exception
     */
    public static void loadInstrumentTCP() throws IOException, InvalidFormatException {
        List<InstrumentTCP> listInstTCP = new ArrayList<>();
        InputStream inp = new FileInputStream(FilePath.fileInstrumentTCP);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            String profileName = getCellValueString(nextRow.getCell(0));
            if (profileName.equalsIgnoreCase("")
                    || profileName.equalsIgnoreCase("ProfileName"))
                continue;

            String provider = getCellValueString(nextRow.getCell(1));
            String domainName = getCellValueString(nextRow.getCell(2));
            String categoryName = getCellValueString(nextRow.getCell(3));
            String gradeName = getCellValueString(nextRow.getCell(4));
            String payInstrument = getCellValueString(nextRow.getCell(5));
            String payInstrumentType = getCellValueString(nextRow.getCell(6));

            // get the service Charge Object
            InstrumentTCP instTcp = new InstrumentTCP(profileName, provider, domainName, categoryName, gradeName, payInstrument, payInstrumentType);

            listInstTCP.add(instTcp);
        }
        GlobalData.instrumentTCPs = listInstTCP; // Set to global Variable
    }

    public static InstrumentTCP getInstrumentTCP(String domainName, String categoryName, String gradeName, String providerName, String payInstType) throws Exception {
        for (InstrumentTCP tcp : GlobalData.instrumentTCPs) {
            if (tcp.DomainName.equals(domainName)
                    && tcp.CategoryName.equals(categoryName)
                    && tcp.GradeName.equals(gradeName)
                    && tcp.CurrencyProvider.equals(providerName)
                    && tcp.InstrumentType.equalsIgnoreCase(payInstType)) {
                return tcp;
            }
        }
        return null;
    }

    /**
     * Get the lIst of instrument TCP with the combination of Grade, Domain  and Category
     *
     * @param domainName
     * @param categoryName
     * @param gradeName
     * @return
     * @throws Exception
     */
    public static ArrayList<InstrumentTCP> getInstrumentTCPLinkedToWallet(String domainName, String categoryName, String gradeName) throws Exception {
        int count = GlobalData.instrumentTCPs.size();
        ArrayList<InstrumentTCP> instList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            InstrumentTCP tcp = GlobalData.instrumentTCPs.get(i);
            if (tcp.DomainName.equals(domainName)
                    && tcp.CategoryName.equals(categoryName)
                    && tcp.GradeName.equals(gradeName)
                    && tcp.InstrumentType.equals("WALLET")) {
                instList.add(tcp);
            }
        }
        return instList;
    }

    /*********************************************************************************************
     * Load Grade Category Map
     */

    public static void loadGradeCategoryDBMap() {
        try {
            MobiquityGUIQueries dbConn = new MobiquityGUIQueries();
            // Set the Category and Domain details
            ResultSet result = dbConn.dbFetchGradeCategoryMap();
            HashMap<String, List<GradeDB>> map = new HashMap<>();

            while (result.next()) {
                List<GradeDB> tempList = new ArrayList<>();
                String GradeName = result.getString("GRADE_NAME");
                String GradeCode = result.getString("GRADE_CODE");
                String CategoryCode = result.getString("CATEGORY_CODE");
                GradeDB objGrade = new GradeDB(GradeName, GradeCode, CategoryCode);
                if (map.containsKey(CategoryCode)) {
                    map.get(CategoryCode).add(objGrade);
                } else {
                    tempList.add(objGrade);
                    map.put(CategoryCode, tempList);
                }
            }
            GlobalData.gradeCategoryDBMap = map;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get All Grade Names
     *
     * @return
     */
    public static List<String> getAllGradeNames() {
        List<String> gradeNamelist = new ArrayList<>();

        for (List<GradeDB> tempList : GlobalData.gradeCategoryDBMap.values()) {
            for (GradeDB grade : tempList) {
                gradeNamelist.add(grade.GradeName);
            }
        }
        return gradeNamelist;
    }

    public static List<GradeDB> getGradesForCategory(String categoryCode) {
        System.out.println(categoryCode + "\n");
        List<GradeDB> gradeList = GlobalData.gradeCategoryDBMap.get(categoryCode);
        return gradeList;
    }

    /**
     * Get Grade Code From the Global Data
     *
     * @param gradeName
     * @return
     */
    public static String getGradeCode(String gradeName) {
        for (List<GradeDB> tempList : GlobalData.gradeCategoryDBMap.values()) {
            for (int i = 0; i < tempList.size(); i++) {
                if (tempList.get(i).GradeName.equals(gradeName)) {
                    return tempList.get(i).GradeCode;
                }
            }
        }
        return null;
    }

    /**
     * Return Grade Name of Particular Grade Code
     *
     * @param gradeCode
     * @return
     */
    public static String getGradeName(String gradeCode) {
        for (List<GradeDB> tempList : GlobalData.gradeCategoryDBMap.values()) {
            for (int i = 0; i < tempList.size(); i++) {
                if (tempList.get(i).GradeCode.equals(gradeCode)) {
                    return tempList.get(i).GradeName;
                }
            }
        }
        return null;
    }

    public static String getGradeName(String catCode, String gradeCode) {
        for (List<GradeDB> tempList : GlobalData.gradeCategoryDBMap.values()) {
            for (int i = 0; i < tempList.size(); i++) {
                if (tempList.get(i).GradeCode.equals(gradeCode)) {
                    return tempList.get(i).GradeName;
                }
            }
        }
        return null;
    }

    /**
     * Get Current MSISDN (should be unique in system)
     * Get a range of msisdn and check whether it exists in DB, if exist look for next range else return the unique msisdn
     *
     * @author Navin Pramanik
     */
    public static void loadSystemMsisdn() {
        LOGGER.info("*** Loading System MSISDN ***\n");
        String currentMsisdn = MsisdnProperties.getInstance().getProperty("web.currentmsisdn");
        BigInteger startMSISDN = new BigInteger(currentMsisdn);
        startMSISDN = startMSISDN.add(new BigInteger(DataFactory.getRandomNumberAsString(4)));
        BigInteger range = new BigInteger("1500");
        BigInteger endMSISDN = startMSISDN.add(range);

        String startM = startMSISDN.toString();
        String endM = endMSISDN.toString();

        if (currentMsisdn.charAt(0) == '0') {
            startM = "0" + startMSISDN;
            endM = "0" + endMSISDN;
        }

        GlobalData.msisdnListDB = MobiquityGUIQueries.getAllMSISDNBetRange(startM, endM);
        GlobalData.availableMsisdnList = msisdnNotinMobiquity(startMSISDN.toString(), range);
    }

    public static boolean checkMsisdnExistMobiquity(String msisdn) {
        boolean exist = false;
        for (String dbMSISDN : GlobalData.msisdnListDB) {
            if (msisdn.equalsIgnoreCase(dbMSISDN)) {
                exist = true;
            }
        }
        //To Do
        return exist;
    }

    public static List<String> msisdnNotinMobiquity(String startMsisdn, BigInteger value) {

        String prefix = "";
        if (startMsisdn.startsWith("0")) {
            prefix = "0";
        }

        BigInteger start = new BigInteger(startMsisdn);
        List notMsisdn = new ArrayList();
        BigInteger one = new BigInteger("1");
        for (int j = 0; j < value.intValue(); j++) {
            if (!GlobalData.msisdnListDB.contains(prefix + start.toString())) {
                notMsisdn.add(prefix + start.toString());
            }
            start = start.add(one);
        }
        return notMsisdn;
    }


    /***********************************************************************************
     * B I L L E R
     */

    /***********************************************************************************
     * P a g e   C o d e   a n d   R o l e   C o d e   M a p
     */

    public static HashMap<String, List<String>> getPageCodeRoleCodeMap() throws SQLException {
        HashMap<String, List<String>> codeMap = new HashMap<>();
        MobiquityGUIQueries dbHandle = new MobiquityGUIQueries();
        ResultSet result = dbHandle.fetchRoleCodePageCodeMap();

        while (result.next()) {
            String roleCode = result.getString("ROLE_CODE").trim();
            String pageCode = result.getString("PAGE_CODE").trim();

            if (codeMap.keySet().contains(roleCode)) {
                codeMap.get(roleCode).add(pageCode);
            } else {
                List<String> tempList = new ArrayList<>();
                tempList.add(pageCode);
                codeMap.put(roleCode, tempList);
            }
        }
        return codeMap;
    }

    /**
     * Get the Biller From AppData
     *
     * @return - Biller with Status Y
     */
    public static Biller getBillerFromAppdata() throws Exception {
        List<Biller> billerList = getBillerSet();
        for (Biller usr : billerList) {
            if (usr.getStatus().equalsIgnoreCase(Constants.STATUS_ACTIVE)) {
                // if the user from excel is valid and exist in DB, return the user else return null
                if (MobiquityGUIQueries.checkUserLoginIdExist(usr.LoginId)) {
                    return usr;
                } else {
                    ExcelUtil.deleteBiller(usr);
                }
            }
        }
        return null;
    }

    public static List<Biller> getBillerSet() throws IOException, InvalidFormatException {
        DataFormatter formatter = new DataFormatter();
        List<Biller> billerList = new ArrayList<>();
        // * Read the Excel *

        InputStream inp = new FileInputStream(FilePath.fileTempBillers);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        Iterator<Row> iterator = sheet.iterator();
        int counter = 0; // counter to skip the header
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (counter > 0) {
                String billerCode = formatter.formatCellValue(nextRow.getCell(0));
                if (billerCode.equalsIgnoreCase("")
                        || billerCode.equalsIgnoreCase("BillerCode"))
                    continue;

                String loginId = formatter.formatCellValue(nextRow.getCell(1));
                String password = formatter.formatCellValue(nextRow.getCell(2));
                String categoryCode = formatter.formatCellValue(nextRow.getCell(3));
                String gradeName = formatter.formatCellValue(nextRow.getCell(4));
                String providerName = formatter.formatCellValue(nextRow.getCell(5));
                String billerCategoryName = formatter.formatCellValue(nextRow.getCell(6));
                String tcp = formatter.formatCellValue(nextRow.getCell(7));
                String status = formatter.formatCellValue(nextRow.getCell(9));
                String gradeCode = formatter.formatCellValue(nextRow.getCell(10));
                String serviceLevel = formatter.formatCellValue(nextRow.getCell(11));
                String processType = formatter.formatCellValue(nextRow.getCell(12));
                String billerType = formatter.formatCellValue(nextRow.getCell(13));
                String billAmount = formatter.formatCellValue(nextRow.getCell(14));
                String paymentSubType = formatter.formatCellValue(nextRow.getCell(15));

                Biller user = new Biller(billerCode, categoryCode,
                        gradeName, loginId, password,
                        providerName, billerCategoryName, tcp, gradeCode, status, serviceLevel, processType, billerType, billAmount, paymentSubType);

                billerList.add(user);
            }
            counter++;
        }

        return billerList;

    }

    public static List<Biller> getBarredBillerList() throws Exception {
        List<Biller> usrList = new ArrayList<>();

        List<Biller> billerList = getBillerSet();
        for (Biller usr : billerList) {
            if ((usr.isBarred())) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    /***********************************************************************************
     * S A V I N G S   C L U B
     */
    public static SavingsClub getSavingClubFromAppdata() throws Exception {
        DataFormatter formatter = new DataFormatter();

        // * Read the Excel *
        InputStream inp = new FileInputStream(FilePath.fileTempSavingClub);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);

        if (sheet.getRow(1) == null) {
            return null;
        }

        String clubName = formatter.formatCellValue(sheet.getRow(1).getCell(0));
        String cmMsisdn = formatter.formatCellValue(sheet.getRow(1).getCell(1));
        String location = formatter.formatCellValue(sheet.getRow(1).getCell(2));
        String clubType = formatter.formatCellValue(sheet.getRow(1).getCell(3));
        String cmGradeName = formatter.formatCellValue(sheet.getRow(1).getCell(4));
        String clubWalletTcp = formatter.formatCellValue(sheet.getRow(1).getCell(5));
        String clubBankTcp = formatter.formatCellValue(sheet.getRow(1).getCell(6));
        String clubWalletGrpRole = formatter.formatCellValue(sheet.getRow(1).getCell(7));
        String clubBankGrpRole = formatter.formatCellValue(sheet.getRow(1).getCell(8));
        String bankName = formatter.formatCellValue(sheet.getRow(1).getCell(9));
        String isApproved = formatter.formatCellValue(sheet.getRow(1).getCell(10));
        String clubId = formatter.formatCellValue(sheet.getRow(1).getCell(11));

        SavingsClub club = new SavingsClub(clubName, cmMsisdn, location, clubType, cmGradeName, clubWalletTcp, clubBankTcp, clubWalletGrpRole, clubBankGrpRole, bankName, isApproved, clubId);
        return club;
    }

    /***********************************************************************************
     * REG TYPE
     */
    public static void loadRegistrationTypeDB() {
        try {
            MobiquityGUIQueries dbConn = new MobiquityGUIQueries();
            // Set the Category and Domain details
            ResultSet result = dbConn.getRegistrationType();
            List regTypeList = new ArrayList();

            while (result.next()) {
                regTypeList.add(result.getString("DESCRIPTION"));
            }
            GlobalData.regTypeListDB = regTypeList;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /***********************************************************************************
     * HELP METHODS
     */

    /**
     * Can do max random of 18
     *
     * @param length
     * @return
     */
    public static String getRandomNumberAsString(int length) {
        try {
            Thread.sleep(Constants.THREAD_SLEEP_1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int firstpart, secondPart;
        if (length > 9) {
            firstpart = getRandomNumber(9);
            secondPart = getRandomNumber(length - 9);
            return firstpart + "" + secondPart;
        } else {
            return "" + getRandomNumber(length);
        }

    }

    /**
     * @param length
     * @return
     */
    public static int getRandomNumber(int length) {
        Random r = new Random(System.currentTimeMillis());


        switch (length) {
            case 2: {
                return r.nextInt(20 - 10 + 1) + 10;
            }
            case 3: {
                return r.nextInt(999 - 100 + 1) + 100;
            }
            case 4: {
                return r.nextInt(9999 - 1000 + 1) + 1000;
            }
            case 5: {
                return r.nextInt(99999 - 10000 + 1) + 10000;
            }
            case 6: {
                return r.nextInt(999999 - 100000 + 1) + 100000;
            }
            case 7: {
                return r.nextInt(9999999 - 1000000 + 1) + 1000000;
            }
            case 8: {
                return r.nextInt(99999999 - 10000000 + 1) + 10000000;
            }
            case 9: {
                return r.nextInt(999999999 - 100000000 + 1) + 100000000;
            }
        }
        return length;
    }

    public static String getRandomString(int length) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzhahaha".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    /**
     * Get random String, with prefix and fixed length
     *
     * @param prefix
     * @param length
     * @return
     */
    public static String getRandomNumber(String prefix, int length) {
        if (prefix.length() >= length) {
            return prefix;
        }
        return prefix + getRandomNumberAsString(length - prefix.length());
    }

    /**
     * Get Current Date yyyy-mm-dd
     *
     * @return
     */
    public static String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }

    public static String getCurrentDateTimeExcel() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yy-HH-mm-SS");
        LocalDateTime localDateTime = LocalDateTime.now();
        String curr = dtf.format(localDateTime);
        return curr;
    }

    public static String getCurrentDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:SS");
        LocalDateTime localDateTime = LocalDateTime.now();
        String curr = dtf.format(localDateTime);
        return curr;
    }

    public static String getCurrentDateSlash() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }

    public static String getNextDate(int Days) {
        DateFormat sdf = null;
        Date time1 = null;
        String output = null;
        String date = getCurrentDateSlash();
        try {
            sdf = new SimpleDateFormat("dd/MM/yyyy");
            time1 = sdf.parse(date);
            Calendar c = Calendar.getInstance();
            c.setTime(time1);
            c.add(Calendar.DATE, Days);
            output = sdf.format(c.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output;
    }

    /**
     * Modified by Navin for Checking MSISDN in DB
     *
     * @return
     */
    public static String getMSISDNcounter() {
        BigInteger one = new BigInteger("1");
        MSISDNCounter = MSISDNCounter.add(one);
        MsisdnProperties.getInstance().writeProperty("web.currentmsisdn", MSISDNCounter + "");

        boolean exists = checkMsisdnExistMobiquity(MSISDNCounter.toString());
        //keep getting new msisdn until not exist is found
        while (exists) {
            LOGGER.info("GLOBALDATA msisdn exist " + MSISDNCounter.toString());
            MSISDNCounter = MSISDNCounter.add(one);
            MsisdnProperties.getInstance().writeProperty("web.currentmsisdn", MSISDNCounter + "");
            exists = checkMsisdnExistMobiquity(MSISDNCounter.toString());
        }
        return MSISDNCounter.toString();
    }

    /**
     * The below method will return unique MSISDN
     * from the available MSISDN List from DB
     * and remove the assigned msisdn from the Available List
     *
     * @return
     */
    public static String getAvailableMSISDN() {

        String msisdn = GlobalData.availableMsisdnList.get(0).toString();

        MsisdnProperties.getInstance().writeProperty("web.currentmsisdn", msisdn + "");

        GlobalData.availableMsisdnList.remove(msisdn);

        return msisdn;
    }

    /**
     * get Cell String Value
     *
     * @param cell
     * @return
     */
    public static String getCellValueString(Cell cell) {
        DataFormatter formatter = new DataFormatter();
        if (cell != null) {
            return formatter.formatCellValue(cell);
        } else {
            return "";
        }
    }

    /**
     * Get Decrypted Account Number
     *
     * @param encrptedValue
     * @return
     */
    /*public static String getDecryptedValue(String encrptedValue) {
        return DesEncryptor.getDecryptedBankAccount(encrptedValue);
    }*/

    /**
     * Get Time Stamp
     *
     * @return
     */
    public static String getTimeStamp() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static int generateRandomNumber(long digits) {
        int minimum = (int) Math.pow(10, digits - 1);
        int maximum = (int) Math.pow(10, digits) - 1;
        Random random = new Random();
        return minimum + random.nextInt((maximum - minimum) + 1);
    }


    /**
     * @return
     * @throws Exception
     */
    public static User getAnyChannelUser() throws Exception {

        Map<String, User> map = getChannelUserSet(true);
        /*
         * Fetch the List of Channel users
         */
        for (User usr : map.values()) {
            if (!(usr.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)
                    || usr.CategoryCode.equalsIgnoreCase(Constants.MERCHANT) ||
                    usr.CategoryCode.equalsIgnoreCase(Constants.HEAD_MERCHANT) ||
                    usr.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))
            )
                return usr;
        }
        // return List of specific permission Users
        return null;
    }

    /**
     * Get Channel user list with Category Names
     *
     * @return
     * @throws Exception
     */
    public static String getAccountID(String msisdn) throws Exception {
        Map<String, User> map = getChannelUserSet(true);
        String accountID = null;

        for (User usr : map.values()) {
            if (usr.MSISDN.equals(msisdn)) {
                accountID = usr.DefaultAccNum;
            }
        }
        return accountID;
    }

    public static Map<String, User> getChannelUserSetUniqueGrade() throws Exception {
        Map<String, User> map = new HashMap<String, User>();
        InputStream inp = new FileInputStream(FilePath.fileChannelUsers);
        Workbook wb = WorkbookFactory.create(inp);
        Sheet sheet = wb.getSheetAt(0);
        List<String> allGrades = new ArrayList<>();

        Iterator<Row> iterator = sheet.iterator();
        int counter = 0; // counter to skip the header
        while (iterator.hasNext()) {
            Row nextRow = iterator.next();
            if (counter > 0) {

                String categoryCode = getCellValueString(nextRow.getCell(0));
                String domainCode = getCellValueString(nextRow.getCell(1));
                String firstName = getCellValueString(nextRow.getCell(2));
                String loginId = getCellValueString(nextRow.getCell(3));
                String msisdn = getCellValueString(nextRow.getCell(5));
                String password = getCellValueString(nextRow.getCell(6));
                String gradeCode = getCellValueString(nextRow.getCell(7));
                String webGroupRole = getCellValueString(nextRow.getCell(8));
                String isPwdReset = getCellValueString(nextRow.getCell(11));
                String defaultAccNum = getCellValueString(nextRow.getCell(12));
                String defaultCustId = getCellValueString(nextRow.getCell(13));
                String categoryName = getCellValueString(nextRow.getCell(16));
                String domainName = getCellValueString(nextRow.getCell(17));
                String gradeName = getCellValueString(nextRow.getCell(18));
                String externalCode = getCellValueString(nextRow.getCell(19));
                String idNum = getCellValueString(nextRow.getCell(20));
                String status = getCellValueString(nextRow.getCell(21));
                String zoneName = getCellValueString(nextRow.getCell(22));
                String areaName = getCellValueString(nextRow.getCell(23));
                String mode = getCellValueString(nextRow.getCell(24));
                String mpin = getCellValueString(nextRow.getCell(25));
                String tpin = getCellValueString(nextRow.getCell(26));

                if (isPwdReset.equals("Y")) {
                    // get the service Charge Object
                    User chUser = new User(categoryCode, categoryName, domainCode, domainName, loginId, firstName, msisdn,
                            password, gradeCode, gradeName, webGroupRole, isPwdReset, defaultAccNum, defaultCustId, externalCode,
                            idNum, status, zoneName, areaName, mode, mpin, tpin);

                    // Push it to the Map
                    if (!allGrades.contains(gradeCode)) {
                        map.put(chUser.LoginId, chUser);
                    }
                    allGrades.add(gradeCode);
                }
            }
            counter++;
        }
        return map;
    }

    public static void loadAutomationGeography() {
        GlobalData.autGeography = MobiquityGUIQueries.getAutomationZoneAndArea();
        if (GlobalData.autGeography == null) {

            //TODO remove comment

            //            Assert.fail("No Geography or Area are found in DataBase");
        }
    }

    public static void setDefaultProviderWalletAndBank() {
        GlobalData.defaultProvider = getDefaultProvider();
        GlobalData.defaultWallet = getDefaultWallet();
        GlobalData.defaultBankName = getDefaultBankNameForDefaultProvider();
        GlobalData.normalWallet = DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL);
        GlobalData.savingClubWallet = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB);
        GlobalData.commissionWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);

    }

    public static void loadPartnerDetails() throws SQLException {
        List<Partner> pList = new ArrayList<>();
        Multimap<String, String> result = MobiquityGUIQueries.getThirdPartyMandatoryFields();

        if (!result.isEmpty()) {
            for (String key : result.keySet()) {
                Partner obj = new Partner(key, (List<String>) result.get(key));
                pList.add(obj);
            }
            GlobalData.partnerMap = pList;
        }
    }

    /**
     * Get List of Suspended User
     *
     * @param catCode
     * @param gradeCode
     * @return
     */
    public static List<User> getSuspendedUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.getStatus().equalsIgnoreCase(Constants.STATUS_SUSPEND) &&
                    usr.CategoryCode.equals(catCode) &&
                    usr.GradeCode.equals(gradeCode)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getBlacklistedUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.getStatus().equalsIgnoreCase(Constants.STATUS_BLACKLIST) &&
                    usr.CategoryCode.equals(catCode) &&
                    usr.GradeCode.equals(gradeCode)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    /**
     * Get List of barred users
     *
     * @param catCode   *
     * @param gradeCode
     * @return
     * @throws Exception
     */
    public static List<User> getBarredUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    (usr.getStatus().equals(Constants.BAR_AS_RECIEVER) ||
                            usr.getStatus().equals(Constants.BAR_AS_BOTH) ||
                            usr.getStatus().equals(Constants.BAR_AS_SENDER))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getChurnedUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    usr.getStatus().equals(Constants.AUT_STATUS_CHURNED)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getChurnInitiatedUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    usr.getStatus().equals(Constants.AUT_STATUS_CHURN_INITIATE)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getDeleteInitiateUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    usr.getStatus().equals(Constants.AUT_STATUS_DELETE_INITIATE)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getDeletedUserList(String catCode, String gradeCode) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    usr.getStatus().equals(Constants.AUT_STATUS_DELETED)) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<OperatorUser> getBarredOperatorUserList(String catCode) throws Exception {
        List<OperatorUser> usrList = new ArrayList<>();

        Map<String, OperatorUser> map = getOperatorUserSet(false);
        for (OperatorUser usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) &&
                    (usr.getStatus().equals(Constants.BAR_AS_BOTH))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<Biller> getSuspendedBillerList(String catCode, String barStatus, String gradeCode) throws Exception {
        List<Biller> usrList = new ArrayList<>();

        List<Biller> billers = getBillerSet();
        for (Biller usr : billers) {
            if (usr.CategoryCode.equals(catCode) && usr.GradeCode.equals(gradeCode) &&
                    (usr.getStatus().equals(Constants.BAR_AS_RECIEVER) ||
                            usr.getStatus().equals(Constants.BAR_AS_BOTH) ||
                            usr.getStatus().equals(Constants.BAR_AS_SENDER))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getTempSubscriberList(String status) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(Constants.SUBSCRIBER) &&
                    (usr.getStatus().equals(status))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    /**
     * Get Channel user list with Category Names
     *
     * @return
     * @throws Exception
     */
    private List<User> getChannelUserWithSpecificGrade(String gradeCode) throws Exception {
        List<User> userList = new ArrayList<User>();
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (User usr : map.values()) {
            if (usr.isPwdReset.equals("Y") && usr.GradeCode.equals(gradeCode)) {
                userList.add(usr);
            }
        }
        // return List of specific permission Users
        return userList;
    }


    public static List<User> getTempChannelUserList(String catCode, String status) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) &&
                    (usr.getStatus().equals(status))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static List<User> getTempChannelUserList(String catCode, String gradeCode, String status) throws Exception {
        List<User> usrList = new ArrayList<>();

        Map<String, User> map = getChannelUserSet(false);
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(catCode) &&
                    usr.GradeCode.equals(gradeCode) &&
                    (usr.getStatus().equals(status))) {
                usrList.add(usr);
            }
        }
        return usrList;
    }

    public static User getChannelUserWithCategoryAndGradeCode(String catCode, String gradeCode, int... userNumber) throws Exception {
        List<User> userList = getChannelUserListWithCategoryAndGradeCode(catCode, gradeCode);
        int uNumber = (userNumber.length > 0) ? userNumber[0] : 0;

        // return List of specific permission Users
        if (userList.size() == 0 || userList.size() < uNumber + 1) {
            try {
                ExtentTest t1 = dFactory.createNode("Base User Creation",
                        "Creating User With Grade:" + gradeCode + " & having category: " + catCode);
                // Create the User with Specific Role and Category
                User newUser = new User(catCode, DataFactory.getGradeName(gradeCode));

                if (catCode.equals(Constants.SUBSCRIBER)) {

                } else {
                    ChannelUserManagement.init(t1)
                            .createBaseSetChannelUser(newUser);
                }

                if (newUser.getStatus().equals(Constants.STATUS_ACTIVE)) {
                    newUser.writeDataToExcel();
                    if (catCode.equals(Constants.SUBSCRIBER)) {
                        TransactionManagement.init(t1)
                                .makeSureLeafUserHasBalance(newUser);
                    } else {
                        TransactionManagement.init(t1)
                                .makeSureChannelUserHasBalance(newUser);
                    }
                    return newUser;
                }
            } catch (Exception e) {
                e.printStackTrace();
                Assertion.markAsFailure("Failed to create User With Grade Code:" + gradeCode + " & having category: " + catCode);
            }
        } else {
            return userList.get(uNumber);
        }
        return null;
    }

    private static List<User> getChannelUserListWithCategoryAndGradeCode(String catCode, String gradeCode) throws Exception {
        List<User> userList = new ArrayList<User>();
        Map<String, User> map = getChannelUserSet(true);

        /*
         * Fetch the List of operator users having specific role
         */
        for (WebGroupRole role : GlobalData.rnrDetails) {
            if (role.DomainCode.equals(Constants.OPERATOR)) {
                continue; // skipp for operator user Roles
            }

            for (User usr : map.values()) {
                if (usr.isPwdReset.equals("Y")
                        && usr.GradeCode.equalsIgnoreCase(gradeCode) && usr.CategoryCode.equalsIgnoreCase(catCode)) {

                    if (MobiquityGUIQueries.isUserActiveAndNotBlackListed(usr.MSISDN, usr.CategoryCode)) {
                        userList.add(usr);
                    } else {
                        ExcelUtil.deleteAppDataUser(usr.MSISDN);
                    }
                }
            }
        }
        return userList;
    }

    /**
     * Load Configuration Preference
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    public static void loadConfigPreference() throws IOException, InvalidFormatException {
        int prefSheet = 4;
        LOGGER.info("*** Loading Current Client Specific Preferences ***\n");
        DataFormatter formatter = new DataFormatter();

        // * Read the Excel *
        Sheet sheet = rnrWb.getSheetAt(prefSheet);

        /* get last used row */
        int lastRow = sheet.getLastRowNum();
        for (int i = 1; i < lastRow; i++) {
            String prefCode = formatter.formatCellValue(sheet.getRow(i).getCell(0));
            String prefValue = formatter.formatCellValue(sheet.getRow(i).getCell(1));
            GlobalData.prefMap.put(prefCode, prefValue);
        }
    }

    /**
     * @param domain
     * @param category
     * @param regType
     * @return
     */
    public static List<CustomerTCP> loadRequiredCustomerTCPs(String domain, String category, String regType) {
        List<CustomerTCP> requiredCustomerTCPs = new ArrayList<>();
        CustomerTCP cusTCP = new CustomerTCP(domain, category, regType);
        requiredCustomerTCPs.add(cusTCP);
        return requiredCustomerTCPs;
    }

    public static List<CustomerTCP> loadRequiredCustomerTCPs(String domain, String category, List<String> regType) {
        List<CustomerTCP> requiredCustomerTCPs = new ArrayList<>();
        List<String> providerList = DataFactory.getAllProviderNames();
        for (String rType : regType) {
            for (String provider : providerList) {
                CustomerTCP cusTCP = new CustomerTCP(domain, category, rType);
                cusTCP.ProviderName = provider;
                requiredCustomerTCPs.add(cusTCP);
            }
        }

        return requiredCustomerTCPs;
    }

    public static List<InstrumentTCP> getListOfRequiredInstrumentTCPs(String domainName, String categoryCode, String gradeName, List<String> bankList) {
        Wallet rWallet = DataFactory.getWalletUsingAutomationCode(Wallets.REMMITANCE);
        Wallet cWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);

        List<InstrumentTCP> requiredInstrumentTCPs = new ArrayList<>();
        List<String> allProviders = DataFactory.getAllProviderNames();
        String categoryName = DataFactory.getCategoryName(categoryCode);
        List<String> allWallets = DataFactory.getWalletForUserType(categoryCode);

        List<String> arrCatNotForCommissionWallet = Arrays.asList(
                Constants.SUBSCRIBER,
                Constants.BILL_COMPANY,
                Constants.ENTERPRISE
        );

        for (String mfsProvider : allProviders) {
            for (String wallet : allWallets) {
                String payInstType = wallet;  //inst name
                InstrumentTCP inst = new InstrumentTCP(mfsProvider,
                        domainName,
                        categoryName,
                        gradeName,
                        Constants.PAYINST_WALLET_CONST,
                        payInstType
                );

                // as remittance wallet is not applicable for Subscriber
                if (inst.InstrumentType.equalsIgnoreCase(rWallet.WalletName)
                        && !categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                    continue;

                // as Commission is not for Subscriber, Enterprise, and Bill Company
                if (inst.InstrumentType.equalsIgnoreCase(cWallet.WalletName)
                        && (arrCatNotForCommissionWallet.contains(categoryCode)))
                    continue;

                requiredInstrumentTCPs.add(inst);
            }
        }

        // Bank is not required for International Partner, hence Skip
        if (categoryCode.equalsIgnoreCase(Constants.INT_PARTNER))
            return requiredInstrumentTCPs;

        for (String bank : bankList) {
            String payInstType = bank;  //inst name
            InstrumentTCP inst = new InstrumentTCP(DataFactory.getProviderNameUsingBankName(bank),
                    domainName,
                    categoryName,
                    gradeName,
                    Constants.PAYINST_BANK_CONST,
                    bank
            );
            requiredInstrumentTCPs.add(inst);
        }


        return requiredInstrumentTCPs;
    }

    public static List<String> getWalletForUserType(String catCode) {
        if (catCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            return DataFactory.getWalletForSubscriber();
        } else {
            return DataFactory.getWalletForChannelUser();
        }
    }

    public static String getUniqueLoginID(String catCode, int length) {
        String loginID = null;
        while (true) {
            loginID = catCode + getRandomNumberAsString(length - catCode.length());
            String LoginIDChecker = MobiquityGUIQueries.checkForUniqueLoginID(loginID);
            if (!LoginIDChecker.equals("Y")) {
                break;
            }
        }

        return loginID;
    }


}
