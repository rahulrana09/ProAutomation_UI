package framework.util.common;

import framework.util.globalConstant.FilePath;
import framework.util.propertiesManagement.MfsTestProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by rahul.rana on 5/5/2017.
 */
public class DriverFactory {
    private static final String CHROME = "chrome";
    private static final String IE = "ie";
    private static final String DEFAULT = "firefox";
    public static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = createDriver();
            driver.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
            //driver.manage().timeouts().pageLoadTimeout(9, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        return driver;
    }

    public static WebDriver createDriver() {
        // Uses firefox driver by default
        String browser = MfsTestProperties.getInstance().getProperty("browser.name");
        if (browser == null) {
            browser = DEFAULT;
        }

        if (browser.toLowerCase().equals(CHROME)) {
            /**
             * Chrome
             */
            // ChromeDriverManager.getInstance().setup();
            //ChromeDriverManager.getInstance(). proxy("http://172.16.1.168:8080").setup();
            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

            HashMap<String, Object> chromePrefs = new HashMap<>();
            chromePrefs.put("profile.default_content_settings.popups", 0);
            chromePrefs.put("download.default_directory", FilePath.dirFileDownloads);
            chromePrefs.put(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR,
                    org.openqa.selenium.UnexpectedAlertBehaviour.ACCEPT);

            ChromeOptions options = new ChromeOptions();
            options.setExperimentalOption("prefs", chromePrefs);

            DesiredCapabilities cap = DesiredCapabilities.chrome();
            cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
            cap.setCapability(ChromeOptions.CAPABILITY, options);
            cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
            cap.setCapability("chrome.switches", Arrays.asList("--incognito"));
            cap.setCapability("record_network", "true");

            return new ChromeDriver(cap);
        } else if (browser.toLowerCase().equals(IE)) {
            /**
             * IE Browser Capabilities
             */
            System.setProperty("webdriver.ie.driver", "drivers/IEDriverServer.exe");
            DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
            capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
            capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_ALERTS, true);
            capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
            capabilities.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
            capabilities.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE, true);
//            capabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,"https://www.google.com/");

            capabilities.setCapability("requireWindowFocus", true);
            return new InternetExplorerDriver(capabilities);
        } else {
            /**
             * FireFox
             */
            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");

            FirefoxProfile profile = new FirefoxProfile();
            profile.setAcceptUntrustedCertificates(false);
            profile.setAssumeUntrustedCertificateIssuer(true);
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
            profile.setPreference("browser.download.manager.showWhenStarting", false);
            profile.setPreference("dom.file.createInChild", true);
            profile.setPreference("browser.download.downloadDir", FilePath.dirFileDownloads);

            DesiredCapabilities dc = DesiredCapabilities.firefox();
            dc.setCapability(FirefoxDriver.PROFILE, profile);
            dc.setCapability("marionette", true);

            return new FirefoxDriver(dc);
        }
    }

    public static void closeDriver() {
        driver.close();
    }

    public static void quitDriver() {
        if (driver == null) {
            return;
        }
        driver.quit();
        driver = null;

    }

    public static void quitProcess() {
        Utils.runBatFileThroughCMD(FilePath.fileTXDriver);
    }
}
