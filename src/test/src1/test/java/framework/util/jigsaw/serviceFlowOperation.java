package framework.util.jigsaw;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import framework.util.MultiLineString;
import framework.util.propertiesManagement.MfsTestUtils;

import java.util.concurrent.Future;

import static framework.util.common.ProcessingHelper.fireCallable;
import static framework.util.jigsaw.JigsawOperations.getSFMFlow;
import static framework.util.propertiesManagement.MfsTestUtils.getTenantId;
import static framework.util.propertiesManagement.MfsTestUtils.moneyUri;

public class serviceFlowOperation {
    public static Future<ValidatableResponse> createFlowForCommissionDisbursementToBankTransferAsync(
            String serviceCode, String serviceFlowIdAndNameToReplace) {
        return fireCallable(() -> createFlowForCommissionDisbursement(serviceCode, serviceFlowIdAndNameToReplace));
    }

    public static ValidatableResponse createFlowForCommissionDisbursement(String serviceCode, String serviceFlowIdAndNameToReplace) {
        return moneyUri().contentType(ContentType.XML)
                .body(MultiLineString.replaceServiceCode(serviceCode, serviceFlowIdAndNameToReplace, MultiLineString.multiLineString(
        /*
<serviceFlow id="serviceFlowIdAndNameToReplace" name="serviceFlowIdAndNameToReplace" enabled="true" serviceCode="serviceCodeToReplace">
    <description>Commission Disbursement to channel user's normal Wallet/Bank Account</description>
    <flowMode>NONE</flowMode>
    <steps>
        <step id="serviceRequest.validation" name="serviceRequest.validation" enabled="true"/>
        <step id="identical.txn.detect" name="identical.txn.detect" enabled="true">
            <onResume>
                <subSteps>
                    <subStep id="load.party" name="load.party"/>
                    <subStep id="identical.transaction.pin.validation" name="identical.transaction.pin.validation"/>
                    <subStep id="txn.resume" name="txn.resume"/>
                </subSteps>
            </onResume>
            <onCancel>
                <subSteps>
                    <subStep id="load.party" name="load.party"/>
                    <subStep id="identical.transaction.pin.validation" name="identical.transaction.pin.validation"/>
                    <subStep id="txn.cancel" name="txn.cancel"/>
                </subSteps>
            </onCancel>
        </step>
        <step id="party.validation" name="party.validation" enabled="true"/>
        <step id="initiator.authentication" name="initiator.authentication" enabled="false"/>
        <step id="user.role.validation" name="user.role.validation" enabled="true">
            <role-code>COMMDIS_INITIATE</role-code>
            <party>transactor</party>
        </step>
        <step id="account.validation" name="account.validation" enabled="true"/>
        <step id="transaction.id.generation" name="transaction.id.generation" enabled="true"/>
        <step id="validate.additional.parties" name="validate.additional.parties" enabled="true"/>
        <step id="lta.updation" name="lta.updation" enabled="false"/>
        <step when="['disbursementMethod'] == 'wallet'" id="transaction.posting" name="transaction.posting" enabled="true" eventOnSuccess="transactionIsSuccessful">
            <onError>
                <subSteps>
                    <subStep id="lta.rollback" name="lta.rollback"/>
                </subSteps>
            </onError>
        </step>
        <step when="['disbursementMethod'] == 'bank'" id="transaction.ambiguous" name="transaction.ambiguous" enabled="true" eventOnSuccess="transactionIsAmbiguous"/>
        <step when="['disbursementMethod'] == 'bank'" id="txn.pause" name="txn.pause" enabled="true">
            <onError>
                <subSteps>
                    <subStep id="transaction.clear.fic" name="transaction.clear.fic"/>
                    <subStep id="lta.rollback" name="lta.rollback"/>
                </subSteps>
            </onError>
        </step>
        <step when="['disbursementMethod'] == 'bank'" id="call.eig" name="call.eig" enabled="false" party="receiver" interfaceId="partyId">
            <onError>
                <subSteps>
                    <subStep id="transaction.clear.fic" name="transaction.clear.fic"/>
                    <subStep id="lta.rollback" name="lta.rollback"/>
                </subSteps>
            </onError>
        </step>
    </steps>
</serviceFlow>
           */)))
                .auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId())
                .post("/sfm/admin/serviceFlow")
                .then().statusCode(200);
    }

    // todo development pending
    public void disableEigCall() {
        ValidatableResponse resp = getSFMFlow("RC");
        moneyUri().contentType(ContentType.XML)
                .body(resp)
                .auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId()).then().statusCode(200);

    }

}
