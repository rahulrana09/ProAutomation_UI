package framework.util.jigsaw;

import com.aventstack.extentreports.ExtentTest;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.path.json.config.JsonPathConfig;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.UsrBalance;
import framework.util.JsonPathOperation;
import framework.util.common.ProcessingHelper;
import framework.util.propertiesManagement.MfsTestProperties;
import framework.util.propertiesManagement.MfsTestUtils;
import org.hamcrest.CoreMatchers;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;
import static framework.util.JsonPathOperation.set;
import static framework.util.jigsaw.ServiceRequestContracts.serviceChargePolicyJson;
import static framework.util.propertiesManagement.MfsTestUtils.*;

public class CommonOperations {

    public static final String SHULKA_AUTH_TOKEN = "shulka.auth.token";
    public static final String MONEY_CURRENCY_CODE_PROPERTY = "mfs1.currency.code";
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();
    private static int pollInterval = mfsTestProperties.getIntProperty("notificationMessage.response.pollIntervalInMillis", 200);
    private static int maxPollTime = mfsTestProperties.getIntProperty("notificationMessage.response.maxPollTimeInMillis", 8000);

    public static ValidatableResponse setPasswordAndPinExpiryToLargeValues(Map config) {
        return moneyUri().contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(config)
                .auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId())
                .post("auth/internal/configuration")
                .then().statusCode(200);
    }

    public static ValidatableResponse setTxnConfig(Map config) {
        Map map = new HashMap(config);
        map.put("mfsTenantId", getTenantId());
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(map)
                .post("/txn/internal/configuration")
                .then().statusCode(200);
    }

    public static ValidatableResponse setTxnProperty(Map config) {
        Map map = new HashMap(config);
        map.put("mfsTenantId", getTenantId());
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .body(map)
                .post("/txn/internal/configuration")
                .then();
    }

    public static void deleteAllPricingPolicies() {
        moneyUriInternalApi()
                .header("mfsTenantId", getTenantId())
                .delete("/shulka/internal/pricingPolicies").then().statusCode(200).log().all();
    }

    public static ValidatableResponse saveServiceChargePolicy(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .body(serviceChargePolicyJson(operationsSpecificToCurrency))
                .post("/shulka/serviceChargePolicy")
                .then().log().all().statusCode(200);
    }

    private static JsonPathOperation[] addConfiguredCurrency(JsonPathOperation[] operations) {
        String currencyCode = mfsTestProperties.getProperty(MONEY_CURRENCY_CODE_PROPERTY);
        List<JsonPathOperation> operationList = new ArrayList<JsonPathOperation>(Arrays.asList(operations));
        operationList.add(set("$.currency", currencyCode));
        JsonPathOperation[] jsonPathOperations = new JsonPathOperation[operationList.size()];
        jsonPathOperations = operationList.toArray(jsonPathOperations);
        return jsonPathOperations;
    }

    public static UsrBalance getBalanceForOperatorUsersBasedOnUserId(String userId) {
        ValidatableResponse validatableResponse = moneyUriInternalApi().given().log().all()
                .accept(ContentType.JSON)
                .when()
                .header("mfsTenantId", getTenantId())
                .get("txn/internal/api/operatorBalances?userId=" + userId)
                .then().statusCode(200);
        JsonPath balanceString = validatableResponse.extract().jsonPath().using(new JsonPathConfig(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        return new UsrBalance(new BigDecimal(balanceString.getString("availableBalance")),
                new BigDecimal(balanceString.getString("fic")),
                new BigDecimal(balanceString.getString("frozen")));
    }

    public static ValidatableResponse getTransactionDetails(String transactionId) {
        return moneyUriInternalApi().basePath("/txn/internal/api/transaction/" + transactionId)
                .header("mfsTenantId", getTenantId()).get().then().log().all();
    }

    public static ValidatableResponse setUMSProperties(Map properties) {
        return moneyUriInternalApi().accept(ContentType.JSON)
                .parameters(properties)
                .log().all()
                .header("mfsTenantId", getTenantId())
                .get("/ums/internal/admin/setProperty")
                .then().log().all().statusCode(200);
    }

    public static ValidatableResponse executeStubForGetOperation(String uri) {
        return moneyUriStubApi()
                .accept(ContentType.JSON)
                .log().all().get(uri).then().log().all();
    }

    public static ValidatableResponse setShulkaConfig(Map config) {
        Map map = new HashMap(config);
        map.put("mfsTenantId", getTenantId());
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(map)
                .post("/shulka/internal/configuration")
                .then().statusCode(200);
    }

    public static Future<ValidatableResponse> setServiceValidatorPropertiesAsync(Map config) {
        return ProcessingHelper.fireCallable(() -> setServiceValidatorProperties(config));
    }

    public static ValidatableResponse setServiceValidatorProperties(Map properties) {
        return moneyUriInternalApi().accept(ContentType.JSON)
                .parameters((properties))
                .header("mfsTenantId", getTenantId())
                .get("/sv/internal/admin/setProperty")
                .then().statusCode(200);
    }

    public static UsrBalance getWalletBalanceForOperatorUsersBasedOnWalletNumber(String walletNumber) {
        ValidatableResponse validatableResponse = moneyUriInternalApi().given().log().all()
                .accept(ContentType.JSON)
                .when()
                .header("mfsTenantId", getTenantId())
                .get("txn/internal/api/operatorWalletBalances?walletNumber=" + walletNumber)
                .then().statusCode(200);
        JsonPath balanceString = validatableResponse.extract().jsonPath().using(new JsonPathConfig(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
        return new UsrBalance(new BigDecimal(balanceString.getString("availableBalance")),
                new BigDecimal(balanceString.getString("fic")),
                new BigDecimal(balanceString.getString("frozen")));
    }

    public static BigDecimal getAvailableBalanceForOperatorUsersBasedOnWalletNo(String walletNo) {
        String balanceString = moneyUriInternalApi().given().log().all()
                .accept(ContentType.JSON)
                .when()
                .header("mfsTenantId", getTenantId())
                .get("/txn/internal/api/operatorWalletBalances?walletNumber=" + walletNo)
                .then().statusCode(200).extract().jsonPath().using(new JsonPathConfig(JsonPathConfig.NumberReturnType.BIG_DECIMAL)).getString("availableBalance");
        return new BigDecimal(balanceString);
    }

    public static ValidatableResponse checkReconciliationMismatch() {
        return moneyUri().accept(ContentType.JSON).auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId())
                .get("/txn/internal/api/reconciliation")
                .then().statusCode(200).log().all();
    }

    public static ValidatableResponse fetchOperatorBalance(String operatorWalletId) {
        return moneyUri().accept(ContentType.JSON).auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId())
                .get("/txn/internal/api/operatorWalletBalances?walletNumber=" + operatorWalletId)
                .then().statusCode(200).log().all();
    }

    public static ValidatableResponse getRecentNotification(String mobNum) {
        return moneyUri().accept(ContentType.JSON).auth().preemptive().basic(MfsTestUtils.getInternalApiUsername(), MfsTestUtils.getInternalApiPassword())
                .header("mfsTenantId", getTenantId())
                .get("/notification/internal/getMessage?toWhom=" + mobNum + "")
                .then().statusCode(200).log().all();
    }

    private static ValidatableResponse getNotificationMessagesResponse(Map requestParameters) {
        return moneyUriInternalApi().accept(ContentType.JSON)
                .parameters(requestParameters)
                .get("/notification/internal/getMessages")
                .then().statusCode(200).log().all();
    }

    private static ValidatableResponse getNotificationMessages(Map requestParameters) {
        ValidatableResponse validatableResponse = getNotificationMessagesResponse(requestParameters);
        return validatableResponse;
    }

    public static void validateRecentNotification(Map requestParameters, String matcher1, ExtentTest test) {
//        List<Argument> listMessages = getNotificationMessages(requestParameters).extract().body().
//                jsonPath().getList("messages");

        await().atMost(maxPollTime, TimeUnit.MILLISECONDS).pollInterval(pollInterval, TimeUnit.MILLISECONDS)
                .until(() -> verifyNotificationMessageInTheList(requestParameters, matcher1, test),
                        CoreMatchers.equalTo(true));
//
//        await().atMost(maxPollTime, TimeUnit.MILLISECONDS).pollInterval(pollInterval, TimeUnit.MILLISECONDS)
//                .until(() ->getNotificationMessages(requestParameters).root("messages").body(listMessages,
//                        response -> equalTo(matcher1)));
//
//        List<String> listMessages1 = getNotificationMessages(requestParameters).extract().body().
//                jsonPath().getList("messages");
//
//        int count = 0;
//        for (String message : listMessages1) {
//            if (message.equals(matcher1)) {
//                count++;
//            }
//        }
//        if (count >= 2) {
//            test.pass("Recent Notification Received");
//        } else {
//            test.fail("Recent Notification Not Received");
//        }
    }

    public static boolean verifyNotificationMessageInTheList(Map requestParameters, String matcher1, ExtentTest test){
        List<String> listMessages1 = getNotificationMessages(requestParameters).extract().body().
                jsonPath().getList("messages");

        int count = 0;
        for (String message : listMessages1) {
            if (message.equals(matcher1)) {
                count++;
            }
        }
        if (count >= 2) {
            test.pass("Recent Notification Received");
            return true;
        } else {
            test.fail("Recent Notification Not Received");
            return false;
        }
    }
}




