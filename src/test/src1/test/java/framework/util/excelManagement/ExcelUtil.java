/*
 *  COPYRIGHT: Comviva Technologies Pvt. Ltd.
 *  This software is the sole property of Comviva
 *  and is protected by copyright law and international
 *  treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of
 *  it may result in severe civil and criminal penalties
 *  and will be prosecuted to the maximum extent possible
 *  under the law. Comviva reserves all rights not
 *  expressly granted. You may not reverse engineer, decompile,
 *  or disassemble the software, except and only to the
 *  extent that such activity is expressly permitted
 *  by applicable law notwithstanding this limitation.
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT
 *  WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 *  INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
 *  OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY
 *  AND THE USE OF THIS SOFTWARE. Comviva SHALL NOT BE LIABLE FOR
 *  ANY DAMAGES WHATSOEVER ARISING OUT OF THE USE OF OR INABILITY TO
 *  USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 *  Author Name: Automation Team
 *  Date: 5-8-2017
 *  Purpose: Excel Related operations
 */
package framework.util.excelManagement;

import com.csvreader.CsvWriter;
import framework.entity.Biller;
import framework.entity.CustomerBill;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tests.core.pOne_v4_16.Suite_P1_AuditAdminTrials;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class ExcelUtil {

    private static final int THREAD_SLEEP_50 = 50;
    private static final int RANDOM_NO_5 = 5;
    private static final int RANDOM_NO_3 = 3;
    private static final int RANDOM_NO_6 = 6;
    private static final Logger logger = LoggerFactory.getLogger(ExcelUtil.class);
    //Bulk Channel user Output Header File
//    private static final String[] NewModulebulkChUserUploadHeader = {
//            "NamePrefix (Mr/Mrs/Miss/M/s)*",
//            "FirstName*",
//            "LastName*",
//            "Identification Number*",
//            "City",
//            "State",
//            "Country",
//            "Email*",
//            "Designation",
//            "Contact Person",
//            "Contact Number",
//            "Gender (Male/Female)*",
//            "Date of Birth*(dd/mm/yyyy)",
//            "Registration Form Number",
//            "Web login Id*",
//            "User Status*(A/M/N)",
//            "MSISDN*",
//            "Preferred Language*",
//            "User Category*",
//            "Owner MSISDN*",
//            "Parent MSISDN*",
//            "Geography Domain Code*",
//            "Group Role Code*",
//            "Mobile Group Role Id*",
//            "Grade Code*",
//            "TCP Profile Id*",
//            "Primary Account (Y/N)*",
//            "Customer Id",
//            "Account Number",
//            "Account Type",
//            "Wallet Type/Linked Bank Status* (A/M/S/D)",
//            "Merchant Type(ONLINE/OFFLINE)",
//            "IdIssueDate",
//            "IdExpiryDate",
//            "IdIssueCountry",
//            "ResidenceCountry",
//            "PostalCode",
//            "Nationality",
//            "EmployerName",
//            "IdExpiresAllowed",
//            "_shortName",
//            "_address1",
//            "_address2",
//            "Url(Semicolon Separated)",
//            "Block Notification(0/1/2/3/4/5/6/7)",
//            "Address*",
//            "IDType*",
//            "IDNumber*",
//            "Nickname*",
//            "Relationship Officer*"
//    };

    /**
     * New Bulk Channel User registartion header list
     */
    private static final String[] NewModulebulkChUserUploadHeader = {
            "NamePrefix (Mr/Mrs/Miss/M/s)*",
            "FirstName*",
            "LastName*",
            "Identification Number*",
            "City",
            "State",
            "Country",
            "Email*",
            "Designation",
            "Contact Person",
            "Contact Number",
            "Gender (Male/Female)*",
            "Date of Birth*(dd/mm/yyyy)",
            "Registration Form Number",
            "Web login Id*",
            "User Status*(A/M/N)",
            "MSISDN*",
            "Preferred Language*",
            "User Category*",
            "Owner MSISDN*",
            "Parent MSISDN*",
            "Geography Domain Code*",
            "Group Role Code*",
            "Mobile Group Role Id*",
            "Grade Code*",
            "TCP Profile Id*",
            "Primary Account (Y/N)*",
            "Customer Id",
            "Account Number",
            "Account Type",
            "Wallet Type/Linked Bank Status* (A/M/S/D)",
            "Merchant Type(ONLINE/OFFLINE)",
            "Supports Online Trans Reversal(Y/N)",
            "IdIssueDate",
            "IdExpiryDate",
            "IdIssueCountry",
            "ResidenceCountry",
            "PostalCode",
            "Nationality",
            "EmployerName",
            "IdExpiresAllowed",
            "_shortName",
            "_address1",
            "_address2",
            "Url(Semicolon Separated)",
            "Block Notification(0/1/2/3/4/5/6/7)",
            "Address*",
            "IDType*",
            "IDNumber*",
            "Nickname*",
            "Relationship Officer*",
            "Autosweep Allowed (Y/N)",
            "ContactIDType",
            "AllowedDays",
            "AllowedFormTime",
            "AllowedToTime",
            "CommercialField1",
            "CommercialField2",
            "CommercialField3",
            "CommercialField4",
            "CommercialField5",
            "CommercialField6",
            "CommercialField7",
            "CommercialField8",
            "CommercialField9",
            "CommercialField10",
            "CommercialField11",
            "ShopName"
    };


    //Bulk Channel User Modification Header
    private static final String[] NewModulebulkChUserModificationUploadHeader = {
            "NamePrefix (Mr/Mrs/Miss/M/s)*",
            "FirstName*",
            "LastName*",
            "Identification Number*",
            "City",
            "State",
            "Country",
            "Email*",
            "Designation",
            "Contact Person",
            "Contact Number",
            "Gender (Male/Female)*",
            "Date of Birth*(dd/mm/yyyy)",
            "Registration Form Number",
            "Web login Id*",
            "User Status*(A/M/N)",
            "MSISDN*",
            "Preferred Language*",
            "User Category*",
            "Owner MSISDN*",
            "Parent MSISDN*",
            "Geography Domain Code*",
            "Group Role Code*",
            "Mobile Group Role Id*",
            "Grade Code*",
            "TCP Profile Id*",
            "Primary Account (Y/N)*",
            "Customer Id",
            "Account Number",
            "Account Type",
            "Wallet Type/Linked Bank Status* (A/M/S/D)",
            "Merchant Type(ONLINE/OFFLINE)",
            "Supports Online Trans Reversal(Y/N)",
            "IdIssueDate",
            "IdExpiryDate",
            "IdIssueCountry",
            "ResidenceCountry",
            "PostalCode",
            "Nationality",
            "EmployerName",
            "IdExpiresAllowed",
            "Url(Semicolon Separated)",
            "Block Notification(0/1/2/3/4/5/6/7)",
            "Relationship Officer*",
            "IsMerchant(Y/N)",
            "MerchantCategoryID",
            "DescriptionForMerchant",
            "ShowOnLocatorForMerchant(Y/N)",
            "IsAgent(Y/N)",
            "AgentCategoryID",
            "DescriptionForAgent",
            "ShowOnLocatorForAgent(Y/N)",
            "Latitude",
            "Longitude",
            "Autosweep Allowed (Y/N)",
            "ContactIDType",
            "AllowedDays",
            "AllowedFormTime",
            "AllowedToTime",
            "CommercialField1",
            "CommercialField2",
            "CommercialField3",
            "CommercialField4",
            "CommercialField5",
            "CommercialField6",
            "CommercialField7",
            "CommercialField8",
            "CommercialField9",
            "CommercialField10",
            "CommercialField11",
            "ShopName"
    };

    //Bulk Subscriber Header File
    // TODO - this is only due to issue MON-8357, both Registration and Modificaiotn must have same CSV headers
    private static final String[] NewModulebulkSubModifyUploadHeader = {
            "NamePrefix (Mr./Mrs./Ms/M/S)*",
            "FirstName*",
            "LastName*",
            "MSISDN*",
            "IdentificationNumber*",
            "FormNumber",
            "DateofBirth(dd/MM/yyyy)*",
            "Gender (Male/Female)*",
            "Address",
            "District",
            "City*",
            "State",
            "Country",
            "Description",
            "Preferred Language*",
            "Web Group Role Id*",
            "Login Id*",
            "Type of Identity Proof",
            "Type of Address Proof",
            "Type of Photo Proof",
            "Mobile Group Role Id*",
            "Grade Code*",
            "TCP Profile Id*",
            "Primary Account (Y/N)*",
            "Customer Id",
            "Account Number",
            "Account Type",
            "Wallet Type/Linked Bank Status* (A/M/S/D)",
            "User Status*(A/M/N)",
            "MiddleName",
            "Nationality",
            "Id Type",
            "IMT Id No",
            "Issue Place",
            "IssuedCountryCode",
            "ResidencyCountryCode",
            "IssuedDate(dd/MM/yyyy)",
            "IsIDExpires(TRUE/FALSE)",
            "ExpireDate(dd/MM/yyyy)",
            "PostalCode",
            "EmployerName",
            "Kin First Name",
            "Kin Last Name",
            "Kin Middle Name",
            "Kin Relationship",
            "Kin Nationality",
            "Kin Contact Number",
            "Kin Identification Number",
            "Kin DOB",
            "Email",
            "IMT Enable(Y/N)",
            "IMT ID Type",
            "Enable WU Services(Y/N)",
            "Enable MoneyGram Services(Y/N)",
            "Birth Country",
            "Passport Issue Country",
            "Passport Issue City",
            "Passport Issue Date",
            "Occupation",
            "Birth City",
            "PartnerName",
            "Region",
            "SecondName",
            "HusbandLastName",
            "MaritalStatus",
            "ContactPerson",
            "ContactNo",
            "BankCode",
            "BankAccountName",
            "BankAccountNumber"
    };

    private static final String[] NewModulebulkSubUploadHeader = {
            "NamePrefix (Mr./Mrs./Ms/M/S)*",
            "FirstName*",
            "LastName*",
            "MSISDN*",
            "Identification Number*",
            "FormNumber",
            "DateofBirth(dd/MM/yyyy)*",
            "Gender (Male/Female)*",
            "Address",
            "District",
            "City*",
            "State",
            "Country",
            "Description",
            "Preferred Language*",
            "Web Group Role Id*",
            "Login Id*",
            "Type of Identity Proof",
            "Type of Address Proof",
            "Type of Photo Proof",
            "Mobile Group Role Id*",
            "Grade Code*",
            "TCP Profile Id*",
            "Primary Account (Y/N)*",
            "Customer Id",
            "Account Number",
            "Account Type",
            "Wallet Type/Linked Bank Status* (A/M/S/D)",
            "User Status*(A/M/N)",
            "MiddleName",
            "Id Type*",
            "IsIMTEnable(Y/N)",
            "IMT Id Type",
            "IMT Id No",
            "PlaceOfIDIssued",
            "IssuedCountryCode",
            "ResidencyCountryCode",
            "Nationality",
            "IssuedDate(dd/MM/yyyy)",
            "IsIDExpires(TRUE/FALSE)",
            "ExpireDate(dd/MM/yyyy)",
            "PostalCode",
            "EmployerName",
            "Occupation",
            "WU Enable(Y/N)",
            "MoneyGram Enable(Y/N)",
            "Birth City",
            "Birth Country",
            "PassportIssueCountryCode",
            "PassportIssueCity",
            "PassportIssueDate(dd/MM/yyyy)",
            "Kin First Name",
            "Kin Last Name",
            "Kin Middle Name",
            "Kin Relationship",
            "Kin Nationality",
            "Kin Contact Number",
            "Kin Identification Number",
            "Kin DOB",
            "Region",
            "NickName"

    };

    private static final String[] tempBillerHeader = {
            "BillerCode",
            "LoginId",
            "Password",
            "CategoryCode",
            "GradeName",
            "ProviderName",
            "BillerCategoryName",
            "TCP",
            "IsCreated",
            "Status",
            "GradeCode",
            "ServiceLevel",
            "ProcessType"
    };
    /**
     * Added by Navin for CIT Report
     */
    private static final String[] citReportExcelHeader = {
            "BUILD_ID",
            "Lead-Name",
            "Test-Framework-IP",
            "Test-Framework-Name",
            "Test-Framework-SVN-Path",
            "Test-Execution-Date-Time",
            "Product-Interface",
            "Unique-TestCase-ID",
            "Test-Case-Description",
            "Test-Status"
    };
    //Bulk Bank Association Header File
    static String[] BulkBankAssoci = {
            "MSISDN*",
            "Bank Id*",
            "Provider Id*",
            "User Type*"
    };
    //Bulk Bank Association for Econet Header File
    static String[] BulkBankAssociEconet = {
            "MSISDN*",
            "Bank Id*",
            "Provider Id*",

    };
    //BulkCustomerRegistrationCore file
    private static String[] BulkCustomerRegistrationCore = {"NamePrefix (Mr./Mrs./Ms/M/S)*", "FirstName*", "LastName*",
            "MSISDN*", "IdentificationNumber*", "FormNumber", "DateofBirth(dd/MM/yyyy)*", "Gender (Male/Female)*",
            "Address", "District", "City*", "State", "Country", "Description", "Preferred Language*",
            "Web Group Role Id*", "Login Id*", "Type of Identity Proof", "Type of Address Proof", "Type of Photo Proof",
            "Mobile Group Role Id*", "Grade Code*", "TCP Profile Id*", "Primary Account (Y/N)*", "Customer Id",
            "Account Number", "Account Type", "Wallet Type/Linked Bank Status* (A/M/S/D)", "User Status*(A/M/N)",
            "MiddleName", "Nationality", "Id Type", "Id No", "PlaceOfIDIssued", "IssuedCountryCode",
            "ResidencyCountryCode", "IssuedDate(dd/MM/yyyy)", "IsIDExpires(TRUE/FALSE)", "ExpireDate(dd/MM/yyyy)",
            "PostalCode", "EmployerName"};
    // group Role output file
    private static String[] groupRoleFileExcelHeader = {
            "Domain",
            "Category",
            "Role Type",
            "Role Name",
            "grade Name",
            "MFS Provider",
            "Payment Instrument",
            "Wallet Type or Linked Banks",
            "CREATED",
            "ERROR",
            "REMARK"
    };
    private static String[] mobileRoleFileExcelHeader = {
            "MobileRoleName",
            "MobileRoleCode",
            "Domain",
            "Category",
            "GradeName",
            "MFSProvider",
            "PayInstrument",
            "PayInstrumentType",
            "Status"
    };
    // -- CCE / CA / NA / BA Operator User OutPutFile
    private static String[] operatorUserFileHeader = {
            "CategoryCode",
            "FirstName",
            "LastName",
            "MSISDN",
            "BankName",
            "LoginID",
            "Password",
            "WebRoleName",
            "UserCreated",
            "UserApproved",
            "passwordChanged",
            "ERROR",
            "REMARK",
            "Status",
            "ParentUserName"
    };
    // Transfer Rule outPut File
    private static String[] transferRuleOutputFile = {
            "Service Name",
            "From MFS Provider",
            "From Domain",
            "From Payment Instrument",
            "From Wallet Type Linked Bank",
            "To MFS Provider",
            "To Domain",
            "To Payment Instrument",
            "To Wallet Type Linked Bank",
            "FromCategory",
            "ToCategory",
            "CREATED",
            "ERROR",
            "REMARK"
    };
    // TCP output FIle
    private static String[] instTCPOutputFile = {
            "ProfileName",
            "Provider",
            "DomainName",
            "CategoryName",
            "GradeName",
            "PayInstrument",
            "PayInstrumentType",
            "CREATED",
            "APPROVED",
            "ERROR",
            "REMARK"
    };
    private static String[] custTCPOutputFile = {
            "ProfileName",
            "DomainName",
            "CategoryName",
            "RegistrationType",
            "CREATED",
            "APPROVED",
            "ERROR",
            "REMARK"
    };
    //Channel user Output Header File
    private static String[] chaneluserOutputFile = {
            "CategoryCode",
            "DomainCode",
            "First Name",
            "login ID",
            "Bank Name",
            "MSISDN",
            "Password",
            "GradeCode",
            "WebGroupRole",
            "userCreated",
            "UserApproved",
            "PasswordChanged",
            "DefaultBankAccount",
            "DefaultBankCustId",
            "ERROR",
            "REMARK",
            "CategoryName",
            "DomainName",
            "GradeName",
            "ExternalCode",
            "IDNUM",
            "Status",
            "Zone",
            "Area",
            "MercType",
            "mpin",
            "tpin",

    };
    //Service Charge output file
    private static String[] serviceChargeOutputFile = {
            "ServiceName",
            "ProfileName",
            "FromProvider",
            "FromDomain",
            "FromPayInst",
            "FromPayInstType",
            "ToProvider",
            "ToDomain",
            "ToPayInst",
            "ToPayInstType",
            "SenderGrade",
            "ReceiverGrade",
            "PayEntity",
            "ERROR",
            "REMARK",
            "IsNew"
    };
    //Subscriber output file
    private static String[] subscriberOutputFileHeader = {
            "MSISDN",
            "FirstName",
            "IdNumber",
            "customerID",
            "AccountNumber",
            "CreatedBy",
            "CREATED",
            "Approved",
            "ERROR",
            "REMARK"
    };
    //Channel user Output Header File
    private static String[] bulkChUserUploadHeader = {
            "NamePrefix (Mr/Mrs/Miss/M/s)*",
            "FirstName*",
            "LastName*",
            "Identification Number*",
            "City",
            "State",
            "Country",
            "Email*",
            "Designation",
            "Contact Person",
            "Contact Number",
            "Gender (Male/Female)*",
            "Date of Birth*(dd/mm/yyyy)",
            "Registration Form Number",
            "Web login Id*",
            "User Status*(A/M/N)",
            "MSISDN*",
            "Preferred Language*",
            "User Category*",
            "Owner MSISDN*",
            "Parent MSISDN*",
            "Geography Domain Code*",
            "Group Role Code*",
            "Mobile Group Role Id*",
            "Grade Code*",
            "TCP Profile Id*",
            "Primary Account (Y/N)*",
            "Customer Id",
            "Account Number",
            "Account Type",
            "Wallet Type/Linked Bank Status* (A/M/S/D)",
            "Merchant Type(ONLINE/OFFLINE)",
            "IdIssueDate",
            "IdExpiryDate",
            "IdIssueCountry",
            "ResidenceCountry",
            "PostalCode",
            "Nationality",
            "EmployerName",
            "IdExpiresAllowed",
            "_address1",
            "_shortName",
            "_address2",
            "Url(Semicolon Separated)",
            "Block Notification(0/1/2/3/4/5/6/7)"
    };
    //Bar Suspend user Output Header File
    private static String[] barsuspenduserOutputFile = {
            "Domain Code",
            "Category Code",
            "First Name",
            "Last Name",
            "MSISDN",
            "login ID",
            "Password",
            "userCreated",
            "Status",
            "External Code",
            "ERROR",
            "REMARK"
    };
    //This is the name of the file to be opened
    protected String fileName = "";
    private Workbook Localwb;
    private InputStream Localinp;

    /**
     * Create to have the faster execution of reading
     *
     * @param filename -> Pass filename to open
     */
    public ExcelUtil(String filename) {
        this.fileName = filename;
        try {
            Localinp = new FileInputStream(fileName);
            Localwb = WorkbookFactory.create(Localinp);
        } catch (Exception e) {
            System.err.println("Exception Occurred in ExcelUtil.");
            logger.error("Exception Occurred in ExcelUtil.");
            e.printStackTrace();
        }

    }

    /**
     * @param filename FileName
     * @param rowt     Row
     * @param cellt    Cell
     * @param textt    Text
     */
    public static void WriteDataToExcel(String filename, int rowt, int cellt, String textt, int... index) throws IOException {
        int sheetId = (index.length > 0) ? index[0] : 0;
        InputStream inpx = null;
        FileOutputStream fileOut = null;
        try {
            inpx = createExcel(inpx, filename);

            XSSFWorkbook wbx = new XSSFWorkbook(inpx);
            XSSFSheet sheet = wbx.getSheetAt(sheetId);
            XSSFCell cell;
            XSSFRow row = sheet.getRow(rowt);

            if (row == null) {
                //logger.info("Row is null");
                sheet.createRow(rowt);
                row = sheet.getRow(rowt);
                row.createCell(cellt);
                cell = row.getCell(cellt);
            } else {
                row.createCell(cellt);
                cell = row.getCell(cellt);
            }

            cell.setCellValue("" + textt);

            fileOut = new FileOutputStream(filename);
            wbx.write(fileOut);

        } catch (IOException ex) {
            logger.error("Error occurred in Excel Util....");
            ex.printStackTrace();
        } finally {
            if (inpx != null)
                inpx.close();

            if (fileOut != null)
                fileOut.close();
        }
    }


    /**
     * This method is written to remove the nested try catch
     * As per analysis of Sonar Report this method is created and
     * called in WriteDataToExcel method
     *
     * @param inputStream
     * @param filename
     * @return
     * @throws IOException
     */
    private static InputStream createExcel(InputStream inputStream, String filename) throws IOException {

        try {
            inputStream = new FileInputStream(filename);
            //logger.info("File already exist " + filename);
        } catch (java.io.FileNotFoundException ex) {
            //logger.info("File does not exist creating file :- " + filename);

            // Create Blank workbook
            XSSFWorkbook workbook = new XSSFWorkbook();
            // Create file system using specific name
            FileOutputStream out = new FileOutputStream(new File(filename));

            workbook.createSheet("new sheet");
            // write operation workbook using file out object
            workbook.write(out);
            out.close();

            inputStream = new FileInputStream(filename);
            //logger.info("File Opened " + filename);
        }
        return inputStream;
    }

    /**
     * To get last row of the excels
     *
     * @param fileName
     * @return Row Number
     */
    public static int getExcelLastRow(String fileName, int... index) throws IOException {
        int sheetId = (index.length > 0) ? index[0] : 0;
        InputStream inp = null;
        try {
            int val = 0;
            inp = new FileInputStream(fileName);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(sheetId);
            int maxRowNo = sheet.getLastRowNum();
            val = maxRowNo;
            return val + 1;

        } catch (InvalidFormatException ex) {
            return 0;
        } catch (IOException ex) {
            return 0;
        } finally {
            inp.close();
        }
    }

    /**
     * To get the number of sheets in the file
     *
     * @param Filename
     * @return
     * @throws Exception
     */
    public static int getNoOfSheet(String Filename)
            throws Exception {
        InputStream inp = new FileInputStream(Filename);

        Workbook wb = WorkbookFactory.create(inp);

        int val = wb.getNumberOfSheets();

        inp.close();
        return val;
    }

    /**
     * return the data in data in specific  cell of sheeet
     *
     * @param filename FileName to Read
     * @param rowt     Rows of Excel
     * @param cellt    Cell of Excel
     * @return value
     * @throws IOException
     * @throws EncryptedDocumentException
     */
    public static String ReadDataFromExcel(String filename, int rowt, int cellt) throws EncryptedDocumentException, IOException, org.apache.poi.openxml4j.exceptions.InvalidFormatException {
        InputStream inp = new FileInputStream(filename);
        Workbook wb = WorkbookFactory.create(inp);
        DataFormatter formatter = new DataFormatter();


        Sheet sheet = wb.getSheetAt(0);

        Row row = sheet.getRow(rowt);
        Cell cellRoleCode = row.getCell(cellt);
        String val = formatter.formatCellValue(cellRoleCode);

        inp.close();
        return val;
    }


    /**
     * To get last column of the excel
     *
     * @param fileName - Name of Excel File
     * @param row      - Row
     * @return Last Column No
     */
    public static int getExcelLastCol(String fileName, int row) throws Exception {
        try {
            int sheetNum = 0;

            InputStream inp = new FileInputStream(fileName);

            Workbook wb = WorkbookFactory.create(inp);

            Sheet sheet = wb.getSheetAt(sheetNum);

            Row r = sheet.getRow(row);
            int maxCell = r.getLastCellNum();

            inp.close();

            return maxCell;
        } catch (Exception ex) {
            return 0;
        }

    }

    /**
     * Delete a Excel FIle
     *
     * @param filePath
     */
    public static void deleteExcel(String filePath) {

        File file = new File(filePath);
        if (file.delete()) {
            logger.info(file.getName() + " is deleted!");
        } else {
            logger.info("Delete operation is failed.");
        }

    }

    /**
     * write header to WebGroupRole OutPut Files
     */
    public static void writeGroupRoleFileHeaders() throws IOException {
        writeHeader(FilePath.fileGroupRoles, groupRoleFileExcelHeader);
    }

    /**
     * write header to WebGroupRole OutPut Files
     */
    public static void writeMobileRoleFileHeaders() throws IOException {
        writeHeader(FilePath.fileMobileRoles, mobileRoleFileExcelHeader);
    }

    /**
     * write Operator User's Headers
     */
    public static void writeOperatorUserFileHeaders() throws IOException {
        writeHeader(FilePath.fileOperatorUsers, operatorUserFileHeader);
    }

    /**
     * write header to the Customer TCP outPut file
     */
    public static void writeCustomerTcpFileHeaders() throws IOException {
        writeHeader(FilePath.fileConsumerTCP, custTCPOutputFile);
    }

    /**
     * method write header to the Instrument TCP outPut file
     */
    public static void writeInstrumentTcpFileHeaders() throws IOException {
        writeHeader(FilePath.fileInstrumentTCP, instTCPOutputFile);
    }

    /**
     * write headers in the users excel
     */
    public static void writeChannelUserHeaders() throws IOException {
        writeHeader(FilePath.fileChannelUsers, chaneluserOutputFile);
    }

    public static void writeTempUserHeaders() throws IOException {
        writeHeader(FilePath.fileTempUsers, chaneluserOutputFile);
    }

    public static void writeTempOptUserHeaders() throws IOException {
        writeHeader(FilePath.fileTempOptUsers, operatorUserFileHeader);
    }

    /**
     * this method will write output to the subscribers file
     */
    public static void writeSubscribersHeaders() throws IOException {
        writeHeader(FilePath.fileLeafUsers, subscriberOutputFileHeader);
    }

    /**
     * Write Temp biller's header
     */
    public static void writeTempBillerHeader() throws IOException {
        writeHeader(FilePath.fileTempBillers, tempBillerHeader);
    }

    /**
     * write CIT report excel header
     */
    public static void writeCITReportFileHeader() throws IOException {
        writeHeader(FilePath.CIT_REPORT_OUTPUT_FILE, citReportExcelHeader);
    }

    /**
     * Write Header
     *
     * @param filePath   : Path to the file
     * @param headerList List of the header of the File
     */
    private static void writeHeader(String filePath, String[] headerList) throws IOException {
        logger.info("Writing file headers " + filePath);
        Path path = Paths.get(filePath);

        if (!Files.exists(path)) {
            logger.info("File does not exist: " + path);
            for (int i = 0; i < headerList.length; i++) {
                ExcelUtil.WriteDataToExcel(filePath, 0, i, headerList[i]);
            }
        } else {
            logger.info("File already exist: " + path);
        }
    }

    /**
     * Get Bulk User Registration File
     *
     * @return FileName
     */
    public static String getBulkUserRegistrationFile() {
        return FilePath.dirFileUploads + "BulkUserRegistration" + DataFactory.getRandomNumber(RANDOM_NO_5) + ".csv";
    }

    /**
     * Write Data to Bulk user registration file
     *
     * @param csvOutputFile - Output file
     * @param fields        Fields of the CSV
     */
    public static void writeDataToBulkUserRegisModifCsv(String csvOutputFile, String[] fields, boolean isRegistration, boolean... isneedToAppend) throws InterruptedException {
        boolean appendRequired = isneedToAppend.length > 1 ? isneedToAppend[1] : false;
        //define output file name
        if (!appendRequired)
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserRegId");
        //check if file exist
        boolean isFileExist = new File(csvOutputFile).exists();
        CsvWriter fObj = null;
        try {
            //create a FileWriter constructor to open a file in appending mode
            fObj = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
            //write header column if the file did not already exist
            if (!isFileExist) {
                if (isRegistration) {
                    for (int i = 0; i < NewModulebulkChUserUploadHeader.length; i++) {
                        fObj.write(NewModulebulkChUserUploadHeader[i].trim());
                    }
                } else {
                    for (int i = 0; i < NewModulebulkChUserModificationUploadHeader.length; i++) {
                        fObj.write(NewModulebulkChUserModificationUploadHeader[i].trim());
                    }
                }

                fObj.endRecord();
            }

            // Write the fields to CSV
            for (int i = 0; i < fields.length; i++) {
                fObj.write(fields[i].trim());
                Utils.putThreadSleep(THREAD_SLEEP_50);
            }
            fObj.endRecord();

        } catch (IOException e) {
            logger.error("Error occured in method:-> writeDataToBulkUserRegisModifCsv" + e);
        } finally {
            if (fObj != null)
                fObj.close();
        }
    }

    /**
     * Write Data to Bulk user registration file
     *
     * @param csvOutputFile - Output file
     * @param fields        Fields of the CSV
     */
    public static void writeDataToBulkUserRegistrationCsv(String csvOutputFile, String[] fields, boolean isRegistration, boolean... isneedToAppend) throws InterruptedException {
        boolean appendRequired = isneedToAppend.length > 1 ? isneedToAppend[1] : false;
        //define output file name
        if (!appendRequired)
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserRegId");
        //check if file exist
        boolean isFileExist = new File(csvOutputFile).exists();
        CsvWriter fObj = null;
        try {
            //create a FileWriter constructor to open a file in appending mode
            fObj = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
            //write header column if the file did not already exist
            if (!isFileExist) {
                if (isRegistration) {
                    for (int i = 0; i < NewModulebulkChUserUploadHeader.length; i++) {
                        fObj.write(NewModulebulkChUserUploadHeader[i].trim());
                    }
                } else {
                    for (int i = 0; i < NewModulebulkChUserModificationUploadHeader.length; i++) {
                        fObj.write(NewModulebulkChUserModificationUploadHeader[i].trim());
                    }
                }

                fObj.endRecord();
            }

            // Write the fields to CSV
            for (int i = 0; i < fields.length; i++) {
                fObj.write(fields[i].trim());
                Utils.putThreadSleep(THREAD_SLEEP_50);
            }
            fObj.endRecord();

        } catch (IOException e) {
            logger.error("Error occured in method:-> writeDataToBulkUserRegisModifCsv" + e);
        } finally {
            if (fObj != null)
                fObj.close();
        }
    }

    /**
     * Write Data to Bulk Subscriber Registration file
     *
     * @param csvOutputFile - Output file
     * @param fields        Fields of the CSV
     */
    public static void writeDataToBulkSubscriberRegistrationCsv(String csvOutputFile, String[] fields, boolean isAppend) throws InterruptedException {
        //define output file name
        if (!isAppend)
            FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkSubRegId");
        //check if file exist
        boolean isFileExist = new File(csvOutputFile).exists();
        try {
            //create a FileWriter constructor to open a file in appending mode
            CsvWriter fObj = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
            //write header column if the file did not already exist
            if (!isFileExist) {
                for (int i = 0; i < NewModulebulkSubUploadHeader.length; i++) {
                    fObj.write(NewModulebulkSubUploadHeader[i]);
                }
                fObj.endRecord();
            }

            // Write the fields to CSV
            for (int i = 0; i < fields.length; i++) {
                fObj.write(fields[i]);
                Thread.sleep(50);
            }
            fObj.endRecord();
            fObj.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeDataToBulkBankAssociCsv(String csvOutputFile, String[] fields, boolean... needToAppend) throws InterruptedException {
        boolean appendRequired = needToAppend.length > 0 ? needToAppend[0] : false;
        if (AppConfig.isUserTypeAllowed) {
            //define output file name
            if (!appendRequired)
                FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserBankAssociation");
            //check if file exist
            boolean isFileExist = new File(csvOutputFile).exists();
            try {
                //create a FileWriter constructor to open a file in appending mode
                CsvWriter fObj = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
                //write header column if the file did not already exist
                if (!isFileExist) {
                    for (int i = 0; i < BulkBankAssoci.length; i++) {
                        fObj.write(BulkBankAssoci[i]);
                    }
                    fObj.endRecord();
                }

                // Write the fields to CSV
                for (int i = 0; i < fields.length; i++) {
                    fObj.write(fields[i]);
                    Thread.sleep(50);
                }
                fObj.endRecord();
                fObj.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (!AppConfig.isUserTypeAllowed) {
            //define output file name
            if (!appendRequired)
                FilePath.deleteFilesForPathByPrefix(FilePath.dirFileDownloads, "BulkUserBankAssociationEconet");
            //check if file exist
            boolean isFileExist = new File(csvOutputFile).exists();
            try {
                //create a FileWriter constructor to open a file in appending mode
                CsvWriter fObj = new CsvWriter(new FileWriter(csvOutputFile, true), ',');
                //write header column if the file did not already exist
                if (!isFileExist) {
                    for (int i = 0; i < BulkBankAssociEconet.length; i++) {
                        fObj.write(BulkBankAssociEconet[i]);
                    }
                    fObj.endRecord();
                }

                // Write the fields to CSV
                for (int i = 0; i < fields.length; i++) {
                    fObj.write(fields[i]);
                    Thread.sleep(50);
                }
                fObj.endRecord();
                fObj.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * @param billList - Biller List
     * @param file     File to write
     * @throws IOException - If any IO Exception occurred during Excel open Close
     */
    public static void writeDataToBulkBillerAssociationCsv(List<CustomerBill> billList, String file) throws IOException {
        BufferedWriter out = null;
        try {
            // Write the fields to CSV
            out = new BufferedWriter(new FileWriter(file, true));
            for (CustomerBill bill : billList) {
                out.write(bill.ProviderId + "," +
                        bill.CustomerMsisdn + "," +
                        bill.BillerCode + "," +
                        bill.BillAccNum + "," +
                        bill.Remark + "\n");
            }
            out.newLine();


        } catch (IOException e) {
            logger.error("IO Exception occured in method: writeDataToBulkBillerAssociationCsv" + e.getMessage());
            e.printStackTrace();
        } finally {
            if (out != null)
                out.close();
        }
    }

    /**
     * Add Data to Add branch CSV for Band addition
     *
     * @throws IOException -IO Exception throw if any
     */
    public static void writeDataToAddBranchCsv() throws IOException {
        BufferedWriter out = null;
        try {
            // Write the fields to CSV
            out = new BufferedWriter(new FileWriter(FilePath.fileAddBranch, false));

            DateAndTime.getTimeStampDayAndTime();
            out.write("BranchCode" + "," +
                    "BranchName" + "\n" +
                    //DataFactory.getRandomNumberAsString(RANDOM_NO_6) + "," +
                    DateAndTime.getTimeStampDayAndTime() + "," +
                    DataFactory.getRandomNumberAsString(RANDOM_NO_3) +
                    DataFactory.getRandomString(RANDOM_NO_3) + "\n");

            out.newLine();

        } catch (IOException e) {
            logger.error("Error occurred in method: writeDataToAddBranchCsv " + e.getMessage());
        } finally {
            if (out != null)
                out.close();
        }
    }

    /**
     * Get Table as Array - use this method for creating a Data Provider
     *
     * @param FilePath  - TestData file path
     * @param sheetName - 0 for Sheet 1
     * @return
     * @throws Exception
     */
    public static Object[][] getTableArray(String FilePath, int sheetName) throws Exception {
        DataFormatter formatter = new DataFormatter();
        String[][] tabArray = null;

        try {

            FileInputStream inp = new FileInputStream(FilePath);
            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(sheetName);
            int startRow = 1;
            int startCol = 0;
            int ci, cj;

            // get total roles
            int totalRows = sheet.getLastRowNum();
            Row r = sheet.getRow(1);

            // get total number of columns
            int totalCols = r.getLastCellNum();

            tabArray = new String[totalRows][totalCols];
            ci = 0;
            for (int i = startRow; i <= totalRows; i++, ci++) {
                cj = 0;
                for (int j = startCol; j < totalCols; j++, cj++) {
                    Cell cell = sheet.getRow(i).getCell(j);
                    if (cell == null) {
                        tabArray[ci][cj] = "";
                    } else {
                        tabArray[ci][cj] = formatter.formatCellValue(cell);
                    }
                }
            }

        } catch (Exception e) {
            logger.error("Could not read the Excel sheet");
            e.printStackTrace();
        }

        return (tabArray);
    }

    public static Object[] readFieldData(String sheetName) {
        ArrayList<String> list = new ArrayList<>();
        try {
            FileInputStream input = new FileInputStream(FilePath.dirTestData + Suite_P1_AuditAdminTrials.class.getSimpleName() + ".xlsx");
            Workbook wb = WorkbookFactory.create(input);
            Sheet sheet = wb.getSheet(sheetName);
            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i);
                Cell cel = row.getCell(0);
                list.add(cel.getStringCellValue());

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to read from the file check Test Data");
        }
        Object[] data = list.toArray();
        Arrays.sort(data);
        return data;
    }

    /**
     * Only delete the Channel Users
     *
     * @param filepath
     * @throws Exception
     */
    public static void deleteChannelUser(String filepath) throws Exception {
        Map<String, User> map = DataFactory.getChannelUserSet(true);
        deleteExcel(filepath);
        writeChannelUserHeaders();
        for (User usr : map.values()) {
            if (usr.CategoryCode.equals(Constants.SUBSCRIBER) || usr.CategoryCode.equals(Constants.ENTERPRISE)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Only delete the Biller Users
     *
     * @param biller
     * @throws Exception
     */
    public static void deleteBiller(Biller biller) throws Exception {
        List<Biller> billers = DataFactory.getBillerSet();
        deleteExcel(FilePath.fileTempBillers);
        writeTempBillerHeader();
        for (Biller usr : billers) {
            if (!usr.BillerCode.equals(biller.BillerCode)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Delete Temp User which is no longer Valid
     *
     * @param tempUser - User to be deleted
     * @throws Exception
     */
    public static void removeTempUser(User tempUser) throws Exception {
        Map<String, User> map = DataFactory.getChannelUserSet(false);
        deleteExcel(FilePath.fileTempUsers);
        writeTempUserHeaders();
        for (User usr : map.values()) {
            if (!usr.MSISDN.equals(tempUser.MSISDN)) {
                usr.writeDataToExcel(true);
            }
        }
    }

    /**
     * Delete Temp User which is no longer Valid
     *
     * @param tempUser - User to be deleted
     * @throws Exception
     */
    public static void removeTempOperatorUser(OperatorUser tempUser) throws Exception {
        Map<String, OperatorUser> map = DataFactory.getOperatorUserSet(false);
        deleteExcel(FilePath.fileTempOptUsers);
        writeTempUserHeaders();
        for (OperatorUser usr : map.values()) {
            if (!usr.MSISDN.equals(tempUser.MSISDN)) {
                usr.writeDataToExcel(true);
            }
        }
    }

    public static void clearBulkPayAdminfromAppData() throws Exception {
        Map<String, OperatorUser> map = DataFactory.getOperatorUserSet(true);
        deleteExcel(FilePath.fileOperatorUsers);
        writeOperatorUserFileHeaders();
        for (OperatorUser usr : map.values()) {
            if (!usr.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Only delete the Subscriber Users
     *
     * @param filepath
     * @throws Exception
     */
    public static void deleteSubscriberUser(String filepath) throws Exception {
        Map<String, User> map = DataFactory.getChannelUserSet(true);
        deleteExcel(filepath);
        writeChannelUserHeaders();
        for (User usr : map.values()) {
            if (!usr.CategoryCode.equals(Constants.SUBSCRIBER)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Only delete the enterPrise Users
     *
     * @param filepath
     * @throws Exception
     */
    public static void deleteEnterPriseUser(String filepath) throws Exception {
        Map<String, User> map = DataFactory.getChannelUserSet(true);
        deleteExcel(filepath);
        writeChannelUserHeaders();
        for (User usr : map.values()) {
            if (!usr.CategoryCode.equals(Constants.ENTERPRISE)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Only delete the enterPrise Users
     *
     * @throws Exception
     */
    public static void deleteAppDataUser(String msisdn) throws Exception {
        Map<String, User> map = DataFactory.getChannelUserSet(true);
        deleteExcel(FilePath.fileChannelUsers);
        writeChannelUserHeaders();
        for (User usr : map.values()) {
            if (!usr.MSISDN.equals(msisdn)) {
                usr.writeDataToExcel();
            }
        }
    }

    /**
     * Read data from the open excel
     *
     * @param rowt
     * @param cellt
     * @return
     */
    public String readExcel(int rowt, int cellt) {
        DataFormatter formatter = new DataFormatter();

        Sheet sheet = Localwb.getSheetAt(0);

        Row row = sheet.getRow(rowt);
        Cell cellRoleCode = row.getCell(cellt);
        String val = formatter.formatCellValue(cellRoleCode);
        return val;
    }

    /**
     * Close the opened excel
     */
    public void close() {
        try {
            Localinp.close();
        } catch (IOException e) {
            logger.error("Error while closing file..");
        }
    }
}
