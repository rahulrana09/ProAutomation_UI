package framework.util.dbManagement;

import framework.util.propertiesManagement.MfsTestProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;

/**
 * Created by rahul.rana on 5/4/2017.
 */
public class OracleDB {
    private static final Logger LOGGER = LoggerFactory.getLogger(OracleDB.class);
    private static Connection connection;
    private static Connection connection_infra;
    public ResultSet rset;
    public ResultSet rset_infra;
    MfsTestProperties properties = MfsTestProperties.getInstance();
    private String url, username, password;
    private String url_infra,username_infra,password_infra;
    private Statement stmt;
    private Statement stmt_infra;

    public OracleDB() {
        url = "jdbc:oracle:thin:@" + properties.getProperty("db.ip") + ":" + properties.getProperty("db.port") + ":" + properties.getProperty("db.sid");
        username = properties.getProperty("db.username");
        password = properties.getProperty("db.password");
    }

    public void OracleDB_infra() {
        url_infra = "jdbc:oracle:thin:@" + properties.getProperty("db.ip") + ":" + properties.getProperty("db.port") + ":" + properties.getProperty("db.sid");
        username_infra = properties.getProperty("db.infra.username");
        password_infra = properties.getProperty("db.infra.password");
    }

    /**
     * Close Oracle connection
     *
     * @throws IOException
     */
    public static void CloseConnection() throws IOException {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e1) {
                LOGGER.error("Query Execution Error");
                e1.printStackTrace();
            }
        }

    }

    public static void CloseConnection_infra() throws IOException {
        if (connection_infra != null) {
            try {
                connection_infra.close();
                connection_infra = null;
            } catch (SQLException e1) {
                LOGGER.error("Query Execution Error");
                e1.printStackTrace();
            }
        }

    }


    /**
     * Open DB Connection
     */
    public void OpenDBConnection() {
        try {
            // Load the JDBC driver
            String driverName = "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);
            // Create a connection to the database
            System.out.println("url: " + url + "\n");
            System.out.println("username:" + username + "\n");
            System.out.println("pwd: " + password + "\n");
            connection = DriverManager.getConnection(url, username, password);
            stmt = connection.createStatement();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found from database");
            e.printStackTrace();
        } catch (SQLException e1) {
            System.out.println("ORACLE Connection error ");
            e1.printStackTrace();
        }
    }

    public void OpenDBConnection_infra() {
        try {
            // Load the JDBC driver
            String driverName = "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);
            // Create a connection to the database
            System.out.println("url: " + url_infra + "\n");
            System.out.println("username:" + username_infra + "\n");
            System.out.println("pwd: " + password_infra + "\n");
            connection_infra = DriverManager.getConnection(url_infra, username_infra, password_infra);
            stmt_infra = connection_infra.createStatement();
        } catch (ClassNotFoundException e) {
            System.out.println("Class not found from database");
            e.printStackTrace();
        } catch (SQLException e1) {
            System.out.println("ORACLE Connection error ");
            e1.printStackTrace();
        }
    }

    /**
     * Execute a Query
     *
     * @param Query
     * @return
     * @throws IOException
     */
    public ResultSet RunQuery(String Query) throws IOException {
        System.out.print("Query: " + Query + "\n");
        try {
            if (connection == null) {
                OpenDBConnection();
            }
            stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            rset = stmt.executeQuery(Query);
        } catch (SQLException e1) {
            LOGGER.error("Query Execution Error");
            e1.printStackTrace();
            CloseConnection();
        }
        return rset;
    }

    public ResultSet RunQuery_infra(String Query) throws IOException {
        System.out.print("Query: " + Query + "\n");
        try {
            if (connection_infra == null) {
                OracleDB_infra();
                OpenDBConnection_infra();
            }
            stmt_infra = connection_infra.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            rset_infra = stmt_infra.executeQuery(Query);
        } catch (SQLException e1) {
            LOGGER.error("Query Execution Error");
            e1.printStackTrace();
            CloseConnection_infra();
        }
        return rset_infra;
    }

    public ResultSet RunQueryPreparedStatement(String Query) {
        //User user = null;
        ResultSet rs = null;
        try {
            if (connection == null) {
                OpenDBConnection();
            }

            PreparedStatement stmt = connection.prepareStatement(Query);
            rs = stmt.executeQuery();


        } catch (SQLException e1) {
            LOGGER.error("Query Execution Error");
            e1.printStackTrace();
        }

        return rs;
    }

    public ResultSet RunUpdateQuery(String Query) throws IOException {
        LOGGER.info("DQuery:" + Query + "\n");
        try {
            if (connection == null) {
                OpenDBConnection();
            }
            stmt = connection.createStatement();
            stmt.executeUpdate(Query);
        } catch (SQLException e1) {
            LOGGER.error("Query Execution Error");
            e1.printStackTrace();
        }
        return rset;
    }
}
