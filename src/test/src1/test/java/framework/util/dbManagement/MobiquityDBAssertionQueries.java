/*
* ******************************************************************************
*  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
*  <p>
*  This software is the sole property of Comviva and is protected by copyright
*  law and international treaty provisions. Unauthorized reproduction or
*  redistribution of this program, or any portion of it may result in severe
*  civil and criminal penalties and will be prosecuted to the maximum extent
*  possible under the law. Comviva reserves all rights not expressly granted.
*  You may not reverse engineer, decompile, or disassemble the software, except
*  and only to the extent that such activity is expressly permitted by
*  applicable law notwithstanding this limitation.
*  <p>
*  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
*  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
*  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
*  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
*  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
*  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
*  OF THE POSSIBILITY OF SUCH DAMAGE.
*  ******************************************************************************
*   Author Name: Automation Team
 *  Date: 01-Jan-2018
*  Purpose: Features of MobiquityDBAssertionQueries
*/

package framework.util.dbManagement;


import framework.entity.OperatorUser;
import framework.entity.User;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.propertiesManagement.MfsTestProperties;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


public class MobiquityDBAssertionQueries {
    public static String dbTxnStatus = null;
    private static OracleDB dbConn;

    /**
     * Initialize Constructor
     */
    public MobiquityDBAssertionQueries() {
        dbConn = new OracleDB();
    }


    /**
     * Method to verify Transaction Status in DB
     *
     * @param txnID
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static String getMobiquityTransactionDBStatus(String txnID) {
        dbTxnStatus = null;
        String query = "select transfer_status from MTX_TRANSACTION_HEADER where transfer_id='" + txnID + "' order by transfer_date";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                dbTxnStatus = result.getString("TRANSFER_STATUS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbTxnStatus;
    }

    /**
     * @return
     */

    public static void main(String[] args) {
        MfsTestProperties.getInstance();
        dbConn = new OracleDB();

        System.out.println("" + getBalanceOfIND03());
    }

    public static String getBalanceOfIND03() {
        String bal = null;
        String query = "select SUM(BALANCE) from mtx_wallet_balances where wallet_number = '101IND03'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bal = result.getString("SUM(BALANCE)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bal = "" + new BigDecimal(bal).divide(AppConfig.currencyFactor);
        return bal;
    }


    public static String getBalanceOfIND01() {
        String bal = null;
        String query = "select SUM(BALANCE) from mtx_wallet_balances where wallet_number = '101IND01'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                bal = result.getString("SUM(BALANCE)");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        bal = "" + new BigDecimal(bal).divide(AppConfig.currencyFactor);
        return bal;
    }



   /* *//**
     * Method to Get recon Balance from Items Table
     *
     * @param tid
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws MessagingException
     *//*
    public static Long getMobiquityReconItemsTable(String tid) throws SQLException, ClassNotFoundException, MessagingException, IOException {

        long difference = 0;
        long creditSum = 0;
        long debitSum = 0;

        String query1 = "select SUM(APPROVED_VALUE) from mtx_transaction_items where transfer_id='" + tid + "' and ENTRY_TYPE = 'CR' order by transfer_date";
        String query2 = "select SUM(APPROVED_VALUE) from mtx_transaction_items where transfer_id='" + tid + "' and ENTRY_TYPE = 'DR' order by transfer_date";

        try {
            //CHECKING Credit entries for transaction tid
            ResultSet resultSet = dbConn.RunQuery(query1);
            while (resultSet.next()) {
                creditSum = resultSet.getLong("SUM(APPROVED_VALUE)");
            }
            //CHECKING Debit entries for transaction tid
            resultSet = dbConn.RunQuery(query2);
            while (resultSet.next()) {
                debitSum = resultSet.getLong("SUM(APPROVED_VALUE)");
            }
            difference = creditSum - debitSum;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return difference;
    }*/

    /**
     * Method to
     *
     * @param transId
     * @param payeeMSISDN
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws MessagingException
     * @throws IOException
     *//*
    public static String getMobiquityPayeeSMSApplied(String transId, String payeeMSISDN) throws SQLException, ClassNotFoundException, MessagingException, IOException {

        String query = "select MESSAGE from sentsms where reference ='" + transId + "' and MSISDN ='" + payeeMSISDN + "'";
        String dbMessage = null;
        try {

            //PAYEE SMS VERIFICATION
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                dbMessage = resultSet.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dbMessage;
    }
*/

    /**
     * Methdo to get Payer SMS from DB
     *
     * @param payerMSISDN
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static String getLatestSMSAppliedFromSentSMS(String payerMSISDN) throws Exception {

        String query = "select MESSAGE from(select MESSAGE from sentsms where  MSISDN ='" + payerMSISDN + "' order by deliveredon desc) where rownum = '1'";
        String dbMessage = null;

        try {
            //PAYER SMS VERIFICATION
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                dbMessage = resultSet.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbMessage;
    }
    public static String getLatestSMSAppliedFromSMSDelivered(String payerMSISDN) throws Exception {

        String query = "select MESSAGE from(select MESSAGE from sms_delivered where  MSISDN ='" + payerMSISDN + "' order by deliveredon desc) where rownum = '1'";
        String dbMessage = null;

        try {
            //PAYER SMS VERIFICATION
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                dbMessage = resultSet.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dbMessage;
    }

    /**
     * Method to get Mobiquity Users DataBase Status
     *
     * @param user
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static String getUserDBStatus(Object user) {

        String categoryCode = null, msisdn = null;
        String result = null, query;

        if (user instanceof User) {
            categoryCode = ((User) user).CategoryCode;
            msisdn = ((User) user).MSISDN;
        } else if (user instanceof OperatorUser) {
            categoryCode = ((OperatorUser) user).CategoryCode;
            msisdn = ((OperatorUser) user).MSISDN;
        }

        if (!categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select status from users where msisdn='" + msisdn + "'order by modified_on desc";
        } else {
            query = "select status from mtx_party where msisdn='" + msisdn + "'order by modified_on desc";
        }

        try {
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                result = resultSet.getString("STATUS");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    public static String getUserDBStatusModify(Object user) {

        String categoryCode = null, msisdn = null;
        String result = null, query;

        if (user instanceof User) {
            categoryCode = ((User) user).CategoryCode;
            msisdn = ((User) user).MSISDN;
        } else if (user instanceof OperatorUser) {
            categoryCode = ((OperatorUser) user).CategoryCode;
            msisdn = ((OperatorUser) user).MSISDN;
        }

        if (!categoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = "select status from users_m where msisdn='" + msisdn + "'order by modified_on desc";
        } else {
            query = "select status from mtx_party_m where msisdn='" + msisdn + "'order by modified_on desc";
        }

        try {
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                result = resultSet.getString("STATUS");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }


    /**
     * Method to get Requested value from Header table
     *
     * @param TRANSFER_ID
     * @return
     */
    public static BigDecimal getRequestedAmount(String TRANSFER_ID) {
        BigDecimal res = null;
        final String query = "select REQUESTED_VALUE from MTX_TRANSACTION_HEADER  where TRANSFER_ID='" + TRANSFER_ID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("REQUESTED_VALUE");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    /**
     * Method to get post Balance
     *
     * @param msisdn
     * @param paymentId
     * @param providerId
     * @return
     */
    /*public static BigDecimal getUserBalance(String msisdn, String paymentId, String providerId) throws NullPointerException {
        BigDecimal res = null;
        paymentId = (paymentId == null) ? DataFactory.getDefaultWallet().WalletId : paymentId;
        providerId = (providerId == null) ? DataFactory.getDefaultProvider().ProviderId : providerId;

        String query = "select balance from mtx_wallet_balances where wallet_number = " +
                "(select wallet_number from mtx_wallet where msisdn = '" + msisdn + "' and payment_type_id='"
                + paymentId + "' and provider_id ='" + providerId + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }*/

    /**
     *
     * @param user
     * @param paymentId
     * @param providerId
     * @return
     * @throws NullPointerException
     */
    public static BigDecimal getUserBalance(User user, String paymentId, String providerId) throws NullPointerException {
        BigDecimal res = null;
        paymentId = (paymentId == null) ? DataFactory.getDefaultWallet().WalletId : paymentId;
        providerId = (providerId == null) ? DataFactory.getDefaultProvider().ProviderId : providerId;

        String query = "select balance from mtx_wallet_balances where wallet_number = " +
                "(select wallet_number from mtx_wallet where msisdn = '" + user.MSISDN + "' and payment_type_id='"
                + paymentId + "' and provider_id ='" + providerId + "' and STATUS='Y' " +
                " and USER_GRADE='"+user.GradeCode+"')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    public static BigDecimal getWalletBalance(String walletNumber) {
        BigDecimal res = null;
        String query = "select balance from mtx_wallet_balances where wallet_number='" + walletNumber + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     * Method to get Wallet Balance
     *
     * @param walletNumber
     * @return
     */
    public static BigDecimal getPreviousBalance(String walletNumber) {
        BigDecimal res = null;

        String query = "select previous_balance from mtx_wallet_balances where wallet_number = '" + walletNumber + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("previous_balance");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     * Method to get post balance
     *
     * @param txnID
     * @return
     */
    public static String getPostBalance(String txnID, String userType) {
        BigDecimal res = null;
        String walletNumber = getWalletNumberFromItemsTable(txnID, userType);

        String query = "select post_balance from mtx_wallet_balances where wallet_number = '" + walletNumber + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("post_balance");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res.toString();
    }


    /**
     * getWalletNumberFromItemsTable
     *
     * @param transferID           Pass the Transaction ID
     * @param userTypePayerorPayee pass user type as PAYER or PAYEE
     * @return
     */
    public static String getWalletNumberFromItemsTable(String transferID, String userTypePayerorPayee) {
        String res = null;
        String query = "select wallet_number from mtx_transaction_items where transfer_id='" + transferID + "' and USER_TYPE='" + userTypePayerorPayee + "'";

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("WALLET_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    public static BigDecimal getWalletBalanceByWalletNumber(String walletNum) {
        BigDecimal res = null;

        String query = "select balance from mtx_wallet_balances where wallet_number='" + walletNum + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
                //res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static BigDecimal getWalletBalanceByUserID(String userID, String providerId, String paymentId) {
        BigDecimal res = null;
        paymentId = (paymentId == null) ? DataFactory.getDefaultWallet().WalletId : paymentId;
        providerId = (providerId == null) ? DataFactory.getDefaultProvider().ProviderId : providerId;

        String query = "select balance from mtx_wallet_balances where wallet_number = (select wallet_number from mtx_wallet where user_id = '" + userID + "' and payment_type_id='" + providerId + "' and provider_id ='" + paymentId + "')";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
                res = res.divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }



    /**
     * @param bankName
     * @return
     */
    public static String getBankID(String bankName) {
        String res = null;
        String query = "select BANK_ID from mbk_bank_details where bank_name='" + bankName + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("BANK_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * @return
     */
    public static BigDecimal getReconBalance() {
        BigDecimal res = null;
        String query = "select sum(balance) from mtx_wallet_balances";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("SUM(BALANCE)").divide(AppConfig.currencyFactor);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     * Method to get User Expected Message.
     *
     * @param messageCode
     * @param msisdn
     * @param userCategory
     * @return
     * @throws NullPointerException
     */
    public static String getUserExpectedMessages(String messageCode, String msisdn, String userCategory) throws NullPointerException {
        String res = null, query = null;
        String query1 = "select MESSAGE from SYS_MESSAGES where MESSAGE_CODE ='" + messageCode + "' and LANGUAGE_CODE = (select PREF_LANGUAGE from USERS where MSISDN ='" + msisdn + "')";
        String query2 = "select MESSAGE from SYS_MESSAGES where MESSAGE_CODE ='" + messageCode + "' and LANGUAGE_CODE = (select PREF_LANGUAGE from MTX_PARTY where MSISDN ='" + msisdn + "')";
        if (!userCategory.equalsIgnoreCase(Constants.SUBSCRIBER)) {
            query = query1;
        } else
            query = query2;

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     * getSMSAppliedFromSentSMS
     *
     * @param transId
     * @param msisdn
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static String getSMSAppliedFromSentSMS(String transId, String msisdn) throws SQLException, ClassNotFoundException {
        String query = "select MESSAGE from SENTSMS where reference ='" + transId + "' and MSISDN ='" + msisdn + "'";
        String sentMsg = null;
        try {
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                sentMsg = resultSet.getString("MESSAGE");
            }
        } catch (NullPointerException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sentMsg;
    }


    /**
     * Method to
     *
     * @param transId
     * @param msisdn
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static String getSMSAppliedFromSMSDelivered(String transId, String msisdn) throws SQLException, ClassNotFoundException, IOException {

        String query = "select MESSAGE from sms_delivered where reference ='" + transId + "' and MSISDN ='" + msisdn + "' order by deliveredon asc";
        String dbMessage = null;
        try {

            //PAYEE SMS VERIFICATION
            ResultSet resultSet = dbConn.RunQuery(query);
            while (resultSet.next()) {
                dbMessage = resultSet.getString("MESSAGE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dbMessage;
    }

    public static Map<String, BigDecimal> getPrePostBalanceUsingTxnId(String txnId, String userType) {
        Map<String, BigDecimal> map = new HashMap<String, BigDecimal>();
        try {
            String walletNum = MobiquityDBAssertionQueries.getWalletNumberFromItemsTable(txnId, userType);

            String query = "select PREVIOUS_BALANCE,BALANCE from mtx_wallet_balances where wallet_number='" + walletNum +
                    "' and LAST_TRANSFER_ID ='" + txnId + "'";
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put("PREVIOUS_BALANCE", result.getBigDecimal("PREVIOUS_BALANCE").divide(AppConfig.currencyFactor));
                map.put("BALANCE", result.getBigDecimal("BALANCE").divide(AppConfig.currencyFactor));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * @param msisdn
     * @return
     */
    public static String getReferenceFromSentSMS(String msisdn) {
        String res = null;
        String query = "select reference from (select reference from sentsms where MSISDN ='" + msisdn + "' and reference like '%CD%' order by deliveredon desc) where rownum=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("reference");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * @param msisdn
     * @return
     */
    public static String getReferenceFromSMSDelivered(String msisdn) {
        String res = null;
        String query = "select reference from (select reference from sms_delivered where MSISDN ='" + msisdn + "' and reference like '%CD%' order by deliveredon desc) where rownum=1";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("reference");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    /**
     * @param userID
     * @param paymentTypeID
     * @return
     */
    public static String getWalletNumberByUserID(String userID, String paymentTypeID) {
        String res = null;
        String query = "select WALLET_NUMBER from mtx_wallet where user_id='" + userID + "' and PAYMENT_TYPE_ID='" + paymentTypeID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("WALLET_NUMBER");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * getWalletBalanceByWalletNumber
     * This method will fetch the User balance by Wallet number and Last Transaction ID
     *
     * @param walletNum --> Provide the wallet number
     * @param lastTxnID --> Last Transaction ID
     * @return Wallet Balance (String)
     */
    public static BigDecimal getWalletBalanceByWalletNumber(String walletNum, String lastTxnID) {
        BigDecimal res = null;

        String query = "select balance from mtx_wallet_balances where wallet_number='" + walletNum + "' and LAST_TRANSFER_ID ='" + lastTxnID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getBigDecimal("BALANCE");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


    public static Map<String, String> getAuditTrailDetailsFromMtxTxnHeader(String txnID) {
        Map<String, String> map = new HashMap<>();
        String query = "select TRANSFER_STATUS,SERVICE_TYPE,TRANSFER_SUBTYPE,PAYER_USER_ID,PAYEE_USER_ID,PAYER_PROVIDER_ID," +
                "PAYER_PAYMENT_METHOD_TYPE,PAYER_PAYMENT_TYPE_ID,PAYEE_PROVIDER_ID,PAYEE_PAYMENT_METHOD_TYPE,PAYEE_PAYMENT_TYPE_ID" +
                " from mtx_transaction_header where TRANSFER_ID = '" + txnID + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put("TRANSFER_STATUS", result.getString("TRANSFER_STATUS"));
                map.put("SERVICE_TYPE", result.getString("SERVICE_TYPE"));
                map.put("TRANSFER_SUBTYPE", result.getString("TRANSFER_SUBTYPE"));
                map.put("PAYER_USER_ID", result.getString("PAYER_USER_ID"));
                map.put("PAYEE_USER_ID", result.getString("PAYEE_USER_ID"));

                map.put("PAYER_PROVIDER_ID", result.getString("PAYER_PROVIDER_ID"));
                map.put("PAYER_PAYMENT_METHOD_TYPE", result.getString("PAYER_PAYMENT_METHOD_TYPE"));
                map.put("PAYER_PAYMENT_TYPE_ID", result.getString("PAYER_PAYMENT_TYPE_ID"));

                map.put("PAYEE_PROVIDER_ID", result.getString("PAYEE_PROVIDER_ID"));
                map.put("PAYEE_PAYMENT_METHOD_TYPE", result.getString("PAYEE_PAYMENT_METHOD_TYPE"));
                map.put("PAYEE_PAYMENT_TYPE_ID", result.getString("PAYEE_PAYMENT_TYPE_ID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }


    /**
     * @param actionType
     * @param msisdn
     * @return
     */
    public static Map<String, String> getAdminTrailDetails(String actionType, String msisdn) {
        Map<String, String> map = new HashMap<>();
        String query = "select PARTY_ID,PARTY_ACCESS_ID,CATEGORY_CODE,ACTION_TYPE,CREATED_BY from mtx_admin_audit_trail where ACTION_TYPE = " + actionType + " and MSISDN = " + msisdn + "'";
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                map.put("PARTY_ID", result.getString("PARTY_ID"));
                map.put("PARTY_ACCESS_ID", result.getString("PARTY_ACCESS_ID"));
                map.put("CATEGORY_CODE", result.getString("CATEGORY_CODE"));
                map.put("ACTION_TYPE", result.getString("ACTION_TYPE"));
                map.put("CREATED_BY", result.getString("CREATED_BY"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static String getTotalNoSubsForKPI(boolean prevDate) {
        String query, res = null;
        if (prevDate) {
            query = "select count(*) as TOTAL from mtx_party where created_on <= sysdate-1";
        } else {
            query = "select count(*) as TOTAL from mtx_party";
        }

        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("TOTAL");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }


}
