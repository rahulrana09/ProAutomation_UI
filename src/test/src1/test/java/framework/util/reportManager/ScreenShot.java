package framework.util.reportManager;

import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
/*
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
*/

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rahul.rana on 6/7/2017.
 */
public class ScreenShot {

    private static WebDriver driver;

    public static String TakeScreenshot(WebDriver... newDriver) {

        if (newDriver.length > 0) {
            driver = newDriver[0]; // its quite possible that screen shot of other driver instance is required
        } else {
            driver = DriverFactory.getDriver();
        }

        String fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
        try {
            File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(src, new File(FilePath.dirSnapShot + fileName));
        } catch (Exception ex) {
            System.err.println(ex);
        }

        return "./capture/" + fileName;
    }


    public static String TakeScreenshotWithoutDriver() {

        Utils.putThreadSleep(Constants.TWO_SECONDS);
        String fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".jpg";
        try {


            Robot r = new Robot();

            // It saves screenshot to desired path
            String path = FilePath.dirSnapShot + fileName;

            // Used to get ScreenSize and capture image
            Rectangle capture = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage Image = r.createScreenCapture(capture);
            ImageIO.write(Image, "jpg", new File(path));
            System.out.println("Screenshot saved");

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return "./capture/" + fileName;
    }


    public static String takeFullPageScreenShot() {
        String fileName = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ".png";
        try {
            driver = DriverFactory.getDriver();

/*
            Screenshot screenshot = new AShot().
                    shootingStrategy(ShootingStrategies.viewportPasting(1000)).
                    takeScreenshot(driver);

            ImageIO.write(screenshot.getImage(), "PNG", new File(FilePath.dirSnapShot + fileName));

            */
        } catch (Exception e) {
            System.err.println("Error Occured while taking full Page Screenshot");
        }

        return "./capture/" + fileName;
    }


}
