package framework.util.reportManager;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import framework.util.globalConstant.FilePath;

/**
 * Created by rahul.rana on 6/26/2017.
 */
public class ExtentManager {

    private static ExtentReports extent;
    private static String fileName;
    private static String filePath;

    public static ExtentReports getInstance() {
        if (fileName == null) {
            return null;
        }

        if (extent == null) {
            filePath = FilePath.dirReports + fileName;
            extent = createInstance();
        }

        return extent;
    }

    public static void extentFlush() {
        extent.flush();
    }

    public static String getFileName() {
        return fileName;
    }

    public static void setFileName(String fName) {
        fileName = fName;
    }

    public static ExtentReports createInstance() {
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(filePath);
        htmlReporter.config().setTestViewChartLocation(ChartLocation.BOTTOM);
        htmlReporter.config().setChartVisibilityOnOpen(false);
        htmlReporter.config().setTheme(Theme.STANDARD);
        htmlReporter.config().setDocumentTitle(fileName);
        htmlReporter.config().setEncoding("utf-8");
        htmlReporter.config().setReportName(fileName);

        extent = new ExtentReports();
        extent.setAnalysisStrategy(AnalysisStrategy.SUITE);
        extent.attachReporter(htmlReporter);
        extent.flush();

        return extent;
    }

}
