package framework.util;

import framework.util.excelManagement.ExcelUtil;
import org.apache.commons.collections.map.HashedMap;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TestMatch {
    private String id1, id2, sentence1, sentence2, s2Status, perMatch, developer, function, function2, perMatchFunction;
    private int lineNum;
    private double factor = 0.60; // factor below .80 yield more error result

    public TestMatch() {
    }

    public static double matchStrings(String firstString, String SecondString) {

        double matchingCount = 0;

        //Getting the whole set of words in to array.
        String[] allWords = firstString.split("\\s");
        Set<String> firstInputset = new HashSet<String>();

        //getting unique words in to set
        for (String string : allWords) {
            firstInputset.add(string);
        }

        //Loop through the set and check whether number of words occurrence in second String
        for (String string : firstInputset) {
            if (SecondString.toLowerCase().contains(string.toLowerCase())) {
                matchingCount++;
            }
        }
        return matchingCount;
    }

    public static void writeDataToExcel(String filename, Map<String, TestMatch> dataMap) throws IOException {
        FileOutputStream fileOut = null;
        try {

            FileInputStream inputStream = new FileInputStream(filename);
            XSSFWorkbook wbx = new XSSFWorkbook(inputStream);
            XSSFSheet foundSheet = wbx.getSheetAt(1);
            int foundCounter = 1;
            for (TestMatch data : dataMap.values()) {
                XSSFRow row = foundSheet.createRow(foundCounter);
                row.createCell(0).setCellValue(data.id1);
                row.createCell(1).setCellValue(data.sentence1);
                row.createCell(2).setCellValue(data.id2);
                row.createCell(3).setCellValue(data.sentence2);
                row.createCell(4).setCellValue(data.perMatch);
                foundCounter++;
            }
            fileOut = new FileOutputStream(filename);
            wbx.write(fileOut);

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (fileOut != null)
                fileOut.close();
        }
    }

    @Test
    public void run() throws Exception {
        String basePath = new File("").getAbsolutePath();

        Map<String, TestMatch> map = new HashedMap();

        String dataFile = basePath + "/src/test/resources/TempData/TCMapper.xlsx";
        Object[][] test = ExcelUtil.getTableArray(dataFile, 0);

        for (int i = 0; i < test.length; i++) {
            boolean isFound = false;
            String sent1 = test[i][1].toString().trim(); // description from main sheet

            if (sent1 != "") {
                double length = sent1.split("\\s").length;
                double max = 0;
                int rowCounter = 0;
                for (int j = 0; j < test.length; j++) {
                    String sent2 = test[j][3].toString().trim(); //description for match
                    double match = matchStrings(sent1, sent2);
                    double perMatch = match / length;
                    if (perMatch > factor && perMatch > max) {
                        isFound = true;
                        max = perMatch;
                        rowCounter = j;
                        if (max > 0.95)
                            break;
                    }
                }


                TestMatch testData = new TestMatch();
                testData.id1 = test[i][0].toString().trim();
                testData.sentence1 = test[i][1].toString().trim();


                if (isFound) {
                    testData.id2 = test[rowCounter][2].toString().trim();
                    testData.sentence2 = test[rowCounter][3].toString().trim();
                    testData.perMatch = "" + max * 100;
                } else {
                    testData.id2 = "Not Found";
                    testData.sentence2 = "Not Found";
                    testData.perMatch = "0";
                }
                map.put(testData.id1, testData);
            }
        }

        writeDataToExcel(dataFile, map);
    }

    private String getFunctionMatchPercentage(String one, String two) {
        double length = one.split("\\s").length;
        double match = matchStrings(one, two);
        return "" + (match / length);
    }

}

