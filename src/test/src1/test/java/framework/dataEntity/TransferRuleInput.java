package framework.dataEntity;

/**
 * Created by rahul.rana on 5/25/2017.
 */
public class TransferRuleInput {
    public String StatusId, TransferType, ControlledTxLevel, FixedTxLevel, ModifyStatusID, ModifyTxType, ModifyControlTxLevel,
            ModifyFixedTxLevel, GeoDomainCode;

    public TransferRuleInput(String statusId, String transferType, String controlledTxLevel, String fixedTxLevel,
                             String modifyStatusID, String modifyTxType, String modifyControlTxLevel, String modifyFixedTxLevel, String geoDomainCode) {

        this.StatusId = statusId;
        this.TransferType = transferType;
        this.ControlledTxLevel = controlledTxLevel;
        this.FixedTxLevel = fixedTxLevel;
        this.ModifyStatusID = modifyStatusID;
        this.ModifyTxType = modifyTxType;
        this.ModifyControlTxLevel = modifyControlTxLevel;
        this.ModifyFixedTxLevel = modifyFixedTxLevel;
        this.GeoDomainCode = geoDomainCode;
    }
}
