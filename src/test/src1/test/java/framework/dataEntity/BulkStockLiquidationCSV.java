package framework.dataEntity;

public class BulkStockLiquidationCSV {
    public String serialNum, liquidationTxnId, batchId, transactionDate, transactionTime, usrCategoryCode, usrName, usrMsisdn, usrCode, bankAccountNum, bankBranchName, providerId, liquidationAmount, productId, isApproved, remark;

    public BulkStockLiquidationCSV(String serialNum, String liquidationTxnId, String batchId, String transactionDate,
                                   String transactionTime, String usrCategoryCode, String usrName,
                                   String usrMsisdn, String usrCode, String bankAccountNum, String bankBranchName,
                                   String providerId, String liquidationAmount, String productId,
                                   String isApproved, String remark) {
        this.serialNum = serialNum;
        this.liquidationTxnId = liquidationTxnId;
        this.batchId = batchId;
        this.transactionDate = transactionDate;
        this.transactionTime = transactionTime;
        this.usrCategoryCode = usrCategoryCode;
        this.usrName = usrName;
        this.usrMsisdn = usrMsisdn;
        this.usrCode = usrCode;
        this.bankAccountNum = bankAccountNum;
        this.bankBranchName = bankBranchName;
        this.providerId = providerId;
        this.liquidationAmount = liquidationAmount;
        this.productId = productId;
        this.isApproved = isApproved;
        this.remark = remark;
    }

}
