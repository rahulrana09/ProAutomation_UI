package framework.dataEntity;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.jayway.restassured.response.ValidatableResponse;
import framework.util.common.Assertion;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;

import java.io.IOException;
import java.text.MessageFormat;

/**
 * Created by rahul.rana on 9/21/2017.
 */
public class TxnResponse {

    public String Message, Type, TxnId = null, TxnStatus, TrId, OTP = null, SVCBalance, AdminWalletNum, currency, BALANCE, BENMSISDN, DBTAMT;
    private ExtentTest pNode;
    public ValidatableResponse response;
    public boolean isSuccess = false;

    public TxnResponse(ValidatableResponse response, ExtentTest pNode) {
        this.pNode = pNode;
        this.response = response;

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(response.extract().body().asString());
        String prettyJsonString = gson.toJson(je);

        pNode.info("RESPONSE :\t" + prettyJsonString);
        try {
            this.Type = response.extract().jsonPath().getString("COMMAND.TYPE").toString();
            this.TxnStatus = response.extract().jsonPath().getString("COMMAND.TXNSTATUS").toString();

            if (response.extract().jsonPath().getString("COMMAND.TRID") != null)
                this.TrId = response.extract().jsonPath().getString("COMMAND.TRID").toString();

            if (response.extract().jsonPath().getString("COMMAND.MESSAGE") != null)
                this.Message = Utils.removeExtraSpace(response.extract().jsonPath().getString("COMMAND.MESSAGE").toString());

            if (response.extract().jsonPath().getString("COMMAND.MSISDN_SVCBALANCE") != null)
                this.SVCBalance = response.extract().jsonPath().getString("COMMAND.MSISDN_SVCBALANCE").toString();

            if (response.extract().jsonPath().getString("COMMAND.ADMIN_WALLETNO") != null)
                this.AdminWalletNum = response.extract().jsonPath().getString("COMMAND.ADMIN_WALLETNO").toString();

            if (response.extract().jsonPath().getString("COMMAND.TXNID") != null)
                this.TxnId = response.extract().jsonPath().getString("COMMAND.TXNID").toString();

            if (response.extract().jsonPath().getString("COMMAND.OTP") != null)
                this.OTP = response.extract().jsonPath().getString("COMMAND.OTP").toString();

            if (response.extract().jsonPath().getString("COMMAND.BALANCE") != null)
                this.BALANCE = response.extract().jsonPath().getString("COMMAND.BALANCE").toString();

            if (response.extract().jsonPath().getString("COMMAND.DATA") != null) {
                if (response.extract().jsonPath().getString("COMMAND.DATA.BENMSISDN") != null)
                    this.BENMSISDN = response.extract().jsonPath().getString("COMMAND.DATA.BENMSISDN").toString();

                if (response.extract().jsonPath().getString("COMMAND.DATA.DBTAMT") != null)
                    this.DBTAMT = response.extract().jsonPath().getString("COMMAND.DATA.DBTAMT").toString();
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDataFromResponseBody(){
        if (response.extract().jsonPath().getString("COMMAND.DATA") != null)
            return response.extract().jsonPath().getString("COMMAND.DATA").toString();

        return null;
    }


    public TxnResponse assertMessage(String messageCode, String... params) throws IOException {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.assertEqual(this.Message, message, "Assert Message", pNode, false);
        return this;
    }

    public TxnResponse verifyMessage(String messageCode, String... params) throws IOException {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.verifyContains(this.Message, message, "Verify Message", pNode, false);
        return this;
    }

    public boolean isMessageContains(String messageCode, String... params) {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        if(this.Message.contains(message))
            return true;

        return false;
    }

    public TxnResponse verifyMessageWithInfo(String messageCode, String info, String... params) throws IOException {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.verifyContains(this.Message, message, info, pNode, false);
        return this;
    }

    public boolean getMessageVerificationStatus(String messageCode, String... params) throws IOException {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MessageReader.getMessage(messageCode, null);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        return Assertion.verifyContains(this.Message, message, "Verify Message", pNode, false);
    }

    public TxnResponse assertStatus(String statusCode) throws IOException {
        pNode.info("Message: " + this.Message);
        Assertion.assertEqual(this.TxnStatus, statusCode, "Assert Status code", pNode, false);
        return this;
    }

    public TxnResponse verifyStatus(String statusCode) throws IOException {
        pNode.info("Message: " + this.Message);
        Assertion.verifyEqual(this.TxnStatus, statusCode, "Verify Status code", pNode, false);
        return this;
    }

    public Boolean isTxnSuccessful(String status) throws IOException {
        pNode.info("Message: " + this.Message);
        if(Assertion.verifyEqual(this.TxnStatus, status, "Verify Transaction is successful", pNode, false))
            return true;

        return false;
    }

    public TxnResponse verifyBeneficiaryMsisdn(String msisdn) throws IOException {
        pNode.info("Message: " + this.Message);
        Assertion.verifyContains(this.BENMSISDN, msisdn, "Verify Beneficiary MSISDN", pNode, false);
        return this;
    }

    public TxnResponse verifyDebitAmount(String msisdn) throws IOException {
        pNode.info("Message: " + this.Message);
        Assertion.verifyContains(this.DBTAMT, msisdn, "Verify Debit amount", pNode, false);
        return this;
    }

    public boolean verifyStatus1(String statusCode, ExtentTest t1) throws IOException {
        t1.info("Message: " + this.Message);
        return Assertion.verifyEqual(this.TxnStatus, statusCode, "Verify Status code", t1, false);
    }

    public TxnResponse verifyMessageFromDB(String messageCode, String langCode, String... params) throws IOException {
        pNode.info("Transaction Status:" + this.TxnStatus);
        String message = MobiquityGUIQueries.getMessageFromSysMessages(messageCode, langCode);
        if (params != null && params.length != 0) {
            message = MessageFormat.format(message, params); // dynamic Message
        }

        Assertion.verifyContains(this.Message, message, "Verify Message", pNode, false);
        return this;
    }

    public String getMessage() {
        return this.Message;
    }

    public ValidatableResponse getResponse() {
        return this.response;
    }

    public void setSuccess(){
        this.isSuccess = true;
    }

    public boolean isSuccess(){
        return this.isSuccess;
    }

}
