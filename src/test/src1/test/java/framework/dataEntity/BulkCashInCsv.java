package framework.dataEntity;

public class BulkCashInCsv {
    public String serialNum, providerId, senderSvaTypeId, senderMsisdn, receiverSvaTypeId, receiverMsisdn, amount, remark;

    public BulkCashInCsv(String serialNum,
                         String providerId,
                         String senderSvaTypeId,
                         String senderMsisdn,
                         String receiverSvaTypeId,
                         String receiverMsisdn,
                         String amount,
                         String remark) {

        this.serialNum = serialNum;
        this.providerId = providerId;
        this.senderSvaTypeId = senderSvaTypeId;
        this.senderMsisdn = senderMsisdn;
        this.receiverSvaTypeId = receiverSvaTypeId;
        this.receiverMsisdn = receiverMsisdn;
        this.amount = amount;
        this.remark = remark;
    }

}
