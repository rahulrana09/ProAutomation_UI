package framework.dataEntity;

import framework.util.common.DataFactory;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;

/**
 * Created by rahul.rana on 5/16/2017.
 */
public class Wallet {
    public String WalletId, WalletName, AutWalletCode;
    public boolean IsDefault, ForChannelUser, ForSubscriber, IsSpecialWallet, IsActive;

    public Wallet(String autWalletCode, String walletId, String walletName, String isDefault,
                  String forChannelUser, String forSubscriber, String isSpecialWallet, String isActive) {
        this.AutWalletCode = autWalletCode;
        this.WalletId = walletId;
        this.WalletName = walletName;
        this.IsDefault = (isDefault.toLowerCase().equals("true")) ? true : false;
        this.ForChannelUser = (forChannelUser.toLowerCase().equals("true")) ? true : false;
        this.ForSubscriber = (forSubscriber.toLowerCase().equals("true")) ? true : false;
        this.IsSpecialWallet = (isSpecialWallet.toLowerCase().equals("true")) ? true : false;
        this.IsActive = (isActive.equalsIgnoreCase("y")) ? true : false;

        if (!AppConfig.isRemittanceRequired &&
                AutWalletCode.equals(Wallets.REMMITANCE))
            this.ForSubscriber = false;

        if (!AppConfig.isCommissionWalletRequired &&
                AutWalletCode.equals(Wallets.COMMISSION))
            this.ForChannelUser = false;
    }
}
