package framework.dataEntity;

import framework.util.globalVars.AppConfig;

import java.math.BigDecimal;

/**
 * Created by rahul.rana on 9/13/2017.
 */
public class UsrBalance {

    public BigDecimal Balance, FIC, frozenBalance;

    public UsrBalance(BigDecimal balance, BigDecimal ficBalance) {
        this.Balance = balance.divide(AppConfig.currencyFactor);
        this.FIC = ficBalance.divide(AppConfig.currencyFactor);
    }

    public UsrBalance(BigDecimal balance, BigDecimal ficBalance, BigDecimal frozenBalance) {
        this.Balance = balance.divide(AppConfig.currencyFactor);
        this.FIC = ficBalance.divide(AppConfig.currencyFactor);
        this.frozenBalance = frozenBalance.divide(AppConfig.currencyFactor);
    }

    public void setBalance(BigDecimal amount) {
        this.Balance = amount;
    }

    public void setFIC(BigDecimal fic) {
        this.FIC = fic;
    }
}
