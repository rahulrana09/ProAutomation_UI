package framework.dataEntity;

public class BulkMerchantPaymentCSV {
    public String serialNum,
            provider,
            senderPayId,
            senderMsisdn,
            receiverPayId,
            receiverMsisdn,
            amount,
            remark;

    public BulkMerchantPaymentCSV(String serialNum,
                                  String providerId,
                                  String senderPayId,
                                  String senderMsisdn,
                                  String receiverPayId,
                                  String receiverMsisdn,
                                  String amount,
                                  String remark)
    {
        this.serialNum = serialNum;
        this.provider = providerId;
        this.senderPayId = senderPayId;
        this.senderMsisdn = senderMsisdn;
        this.receiverPayId = receiverPayId;
        this.receiverMsisdn = receiverMsisdn;
        this.amount = amount;
        this.remark = remark;
    }

}
