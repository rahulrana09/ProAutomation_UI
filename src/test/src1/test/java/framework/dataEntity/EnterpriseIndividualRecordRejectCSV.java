package framework.dataEntity;

public class EnterpriseIndividualRecordRejectCSV {
    public String batchID,
            recordNum;

    public EnterpriseIndividualRecordRejectCSV(String batchID,
                                    String recordNum) {
        this.batchID = batchID;
        this.recordNum = recordNum;
    }
}
