package framework.components.shulka;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ValidatableResponse;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.JsonPathOperation;
import framework.util.propertiesManagement.MfsTestProperties;

import java.util.*;
import java.util.concurrent.Future;

import static framework.util.JsonPathOperation.set;
import static framework.util.common.ProcessingHelper.fireCallable;
import static framework.util.common.ProcessingHelper.fireRunnable;
import static framework.util.common.Utils.json;
import static framework.util.propertiesManagement.MfsTestUtils.getTenantId;
import static framework.util.propertiesManagement.MfsTestUtils.moneyUri;
import static framework.util.propertiesManagement.MfsTestUtils.moneyUriInternalApi;


public class PricingPolicyOperations {
    public static final String SHULKA_AUTH_TOKEN = "shulka.auth.token";
    public static final String MONEY_CURRENCY_CODE_PROPERTY = "mfs1.currency.code";
    private static final MfsTestProperties mfsTestProperties = MfsTestProperties.getInstance();


    public static Future<ValidatableResponse> saveServiceChargePolicyAsync(JsonPathOperation... operations) {
        return fireCallable(() -> saveServiceChargePolicy(operations));
    }

    public static Future<ValidatableResponse> saveServiceChargePolicyWithDiscountAsync(JsonPathOperation... operations) {
        return fireCallable(() -> saveServiceChargeWithDiscountPolicy(operations));
    }

    public static ValidatableResponse saveServiceChargePolicy(String jsonRequestBody) {
        String currencyCode = mfsTestProperties.getProperty(MONEY_CURRENCY_CODE_PROPERTY);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(json(jsonRequestBody, set("$.currency", currencyCode)))
                .post("/shulka/serviceChargePolicy")
                .then().log().all().statusCode(200);
    }

    public static ValidatableResponse saveServiceChargePolicy(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(PricingPolicyContracts.serviceChargePolicyJson(operationsSpecificToCurrency))
                .post("/shulka/serviceChargePolicy")
                .then().log().all().statusCode(200);
    }

    public static ValidatableResponse saveServiceChargePolicyAndFailWhenChargePayerIsInvalid(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(PricingPolicyContracts.serviceChargePolicyJson(operationsSpecificToCurrency))
                .post("/shulka/serviceChargePolicy")
                .then().log().all().statusCode(422);
    }

    public static ValidatableResponse saveServiceChargeWithDiscountPolicy(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(PricingPolicyContracts.serviceChargePolicyWithDiscountJson(operationsSpecificToCurrency))
                .post("/shulka/serviceChargePolicy")
                .then().log().all().statusCode(200);
    }

    private static JsonPathOperation[] addConfiguredCurrency(JsonPathOperation[] operations) {
        String currencyCode = mfsTestProperties.getProperty(MONEY_CURRENCY_CODE_PROPERTY);
        List<JsonPathOperation> operationList = new ArrayList<JsonPathOperation>(Arrays.asList(operations));
        TestJsonContext context = new TestJsonContext();
        context.evaluate(operationList);
        if (context.getFields().keySet().contains("$.currency")== false)
        {
            operationList.add(set("$.currency", currencyCode));
            JsonPathOperation[] jsonPathOperations = new JsonPathOperation[operationList.size()];
            jsonPathOperations = operationList.toArray(jsonPathOperations);
            return jsonPathOperations;
        }
        return operations;
    }

    public static Future<ValidatableResponse> saveCommissionPolicyAsync(JsonPathOperation... operations) {
        return fireCallable(() -> saveCommissionPolicy(operations));
    }

    public static Future<ValidatableResponse> saveCommissionPolicyAsync(String jsonRequestBody) {
        return fireCallable(() -> saveCommissionPolicy(jsonRequestBody));
    }

    public static ValidatableResponse saveCommissionPolicy(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        ValidatableResponse response = moneyUri().given().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(PricingPolicyContracts.commissionPolicyJson(operationsSpecificToCurrency)).post("/shulka/commissionPolicy").then().log().all();
        return response.statusCode(200);
    }


    public static ValidatableResponse saveCommissionPolicy(String jsonRequestBody) {
        String currencyCode = mfsTestProperties.getProperty(MONEY_CURRENCY_CODE_PROPERTY);
        ValidatableResponse response = moneyUri().given().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(json(jsonRequestBody, set("$.currency", currencyCode))).post("/shulka/commissionPolicy").then().log().all();
        return response.statusCode(200);
    }

    public static void deleteAllPricingPolicies() {
        moneyUriInternalApi()
                .header("mfsTenantId", getTenantId())
                .delete("/shulka/internal/pricingPolicies").then().statusCode(200);
    }

    public static Future<?> deleteAllPricingPoliciesAsync() {
        return fireRunnable(() -> deleteAllPricingPolicies());
    }

    public static ValidatableResponse approval(String changeProposalId, Map config) {
        return moneyUri().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body((config))
                .post("/shulka/pricingPolicyChangeProposal/" + changeProposalId + "/approval")
                .then().statusCode(200);
    }

    public static ValidatableResponse setShulkaConfig(Map config) {
        Map map = new HashMap(config);
        map.put("mfsTenantId", getTenantId());
        return moneyUriInternalApi().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(map)
                .post("/shulka/internal/configuration")
                .then().statusCode(200);
    }

    public static Future<ValidatableResponse> saveTaxOnTransactionAmountPolicyAsync(JsonPathOperation... operations) {
        return fireCallable(() -> saveTaxOnTransactionAmountPolicy(operations));
    }

    public static ValidatableResponse saveTaxOnTransactionAmountPolicy(JsonPathOperation... operations) {
        JsonPathOperation[] operationsSpecificToCurrency = addConfiguredCurrency(operations);
        return moneyUri().given().log().all().contentType(ContentType.JSON).accept(ContentType.JSON)
                .queryParam("token", MfsTestProperties.getInstance().getProperty(SHULKA_AUTH_TOKEN))
                .body(PricingPolicyContracts.taxOnTransactionAmountJson(operationsSpecificToCurrency))
                .post("/shulka/taxPolicy")
                .then().log().all().statusCode(200);
    }
}
