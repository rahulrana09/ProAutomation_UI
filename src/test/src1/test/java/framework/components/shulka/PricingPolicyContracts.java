package framework.components.shulka;


import framework.util.JsonPathOperation;

import java.util.Map;

import static framework.util.MultiLineString.multiLineString;
import static framework.util.common.Utils.json;
import static framework.util.common.Utils.jsonToMap;
import static framework.util.jigsaw.ServiceRequestContracts.getJsonWithTenantId;

public class PricingPolicyContracts {

    public static String serviceChargePolicyJson(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                    {
                      "serviceCode": "CASHIN",
                      "currency": "RUB",
                      "isFinancial": true,
                      "isInclusive": false,
                      "isTaxInclusive": false,
                      "chargePricingRange": {
                        "minimumAmount": null,
                        "maximumAmount": null
                      },
                      "taxPricingRange": {
                        "minimumAmount": 0,
                        "maximumAmount": 9999999
                      },
                      "chargeRules": [
                        {
                          "name": "Default Charge",
                          "startDate": "2015-10-01",
                          "endDate": null,
                          "condition": {
                            "lhsExpression": "bearerCode",
                            "operator": "equals",
                            "rhsValue": "USSD"
                           },
                          "status": "ACTIVE",
                          "chargePricingRange": {
                            "minimumAmount": null,
                            "maximumAmount": null
                          },
                          "chargeStatements": [
                            {
                              "chargePayer": "sender",
                              "chargePayerProductId": "12",
                              "chargeReceiver": "serviceProvider",
                              "pricingFactor": "both",
                              "chargePayerParty":null,
                              "pricingMethod": {
                                "type": "flatPricing",
                                "percentage": "1",
                                "fixedAmount": "10"
                              }
                            }
                          ]
                        }
                      ],
                      "taxRules": [
                        {
                          "name": "Default Tax",
                          "startDate": "2015-02-12",
                          "endDate": "2019-06-29",
                          "condition":null,
                          "taxStatements": [
                            {
                              "pricingMethod": {
                                "percentage": 10,
                                "fixedAmount": 10,
                                "type": "flatPricing"
                              },
                              "pricingFactor": "both"
                            }
                          ]
                        }
                      ]
                    }
                    */), operations);
        return getJsonWithTenantId(json);
    }

    public static String commissionPolicyJson(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                  {
                            "serviceCode": "CASHIN",
                            "currency": "RUB",
                            "isFinancial": true,
                            "version": 1,
                            "isInclusive": false,
                            "isTaxInclusive": false,
                            "chargePricingRange": {
                                "minimumAmount": null,
                                "maximumAmount": null
                            },
                            "taxPricingRange": {
                                "minimumAmount": 0,
                                "maximumAmount": 9999999
                            },
                            "chargeRules": [{
                                "name": "Default Charge",
                                "startDate": "2015-10-01",
                                "endDate": null,
                                "condition": null,
                                "status": "ACTIVE",
                                "chargePricingRange": {
                                    "minimumAmount": null,
                                    "maximumAmount": null
                                },
                                "chargeStatements": [{
                                    "chargePayerProductId": "primary",
                                    "chargeReceiverPartyId": "",
                                    "chargeReceiver": "receiver",
                                    "chargeReceiverProductId": "primary",
                                    "chargeReceiverExpressionArgs": null,
                                    "pricingMethod": {
                                        "type": "flatPricing",
                                        "percentage": 0,
                                        "fixedAmount": 1
                                    },
                                    "pricingFactor": "both",
                                    "chargePayer": "sender"
                                }]
                            }],
                            "waiveRules": [],
                            "taxRules": [{
                                "name": "Default Tax",
                                "startDate": "2015-02-12",
                                "taxStatements": [{
                                    "pricingMethod": {
                                        "percentage": 0,
                                        "fixedAmount": null,
                                        "type": "flatPricing"
                                    },
                                    "pricingFactor": "both"
                                }]
                            }]
                        }
                    */), operations);
        return getJsonWithTenantId(json);
    }

    public static Map chargeStatement(JsonPathOperation... operations) {
        Map map = jsonToMap(json(multiLineString(/*
                        {
                          "chargePayer": "sender",
                          "chargePayerProductId": "12",
                          "chargeReceiverProductId": "primary",
                          "chargeReceiverPartyId": "",
                          "chargeReceiver": "",
                          "chargeReceiverExpressionArgs": {},
                          "pricingFactor": "both",
                          "pricingMethod": {
                            "type": "flatPricing",
                            "percentage": "1",
                            "fixedAmount": "10"
                          }
                        }
                    */), operations));
        return map;
    }

    public static Map taxStatement(JsonPathOperation... operations) {
        Map map = jsonToMap(json(multiLineString(/*
                  {
                    "pricingFactor": "both",
                    "pricingMethod": {
                        "percentage": 10,
                        "fixedAmount": 0.5,
                        "type": "flatPricing"
                        }
                    }
                    */), operations));
        return map;
    }

    public static Map chargeRuleForCommission(JsonPathOperation... operations) {
        Map map = jsonToMap(json(multiLineString(/*
                        {
                                "name": "Default Charge",
                                "startDate": "2015-10-01",
                                "endDate": null,
                                "condition": null,
                                "chargePricingRange": {
                                    "minimumAmount": null,
                                    "maximumAmount": null
                                },
                                "chargeStatements": [{
                                    "chargePayerProductId": "primary",
                                    "chargeReceiverPartyId": "",
                                    "chargeReceiver": "serviceProvider",
                                    "chargeReceiverProductId": "primary",
                                    "chargeReceiverExpressionArgs": null,
                                    "pricingMethod": {
                                        "type": "flatPricing",
                                        "percentage": 0,
                                        "fixedAmount": 1
                                    },
                                    "pricingFactor": "both",
                                    "chargePayer": "sender"
                                }]
                        }
                    */), operations));
        return map;
    }

    public static Map chargeRuleForServiceCharge(JsonPathOperation... operations) {
        Map map = jsonToMap(json(multiLineString(/*
                        {
                                "name": "Default Charge",
                                "startDate": "2015-10-01",
                                "endDate": null,
                                "condition": null,
                                "chargePricingRange": {
                                    "minimumAmount": null,
                                    "maximumAmount": null
                                },
                                "chargeStatements": [{
                                    "chargePayerProductId": "12",
                                    "chargeReceiverPartyId": "",
                                    "chargeReceiver": "serviceProvider",
                                    "chargeReceiverExpressionArgs": null,
                                    "pricingMethod": {
                                        "type": "flatPricing",
                                        "percentage": 0,
                                        "fixedAmount": 1
                                    },
                                    "pricingFactor": "both",
                                    "chargePayer": "sender"
                                }]
                        }
                    */), operations));
        return map;
    }
    public static Map taxRule(JsonPathOperation... operations) {
        Map map = jsonToMap(json(multiLineString(/*
                        {
                                "name": "Default Tax",
                                "startDate": "2015-10-01",
                                "endDate": null,
                                "condition": null,
                                "taxPricingRange": {
                                    "minimumAmount": null,
                                    "maximumAmount": null
                                },
                                "taxStatements": [{
                                    "pricingMethod": {
                                        "type": "flatPricing",
                                        "percentage": 0,
                                        "fixedAmount": 1
                                    },
                                    "pricingFactor": "both",
                                }]
                        }
                    */), operations));
        return map;
    }

    public static String serviceChargePolicyWithDiscountJson(JsonPathOperation... operations) {
        final String json = json(multiLineString(/*
                    {
                      "serviceCode": "CASHIN",
                      "currency": "RUB",
                      "isFinancial": true,
                      "isInclusive": false,
                      "isTaxInclusive": false,
                      "chargePricingRange": {
                        "minimumAmount": null,
                        "maximumAmount": null
                      },
                      "taxPricingRange": {
                        "minimumAmount": 0,
                        "maximumAmount": 9999999
                      },
                      "chargeRules": [
                        {
                          "name": "Default Charge",
                          "startDate": "2015-10-01",
                          "endDate": null,
                          "condition": {
                            "lhsExpression": "bearerCode",
                            "operator": "equals",
                            "rhsValue": "USSD"
                           },
                          "status": "ACTIVE",
                          "chargePricingRange": {
                            "minimumAmount": null,
                            "maximumAmount": null
                          },
                          "chargeStatements": [
                            {
                              "chargePayer": "sender",
                              "chargePayerProductId": "12",
                              "chargeReceiver": "serviceProvider",
                              "pricingFactor": "both",
                              "chargePayerParty":null,
                              "pricingMethod": {
                                "type": "flatPricing",
                                "percentage": "1",
                                "fixedAmount": "10"
                              }
                            }
                          ]
                        }
                      ],
                      "taxRules": [
                        {
                          "name": "Default Tax",
                          "startDate": "2015-02-12",
                          "endDate": "2019-06-29",
                          "condition":null,
                          "taxStatements": [
                            {
                              "pricingMethod": {
                                "percentage": 10,
                                "fixedAmount": 10,
                                "type": "flatPricing"
                              },
                              "pricingFactor": "both"
                            }
                          ]
                        }
                      ],
                      "discountRules": [
                          {
                              "name": "Default Tax",
                              "startDate": "2015-02-12",
                              "endDate": "2019-06-29",
                              "condition":null,
                              "discountStatements": [
                                {
                                    "discountType": "discount",
                                    "partyType": "sender",
                                    "definedInPercentage": true,
                                    "fixedAmount": "0",
                                    "percentageAmount": 0
                                }
                              ]
                            }
                      ]
                    }
                    */), operations);
        return getJsonWithTenantId(json);
    }

    public static Map slabbedPricing(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                       {
                       "type": "slabbedPricing",
                       "slabBasis": "transactionAmount",
                       "isTelescopic": false,
                       "pricingSlabs": [
                           {
                               "min": 0,
                               "max": 50,
                               "percentage": 1,
                               "fixedAmount": 2
                           },
                           {
                               "min": 50.01,
                               "max": 100,
                               "percentage": 1,
                               "fixedAmount": 2
                           }
                       ]
                   }
                    */), operations));
    }

    public static Map pricingSlab(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                           {
                               "min": 0,
                               "max": 50,
                               "percentage": 1,
                               "fixedAmount": 2
                           }
                    */), operations));
    }

    public static Map condition(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                            {
                                "lhsExpression": "locationHierarchyIds",
                                "operator": "equals",
                                "rhsValue": {
                                    "id": "111",
                                    "name": "Bangalore",
                                    "displayLabel": "Bangalore"
                                },
                                childCondition: null
                            }
                    */), operations));

    }

    public static Map compositeCondition(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                            {
                                "operator": "and",
                                "conditions": []
                            }
                    */), operations));

    }

    public static Map conditionWithChildCondition(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                             {
                                "lhsExpression": "sender.roleWithGrade",
                                "operator": "equals",
                                "rhsValue": "DISTWS",
                                "childCondition": {
                                    "lhsExpression": "grade",
                                    "operator": "equals",
                                    "rhsValue": "GWS"
                                }
                            }
                    */), operations));

    }

    public static Map chargePayerParty(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                            {
                         "name": "Prathibha",
                            "partyId": "PT160524.0311.017823",
                            "displayLabel": "Prathibha-7740004000"

                }
                    */), operations));
    }

    public static Map taxOnTransactionAmountJson(JsonPathOperation... operations) {
        return jsonToMap(json(multiLineString(/*
                            {
                          "serviceCode": "",
                          "currency": "RUB",
                          "isInclusive": false,
                          "isTaxInclusive": false,
                          "isFinancial": true,
                          "chargePricingRange": {},
                          "discountPricingRange": {},
                          "taxPricingRange": {},
                          "chargeRules": [
                            {
                              "id": null,
                              "name": "automationCreation",
                              "startDate": "2016-12-01",
                              "condition": null,
                              "ruleType": "chargeRule",
                              "chargePricingRange": {},
                              "chargeStatements": [
                                {
                                  "pricingMethod": {
                                    "type": "flatPricing",
                                    "percentage": "0.00",
                                    "fixedAmount": "0.00"
                                  },
                                  "pricingFactor": "both",
                                  "chargePayerBankId": null,
                                  "chargePayer": "sender",
                                  "chargePayerProductId": "12",
                                  "chargeReceiver": "serviceProvider"
                                }
                              ]
                            }
                          ],
                          "discountRules": [],
                          "taxRules": []
                        }
                    */), operations));
    }
}
