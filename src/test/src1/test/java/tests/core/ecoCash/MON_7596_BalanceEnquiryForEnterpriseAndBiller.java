package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.GroupRoleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class MON_7596_BalanceEnquiryForEnterpriseAndBiller extends TestInit {

    private WebGroupRole entRole, entAdminRole;
    private User entUser;
    private OperatorUser entAdmin;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        /*
        Create separate web group role for Enterprise, Enterprise Admin
         */
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for this test");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            entRole = DataFactory.getWebRoleWithRoleName(entUser.WebGroupRole);
            entAdmin = DataFactory.getBulkPayerWithAccess("ADD_EMP", entUser); // get bulk payer admin with generic role
            entAdminRole = DataFactory.getWebRoleWithRoleName(entAdmin.WebGroupRole);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void test_01() {
        ExtentTest t1 = pNode.createNode("test_01",
                "Verify that Enterprise User without role ENQ_USC can perform self balance enquiry");
        try {
           /*
           update Group Role
            */
            GroupRoleManagement.init(t1).removeSpecificRole(entRole, new String[]{"ENQ_USC"});

            // login as Enterprise user, it must NOT get an option to enquiry Self Balance
            Login.init(t1).login(entUser);
            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_ENQ_USC"), false,
                    "Verify that Option to Do self Enquiry is not available", t1);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("enterpriseWithAppropriateRoleCanDoBalanceEnquiry",
                "Verify that Enterprise User with role ENQ_USC can perform self balance enquiry");
        try {
            GroupRoleManagement.init(t1).addSpecificRole(entRole, "ENQ_USC");
            // login as Enterprise user, it must get an option to enquiry Self Balance
            Login.init(t1).login(entUser);
            Enquiries.init(t1).doSelfBalanceEnquiry(entUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void test_03() {
        ExtentTest t1 = pNode.createNode("test_03",
                "Verify that Enterprise Admin without role ENQ_USC can NOT perform self balance enquiry");
        try {
           /*
           update Group Role
            */
            GroupRoleManagement.init(t1).removeSpecificRole(entAdminRole, new String[]{"ENQ_USC"});

            // login as Enterprise user, it must NOT get an option to enquiry Self Balance
            Login.init(t1).login(entAdmin);
            Assertion.verifyEqual(fl.isLeftNavigationAvailable("ENQUIRY_ALL", "ENQUIRY_ENQ_USC"), false,
                    "Verify that Option to Do self Enquiry is not available", t1);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("entAdminWithAppropriateRoleCanDoBalanceEnquiry",
                "Verify that Enterprise Admin User with role ENQ_USC can perform self balance enquiry");
        try {
            GroupRoleManagement.init(t1).addSpecificRole(entAdminRole, "ENQ_USC");

            // login as Enterprise Admin, it must get an option to enquiry Self Balance
            Login.init(t1).login(entAdmin);
            Enquiries.init(t1).doSelfBalanceEnquiry(entAdmin);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_05() throws Exception {
        ExtentTest t1 = pNode.createNode("billerCanPerformSelfBalanceEnquiry",
                "Verify that Biller can perform self balance enquiry");
        try {
            Biller biller = BillerManagement.init(t1).getDefaultBiller();
            // login as Enterprise Admin, it must get an option to enquiry Self Balance
            Login.init(t1).login(biller);
            Enquiries.init(t1).doSelfBalanceEnquiry(biller);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
