package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.TransferRuleManagement;
import framework.pageObjects.enquiries.TransactionDetail_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.1
 * Objective        : Transaction Enquiry via web portal must support querying with upstream reference id
 * Author Name      : Amith
 * Created Date     :
 */


public class TransactionEnquiry extends TestInit {
    private static User whs;
    private String trustBank;
    private OperatorUser net;
    private String trustBankId, externalRefId, txnID;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception, MoneyException {
        ExtentTest Setup = pNode.createNode("Setup", "Setup Specific to this Script");

        Markup m = MarkupHelper.createLabel("Setup", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker*//*


        try {


            whs = DataFactory.getChannelUserWithCategory("WHS");
            net = DataFactory.getOperatorUserWithAccess("ENQ_US", Constants.NETWORK_ADMIN);

            trustBank = DataFactory.getDefaultBankNameForDefaultProvider();
            trustBankId = DataFactory.getBankId(trustBank);

            // Configure Transfer Rule for Bank to Wallet Service
            ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, whs, whs, trustBankId, null, null, null);
            TransferRuleManagement.init(pNode).configureTransferRule(b2W);
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(whs, DataFactory.getDefaultProvider().ProviderId, trustBankId);
            String channaccount = AccountNum.get("ACCOUNT_NO");

            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(channaccount);

            //Perform bank to wallet service
            SfmResponse res = Transactions.init(Setup).bankToWalletService(whs, Constants.MIN_TRANSACTION_AMT, trustBankId, trustAccNo);
            String serReqId = res.ServiceRequestId;
            txnID = res.TransactionId;
            res.verifyMessage("bank.to.SVA.success", res.TransactionId);

            Transactions.init(Setup).performEIGTransaction(serReqId, "true", Services.RESUME_BANK_TO_WALLET);

            //fetch upstream reference number FTXNID
            MobiquityGUIQueries b = new MobiquityGUIQueries();
            externalRefId = b.dbGetLastFTXNID(Services.BANK_TO_WALLET);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1)
    public void TC_TransactionEnquiry_1() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TC_1",
                "To verify that Transaction Enquiry is success with valid Reference ID");

        try {

            Login.init(t1).login(net);
            Enquiries.init(t1).transactionDetails(txnID);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2)
    public void TC_TransactionEnquiry_2() throws Exception, MoneyException {
        ExtentTest t2 = pNode.createNode("TC_2",
                "To verify that Transaction Enquiry is success with valid External reference Number");

        try {

            Login.init(t2).login(net);
            Enquiries.init(t2).transactionDetails(externalRefId, true);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3)
    public void TC_TransactionEnquiry_3() throws Exception, MoneyException {
        ExtentTest t3 = pNode.createNode("TC_3",
                "To verify that appropriate error message should be displayed when transaction enquiry is \n" +
                        "performed with invalid reference ID");

        try {
            Login.init(t3).login(net);
            Enquiries.init(t3).startNegativeTest().transactionDetails(DataFactory.getRandomNumberAsString(5));

            Assertion.verifyErrorMessageContain("transaction.no.record.found", "Verify Error Message reference ID is Invalid", t3);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4)
    public void TC_TransactionEnquiry_4() throws Exception, MoneyException {
        ExtentTest t4 = pNode.createNode("TC_4",
                "To verify that appropriate error message should be displayed when transaction enquiry is \n" +
                        "performed with invalid external reference number");

        try {
            Login.init(t4).login(net);
            Enquiries.init(t4).startNegativeTest().transactionDetails(DataFactory.getRandomNumberAsString(5), true);

            Assertion.verifyErrorMessageContain("transaction.no.record.found", "Verify Error Message when MSISDN Does not exist", t4);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 5)
    public void TC_TransactionEnquiry_5() throws Exception, MoneyException {
        ExtentTest t5 = pNode.createNode("TC_5",
                "To verify that appropriate error message should be displayed when transaction enquiry is \n" +
                        "performed with both transaction id and external reference number");

        try {
            Login.init(t5).login(net);
            Enquiries.init(t5).startNegativeTest().transactionDetails(DataFactory.getRandomNumberAsString(0));
            TransactionDetail_page1 txnDetail = new TransactionDetail_page1(pNode);
            txnDetail.navigateToLink();
            txnDetail.setTransactionID(txnID);
            txnDetail.setExternalReferenceID(externalRefId);
            txnDetail.clickSubmitButton();

            Assertion.verifyErrorMessageContain("error.transactionDetailsByWingList.empty", "Verify Error Message when MSISDN Does not exist", t5);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 6)
    public void TC_TransactionEnquiry_6() throws Exception, MoneyException {
        ExtentTest t6 = pNode.createNode("TC_6",
                "To verify that appropriate error message should be displayed when transaction enquiry is \n" +
                        "performed without referenceid and external reference number");

        try {
            Login.init(t6).login(net);
            TransactionDetail_page1 txnDetail = new TransactionDetail_page1(pNode);
            txnDetail.navigateToLink();
            txnDetail.clickSubmitButton();

            Assertion.verifyErrorMessageContain("transaction.parameter.blank", "Verify Error Message when MSISDN Does not exist", t6);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();

    }


}

