package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;

import static framework.util.common.Assertion.finalizeSoftAsserts;

public class MON_7581_Remark_For_O2C_And_Reimbursement extends TestInit {
    private User channelUser;
    private OperatorUser o2cInitiator, o2cApprover1, o2cApprover2;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            o2cApprover2 = DataFactory.getOperatorUserWithAccess("O2C_APP2");

            Login.init(setup).login(o2cInitiator); //set o2c transfer limit to minimum value for level 2 approval
            TransferRuleManagement.init(setup).setO2CTransferLimitWithAmount(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, "1");

            ServiceCharge sCharge = new ServiceCharge
                    (Services.OPERATOR_WITHDRAW, channelUser, o2cInitiator, null, null, null, null);

            TransferRuleManagement.init(setup).
                    configureTransferRule(sCharge);

            Utils.putThreadSleep(60000); //wait for 1 min to reload the cache
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest test = pNode.createNode("MON_7581_Test_01", "verify that after initiating O2C, the " +
                "approver 1 should be able to see & override the remark mentioned by O2C initiator while approving O2C Transfer");
        try {
            String actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cInitiator);

            String txnId = TransactionManagement.init(test).initiateO2C(channelUser, "5", actualRemark);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Initiation", test);

            actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cApprover1);

            TransactionManagement.init(test).approveO2CLevelOne(txnId, actualRemark);

            expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Approver One", test);

            test = pNode.createNode("MON_7581_Test_03", "verify that after approving O2C from approver 1, approver 2 should be able to see " +
                    "& override the remark mentioned by O2C approver 1/initiator while approving O2C Transfer");

            actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cApprover2);
            TransactionManagement.init(test).o2cApproval2(txnId, actualRemark);

            expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Approver Two", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("MON_7581_Test_02", "verify that if approver 1 doesn't add " +
                "any new remark while approving/rejecting O2C Transfer, then the remark entered by initiator should be persist. ");
        try {
            String actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cInitiator);
            String txnId = TransactionManagement.init(test).initiateO2C(channelUser, "5", actualRemark);

            Login.init(test).login(o2cApprover1);
            TransactionManagement.init(test).approveO2CLevelOne(txnId);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Approver One", test);

            test = pNode.createNode("MON_7581_Test_04", "verify that if approver 2 doesn't add any new " +
                    "remark while approving/rejecting O2C Transfer, then the remark entered by initiator/approver 1 should be persist.");

            Login.init(test).login(o2cApprover2);
            TransactionManagement.init(test).o2cApproval2(txnId);

            expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Approver Two", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("MON_7581_Test_01_Reject_One", "verify that after initiating O2C, the " +
                "approver 1 should be able to see & override the remark mentioned by O2C initiator while rejecting O2C Transfer");
        try {
            String actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);
            Login.init(test).login(o2cInitiator);
            String txnId = TransactionManagement.init(test).initiateO2C(channelUser, "5", actualRemark);

            actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cApprover1);
            TransactionManagement.init(test).rejectO2CLevel1(txnId, actualRemark);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.verifyEqual(actualRemark, expectedRemark, "Verify Remark For Approver Two", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode("MON_7581_Test_01_Reject_Two", "verify that after approving O2C from approver 1, approver 2 should be able to see " +
                "& override the remark mentioned by O2C approver 1/initiator while rejecting O2C Transfer");
        try {
            String actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cInitiator);
            String txnId = TransactionManagement.init(test).initiateO2C(channelUser, "5", actualRemark);

            actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cApprover1);
            TransactionManagement.init(test).approveO2CLevelOne(txnId, actualRemark);

            actualRemark = "O2C" + DataFactory.getRandomNumberAsString(3);

            Login.init(test).login(o2cApprover2);
            TransactionManagement.init(test).rejectO2CLevel2(txnId, actualRemark);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.assertEqual(actualRemark, expectedRemark, "Approver 2 remark", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void Test_05() {
        ExtentTest test = pNode.createNode("MON_7581_Test_05", "verify that after initiating Stock Reimbursement, the approver should be " +
                "able to see & override the remark mentioned by Stock Reimbursement Initiator while approving Stock Reimbursement Transfer ");
        try {
            String referenceNum = DataFactory.getRandomNumberAsString(3);
            String actualRemark = "StockRim" + referenceNum;

            String txnId = StockManagement.init(test)
                    .initiateStockReimbursement(channelUser, referenceNum, "1", actualRemark, Constants.NORMAL_WALLET);

            actualRemark = "StockRim" + referenceNum;

            StockManagement.init(test).approveReimbursement(txnId, actualRemark);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.assertEqual(actualRemark, expectedRemark, "Approver remark", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void Test_06() {
        ExtentTest test = pNode.createNode("MON_7581_Test_06", " verify that if approver doesn't add any " +
                "new remark while approving Stock Reimbursement Transfer, then the remark entered by initiator should be persist.");
        try {
            String referenceNum = DataFactory.getRandomNumberAsString(3);
            String actualRemark = "StockRim" + referenceNum;

            String txnId = StockManagement.init(test)
                    .initiateStockReimbursement(channelUser, referenceNum, "1", actualRemark, Constants.NORMAL_WALLET);

            StockManagement.init(test).approveReimbursement(txnId);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.assertEqual(actualRemark, expectedRemark, "Initiator remark", test);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void Test_07() {
        ExtentTest test = pNode.createNode("MON_7581_Test_07", "verify that after initiating Stock Reimbursement, the approver should be " +
                "able to see & override the remark mentioned by Stock Reimbursement Initiator while rejecting Stock Reimbursement Transfer");
        try {
            String referenceNum = DataFactory.getRandomNumberAsString(3);
            String actualRemark = "StockRim" + referenceNum;

            String txnId = StockManagement.init(test)
                    .initiateStockReimbursement(channelUser, referenceNum, "1", actualRemark, Constants.NORMAL_WALLET);

            actualRemark = "StockRim" + referenceNum;

            Login.init(test).login(o2cApprover1);
            StockManagement.init(test).rejectReimbursement(txnId, actualRemark);

            String expectedRemark = getTxnRemarkFromDb(txnId);

            Assertion.assertEqual(actualRemark, expectedRemark, "Approver remark", test);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        finalizeSoftAsserts();
    }

    @BeforeClass(alwaysRun = true)
    public void postCondition() {
        ExtentTest teardown = pNode.createNode("teardown", "Post Condition");
        try {
            Login.init(teardown).login(o2cInitiator);
            TransferRuleManagement.init(teardown).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName);
        } catch (Exception e) {
            markTeardownAsFailure(e);
        }
        finalizeSoftAsserts();
    }

    private String getTxnRemarkFromDb(String txnId) {
        OracleDB conn = new OracleDB();
        String remark = null;
        try {
            ResultSet resultSet = conn.RunQuery("select remarks from mtx_transaction_header where transfer_id = '" + txnId + "'");
            while (resultSet.next()) {
                remark = resultSet.getString("REMARKS");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return remark;
    }

}
