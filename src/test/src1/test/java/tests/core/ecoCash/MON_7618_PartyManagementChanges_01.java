package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.base.Joiner;
import com.google.common.collect.Multimap;
import framework.dataEntity.Partner;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubcriber_Page3;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

public class MON_7618_PartyManagementChanges_01 extends TestInit {

    private User channelUser;
    private Multimap thirdPartyDbResults;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            channelUser = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void MON_7618_001() {
        ExtentTest t1 = pNode.createNode("MON_7618_001",
                "Verify that third party options are displayed, " +
                        "verify selecting a partner displays the list of mandatory fields for that partner");

        try {
            Login.init(t1).login(channelUser);
            AddSubscriber_Page1 subscriberRegPage = AddSubscriber_Page1.init(t1).navAddSubscriber();
            subscriberRegPage.selectThirdPartyOption(GlobalData.partnerMap);

            for (Partner p1 : GlobalData.partnerMap) {
                try {
                    String fields = Joiner.on(",").join(p1.mandatoryList);    //get the fields corresponding to this key in a single string
                    String searchString = "Mandatory Fields for " + p1.partnerName.toLowerCase() + " are " + fields;
                    //* check '.' in xpath and refactor accordingly
                    String xpath = ".//label[contains(text(), '" + searchString + "')]";
                    boolean isThirdPartyMandatoryFieldsListPresent = Utils.checkElementPresent(xpath, Constants.FIND_ELEMENT_BY_XPATH);
                    Utils.captureScreen(t1);
                    Assertion.verifyEqual(isThirdPartyMandatoryFieldsListPresent, true,
                            "Verify that third party mandatory fields are displayed when clicking on the corresponding third party option",
                            t1, true);
                } catch (Exception e) {
                    markTestAsFailure(e, t1);
                }

            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void MON_7618_002() {
        ExtentTest t1 = pNode.createNode("MON_7618_002", "Verify that not entering value for a mandatory field gives an error");

        try {
            User sub_01 = new User(Constants.SUBSCRIBER);
            //fill all the field values and empty a mandatory field
            Login.init(t1).login(channelUser);
            ConfigInput.shouldClickNext = false;
            AddSubscriber_Page1 subscriberRegPage = AddSubscriber_Page1.init(t1).navAddSubscriber();

            Partner p1 = GlobalData.partnerMap.get(0);
            sub_01.addThirdParty(p1);
            SubscriberManagement.init(t1).addInitiateSubscriber(sub_01);
            String fieldName = p1.mandatoryList.get(0);
            try {
                WebElement w = driver.findElement(By.xpath("//select[@name='" + fieldName + "']"));
                Select s = new Select(w);
                s.selectByVisibleText("Select");
            } catch (Exception e) {
                WebElement w = driver.findElement(By.xpath("//input[@name='" + fieldName + "']"));
                w.sendKeys("");
            }
            subscriberRegPage.clickNext();
            Utils.captureScreen(t1);
            Assertion.verifyErrorMessageContain("subs.registration.mandatory.missing",
                    "Verify that expected error message appears when a mandatory field is empty",
                    t1, fieldName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void MON_7618_003() {

        ExtentTest t1 = pNode.createNode("MON_7618_003",
                "Verify that expected third party options are preselected on subscriber modification page");

        try {
            Partner p1 = GlobalData.partnerMap.get(0);
            User sub_02 = new User(Constants.SUBSCRIBER);
            sub_02.addThirdParty(p1);
            SubscriberManagement.init(t1).createSubscriber(sub_02);
            Assertion.verifyListContains(MobiquityGUIQueries.getPreSelectedPartners(sub_02.MSISDN), p1.partnerName,
                    "Verify that the selected partner is inserted into the database correctly", t1);

            // verify subscriber Modification page
            Login.init(t1).login(channelUser);
            ModifySubscriber_page1.init(t1).navSubscriberModification().setMSISDN(sub_02.MSISDN).clickOnSubmitPg1();

            //getting preselected values
            List<String> preselectedPartners = MobiquityGUIQueries.getPreSelectedPartners(sub_02.MSISDN);
            //check the preselected values in the third party options
            Boolean checkSelected = true;
            List<WebElement> checkBoxes = DriverFactory.getDriver().findElements(By.cssSelector(".checkboxLabel"));
            if (checkBoxes.size() > 0) {
                for (WebElement element : checkBoxes) {
                    if (preselectedPartners.contains(element.getText()) && !DriverFactory.getDriver().findElement(By.id(element.getAttribute("for"))).isSelected()) {
                        checkSelected = false;
                    }
                }
            } else {
                markTestAsFailure("Partners are not shown on the modification page", t1);
            }

            Utils.captureScreen(t1);
            Assertion.verifyEqual(checkSelected, true, "Verify that the third parties associated with the subscriber are pre-selected on modification page", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7618_004() {
        ExtentTest t1 = pNode.createNode("MON_7618_004", "Verify that the values entered in the mandatory fields of the selected third party are shown on the confirmation page");

        try {
            Partner p1 = GlobalData.partnerMap.get(0);
            User sub2 = new User(Constants.SUBSCRIBER);
            sub2.addThirdParty(p1);

            Login.init(t1).login(channelUser);
            SubscriberManagement.init(t1).addInitiateSubscriber(sub2);
            CommonUserManagement.init(t1).assignWebGroupRole(sub2);
            AddSubcriber_Page3 page3 = new AddSubcriber_Page3(t1);
            page3.selectGrade();
            Thread.sleep(Constants.WAIT_TIME);
            page3.clickSubmit();
            // page3.selectBankStatus();
            page3.clickSubmit2();

            //* add this element to Page Object
            String confirmationPartners = driver.findElement(By.id("subsRegistrationServiceBean_confirm_selectdPartnerName")).getText();

            Boolean partnerPresentInConfirmation = true;
            if (!confirmationPartners.toLowerCase().contains(p1.partnerName.toLowerCase()))
                partnerPresentInConfirmation = false;
            Utils.captureScreen(t1);
            Assertion.verifyEqual(partnerPresentInConfirmation, true, "Selected third parties are present on confirmation page", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }
}
