package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class MON_7588_PreSelectedCountryOfResidenceAndIDType extends TestInit {

    private User chAddSubs;
    private Object[][] testData;

    @DataProvider(name = "testData")
    public static Object[][] getTestData() throws Exception {
        String filePath = FilePath.dirTestData + "MON_7588_DataFile.xlsx";
        return ExcelUtil.getTableArray(filePath, 0);
    }

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this test");

        try {
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, dataProvider = "testData", groups = {FunctionalTag.MONEY_SMOKE})
    public void verify_7588(String testId, String description, String isExecute, String uiField, String prefCode, String prefValue, String expected) {
        if (isExecute.equalsIgnoreCase("yes")) {
            ExtentTest t1 = pNode.createNode(testId, description);
            try {
                SystemPreferenceManagement.init(t1)
                        .updateSystemPreference(prefCode, prefValue);

                Login.init(t1).login(chAddSubs);

                String selectedData = "";
                if (uiField.equalsIgnoreCase("COUNTRY")) {
                    selectedData = AddSubscriber_Page1.init(t1)
                            .navAddSubscriber()
                            .getSelectedCountryOfResidence();
                } else if (uiField.equalsIgnoreCase("IDTYPE")) {
                    selectedData = AddSubscriber_Page1.init(t1)
                            .navAddSubscriber()
                            .getSelectedIDType();
                }

                Assertion.assertEqual(selectedData, expected, description, t1);

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }
            Assertion.finalizeSoftAsserts();
        } else {
            System.out.println("Skipp Test!");
        }
    }

    @Test(priority = 2)
    public void verify_confirmation_page() throws Exception {
        ExtentTest t2 = pNode.createNode("MON_7588_CONFIRMPAGE", "To verify that the set system preference is reflected on the confirmation page for subscriber registration");
        try {

            //Setting values in system preferences for Country of Residence and ID Type
            SystemPreferenceManagement.init(t2)
                    .updateSystemPreference("DEFAULT_COUNTRY_FOR_RESIDENCE", "IN");
            SystemPreferenceManagement.init(t2)
                    .updateSystemPreference("DEFAULT_ID_TYPE", "PASSPORT");

            Login.init(t2).login(chAddSubs);

            ConfigInput.isAssert = false;
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t2).addSubscriber(subs, false);

            String residenceCountryConfirm = AddChannelUser_pg5.GetCountryOfResidenceOnConfirmationPage();
            String idTypeConfirm = AddChannelUser_pg5.GetSysIDOnConfirmationPage();

            Utils.captureScreen(t2);
            Assertion.verifyEqual(residenceCountryConfirm, "Albania", "Verify that system preference for Country of Residence is reflecting on the Subscriber Registration's Confirmation Page", t2, false);
            Assertion.verifyEqual(idTypeConfirm, "Passport", "Verify that system preference for IDType is reflecting on the Subscriber Registration's Confirmation Page", t2, false);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }
}
