package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.pageObjects.enquiries.ChannelSubsEnqPage1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.propertiesManagement.ConstantsProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;
import java.util.List;

public class QRCodeTests extends TestInit {
    boolean isQRCodeEnabled = false;
    private List<String> categoryList;

    @BeforeClass(alwaysRun = true)
    public void setup() {

        /*user = new ChannelUser();
        user.setMSISDN("27440901");
        user.setCategoryCode("MER");
        netAdmLoginID="netadmin";
        netAdminPasswd="Com@4827";*/

        try {
            if (ConstantsProperties.getInstance().getProperty("IS_QR_CODE_ENABLED").equalsIgnoreCase("Y"))
                isQRCodeEnabled = true;

            categoryList = Arrays.asList(ConstantsProperties.getInstance().getProperty("QR_CODE_ALLOWED_CATEGORIES").split(","));

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1)
    public void QR_CODE_01_02() {
        ExtentTest t1 = pNode.createNode("QR-CODE-01",
                "To verify the 'Generate QR Code' link is available for the mentioned category users ");

        try {

            if (!isQRCodeEnabled || categoryList.size() == 0) {
                t1.warning("Please check properties 'IS_QR_CODE_ENABLED' and 'QR_CODE_ALLOWED_CATEGORIES'");
                return;
            }

            ChannelSubsEnqPage1 page = new ChannelSubsEnqPage1(t1);
            OperatorUser netadmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            //* above user can be fetched based on role code too

            // login as Network admin
            Login.init(t1).login(netadmin);

            // iterate over category QR_CODE_ALLOWED_CATEGORIES
            for (String catCode : categoryList) {
                User chUser = new User(catCode);

                String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

                Enquiries.init(t1)
                        .performChannelSubsEnquiry(chUser);

                Assertion.verifyEqual(page.isGenerateQRCodeLinkVisible(),
                        true,
                        "Verify the Link is Visible QR Code available for category: " + chUser.CategoryName, t1,
                        true);

                String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

                ExtentTest t2 = pNode.createNode("QR-CODE-02",
                        "To verify the file should be downloaded once user clicks on 'Generate QR code link'");

                Assertion.verifyEqual(Utils.isFileDownloaded(oldFile, newFile),
                        true,
                        "Verify File is Successfully Downloaded", t2);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
