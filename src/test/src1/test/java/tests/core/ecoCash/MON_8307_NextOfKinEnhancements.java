package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.KinUser;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class MON_8307_NextOfKinEnhancements extends TestInit {

    private User chAddSubs;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        ExtentTest t1 = pNode.createNode("setup", "Setup specific for this suite");

        try {
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_AGE_LIMIT_REQUIRE", "Y");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_8307_01() {
        ExtentTest t1 = pNode.createNode("MON_8307_01_a",
                "UI Verify that the Kin first name can not exceed 80 characters");
        try {

            KinUser kinUser = new KinUser();

            // set Kin first name of 81 character
            kinUser.FirstName = DataFactory.getRandomString(81);
            kinUser.MiddleName = DataFactory.getRandomString(81);
            kinUser.LastName = DataFactory.getRandomString(81);
            kinUser.RelationShip = DataFactory.getRandomString(81);

            User sub0 = new User(Constants.SUBSCRIBER);
            sub0.setKinUser(kinUser);

            // as from UI only 80 char can be set, verify on confirmation page that Kin First Name is Set to 80 Char
            Login.init(t1).login(chAddSubs);

            ConfigInput.isAssert = false;
            SubscriberManagement.init(t1)
                    .addSubscriber(sub0, false);

            // verify that none of the above fields are set more than 80
            Utils.scrollToAnElement(AddChannelUser_pg5.init(t1).confirmKinRelationShip);
            Assertion.verifyEqual(AddChannelUser_pg5.init(t1).getConfirmKinFirstName().length(), 80,
                    "Verify that max allowed First Name is 80", t1, true);

            ExtentTest t2 = pNode.createNode("MON_8307_01_b",
                    "UI Verify that the Kin Last name can not exceed 80 characters");
            Assertion.verifyEqual(AddChannelUser_pg5.init(t2).getConfirmKinLastName().length(), 80,
                    "Verify that max allowed Last Name is 80", t2, true);

            ExtentTest t3 = pNode.createNode("MON_8307_01_c",
                    "UI Verify that the Kin middle name can not exceed 80 characters");
            Assertion.verifyEqual(AddChannelUser_pg5.init(t3).getConfirmKinMiddleName().length(), 80,
                    "Verify that max allowed Middle Name is 80", t3, true);

            ExtentTest t4 = pNode.createNode("MON_8307_01_d",
                    "UI Verify that the Kin relationship can not exceed 80 characters");
            Assertion.verifyEqual(AddChannelUser_pg5.init(t4).getConfirmKinRelationShip().length(), 80,
                    "Verify that max allowed Relationship is 80", t4, true);

            // verify that Subscribed with kin details are successfully created from UI
            ExtentTest t5 = pNode.createNode("MON_8307_01_e",
                    "UI Verify that the Subscribed is successfully created " +
                            "with providing firstname, middlename, lastname, relationship text not exceeding 80 character");
            ConfigInput.isAssert = true;
            SubscriberManagement.init(t5)
                    .createSubscriber(sub0);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void MON_8307_02() {
        ExtentTest t1 = pNode.createNode("MON_8307_02_a",
                "API CUSTREG, Verify that the Kin first name can not exceed 80 characters");
        try {

            User sub1 = new User(Constants.SUBSCRIBER);
            KinUser kinUser = new KinUser();
            sub1.setKinUser(kinUser);

            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(sub1, defaultWallet.WalletId);

            ConfigInput.isAssert = false;

            // set invalid First name
            kinUser.FirstName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t1)
                    .subsSelfRegistrationWithPayId(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.First.Name.Length.Invalid");

            ExtentTest t2 = pNode.createNode("MON_8307_02_b",
                    "API CUSTREG, Verify that the Kin middle name can not exceed 80 characters");

            // set invalid Middle Name
            kinUser.FirstName = DataFactory.getRandomString(80);
            kinUser.MiddleName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t2)
                    .subsSelfRegistrationWithPayId(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Middle.Name.Length.Invalid");


            ExtentTest t3 = pNode.createNode("MON_8307_02_c",
                    "API CUSTREG, Verify that the Kin Last name can not exceed 80 characters");
            // set invalid LastName Name
            kinUser.MiddleName = DataFactory.getRandomString(80);
            kinUser.LastName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t3)
                    .subsSelfRegistrationWithPayId(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Last.Name.Length.Invalid");

            ExtentTest t4 = pNode.createNode("MON_8307_02_d",
                    "API CUSTREG, Verify that the Kin relationship can not exceed 80 characters");
            // set invalid relationship
            kinUser.LastName = DataFactory.getRandomString(80);
            kinUser.RelationShip = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t4)
                    .subsSelfRegistrationWithPayId(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Relationship.Length.Invalid");

            ExtentTest t5 = pNode.createNode("MON_8307_02_e",
                    "API CUSTREG, Verify that the Subscribed is successfully created " +
                            "with providing firstname, middlename, lastname, relationship text not exceeding 80 character");
            // create a valid User
            ConfigInput.isAssert = true;
            kinUser.RelationShip = DataFactory.getRandomString(80);
            sub1.setKinUser(kinUser);
            Transactions.init(t5)
                    .subsSelfRegistrationWithPayId(sub1, defaultWallet.WalletId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void MON_8307_03() {
        ExtentTest t1 = pNode.createNode("MON_8307_03_a",
                "API REGST, Verify that the Kin first name can not exceed 80 characters");
        try {

            User sub1 = new User(Constants.SUBSCRIBER);
            KinUser kinUser = new KinUser();
            sub1.setKinUser(kinUser);

            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(sub1, defaultWallet.WalletId);

            ConfigInput.isAssert = false;

            // set invalid First name
            kinUser.FirstName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t1)
                    .subRegistrationByAS400(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.First.Name.Length.Invalid");

            ExtentTest t2 = pNode.createNode("MON_8307_03_b",
                    "API REGST, Verify that the Kin middle name can not exceed 80 characters");
            // set invalid Middle Name
            kinUser.FirstName = DataFactory.getRandomString(80);
            kinUser.MiddleName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t2)
                    .subRegistrationByAS400(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Middle.Name.Length.Invalid");


            ExtentTest t3 = pNode.createNode("MON_8307_03_c",
                    "API REGST, Verify that the Kin Last name can not exceed 80 characters");
            // set invalid LastName Name
            kinUser.MiddleName = DataFactory.getRandomString(80);
            kinUser.LastName = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t3)
                    .subRegistrationByAS400(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Last.Name.Length.Invalid");

            ExtentTest t4 = pNode.createNode("MON_8307_03_d",
                    "API REGST, Verify that the Kin relationship can not exceed 80 characters");
            // set invalid relationship
            kinUser.LastName = DataFactory.getRandomString(80);
            kinUser.RelationShip = DataFactory.getRandomString(81);
            sub1.setKinUser(kinUser);
            Transactions.init(t4)
                    .subRegistrationByAS400(sub1, defaultWallet.WalletId)
                    .verifyMessage("Kin.Relationship.Length.Invalid");

            ExtentTest t5 = pNode.createNode("MON_8307_03_e",
                    "API REGST, Verify that the Subscribed is successfully created " +
                            "with providing firstname, middlename, lastname, relationship text not exceeding 80 character");
            // create a valid User
            ConfigInput.isAssert = true;
            kinUser.RelationShip = DataFactory.getRandomString(80);
            sub1.setKinUser(kinUser);
            Transactions.init(t5)
                    .subRegistrationByAS400(sub1, defaultWallet.WalletId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void MON_8307_04() {
        ExtentTest t1 = pNode.createNode("MON_8307_04",
                "API CUSTREG Verify KinIdNumber is controlled by MIN_EXT_CODE_LENGTH & EXTERNAL_CODE_LENGTH");
        try {

            KinUser kinUser = new KinUser();
            User sub2 = new User(Constants.SUBSCRIBER);

            // set the kid Id number length less than the Preference defined MIN_EXT_CODE_LENGTH
            kinUser.KinNumber = DataFactory.getRandomNumberAsString(AppConfig.minExternalCodeLength - 1);
            sub2.setKinUser(kinUser);

            // registration must fail will proper error
            ConfigInput.isAssert = false;
            Transactions.init(t1)
                    .subsSelfRegistrationWithPayId(sub2, defaultWallet.WalletId)
                    .verifyMessage("Kin.id.number.Length.Invalid");


            // set the id num more than the EXTERNAL_CODE_LENGTH, operation must be successful
            kinUser.KinNumber = DataFactory.getRandomNumberAsString(AppConfig.externalCodeLength + 1);
            sub2.setKinUser(kinUser);

            // registration must pass
            ConfigInput.isAssert = false;
            Transactions.init(t1)
                    .subsSelfRegistrationWithPayId(sub2, defaultWallet.WalletId)
                    .verifyMessage("Kin.id.number.Length.Invalid");

            if (AppConfig.externalCodeLength != AppConfig.minExternalCodeLength) {
                // set the id num less than the EXTERNAL_CODE_LENGTH, operation must be successful
                kinUser.KinNumber = DataFactory.getRandomNumberAsString(AppConfig.externalCodeLength - 1);
                sub2.setKinUser(kinUser);
            }

            // registration must pass
            ConfigInput.isAssert = true;
            kinUser.KinNumber = DataFactory.getRandomNumberAsString(AppConfig.externalCodeLength);
            sub2.setKinUser(kinUser);
            Transactions.init(t1)
                    .subsSelfRegistrationWithPayId(sub2, defaultWallet.WalletId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void MON_8307_05() {
        ExtentTest t1 = pNode.createNode("MON_8307_05",
                "BULK Verify that the Kin first name, middle name, last Name and relationship can not exceed 80 characters");
        try {

            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess(Roles.BULK_USER_REGISTRATION_AND_MODIFICATION);

            KinUser kinUser = new KinUser();
            kinUser.FirstName = DataFactory.getRandomString(81);
            kinUser.MiddleName = DataFactory.getRandomString(81);
            kinUser.LastName = DataFactory.getRandomString(81);
            kinUser.RelationShip = DataFactory.getRandomString(81);
            User sub5 = new User(Constants.SUBSCRIBER);
            sub5.setKinUser(kinUser);

            Login.init(t1).login(optUsr);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t1);

            page.navBulkChUserAndSubsciberRegistrationAndModification()
                    .SelectSubscriberRegistrationService();

            SubscriberManagement.init(t1)
                    .writeDataToBulkSubsRegistrationCsv(sub5,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false)
            ;

            String batchId = page.uploadFile(FilePath.fileBulkSubReg)
                    .submitCsv()
                    .getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage()
                    .ClickOnBatchSubmit()
                    .downloadLogFileUsingBatchId(batchId)
                    .verifyErrorMessageInLogFile("Kin.First.Name.Length.Invalid.Bulk", batchId, t1, kinUser.FirstName)
                    .verifyErrorMessageInLogFile("Kin.Last.Name.Length.Invalid.Bulk", batchId, t1, kinUser.LastName)
                    .verifyErrorMessageInLogFile("Kin.Middle.Name.Length.Invalid.Bulk", batchId, t1, kinUser.MiddleName)
                    .verifyErrorMessageInLogFile("Kin.Relationship.Name.Length.Invalid.Bulk", batchId, t1, kinUser.RelationShip);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
