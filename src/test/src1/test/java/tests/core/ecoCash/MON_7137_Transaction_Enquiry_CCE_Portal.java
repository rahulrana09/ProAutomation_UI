package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : v5.0  Grade Network Association
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 26/11/2018.
 */
public class  MON_7137_Transaction_Enquiry_CCE_Portal extends TestInit {

    private String externalReferenceId;
    private User sub;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        ExtentTest test = pNode.createNode("Setup", "Initiating Test");
        try {
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            TransactionManagement.init(test)
                    .makeSureLeafUserHasBalance(sub);

            ServiceCharge sva2bank = new ServiceCharge(Services.SVA_TO_BANK, sub, sub,
                    null, null, null, null);

            TransferRuleManagement.init(test)
                    .configureTransferRule(sva2bank);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void mon_7137_01() throws MoneyException {
        ExtentTest test = pNode.createNode("MON-7137-01", "Verify That After Performing SVA to Bank, CCE " +
                ".should be able to search transaction details On cce portal based on External Reference ID");
        try {
            String serviceId = Transactions.init(test)
                    .walletToBankService(sub, "1", defaultBank.BankID).ServiceRequestId;

            String txnId = Transactions.init(test)
                    .resumeAmbiguousTransaction(Services.RESUME_WALLET_TO_BANK, serviceId, "true").TransactionId;

            externalReferenceId = MobiquityGUIQueries.getExternalReferenceId(txnId);
            Enquiries.init(test)
                    .getTxnInfoFromExternalReferenceId(Constants.USR_TYPE_SUBS, Constants.MOBILENUMBER, sub.MSISDN, externalReferenceId)
                    .validateTransactionDetails(txnId, Constants.TXN_SUCCESS_STRING).switchToMainWindow();

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Test_02", "Verify That Proper Error Message should display in" +
                " case of blank  External Reference Id on cce portal");
        try {
            Enquiries.init(test)
                    .getTxnInfoFromExternalReferenceId(Constants.USR_TYPE_SUBS, Constants.MOBILENUMBER, sub.MSISDN, " ");

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            String actualErrorMsg = driver.findElement(By.id("invalidTxnIdMessage")).getText();
            Assertion.verifyEqual(actualErrorMsg, MessageReader.getMessage("invalid.txn.id", null),
                    "External Reference Id Cannot be blank", test, true);

            Enquiries.init(test).switchToMainWindow();
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Test_03", "Verify That Proper Error Message should display in case of " +
                "invalid External Reference Id Is Entered By CCE in CCE portal");
        try {
            externalReferenceId = DataFactory.getRandomNumberAsString(5);
            Enquiries.init(test)
                    .getTxnInfoFromExternalReferenceId(Constants.USR_TYPE_SUBS, Constants.MOBILENUMBER, sub.MSISDN, externalReferenceId);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            String actualErrorMsg = driver.findElement(By.id("invalidTxnIdMessage")).getText();
            Assertion.verifyEqual(actualErrorMsg, MessageReader.getMessage("invalid.txn.id", null),
                    "Record Not Exists in Database", test, true);

            Enquiries.init(test).switchToMainWindow();
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
