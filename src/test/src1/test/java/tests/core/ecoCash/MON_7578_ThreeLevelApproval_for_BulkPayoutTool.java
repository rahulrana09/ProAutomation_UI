package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkStockLiquidationCSV;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MON_7578_ThreeLevelApproval_for_BulkPayoutTool extends TestInit {

    private OperatorUser netAdminInitiator, netAdminApprover_1, netAdminApprover_2, bankAdmApprove_L3_BNK_1, bankUsrApprove_L3_BNK_1, bankAdmApprove_L3_BNK_2;
    private String liqBankName;
    private Bank trustBank, nonTrustBank;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        try {
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            liqBankName = DataFactory.getDefaultLiquidationBankName(defaultProvider.ProviderId);

            netAdminInitiator = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);
            netAdminApprover_1 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE", Constants.NETWORK_ADMIN);
            netAdminApprover_2 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE_2", Constants.NETWORK_ADMIN);
            bankAdmApprove_L3_BNK_1 = DataFactory.getBankAdminWithAccessAndBank("NEW_BULK_APPROVE_3", trustBank.BankName);
            bankAdmApprove_L3_BNK_2 = DataFactory.getBankAdminWithAccessAndBank("NEW_BULK_APPROVE_3", nonTrustBank.BankName);
            bankUsrApprove_L3_BNK_1 = DataFactory.getBankUserWithAccessAndBank("NEW_BULK_APPROVE_3", trustBank.BankName);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7578_01() {
        ExtentTest t1 = pNode.createNode("MON_7578_01",
                "Verify that Operator user with access can initiating Stock Liquidation Successfully for Channel User");
        try {
            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(new ServiceCharge
                            (Services.STOCK_LIQUIDATION_SERVICE,
                                    chUser,
                                    netAdminInitiator,
                                    null,
                                    null,
                                    null,
                                    null)
                    );

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(chUser);

            /*
            Initiate Stock Liquidation
            download the csv file from stock liquidation approval Page1
             */
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(chUser, batchId);
            String liqTxnId = MobiquityGUIQueries.getLiquidationId(chUser.MSISDN, batchId);

            Login.init(t1).login(netAdminInitiator);
            String fileName = StockManagement.init(t1)
                    .downloadStockLiquidationCSV(liqBankName, defaultProvider.ProviderId);

            /*
            Once the file is successfully downloaded, get the information only relevant to chUser
            Update the csv file
            Make sure that liquidation amount is a little less than available balance, as service charges are configured,
            initiation / approval can yield error
             */
            BulkStockLiquidationCSV entry1 = StockManagement.init(t1).getStockLiquidationDetailsForUser(batchId, fileName);
            entry1.liquidationAmount = new BigDecimal(entry1.liquidationAmount).divide(new BigDecimal(2)).toString();
            List<BulkStockLiquidationCSV> stockLiquidationList = new ArrayList<>();
            stockLiquidationList.add(entry1);

            String fileName1 = BulkPayoutTool.init(t1)
                    .downloadAndUpdateBulkStockLiquidationCsv(stockLiquidationList);

            String batchId1 = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, fileName1);


            if (batchId1 != null) {
                ExtentTest t2 = pNode.createNode("MON_7578_02",
                        "Level 1 Approval | Verify that Operator User with access should be able to Provide the Trust Bank and approve Stock Liquidation Level 1");
                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(t2)
                        .approveNewBulkPayoutForStockLiquidation(netAdminApprover_1,
                                 batchId1, trustBank.BankName, true, 1);

                ExtentTest t3 = pNode.createNode("MON_7578_03",
                        "Level 2 Approval | Verify that Operator User with access should be able to Provide the Trust Bank and approve Stock Liquidation Level 2");
                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(t3)
                        .approveNewBulkPayoutForStockLiquidation(netAdminApprover_2, batchId1, trustBank.BankName, true, 2);


                /*ExtentTest t4 = pNode.createNode("MON_7578_04",
                        "Level 3 Approval | Verify that Operator User Not associated with the Bank routed in approval level 1, should not be able to approve Level 3");
                Login.init(t3).login(bankAdmApprove_L3_BNK_2);
                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(t3)
                        .approveNewBulkPayoutForStockLiquidation(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, batchId1, trustBank.BankName, true, 3);*/

                ExtentTest t5 = pNode.createNode("MON_7578_05",
                        "Level 3 Approval | Verify that Operator User associated with the Bank, which was used as route bank in approval level 1" +
                                "should  be able to approve Level 3");
                //Approve bulk payout for Stock Liquidation
                BulkPayoutTool.init(t5)
                        .approveNewBulkPayoutForStockLiquidation(bankAdmApprove_L3_BNK_1, batchId1, trustBank.BankName, true, 3);


                ExtentTest t6 = pNode.createNode("MON_7578_06",
                        "Verify Dashboard after Approving Stock Liquidation");
                Login.init(t6).login(netAdminInitiator);
                //Check Bulk in Dashboard
                BulkPayoutTool.init(t6)
                        .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, batchId1, true, true);

            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void MON_7578_02() {
        ExtentTest t1 = pNode.createNode("MON_7578_07",
                "Enterprise | Verify that Operator user with access can initiating Stock Liquidation Successfully");
        try {

            User entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(new ServiceCharge
                            (Services.STOCK_LIQUIDATION_SERVICE,
                                    entUser,
                                    netAdminInitiator,
                                    null,
                                    null,
                                    null,
                                    null)
                    );

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(entUser);

            BulkPayoutTool.init(t1)
                    .initiateAndApproveStockLiquidationForChannelUser(entUser,
                            trustBank.BankName,
                            liqBankName,
                            defaultProvider.ProviderId,
                            "100");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
