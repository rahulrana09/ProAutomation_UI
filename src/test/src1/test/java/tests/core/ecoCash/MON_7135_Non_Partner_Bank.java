package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.bankMaster.AddBank_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

public class MON_7135_Non_Partner_Bank extends TestInit {

    private Bank nonPartnerBank;

    @BeforeClass(alwaysRun = true)
    void setup_7135() throws Exception {
        try{
            nonPartnerBank = new Bank(defaultProvider.ProviderId,
                    "NPB" + DataFactory.getRandomNumberAsString(3),
                    null,
                    null,
                    false,
                    true);
        }catch (Exception e){
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    void TEST_7135_01() {
        ExtentTest t1 = pNode.createNode("TEST_01",
                "When adding Non partner Bank, verify field for adding default routing bank is available");
        ExtentTest t2 = pNode.createNode("TEST_02",
                "Verify that default routing bank has trust bank listed as options");
        ExtentTest t3 = pNode.createNode("TEST_03",
                "Verify that default routing bank Does't has Non trust bank listed as options");
        ExtentTest t4 = pNode.createNode("TEST_04",
                "Verify that default routing bank Does't has Non Partner bank listed as options");
        ExtentTest t5 = pNode.createNode("TEST_05",
                "Verify that default routing bank ia a mandatory field");
        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            AddBank_Page1 page1 = AddBank_Page1.init(t1);
            page1.navAddBank();
            page1.selectProviderName(nonPartnerBank.ProviderName);
            page1.setBankName(nonPartnerBank.BankName);

            if (AppConfig.isBankIdRequired) {
                page1.setBankId(nonPartnerBank.BankID);
            } else {
                t1.info("Bank Id is not required!");
            }

            page1.selectBankType(nonPartnerBank.TrustType);

            // verify that Field to select default routing bank is available- test1
            Utils.captureScreen(t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page1.getSelectDefaultRoutingBank()), true,
                    "Verify Option to select default routing bank is available", t1);

            // verify option list contain trust banks- test2
            Assertion.verifyListContains(page1.getListOfBanksInDefaultRoutingDropdown(),
                    DataFactory.getTrustBankName(nonPartnerBank.ProviderName),
                    "Verify Trust Bank is available as option", t2);

            // test 3, verify Non trust banks must not be available in the dropdown
            Assertion.verifyListNotContains(page1.getListOfBanksInDefaultRoutingDropdown(),
                    DataFactory.getNonTrustBankName(nonPartnerBank.ProviderName),
                    "Verify Non Trust Bank is Not available as option", t3);

            // test 4, verify Non Partner banks must not be available in the dropdown
            Assertion.verifyListNotContains(page1.getListOfBanksInDefaultRoutingDropdown(),
                    DataFactory.getAllLiquidationBanksLinkedToProvider(nonPartnerBank.ProviderName).get(0),
                    "Verify Non Trust Bank is Not available as option", t4);

            page1.clickOnSubmit();
            // test5 verify that deafault routing bank is a mandatory field
            Assertion.verifyErrorMessageContain("default.routiing.bank.not.provided.error",
                    "Verify that Default routing bank is mandatory", t5);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    void TEST_7135_02() {
        ExtentTest t1 = pNode.createNode("TEST_06", "Verify adding Non partner bank with default routing bank");
        ExtentTest t2 = pNode.createNode("TEST_07", "Verify DataBase, Trust bank Id is linked to the newly added Non Partner Bank");
        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            CurrencyProviderMapping.init(t1)
                    .addBank(nonPartnerBank);

            String bankId = MobiquityGUIQueries.getDefaultRoutingBankId(nonPartnerBank.BankName);
            if (bankId != null) {
                Assertion.verifyEqual(bankId, nonPartnerBank.defaultRoutingBankId,
                        "Verify DB table mbk_bank_details is successfully Updated", t2);
            } else {
                t2.fail("DB Verification failed, please check manually");
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(dependsOnMethods = "TEST_7135_02")
    void TEST_7135_03() {
        ExtentTest t1 = pNode.createNode("TEST_08", "Verify deleting a Non partner Bank");
        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_DELETE");

            CurrencyProviderMapping.init(t1)
                    .deleteBank(nonPartnerBank.BankName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
