package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.pageObjects.Enterprise_Management.EntBulkPayApproveReject_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MON_7579_EnterprisePaymentEnhancement extends TestInit {
    private User entUser, entReject, sub;
    private OperatorUser bpaApprover1, bpaApprover2;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            bpaApprover1 = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);
            bpaApprover2 = DataFactory.getBulkPayerWithAccess("SAL_AP2", entUser);
            entReject = DataFactory.getChannelUserWithAccess(Constants.REJECT_BULK_ENT_PAY, 1);

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(new ServiceCharge(Services.ENTERPRISE_PAYMENT,
                            entUser,
                            sub,
                            null,
                            null,
                            null,
                            null)
                    );

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void MON_7579_01() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7579_01",
                "To verify Enterprise/bulk payer admin able to initiate " +
                        "individual records within a batch for Enterprise bulk payment");


        try {

            String remark1 = "initiation remark";
            String remark2 = "approval 1 remark";
            String remark3 = "approval 2 remark";
            BigDecimal txnAmount = new BigDecimal(Constants.MAX_TRANSACTION_ALLOWED).add(new BigDecimal(5));

            Login.init(t1)
                    .login(entUser);

            // use beneficiary as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId,
                    sub.MSISDN,
                    txnAmount.toString(),
                    "",
                    "",
                    ""));

            EnterpriseManagement.init(t1)
                    .addBulkPayee(sub.MSISDN, DataFactory.getRandomNumberAsString(5));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);
            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT,
                            fileName,
                            false,
                            false,
                            remark1);

            if (batchId == null) {
                t1.fail("No batch Id is generated");
                return;
            }


            ExtentTest t0 = pNode.createNode("MON_7579_00",
                    "Login as Approver 1 and verify the initiated batch is available for approval");
            Login.init(t0).login(bpaApprover1); // login as bpa level 1 approver
            EntBulkPayApproveReject_pg1 page = EntBulkPayApproveReject_pg1.init(t0);
            page.NavigateToApp1Link();
            page.clickSortNewest();

            // get all the requests and check for the bacthc Id
            List<WebElement> pendingReqs = driver.findElements(By.cssSelector(".pending-policy-name"));

            boolean isFound = false;
            for (WebElement elem : pendingReqs) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(Constants.THREAD_SLEEP_1000);

                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    t0.pass("Batch Id is available for approval level 1");
                    Utils.captureScreen(t0);

                    ExtentTest t2 = pNode.createNode("MON_7579_02",
                            "Verify that the batch remark field is editable, when approving at Level 1");

                    page = EntBulkPayApproveReject_pg1.init(t2);
                    Assertion.verifyEqual(page.isBatchRemarkFieldAppOneIsEnabled(), true,
                            "Verify that the batch remark field is editable, when approving at Level 1", t2, true);

                    ExtentTest t3 = pNode.createNode("MON_7579_03",
                            "Verify that the batch remark field is the same as provided during the initiation");

                    page = EntBulkPayApproveReject_pg1.init(t3);
                    Assertion.verifyEqual(page.getBatchRemarkApprovalOne(),
                            remark1,
                            "Verify that the batch remark field is the same as provided during the initiation", t3, true);


                    ExtentTest t4 = pNode.createNode("MON_7579_04",
                            "Verify that approver can update the initiation remark");

                    page = EntBulkPayApproveReject_pg1.init(t4);
                    page.setBatchRemarkApprovalOne(remark2);

                    page.clickApprove();
                    break;
                }
            }

            if (isFound) {
                String actual = page.getActionMessage();
                Assertion.verifyMessageContain(actual, "bulkupload.success.approve", "Bulk Payout Approval", t1);
            } else {
                t1.fail("Failed to find the Entry for approval");
                Utils.captureScreen(t1);
                return;
            }


            ExtentTest t5 = pNode.createNode("MON_7579_05",
                    "Login as Approver 2 and verify the initiated batch is available for approval");
            Login.init(t5).login(bpaApprover1); // login as bpa level 1 approver
            page = EntBulkPayApproveReject_pg1.init(t0);
            page.NavigateToApp1Link();
            page.clickSortNewest();

            // get all the requests and check for the bacthc Id
            pendingReqs = driver.findElements(By.cssSelector(".pending-policy-name"));

            isFound = false;
            for (WebElement elem : pendingReqs) {
                elem.findElement(By.xpath("parent::*")).click();
                Thread.sleep(Constants.THREAD_SLEEP_1000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    isFound = true;
                    t5.pass("Batch Id is available for approval level 2");
                    Utils.captureScreen(t0);

                    ExtentTest t6 = pNode.createNode("MON_7579_06",
                            "Verify that the batch remark field is editable, when approving at Level 1");

                    page = EntBulkPayApproveReject_pg1.init(t6);
                    Assertion.verifyEqual(page.isBatchRemarkFieldIsEnabled(), true,
                            "Verify that the batch remark field is editable, when approving at Level 2", t6, true);

                    ExtentTest t7 = pNode.createNode("MON_7579_07",
                            "Verify that the batch remark field is the same as provided during the approval level 1");

                    page = EntBulkPayApproveReject_pg1.init(t7);
                    Assertion.verifyEqual(page.getBatchRemark(),
                            remark2,
                            "Verify that the batch remark field is the same as provided during the approval level 1", t7, true);


                    ExtentTest t8 = pNode.createNode("MON_7579_08",
                            "Verify that approver 2 can update the approval 1 remark");

                    page = EntBulkPayApproveReject_pg1.init(t8);
                    page.setBatchRemark(remark3);

                    page.clickApprove();
                    break;
                }
            }

            if (isFound) {
                String actual = page.getActionMessage();
                Assertion.verifyMessageContain(actual, "bulkupload.success.approve", "Bulk Payout Approval", t5);
            } else {
                t5.fail("Failed to find the Entry for approval");
                Utils.captureScreen(t5);
                return;
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
