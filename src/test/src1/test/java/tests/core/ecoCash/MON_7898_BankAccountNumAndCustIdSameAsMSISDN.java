package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.pageObjects.userManagement.ModifyChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by ravindra.dumpa on 4/2/2019.
 */
public class MON_7898_BankAccountNumAndCustIdSameAsMSISDN extends TestInit {

    private User user;
    private OperatorUser optUsr;
    private String providerName, trustBankId, nonTrustBankId;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {

        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");

            SystemPreferenceManagement.init(setup).updateSystemPreference("CUSTOMER_ID_LENGTH", "10");
            user = new User(Constants.WHOLESALER);

            optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            providerName = DataFactory.getDefaultProvider().ProviderName;

            String trustBankName = DataFactory.getTrustBankName(providerName);
            trustBankId = DataFactory.getBankId(trustBankName);

            String nonTrustBankName = DataFactory.getNonTrustBankName(providerName);
            nonTrustBankId = DataFactory.getBankId(nonTrustBankName);


        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void channelUserCreationWhenBANK_ACC_LINKING_VIA_MSISDNisY() throws Exception {
        ExtentTest test = pNode.createNode("Test_01", "To Verify channel user able to associate Customer id and bank account number as with same msisdn " +
                "across all banks via subscriber/channel registration if system preference: BANK_ACC_LINKING_VIA_MSISDN is Y. ");
        /*
         * Login as Operator user
         * Create Channel user Object
         *
         * Select the service
         * write the data in csv file
         * Upload csv file
         * go to approve page and verify summary filed
         */

        try {
            Login.init(test).login(optUsr);

            ChannelUserManagement.init(test)
                    .initiateChannelUser(user)
                    .assignHierarchy(user)
                    .assignWebGroupRole(user)
                    .mapWalletPreferences(user)
                    .mapBankPreferences(user);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, test);
        }

    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Test_01", "To Verify channel user able to associate Customer id and bank account number as with same msisdn " +
                "across all banks via subscriber/channel registration if system preference: BANK_ACC_LINKING_VIA_MSISDN is Y. ");
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(test).login(optUsr);

            ChannelUserManagement.init(test)
                    .initiateChannelUser(user)
                    .assignHierarchy(user)
                    .assignWebGroupRole(user)
                    .mapWalletPreferences(user);

            Select select = new Select(driver.findElement(By.name("bankCounterList[0].paymentTypeSelected")));
            select.selectByValue(trustBankId);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            select = new Select(driver.findElement(By.name("bankCounterList[0].channelGradeSelected")));
            select.selectByValue(Constants.GOLD_WHOLESALER);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            AddChannelUser_pg5.init(test).addMoreBank();

            select = new Select(driver.findElement(By.name("bankCounterList[1].paymentTypeSelected")));
            select.selectByValue(trustBankId);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            select = new Select(driver.findElement(By.name("bankCounterList[1].channelGradeSelected")));
            select.selectByValue(Constants.GOLD_WHOLESALER);
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            select = new Select(driver.findElement(By.name("bankCounterList[1].primaryAccountSelected")));
            select.selectByIndex(1);

            driver.findElement(By.xpath("//input[@value = 'Confirm']")).click();
            Utils.putThreadSleep(10000);

            Assertion.verifyErrorMessageContain("cannot.have.same.account.number", "duplicate bank acc num", test);
            Assertion.verifyErrorMessageContain("user.error.custid.not.unique", "duplicate cust id", test);

        } catch (Exception e) {

        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode(" ");
        try {

            user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Login.init(test).login(optUsr);
            ChannelUserManagement.init(test)
                    .initiateChannelUserModification(user);

            ModifyChannelUser_pg1.init(test).expiryCheckBox();

            CommonChannelUserPage.init(test)
                    .clickNxtModify()
                    .clickNextUserHeirarcy()
                    .clickNextWebRole().clickNextWalletMap();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);


            int i = 0;
            while (true) {
                try {
                    driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected"));
                } catch (Exception e) {
                    AddChannelUser_pg5.init(test).addMoreBank();

                    Select select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected")));
                    select.selectByValue(defaultBank.BankID);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].channelGradeSelected")));
                    select.selectByValue(Constants.GOLD_WHOLESALER);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].primaryAccountSelected")));
                    select.selectByIndex(1);

                    AddChannelUser_pg5.init(test).addMoreBank();
                    i = i + 1;

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected")));
                    select.selectByValue(nonTrustBankId);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].channelGradeSelected")));
                    select.selectByIndex(1);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].primaryAccountSelected")));
                    select.selectByIndex(1);

                    driver.findElement(By.xpath("//input[@value = 'Confirm']")).click();

                    Assertion.verifyEqual(Assertion.isErrorInPage(test), false, "No Error Message Different Bank With Same Account number", test);
                    break;
                }

                i++;
            }

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
    }

    @Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode(" ");
        try {
            user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Login.init(test).login(optUsr);
            ChannelUserManagement.init(test)
                    .initiateChannelUserModification(user);

            ModifyChannelUser_pg1.init(test).expiryCheckBox();

            CommonChannelUserPage.init(test)
                    .clickNxtModify()
                    .clickNextUserHeirarcy()
                    .clickNextWebRole().clickNextWalletMap();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            int i = 0;
            while (true) {
                try {
                    driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected"));
                } catch (Exception e) {
                    AddChannelUser_pg5.init(test).addMoreBank();

                    Select select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected")));
                    select.selectByValue(trustBankId);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].channelGradeSelected")));
                    select.selectByValue(Constants.GOLD_WHOLESALER);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].primaryAccountSelected")));
                    select.selectByIndex(1);

                    AddChannelUser_pg5.init(test).addMoreBank();
                    i = i + 1;

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].paymentTypeSelected")));
                    select.selectByValue(trustBankId);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].channelGradeSelected")));
                    select.selectByIndex(1);
                    Utils.putThreadSleep(Constants.TWO_SECONDS);

                    select = new Select(driver.findElement(By.name("bankCounterList[" + i + "].primaryAccountSelected")));
                    select.selectByIndex(1);

                    driver.findElement(By.xpath("//input[@value = 'Confirm']")).click();

                    Assertion.verifyErrorMessageContain("cannot.have.same.account.number", "duplicate bank acc num", test);
                    Assertion.verifyErrorMessageContain("user.error.custid.not.unique", "duplicate cust id", test);
                    break;
                }
                i++;
            }
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() {
        ExtentTest tearDown = pNode.createNode(" ");
        try {
            SystemPreferenceManagement.init(tearDown)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        } catch (Exception e) {
            markTestAsFailure(e, tearDown);
        }
        Assertion.finalizeSoftAsserts();
    }
}
