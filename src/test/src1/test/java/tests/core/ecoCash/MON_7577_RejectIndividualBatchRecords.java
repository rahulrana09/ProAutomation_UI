package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.dataEntity.EnterpriseIndividualRecordRejectCSV;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.Enterprise_Management.EntBulkPayApproveReject_pg1;
import framework.pageObjects.Enterprise_Management.EntBulkPayDashBoard_Pg1;
import framework.pageObjects.bulkPayoutTool.BulkPayoutInitiate_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.apache.poi.ss.usermodel.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;
import tests.core.pOne_v4_16.Suite_P1_AuditAdminTrials;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MON_7577_RejectIndividualBatchRecords extends TestInit {

    private User entUser, entReject, sub, sub2;
    private OperatorUser  entApprove1, entApprove2, netAdminInitTxn, netAdminAp1, netAdminAp2, bankAdminAp3;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            entApprove1 = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);
            entApprove2 = DataFactory.getBulkPayerWithAccess("SAL_AP2", entUser);
            entReject = DataFactory.getChannelUserWithAccess(Constants.REJECT_BULK_ENT_PAY, 1);

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            sub2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            netAdminInitTxn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            netAdminAp1 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE", Constants.NETWORK_ADMIN);
            netAdminAp2 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE_2", Constants.NETWORK_ADMIN);
            bankAdminAp3 = DataFactory.getOperatorUserWithAccess("NEW_BULK_APPROVE_2", Constants.NETWORK_ADMIN);

            ServiceCharge tRule = new ServiceCharge(Services.ENTERPRISE_PAYMENT, entUser, sub, null, null, null, null);
            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(tRule);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(enabled = true)
    public void sample() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7577_01",
                "To verify Enterprise/bulk payer admin able to reject individual records within a batch for Enterprise bulk payment");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use beneficiary as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId,
                    sub.MSISDN,
                    txnAmount.toString(),
                    "",
                    "",
                    ""));

            EnterpriseManagement.init(t1)
                    .addBulkPayee(sub.MSISDN, DataFactory.getRandomNumberAsString(5));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);
            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            //download csv, fill, upload, submit
            List<EnterpriseIndividualRecordRejectCSV> rejectRecordList = new ArrayList<>();
            rejectRecordList.add(new EnterpriseIndividualRecordRejectCSV(batchId, "1"));

            String rejectFileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEntRejectIndividualRecordCsv(rejectRecordList);
            String batchRejectBatchId = EnterpriseManagement.init(t1)
                    .rejectIndividualRecordEnterpriseBulkPay(entReject, rejectFileName);


            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);
            page.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> availableEntry = driver.findElements(By.cssSelector(".pending-policy-list"));
            Utils.captureScreen(t1);
            for (WebElement elem : availableEntry) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchRejectBatchId.equalsIgnoreCase(batchIdUI)) {
                    Integer successCount = Integer.parseInt(driver.findElement(By.cssSelector(".primary-color-light_success_count")).getText());
                    Utils.captureScreen(t1);
                    if (successCount > 0)
                        t1.pass("Individual record is successfully rejected by appropriate enterprise user");
                    break;
                } else {
                    t1.fail("individual record not rejected");
                }
            }
            if (availableEntry.size() == 0) {
                t1.warning("No Data available!");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void MON_7577_01() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7577_01",
                "1. To verify Enterprise/bulk payer admin able to reject individual records within a batch for Enterprise bulk payment. " +
                        "2. To verify Enterprise/bulk payer admin able to view records which are pending for approval 1 with rejected records which are rejected by enterprise and bulk payer admin." +
                        "3. To verify Enterprise/bulk payer admin able to view records which are pending for approval 2 with rejected records which are rejected by enterprise and bulk payer admin.To verify Enterprise/bulk payer admin able to view records which are pending for approval 2 with rejected records which are rejected by enterprise and bulk payer admin." +
                        "4.To verify Enterprise/bulk payer admin should not able to reject individual records within a batch for which final approval has already done(Enterprise bulk payment).");
        try {
            Login.init(t1)
                    .login(entUser);

            // use beneficiary as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId,
                    sub.MSISDN,
                    "1",
                    "",
                    "",
                    ""));
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId,
                    sub2.MSISDN,
                    "10",
                    "",
                    "",
                    ""));

            EnterpriseManagement.init(t1)
                    .addBulkPayee(sub.MSISDN, DataFactory.getRandomNumberAsString(5))
                    .addBulkPayee(sub2.MSISDN, DataFactory.getRandomNumberAsString(5));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);
            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            //download csv, fill, upload, submit
            List<EnterpriseIndividualRecordRejectCSV> rejectRecordList = new ArrayList<>();
            rejectRecordList.add(new EnterpriseIndividualRecordRejectCSV(batchId, "1"));

            //reject first record
            String rejectFileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEntRejectIndividualRecordCsv(rejectRecordList);
            String batchRejectBatchId = EnterpriseManagement.init(t1)
                    .rejectIndividualRecordEnterpriseBulkPay(entReject, rejectFileName);

            //verify it got rejected
            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);
            page.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> availableEntry = driver.findElements(By.cssSelector(".pending-policy-list"));
            Utils.captureScreen(t1);
            for (WebElement elem : availableEntry) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchRejectBatchId.equalsIgnoreCase(batchIdUI)) {
                    Integer successCount = Integer.parseInt(driver.findElement(By.cssSelector(".primary-color-light_success_count")).getText());
                    Utils.captureScreen(t1);
                    if (successCount > 0)
                        t1.pass("Individual record is successfully rejected by appropriate enterprise user");
                    break;
                } else {
                    t1.fail("individual record not rejected");
                }
            }
            if (availableEntry.size() == 0) {
                t1.warning("No Data available!");
            }

            EntBulkPayApproveReject_pg1 appPg = new EntBulkPayApproveReject_pg1(pNode);
            //navigate to approval 1 page, verify, perform approval 1
            Login.init(t1).login(entApprove1);
            appPg.NavigateToApp1Link();
            driver.findElement(By.xpath("//span[text()='"+entUser.LoginId+"']")).click();
            driver.findElement(By.xpath("//span[text()='DOWNLOAD']")).click();
            String checkFile = "bulk-upload-" + batchId.split("-")[0];
            FileInputStream input = new FileInputStream(FilePath.dirFileDownloads + checkFile + ".xlsx");
            Workbook wb = WorkbookFactory.create(input);
            String isRejected = wb.getSheetAt(0).getRow(1).getCell(6).getStringCellValue();
            if(isRejected.equalsIgnoreCase("Yes"))
                t1.pass("Contents are as expected");
            else
                t1.fail("Rejected row is not marked as so in approval sheet");
            driver.findElement(By.xpath("//span[text()='APPROVE']")).click();
            //check in dashboard

            //navigate to approval 2 page, verify, and check contents. Approve all
            Login.init(t1).login(entApprove2);
            appPg.NavigateToApp2Link();

            //Try to reject a record in this batch ID again. should fail
            List<EnterpriseIndividualRecordRejectCSV> rejectRecordList2 = new ArrayList<>();
            rejectRecordList2.add(new EnterpriseIndividualRecordRejectCSV(batchId, "2"));
            //reject first record
            String rejectFileName2 = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEntRejectIndividualRecordCsv(rejectRecordList2);
            String batchRejectBatchId2 = EnterpriseManagement.init(t1)
                    .rejectIndividualRecordEnterpriseBulkPay(entReject, rejectFileName2);

            //verify it got rejected
            EntBulkPayDashBoard_Pg1 pg = EntBulkPayDashBoard_Pg1.init(pNode);
            pg.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> availableEntry2 = driver.findElements(By.cssSelector(".pending-policy-list"));
            Utils.captureScreen(t1);
            for (WebElement elem : availableEntry2) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchRejectBatchId2.equalsIgnoreCase(batchIdUI)) {

                    //TODO:- correct the css selector
                    Integer failureCount = Integer.parseInt(driver.findElement(By.cssSelector(".primary-color-light_success_count")).getText());

                    Utils.captureScreen(t1);
                    if (failureCount > 0)
                        t1.pass("Individual record cannot be rejected by enterprise user after it has been approved");
                    break;
                } else {
                    t1.fail("individual record rejected incorrectly");
                }
            }
            if (availableEntry.size() == 0) {
                t1.warning("No Data available!");
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void MON_7577_02() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_7577_02",
                "To verify three level approval for stock liquidation");
        try {

            Login.init(t1)
                    .login(netAdminInitTxn);

            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(pNode);

            page.navigateToLink();
            page.service_SelectValue(Services.STOCK_LIQUIDATION_SERVICE);
            //page.fileUpload_SetText(fileName);
            page.fileUpload(fileName);
            page.remark_SetText("OK");
            page.submitButton_Click();

            if (ConfigInput.isAssert) {
                String msg = MessageReader.getMessage("bulkupload.success.batchNumber", null);
                msg = msg.split("\\{")[0].trim();
                String actual = Assertion.getActionMessage();
                Assertion.verifyContains(actual, msg, "Verify Bulk Success", pNode);
                String bulkID = actual.split("BA")[1].trim().substring(0, 18);
                bulkID = "BA" + bulkID;
                pNode.info("Bulk ID Generated :" + bulkID);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }


}
