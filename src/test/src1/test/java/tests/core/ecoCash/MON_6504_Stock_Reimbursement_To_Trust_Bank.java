package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.stockManagement.Reimbursement_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.JigsawOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableSortedMap.of;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : v5.1 Stock Reimbursement To Trust/Non Trust Bank
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 11/13/2018.
 */
public class MON_6504_Stock_Reimbursement_To_Trust_Bank extends TestInit {

    private OperatorUser optUsr;
    private String refNum;
    private Bank trustBank, nonTrustBank;
    private ServiceCharge sChargeOPTWithDraw, optWithdraw2;
    private User chUsr;
    private String operatorId;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            refNum = DataFactory.getRandomNumberAsString(5);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            operatorId = MobiquityGUIQueries.getOperatorId();

            sChargeOPTWithDraw = new ServiceCharge(Services.OPERATOR_WITHDRAW, optUsr, optUsr,
                    null, null, null, null);

            sChargeOPTWithDraw.setPayeePaymentType(Constants.PAYINST_BANK_CONST, nonTrustBank.BankID);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(sChargeOPTWithDraw);

            optWithdraw2 = new ServiceCharge(Services.OPERATOR_WITHDRAW, chUsr, optUsr,
                    null, null, null, null);

            optWithdraw2.ServiceInfo.PayeePaymentType = Constants.PAYINST_BANK_CONST;
            optWithdraw2.Payee.PaymentType = trustBank.BankName;

            TransferRuleManagement.init(setup)
                    .configureTransferRule(optWithdraw2);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    //EIG Must Be Configured

    // @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("MON_6504_01", "To verify that Stock Reimbursement should be " +
                "successful if sender is Operator(IND03) and receiver is non trust bank");
        try {
            Login.init(test).login(optUsr);

            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND03");

            BigDecimal preBankBal = MobiquityGUIQueries.getBalanceBankWallet(nonTrustBank.BankName);

            String txnId = StockManagement.init(test)
                    .stockReimbursementInitiation(optUsr, defaultProvider.ProviderId, refNum,
                            Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null, true, nonTrustBank.BankID);

            StockManagement.init(test)
                    .approveReimbursement(txnId);

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND03");
            BigDecimal postBankBal = MobiquityGUIQueries.getBalanceBankWallet(nonTrustBank.BankName);

            BigDecimal amount = preOptBal.subtract(postOptBal);
            Assertion.verifyAccountIsDebited(preOptBal, postOptBal, amount, "Amount Debited From User", test);

            amount = postBankBal.subtract(preBankBal);
            Assertion.verifyAccountIsDebited(postBankBal, preBankBal, amount, "Amount Credited To Operator", test);


        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    //@Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("MON_6504_02", "To verify that Stock Reimbursement should be " +
                "successful if sender is channel user and receiver is trust bank");
        try {
            Login.init(test).login(optUsr);

            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND03");

            BigDecimal preBankBal = MobiquityGUIQueries.getBalanceBankWallet(trustBank.BankName);

            String txnId = StockManagement.init(test)
                    .stockReimbursementInitiation(chUsr, defaultProvider.ProviderId, refNum,
                            "5", Constants.REMARKS, null, true, trustBank.BankID);

            StockManagement.init(test)
                    .approveReimbursement(txnId);

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND03");
            BigDecimal postBankBal = MobiquityGUIQueries.getBalanceBankWallet(trustBank.BankName);

            BigDecimal amount = preOptBal.subtract(postOptBal);
            Assertion.verifyAccountIsDebited(preOptBal, postOptBal, amount, "Amount Debited From User", test);

            amount = postBankBal.subtract(preBankBal);
            Assertion.verifyAccountIsDebited(postBankBal, preBankBal, amount, "Amount Credited To Operator", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    // @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("MON_6504_03", "To verify that in UI 'credit payment instrument' dropdown should be visible with value bank and wallet and \n" +
                "user should be able to perform transaction by selecting bank option");
        try {
            Login.init(test).login(optUsr);
            Reimbursement_page1 page = new Reimbursement_page1(test);
            page.navigateToLink().selectOptInstTypeBank();

            boolean walletCheckBox = Utils.checkElementPresent("optInstTypeId1", Constants.FIND_ELEMENT_BY_ID);
            boolean bankCheckbox = Utils.checkElementPresent("optInstTypeId2", Constants.FIND_ELEMENT_BY_ID);
            boolean bankList = Utils.checkElementPresent("availableBanksId", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(walletCheckBox, true, "Option To Select Wallet Available", test);
            Assertion.verifyEqual(bankCheckbox, true, "Options To Select Bank Available", test);
            Assertion.verifyEqual(bankList, true, "Option To Choose Bank Available", test, true);

            test = pNode.createNode("MON_6504_04", "To verify that Stock Reimbursement should be " +
                    "successful if sender is recharge Operator and receiver is non trust bank");

            if (operatorId == null) {
                RechargeOperator recharge = new RechargeOperator(defaultProvider.ProviderId);
                OperatorUserManagement.init(test).addRechargeOperator(recharge);
            }

            String txnId = StockManagement.init(test)
                    .stockReimbursementInitiation(optUsr, defaultProvider.ProviderId, refNum,
                            Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null, true, nonTrustBank.BankID);

            StockManagement.init(test)
                    .approveReimbursement(txnId);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    //@Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode(" ");
        try {

            OperatorUser optUsr = new OperatorUser(Constants.NETWORK_ADMIN);
            //creating an operator user with odd mobile num
            optUsr.setMSISDN("77" + DataFactory.getRandomNumberAsString(7) + "" + "5");
            OperatorUserManagement.init(test)
                    .initiateOperatorUser(optUsr)
                    .approveOperatorUser(optUsr);

            String txnId = StockManagement.init(test)
                    .stockReimbursementInitiation(optUsr, defaultProvider.ProviderId, refNum,
                            Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null, true, nonTrustBank.BankID);

            StockManagement.init(test)
                    .approveReimbursement(txnId);

            startNegativeTest();


        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
    }

    @Test(priority = 5)
    public void Test_05() {
        ExtentTest test = pNode.createNode("Test_05", "To verify that Stock Reimbursement should be fail in case of response timeout");
        try {
            JigsawOperations.setJigsawProperties(of("syncapi.result.timeOutInMillis", "1000"));

            String txnId = StockManagement.init(test)
                    .stockReimbursementInitiation(optUsr, defaultProvider.ProviderId, refNum,
                            Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS, null, true, nonTrustBank.BankID);

            startNegativeTest();
            StockManagement.init(test)
                    .approveReimbursement(txnId);

            Assertion.verifyErrorMessageContain("response.time.out", "EIG Response Timeout", pNode);


        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            JigsawOperations.setJigsawProperties(of("syncapi.result.timeOutInMillis", "25000"));
        }


//        setJigsawProperties(of("syncapi.result.timeOutInMillis", "1000"));

    }
}
