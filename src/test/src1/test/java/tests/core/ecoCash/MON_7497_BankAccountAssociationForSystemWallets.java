package tests.core.ecoCash;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0 Build 11
 * Objective        : v5.1 System Case: Bank Account Association/Disassociation For System Wallets
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 09/01/2019
 */
public class MON_7497_BankAccountAssociationForSystemWallets extends TestInit {

    private String provider, user, trustBankName, nonTrustBankName, trustBankId, nonTrustBankId, userWallet;

    @BeforeClass(alwaysRun = true)
    public void preConditions() {
        ExtentTest test = pNode.createNode("Setup", "Initiating Test");
        try {

            provider = DataFactory.getDefaultProvider().ProviderName;
            user = Constants.OPERATOR_REIMB;

            trustBankName = DataFactory.getTrustBankName(provider);
            trustBankId = DataFactory.getBankId(trustBankName);

            nonTrustBankName = DataFactory.getNonTrustBankName(provider);
            nonTrustBankId = DataFactory.getBankId(nonTrustBankName);

            userWallet = DataFactory.getAllWallet().get(0).WalletName;

            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest test = pNode.createNode("Test_01", "Verify that a valid user should be able to " +
                "associate operator's system wallets (IND01, IND02, IND03, INDTAX01, INDTAX02, INDTAX03) with bank (trust/non trust) account");
        try {
            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "IND01", trustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "IND02", trustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "IND03", trustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "INDTAX01", nonTrustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "INDTAX02", nonTrustBankName);

            BankAccountAssociation.init(test)
                    .associateBankAccountsForSystemWallet(provider, user, "INDTAX03", nonTrustBankName);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_02() throws Throwable {
        ExtentTest test = pNode.createNode("Test_02", "Verify that a valid user should be able to " +
                "disassociate operator's system wallets (IND01, IND02, IND03, INDTAX01, INDTAX02, INDTAX03) with bank (trust/non trust) account");
        try {

            String bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("IND01", trustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "IND01", bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("IND02", trustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "IND02", bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("IND03", trustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "IND03", bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("INDTAX01", nonTrustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "INDTAX01", bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("INDTAX02", nonTrustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "INDTAX02", bankAccNum);

            bankAccNum = MobiquityGUIQueries.getOperatorBankAccountNum("INDTAX03", nonTrustBankId);
            BankAccountAssociation.init(test)
                    .disassociateBankAccountsForSystemWallet(provider, user, "INDTAX03", bankAccNum);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Test_03", "verify that a valid user should not be able to " +
                "associate any wallet other than system wallet with banks for operator user");
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("BANK_ACC_REG");
            Login.init(test).login(optUsr);

            BankAccountRegistration_Pg1.init(test)
                    .navigateToLink().selectProvider(provider)
                    .selectUser(user).setWalletId(userWallet)
                    .clickSubmitBtnPrefN();

            Assertion.verifyErrorMessageContain("invalid.wallet.type", "Invalid Wallet", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass
    public void postCondition() throws Exception {
        ExtentTest test = pNode.createNode("TearDown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(test)
                    .updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
