package tests.core.base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.Bank;
import framework.entity.CITReport;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MfsTestProperties;
import framework.util.reportManager.ExtentManager;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import static framework.features.apiManagement.Transactions.getReconciliationBalance;


/**
 * Created by rahul.rana on 5/6/2017.
 */
public class TestInit {
    private static final Logger logToConsole = LoggerFactory.getLogger(TestInit.class);
    public static int testcasecount = 000;
    public static String testcase = "Dynamic_Sys_TC_";
    public static ExtentTest cuNode;
    protected static String suiteName = null, fileName = null;
    protected static WebDriver driver;
    private static ExtentReports extent; // test Node
    private static ThreadLocal parentTest = new ThreadLocal();
    protected ExtentTest pNode; // parent Node
    protected FunctionLibrary fl;
    private ExtentTest t1;
    private float preReconciliationBalance, postReconciliationBalance;
    protected Wallet defaultWallet, savingClubWallet;
    protected CurrencyProvider defaultProvider;
    protected Bank trustBank, nonTrustBank;
    protected Bank defaultBank;

    /**
     * used to create dynamic test case id's when same test method
     * runs in loop for different users in the sheet
     *
     * @return
     */
    public static String getTestcaseid() {

        testcasecount++;
        return ":" + testcase + testcasecount + ":";
    }

    protected static void startNegativeTest() {
        ConfigInput.isAssert = false;
    }

    protected static void startNegativeTestWithoutConfirm() {
        ConfigInput.isAssert = false;
        ConfigInput.isConfirm = false;
    }

    protected static void stopNegativeTest() {
        ConfigInput.isAssert = true;
    }

    protected static void stopNegativeTestWithoutConfirm() {
        ConfigInput.isAssert = true;
        ConfigInput.isConfirm = true;
    }

    protected static String getTransactionAmount(ServiceCharge sCharge) throws Exception {
        BigDecimal minServiceChargeAmount = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        if (!(minServiceChargeAmount == null))
            minServiceChargeAmount = minServiceChargeAmount.add(BigDecimal.valueOf(1));
        if (!sCharge.Payer.CategoryCode.equalsIgnoreCase(Constants.OPERATOR)) {
            BigDecimal minTcpAmount = MobiquityGUIQueries.fetchMinTCPAmount(sCharge.Payer.CategoryCode, sCharge.Payer.GradeCode);
            minTcpAmount = minTcpAmount.add(BigDecimal.valueOf(1));
            if (minServiceChargeAmount.compareTo(minTcpAmount) == 1) {
                return minServiceChargeAmount.toString();
            } else
                return minTcpAmount.toString();
        } else
            return minServiceChargeAmount.toString();

    }

    /**
     * Before Suite - Load Environment Variables and Base Set
     *
     * @param ctx
     * @throws Exception
     */
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(ITestContext ctx) throws Exception {
        /**
         * if the suite is executed from XML then create extent file with Suite Name mentioned in the xml,
         * else
         * use the Class name.
         */
        if (ExtentManager.getFileName() == null) {
            suiteName = (ctx.getSuite().getName().
                    equalsIgnoreCase("Default Suite")) ? getClass().getSimpleName() : ctx.getSuite().getName();
            String timeStamp = new SimpleDateFormat("dd-MM-yyyy_HH-mm").format(new Date());
            ExtentManager.setFileName(suiteName + "_" + timeStamp + ".html");
        }

        // Suite Init
        suiteInit();

        //Verify Critical Preference Values Must Set To Default Values
        SystemPreferenceManagement.init(pNode)
                .makeSureAppPreferencesAreIntact();
    }

    public void suiteInit() throws Exception {
        extent = ExtentManager.getInstance();
        new MobiquityGUIQueries();
        new MobiquityDBAssertionQueries();

        pNode = extent.createTest("Load Environment Variables and Base Set");
        ExtentTest t1 = pNode.createNode("Load Environment Variables and Base Set");

        ConfigInput.init();
        t1.info("load Config Inputs, Environment Variables, mfstest.properties");

        AppConfig.init();
        t1.info("load Application Configuration from DB - MTX_SYSTEM_PREFERENCES");

        DataFactory.loadDomainCategoryDBMap();
        t1.info("load Domain Category DB Map");

        DataFactory.loadRNRSheet(t1);
        t1.info("Load ConfigInput.xlsx(roles and responsibility)");

        DataFactory.loadSuperAdminRoles(t1);
        t1.info("Fetch Superadmins roles and responsibility)");

        DataFactory.loadGradesFromConfig();
        t1.info("load Grade Category Config Map");

        DataFactory.loadCurrencyProvider();
        t1.info("load currency provider details");

        if (DataFactory.getAllProviderNames().size() > 1) {
            ConfigInput.isSecondryProvider = true;
        }

        DataFactory.loadWallet();
        t1.info("load Wallet related information");

        DataFactory.loadServiceList();
        t1.info("load Service charge related information, ExecutionConstants.xlsx");

        DataFactory.loadExecutionConstants();
        t1.info("load ExecutionConstants.xlsx");

        DataFactory.loadMobileGroupRole();
        t1.info("load Mobile Group role from App Data");

        DataFactory.loadInstrumentTCP();
        t1.info("load Instrument TCPs from App Data");

        DataFactory.loadTransferRuleInput();
        t1.info("load transfer rule inputs");

        DataFactory.loadSystemMsisdn();
        t1.info("load unique MSISDN from DB");

        DataFactory.loadAutomationGeography();
        t1.info("load Automation Geography");

        DataFactory.setDefaultProviderWalletAndBank();

        if (ConfigInput.isCoreRelease) {
            DataFactory.loadPartnerDetails();
        }

        DataFactory.loadConfigPreference();

        extent.setSystemInfo("URL", ConfigInput.URL);
        extent.setSystemInfo("Browser", MfsTestProperties.getInstance().getProperty("browser.name"));
        extent.setSystemInfo("Number of Category", "" + GlobalData.rnrDetails.size());
        extent.setSystemInfo("Number of Grades", "" + GlobalData.gradeList.size());
        extent.setSystemInfo("Number of Providers", "" + GlobalData.currencyProvider.size());
        extent.setSystemInfo("Number of Wallets", "" + GlobalData.wallet.size());
        extent.setSystemInfo("Number of Banks", "" + DataFactory.getAllBank().size());

        // Create Required Directories

        Utils.createDirectoryIfNotPresent(FilePath.dirFileUploads);

        Utils.createDirectoryIfNotPresent(FilePath.dirFailSuitesPath);

        //Daily Execution Status Log
        Utils.createDirectoryIfNotPresent(FilePath.dirDailyExecStatusLog);

        Utils.createDirectoryIfNotPresent(FilePath.dirFailTestRecord);

        extent.flush();
        driver = DriverFactory.getDriver();
        fl = new FunctionLibrary(driver);
    }

    @BeforeClass(alwaysRun = true)
    public void beforeClassRun() throws Exception {
        try {
            pNode = extent.createTest(getClass().getSimpleName());
            parentTest.set(pNode);

            Markup m = MarkupHelper.createLabel("Verify Pre Post Recon!", ExtentColor.TEAL);
            pNode.info(m); // Method Start Marker

            AppConfig.init();
            logToConsole.info("load Application Configuration from DB - MTX_SYSTEM_PREFERENCES");
            logToConsole.info("Started Test Suite: " + getClass().getSimpleName());

            // Initialize driver
            driver = DriverFactory.getDriver();

            // get the EA Stock, if it's less than the EA_STOCK_THRESHOLD, perform EA Stock
            BigDecimal currentIND03Stock = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_103);
            if (currentIND03Stock.compareTo(Constants.MIN_EA_STOCK_AMT) < 0) {
                OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("O2C_INIT", Constants.NETWORK_ADMIN).get(0);
                Login.init(pNode)
                        .login(netAdmin);
                StockManagement.init(pNode)
                        .initiateApproveEAStock(DataFactory.getRandomNumberAsString(5),
                                Constants.MAX_EA_TRANSACTION_AMT, "Base Setup NW Stock",
                                DataFactory.getDefaultProvider().ProviderName);
            }
            Assertion.resetSoftAsserts();
            defaultProvider = GlobalData.defaultProvider;
            defaultWallet = GlobalData.defaultWallet;
            savingClubWallet = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB);
            defaultBank = DataFactory.getDefaultBankForDefaultProvider();
            trustBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            nonTrustBank = DataFactory.getAllNonTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethodRun(Method method) {
        logToConsole.info("---\\");
        logToConsole.info("STARTED TEST >---> " + method.getName());

        //To handle UnExpected alert
        AlertHandle.handleUnExpectedAlert(pNode);
        Assertion.resetSoftAsserts();
        Login.resetLoginStatus();
        preReconciliationBalance = getReconciliationBalance();
    }


    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result, Method method) throws IOException {
        String dependentOnMethod = method.getDeclaredAnnotations()[0].toString().split("dependsOnMethods=")[1].split(",")[0].replace("[", "").replace("]", "");
        if (result.getStatus() == ITestResult.SUCCESS) {
            logToConsole.info("TEST         >---> " + result.getName() + " - Executed Successfully");
        } else if (result.getStatus() == ITestResult.FAILURE) {
            logToConsole.info("TEST         >---> " + result.getName() + " - Execution Failed");

            // update the Execution log files
            updateFailExecutionLog(result.getInstanceName(), result.getName(), dependentOnMethod);

        } else if (result.getStatus() == ITestResult.SKIP) {
            logToConsole.info("TEST         >---> " + result.getName() + " - Execution Skipped");
            ExtentTest test = pNode.createNode(result.getName() + ": Test Case Execution Skipped!");
            test.skip("Test Case Skipped");

            // update the Execution log files
            updateFailExecutionLog(result.getInstanceName(), result.getName(), dependentOnMethod);
        } else {
            ExtentTest test = pNode.createNode(result.getName() + ": Add Test Case Execution Type in TestInit class");
            test.skip("Test Case type");

        }

        logToConsole.info("END TEST     >---> " + result.getName());
        logToConsole.info("---/");
        extent.flush();
        stopNegativeTest();
        stopNegativeTestWithoutConfirm();
        ConfigInput.shouldClickNext = true;
        ConfigInput.confirmReq = true;
        ConfigInput.changePin = true;
        Assertion.resetSoftAsserts();
        Login.resetLoginStatus();
        stopNegativeTestWithoutConfirm();

        postReconciliationBalance = getReconciliationBalance();

        if (preReconciliationBalance != postReconciliationBalance) {
            pNode.fail("Pre Recon Bal: " + preReconciliationBalance + " & Post Recon Bal: " + postReconciliationBalance + "Mismatch Found");
        }
    }

    @AfterClass(alwaysRun = true)
    public void afterClassRun() {
        extent.flush();
        DriverFactory.quitDriver();
        Login.resetLoginStatus();
        Assertion.resetSoftAsserts();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuiteRun() throws Exception {
        deInitSuite();
    }

    public void deInitSuite() throws IOException {
        DriverFactory.quitDriver();
        if (MfsTestProperties.getInstance().getProperty("report.generate.cit").equalsIgnoreCase("Yes")) {
            CITReport CIT = new CITReport(FilePath.dirReports + fileName);
            CITReport.generateCTReport();
        }
        // Generate Simple Excel Report
        extent.flush();
        OracleDB.CloseConnection();
        OracleDB.CloseConnection_infra();
        //SimpleReport.writeSimpleReport(ExtentManager.getFileName(), suiteName);
    }

    protected void markSetupAsFailure(Exception e) {
        e.printStackTrace();
        pNode.fail("Setup Failure due to Exception for: " + getClass().getSimpleName() + ", refer console For additional details");
        pNode.info("Exception Message: " + e.getMessage());
        pNode.error(e);
        Assert.fail("Setup Failure Due to Exception: " + e.toString());
    }

    protected void markTeardownAsFailure(Exception e) {
        e.printStackTrace();
        pNode.fail("Teardown Failure due to Exception for: " + getClass().getSimpleName() + ", refer console For additional details");
        pNode.info("Exception Message: " + e.getMessage());
        pNode.error(e);
        Assert.fail("Tear down Failure Due to Exception: " + e.toString());
    }

    protected void markTestAsFailure(Exception e, ExtentTest t1) {
        e.printStackTrace();
        t1.fail("Test Failure due to Exception");
        t1.error(e);
        Assert.fail("Test Failure Due to Exception: " + e.toString());
    }

    protected void markTestAsFailure(String message, ExtentTest t1) {
        t1.fail(message);
        Assert.fail("Test failed stopping execution!");
    }

    /**
     * Update Log Files with current test status
     *
     * @param suiteName
     * @param methodName
     * @throws IOException
     */
    private void updateFailExecutionLog(String suiteName, String methodName, String dependentOnMethod) throws IOException {
        /**
         * Update daily log as well as the latest log
         * add timestamp to all entry
         */
        String tDate = java.time.LocalDate.now().toString();
        String tStamp = java.time.LocalTime.now().toString();

        File logFile = new File(FilePath.dirDailyExecStatusLog + tDate + ".csv");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File logFile1 = new File(FilePath.fileCurrentExecutionLog);
        if (!logFile1.exists()) {
            try {
                logFile1.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BufferedWriter out = new BufferedWriter(new FileWriter(logFile, true));
        out.newLine();
        out.write(tStamp + "," +
                suiteName + "," +
                methodName + "," +
                dependentOnMethod);
        out.close();

        BufferedWriter out1 = new BufferedWriter(new FileWriter(FilePath.fileCurrentExecutionLog, true));
        out1.newLine();
        out1.write(tStamp + "," +
                suiteName + "," +
                methodName + "," +
                dependentOnMethod);
        out1.close();


    }

    protected String toCamelCase(final String init) {
        if (init == null)
            return null;

        final StringBuilder ret = new StringBuilder(init.length());

        for (final String word : init.split(" ")) {
            if (!word.isEmpty()) {
                ret.append(Character.toUpperCase(word.charAt(0)));
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length()))
                ret.append(" ");
        }

        return ret.toString().replace(" ", "");
    }

}
