package tests.core.base;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;

/**
 * Created by rahul.rana  on 3/8/2019.
 */
public class CreateUserForManualTest extends TestInit {

    @Test
    public void rechargeOpt() throws Exception {
        ExtentTest t1 = pNode.createNode("BASE_ADD_RECHARGE_OPT", "Add Recharge Operator");

        try {
            RechargeOperator newOperator = OperatorUserManagement.init(t1).getRechargeOperator("101");
            newOperator.writeDataToExcel();
            //  RechargeOperator newOperator1 = OperatorUserManagement.init(t1).getRechargeOperator("102");
            //newOperator1.writeDataToExcel();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test
    public void biller() throws Exception {
        ExtentTest t1 = pNode.createNode("BASE_BILLER_01", "Add Biller " + Constants.BILL_PROCESS_TYPE_OFFLINE + "_" + Constants.BILL_SERVICE_LEVEL_PREMIUM);

        try {

           /* Biller bl0 = BillerManagement.init(t1).getBillerFromAppData(
                    BillerAttribute.PROCESS_TYPE_ONLINE,
                    BillerAttribute.SERVICE_LEVEL_ADHOC
            );*/

            Biller bl1 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName,
                    Constants.BILLER_CATEGORY,
                    BillerAttribute.PROCESS_TYPE_ONLINE,
                    BillerAttribute.SERVICE_LEVEL_ADHOC,
                    BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                    BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE,
                    BillerAttribute.SUB_TYPE_NOT_APPLICABLE
            );

            Biller bl2 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_ADHOC, BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                    BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE, BillerAttribute.SUB_TYPE_NOT_APPLICABLE
            );

            Biller bl3 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE
            );

            Biller bl4 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT
            );

            Biller bl5 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT
            );

            Biller bl6 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE
            );

            Biller bl7 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT
            );

            Biller bl8 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT
            );

            Biller bl9 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE
            );

            Biller bl10 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT
            );

            Biller bl11 = BillerManagement.init(t1).getBillerFromAppData(
                    defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                    BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                    BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT
            );




            /*  if (biller1.isCreated.equalsIgnoreCase("y")) {
                biller1.writeDataToExcel();
            }
            ExtentTest t2 = pNode.createNode("BASE_BILLER_01",
                    "Add Biller " + Constants.BILL_PROCESS_TYPE_ONLINE + "_" + Constants.BILL_SERVICE_LEVEL_PREMIUM);
            Biller biller2 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            BillerManagement.init(t2).createBiller(biller2);
            if (biller2.isCreated.equalsIgnoreCase("y")) {
                biller2.writeDataToExcel();
            }

            ExtentTest t3 = pNode.createNode("BASE_BILLER_01",
                    "Add Biller " + Constants.BILL_PROCESS_TYPE_OFFLINE + "_" + Constants.BILL_SERVICE_LEVEL_BOTH);
            Biller biller3 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_BOTH);

            BillerManagement.init(t3).createBiller(biller3);
            if (biller3.isCreated.equalsIgnoreCase("y")) {
                biller3.writeDataToExcel();
            }

            ExtentTest t4 = pNode.createNode("BASE_BILLER_01",
                    "Add Biller " + Constants.BILL_PROCESS_TYPE_ONLINE + "_" + Constants.BILL_SERVICE_LEVEL_BOTH);
            Biller biller4 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_BOTH);

            BillerManagement.init(t4).createBiller(biller4);
            if (biller4.isCreated.equalsIgnoreCase("y")) {
                biller4.writeDataToExcel();
            }

            ExtentTest t5 = pNode.createNode("BASE_BILLER_01",
                    "Add Biller " + Constants.BILL_PROCESS_TYPE_OFFLINE + "_" + Constants.BILL_SERVICE_LEVEL_BOTH);
            Biller biller5 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILL_SERVICE_LEVEL_ADHOC,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            BillerManagement.init(t5).createBiller(biller5);
            if (biller5.isCreated.equalsIgnoreCase("y")) {
                biller5.writeDataToExcel();
            }

            ExtentTest t6 = pNode.createNode("BASE_BILLER_01",
                    "Add Biller " + Constants.BILL_PROCESS_TYPE_ONLINE + "_" + Constants.BILL_SERVICE_LEVEL_ADHOC);
            Biller biller6 = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            BillerManagement.init(t6).createBiller(biller6);
            if (biller6.isCreated.equalsIgnoreCase("y")) {
                biller6.writeDataToExcel();
            }*/
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test
    public void pseudoUser() throws Exception {
        ExtentTest t1 = pNode.createNode("pseudoUser", "Add PseudoUser");

        try {
            User whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            User whs2 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            User whs3 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 2);
            User ssa1 = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT, 0);
            User ssa2 = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT, 1);
            User rt1 = DataFactory.getChannelUserWithCategory(Constants.RETAILER, 0);
            User rt2 = DataFactory.getChannelUserWithCategory(Constants.RETAILER, 1);

            PseudoUser ps1 = new PseudoUser();
            ps1.setParentUser(whs1);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps1);
            if (ps1.isCreated)
                ps1.writeDataToExcel();

            PseudoUser ps2 = new PseudoUser();
            ps2.setParentUser(whs2);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps2);
            if (ps2.isCreated)
                ps2.writeDataToExcel();


            PseudoUser ps3 = new PseudoUser();
            ps3.setParentUser(whs3);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps3);
            if (ps3.isCreated)
                ps3.writeDataToExcel();

            PseudoUser ps4 = new PseudoUser();
            ps4.setParentUser(ssa1);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps4);
            if (ps4.isCreated)
                ps4.writeDataToExcel();

            PseudoUser ps5 = new PseudoUser();
            ps5.setParentUser(ssa2);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps5);
            if (ps5.isCreated)
                ps5.writeDataToExcel();

            PseudoUser ps6 = new PseudoUser();
            ps6.setParentUser(rt1);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps6);
            if (ps6.isCreated)
                ps6.writeDataToExcel();

            PseudoUser ps7 = new PseudoUser();
            ps7.setParentUser(rt2);
            PseudoUserManagement.init(t1).createCompletePseudoUser(ps7);
            if (ps7.isCreated)
                ps7.writeDataToExcel();


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test
    public void employeeUsers() throws Exception {
        ExtentTest t1 = pNode.createNode("pseudoUser", "Add PseudoUser");

        try {
            User whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            User ssa1 = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT, 1);
            User rt1 = DataFactory.getChannelUserWithCategory(Constants.RETAILER, 1);
            int counter = 1;
            for (int i = 0; i < 3; i++) {
                Employee emp = new Employee(whs1.MSISDN, whs1.CategoryCode);
                emp.ID = counter + "";
                Transactions.init(t1).createEmployee(emp);
                if (emp.isCreated)
                    emp.writeDataToExcel();
                counter++;
                Employee emp1 = new Employee(ssa1.MSISDN, ssa1.CategoryCode);
                emp1.ID = counter + "";
                Transactions.init(t1).createEmployee(emp1);
                if (emp1.isCreated)
                    emp1.writeDataToExcel();
                counter++;
                Employee emp2 = new Employee(rt1.MSISDN, rt1.CategoryCode);
                emp2.ID = counter + "";
                Transactions.init(t1).createEmployee(emp2);
                if (emp2.isCreated)
                    emp2.writeDataToExcel();
                counter++;
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
