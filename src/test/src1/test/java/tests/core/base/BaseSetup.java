package tests.core.base;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.GradeDB;
import framework.entity.*;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.*;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.ChangePasswordPage;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Automation Team on 5/8/2017.
 */
public class BaseSetup extends BaseSetupInit {

    @Test(priority = 1)
    public void checkIfSuperAdminPasswordIsSet() throws Exception {
        ExtentTest eSuite = extent.createTest("SET_SUADM_PSWD",
                "Check if Superadmin's First Time password is updated");

        try {
            if (properties.getProperty("run.baseset.setAdminPwd").equalsIgnoreCase("yes")) {
                for (SuperAdmin admin : GlobalData.superAdmins) {
                    Login.init(eSuite).tryLogin(admin.LoginId, ConfigInput.superadminSchemaPass);
                    if (ChangePasswordPage.init(eSuite).isChangePasswordPageVisible()) {
                        //Set Super Admin First Time password
                        eSuite.info("As Change Password page is shown, change " + admin.LoginId + "'s First Time Password");
                        CommonUserManagement.baseInit(eSuite)
                                .changeAdminFirstTimePassword(admin.LoginId);
                    } else {
                        eSuite.info("First Time Password Page is not shown for " + admin.LoginId + ". Login with Environment password ConfigInput.superadminPass");
                        Login.init(eSuite)
                                .login(admin.LoginId, ConfigInput.superadminPass);
                    }
                }
            } else {
                eSuite.skip("Skip Resetting Admin Password");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * CREATE BANKS
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void createBank() throws Exception {
        ExtentTest eSuite = extent.createTest("createBank",
                "Create Banks for the MFS provider by taking input from CurrencyProvider.csv");

        try {
            if (properties.getProperty("run.baseset.addbank").equalsIgnoreCase("yes")) {
                for (Bank bank : GlobalData.banks) {

                    // extent Test
                    ExtentTest eTest = eSuite.createNode("CREATE_BANK_" + bank.BankName,
                            "Create banks " + bank.BankName + " for " + bank.ProviderName + " by taking input from CurrencyProvider.csv");

                    if (bank.getExisting()) {
                        eTest.pass("Bank -" + bank.BankName + " already exist with bank id:" + bank.BankID);
                        continue;
                    }

                    // login as superadmin with Add bank Rights
                    Login.init(eTest)
                            .loginAsSuperAdmin("BANK_ADD");

                    // Add Bank
                    CurrencyProviderMapping.init(eTest)
                            .addBank(bank);

                    DataFactory.loadCurrencyProvider();
                }
            } else {
                eSuite.skip("Skip createBank");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * ASSOCIATE BANKS WITH MFS PROVIDER
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void associateBanksWithMFSProvider() throws Exception {
        ExtentTest eSuite = extent.createTest("associateBanksWithMFSProvider",
                "Associate the banks with the MFS provider");

        try {
            if (properties.getProperty("run.baseset.associatebank").equalsIgnoreCase("yes")) {
                for (String provider : DataFactory.getAllProviderNames()) {
                    ExtentTest eTest = eSuite.createNode("ASSOCIATE_BANKS_TO_" + provider,
                            "Associate the banks with the MFS provider" + provider);

                    // login as Super admin with associate Bank Role
                    Login.init(eTest)
                            .loginAsSuperAdmin("MFSBTM01");

                    CurrencyProviderMapping.init(eTest).
                            initiateBanksCurrencyProviderMapping(provider);
                }
            } else {
                eSuite.skip("Skip associateBanksWithMFSProvider");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * MODIFY ASSOCIATION OF BANKS WITH MFS PROVIDER
     */
    @Test(priority = 4)
    public void modifyBankAssociationWithMFSProvider() throws Exception {
        ExtentTest eSuite = extent.createTest("modifyBankAssociationWithMFSProvider",
                "modify services associated with banks with the MFS provider");

        try {
            if (properties.getProperty("run.baseset.associatebank").equalsIgnoreCase("yes")) {
                for (String provider : DataFactory.getAllProviderNames()) {
                    ExtentTest eTest = eSuite.createNode("MODIFY_BANK_SERVICES_FOR_" + provider,
                            "modify services associated with banks with the MFS provider: " + provider);

                    // Login Superadmin with Modify Bank Role
                    Login.init(eTest)
                            .loginAsSuperAdmin("MFSBMD");

                    CurrencyProviderMapping.init(eTest).
                            modifyBankCurrencyProviderMapping(provider);
                }

                // login with super admin with Add Service Provide Bank Account Role
                if (ConfigInput.isCoreRelease) {
                    for (CurrencyProvider provider : GlobalData.currencyProvider) {
                        List<String> bankList = DataFactory.getAllBankNamesLinkedToProvider(provider.ProviderName);
                        for (String bankName : bankList) {
                            Login.resetLoginStatus();
                            Login.init(eSuite)
                                    .loginAsSuperAdmin("OPT_BANK_ACC_ADD");

                            CurrencyProviderMapping.init(eSuite)
                                    .addServiceProviderBankAccount(bankName, DataFactory.getRandomNumberAsString(9));
                        }
                    }
                }
            } else {
                eSuite.skip("Skip modifyBankAssociationWithMFSProvider");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * ASSOCIATE WALLETS WITH MFS PROVIDER
     */
    @Test(priority = 5)
    public void associateWalletsWithMFSProvider() throws Exception {
        ExtentTest eSuite = extent.createTest("associateWalletsWithMFSProvider",
                "Associate wallet instrument type with the MFS provider");

        try {
            if (properties.getProperty("run.baseset.associatewallet").equalsIgnoreCase("yes")) {
                for (String provider : DataFactory.getAllProviderNames()) {

                    ExtentTest eTest = eSuite.createNode("ASSOCIATE_WALLET_FOR_" + provider,
                            "Associate Wallet Instrument Type with the MFS provider: " + provider);

                    // Login with Superadmin with Modify Bank Role
                    Login.init(eTest)
                            .loginAsSuperAdmin("MFSWTM01");

                    CurrencyProviderMapping.init(eTest).
                            initiateWalletCurrencyProviderMapping(provider);
                }
            } else {
                eSuite.skip("Skip associateWalletsWithMFSProvider");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Description:
     * Modify Association of Wallet .This function is used to modify services associated
     * Wallet With MFS Provider with wallets with the MFS provider. This test is to ensure
     * wallets are associated with provider while automation execution in existing DB.
     */
    @Test(priority = 6)
    public void modifyWalletAssociationWithMFSProvider() throws Exception {

        ExtentTest eSuite = extent.createTest("modifyWalletAssociationWithMFSProvider",
                "Modify services associated with the wallet instrument type with the MFS provider");

        try {
            if (properties.getProperty("run.baseset.associatewallet").equalsIgnoreCase("yes")) {
                for (String provider : DataFactory.getAllProviderNames()) {

                    ExtentTest eTest = eSuite.createNode("MODIFY_WALLET_FOR_" + provider,
                            "Modify services associated with the wallet instrument type with provider: " + provider);

                    // Login Superadmin with Modify Bank Role
                    Login.init(eTest)
                            .loginAsSuperAdmin("MFSMD");

                    CurrencyProviderMapping.init(eTest).
                            modifyWalletCurrencyProviderMapping(provider);
                }
            } else {
                eSuite.skip("Skip modifyWalletAssociationWithMFSProvider");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test:                                Description:
     * Create Group Roles                   Create All the Group roles based on the rnr input file
     * it's suggested to check the rnr sheet before initiating the base
     * setup execution. rnr sheet is available in the ConfigInput2.xlsx
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void createGroupRoles() throws Exception {
        ExtentTest eSuite = extent.createTest("CreateGroupRoles",
                "Create All the Group roles based on the rnr input file");

        try {
            if (properties.getProperty("run.baseset.grouprole").equalsIgnoreCase("Yes")) {

                ExcelUtil.deleteExcel(FilePath.fileGroupRoles);
                ExcelUtil.writeGroupRoleFileHeaders();

                /*
                 * create group role in the system with all the grade existing in the system
                 * for the specific Category
                 */
                for (WebGroupRole userRole : GlobalData.rnrDetails) {
                    if (!userRole.CategoryCode.equals(Constants.SUPER_ADMIN)) {
                     /*
                    if the Migrated User has to used, no web roles need to be created or updated.
                    Existing role names need to be fetched from the rnr sheet, assuming all roles are already existing
                    However, Mobile roles will not affect as only one mobile role per combination can exist
                    */
                        ExtentTest eTest = eSuite.createNode("ADD_GRP_ROLE_" + userRole.CategoryCode,
                                "Add Group role for Category " + userRole.CategoryName);

                        // Login with Superadmin with user having appropriate access
                        Login.init(eTest)
                                .loginAsSuperAdmin("GRP_ROL");

                        GroupRoleManagement.init(eTest)
                                .addWebGroupRole(userRole);

                        userRole.writeDataToExcel();
                    }
                }
            } else {
                eSuite.skip("Skip CreateGroupRoles");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 8)
    public void createMobileRoles() throws Exception {
        ExtentTest eSuite = extent.createTest("CreateMobileRoles",
                "Create All the Mobile roles based on the rnr input file");

        try {
            if (properties.getProperty("run.baseset.mobilerole").equalsIgnoreCase("yes")) {

                ExcelUtil.deleteExcel(FilePath.fileMobileRoles);
                ExcelUtil.writeMobileRoleFileHeaders();

                /*
                 * create Mobile roles in the system with all the grade existing in the system
                 * for the specific Category
                 */
                GroupRoleManagement.init(eSuite)
                        .addMobileGroupRoles();

            } else {
                eSuite.skip("Skip CreateMobileRoles");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test:                                Description:
     * createOperatorUsers                  Creating Base Setup Operator Users
     *
     * @throws Exception
     */
    @Test(priority = 9)
    public void createOperatorUsers() throws Exception, MoneyException {

        ExtentTest eSuite = extent.createTest("CreateOperatorUsers",
                "Creating Base Setup Operator Users!");

        try {
            if (properties.getProperty("run.baseset.operatoruser").equalsIgnoreCase("yes")) {
                /**
                 * Clean and Create Header
                 */
                ExcelUtil.deleteExcel(FilePath.fileOperatorUsers);
                ExcelUtil.writeOperatorUserFileHeaders();

                /**
                 * Create Operator Users
                 */
                ExtentTest chNode1 = eSuite.createNode("CREATE_OPT_USER_NWADM", "Create and Approve Network Admin");
                OperatorUserManagement.init(chNode1)
                        .baseSetCreateAdmin(Constants.NETWORK_ADMIN);

                /**
                 * Add Zone and Area
                 */
                ExtentTest chNodeAddZone = eSuite.createNode("CREATE_GEO_AND_ZONE", "Creating Geography & Zone");
                if (properties.getProperty("run.baseset.addZone").equalsIgnoreCase("yes")) {
                    OperatorUser naAddZone = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
                    Geography geography = new Geography();

                    Login.init(chNodeAddZone)
                            .login(naAddZone);

                    GeographyManagement.init(chNodeAddZone)
                            .addZone(geography);

                    GeographyManagement.init(chNodeAddZone)
                            .addArea(geography);
                } else {
                    chNodeAddZone.info("Skip CREATE_GEO_AND_ZONE");
                }

                ExtentTest chNode2 = eSuite.createNode("CREATE_OPT_USER_BANK_ADMIN", "Create and Approve Bank Admin");
                OperatorUserManagement.init(chNode2)
                        .baseSetCreateBankAdmin();

                ExtentTest chNode3 = eSuite.createNode("CREATE_OPT_USER_CHANNEL_ADMIN", "Create and approve Channel Admin");
                OperatorUserManagement.init(chNode3)
                        .baseSetCreateOptUser(Constants.CHANNEL_ADMIN, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

                ExtentTest chNode4 = eSuite.createNode("CREATE_OPT_USER_CCE", "Create and Approve Customer Care Executive");
                OperatorUserManagement.init(chNode4)
                        .baseSetCreateOptUser(Constants.CUSTOMER_CARE_EXE, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

                ExtentTest chNode5 = eSuite.createNode("CREATE_OPT_BANK_USER", "Create and approve Bank User");
                OperatorUserManagement.init(chNode5)
                        .baseSetCreateBankUsers();
            } else {
                eSuite.skip("Skip creating Base Set Operator Users!");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Create Base Setup Customer TCPs
     *
     * @throws Exception
     */
    @Test(priority = 10)
    public void createCustomerTCPs() throws Exception {
        ExtentTest eSuite = extent.createTest("createCustomerTCPs");

        try {
            if (properties.getProperty("run.baseset.customertcp").equalsIgnoreCase("yes")) {
                ExcelUtil.deleteExcel(FilePath.fileConsumerTCP);
                ExcelUtil.writeCustomerTcpFileHeaders();

                TCPManagement.init(eSuite)
                        .approveAllPendingCustomerTCP()
                        .addApproveAllCustomerTCP();
            } else {
                eSuite.skip("Skip creating Base Set Customer TCP!");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
            TCPManagement.init(eSuite)
                    .approveAllPendingCustomerTCP();
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Create Instrument TCPs
     *
     * @throws Exception
     */
    @Test(priority = 11)
    public void createInstrumentTCPs() throws Exception {
        ExtentTest tSuite = extent.createTest("createInstrumentTCPs");
        try {
            if (properties.getProperty("run.baseset.instrumenttcp").equalsIgnoreCase("yes")) {
                ExcelUtil.deleteExcel(FilePath.fileInstrumentTCP);
                ExcelUtil.writeInstrumentTcpFileHeaders();

                TCPManagement.init(tSuite)
                        .approveAllPendingInstrumentTCP()
                        .addAndApproveAllInstrumentTCP();
            } else {
                tSuite.skip("Skip creating Base Set Instrument TCP!");
            }
        } catch (Exception e) {
            markTestAsFailure(e, tSuite);
            TCPManagement.init(tSuite)
                    .approveAllPendingInstrumentTCP();
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Create O2C transfer Rules
     *
     * @throws Exception
     */
    @Test(priority = 12)
    public void createO2CtransferRules() throws Exception {
        ExtentTest eSuite = extent.createTest("createO2CtransferRules",
                "Configure O2C transfer Rule for all the channel domains");

        try {
            if (properties.getProperty("run.baseset.createo2c").equalsIgnoreCase("yes")) {
                TransferRuleManagement.init(eSuite)
                        .baseSetCreateO2CTRules();
            } else {
                eSuite.skip("Skip creating O2C Transfer Rule!");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }


        Assertion.finalizeSoftAsserts();
    }

    /**
     * STOCK to system wallet (IND01)
     *
     * @throws Exception
     */
    @Test(priority = 13)
    public void stockToSystemWallet() throws Exception {
        ExtentTest eSuite = extent.createTest("stockToSystemWallet", "Stock to IND01");

        try {
            if (properties.getProperty("run.baseset.operatoruser").equalsIgnoreCase("yes")) {

                List<OperatorUser> netAdmins = DataFactory.getOperatorUsersWithAccess("O2C_INIT", Constants.NETWORK_ADMIN);

                if (netAdmins == null) {
                    eSuite.fail("No Network admins available | Check Operator User Appdata Sheet");
                    return;
                }

                for (String provider : DataFactory.getAllProviderNames()) {
                    ExtentTest tSetStock = eSuite.createNode("SET_STOCK_LIMIT_" + provider,
                            "Set Stock Limit for Currency Provider: " + provider);
                    StockManagement.init(tSetStock)
                            .addNetworkStockLimit(provider, Constants.STOCK_LIMIT_AMOUNT);

                    ExtentTest tSetEAStock = eSuite.createNode("SET_EA_STOCK_LIMIT_" + provider,
                            "Set EA Stock Limit for Currency Provider: " + provider);
                    StockManagement.init(tSetEAStock)
                            .addEAStockLimit(provider, Constants.EA_STOCK_LIMIT_AMOUNT);
                }

                for (OperatorUser admin : netAdmins) {
                    for (String provider : DataFactory.getAllProviderNames()) {
                        ExtentTest eTest = eSuite.createNode("STOCK_TO_SYSTEM_FOR_" + admin.LoginId,
                                "Perform Stock Transfer for: " + admin.LoginId + ", from Currency Provider:" + provider);

                        String bank = DataFactory.getTrustBankName(provider);

                        StockManagement.init(eTest)
                                .initiateAndApproveNetworkStock(provider, bank, Constants.MAX_TRANSACTION_AMT);

                        ExtentTest eTest2 = eSuite.createNode("EA_STOCK_FOR_" + provider,
                                "Initiate and Approve EA Stock for " + provider);

                        Login.init(eTest2).login(DataFactory.getOperatorUserWithAccess("STR_INIT"));

                        // Initiate & Approve EA Stock
                        StockManagement.init(eTest2)
                                .initiateApproveEAStock(DataFactory.getRandomNumberAsString(5),
                                        Constants.MAX_EA_TRANSACTION_AMT, "Base Setup EA Stock", provider);
                    }

                }
            } else {
                eSuite.skip("Skipping adding Network Stock");
            }
        } catch (Exception e) {
            markTestAsFailure(e, eSuite);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * createChannelUser
     *
     * @throws Exception
     */
    @Test(priority = 14)
    public void createChannelUsers() throws Exception, MoneyException {

        // Extent Parent Node
        ExtentTest mainTest = extent.createTest("createChannelUsers", "Create Base Set Channel Users");

        try {
            if (!ConfigInput.runChannelUserCreation) {
                mainTest.skip("Skip creating Base Set Channel Users");
                return;
            }

            DataFactory.loadInstrumentTCP(); // as by now instrument TCP sheet has to be generated
            DataFactory.loadAutomationGeography();
            DataFactory.loadMobileGroupRole();
            ExcelUtil.deleteChannelUser(FilePath.fileChannelUsers);
            String[] payIdArr = DataFactory.getPayIdApplicableForChannelUser();

            List<String> arrCategoryNotToInclude = Arrays.asList(
                    Constants.NETWORK_ADMIN,
                    Constants.BANK_ADMIN,
                    Constants.CHANNEL_ADMIN,
                    Constants.CUSTOMER_CARE_EXE,
                    Constants.BULK_PAYER_ADMIN,
                    Constants.BANK_USER,
                    Constants.SUBSCRIBER,
                    Constants.ENTERPRISE,
                    Constants.BILLER,
                    Constants.SUPER_ADMIN
            );

            /*
             * Iterate on Category and create Users
             */
            for (WebGroupRole grpRole : GlobalData.rnrDetails) {

                if (arrCategoryNotToInclude.contains(grpRole.CategoryCode)) {
                    continue;
                }

                // create the user based on the condition
                // Iterate on Grades
                String catCode = grpRole.CategoryCode;
                List<GradeDB> grades = DataFactory.getGradesForCategory(catCode);
                for (GradeDB grade : grades) {

                    /**
                     * SubTest
                     * Map Wallet Preferences for each Grade
                     */
                    ExtentTest subTest1 = mainTest.createNode("MAP_SPECIAL_WALLET_PREF_FOR_" + grpRole.CategoryCode);
                    CurrencyProviderMapping.init(subTest1)
                            .mapWalletPreferencesUserRegistration(new User(grpRole.CategoryCode, grade.GradeName), payIdArr);

                    // Create based on number of users
                    for (int j = 0; j < grpRole.NumberOfUser; j++) {

                        // Create user object
                        User user = new User(catCode, grade.GradeName, grpRole.RoleName);

                        //Create Channel Users
                        ExtentTest subTest2 = mainTest
                                .createNode("CREATE_CHANNEL_USER_" + grade.GradeName + "_" + catCode,
                                        "Create Channel User for Category:" + catCode + " & Grade: " + grade.GradeCode);

                        ChannelUserManagement.init(subTest2)
                                .createChannelUser(user);

                        user.writeDataToExcel(); // write user to app data

                        // perform O2C
                        ExtentTest subTest3 = mainTest.createNode("PERFORM_O2C_" + grade.GradeCode,
                                "Perform Owner To Channel Transaction for Grade " + grade.GradeName);

                        for (String provider : DataFactory.getAllProviderNames()) {
                            TransactionManagement.init(subTest3)
                                    .initiateAndApproveO2CWithProvider(user, provider, Constants.MAX_O2C_AMOUNT);
                        }
                    }
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, mainTest);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Add Subscriber User
     *
     * @throws Exception
     */
    @Test(priority = 15)
    public void createSubscriberUsers() throws Exception {

        ExtentTest mainTest = extent.createTest("createSubscriberUsers", "Create Base Set Subscriber Users");
        try {

            if (!properties.getProperty("run.baseset.subscriber").equalsIgnoreCase("yes")) {
                mainTest.skip("Skip creating Base Set Subscribers");
                return;
            }

            DataFactory.loadInstrumentTCP();
            DataFactory.loadAutomationGeography();
            DataFactory.loadMobileGroupRole();
            DataFactory.loadCurrencyProvider();
            ExcelUtil.deleteSubscriberUser(FilePath.fileChannelUsers);

            User wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");

            /**
             * SubTest 1
             * Configure Subscriber Commission Rule
             */
            ExtentTest subTest1 = mainTest.createNode("SUBS_COMMISSION_RULE_TEST-01", "Create Subscriber Commission Rule");
            ServiceChargeManagement.init(subTest1).addNewSubsCommissionRule();

            for (WebGroupRole grpRole : GlobalData.rnrDetails) {

                // continue if role is not Subscriber
                if (!grpRole.CategoryCode.equals(Constants.SUBSCRIBER)) {
                    continue;
                }

                String catCode = grpRole.CategoryCode;
                List<GradeDB> grades = DataFactory.getGradesForCategory(catCode);

                for (GradeDB grade : grades) {
                    //Configure Wallet Preference
                    ExtentTest subTest2 = mainTest.createNode("MAP_WALLET_PREFERENCES_" + grpRole.CategoryCode);
                    CurrencyProviderMapping.init(subTest2)
                            .mapWalletPreferencesUserRegistration(new User(grpRole.CategoryCode, grade.GradeName), DataFactory.getPayIdApplicableForSubs());

                    //Acquisition Fees Service Charge Must be Configured
                    ExtentTest subTest3 = mainTest.createNode("CONFIGURE_ACQFEE_" + grade.GradeCode, "Configure Service Charge for Acquisition Fees for " + grade.GradeName);
                    configureAqqFeeServiceCharge(grade.GradeName, wholeSaler, subTest3);

                    for (int j = 0; j < grpRole.NumberOfUser; j++) {
                        // Subscriber Object
                        User subsUser = new User(Constants.SUBSCRIBER, grade.GradeName, grpRole.RoleName);


                        /**
                         * SubTest 4
                         * Create Subscriber
                         * note that subscriber creator has to be of same grade of which the service charge is created
                         */
                        ExtentTest subTest4 = mainTest.createNode("CREATE_LEAF_USER_:" + grade.GradeName,
                                "Create Leaf User with Category SUBS and Grade: " + grade.GradeName);

                        SubscriberManagement.init(subTest4)
                                .createSubscriber(subsUser, wholeSaler.GradeName);

                        /**
                         * Sub Test 5
                         * Perform Cash In
                         */
                        ExtentTest subTest5 = mainTest.createNode("PERFORM_CASH_IN_FOR_" + subsUser.LoginId,
                                "Initiate and Approve Cash In for " + subsUser.GradeCode);


                        for (String provider : DataFactory.getAllProviderNames()) {
                            ServiceCharge tRuleCashIn = new ServiceCharge(Services.CASHIN, wholeSaler, subsUser, null, null, provider, provider);

                            // configure Transfer Rule
                            TransferRuleManagement.init(subTest5)
                                    .configureTransferRule(tRuleCashIn);

                            // make sure Channel user has sufficient balance
                            TransactionManagement.init(subTest5)
                                    .makeSureLeafUserHasBalanceUI(wholeSaler);

                            // Perform Cash In
                            Login.init(subTest5).login(wholeSaler);
                            TransactionManagement.init(subTest5)
                                    .performCashIn(subsUser, Constants.MAX_CASHIN_AMOUNT, provider);

                            //Added for unhandled alert which sometime comes unexpectedly
                            AlertHandle.checkAlert();
                        }

                        subsUser.writeDataToExcel(); // write data to the AppData
                    }
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, mainTest);
        }

        Assertion.finalizeSoftAsserts();
    }

    private void configureAqqFeeServiceCharge(String gradeName, User chUser, ExtentTest t1) {
        try {
            for (String provider : DataFactory.getAllProviderNames()) {
                // Configure the Service Charge
                ServiceChargeManagement.init(t1)
                        .configureServiceCharge(new ServiceCharge(Services.ACQFEE,
                                new User(Constants.SUBSCRIBER, gradeName),
                                chUser,
                                null,
                                null,
                                provider,
                                provider));
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * Add Enterprise User
     *
     * @throws Exception Creating Enterprise User Post Subscriber User Creation
     */
    @Test(priority = 16)
    public void createEnterPriseUsers() throws Exception {
        ExtentTest mainTest = extent.createTest("createEnterPriseUsers", "Create Base Set Enterprise Users");

        try {
            if (!ConfigInput.runChannelUserCreation) {
                mainTest.skip("Skip creating Base Set Enterprise User");
                return;
            }

            DataFactory.loadInstrumentTCP();
            DataFactory.loadAutomationGeography();
            DataFactory.loadMobileGroupRole();
            ExcelUtil.deleteEnterPriseUser(FilePath.fileChannelUsers);

            for (WebGroupRole grpRole : GlobalData.rnrDetails) {

                if (!grpRole.CategoryCode.equals(Constants.ENTERPRISE)) {
                    continue;
                }

                List<GradeDB> grades = DataFactory.getGradesForCategory(Constants.ENTERPRISE);

                for (int j = 0; j < grpRole.NumberOfUser; j++) {
                    String catCode = grpRole.CategoryCode;

                    for (GradeDB grade : grades) {

                        // Create Transfer rule enterp
                        ExtentTest subTest1 = mainTest.createNode("CREATE_TRULE_ENT_PAY_" + j,
                                "Create Transfer Rule ENTERPRISE_PAYMENT for Enterprise User");

                        User entUsr = new User(catCode, grade.GradeName, grpRole.RoleName);

                        TransferRuleManagement.init(subTest1).configureTransferRule(new ServiceCharge(
                                Services.ENTERPRISE_PAYMENT,
                                entUsr,
                                new User(Constants.SUBSCRIBER),
                                null,
                                null,
                                null,
                                null));

                        // create enterprise user
                        ExtentTest subTest2 = mainTest.createNode("CREATE_ENTERPRISE_" + grade.GradeCode,
                                "Create EnterPrise User having Grade " + grade.GradeName);

                        ChannelUserManagement.init(subTest2)
                                .createChannelUser(entUsr);

                        entUsr.writeDataToExcel();
                    }
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, mainTest);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 17)
    public void createBulkPayerAdmin() throws Exception {
        ExtentTest mainTest = extent.createTest("createBulkPayerAdmin", "Create Base Set Bulk Payer Admin");

        try {
            if (!ConfigInput.runChannelUserCreation) {
                mainTest.skip("Skip creating Bulk Payer Admin");
                return;
            }

            DataFactory.loadInstrumentTCP();
            DataFactory.loadAutomationGeography();
            DataFactory.loadMobileGroupRole();
            ExcelUtil.clearBulkPayAdminfromAppData();

            for (WebGroupRole bpgRole : GlobalData.rnrDetails) {
                if (bpgRole.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                    OperatorUser bpa = new OperatorUser(Constants.BULK_PAYER_ADMIN);
                    bpa.setWebGroupRole(bpgRole.RoleName);

                    ExtentTest subTest3 = mainTest.createNode("CREATE_BULK_PAYER_ADMIN_0" + bpa.LoginId,
                            "Create Bulk Payer Admin");

                    User entUsr = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
                    OperatorUserManagement.init(subTest3)
                            .createBulkPayerAdmin(bpa, entUsr);

                    bpa.writeDataToExcel();
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, mainTest);
        }

        Assertion.finalizeSoftAsserts();
    }
}
