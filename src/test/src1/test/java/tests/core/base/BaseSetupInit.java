package tests.core.base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.CITReport;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MfsTestProperties;
import framework.util.reportManager.ExtentManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rahul.rana on 5/28/2017.
 */
public class BaseSetupInit {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseSetupInit.class);
    protected static ExtentReports extent;
    private static ThreadLocal parentTest = new ThreadLocal();
    private static WebDriver driver;
    protected ExtentTest pNode;
    protected MfsTestProperties properties;

    @AfterClass
    public static void tearDown() throws Exception {
        extent.flush();
        //Added for CIT REPORT GENERATION
        if (MfsTestProperties.getInstance().getProperty("report.generate.cit").equalsIgnoreCase("Yes")) {
            CITReport citReport = new CITReport();
            citReport.generateCITReport();
        }
        OracleDB.CloseConnection();
    }

    @BeforeSuite
    public void beforeSuite() throws Exception {

        if (ExtentManager.getFileName() == null) {
            ExtentManager.setFileName(getClass().getSimpleName() + "_" +
                    new SimpleDateFormat("dd-MM-yyyy_HH-mm").format(new Date()) + ".html");
        }

        extent = ExtentManager.getInstance();

        pNode = extent.createTest("Load Environment Variables and Base Set");

        ExtentTest t1 = pNode.createNode("Load Environment Variables and Base Set");
        parentTest.set(pNode);

        ConfigInput.init();
        t1.info("load Config Inputs, Environment Variables, mfstest.properties");

        AppConfig.init();
        t1.info("load Application Configuration from DB - MTX_SYSTEM_PREFERENCES");

        DataFactory.loadDomainCategoryDBMap();
        t1.info("load Domain Category DB Map");

        DataFactory.loadRNRSheet(t1);
        t1.info("Load ConfigInput.xlsx(roles and responsibility)");

        DataFactory.loadSuperAdminRoles(t1);
        t1.info("Fetch Superadmins roles and responsibility)");

        DataFactory.loadGradesFromConfig();
        t1.info("load Grade Category Config Map");

        DataFactory.loadCurrencyProvider();
        t1.info("load currency provider details");

        DataFactory.loadWallet();
        t1.info("load Wallet related information");

        DataFactory.loadServiceList();
        t1.info("load Service charge related information, ExecutionConstants.xlsx");

        DataFactory.loadTransferRuleInput();
        t1.info("load transfer rule inputs");

        DataFactory.loadSystemMsisdn();
        t1.info("load unique MSISDN from DB");

        DataFactory.initializeExecutionGroups();

        //To RUn the Constructor
        new MobiquityGUIQueries();
        new MobiquityDBAssertionQueries();

        extent.setSystemInfo("URL", ConfigInput.URL);
        extent.setSystemInfo("Browser", MfsTestProperties.getInstance().getProperty("browser.name"));
        extent.setSystemInfo("DB", MfsTestProperties.getInstance().getProperty("db.ip"));
        extent.setSystemInfo("DB Port", MfsTestProperties.getInstance().getProperty("db.port"));
        extent.setSystemInfo("DB UserName", MfsTestProperties.getInstance().getProperty("db.username"));
        extent.setSystemInfo("DB SID", MfsTestProperties.getInstance().getProperty("db.sid"));
        extent.setSystemInfo("Number of Category", "" + GlobalData.rnrDetails.size());
        extent.setSystemInfo("Number of Grades", "" + DataFactory.getAllGradeNames().size());
        extent.setSystemInfo("Number of Providers", "" + GlobalData.currencyProvider.size());
        extent.setSystemInfo("Number of Banks", "" + DataFactory.getAllBank().size());
        extent.setSystemInfo("Number of Wallets", "" + GlobalData.wallet.size());

        // Create Required Directories
        File directory = new File(FilePath.dirFileUploads);
        if (!directory.exists())
            directory.mkdir();

        File dirFailSuite = new File(FilePath.dirFailSuitesPath);
        if (!dirFailSuite.exists())
            dirFailSuite.mkdir();

        File dirFailExecStatus = new File(FilePath.dirFailTestRecord);
        if (!dirFailExecStatus.exists())
            dirFailExecStatus.mkdir();

        File dirExecutionStatusLog = new File(FilePath.dirDailyExecStatusLog);
        if (!dirExecutionStatusLog.exists())
            dirExecutionStatusLog.mkdir();

        //Verify Critical Preference Values Must Set To Default Values
        DataFactory.loadConfigPreference();
        SystemPreferenceManagement.baseinit(pNode)
                .makeSureAppPreferencesAreIntact();

        extent.flush();
        LOGGER.info("*** Complete Suite Init For Base Setup ***\n");
    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethodRun(Method method) {
        properties = MfsTestProperties.getInstance();
        Login.resetLoginStatus();
        driver = DriverFactory.getDriver(); // get the driver instance before a method is executed
        System.out.println("---");
        System.out.println("STARTED BASE TEST >>> " + method.getName());
        Assertion.resetSoftAsserts();
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethod(ITestResult result) {
        if (result.getStatus() == ITestResult.SUCCESS) {
            System.out.println("BASE TEST         >--->>  " + result.getName() + " - Executed Successfully");
            pNode.pass("BASE TEST: " + result.getName() + ": Test Case Executed Successfully!");
        } else if (result.getStatus() == ITestResult.FAILURE) {
            System.out.println("BASE TEST         >--->>  " + result.getName() + " - Execution Failed");
            String code = "BASE TEST: " + result.getName() + "\n" +
                    "Reason: " + result.getThrowable().getMessage();
            Markup m = MarkupHelper.createCodeBlock(code);
            pNode.fail(m);
        } else if (result.getStatus() == ITestResult.SKIP) {
            System.out.println("BASE TEST         >--->>  " + result.getName() + " - Execution Skipped");
            pNode.skip(result.getName() + ":Test Case Executed Skipped!");
        }
        System.out.println("END BASE TEST     >--->>> " + result.getName());
        System.out.println("---");

        extent.flush();
        ConfigInput.isAssert = true;

        if (DriverFactory.getDriver().findElements(By.partialLinkText("Logout")).size() > 0) {
            DriverFactory.getDriver().findElement(By.partialLinkText("Logout")).click();
        }

        DriverFactory.quitDriver(); // quit the driver instance once a test method is executed
        Login.resetLoginStatus();
    }

    @AfterTest
    public void afterTestMethod() {
        extent.flush();
    }

    protected void markSetupAsFailure(Exception e) {
        e.printStackTrace();
        pNode.fail("Setup Failure due to Exception for: " + getClass().getSimpleName());
        Assert.fail("Setup Failure Due to Exception: " + e.toString());
    }

    protected void markTestAsFailure(Exception e, ExtentTest t1) {
        e.printStackTrace();
        t1.fail("Test Failure due to Exception");
        Assertion.markAsFailure("Test Failure Due to Exception");
    }


}
