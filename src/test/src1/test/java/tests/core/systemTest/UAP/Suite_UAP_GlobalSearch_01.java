package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.globalSearch.GlobalSearch;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Global Search
 *
 * @author navin.pramanik
 */
public class Suite_UAP_GlobalSearch_01 extends TestInit {

    private OperatorUser usrGlobalSrch;
    private User user;


    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void GlobalSearch() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0071 :Global Search", "To verify that the valid user can perform global search.");


        try {


            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

            usrGlobalSrch = DataFactory.getOperatorUserWithAccess("RP_GOS", Constants.NETWORK_ADMIN);

            user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Login.init(t1).login(usrGlobalSrch);

            GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LOGINID, user.LoginId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
