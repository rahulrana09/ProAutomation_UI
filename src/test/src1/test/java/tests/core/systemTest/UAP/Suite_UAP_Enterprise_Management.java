package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Contain the cases for EnterPrise Management
 *
 * @author Prashant.kumar
 */

public class Suite_UAP_Enterprise_Management extends TestInit {

    private User enterpriseUser, subs, subs2;
    private String uniqueCode, updatedCode;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");
        try {
            enterpriseUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);

            subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(s1)
                    .createDefaultSubscriberUsingAPI(subs);

            subs2 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(s1)
                    .createDefaultSubscriberUsingAPI(subs2);

            uniqueCode = DataFactory.getRandomNumberAsString(5);
            updatedCode = DataFactory.getRandomNumberAsString(5);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    // disabled as enterprise user creeation is taken care in base setup
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.DEPENDENCY_TAG, FunctionalTag.ECONET_UAT_5_0}, priority = 1)
    public void TC0055() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0055",
                "ADD  Enterprise Management: To verify that the valid user can add enterprise in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.DEPENDENCY_TAG, FunctionalTag.ECONET_UAT_5_0);
        try {
            User entUsr = new User(Constants.ENTERPRISE);
            ChannelUserManagement.init(t1).
                    createChannelUser(entUsr);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(enabled = false, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM}, priority = 3)
    public void Enterprise_Management3() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0057 : perform bulk payment", "To verify that the enterprise can perform bulk payment. (Manual Upload Process)")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.ENTERPRISE_MANAGEMENT);


        Login.init(t1).login(enterpriseUser);
        try {
            Login.init(t1).login(enterpriseUser);
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, subs.MSISDN, "1", "", "", ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.PVG_UAP,
            FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0},
            priority = 4)
    public void addBulkPayer() throws Exception {

        OperatorUser bulkPayerAdmin = new OperatorUser(Constants.BULK_PAYER_ADMIN);

        ExtentTest t1 = pNode.createNode("TC0058",
                "To verify that the enterprise can add bulk payer admin in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0,
                        FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            OperatorUserManagement.init(t1)
                    .createBulkPayerAdmin(bulkPayerAdmin, enterpriseUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

        ExtentTest t2 = pNode.createNode("TC0059",
                "View Bulk Payer, To verify that the enterprise can view the details of  bulk payer admin in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t2).login(enterpriseUser);

            EnterpriseManagement.init(t2)
                    .viewBulkPayerAdmin(bulkPayerAdmin);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0}, priority = 6)
    public void Enterprise_Management6() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0060",
                "View Self Details: To verify that the enterprise can view the self details.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT,
                        FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(enterpriseUser);

            EnterpriseManagement.init(t1)
                    .viewSelfDetails(enterpriseUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 7)
    public void Enterprise_Management7() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0056", "ADD Bulk payee, To verify that the enterprise can add bulk payee user.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            Login.init(t1).login(enterpriseUser);
            EnterpriseManagement.init(t1)
                    .addBulkPayee(subs.MSISDN, uniqueCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

        ExtentTest t2 = pNode.createNode("TC0061",
                "Modify Bulk Payee: To verify that the enterprise can modify bulk payee user.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t2).login(enterpriseUser);

            EnterpriseManagement.init(t2)
                    .modifyBulkPayee(subs.MSISDN, uniqueCode, updatedCode);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 8)
    public void Enterprise_Management8() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0062",
                "Delete Bulk Payee: To verify that the enterprise can delete bulk payee user.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            Login.init(t1).login(enterpriseUser);

            EnterpriseManagement.init(t1)
                    .deleteBulkPayee(subs.MSISDN, updatedCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(enabled = false, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM}, priority = 9)
    public void Enterprise_Management9() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0063",
                "Batch Add bulk Payee: To verify that the enterprise can add bulk payee user.(File Upload)")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT);

        try {
            Login.init(t1).login(enterpriseUser);

            String uniqueCode = DataFactory.getRandomNumberAsString(5);

            EnterpriseManagement.init(t1)
                    .addBulkPayeeBatch(subs2, uniqueCode);

            //Added to delete the added Bulk payee
            EnterpriseManagement.init(t1)
                    .deleteBulkPayee(subs2.MSISDN, uniqueCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    // v5.01 enterprise can be assined with multiple wallets
    @Test(enabled = false, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM}, priority = 10)
    public void Enterprise_Management_10() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0064",
                "Create Enterprise with default wallet: To verify that the only " +
                        "default wallet can assigned to the enterprise.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT);

        try {
            Login.init(t1).login(enterpriseUser);

            User newEntUser = new User(Constants.ENTERPRISE);

            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(newEntUser, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }
}
