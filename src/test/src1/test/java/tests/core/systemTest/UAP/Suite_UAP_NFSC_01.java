package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Non Financial Service Charge
 *
 * @author navin.pramanik
 */
public class Suite_UAP_NFSC_01 extends TestInit {

    private ServiceCharge nfscSCharge;
    private OperatorUser opt;
    private User payee;
    private Grade grade;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            opt = new OperatorUser(Constants.NETWORK_ADMIN);
            grade = new Grade(Constants.RETAILER);
            //payee = new User(Constants.RETAILER, grade.GradeName);

            payee = new User(Constants.RETAILER);

            nfscSCharge = new ServiceCharge(Services.BALANCE_ENQUIRY, payee, opt, null, null, null, null);
            nfscSCharge.setIsNFSC();
            ExtentTest test = pNode.createNode("SETUP", "Add grade to create NFSC.");
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }

       /* Login.init(test).loginAsSuperAdmin("ADD_GRADES");
        GradeManagement.init(test).addGrade(grade);*/
    }


    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void addNFSCTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0110 :NFSC-> Add NFSC", "To verify that valid user can add non-financial service charge into the system.");

        t1.assignCategory(FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0);

        ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(nfscSCharge);

        ExtentTest t2 = pNode.createNode("TC0108  :NFSC-> View NFSC", "To verify that valid user can view non-financial service charge in the system.");

        t2.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0);

        ServiceChargeManagement.init(t2).viewNFSChargeLatestVersion(nfscSCharge, "999");
    }


    /**
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.PVG_UAP, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0})
    public void modifyNFSCTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0105 : NFSC-> Modify NFSC", "To verify that valid user can modify Non-financial service charge in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

        ServiceChargeManagement.init(t1).
                modifyNFSCInitAndApprove(nfscSCharge);
    }

    @Test(priority = 4, groups = {FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
            FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0})
    public void suspendResumeNFSCTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0107 :NFSC-> Suspend/Resume NFSC", "To verify that valid user can Suspend/Resume Non-financial service charge in the system.");

        t1.assignCategory(FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_UAP,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0);

        ServiceChargeManagement.init(t1).
                suspendNFSCharge(nfscSCharge);

        ServiceChargeManagement.init(t1).
                resumeNFSCharge(nfscSCharge);
    }


    /**
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_SIT_5_0})
    public void deleteNFSCTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0106 :NFSC-> Delete NFSC", "To verify that valid user can Delete Non-financial service charge in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_SIT_5_0);

        ServiceChargeManagement.init(t1).
                deleteNFSChargeAllVersions(nfscSCharge);

    }


    /**
     * @throws Exception
     */
//    @AfterClass(alwaysRun = true)
    public void tearDownAfterClass() throws Exception {
        ExtentTest test = pNode.createNode("TEAR DOWN", "Again Creating Non Financial Service Charge which was deleted");
        ServiceChargeManagement.init(test).configureNonFinancialServiceCharge(nfscSCharge);
        /*Login.init(test).loginAsSuperAdmin("DELETE_GRADES");
        GradeManagement.init(test).deleteGrade(grade, true);*/
    }
}
