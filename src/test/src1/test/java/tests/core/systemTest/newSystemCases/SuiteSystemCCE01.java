/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 07-Jan-2018
 *  Purpose: Test Suite of CCE
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


public class SuiteSystemCCE01 extends TestInit {
    private OperatorUser networkAdmin, cceAdmin;
    private User subs, chUser;
    private String defProvider;

    @BeforeClass
    public void prequest() throws Exception {
        try {
            subs = new User(Constants.SUBSCRIBER);
            defProvider = DataFactory.getDefaultProvider().ProviderName;
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test to Change status of user from Active to Bar
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_0001() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0001 : CCE",
                "To verify that valid user can change the consumer/channel user status from Active to Bar");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        try {
            networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).barChannelUser(subs, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test used to change Status of user from Bar to Unbar
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.CCE})
    public void SYS_TC_0002() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0002  : CCE",
                "To verify that valid user can change the consumer/channel user status from Bar to unBar");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

        Login.init(t1).login(networkAdmin);
        ChannelUserManagement.init(t1).unBarChannelUser(subs, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test to search the consumer or channel user details
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_0675() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0675  : CCE",
                "To verify that CCE should be able to search the consumer or channel user details as follows with minimal effort through Global Search.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
        Login.init(t1).login(networkAdmin);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test to perform Transaction Correction Initiation
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILL_PAYMENT})
    public void SYS_TC_0694() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_0694 : CCE",
                "To verfiy that valid user can perform Transaction Correction Initiation");

        networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        subs = DataFactory.getChannelUserWithAccess(Constants.SUBSCRIBER);

        Login.init(t1).login(subs);

        String transID = TransactionManagement.init(t1).performCashIn(subs, "100", defProvider);

        Login.init(t1).login(networkAdmin);

        TransactionCorrection.init(t1).initiateTxnCorrection(transID, false, false);
    }


}
