/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: surya.dhal
 *  Date: 23-May-2018
 *  Purpose: Test of Category Management.
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Category;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.categoryManagement.CategoryManagement;
import framework.features.common.Login;
import framework.pageObjects.CategoryManagement.AddCategory_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by surya.dhal on 5/23/2018.
 */
public class SuiteSystemCategoryManagement extends TestInit {
    OperatorUser initiator, approver, modifier;
    User user;
    Category category;


    @BeforeClass
    private void preRequisite() throws Exception {
        category = new Category(Constants.WHOLESALER);
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        initiator = DataFactory.getOperatorUserWithAccess("ADD_CAT");
        modifier = DataFactory.getOperatorUserWithAccess("ADD_CAT");
        approver = DataFactory.getOperatorUserWithAccess("CAT_APPRL");

        //TODO - Have to Call Domain Creation Class when Domain Management completed.
    }

    /**
     * TEST : POSITIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0001
     * DESC : Add Initiate Category
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0001() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0001: ",
                "To verify that channel admin should be able to add initiate category and the Category code & name must be unique in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        Login.init(t1).login(initiator);
        CategoryManagement.init(t1)
                .addInitiateCategory(category);

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0003
     * DESC : At a time system will allow only one category creation
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0003() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0003: ",
                "To verify that At a time system will allow only one category creation across all domains.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        Login.init(t1).login(initiator);
        AddCategory_Page1 page1 = new AddCategory_Page1(t1);
        page1.navigateCategoryManagementPage();
        Assertion.verifyErrorMessageContain("category.AI.exists", "Check If At a time system will allow only one category creation", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0005
     * DESC : Modify Category Creation
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0005() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0005: ",
                "To verify that Once a category is added & approved, the same cannot be modified or deleted");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        Login.init(t1).login(modifier);

        CategoryManagement.init(t1).modifyCategory();
    }

    /**
     * TEST : POSITIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0002
     * DESC : Approve Category
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0002() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0002: ",
                "To verify that only network admin is able to approve the initiated category.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        Login.init(t1).login(approver);

        CategoryManagement.init(t1)
                .approveAddCategory();

    }


    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0006
     * DESC : Reject Category Creation
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0006: ",
                "To verify that Network Admin is able to navigate to previous page once click on back button on initiation page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        user.CategoryName = "AutoCategory2";
        user.CategoryCode = "AutoCat2";

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();
        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        AddCategory_Page1 page1 = new AddCategory_Page1(t1);
        page1.clickBack();
        page1.checkNavigateBackOrNot();

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0004
     * DESC : Reject Category Creation
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0004() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0004: ",
                "To verify that  \"Network Admin\" should be able  to reject a category creation request.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setCategoryName("AutoCategory2");
        category.setCategoryCode("AutoCat2");

        Login.init(t1).login(initiator);
        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        Login.init(t1).login(approver);
        CategoryManagement.init(t1).rejectPendingCategory();

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0007
     * DESC : Add Category  when  "Category Name"  field left blank.
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0007: ",
                "To verify that channel admin should not be able to add Category  when  \"Category Name\"  field left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setCategoryName(Constants.BLANK_CONSTANT);

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();

        CategoryManagement.init(t1).addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("category.catname.blank", "Verify When Category Name left Blank", t1);


    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0008
     * DESC : Add Category  when  "Category Code"  field left blank.
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0008: ",
                "To verify that channel admin should not be able to add Category when  \"Category Code\"  field left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setCategoryCode(Constants.BLANK_CONSTANT);

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();
        CategoryManagement.init(t1).addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("category.catcode.blank", "Verify When Category Code left Blank", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0009
     * DESC : Add Category  when "Special Character" is entered in "Category Name"  field.
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0009: ",
                "To verify that channel admin should not be able to add Category  when  \"Special Character\" is entered in \"Category Name\"  field.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setCategoryName(Constants.SPECIAL_CHARACTER_CONSTANT);

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();

        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("category.isvalid.value", "when  \"Special Character\" is entered in \"Category Name\"  field.", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0010
     * DESC : Add Category  when "Special Character" is entered in "Category Code"  field.
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0010() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0010: ",
                "To verify that channel admin should not be able to add Category  when  \"Special Character\" is entered in \"Category Code\"  field.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setCategoryCode(Constants.SPECIAL_CHARACTER_CONSTANT);

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();

        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("category.catcode.isvalid", "when  \"Special Character\" is entered in \"Category Code\"  field.", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0011
     * DESC : Add Category  when when  "Domain" is not selected.
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0011() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0011: ",
                "To verify that channel admin should not be able to add Category when  \"Domain\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setDomainCode(null);

        Login.init(t1).login(initiator);

        startNegativeTestWithoutConfirm();

        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("grouprole.required.doaminCode", "when  \"Domain\" is not selected.", t1);
    }

    /**
     * TEST : NEGATIVE TEST
     * ID : SYS_TC_CategoryManagement_S_0012
     * DESC : Add Category  when "Parent Category" not selected.
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN})
    public void SYS_TC_CategoryManagement_S_0012() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_CategoryManagement_S_0012: ",
                "To verify that channel admin should not be able to add Category when  \"Parent Category\" not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT);

        category.setDomainCode(null);

        Login.init(t1).login(initiator);

        CategoryManagement.init(t1)
                .addInitiateCategory(category);

        Assertion.verifyErrorMessageContain("system.parentcategory", " when Parent Category is not selected.", t1);
    }

}
