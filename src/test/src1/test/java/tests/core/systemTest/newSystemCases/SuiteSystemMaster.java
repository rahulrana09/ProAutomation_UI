/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 27-Nov-2017
 *  Purpose: Test Of Master Module
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.master.Master;
import framework.pageObjects.master.NetworkStatus_page1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by surya.dhal on 12/27/2017.
 */
public class SuiteSystemMaster extends TestInit {

    private OperatorUser netStat;

    @BeforeClass(alwaysRun = true)
    private void setup() throws Exception {
        netStat = DataFactory.getOperatorUserWithAccess("MNT_STS");
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test Method to verify Back Button Functionality
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER})
    public void SYS_TC_Master_S_0001() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_Master_S_0001",
                "To verify that the \"Network Admin\" should be able to  navigate back when click on back button.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER);

        Login.init(t1).login(netStat);

        startNegativeTestWithoutConfirm();

        Master.init(t1).changeNetworkStatus(Constants.NETWORK_STATUS);

        NetworkStatus_page1 networkStatusPage1 = new NetworkStatus_page1(t1);

        networkStatusPage1.clickBackBtn();

        boolean isTrue = Utils.checkElementPresent("button.reset", Constants.FIND_ELEMENT_BY_NAME);

        Assertion.verifyEqual(isTrue, true, "Verify Reset button", t1);

        if (isTrue) {
            t1.pass("Successfully Navigate to Previous page.");
        } else {
            t1.fail("Failed to Navigate to previous page.");
        }

    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to verify Reset button functionality
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER})
    public void SYS_TC_Master_S_0002() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_Master_S_0002" + getTestcaseid(),
                "To verify that the \"Network Admin\"   should be able to  reset the message when click on \"Reset\" button.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER);

        Login.init(t1).login(netStat);

        NetworkStatus_page1 networkStatusPage1 = new NetworkStatus_page1(t1);

        networkStatusPage1.navigateToLink();

        String statusBeforeReset = networkStatusPage1.getEnglishStatusBoxText();
        networkStatusPage1.setEnglishLangStatusBox(Constants.NETWORK_STATUS);

        networkStatusPage1.clickResetButton();
        String statusAfterReset = networkStatusPage1.getEnglishStatusBoxText();
        Assertion.verifyEqual(statusAfterReset, statusBeforeReset, "Verify Status Message", t1);

    }

    /**
     * TEST : NEGATIVE TEST
     * DESC : Test method when Message field left blank
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER})
    public void SYS_TC_Master_S_0003() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_Master_S_0003" + getTestcaseid(),
                "To verify that the \"Network Admin\"   should not be able to change Network status when message field left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MASTER);

        Login.init(t1).login(netStat);
        Master.init(t1).negativeTestConfirmation().startNegativeTest().changeNetworkStatus(Constants.BLANK_CONSTANT);

        String alertText = AlertHandle.getAlertText(t1);
        String expected = MessageReader.getMessage("alert.Message.for.language.field(s).is.necessary.or.invalid.", null);

        Assertion.verifyEqual(alertText, expected, "Verify Alert Text", t1);

    }

}
