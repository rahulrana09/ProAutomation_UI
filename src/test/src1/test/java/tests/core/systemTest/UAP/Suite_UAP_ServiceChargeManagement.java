package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 *
 */
public class Suite_UAP_ServiceChargeManagement extends TestInit {

    private static ServiceCharge serviceCharge;
    private OperatorUser operatorUser, subsCommRuleCreater, usrSChargeModify, usrSChargeView;
    private User user;
    private String lastDaystoViewSC;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("setup", "Setup specific for this Script");
        try {
            usrSChargeModify = DataFactory.getOperatorUserWithAccess("SVC_MOD");
            usrSChargeView = DataFactory.getOperatorUserWithAccess("SVC_VIEW");
            operatorUser = new OperatorUser(Constants.NETWORK_ADMIN);
            user = new User(Constants.WHOLESALER);
            subsCommRuleCreater = DataFactory.getOperatorUserWithAccess("SVC_SUBCOMRULE");
            lastDaystoViewSC = "999";
            serviceCharge = new ServiceCharge(Services.TXN_CORRECTION, operatorUser, user, null, null, null, null);

            ServiceChargeManagement.init(s1).deleteServiceChargeAllVersions(serviceCharge);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.PVG_UAP})
    public void serviceChargeAdd() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC028: serviceChargeAdd", "To verify that valid user can add service charge.");

            t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_UAP,
                    FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

            //This will initiate and approve Service charge
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(serviceCharge);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void serviceChargeView() throws Exception {

        ExtentTest t1 = pNode.createNode("TC029: serviceChargeView", "To verify that valid user can view service charge.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(serviceCharge);
            ServiceChargeManagement.init(t1)
                    .viewServiceChargesLatestVersion(serviceCharge, lastDaystoViewSC);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP})
    public void serviceChargeModify() throws Exception {
        ExtentTest t1 = pNode.createNode("TC030: serviceChargeModify", "To verify that valid user can Modify service charge.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);
        try {
            Login.init(t1).login(usrSChargeModify);
            ServiceChargeManagement.init(t1)
                    .modifyServiceChargeGeneralInfo(serviceCharge)
                    .approveServiceCharge(serviceCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.SERVICE_CHARGE_MANAGEMENT,
            FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void serviceChargeSuspend() throws Exception {

        ExtentTest t1 = pNode.createNode("TC031: serviceCharge Suspend/Resume", "To verify that Nwadmin can Suspend/Resume service charge.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);


        try {
            Login.init(t1).login(usrSChargeModify);
            //to suspend Service Charge
            ServiceChargeManagement.init(t1)
                    .suspendServiceCharge(serviceCharge);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            //to resume Service Charge
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(serviceCharge);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP})
    public void setSubsCommRule() throws Exception {
        ExtentTest t1 = pNode.createNode("TC032: setSubsCommRule",
                "To verify that valid user can configure New subscriber commision rule.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT);

        try {
            Login.init(t1).login(subsCommRuleCreater);

            ServiceChargeManagement.init(t1)
                    .addNewSubsCommissionRule(serviceCharge.ServiceChargeName, "999", "365");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void serviceChargeDelete() throws Exception {

        ExtentTest t1 = pNode.createNode("TC032: serviceChargeDelete", "To verify that valid user can Delete service charge.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(usrSChargeModify);

            ServiceChargeManagement.init(t1)
                    .deleteServiceCharge(serviceCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(serviceCharge);
        }
        Assertion.finalizeSoftAsserts();


    }

}


