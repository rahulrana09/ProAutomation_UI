package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.globalSearch.GlobalSearch;
import framework.pageObjects.globalSearch.GlobalSearch_page1;
import framework.pageObjects.globalSearch.GlobalSearch_page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Created by Naveen.Karkra on 25-Nov-2017.
 */
public class SuiteSystemGlobalSearch01 extends TestInit {


    private OperatorUser common_optUser;
    private User common_chUser;
    private ResultSet channelUserDetails;
    private String ora_date_of_creation, ora_date_of_modification, ora_last_login;

    @BeforeMethod(alwaysRun = true)
    public void before_method() throws Exception {
        try {
            common_optUser = DataFactory.getOperatorUserWithAccess("RP_GOS");
            common_chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            channelUserDetails = MobiquityGUIQueries.fetchDetailsForGlobalSearch(common_chUser.MSISDN);
            while (channelUserDetails.next()) {
                ora_date_of_creation = channelUserDetails.getString("CREATED_ON");
                ora_date_of_modification = channelUserDetails.getString("MODIFIED_ON");
                ora_last_login = channelUserDetails.getString("LAST_LOGIN_ON");
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0001
     * TEST DESCRIPTION : To verify Global search can be performed successfully for all user types and grades
     *
     * @throws Exception Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.MONEY_SMOKE})
    public void Sys_TC_Global_Search_N2_0001() throws Exception {


        List<OperatorUser> gs_optUsers = DataFactory.getOperatorUsersWithAccess("RP_GOS");

        Map<String, OperatorUser> all_optUsers = DataFactory.getOperatorUserSet();

        Map<String, User> chUsers = DataFactory.getChannelUserSet(true);

        for (OperatorUser opt : gs_optUsers) {
            System.out.println("Operator User Category :" + opt.MSISDN);

            // Doing Global Search for all CUs and SUBs
            for (User user : chUsers.values()) {
                ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0001 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + opt.CategoryName + "\" user can perform \"Global search\" successfully for \"" + user.GradeName + "\".");

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

                //System.out.println("Users CatCode:"+user.CategoryCode +" and MSISDN :"+user.MSISDN+"  "+user.GradeName);
                Login.init(t1).login(opt);
                GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_MSISDN, user.MSISDN);
            }

            // Doing Global Search for all OUs
            for (OperatorUser operatorUser : all_optUsers.values()) {
                ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0001 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + opt.CategoryName + "\" user can perform \"Global search\" successfully for \"" + operatorUser.CategoryName + "\".");

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

                Login.init(t1).login(opt);

                GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_MSISDN, operatorUser.MSISDN);
            }
        }
    }


    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0053
     * TEST DESCRIPTION : To verify Global search can be performed successfully using LOGINID
     *
     * @throws Exception (Exception)
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.MONEY_SMOKE})
    public void Sys_TC_Global_Search_N2_0053() throws Exception {


        //System.out.println( gs_optUser.CategoryName +"   "+ chUser.GradeName);
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0053 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + DataFactory.getCategoryName(common_optUser.CategoryCode) + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using LOGIN ID.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        try {
            //System.out.println("Users CatCode:"+user.CategoryCode +" and MSISDN :"+user.MSISDN+"  "+user.GradeName);
            Login.init(t1).login(common_optUser);
            GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LOGINID, common_chUser.LoginId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0054
     * TEST DESCRIPTION : To verify Global search can be performed successfully using MSISDN
     *
     * @throws Exception Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0054() throws Exception {

        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0054 : Global Search ", "To verify that the \"" + DataFactory.getCategoryName(common_optUser.CategoryCode) + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using MSISDN.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        //System.out.println("Users CatCode:"+user.CategoryCode +" and MSISDN :"+user.MSISDN+"  "+user.GradeName);
        Login.init(t1).login(common_optUser);

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_MSISDN, common_chUser.MSISDN);

    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0055
     * TEST DESCRIPTION : To verify Global search can be performed successfully using FIRSTNAME
     *
     * @throws Exception Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0055() throws Exception {

        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0055 :", "To verify that the \"" + DataFactory.getCategoryName(common_optUser.CategoryCode) + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using FIRST NAME.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_FIRSTNAME, common_chUser.FirstName);
    }


    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0056a
     * TEST DESCRIPTION : To verify Global search can be performed successfully using LASTNAME
     *
     * @throws Exception Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0056() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0056 : Global Search ",
                "To verify that the \"" + DataFactory.getCategoryName(common_optUser.CategoryCode)
                        + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using LAST NAME.").assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LASTNAME, common_chUser.LastName);

    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0057
     * TEST DESCRIPTION : To verify Global search can be performed successfully using "Created On"
     *
     * @throws Exception (If any Exception then throw exception)
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0057() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0057 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + common_optUser.CategoryName + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using \"Date of Creation\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        String gui_date_of_creation = DateAndTime.convertOracleDateToGUIDateFormat(ora_date_of_creation);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_DATE_OF_CREATION, common_chUser, gui_date_of_creation);

    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0058
     * TEST DESCRIPTION : To verify Global search can be performed successfully using "Modified On"
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0058() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0058 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + common_optUser.CategoryName + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using \"Date of Modification\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        String gui_date_of_modification = DateAndTime.convertOracleDateToGUIDateFormat(ora_date_of_modification);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_DATE_OF_MODIFICATION, common_chUser, gui_date_of_modification);

    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0059
     * TEST DESCRIPTION : To verify Global search can be performed successfully using "Last Transaction On"
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0059() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0059", "To verify that the \"" + common_optUser.CategoryName + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using \"Date of Modification\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        String ora_last_txn_on = MobiquityGUIQueries.getLastTransactionOnFromTxnHeader(common_chUser.MSISDN);
        String gui_last_txn_on = DateAndTime.convertOracleDateToGUIDateFormat(ora_last_txn_on);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN, common_chUser, gui_last_txn_on);

    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0060
     * TEST DESCRIPTION : To verify Global search can be performed successfully using "Last Login"
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0060() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0060 " + getTestcaseid() + ": Global Search ", "To verify that the \"" + common_optUser.CategoryName + "\" user can perform \"Global search\" successfully for \"" + common_chUser.GradeName + "\" using \"Date of Modification\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        String gui_last_login = DateAndTime.convertOracleDateToGUIDateFormat(ora_last_login);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LAST_LOGIN, common_chUser, gui_last_login);
    }

    /**
     * TEST TYPE : Positive
     * TEST CASE ID : Sys_TC_Global_Search_N2_0061
     * TEST DESCRIPTION : To verify that all field values at "Global Search" page should get reset when "Reset" button is clicked.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH}, enabled = false)
    public void Sys_TC_Global_Search_N2_0061() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0061 " + getTestcaseid() + ": Global Search ",
                "To verify that all field values at \"Global Search\" page should get reset when \"Reset\" button is clicked.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearchButtonTest(Button.RESET, common_chUser.MSISDN);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0062
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when "Submit" button is clicked without entering value for any mandatory field.
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0062() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0062 " + getTestcaseid() + ": Global Search ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when \"Submit\" button is clicked without entering value for any mandatory field.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);
        common_chUser.MSISDN = Constants.BLANK_CONSTANT;

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearchButtonTest(Button.SUBMIT);

        Assertion.verifyErrorMessageContain("systemparty.error.oneofchoice", "Verify Error Message when Mandatory fields left Blank.", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0063
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when "Login ID" entered does not exist in the system e.g. !@#$%
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0063() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0063 " + getTestcaseid() + ": Global Search ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when \"Login ID\" entered does not exist in the system e.g. !@#$%");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);
        common_chUser.LoginId = Constants.SPECIAL_CHARACTER_CONSTANT;

        Login.init(t1).login(common_optUser);

        startNegativeTestWithoutConfirm();

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LOGINID, common_chUser);

        Assertion.verifyErrorMessageContain("blacklist.invalidloginId", "Verify Error Message when Invalid Login ID Entered", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0064
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when two options are used simulataneously to search user e.g. "Login ID" and "MSISDN".
     *
     * @throws Exception
     */

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0064() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0064 " + getTestcaseid() + ": Global Search ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when two options are used simulataneously to search user e.g. \"Login ID\" and \"MSISDN\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);
        GlobalSearch_page1 page1 = new GlobalSearch_page1(t1);
        page1.navigateToLink();
        page1.setLoginID(common_chUser.LoginId);
        page1.setmsisdn(common_chUser.MSISDN);
        page1.clickSubmit();

        Assertion.verifyErrorMessageContain("systemparty.error.enteronlyone", "Verify Error Message when Both MSISDN and LoginID entered.", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0065
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when "MSISDN" entered has invalid length e.g. 923.
     *
     * @throws Exception
     */

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0065() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0065 " + getTestcaseid() + ": Global Search ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when \"MSISDN\" entered has invalid length e.g. 923");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);
        common_chUser.MSISDN = "923";

        Login.init(t1).login(common_optUser);

        startNegativeTestWithoutConfirm();

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_MSISDN, common_chUser);

        Assertion.verifyErrorMessageContain("global.search.invalid.msisdn.length", "Verify Error Message when Invalid MSISDN length Entered.", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0067
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when "First Name" entered does not exist in the system e.g. !@#$%
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0067() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0067 " + getTestcaseid() + ": Global Search ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when \"First Name\" entered does not exist in the system e.g. !@#$%");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);
        common_chUser.FirstName = Constants.SPECIAL_CHARACTER_CONSTANT;

        Login.init(t1).login(common_optUser);

        startNegativeTestWithoutConfirm();

        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_FIRSTNAME, common_chUser);

        Assertion.verifyErrorMessageContain("blacklist.invalidfirstName", "Verify Error Message when First Name does not Exists.", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0068
     * TEST DESCRIPTION : To verify that the "Network Admin" user should get proper error message at "Global Search" page when "Last Name" entered does not exist in the system e.g. !@#$%
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0068() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0068 ",
                "To verify that the \"Network Admin\" user should get proper error message at \"Global Search\" page when \"Last Name\" entered does not exist in the system e.g. !@#$%");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);
        common_chUser.LastName = Constants.SPECIAL_CHARACTER_CONSTANT;

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LASTNAME, common_chUser);

        Assertion.verifyErrorMessageContain("blacklist.invalidlastName", "Verify Error Message when Last Name does not Exists.", t1);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0069
     * TEST DESCRIPTION : To verify that all previous page should be displayed when "Back" button is clicked at "Global Search -> Results" page.
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0069() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0069",
                "To verify that all previous page should be displayed when \"Back\" button is clicked at \"Global Search -> Results\" page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearchButtonTest(Button.BACK, common_chUser.MSISDN);
    }

    /**
     * TEST TYPE : Negative
     * TEST CASE ID : Sys_TC_Global_Search_N2_0070
     * TEST DESCRIPTION : To verify that all previous page should be displayed when "Back" button is clicked at "Global Search -> User Details" page.
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH})
    public void Sys_TC_Global_Search_N2_0070() throws Exception {
        ExtentTest t1 = pNode.createNode("Sys_TC_Global_Search_N2_0070",
                "To verify that all previous page should be displayed when \"Back\" button is clicked at \"Global Search -> User Details\" page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH);

        Login.init(t1).login(common_optUser);
        GlobalSearch.init(t1).globalSearchButtonTest(Button.SUBMIT, common_chUser.MSISDN);
        GlobalSearch_page2 page2 = new GlobalSearch_page2(t1);
        page2.clickUserDetailsPageBackButton();
        page2.isSubmit1ButtonDisplayed();
    }

}



