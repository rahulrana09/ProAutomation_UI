package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_UAP_SVAtoBank_01 extends TestInit {
    private String provider, walletName, bankName;
    private User user;

    /**
     * SETUP
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s = pNode.createNode("Setup", "Setup specific for this Script");
        try {
            provider = GlobalData.defaultProvider.ProviderName;
            walletName = GlobalData.defaultWallet.WalletName;
            bankName = GlobalData.defaultBankName;
            user = DataFactory.getChannelUserWithAccess("SVATOBANK");
            ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, user, user, null, null, null, null);
            /*TransferRuleManagement.init(s)
                    .configureTransferRule(sCharge);*/
            TransactionManagement.init(s)
                    .makeSureChannelUserHasBalance(user, new BigDecimal(250));
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test Method to perform SVA to own Bank Transfer.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.SVA, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM})
    public void svaNewTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0148 : SVA New",
                "To verify that the valid user can perform SVA to own bank transfer.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        try {
            TransactionManagement.init(t1)
                    .initiateScheduledSVAtoBank(user, provider, walletName, bankName,
                            "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

/*
    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP})
    public void svaSchTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC091 : SVA", "To verify that the valid user can View/modify/stop/delete scheduled transfers.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.AUDIT_TRAIL);

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(user,Constants.SVA_SCHEDULE_LATER, provider, walletName, bankName, "Monday");
    }*/

    /**
     * Test Method to view SVA to Bank scheduled transfers
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP, FunctionalTag.SAV, FunctionalTag.PVG_SYSTEM})
    public void svaView() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0147 : SVA View",
                "To verify that the valid user can View/modify/stop/delete scheduled transfers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        try {
            TransactionManagement.init(t1).
                    viewSVAtoBank(user);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test Method to Modify SVA to Bank Scheduled Transfer.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.SVA, FunctionalTag.PVG_SYSTEM})
    public void svaMod() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0146 : SVA Modify",
                "To verify that the valid user can View/modify/stop/delete scheduled transfers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        try {
            TransactionManagement.init(t1)
                    .ModifySVAtoBank(user, "Tuesday");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    //@AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        TransactionManagement.init(pNode).SVA_Schedule_Delete(user);
    }
}
