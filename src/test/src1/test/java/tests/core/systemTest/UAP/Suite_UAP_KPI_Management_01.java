package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.kpi.KPIManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for KPIManagement
 *
 * @author navin.pramanik
 */
public class Suite_UAP_KPI_Management_01 extends TestInit {

    OperatorUser netStat;
    private String providerID, fromDate, toDate;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            providerID = DataFactory.getDefaultProvider().ProviderId;
            fromDate = new DateAndTime().getDate(-30);
            toDate = new DateAndTime().getDate(0);
            netStat = DataFactory.getOperatorUserWithAccess("KPIR");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.MONEY_SMOKE}, priority = 1)
    public void kpiUsingCurrentDate() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0082\n :KPIManagement-> Current Date", "To verify that the valid user can check KPIManagement(Current Date) results.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.KPI);

        try {
            Login.init(t1).login(netStat);
            KPIManagement.init(t1).performKPIUsingCurrentDate(providerID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.MONEY_SMOKE}, priority = 2)
    public void kpiUsingPreviousDate() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0083\n :KPIManagement-> Previous Date", "To verify that the valid user can check KPIManagement(Previous Date) results.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.KPI);

        try {
            Login.init(t1).login(netStat);
            KPIManagement.init(t1).performKPIUsingPreviousDate(providerID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.MONEY_SMOKE}, priority = 3)
    public void kpiUsingDaterange() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0084\n :KPIManagement-> Date Range", "To verify that the valid user can check KPIManagement(Date Range) results.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.KPI);

        try {
            Login.init(t1).login(netStat);
            KPIManagement.init(t1).performKPIUsingDateRange(providerID, fromDate, toDate);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
