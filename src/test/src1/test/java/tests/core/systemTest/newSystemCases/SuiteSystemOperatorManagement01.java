/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 * Author Name: navin.pramanik
 *  Date: 30-Dec-2017
 *  Purpose: Operator User Management Related Cases (For System Test)
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.net.InetAddress;
import java.util.List;

public class SuiteSystemOperatorManagement01 extends TestInit {

    private static String refNo;
    private ExtentTest tearDown;
    private OperatorUser optUser, optBankUser, optCA, optCCE, optNA, optBA;
    private OperatorUser usrCreator, usrModify, usrModifyApprove, usrDeleteApproval, usrApprove, bankUserCreate, bankUserApprove, bankUserModifier;
    private SuperAdmin saMaker, saChecker, saModifier, saModifyApprover;
    private String DEFAULT_VALUE_IS_RANDOM_PASS_ALLOW;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {

            tearDown = pNode.createNode("TearDown", "Tear Down");
            refNo = DataFactory.getTimeStamp();
            usrCreator = DataFactory.getOperatorUsersWithAccess("PTY_ASU", Constants.NETWORK_ADMIN).get(0);
            usrApprove = DataFactory.getOperatorUsersWithAccess("PTY_ASUA", Constants.NETWORK_ADMIN).get(0);
            usrModify = DataFactory.getOperatorUsersWithAccess("PTY_MSU", Constants.NETWORK_ADMIN).get(0);
            usrModifyApprove = DataFactory.getOperatorUsersWithAccess("PTY_MSUAP", Constants.NETWORK_ADMIN).get(0);
            usrDeleteApproval = DataFactory.getOperatorUsersWithAccess("PTY_ASUD", Constants.NETWORK_ADMIN).get(0);

            bankUserCreate = DataFactory.getOperatorUsersWithAccess("PTY_ASU", Constants.BANK_ADMIN).get(0);
            bankUserApprove = DataFactory.getOperatorUsersWithAccess("PTY_ASUA", Constants.BANK_ADMIN).get(0);
            bankUserModifier = DataFactory.getOperatorUsersWithAccess("PTY_ASU", Constants.BANK_ADMIN).get(0);

            saMaker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            saChecker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            saModifier = DataFactory.getSuperAdminWithAccess("PTY_MSU");
            saModifyApprover = DataFactory.getSuperAdminWithAccess("PTY_MSUAP");

            DEFAULT_VALUE_IS_RANDOM_PASS_ALLOW = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PASS_ALLOW");

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() throws Exception {
        optUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        optBankUser = new OperatorUser(Constants.BANK_USER);
        optCA = new OperatorUser(Constants.CHANNEL_ADMIN);
        optCCE = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
        optNA = new OperatorUser(Constants.NETWORK_ADMIN);
        optBA = new OperatorUser(Constants.BANK_ADMIN);
    }

    /**
     * TYPE : POSITIVE
     * ID : P1_TC_477
     * DESC : To verify that Bank Admin can initiate the Bank User creation request.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 1)
    public void P1_TC_477() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_477", "To verify that Bank Admin can initiate the Bank User creation request.");
        try {
            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT}, priority = 2)
    public void P1_TC_478() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_478", "To verify that Bank Admin can approve the initiation request of the Bank User creation");
        try {
            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT);
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);
            Login.init(t1).login(bankUserApprove);
            obj.approveOperatorUser(optBankUser);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 3)
    public void P1_TC_118() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_118", "To verify that Bank Admin can reject the initiated Bank User creation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);
            Login.init(t1).login(bankUserApprove);
            obj.approveOperatorUser(optBankUser, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 4)
    public void P1_TC_119() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_119", "To verify that Bank Admin can initiate modification of an existing Bank User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optBankUser)
                    .approveOperatorUser(optBankUser);//.changeFirstTimePasswordOpt(optBankUser);

            Login.init(t1).login(bankUserModifier);
            optBankUser.LastName = "Modified";
            obj.modifyOperatorUser(optBankUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT}, priority = 5)
    public void P1_TC_120() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_120", "To verify that Bank Admin can approve modification request of a Bank User.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT);
        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser).approveOperatorUser(optBankUser);

            Login.init(t1).login(bankUserModifier);
            optBankUser.LastName = "Modified";
            obj.modifyOperatorUser(optBankUser);

            Login.init(t1).login(bankUserApprove);
            obj.approveModifyOperatorUser(optBankUser, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 6)
    public void P1_TC_121() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_121", "To verify that Bank Admin can reject modification request of a Bank User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser).approveOperatorUser(optBankUser);

            Login.init(t1).login(bankUserModifier);
            optBankUser.LastName = "Modified";
            obj.modifyOperatorUser(optBankUser);

            Login.init(t1).login(bankUserApprove);
            obj.approveModifyOperatorUser(optBankUser, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 7)
    public void P1_TC_122() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_122", "To verify that Bank Admin can initiate deletion of an existing Bank User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_5_0);
        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);

            Login.init(t1).login(bankUserApprove);
            obj.approveOperatorUser(optBankUser);

            Login.init(t1).login(bankUserModifier);
            obj.initModifyOrDeleteOperator(optBankUser, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT}, priority = 8)
    public void P1_TC_123() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_123", "To verify that Bank Admin can approve the initiate deletion request of an existing Bank User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.MULTIPLE_BANK_TRUST_ACCOUNT);

        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);

            Login.init(t1).login(bankUserApprove);
            obj.approveOperatorUser(optBankUser);

            Login.init(t1).login(bankUserModifier);
            obj.initModifyOrDeleteOperator(optBankUser, false);

            Login.init(t1).login(bankUserApprove);
            obj.approveDeletionOperatorUser(optBankUser, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 9)
    public void SYS_TC_OPERATOR_MGMT_P_0009() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0009", "To verify that Bank admin can REJECT the initiated Bank user DELETION  request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);

        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement obj = OperatorUserManagement.init(t1).initiateOperatorUser(optBankUser);

            Login.init(t1).login(bankUserApprove);
            obj.approveOperatorUser(optBankUser);

            Login.init(t1).login(bankUserModifier);
            obj.initModifyOrDeleteOperator(optBankUser, false);

            Login.init(t1).login(bankUserApprove);
            obj.approveDeletionOperatorUser(optBankUser, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 10)
    public void P1_TC_124() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_124", "To verify that Bank Admin can view his details .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).login(bankUserCreate);
            OperatorUserManagement.init(t1).viewSelfDetails(bankUserCreate);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 11)
    public void P1_TC_125() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_125", "To verify that at the time of modification of operator user network admin," +
                "application ask for new password when random password preference is set as False.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saModifier);

            optNA = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            //Check if Random Password is allowed or not , and if allowed then Set it to "N"
            if (AppConfig.isRandomPasswordAllowed) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_RANDOM_PASS_ALLOW", "N");
            }
            new ModifyOperatorUser_page1(t1).fillFirstPageDetails(optNA);

            boolean isPasswordFieldDisplayed = new ModifyOperatorUser_page2(t1).checkPasswordFieldIsDisplayed();

            Assertion.verifyEqual(isPasswordFieldDisplayed, true, "Check-Password-Field-Is-Displayed", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_RANDOM_PASS_ALLOW", "N");
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 12)
    public void SYS_TC_OPERATOR_MGMT_P_0011() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0011", "To verify that at the time of modification of operator user(network admin/bank admin),application ask for new password when random password preference is set as False.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.modifyOperatorUser(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * //TODO Complete the Test Case
     * //Assertion is Pending
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 13, enabled = false)
    public void P1_TC_126() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_126", "To verify that messages regarding the transactions are displayed " +
                "in that language which is choose at the time of user's creation");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        try {
            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            TransactionManagement.init(t1)
                    .makeSureLeafUserHasBalance(subs, new BigDecimal(Constants.MIN_TRANSACTION_AMT));
            String message = MobiquityGUIQueries.getMessageFromSentSMS(subs.MSISDN, "desc");

            //verify  Messages
            Assertion.verifyMessageContain(message, "",
                    "Check language of sms", t1);

            String msg = SMSReader.getLatestMessageFromNotificationUrl("7710711972", t1);

            t1.info("Message Found: " + msg);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 14)
    public void P1_TC_446() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_446", "To verify that Network Admin can log in the system according to the Allowed IP's provided by Super admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.setIsAllowedIpTrue();
            optNA.isAllowedIp = true;
            InetAddress sys = InetAddress.getLocalHost();
            optNA.allowedIP = sys.getHostAddress().trim();
            OperatorUserManagement.init(t1).initiateOperatorUserWithFull(optNA);
            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);
            Login.init(t1).tryLogin(optNA.LoginId, optNA.Password);
            Login.resetLoginStatus();


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 15)
    public void P1_TC_447() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_447", "To verify that Network/Bank Admin can not log in the Operator other than the defined time by the super admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.setIsDateAndTimeTrue();
            optNA.setIsAllowedIpTrue();
            InetAddress sys = InetAddress.getLocalHost();
            optNA.allowedIP = sys.getHostAddress().trim();
            OperatorUserManagement.init(t1).initiateOperatorUserWithFull(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);
            Login.init(t1).tryLogin(optNA.LoginId, optNA.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("loginevents.user.timingsnotallow", "Login not allowed", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 16)
    public void P1_TC_462() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_462", "To verify that super admin can initiate the operator user creation for Network Admin.");
        try {

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.PRASHANT);

            Login.init(t1).loginAsSuperAdmin(saMaker);

            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);


        } catch (Exception e) {

            markTestAsFailure(e, t1);

        } finally {

            Assertion.finalizeSoftAsserts();

        }

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 17)
    public void P1_TC_462_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_462_2", "To verify that super admin can initiate the operator user creation for Bank Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 18)
    public void P1_TC_463_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_463_1", "To verify that super admin can approve add initiation request of Network Admin Creation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 19)
    public void P1_TC_463_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_463_2", "To verify that super admin can approve add initiation request of Bank Admin creation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 20)
    public void P1_TC_464_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_464_1", "To verify that super admin can REJECT the operator user CREATION request for Network admin.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 21)
    public void P1_TC_464_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_464_2", "To verify that super admin can REJECT the operator user CREATION request for BANK admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 22)
    public void P1_TC_465() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_465", "To verify that super admin can INITIATE the operator user MODIFICATION for Network Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).modifyOperatorUser(optNA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 23)
    public void P1_TC_465_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_465_2", "To verify that super admin can INITIATE the operator user MODIFICATION for Bank Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).modifyOperatorUser(optBA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 24)
    public void P1_TC_466_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_466_1", "To verify that super admin can REJECT the initiated Network admin MODIFICATION request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).modifyOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optNA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 25)
    public void P1_TC_466_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_466_2", "To verify that super admin can REJECT the initiated Bank admin MODIFICATION request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).modifyOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optBA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 26)
    public void P1_TC_467_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_467_1", "To verify that super admin can INITIATE the operator user DELETION for Network Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

        try {
            OperatorUserManagement.init(t1).createAdmin(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optNA, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(tearDown).approveDeletionOperatorUser(optNA, true);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 27)
    public void P1_TC_467_1_a() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_467_b", "To verify that super admin can REJECT the operator user DELETION for Network Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 28)
    public void P1_TC_467_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_467_2", "To verify that super admin can INITIATE the operator user DELETION for Bank Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optBA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 29)
    public void P1_TC_467_2_b() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_467_b", "To verify that super admin can REJECT the operator user DELETION for Bank Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 30)
    public void P1_TC_468_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_468_1", "To verify that super admin can APPROVE the operator user DELETION  request for Network admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optNA, false);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optNA, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 31)
    public void P1_TC_468_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_468_2", "To verify that super admin can APPROVE the operator user DELETION  request for Bank admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optBA, false);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optBA, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 32)
    public void SYS_TC_OPERATOR_MGMT_P_0023() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0023", "To verify that super admin can REJECT the initiated Network admin DELETION  request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optNA, false);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optNA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 33)
    public void SYS_TC_OPERATOR_MGMT_P_0032() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0032", "To verify that super admin can REJECT the initiated Bank admin DELETION  request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optBA);

            Login.init(t1).loginAsSuperAdmin(saModifier);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optBA, false);

            Login.init(t1).loginAsSuperAdmin(saModifyApprover);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optBA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 34)
    public void P1_TC_469_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_469_1", "To verify that super admin can view self details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            optUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).viewSelfDetails(saMaker);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 35)
    public void P1_TC_469_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_469_2", "To verify that Network admin can view self details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            OperatorUser optUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            Login.init(t1).login(optUser);

            OperatorUserManagement.init(t1).viewSelfDetails(optUser);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 36)
    public void P1_TC_469_3() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_469_3", "To verify that Channel admin can view self details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            optUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(optUser);
            OperatorUserManagement.init(t1).viewSelfDetails(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 37)
    public void P1_TC_469_4() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_469_4", "To verify that CCE  can view self details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            optUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(optUser);
            OperatorUserManagement.init(t1).viewSelfDetails(optUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 38)
    public void P1_TC_470_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_470_1", "To verify that admin admin can INITIATE the operator user CREATION for Channel Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 39)
    public void P1_TC_470_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_470_2", "To verify that admin admin can INITIATE the operator user CREATION for CCE Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 40)
    public void P1_TC_471_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_471_1", "To verify that Network admin can Reject the operator user CREATION request for Channel admin.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0).assignAuthor("Navin Kumar Pramanik");

        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 41)
    public void P1_TC_471_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_471_2", "To verify that Network admin can Reject the operator user CREATION request for CCE admin.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 42)
    public void P1_TC_472_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_472_1", "To verify that Network admin can INITIATE the operator user MODIFICATION for Channel Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 43)
    public void P1_TC_472_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_472_2", "To verify that Network admin can INITIATE the operator user MODIFICATION for CCE Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCCE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 44)
    public void P1_TC_473_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_473_1", "To verify that Network admin can APPROVE the operator user MODIFICATION request for Channel admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCA);

            Login.init(t1).login(usrModifyApprove);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optCA, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 45)
    public void P1_TC_473_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_473_2", "To verify that Network admin can APPROVE the operator user MODIFICATION request for CCE admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCCE);

            Login.init(t1).login(usrModifyApprove);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optCCE, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 46)
    public void P1_TC_474_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_474_1", "To verify that Network admin can REJECT the initiated Channel admin MODIFICATION request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCA);

            Login.init(t1).login(usrModifyApprove);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optCA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0}, priority = 47)
    public void P1_TC_474_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_474_2", "To verify that Network admin can REJECT the initiated CCE admin MODIFICATION request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).modifyOperatorUser(optCCE);

            Login.init(t1).login(usrModifyApprove);
            OperatorUserManagement.init(t1).approveModifyOperatorUser(optCCE, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 48)
    public void P1_TC_475_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_475_1", "To verify that Network admin can INITIATE the operator user DELETION  for Channel Admin.To verify that Network admin can INITIATE the operator user DELETION  for Channel Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1}, priority = 49)
    public void P1_TC_475_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_475_2", "To verify that Network admin can INITIATE the operator user DELETION  for CCE Admin.To verify that Network admin can INITIATE the operator user DELETION  for Channel Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCCE, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 50)
    public void P1_TC_476_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_476_1", "To verify that Network admin can APPROVE the operator user DELETION  request for Channel admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCA, false);

            Login.init(t1).login(usrDeleteApproval);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optCA, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0}, priority = 51)
    public void P1_TC_476_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_476_2", "To verify that Network admin can APPROVE the operator user DELETION  request for CCE admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCCE, false);

            Login.init(t1).login(usrDeleteApproval);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optCCE, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 52)
    public void SYS_TC_OPERATOR_MGMT_P_0046() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0046", "To verify that Network admin can REJECT the initiated Channel admin DELETION  request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCA);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCA);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCA, false);

            Login.init(t1).login(usrDeleteApproval);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optCA, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 53)
    public void SYS_TC_OPERATOR_MGMT_P_0055() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0055", "To verify that Network admin can REJECT the initiated CCE admin DELETION  request.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).initiateOperatorUser(optCCE);

            Login.init(t1).login(usrApprove);
            OperatorUserManagement.init(t1).approveOperatorUser(optCCE);

            Login.init(t1).login(usrModify);
            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(optCCE, false);

            Login.init(t1).login(usrDeleteApproval);
            OperatorUserManagement.init(t1).approveDeletionOperatorUser(optCCE, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 54)
    public void SYS_TC_OPERATOR_MGMT_P_0056() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0056", "To verify that proper error message should display when Super admin entered a value with space (e.g like this) in Division field");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Division = "South Delhi";
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.division", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 55)
    public void SYS_TC_OPERATOR_MGMT_P_0057() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0057", "To verify that proper error message should display when Super admin entered a value with space (e.g like this) in Department field");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Dept = "South Delhi";
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.department", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 56)
    public void SYS_TC_OPERATOR_MGMT_P_0058() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0058", "To verify that proper error message should display when Super admin entered a value with space (e.g like this) in WebloginID field");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.LoginId = "Login ID";
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.loginAccessidWhitespace", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 57)
    public void SYS_TC_OPERATOR_MGMT_P_0059() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0059", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in First Name field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.FirstName = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 58)
    public void SYS_TC_OPERATOR_MGMT_P_0060() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0060", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Last Name field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.LastName = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.error.lastnameformat", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 59)
    public void SYS_TC_OPERATOR_MGMT_P_0061() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0061", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in MSISDN field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.MSISDN = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.msisdnnumeric", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 60)
    public void SYS_TC_OPERATOR_MGMT_P_0062() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0062", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Identification Number field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ExternalCode = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.alphaexternalCode", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 61)
    public void SYS_TC_OPERATOR_MGMT_P_0063() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0063", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Email id field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Email = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.emailId.format", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 62)
    public void SYS_TC_OPERATOR_MGMT_P_0064() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0064", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Division field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Division = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.division", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 63)
    public void SYS_TC_OPERATOR_MGMT_P_0065() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0065", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Department field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Dept = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.department", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 64)
    public void SYS_TC_OPERATOR_MGMT_P_0066() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0066", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Contact Number field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ContactNum = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 65)
    public void SYS_TC_OPERATOR_MGMT_P_0067() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0067", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Login ID field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.LoginId = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.loginAccessidWhitespace", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 66)
    public void SYS_TC_OPERATOR_MGMT_P_0068() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0068", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Password field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Password = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("changePassword.error.passwordsNotSame", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 67)
    public void SYS_TC_OPERATOR_MGMT_P_0069() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0069", "To verify that Super Admin should not be able to Create Network Admin in the system when special characters are entered in Confirm Password field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ConfirmPass = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("changePassword.error.passwordsNotSame", "Assert Error Message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 68)
    public void SYS_TC_OPERATOR_MGMT_P_0076() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0076", "To verify that super admin receive a proper error message when First Name field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.FirstName = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.spaceusername", "Assert Error Message First Name", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 69)
    public void SYS_TC_OPERATOR_MGMT_P_0077() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0077", "To verify that super admin receive a proper error message when Last Name field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.LastName = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.lastnamerequired", "Assert Error Message Last Name", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 70)
    public void SYS_TC_OPERATOR_MGMT_P_0078() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0078", "To verify that super admin receive a proper error message when MSISDN field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.MSISDN = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.msisdnrequired", "Assert Error Message MSISDN", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 71)
    public void SYS_TC_OPERATOR_MGMT_P_0079() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0079", "To verify that super admin receive a proper error message when Identification Number field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ExternalCode = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.externalCode.required", "Assert Error Message Identification Number", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 72)
    public void SYS_TC_OPERATOR_MGMT_P_0080() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0080", "To verify that super admin receive a proper error message when Email id field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Email = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.emailId", "Assert Error Message Email id", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 73)
    public void SYS_TC_OPERATOR_MGMT_P_0081() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0081", "To verify that super admin receive a proper error message when Division field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Division = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.division", "Assert Error Message Division", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 74)
    public void SYS_TC_OPERATOR_MGMT_P_0082() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0082", "To verify that super admin receive a proper error message when Department field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Dept = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.department", "Assert Error Message Department", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 75)
    public void SYS_TC_OPERATOR_MGMT_P_0083() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0083", "To verify that super admin receive a proper error message when Contact Number field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ContactNum = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 76)
    public void SYS_TC_OPERATOR_MGMT_P_0084() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0084", "To verify that super admin receive a proper error message when Login ID field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.LoginId = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 77)
    public void SYS_TC_OPERATOR_MGMT_P_0085() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0085", "To verify that super admin receive a proper error message when Password field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Password = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.password", "Assert Error Message Password", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 78)
    public void SYS_TC_OPERATOR_MGMT_P_0086() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0086", "To verify that super admin receive a proper error message when Confirm Password field left blank while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ConfirmPass = Constants.BLANK_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.confirmpassword", "Assert Error Message Confirm Password", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 79)
    public void SYS_TC_OPERATOR_MGMT_P_0087() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0087", "To verify that super admin receive a proper error message when invalid data is entered in MSISDN while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.MSISDN = Constants.ALPHABET_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.MSISDN", "Assert Error Message MSISDN", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 80)
    public void SYS_TC_OPERATOR_MGMT_P_0088() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0088", "To verify that super admin receive a proper error message when invalid data is entered in Email id while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.Email = optNA.Email.replace("@", "");
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.EmailID", "Assert Error Message Email id", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 81)
    public void SYS_TC_OPERATOR_MGMT_P_0089() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0089", "To verify that super admin receive a proper error message when invalid data is entered in Contact Number while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.ContactNum = Constants.ALPHABET_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.ContactNumber", "Assert Error Message Contact Number", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 82)
    public void SYS_TC_OPERATOR_MGMT_P_0090() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0090", "To verify that back button functionality is working on initiation page while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(t1);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(t1);
            p1.NavigateNA();
            p1.selectUserTypeByValue(optNA.CategoryCode);
            p1.clickSubmitButton();
            p2.clickOnBackButton();
            p1.checkSubmitButtonIsavailable();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 83)
    public void SYS_TC_OPERATOR_MGMT_P_0091() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0091", "To verify that reset button functionality is working on initiation page while creating Network admin");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(t1);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(t1);
            p1.NavigateNA();
            p1.selectUserTypeByValue(optNA.CategoryCode);
            p1.clickSubmitButton();
            String before = p2.getFirstName();
            p2.firstName_SetText(optNA.FirstName);
            p2.clickOnResetButton();
            String after = p2.getFirstName();
            Assertion.verifyContains(before, after, "Assert Reset Button", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 84)
    public void SYS_TC_OPERATOR_MGMT_P_0092() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0092", "To verify that Back button functionality is working on confirmation page while initiation page");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUser(optNA);
            AddOperatorUser_pg2.init(t1).clickOnBackBtn();
            AddOperatorUser_pg2.init(t1).checkFirstnameIsAvailable();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 85)
    public void SYS_TC_OPERATOR_MGMT_P_0093() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0093", "To verify that back button functionality is working on approval page of operator user approval");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            ApproveOperatorUser_pg1 ap1 = ApproveOperatorUser_pg1.init(t1);
            ap1.Navigate();
            ap1.loginID_SetText(optNA.LoginId);
            ap1.clickSubmitPage1();

            //approve page 2
            ap1.clickOnBackButton();

            ap1.checkSubmitButtonIsAvailable();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 86)
    public void SYS_TC_OPERATOR_MGMT_P_0094() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0094", "To verify that super admin can not create a network admin with same MSISDN that is already assigned to another existed network User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.MSISDN = usrCreator.MSISDN;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUser(optNA);
            Assertion.verifyErrorMessageContain("user.error.msisdn.exists", "Assert Error Message  Number", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 87)
    public void SYS_TC_OPERATOR_MGMT_P_0095() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0095", "To verify that super admin can not create a network admin with same ID that is already assigned to another existed network User");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.setLoginId(usrCreator.LoginId);
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUser(optNA);
            Assertion.verifyErrorMessageContain("systemparty.validation.loginAccessidExists", "Assert Error Message Login id", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 88)
    public void SYS_TC_OPERATOR_MGMT_P_0099() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_0099", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in Short Name field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.shortName = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("user.error.shortNamenotAlphanumericOnly", "Assert Error Message Short Name", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 89)
    public void SYS_TC_OPERATOR_MGMT_P_00100() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_00100", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in Address 1 field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.address1 = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("channeluser.validation.address1.alphaNumeric", "Assert Error Message Address 1", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 90)
    public void SYS_TC_OPERATOR_MGMT_P_00101() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_00101", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in Address 2 field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.address2 = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("channeluser.validation.address2.alphaNumeric", "Assert Error Message Address 2", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 91)
//error message is not displayed through automation
    public void SYS_TC_OPERATOR_MGMT_P_00102() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_00102", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in Designation field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.designation = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("channeluser.validation.designation.alphaNumeric", "Assert Error Message Designation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 92)
    public void SYS_TC_OPERATOR_MGMT_P_00103() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_00103", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in City field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.city = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("channeluser.validation.city.alphaNumeric", "Assert Error Message City", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 93)
    public void SYS_TC_OPERATOR_MGMT_P_00104() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_OPERATOR_MGMT_P_00104", "To verify that Super Admin should not be able to Create Network Admin in the system when Invalid  data are entered in State field.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.State = Constants.SPECIAL_CHARACTER_CONSTANT;
            OperatorUserManagement obj = OperatorUserManagement.init(t1);
            startNegativeTestWithoutConfirm();
            obj.initiateOperatorUserWithFull(optNA);
            Assertion.verifyErrorMessageContain("subs.error.state.alphabetic", "Assert Error Message State", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT}, priority = 94)
    public void TC_ECONET_0146() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0146", "To verify that no user other than superadmin can Modify  Operator User Network admin/ Bank admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT).assignAuthor(Author.FEBIN);
        try {
            Login.init(t1).login(usrCreator);

            ModifyOperatorUser_page1 page = ModifyOperatorUser_page1.init(t1);
            if (!usrCreator.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                page.NavigateNA();
            } else {
                page.NavigateBPA();
            }
            page.checkUserType(Constants.NETWORK_ADMIN);
            page.checkUserType(Constants.BANK_ADMIN);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 95)
    public void TC_ECONET_0148() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0148", "To verify that the No user other than network admin can Modify channel admin and customer admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);


            ModifyOperatorUser_page1 page = ModifyOperatorUser_page1.init(t1);

            page.NavigateNA();

            page.checkUserType(Constants.CHANNEL_ADMIN);

            page.checkUserType(Constants.CUSTOMER_CARE_EXE);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 96)
    public void TC_ECONET_0154() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0154", "To verify that network admin is able to initiate deletion of an existing Channel admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        OperatorUser chnladmin = null;
        boolean isDelInitiated = false;
        try {

            chnladmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.MODIFY_OPERATOR_USER);

            OperatorUserManagement.init(t1).initModifyOrDeleteOperator(chnladmin, false);

            isDelInitiated = true;

        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, t1);
        } finally {
            if (chnladmin != null && isDelInitiated) {
                Login.init(tearDown).loginAsOperatorUserWithRole(Roles.DELETE_OPERATOR_USER_APPROVAL);
                OperatorUserManagement.init(tearDown).approveDeletionOperatorUser(chnladmin, false);
            }

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 97)
    public void TC_ECONET_0157() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0157", "To verify that higher level system user would be able to view details of a lower level system user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            OperatorUser chnladmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(usrCreator);
            OperatorUserManagement.init(t1).viewOperatorDetails(chnladmin);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 98)
    public void TC_ECONET_0158() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0158", "To verify that lower level system user would not be able to view details of a higher level system user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {

            OperatorUser chnladmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(chnladmin);


            FunctionLibrary.init(t1).verifyLinkNotAvailable("PARTY_ALL", "PARTY_PTY_VSU");


            //OperatorUserManagement.init(t1).viewOperatorDetails(chnladmin);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 99)
    public void TC_ECONET_0633() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0633", "To verify that User other than superadmin can not initiate add request of Network admin/ Bank admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {


            Login.init(t1).login(usrCreator);


            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(t1);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(t1);

            /*
             * Page One
             */
            if (!optNA.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }
            p1.checkUserType(Constants.NETWORK_ADMIN);
            p1.checkUserType(Constants.BANK_ADMIN);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 100)
    public void TC_ECONET_0634() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0634", "To verify that the User other than Bank admin should not able to modify bank users in system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {


            Login.init(t1).login(usrCreator);


            AddOperatorUser_pg1 p1 = AddOperatorUser_pg1.init(t1);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(t1);

            /*
             * Page One
             */
            if (!optNA.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                p1.NavigateNA();
            } else {
                p1.NavigateBPA();
            }
            p1.checkUserType(Constants.BANK_USER);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0}, priority = 101)
    public void TC_ECONET_0635() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0635", "To verify that User other than Valid user is not able to initiate deletion of an existing Network admin/ Bank admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.FEBIN);
        try {


            Login.init(t1).login(usrCreator);


            ModifyOperatorUser_page1 modifyOperatorUser_page1 = ModifyOperatorUser_page1.init(t1);


            /*
             *Page 1 : navigation link is different for BPA and network admin
             */
            if (!usrCreator.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                modifyOperatorUser_page1.NavigateNA();
            } else {
                modifyOperatorUser_page1.NavigateBPA();
            }

            modifyOperatorUser_page1.checkUserType(Constants.NETWORK_ADMIN);
            modifyOperatorUser_page1.checkUserType(Constants.BANK_ADMIN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }


    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0840() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0840", "To verify that the System users would be able to allowed to access the mobiquity system using the web interface only.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {

            Login.init(t1).loginAsSuperAdmin(saMaker);
            Utils.captureScreen(t1);
            Login.init(t1).loginAsSuperAdmin(saChecker);
            Utils.captureScreen(t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0841() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0841", "To verify that on the Basis of system configuration, mobiquity can generate a random 1st time login password for the new user.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_RANDOM_PASS_ALLOW", "Y");
            //AppConfig.isRandomPasswordAllowed = true;
            Login.init(t1).loginAsSuperAdmin(saMaker);
            startNegativeTest();
            OperatorUserManagement optManage = new OperatorUserManagement();
            OperatorUserManagement.init(t1).initiateOperatorUser(optNA);
            AddOperatorUser_pg2 p2 = AddOperatorUser_pg2.init(t1);
            p2.saveButton_Click();
            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.assertActionMessageContain("systemparty.message.addInitiated", "Add Initiate Operator User", pNode);
            String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(optNA.MSISDN, optNA.CategoryCode);
            DBAssertion.verifyDBAssertionEqual(dbStatus, "AI", "Verify DB Status When User is Add initiated", pNode);
            optNA.setStatus(dbStatus);
            if (AppConfig.isRandomPasswordAllowed) {
                String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(optNA.MSISDN, "asc");
                DBAssertion.verifyDBMessageContains(dbMessage, "Success.randompassword", "Verify DB Message When User Creation Initiated", pNode, optNA.FirstName, optNA.CreationPassword);
            } else {
                String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(optNA.MSISDN, "asc");
                DBAssertion.verifyDBMessageContains(dbMessage, "01616", "Verify DB Message When User Creation Initiated", pNode, optNA.FirstName, optNA.CreationPassword);
            }

            String messages = MobiquityGUIQueries.getMessageFromSentSMS(optNA.MSISDN, "desc");
            System.out.println("Message:   " + messages);
            String decmsg = Utils.decryptMessage(messages);
            System.out.println(decmsg);
            String[] pass = decmsg.split(" ");
            int size = pass.length;
            String password = pass[size - 1];
            System.out.println(password);
            // Approve Operator User
            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);
            Login.init(t1).login(optNA.LoginId, password, 3);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
            Login.init(t1).loginAsSuperAdmin(saMaker);
            Preferences.init(t1).modifySystemPreferences("IS_RANDOM_PASS_ALLOW", DEFAULT_VALUE_IS_RANDOM_PASS_ALLOW);
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0844() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0844", "To verify that the creator is able to configure the days & also the time when the system user can access / login to the system.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).loginAsSuperAdmin(saMaker);
            optNA.setIsDateAndTimeTrue();
            optNA.from = "11:00";
            optNA.to = "11:30";
            OperatorUserManagement.init(t1).initiateOperatorUserWithFull(optNA);
            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(optNA);
            Login.init(t1).tryLogin(optNA.LoginId, optNA.Password);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0848() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0848", "To verify that lower level system user would not be able to view details of a higher level system user.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            OperatorUser viewOptDet = DataFactory.getOperatorUserWithAccess("PTY_VSU");
            Login.init(t1).login(viewOptDet);
            ViewOperatorDetails_Page1 page = ViewOperatorDetails_Page1.init(t1);
            page.navigateToLink();
            List<String> values = fl.getOptions(page.getUserType());
            for (String val : values) {
                Assertion.verifyNotEqual(val, "Super Admin", "super admin is not there", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0847() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0847", "To verify that Using the ‘View Operator Details’ functionality, a system user would be able to view details of other system users (in read only mode) who are down the hierarchy.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            OperatorUser cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_SIT_5_0);

            OperatorUser viewOptDet = DataFactory.getOperatorUserWithAccess("PTY_VSU");
            Login.init(t1).login(viewOptDet);
            OperatorUserManagement.init(t1).viewOperatorDetails(cce);
            ViewOperatorDetails_Page2 page = new ViewOperatorDetails_Page2(t1);
            boolean actualAttribute1 = page.UserNamePrefixDisabled();
            Assertion.verifyEqual(actualAttribute1, true, "Verify disabled attribute for msisdn", t1);
            boolean actualAttribute2 = page.statusTypeDisabled();
            Assertion.verifyEqual(actualAttribute2, true, "Verify disabled attribute for status", t1);
            boolean actualTag = page.VerifyfirstNameTag();
            Assertion.verifyEqual(actualTag, true, "Verify tag for first name", t1);
            boolean actualTag1 = page.VerifyContactNoTag();
            Assertion.verifyEqual(actualTag1, true, "Verify tag for contact no", t1);
            boolean actualTag2 = page.VerifyLastNameTag();
            Assertion.verifyEqual(actualTag2, true, "Verify tag for last name", t1);
            boolean actualTag3 = page.VerifyMsisdnTag();
            Assertion.verifyEqual(actualTag3, true, "Verify tag for msisdn", t1);
            boolean actualTag4 = page.VerifyDepartmentTag();
            Assertion.verifyEqual(actualTag4, true, "Verify tag for department", t1);
            boolean actualTag5 = page.VerifyDivisionTag();
            Assertion.verifyEqual(actualTag5, true, "Verify tag for division", t1);
            boolean actualTag6 = page.VerifyWebLoginTag();
            Assertion.verifyEqual(actualTag6, true, "Verify tag for weblogin", t1);
            boolean actualTag7 = page.VerifyPrefLanguageTag();
            Assertion.verifyEqual(actualTag7, true, "Verify tag for prefered language", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 18)
    public void test() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0845", "To verify that super admin can approve add initiation request of Network Admin Creation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.OPT_USER_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            OperatorUser opt = new OperatorUser(Constants.NETWORK_ADMIN);
            Login.init(t1).loginAsSuperAdmin(saMaker);
            OperatorUserManagement.init(t1).initiateOperatorUser(opt);

            Login.init(t1).loginAsSuperAdmin(saChecker);
            OperatorUserManagement.init(t1).approveOperatorUser(opt);

            String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(opt.MSISDN, "desc");
            Assertion.verifyDBMessageContains(dbMessage, "00668", "Verify DB Message When Operator User Approved", t1, opt.LoginId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

}

