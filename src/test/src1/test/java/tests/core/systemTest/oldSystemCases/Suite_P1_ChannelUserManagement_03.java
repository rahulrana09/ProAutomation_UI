package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.InstrumentTCP;
import framework.entity.MobileGroupRole;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.Subscriber_Notification_Registration_Page1;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.BulkChUserRegistrationPage;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by shubham.kumar1 on 7/24/2017.
 */
public class Suite_P1_ChannelUserManagement_03 extends TestInit {

    private static WebDriver driver;
    private OperatorUser chAdmAddChUsr, chAdmAddChUsrApp, chAdmViewChUsrApp, chAdmModifyChUsr;
    private User userTC_284_280;
    private User existingChannelUsr, subs_02, subs_01;
    private FunctionLibrary fl;
    private String csvFile = null;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific fo rthis suite");
        try {
            chAdmAddChUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            chAdmAddChUsrApp = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2", Constants.CHANNEL_ADMIN);
            chAdmViewChUsrApp = DataFactory.getOperatorUserWithAccess("PTY_VCU", Constants.CHANNEL_ADMIN);
            chAdmModifyChUsr = DataFactory.getOperatorUserWithAccess("PTY_MCU", Constants.CHANNEL_ADMIN);
            existingChannelUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            subs_01 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(s1).createDefaultSubscriberUsingAPI(subs_01);
            subs_02 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(s1).createDefaultSubscriberUsingAPI(subs_02);

            csvFile = FilePath.dirFileDownloads + "BulkChannelRegistrationCore.csv"; // at any instance only one will be available to make test simple
            userTC_284_280 = new User(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TODO - Partially Automated - transactions for single step Cash Out need to be tested, end to end testing is missing
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void P1_TC_038() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_038", "To Verify that after successful creation of " +
                "channel user Each Channel User(Not of merchant domain) when created " +
                "will be assigned an Agent code. This code can be used in transactions for single step Cash Out");
        Login.init(extentTest)
                .login(chAdmAddChUsr);

        ChannelUserManagement.init(extentTest).
                createChannelUser(userTC_284_280);
    }

    /**
     * Test             :             TC280
     * descriptin       :             To verify that channel user MSISDN cannot be modified by an existing channel/subscriber user's MSISDN.
     * date             :             21-07-2017
     * author           :             shubham.kumar1
     *
     * @throws Exception
     */
    /**
     * this test case is disabled because in earlier builds there was option to change the msisdn but in latest build
     * msisdn field is uneditable and it is a readonly box
     **/
    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_321() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_321", "To verify that channel user MSISDN cannot be modified by an existing channel/subscriber user's MSISDN.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST, FunctionalTag.ECONET_SIT_5_0);


        SystemPreferenceManagement.init(extentTest)
                .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "Y");

        Login.init(extentTest)
                .login(chAdmAddChUsr);
        User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
        ChannelUserManagement.init(extentTest)
                .startNegativeTest()
                .initiateChannelUserModification(chUser);

        // Change Msisdn to existing channel user and verify the error message
        AddChannelUser_pg1.init(extentTest)
                .changeMSISDN(existingChannelUsr.MSISDN);
        String actualMessage1 = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", existingChannelUsr.MSISDN);

        List<String> errorMsg = Assertion.getAllErrorMessages();
        for (String msg : errorMsg) {
            if (msg.contains(actualMessage1)) {
                extentTest.pass("MSISDN Is Already Registered In System");
                Utils.captureScreen(extentTest);
            }
        }

        //String actualMessage1 = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", existingChannelUsr.MSISDN);
        Assertion.verifyErrorMessageContain(actualMessage1, "Existing Channel User MSISDN couldn't be used for User Modification", extentTest);

        // Change Msisdn to existing Subscriber user and verify the error message
//        AddChannelUser_pg1.init(extentTest)
//                .changeMSISDN(subs_02.MSISDN);

//        String actualMessage2 = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", subs_02.MSISDN);
//        Assertion.verifyErrorMessageContain(actualMessage2, "Existing Subscriber User MSISDN couldn't be used for User Modification", extentTest);

        //tear down
        SystemPreferenceManagement.init(extentTest)
                .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "Y");

    }

    /**
     * Test             :             TC281
     * descriptin       :             To verify that channel user's MSISDN cannot be created by an existing channel/subscriber user's MSISDN.
     * date             :             21-07-2017
     * author           :             shubham.kumar1
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_507() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_507",
                "To verify that channel user's MSISDN cannot be created by an existing channel/subscriber user's MSISDN.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST, FunctionalTag.ECONET_SIT_5_0);

        User usrTC281 = new User(Constants.WHOLESALER);

        SystemPreferenceManagement.init(extentTest)
                .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");

        /*
        Assign the user object existing channel user's msisdn
        Try Creating channel user
        Verify the error message
         */
        usrTC281.setMSISDN(existingChannelUsr.MSISDN);

        Login.init(extentTest)
                .login(chAdmAddChUsr);

        ChannelUserManagement.init(extentTest).
                startNegativeTest().
                initiateChannelUser(usrTC281);

        String actualMessage1 = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", existingChannelUsr.MSISDN);
        Assertion.verifyErrorMessageContain(actualMessage1, "New User Couldn't be created if MSIDN exists (Channel User Msisdn is used in this case)", extentTest);


        usrTC281.setMSISDN(subs_02.MSISDN);
        ChannelUserManagement.init(extentTest).
                startNegativeTest().
                initiateChannelUser(usrTC281);

        String actualMessage2 = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", subs_02.MSISDN);
        Assertion.verifyErrorMessageContain(actualMessage2, "New User Couldn't be created if MSIDN exists (Subscriber User Msisdn is used in this case)", extentTest);
    }

    /**
     * Test             :             TUNG46323/TC306
     * descriptin       :             To verify that Channel admin can approve modification request of an Channel user
     * date             :
     * author           :             shubham.kumar1
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TUNG46323_TC306() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_155",
                "To verify that Channel admin can approve modification request of an Channel user");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        User chUsr = new User(Constants.WHOLESALER);
        ChannelUserManagement.init(extentTest)
                .createChannelUserDefaultMapping(chUsr, false);

        Login.init(extentTest)
                .login(chAdmAddChUsr);

        ChannelUserManagement.init(extentTest)
                .initiateChannelUserModification(chUsr);

        /*
        Modify Channel User's First Name
        Modify User Approval, Note that Users First Name is yet not modified
        Hence, It's previous First name will be used to access it
         */
        String newName = chUsr.CategoryCode + DataFactory.getRandomNumber(3);
        AddChannelUser_pg1.init(extentTest).changeUserName(newName);

        ChannelUserManagement.init(extentTest)
                .completeChannelUserModification()
                .modifyUserApproval(chUsr, newName);

        chUsr.setFirstAndLastName(newName, newName);

        /*
        Additional validation
        Now the First Name is Changed and Approved
        Hence the same User has to be accessed using the new First Name and it has to be reflected on it's View Page
         */
        ExtentTest extentTest2 = pNode.createNode("P1_TC_040", "To verify that channel admin can view details of special super agent");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        User ssa = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT);
        Login.init(extentTest2)
                .login(chAdmViewChUsrApp);

        ChannelUserManagement.init(extentTest2)
                .viewChannelUser(ssa);
    }

    /*
     * Test:            TC310
     * Description:     To verify that channel admin can reject the Channel user creation.
     * date:
     * author:         shubham.kumar1
     *
     * */
    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC310() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_158", "To verify that channel admin can reject initiated special super agent");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        User userTC310 = new User(Constants.WHOLESALER);

        Login.init(extentTest)
                .login(chAdmAddChUsr);

        //rejecting the user approval
        ChannelUserManagement.init(extentTest)
                .addChannelUser(userTC310);

        CommonUserManagement.init(extentTest)
                .addInitiatedApproval(userTC310, false);

        //using the same user this time to be added in the system as per the test case
        ChannelUserManagement.init(extentTest)
                .addChannelUser(userTC310);

        CommonUserManagement.init(extentTest)
                .addInitiatedApproval(userTC310, true);

    }

    /*
     * Test:            TC391
     * Description:     To verify that system gives "Subscriber is not utility registered" error when account Number/Msisdn which is not accosicate with company is entered for Notification Registration
     * date:
     * author:         shubham.kumar1
     *
     * */
    @Test(priority = 6, enabled = false)
    public void TC391() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC391",
                " To verify that system gives Subscriber is not utility registered" +
                        " error when account Number/Msisdn which is not associate with company " +
                        "is entered for Notification Registration")
                .assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.NEGATIVE_TEST);


        try {
            User chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            Login.init(extentTest)
                    .login(chAddSubs);

            Subscriber_Notification_Registration_Page1 subscriber_notification_registration_page1 = Subscriber_Notification_Registration_Page1.init(extentTest);

            subscriber_notification_registration_page1.navSubscriberManagementNotificationRegistration();
            subscriber_notification_registration_page1.selectCompanyName();
            //passing any wrong account number
            subscriber_notification_registration_page1.setAccountNumber("71233471122");
            subscriber_notification_registration_page1.setMSISDN(subs_01.MSISDN);
            subscriber_notification_registration_page1.clickSubmit();
            Assertion.verifyErrorMessageContain("notif.error.Subscriber.notRegistered",
                    "To verify that system gives Subscriber is not utility registered" +
                            " error when account Number/Msisdn which is not accosicate with company is " +
                            "entered for Notification Registration", extentTest);
        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
        }

    }


    @Test(priority = 2, enabled = true, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC305() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_154", "Map Bank account to parent Bank Details");
        OperatorUser usrCreator = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);
        User childUser_01 = new User(Constants.RETAILER);
        /*
        Create Channel Users for Test
        Below Child User has assigned parentUser as parent
        Hence this Bank Mapping is to Be allowed!
         */
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1)
                .createChannelUser(childUser_01);

    }

    /**
     * Test             :             TC301
     * descriptin       :             To verify that channel user assign multiple bank accounts for different or even same bank on Channel user registration assign Linked bank pop-up.
     * date             :
     * author           :             shubham.kumar1
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC301() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_152",
                "To verify that channel user assign multiple bank accounts for different or even same bank on Channel user registration assign Linked bank pop-up.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        String defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");

        try {
            if (defaultValue.equalsIgnoreCase("Y")) {

                SystemPreferenceManagement.init(extentTest).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            }

            Login.init(extentTest)
                    .login(chAdmModifyChUsr);
            User chUsr = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(extentTest).startNegativeTest()
                    .initiateChannelUser(chUsr)
                    .assignHierarchy(chUsr);

            CommonUserManagement.init(extentTest).assignWebGroupRole(chUsr)
                    .mapDefaultWalletPreferences(chUsr)
                    .mapMultipletBankPreferencesWitMulPrimaryAcc(chUsr, false);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            Assertion.verifyEqual(Assertion.isErrorInPage(extentTest), false,
                    "User Assigned With Multiple Bank Accounts", extentTest, true);

            //checkMultipleBankMappingValidation(existingChannelUsr, extentTest);

        } catch (Exception e) {
            markTestAsFailure(e, extentTest);
            Assertion.finalizeSoftAsserts();
        } finally {
            SystemPreferenceManagement.init(extentTest).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", defaultValue);
        }
    }


    /**
     * Test             :               TC286
     * Description      :               Bulk User Registration>> Bulk User Registration
     * To Verify that system give proper message when user
     * click on submit button without upload any bulk file.
     * Date             :
     * Author           :
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void TC286() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC286", "Bulk User Registration>> Bulk User Registration\n" +
                "To Verify that system give proper message when user click on submit button without upload any bulk file.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        Login.init(extentTest).
                login(chAdmAddChUsr);
        BulkChUserRegistrationPage bulkChUserRegistrationPage = BulkChUserRegistrationPage.init(extentTest);
        bulkChUserRegistrationPage.navBulkUserRegistration();
        bulkChUserRegistrationPage.submitCsv();

        Assertion.verifyErrorMessageContain("bulkupload.error.fileNotUploaded", "uploading a file is mandatory", extentTest);
    }

    /**
     * Test             :    TC287
     * Description      :    To Verify that System not allowed to upload
     * the file when User enters Invalid detail.
     * Date             :
     * Author           :
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC287() throws Exception {
        ExtentTest extentTest = pNode.createNode("P1_TC_151", "To Verify that System not allowed to upload the" +
                " file when User enters Invalid detail.");
        extentTest.assignCategory(FunctionalTag.P1, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        User bulkUser = new User(Constants.WHOLESALER);
        User existingWhs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
        String ownerMsisdn, parentMsisdn;

        //setting new user msisdn to existing user msisdn so that error comes
        bulkUser.setMSISDN(existingWhs.MSISDN);

        Login.init(extentTest).
                login(chAdmAddChUsr);

        BulkChUserRegistrationPage bulkChUserRegistrationPage = BulkChUserRegistrationPage.init(extentTest);
        bulkChUserRegistrationPage.navBulkUserRegistration();
        BulkChUserRegistrationPage.clickOnStartDownload();


        // Below Code is written assuming only 2 level Hierarchy
        if (bulkUser.ParentUser != null) {
            ownerMsisdn = bulkUser.ParentUser.MSISDN;
            parentMsisdn = bulkUser.ParentUser.MSISDN;
        } else {
            ownerMsisdn = bulkUser.MSISDN;
            parentMsisdn = bulkUser.MSISDN;
        }

        String geoZone = dbHandler.dbGetGeoDomainName();
        String geoArea = dbHandler.dbGetChannelGeo(geoZone);
        String dateOfBirth = "28/02/1986";
        String webRole = bulkUser.WebGroupRole;

        String providerName = DataFactory.getDefaultProvider().ProviderName;
        String bank = DataFactory.getDefaultBankNameForDefaultProvider();

        InstrumentTCP insTcp = DataFactory.getInstrumentTCP(bulkUser.DomainName, bulkUser.CategoryName, bulkUser.GradeName, providerName, bank);
        MobileGroupRole mRole = DataFactory.getMobileGroupRole(bulkUser.DomainName, bulkUser.CategoryName, bulkUser.GradeName, providerName, bank);

        String l_tcpID = MobiquityGUIQueries.getIDofTCPprofile(insTcp.ProfileName);

        try {
            Thread.sleep(1000);
            BufferedWriter out = new BufferedWriter(new FileWriter(csvFile, true));
            out.write("Mr" + "," + bulkUser.FirstName + "," + bulkUser.LastName + "," + bulkUser.ExternalCode + "," + "Gurgaon" + "," + "Haryana" + "," + "India" + "," + bulkUser.Email + "," +
                    "" + "," + "" + "," + "" + "," + "Male" + "," + dateOfBirth + "," + "" + "," + bulkUser.LoginId + "," + "A" + "," + bulkUser.MSISDN + "," + "English" + "," + bulkUser.CategoryCode + "," +
                    ownerMsisdn + "," + parentMsisdn + "," + geoZone + "," + geoArea + "," + webRole + "," + mRole.ProfileName + "," + bulkUser.GradeCode + "," + l_tcpID + "," + "Y" + "," + "" + "," + "" + "," + "A" + "," + "" + "," + "" + "," + "" + "," +
                    "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "" + "," + "Relationship Officer");
            out.newLine();
            out.close();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, extentTest);
        }

        bulkChUserRegistrationPage.uploadFile(csvFile);
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        bulkChUserRegistrationPage.submitCsv();
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        bulkChUserRegistrationPage.clickOnStartDownloadLogFile();

        //checking the contents of log file generated
        bulkChUserRegistrationPage.checkLogFileDetails(bulkUser.MSISDN);

        Assertion.verifyErrorMessageContain("bulkUpload.no.record",
                "System does not allowed to upload the file when User enters Invalid detail.", extentTest);
    }

    /**
     * HELP METHODS
     */

    /**
     * checkMultipleBankMappingValidation -
     *
     * @param extentTest
     * @throws Exception
     */
    public void checkMultipleBankMappingValidation(User user, ExtentTest extentTest) throws Exception {
        Markup m = MarkupHelper.createLabel("checkMultipleBankMappingValidation: ", ExtentColor.BLUE);
        extentTest.info(m); // Method Start Marker

        try {
            CommonChannelUserPage page = CommonChannelUserPage.init(extentTest);

            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            page.clickNextWalletMap();
            page.addMoreBankModification();

            int tablesize = DriverFactory.getDriver().findElement(By.className("wwFormTableC")).findElements(By.tagName("tr")).size();
            int index = tablesize - 2;

            //Thread.sleep(1000);
            page.selectProviderType(index);
            page.selectLinkedBank(DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId));
            page.selectGrade(index, user.GradeName);
            page.selectTcp(index);
            DriverFactory.getDriver().findElement(By.name("bankCounterList[" + index + "].customerId")).sendKeys(DataFactory.getRandomNumberAsString(9));
            DriverFactory.getDriver().findElement(By.name("bankCounterList[" + index + "].accountNumber")).sendKeys(DataFactory.getRandomNumberAsString(9));
            new Select(DriverFactory.getDriver().findElement(By.name("bankCounterList[" + index + "].primaryAccountSelected"))).selectByVisibleText("No");
            extentTest.info("Refer below for Bank Mapping screen");
            Utils.captureScreen(extentTest);
            page.clickNextBankMapModification();

            Utils.captureScreen(extentTest);
            if (page.isFinalSubmitAvailable()) {
                extentTest.pass("user can be assigned multiple bank accounts for different or even same bank on Channel user registration");
            } else {
                extentTest.fail("user can not be assigned multiple bank accounts for different or even same bank on Channel user registration");
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, extentTest);
        }
    }
}
