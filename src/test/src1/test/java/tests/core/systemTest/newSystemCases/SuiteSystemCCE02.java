/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 07-Jan-2018
 *  Purpose: Test Suite of CCE
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.cceManagement.CCE;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Created by surya.dhal on 1/7/2018.
 */
public class SuiteSystemCCE02 extends TestInit {
    private OperatorUser networkAdmin, cceAdmin;
    private User subs, chUser;


    @BeforeClass(alwaysRun = true)
    public void preRequisite() throws Exception {
        try {
            cceAdmin = DataFactory.getOperatorUsersWithAccess("CCE_ENQUIRY", Constants.CUSTOMER_CARE_EXE).get(0);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test to do CCE Enquiry of Subscriber using Mobile Number
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_CCE_S_0001() throws Exception {
        List<OperatorUser> optUsers = DataFactory.getOperatorUsersWithAccess("CCE_ENQUIRY");
        for (OperatorUser optUser : optUsers) {
            ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0001 : CCE" + getTestcaseid(),
                    "To verify That " + optUser.CategoryName + " should be able to view \"user details\" of \"Subscriber\" using \"Mobile Number\".\n");
            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
            Login.init(t1).login(optUser);
            CCE.init(t1)
                    .enterUserDetails(subs, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, subs.MSISDN)
                    .checkUserDetails(subs);
        }
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test Method to do CCE Enquiry using KYC ID
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_0004() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0004 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Subscriber\" using \"KYC ID\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        try {
            String kycId = MobiquityGUIQueries.getKYCId(subs.MSISDN, Constants.SUBSCRIBER);
            if (kycId != null) {
                Login.init(t1).login(cceAdmin);
                CCE.init(t1)
                        .enterUserDetails(subs, Constants.ACCOUNT_IDENTIFIER_KYC_ID, kycId)
                        .checkUserDetails(subs);
            } else {
                t1.skip("KYC Id is not available for : " + subs.MSISDN);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test Method to Do CCE Enquiry of subscriber using Account ID
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_0005() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0005 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Subscriber\" using \"Account ID\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        try {
            String accountID = DataFactory.getAccountID(subs.MSISDN);
            if (accountID != null) {
                Login.init(t1).login(cceAdmin);
                CCE.init(t1)
                        .enterUserDetails(subs, Constants.ACCOUNT_IDENTIFIER_ACCOUNT_ID, accountID)
                        .checkUserDetails(subs);
            } else
                t1.warning("Account ID is not available for : " + subs.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to do CCE Enquiry of Channel user using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_0006() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0006 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Channel User\" using \"Mobile Number\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        try {
            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, chUser.MSISDN)
                    .checkUserDetails(chUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to do CCE Enquiry of Channel user using KYC ID
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_CCE_S_0007() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0007 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Channel User\" using \"KYC ID\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        String kycId = MobiquityGUIQueries.getKYCId(chUser.MSISDN, Constants.WHOLESALER);
        if (kycId != null) {
            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_KYC_ID, kycId)
                    .checkUserDetails(chUser);
        } else
            t1.warning("KYC Id is not available for : " + chUser.MSISDN);
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to do CCE Enquiry of Channel user using Account ID
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_CCE_S_0008() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0008 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Channel User\" using \"Account ID\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        String accountId = DataFactory.getAccountID(chUser.MSISDN);
        if (accountId != null) {
            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_ACCOUNT_ID, accountId)
                    .checkUserDetails(chUser);
        } else
            t1.warning("Account ID is not available for : " + chUser.MSISDN);
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to do CCE Enquiry of Channel user using Agent Code
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_0009() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0009 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"user details\" of \"Channel User\" using \"Agent Code\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        try {
            String agentCode = MobiquityGUIQueries.getAgentCode(chUser.MSISDN);
            if (agentCode != null) {
                Login.init(t1).login(cceAdmin);
                CCE.init(t1)
                        .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_AGENT_CODE, agentCode)
                        .checkUserDetails(chUser);
            } else
                t1.warning("Agent Code is not Available for : " + chUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to verify SVA Account Information of Subscriber using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_00010() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0010 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"Account Information\" and verify \"Stored Value Account (SVA)\" of Subscriber using \"Mobile Number\" .");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        try {
            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(subs, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, subs.MSISDN)
                    .checkAccountInformation(subs, "sva");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to verify Bank Account Information of Subscriber using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_00011() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0011 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to view \"Account Information\"and verify \"Bank Info\" of Subscriber using \"Mobile Number\".");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        try {
            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(subs, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, subs.MSISDN)
                    .checkAccountInformation(subs, "bank");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    //TODO - Bellow Tests Have to Run on The Web.

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to reset M-PIN of Channel User using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_CCE_S_00012() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0012 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to Reset \"M Pin\" of Channel User using \"Mobile Number\" under \"Reset Credential\" section.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        try {
            chUser = CommonUserManagement.init(t1)
                    .getBarredUser(Constants.RETAILER, Constants.BAR_AS_SENDER, null);

            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, chUser.MSISDN)
                    .resetCredentials("mpin");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to reset WebPassword of Channel User using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_CCE_S_00013() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0013 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to Reset \"Web Password\" of Channel user using \"Mobile Number\" under \"Reset Credential\" section.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);

        try {
            chUser = CommonUserManagement.init(t1)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);

            Login.init(t1).login(cceAdmin);
            CCE.init(t1)
                    .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, chUser.MSISDN)
                    .resetCredentials("webPassword");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : POSITIVE TEST
     * DESC : Test method to reset Employee PIN of Channel User using Mobile Number
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE})
    public void SYS_TC_CCE_S_00014() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_CCE_S_0014 : CCE" + getTestcaseid(),
                "To verify That \"CCE Admin\" should be able to Reset \"Employee PIN\" of Channel user using \"Mobile Number\" under \"Reset Credential\" section.\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE);
        chUser = CommonUserManagement.init(t1)
                .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);

        Login.init(t1).login(cceAdmin);
        CCE.init(t1)
                .enterUserDetails(chUser, Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, chUser.MSISDN)
                .resetCredentials("employeePin");
    }

}
