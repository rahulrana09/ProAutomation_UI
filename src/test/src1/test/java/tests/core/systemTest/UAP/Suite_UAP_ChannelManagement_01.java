package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created on 10-07-2017.
 */
public class Suite_UAP_ChannelManagement_01 extends TestInit {
    private OperatorUser channelModify;
    private User chUser;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        chUser = new User(Constants.WHOLESALER);
    }

    /**
     * Test: TC197
     * Description: To verify that  valid user can add Channel
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void addChUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0044  : Add Channel User", "To verify that the valid user can add Channel User.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        ChannelUserManagement.init(t1)
                .createChannelUserDefaultMapping(chUser, false);

    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM})
    public void viewChUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0049  : View Channel User", "To verify that the valid user can view Channel user.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            OperatorUser usrView = DataFactory.getOperatorUserWithAccess("PTY_VCU");

            User userCh = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            Login.init(t1).login(usrView);

            ChannelUserManagement.init(t1).
                    viewChannelUser(userCh);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM})
    public void modifyChUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0046 : Modify Channel User", "To verify that the valid user can modify Channel user.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            channelModify = DataFactory.getOperatorUserWithAccess("PTY_MCU");

            Login.init(t1).login(channelModify);

            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(chUser).
                    completeChannelUserModification().
                    modifyUserApproval(chUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 4, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM})
    public void suspendChUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0045  :SUSPEND CHANNEL USER", "To verify that the valid user can Suspend Channel user.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            ChannelUserManagement.init(t1).
                    suspendChannelUser(chUser);
        } catch (Exception e1) {
            markTestAsFailure(e1, t1);
        }


        ExtentTest t2 = pNode.createNode("TC0048  :Resume Channel User",
                "To verify that the valid user can Resume Channel user.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);

        try {
            ChannelUserManagement.init(t2).
                    resumeChannelUser(chUser);
        } catch (Exception e2) {
            markTestAsFailure(e2, t2);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0}, alwaysRun = true)
    public void deleteChUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0047  : Delete Channel User", "To verify that the valid user can delete Operator User.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            ChannelUserManagement.init(t1)
                    .deleteChannelUser(chUser);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
