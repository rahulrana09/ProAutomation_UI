package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 7/20/2017.
 */


public class Suite_UAP_C2CTransaction_01 extends TestInit {

    private User payer, payee;

    public void setup(ExtentTest test) throws Exception {
        try {
            payer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 0);
            payee = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, 1);
        } catch (Exception e) {
            test.fail("Channel Users Not found with access '" + Roles.CHANNEL_TO_CHANNEL_TRANSFER + "'. At least two users required to perform the C2C");
            markSetupAsFailure(e);
        }

    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    /**
     * Test Case : channeltoChannelTest : Channel to Channel Transfer
     * To verify that the valid user can perform
     * Channel to Channel Transfer
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void channelToChannelTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0038 : C2C from Web", "To verify that the valid user should able to do C2C from web.");

        t1.assignCategory(FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        t1.info("Checking Service Charge for the C2C Transaction..");

        setup(t1);

        ServiceCharge sCharge = new ServiceCharge
                (Services.C2C, payer, payee, null, null, null, null);

        if (!ConfigInput.isCoreRelease) {
            sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
        }

        ServiceChargeManagement.init(t1)
                .configureServiceCharge(sCharge);

        checkPre(payer, payee);
        //Login with valid user
        Login.init(t1).login(payer);

        TransactionManagement.init(t1).performC2C(payee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

        checkPost(payer, payee);

        DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

        /**
         * TODO Correct the Message Code and Un-comment the SMS assertion block
         */
        /*String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(payee.MSISDN,"DESC");
        DBAssertion.verifyDBMessageContains(dbMessage,"01616","Verify DB Message For O2C Transaction",t1,payee.LoginId,Constants.O2C_TRANS_AMOUNT);
*/
    }
}

