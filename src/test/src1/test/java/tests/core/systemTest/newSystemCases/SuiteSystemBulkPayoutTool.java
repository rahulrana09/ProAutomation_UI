package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/9/2017.
 */
public class SuiteSystemBulkPayoutTool extends TestInit {

    OperatorUser netAdmin;
    User subscriber, channelUser;

    /**
     * SETUP
     */
    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
        netAdmin = new OperatorUser(Constants.NETWORK_ADMIN);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_002
     * DESC : To verify that system should be able to perform the Cash Out service through Bulk payout tool.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_P1, FunctionalTag.BULK_PAYOUT_TOOL})
    public void P1_TC_002() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_002",
                "To verify that system should be able to perform the Cash Out service through Bulk payout tool.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.SMS_CONFIGURATION,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.BULK_PAYOUT_TOOL);

        netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);

        Login.init(t1).login(netAdmin);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        TransactionManagement.init(t1).makeSureLeafUserHasBalance(subscriber);

        channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        String fileName = bulkPayObj.generateBulkCashOutCsvFile(subscriber.MSISDN, channelUser.MSISDN);

        bulkPayObj.initiateNewBulkPayout("CASH OUT", fileName);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_003
     * DESC : To verify that system should be able to perform the Recharge service through Bulk payout tool.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_P1, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_003() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_003",
                "To verify that system should be able to perform the Recharge service through Bulk payout tool.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);

        netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);

        Login.init(t1).login(netAdmin);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        TransactionManagement.init(t1).makeSureChannelUserHasBalance(channelUser);

        String fileName = bulkPayObj.generateBulkRechargeSelfCsvFile(channelUser.MSISDN);

        bulkPayObj.initiateNewBulkPayout("RECHARGE - SELF", fileName);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_177
     * DESC : To verify that system should  generate unique Batch Id on every transaction/service performed through Bulk payout tool.
     *
     * @throws Exception
     */
    // Duplicate of "TC_ECONET_0386"
    @Test(enabled = false, priority = 3, groups = {FunctionalTag.PVG_P1, FunctionalTag.BULK_PAYOUT_TOOL})
    public void P1_TC_177() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_177",
                "To verify that system should  generate unique Batch Id on every transaction/service performed through Bulk payout tool.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL);

        netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);

        Login.init(t1).login(netAdmin);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        User whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
        User whs2 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);

        String fileName = bulkPayObj.generateBulkP2PCsvFile(whs1.MSISDN, whs2.MSISDN, "10");
        String bulkID1 = bulkPayObj.initiateBulkPayout(Services.BULK_P2P, fileName);

        String o2cFileName = bulkPayObj.generateBulkO2CCsvFile(netAdmin.MSISDN, channelUser.MSISDN);
        String bulkID2 = bulkPayObj.initiateBulkPayout(Services.BULK_O2C, o2cFileName);

        Assertion.verifyNotEqual(bulkID1, bulkID2, "Verify Unique Batch ID Generated Or Not", t1);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_178
     * DESC : To verify that system should be able to perform the Operator Withdraw service through Bulk payout tool.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_P1, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_178() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_178",
                "To verify that system should be able to perform the Operator Withdraw service through Bulk payout tool.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0,
                FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        TransactionManagement.init(t1).makeSureChannelUserHasBalance(channelUser);

        String fileName = bulkPayObj.generateBulkOperatorWithdraw(channelUser.MSISDN, DataFactory.getDefaultWallet().WalletId);

        String batchId = BulkPayoutTool.init(t1)
                .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, fileName);

        //Approve bulk payout for commission disbursement
        BulkPayoutTool.init(t1)
                .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, batchId, true);

        //check for the status in BulkPayout Dashboard screen
        BulkPayoutTool.init(t1)
                .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, batchId, true, true);


    }

}
