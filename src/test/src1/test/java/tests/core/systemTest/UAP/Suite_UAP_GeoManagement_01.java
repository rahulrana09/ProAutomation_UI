package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/**
 * Geography management for ADD ,Modify And Delete
 *
 * @author Prashant.kumar
 * modified by navin.pramanik
 */
public class Suite_UAP_GeoManagement_01 extends TestInit {

    private Geography geo;
    private SuperAdmin saGeoManager;
    private OperatorUser netAdmin;

    @BeforeClass(alwaysRun = true)
    public void beforeTest() throws Exception {
        geo = new Geography();
        netAdmin = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
    }


    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void addZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0069: Add Zone", "To verify that the valid user can add new Zone in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        //Login.init(t1).loginAsSuperAdmin(saGeoManager);
        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).addZone(geo);
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void addAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0070: Add Area", "To verify that valid user is able to add a new Area in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        //Login.init(t1).loginAsSuperAdmin(saGeoManager);
        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).addArea(geo);

    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void modifyAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0067: Modify Area", "To verify that valid user is able to modify Area");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).modifyArea(geo, "Active");
    }


    @Test(priority = 4, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void modifyZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0065: Modify Zone", "To verify that valid user is able to modify Zone");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        //Login.init(t1).loginAsSuperAdmin(saGeoManager);
        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).modifyZone(geo, "Active");
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void deleteAreaTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0068:Delete Area", "To verify that valid user is able to Delete Area");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        //Login.init(t1).loginAsSuperAdmin(saGeoManager);
        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).deleteArea(geo);
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void deleteZoneTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0066:Delete Zone", "To verify that valid user is able to Delete Zone");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.GEOGRAPHY_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        //Login.init(t1).loginAsSuperAdmin(saGeoManager);
        Login.init(t1).login(netAdmin);

        GeographyManagement.init(t1).deleteZone(geo);
    }

}
