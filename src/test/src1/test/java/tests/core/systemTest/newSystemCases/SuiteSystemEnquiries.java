/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: surya.dhal
 *  Date: 14-Dec-2017
 *  Purpose: Test Of Enquiries
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.enquiries.ChannelSubsEnq_pg1;
import framework.pageObjects.enquiries.TransactionDetail_page1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by surya.dhal on 12/14/2017.
 */
public class SuiteSystemEnquiries extends TestInit {

    private OperatorUser enqUser;
    private User payee, user, cashinUserPayer, cashinUserPayee;
    private String userType, txnID, expected, alertMsg;

    /**
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void preRequisite() throws Exception {

        MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
        txnID = dbHandler.dbGetLastTransID(Constants.O2C_SERVICE_TYPE);
        cashinUserPayer = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);
        cashinUserPayee = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }

    /**
     * Test for Channel User and Subscriber Enquiry
     *
     * @throws Exception
     */
    @Test(priority = 1, enabled = true, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0001() throws Exception {

        List<OperatorUser> optUser = DataFactory.getOperatorUsersWithAccess("ENQ_US");
        Map<String, User> channelUserSet = DataFactory.getChannelUserSetUniqueGrade();
        //Map<String, User> channelUserSet = DataFactory.getChannelUserSet();
        List<User> chUser_list = new ArrayList<User>(channelUserSet.values());
        List<User> chUsers = new ArrayList<User>();

        for (User user : chUser_list) {
            Boolean flag = false;

            if (!(user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE) || user.CategoryCode.equalsIgnoreCase(Constants.HEAD_MERCHANT))) {
                //looping through existing list to not add if same grade code already added in list
                Iterator<User> iterator = chUsers.iterator();
                while (iterator.hasNext()) {
                    if (iterator.next().GradeCode.equals(user.GradeCode)) {
                        flag = true;
                    }
                }
                if (flag == false) {
                    chUsers.add(user);
                }
            }

        }
        for (OperatorUser enqUser : optUser) {
            for (User user : chUsers) {
                try {

                    ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0001 : Channel/Subs Enquiry" + getTestcaseid(), "To verify that \"" + DataFactory.getCategoryName(enqUser.CategoryCode) + "\" should be able to do enquiry of \"" + DataFactory.getGradeName(user.GradeCode) + "\" using MSISDN");

                    t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

                    Login.init(t1).login(enqUser);

                    if (user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER))
                        userType = Constants.SUBS_USERS_CONST;
                    else
                        userType = Constants.CHANNEL_USERS_CONST;

                    Enquiries.init(t1).
                            channelSubsEnquiry(user, userType);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            for (User user : chUsers) {
                try {

                    if (!user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) {

                        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0001 : Channel/Subs Enquiry" + getTestcaseid(), "To verify that \"" + DataFactory.getCategoryName(enqUser.CategoryCode) + " \"should be able to do enquiry of \"" + DataFactory.getGradeName(user.GradeCode) + "\" using agent Code");

                        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

                        Login.init(t1).login(enqUser);
                        String agentCode = MobiquityGUIQueries.getAgentCode(user.MSISDN);

                        Enquiries.init(t1).channelSubsEnquiry(user, Constants.CHANNEL_USERS_CONST, agentCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }

    /**
     * Test to Verify User Detail
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0037() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0037 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should be able to click on \"User Detail\" Link and a new window should be appear.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).
                channelSubsDetailLink(user, Constants.CHANNEL_USERS_CONST, Constants.SHOW_USER_DETAILS);

    }

    /**
     * Test Method to Verify "User Access Detail"
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0038() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0038 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should be able to click on \"User Access Detail\" Link and a new window should be appear.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).
                channelSubsDetailLink(user, Constants.CHANNEL_USERS_CONST, Constants.SHOW_USER_ACCESS_DETAILS);

    }

    /**
     * Test method Verify "User Transfer Control Information"
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0039() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0039 : " + getTestcaseid(),
                "To verify that Network Admin Should be able to click on User Transfer Control Information Link and a new window should be appear.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).
                channelSubsDetailLink(user, Constants.CHANNEL_USERS_CONST, Constants.SHOW_TRANS_CONTROL);

    }

    /**
     * Test Method Verify "User Balance"
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0040() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0040",
                "To verify that \"Network Admin\" Should be able to click on \"User Balance\" Link and a new window should be appear.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).
                channelSubsDetailLink(user, Constants.CHANNEL_USERS_CONST, Constants.SHOW_USER_BALANCE);

    }

    /**
     * Test Method Verify back button on enquiry page
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0041() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0041 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should be able to navigate to back page when click on back button.");

        Login.resetLoginStatus();

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);
        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).negativeTestConfirmation().channelSubsDetailLink(user, Constants.CHANNEL_USERS_CONST, Constants.SHOW_USER_ACCESS_DETAILS);
        ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(t1);
        channelSubsEnqPg1.clickOnBackBtn();
        Boolean isTrue = Utils.checkElementPresent("userEnquir_forwardUserPage", Constants.FIND_ELEMENT_BY_ID);
        if (isTrue) {
            t1.pass("Successfully Navigate to previous page.");
        } else {
            t1.fail("Failed to Navigate to previous page.");
        }
    }

    /**
     * Test Method to Verify back button on transaction enquiry page
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0042() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0042 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should be able to navigate to back page when click on back button on \"transaction enquiry\" page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);
        Login.resetLoginStatus();

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).startNegativeTest().transactionDetails(txnID);
        TransactionDetail_page1 page = new TransactionDetail_page1(t1);
        page.clickBackButton();
        Boolean isTrue = Utils.checkElementPresent("txnAction_loadTxnDetails_button_submit", Constants.FIND_ELEMENT_BY_ID);
        if (isTrue) {
            t1.pass("Successfully Navigate to previous page.");
        } else {
            t1.fail("Failed to Navigate to previous page.");
        }
    }

    /**
     * Test Method for Transaction Details
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0043() throws Exception {
        List<OperatorUser> optUser = DataFactory.getOperatorUsersWithAccess("ENQ_US");
        for (OperatorUser user : optUser) {
            try {
                ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0043" + getTestcaseid(),
                        "To verify that \"" + DataFactory.getCategoryName(user.CategoryCode) + "\"should be able to view the transaction details.");
                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);
                Login.resetLoginStatus();

                Login.init(t1).login(user);
                Enquiries.init(t1).transactionDetails(txnID);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Test Method to do Enquiry of Invalid MSISDN (Special Character)
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0046() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0046 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of Channel User using \"invalid MSISDN\" (Special Character).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).invalidMsisdn(Constants.SPECIAL_CHARACTER_CONSTANT);

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);

        Assertion.verifyEqual(alertMsg, expected, "Verify Error Message for Invalid MSISDN (Special Character)", t1);
    }

    /**
     * Test Method to do Enquiry of Invalid MSISDN (Alphabets)
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0047() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0047 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of Channel User using \"invalid MSISDN\" (Alphabets).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).invalidMsisdn(Constants.ALPHABET_CONSTANT);

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("ci.error.onlynum", null);

        Assertion.verifyEqual(alertMsg, expected, "Verify Error Message for Invalid MSISDN (Alphabets)", t1);

    }

    /**
     * Test Method to do Enquiry when MSISDN left blank
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0048() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0048 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of Channel User when \"MSISDN\" left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).invalidMsisdn(Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("cce.error.Msisdn", "Verify Error Message for blank MSISDN", t1);

    }

    /**
     * Test Method to do Enquiry when MSISDN doesn't exist
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0049() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0049 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of Channel User when \"MSISDN\" does not exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        int msisdn = DataFactory.generateRandomNumber(8);

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).invalidMsisdn(String.valueOf(msisdn));

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("error.msisdnnotexist", null);

        Assertion.verifyEqual(alertMsg, expected, "Verify Error Message when MSISDN Does not exist", t1);

    }

    /**
     * Test Method to do Enquiry without selecting user type
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0050() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0050 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of Channel User when user type is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Login.init(t1).login(enqUser);
        ChannelSubsEnq_pg1 channelSubsEnqPg1 = new ChannelSubsEnq_pg1(t1);

        channelSubsEnqPg1.navChannelSubsEnquiry();
        channelSubsEnqPg1.setMSISDN(user.MSISDN);
        channelSubsEnqPg1.clickonSubmit();

        alertMsg = AlertHandle.getAlertText(t1);
        expected = MessageReader.getMessage("resetPass.error.userType", null);

        Assertion.verifyEqual(alertMsg, expected, "Verify Error Message when user type is not selected", t1);

    }

    /**
     * Test Method to do Enquiry of Invalid Transaction ID
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES})
    public void SYS_Enquiries_S_0051() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_Enquiries_S_0051 : " + getTestcaseid(),
                "To verify that \"Network Admin\" Should not be able to do Enquiry of \"Transaction Details\" when invalid transaction ID (with proper format but not exist) is entered.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES);

        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");
        int randomNumber = DataFactory.generateRandomNumber(6);
        txnID = "OC171124.0516." + randomNumber;

        Login.init(t1).login(enqUser);
        Enquiries.init(t1).startNegativeTest().transactionDetails(txnID);

        Assertion.verifyErrorMessageContain("error.transactionDetailsByWingList.empty", "Verify Error Message when MSISDN Does not exist", t1);

    }

    /**
     * Test Methods to Verify Transaction ID format for CashIn  Service.
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.PVG_P1})
    public void P1_TC_047() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_047 : " + getTestcaseid(),
                "Transaction ID Format: To verify that  Transaction ID is generated in correct format for the service Cash-In.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENQUIRIES, FunctionalTag.PVG_P1);

        if (ConfigInput.is4o9Release) {

            ServiceCharge cashinService = new ServiceCharge(Services.CASHIN, cashinUserPayer, cashinUserPayee, null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureServiceCharge(cashinService);
        }

       /* Login.init(t1).login(cashinUserPayer);

        String txnID = TransactionManagement.init(t1)
                .performCashIn(cashinUserPayee, Constants.CASHIN_TRANS_AMOUNT,DataFactory.getDefaultProvider().ProviderName);
*/
        enqUser = DataFactory.getOperatorUserWithAccess("ENQ_US");

        Login.init(t1).login(enqUser);
        startNegativeTest();
        Enquiries.init(t1).transactionDetails("CI180607.1751.A00036");
        String actualTxnID = TransactionDetail_page1.getTransactionID();

        Assertion.verifyContains(actualTxnID, "CI", "Verify Transaction ID Format", t1);

    }
}
