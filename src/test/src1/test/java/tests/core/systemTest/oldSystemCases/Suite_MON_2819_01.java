package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.CurrencyProvider;
import framework.entity.*;
import framework.features.channelUserManagement.HierarchyBranchMovement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by rahul.rana on 5/6/2017.
 */
public class Suite_MON_2819_01 extends TestInit {

    private User parentUser, usrWholeSaler, childUser_01, childUser_02, childUser_03_04;
    private OperatorUser usrCreator, usrModify, usrDelete, usrApprover, bankApprover;
    private String defaultProvider, defaultWallet, defaultBank;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("SETUP", "Create Test Data Specific to this Suite!");

        try {
            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            defaultWallet = DataFactory.getDefaultWallet().WalletName;
            defaultBank = DataFactory.getDefaultBankNameForDefaultProvider();

            // SETUP USERS
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            usrModify = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            usrDelete = DataFactory.getOperatorUserWithAccess("PTY_DCU");
            usrWholeSaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            /*
             *Create Channel User With Default Mapping
             * Add balance to the User in order to perform SVA to Bank transaction
             */
            parentUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(parentUser, false);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(parentUser, new BigDecimal("150"));

            // TEST USERS
            childUser_01 = new User(Constants.RETAILER, parentUser);
            childUser_02 = new User(Constants.RETAILER, parentUser);
            childUser_03_04 = new User(Constants.RETAILER, parentUser);

            /**
             * Configure Transfer Rule for SVA to Bank Transfer for Parent
             * As per the Test Case: both Parent and user share the same bank details and hence when performing SVA to Bank
             * both User should be able to perform Transfer.
             *
             * NOTE - just to make sure that Transfer rule exist, if not then only the respective transfer rule will be created!
             */
            ServiceCharge tRule1 = new ServiceCharge(Services.SVA_TO_BANK, parentUser, parentUser, null, null, null, null);
            TransferRuleManagement.init(t1).
                    configureTransferRule(tRule1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TC_01
     * <p>
     * Map Bank account to parent Bank Details
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void TC_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_01", "Map Bank account to parent Bank Details");

        try {
            /*
        Create Channel Users for Test
        Below Child User has assigned parentUser as parent
        Hence this Bank Mapping is to Be allowed!
         */
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(childUser_01)
                    .assignHierarchy(childUser_01);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(childUser_01)
                    .mapDefaultWalletPreferences(childUser_01);

            /**
             * Providing the Account number associated with the Parent User
             * Expected parent Account num and Customer ID should be accepted
             * Approve the User
             * Approve The bank Associated
             */
            testMapBankPreferences(childUser_01, parentUser, true, t1);

            completeChannelUserCreation(childUser_01, true, t1);

            Login.init(t1).login(usrApprover);
            CommonUserManagement.init(t1)
                    .addInitiatedApproval(childUser_01, true);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .approveAllAssociatedBanks(childUser_01);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Initiate SVA to Bank From both Parent and Child User with Same Associated Bank Account Details
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void TC_02() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_02", "Created User: Initiate SVA to Bank From both Parent and Child User with Same Associated Bank Account Details");

        try {
            // Perform O2C make sure that user has sufficient balance to perform SVA to Bank
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(childUser_01, "150", null);

            // Perform SVA Transfer Now from Child User
            TransactionManagement.init(t1).
                    initiateSVAtoBank(childUser_01, Constants.SVA_TRANSFER_NOW, defaultProvider, defaultWallet, defaultBank, "50");

            // Perform SVA Tranasfer Now from Parent User
            TransactionManagement.init(t1).
                    initiateSVAtoBank(parentUser, Constants.SVA_TRANSFER_NOW, defaultProvider, defaultWallet, defaultBank, "50");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TC_02
     * <p>
     * Map Bank Details associated with Non_parent user
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_03() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_03", "Map Bank Details associated with Non_parent user");

        /*
        Create Channel Users for Test
        Below Child User has assigned parentUser as parent but during Bank Mapping
        Account Details of Other than Parent User is provided
        Expected - Bank Account Mapping must Fail!
         */
        try {
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(childUser_02)
                    .assignHierarchy(childUser_02);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(childUser_02)
                    .mapDefaultWalletPreferences(childUser_02);

            /**
             * Provide the Account detail of user who is not assigned as Parent User
             * User creation should not happen
             */
            testMapBankPreferences(childUser_02, usrWholeSaler, true, t1);
            verifyAmbiguousMessage(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TC_03
     * <p>
     * Modify Bank Preferences of existing user with Parent Details
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_04() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_04", "Modify Bank Preferences of existing user with NON Parent Details");

        /*
        Create Channel Users for Test
        Below Child User has assigned parentUser as parent
        */
        try {
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(childUser_03_04, true);

        /*
        Provide the Account and Customer Id of user Other Then Parent User
        Assert that this action is not allowed
        */
            Login.init(t1).login(usrModify);
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(childUser_03_04);

            CommonUserManagement.init(t1)
                    .navModifyToBankMapping(childUser_03_04);

            testMapBankPreferences(childUser_03_04, usrWholeSaler, false, t1);
            verifyAmbiguousMessage(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TC_04
     * <p>
     * Modify Bank Preferences of existing user with Parent Details
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_05() throws Exception {
        /**
         * Provide the Account and Customer ID of Parent User
         * Assert the operation is successful
         */
        ExtentTest t2 = pNode.createNode("MON_2819_TC_05", "Modify Bank Preferences of existing user with Parent Details");

        try {
            Login.init(t2).login(usrModify);
            ChannelUserManagement.init(t2)
                    .initiateChannelUserModification(childUser_03_04);

            CommonUserManagement.init(t2)
                    .navModifyToBankMapping(childUser_03_04);

            testMapBankPreferences(childUser_03_04, parentUser, false, t2); // Test Bank Mapping could be Done

            completeChannelUserCreation(childUser_03_04, false, t2); // Complete the user Modification

            ChannelUserManagement.init(t2)
                    .modifyUserApproval(childUser_03_04);     // Approve The Modified Channel User

            // as the bank details are mobified, it has to be approved as well
            Login.init(t2).login(bankApprover);
            CommonUserManagement.init(t2)
                    .approveAllAssociatedBanks(childUser_03_04);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Initiate SVA to Bank From both Parent and Child User with Same Associated Bank Account Details
     * <p>
     * Post Child User Modification
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_06() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_6", "User Modified : Initiate SVA to Bank From both Parent and Child User with Same Associated Bank Account Details");

        try {
            // Perform O2C make sure that user has sufficient balance to perform SVA to Bank
            TransactionManagement.init(t1)
                    .initiateAndApproveO2CWithProvider(childUser_03_04, defaultProvider, "150");

            // Perform SVA Transfer Now from Child User
            TransactionManagement.init(t1).
                    initiateSVAtoBank(childUser_03_04, Constants.SVA_TRANSFER_NOW, defaultProvider, defaultWallet, defaultBank, "50");

            // Perform SVA Transfer Now from Parent User
            TransactionManagement.init(t1).
                    initiateSVAtoBank(parentUser, Constants.SVA_TRANSFER_NOW, defaultProvider, defaultWallet, defaultBank, "50");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TC_05
     * <p>
     * Delete Parent User to whose account details are mapped to child
     * Verify that Channel User with Child having linked account could not be deleted
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_07() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_07", "Delete Parent User to whose account details are mapped to child");
        try {
            Login.init(t1).login(usrDelete);
            ChannelUserManagement.init(t1)
                    .initiateChannelUserDelete(parentUser)
                    .confirmDeleteChannelUser();

            Assertion.verifyErrorMessageContain("channel.error.childExist", "Child Chould't be deleted", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * User with bank Mapped with a parent is not allowed to have a heirarchy movement
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.SYSTEM_TEST})
    public void TC_08() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_2819_TC_08", "User Hierarchy Movement");

        try {
            Login.init(t1).login(usrModify);
            HierarchyBranchMovement.init(t1)
                    .initiateChUserHierarchyMovement(childUser_03_04, usrWholeSaler);

            Assertion.verifyErrorMessageContain("branch.movement.child.not.allowed",
                    "Branch Movement of user with associated Bank account is not permitted", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


    }


    /****************************************************************************************************
     * HELPER METHODS
     *****************************************************************************************************/
    /**
     * Map Bank Preferences
     *
     * @param user       - User to be created
     * @param parentUser -  Parent User
     * @param isCreate   - True if the user is to be created, false if the existing user has to be modified
     * @param chNode     - Extent Report Node
     * @throws Exception
     */
    public void testMapBankPreferences(User user, User parentUser, Boolean isCreate, ExtentTest chNode) throws Exception {

        WebDriver driver = DriverFactory.getDriver();
        Markup m = MarkupHelper.createLabel("testMapBankPreferences: ", ExtentColor.BLUE);
        chNode.info(m); // Method Start Marker

        try {
            CurrencyProvider defProvider = DataFactory.getDefaultProvider();
            Bank defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defProvider.ProviderName).get(0);
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(chNode);
            int index = 0;
            if (!isCreate) {
                index = 1;
                chNode.info("Clicked on Add More!");
                try {
                    page5.clickAddMore(user.CategoryCode);
                } catch (Exception e) {
                    if (e.getMessage().contains("element is not attached")) {
                        Thread.sleep(8000);
                        page5.clickAddMore(user.CategoryCode);
                    }
                }
            }

            InstrumentTCP insTcp = DataFactory
                    .getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, defProvider.ProviderName, defaultBank.BankName);

            // UI elements
            Select selProviderUI = new Select(driver.findElement(By.name("bankCounterList[" + index + "].providerSelected")));
            Select selPaymentTypeUI = new Select(driver.findElement(By.name("bankCounterList[" + index + "].paymentTypeSelected")));
            Select selGrade = new Select(driver.findElement(By.name("bankCounterList[" + index + "].channelGradeSelected")));
            Select selInstrumentTcp = new Select(driver.findElement(By.name("bankCounterList[" + index + "].tcpSelected")));
            Select selPrimaryAccount = new Select(driver.findElement(By.name("bankCounterList[" + index + "].primaryAccountSelected")));

            WebElement customerID = driver.findElement(By.name("bankCounterList[" + index + "].customerId"));
            WebElement accountNum = driver.findElement(By.name("bankCounterList[" + index + "].accountNumber"));

            // Actions

            selProviderUI.selectByVisibleText(defProvider.ProviderName);
            chNode.info("Select Provider: " + defProvider.ProviderName);

            selPaymentTypeUI.selectByVisibleText(defProvider.Bank.BankName);
            chNode.info("Select Provider: " + defProvider.Bank.BankName);
            Thread.sleep(1500);

            selGrade.selectByVisibleText(user.GradeName);
            chNode.info("Select Grade: " + user.GradeName);
            Thread.sleep(1500);

            selInstrumentTcp.selectByVisibleText(insTcp.ProfileName);
            chNode.info("Select TCP : " + insTcp.ProfileName);
            Thread.sleep(1000);

            /*
            Set The Customer ID
             */
            customerID.sendKeys(parentUser.DefaultCustId);
            chNode.info("Select Customer Id: " + parentUser.DefaultCustId);
            /*
            Set The account number
             */
            accountNum.sendKeys(parentUser.DefaultAccNum);
            chNode.info("Select account Number: " + parentUser.DefaultAccNum);

            if (!isCreate) {
                selPrimaryAccount.selectByVisibleText("No");
            }

            page5.clickNext(user.CategoryCode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, chNode);
        }
    }

    /**
     * Complete Channel User Creation
     *
     * @param isCreated - True if the User has to be created, False if existing user is modified
     * @param chNode
     * @throws Exception
     */
    public void completeChannelUserCreation(User user, Boolean isCreated, ExtentTest chNode) throws Exception {
        Markup m = MarkupHelper.createLabel("completeChannelUserCreation: ", ExtentColor.BLUE);
        chNode.info(m); // Method Start Marker

        try {
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(chNode);
            page5.completeUserCreation(user, false);

            if (isCreated) {
                Assertion.assertActionMessageContain("channeluser.add.approval",
                        "Create User Having Associated with Parent's Account Number and Customer ID", chNode);
            } else {
                Assertion.verifyActionMessageContain("channeluser.modify.approval",
                        "Modify User Having Associated with Parent's Account Number and Customer ID",
                        chNode, user.FirstName, user.LastName);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, chNode);
        }

    }

    /**
     * Verify Ambigous Message
     *
     * @param chNode
     */
    public void verifyAmbiguousMessage(ExtentTest chNode) throws IOException {
        Markup m = MarkupHelper.createLabel("verifyAmbiguousMessage: ", ExtentColor.BLUE);
        chNode.info(m); // Method Start Marker

        try {
            Assertion.verifyErrorMessageContain("user.error.accountNumber.not.unique",
                    "Bank Details other Than Parent User are not allowed to be associated", chNode);

            Assertion.verifyErrorMessageContain("user.error.custid.not.unique",
                    "Customer ID is not Unique in the System", chNode);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, chNode);
        }
    }


}
