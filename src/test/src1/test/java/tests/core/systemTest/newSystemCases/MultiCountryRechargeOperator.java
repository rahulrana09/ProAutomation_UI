package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.operatorManagement.OperatorManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.operatorManagement.OperatorManagementPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

//* Change the Class name, it must start with the Story ID
//* move this test to package tests.core.systemCases_v5_1
//* add positive case for successful recharge operator creation, provide details for all fields verified in test -Id01
//* add negative case, if any  of the field is not set, operator user creation must fail with error
//* if preference MULTI_COUNTRY_OPERATOR_ZEBRA_ALLOWED is set to N, fields mentioned in test Id01 must not be available
//* modify Operator User is not covered in any test
//* Test specific to Access detail section is missing
//* Access details are available in groups and its mandatory, any one combination has to be provided, this test is also missing
//* DB test related to table entry MTX_GLOBAL_MASTER for storing zebra country url is missing
//* password and pin are encrypted and are shown with masking in operator user modification page, test is missing
//* Operator ID field is newly added and test corresponding to these are missing
//* Service Recharge Self and Recharge Others need to be tested with additional parameter interface ID
//* POINT 8 in story OG5-62 description need to be checked for feasibility with regard to automation
//* Delete operator user need to be tested
public class MultiCountryRechargeOperator extends TestInit {

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        //* add try catch block
        SystemPreferenceManagement.init(eSetup)
                .updateSystemPreference("MULTI_COUNTRY_OPERATOR_ZEBRA_ALLOWED", "Y");

        SystemPreferenceManagement.init(eSetup)
                .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "Y");
        //* add Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void AddMultiCountryRechargeOperator() throws Exception {
        //* giv a more descriptive test case id, if a unique Test Id is not available
        ExtentTest test = pNode.createNode("Id01", "Add Multi Country Recharge Operator");

        //* add try catch block
        OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        Login.init(test).login(optUsr);
        String rechargeOptName = "OPT" + DataFactory.getRandomNumberAsString(3);

        OperatorManagementPage page1 = OperatorManagementPage.init(test);

        page1.navigateToAddOperatorLink();

        boolean loginTextbox = Utils.checkElementPresent("operator_verifyAddOperator_loginId", Constants.FIND_ELEMENT_BY_ID);
        boolean passwordTextbox = Utils.checkElementPresent("operator_verifyAddOperator_password", Constants.FIND_ELEMENT_BY_ID);
        boolean msisdnTextbox = Utils.checkElementPresent("operator_verifyAddOperator_msisdn", Constants.FIND_ELEMENT_BY_ID);
        boolean pinTextbox = Utils.checkElementPresent("operator_verifyAddOperator_pin", Constants.FIND_ELEMENT_BY_ID);
        boolean externalCodeTextbox = Utils.checkElementPresent("operator_verifyAddOperator_externalCode", Constants.FIND_ELEMENT_BY_ID);
        boolean urlDropDown = Utils.checkElementPresent("url", Constants.FIND_ELEMENT_BY_ID);
        boolean countryDropDown = Utils.checkElementPresent("country", Constants.FIND_ELEMENT_BY_ID);
        boolean interfaceDropDown = Utils.checkElementPresent("interfaceName", Constants.FIND_ELEMENT_BY_ID);
        boolean interfaceAlias = Utils.checkElementPresent("operator_verifyAddOperator_interfaceAlias", Constants.FIND_ELEMENT_BY_ID);

        Assertion.verifyEqual(loginTextbox, true, "Validating Login Text box", test, true);
        Assertion.verifyEqual(passwordTextbox, true, "Validating Password Text box", test, true);
        Assertion.verifyEqual(msisdnTextbox, true, "Validating Msisdn Text box", test, true);
        Assertion.verifyEqual(pinTextbox, true, "Validating Pin Text box", test, true);
        Assertion.verifyEqual(externalCodeTextbox, true, "Validating External Code Text box", test, true);
        Assertion.verifyEqual(urlDropDown, true, "Validating URL Drop down", test, true);
        Assertion.verifyEqual(countryDropDown, true, "Validating Country Drop down", test, true);
        Assertion.verifyEqual(interfaceDropDown, true, "Validating Interface Drop down", test, true);
        Assertion.verifyEqual(interfaceAlias, true, "Validating Interface Alias", test, true);

        //* Currency provided ID is hardcoded, refer other test cases for using currency provider details
        //* method addRechargeOperator() is not as per the framework standards, an object for recharge operator must be created
        // and same need to passed for creation, refer class OperatorUser under package entity, also refere OperatorUserManagement for
        // methods related to operator user creation
        //* values set in method addRechargeOperator() are hard coded, need to parametrize the same
        OperatorManagement.init(test).addRechargeOperator(rechargeOptName, "101");

        //* add Assertion.finalizeSoftAsserts();
    }

    @Test
    public void ModifyMultiCountryRechargeOperator() throws Exception {
        //* giv a more descriptive test case id, if a unique Test Id is not available
        ExtentTest test = pNode.createNode("Id02", "Modify Multi Country Recharge Operator");

        //* add try catch block
        //*  method modifyOperator can be re named, as its doing a specific operation of updating the interface ID
        // also its not a generic method and should be local to the script
        //* Operator user name passed to below method is hardcoded
        OperatorManagement.init(test).modifyOperator("OPT123");
        //* add Assertion.finalizeSoftAsserts();

    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");
        //* add try catch block

        SystemPreferenceManagement.init(tTeardown)
                .updateSystemPreference("MULTI_COUNTRY_OPERATOR_ZEBRA_ALLOWED", "N");

        SystemPreferenceManagement.init(tTeardown)
                .updateSystemPreference("MULTI_OPERATOR_SUPPORT", "N");

        //* add Assertion.finalizeSoftAsserts();
    }

}
