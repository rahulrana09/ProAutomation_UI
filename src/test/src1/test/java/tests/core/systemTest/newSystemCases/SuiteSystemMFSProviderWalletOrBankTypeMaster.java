/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 10-Jan-2018
 *  Purpose: Test Suite of MFS Provider Wallet Type Master
 */

package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.pageObjects.mfsProviderWalletTypeMaster.AddWallet_Page1;
import framework.pageObjects.mfsProviderWalletTypeMaster.ModifyDeleteWallet_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Created by surya.dhal on 1/10/2018.
 */
public class SuiteSystemMFSProviderWalletOrBankTypeMaster extends TestInit {

    public static String bankName, providerName, poolAccountNo, bankAcNo, providerID, normalWallet, commissionWallet;
    SuperAdmin saAddWallet, saModifyWallet;

    @BeforeClass(alwaysRun = true)
    public void preRequisite() throws Exception {
        bankName = "AUTO" + DataFactory.getRandomNumber(3);
        providerName = DataFactory.getDefaultProvider().ProviderName;
        providerID = DataFactory.getDefaultProvider().ProviderId;
        normalWallet = DataFactory.getWalletName(Constants.NORMAL_WALLET);
        commissionWallet = DataFactory.getWalletName(Constants.COMMISSION_WALLET);
        poolAccountNo = "" + DataFactory.getRandomNumber(9);
        bankAcNo = DataFactory.getTimeStamp();
        saAddWallet = DataFactory.getSuperAdminWithAccess("MFSWTM01");
        saModifyWallet = DataFactory.getSuperAdminWithAccess("MFSMD");
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_WALLET}, enabled = false)
    public void SYS_TC_MFS_WALLET_TYPE_S_1() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_MFS_WALLET_TYPE_S_1 :Add Wallet Mapping",
                "To verify that the Super Admin can Add MFS provider wallet type master of Commission Type");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin("MFSWTM01");

        CurrencyProviderMapping.init(t1).initiateMFSProviderWalletMapping(providerName, commissionWallet);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_WALLET}, enabled = false)
    public void SYS_TC_MFS_WALLET_TYPE_S_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_MFS_WALLET_TYPE_S_2 : Verify Checkbox Functionality",
                "To verify that SuperAdmin should be able to click on all check boxes of different wallet types on the page.\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin("MFSWTM01");

        CurrencyProviderMapping.init(t1).verifyCheckBoxFunctionality();

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_033
     * DESC : Modify MFS Provider Wallet Types:
     * To verify that list of services against each Wallet Types should display in Wallet Service selection window.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1}, enabled = false)
    public void P1_TC_033() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_033",
                "Modify MFS Provider Wallet Types:\n" +
                        "To verify that list of services against each Wallet Types should display in Wallet Service selection window.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);
        String providerName = DataFactory.getDefaultProvider().ProviderName;

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);

        ModifyDeleteWallet_Page1 page1 = new ModifyDeleteWallet_Page1(pNode);
        page1.navigateToModifyDeleteWallet();
        page1.selectProviderRadio(providerName);
        page1.clickOnmodify1();
        if (Constants.NORMAL_WALLET != null)
            page1.selectDefaultWalletByValue(Constants.NORMAL_WALLET);
        page1.clickOnModify2();

        CurrencyProviderMapping.init(t1).verifyServicesForWallet(Constants.NORMAL_WALLET);

    }

    /**
     * TEST : NEGATIVE
     * ID : P1_TC_456
     * DESC : MFS Provider Wallet Type Master:" +
     * "To verify that Wallet Type cannot be linked if Default Wallet Type selected from drop down list" +
     * "should not be checked from Wallet Type List.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_456() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_456", " MFS Provider Wallet Type Master:" +
                "To verify that Wallet Type cannot be linked if Default Wallet Type selected from drop down list " +
                "should not be checked from Wallet Type List.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saAddWallet);
        AddWallet_Page1 page1 = AddWallet_Page1.init(t1);

        page1.navigateToMFSProviderWalletTypeMaster();
        page1.selectProviderName(providerName);
        page1.selectDefaultWalletByText(DataFactory.getDefaultWallet().WalletName);
        page1.Submit.click();
        Assertion.verifyErrorMessageContain("pls.select.oneWallet.Type",
                "select any wallet type", t1);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_458
     * DESC : Delete MFS Provider Wallet Types:
     * "To verify that A Wallet Types can't be deleted from system if it is associated with any subscriber
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_458() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_458",
                "Delete MFS Provider Wallet Types:\n" +
                        "To verify that A Wallet Types can't be deleted from system if it is associated with any subscriber");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);

        CurrencyProviderMapping.init(t1).deleteMFSProviderWalletMapping(DataFactory.getDefaultProvider().ProviderName, DataFactory.getAllWallet().get(0).WalletName);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_460
     * DESC : MFS Provider Linked Bank Master:
     * To verify that a list of available banks in Mobiquity should display in Default Linked Bank.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_BANK})
    public void P1_TC_460() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_460", "MFS Provider Linked Bank Master:\n" +
                "To verify that a list of available banks in Mobiquity should display in Default Linked Bank.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_BANK);

        Login.init(t1).loginAsSuperAdmin("MFSBTM01");

        framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1 page1 = new framework.pageObjects.mfsProviderBankTypeMaster.AddBank_Page1(t1);
        page1.verifyBankNames(DataFactory.getDefaultProvider().ProviderName);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_461
     * DESC : MFS Provider Linked Bank Master:
     * To verify that Banks cannot be linked if Default Linked Bank selected from drop down list should not be checked from Linked banks List.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_BANK})
    public void P1_TC_461() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_461", "MFS Provider Linked Bank Master:\n" +
                "To verify that Banks cannot be linked if Default Linked Bank selected from drop down list should not be checked from Linked banks List.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_BANK);
        String provider = DataFactory.getDefaultProvider().ProviderName;
        String bankName = DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId);

        Login.init(t1).loginAsSuperAdmin("MFSBTM01");

        framework.pageObjects.mfsProviderBankTypeMaster.ModifyDeleteBank_Page1 page1 = new framework.pageObjects.mfsProviderBankTypeMaster.ModifyDeleteBank_Page1(t1);

        page1.navigateToModifyDeleteBank();
        driver.findElement(By.xpath(".//*[text()='" + provider + "']")).click();
        page1.initiateModify();
        String otherBank = page1.selectOtherBankName(bankName);
        page1.unCheckBank(MobiquityGUIQueries.getBankId(otherBank));
        Utils.captureScreen(t1);
        page1.initiateModify2();

        Assertion.verifyErrorMessageContain("Default.Bank.Mustbe.Cheked", "Verify Message When Bank Name is not selected", t1);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_445
     * DESC : Modify MFS Provider Wallet Types:
     * "To verify that atleast one service should be selected against each Wallet Type for successfull Modification of Wallet Type for MFS Provider
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_445() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_445",
                "Modify MFS Provider Wallet Types:\n" +
                        "To verify that atleast one service should be selected against each Wallet Type for successful Modification of Wallet Type for MFS Provider");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);
        startNegativeTest();
        CurrencyProviderMapping.init(t1).modifyMFSProviderWalletMapping(providerName, normalWallet, true);

        Assertion.verifyErrorMessageContain("Please.select.atleast.one.service.par.wallet", "Verify Error Message When No Services Are Selected", t1);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_451
     * DESC : MFS Provider Wallet Type Master:
     * To Verify that different Wallet Type can be linked to a particular MFS Provider
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_451() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_451",
                "MFS Provider Wallet Type Master:\n" +
                        "To Verify that different Wallet Type can be linked to a particular MFS Provider");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);

        CurrencyProviderMapping.init(t1).mapAllWalletPreferences(providerName);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_452
     * DESC : MFS Provider Wallet Type Master:
     * To verify that a list of available Wallet Type  in Mobiquity should display in Default Wallet Type .
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_452() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_452",
                "Add MFS Provider Wallet Type Master:\n" +
                        "To verify that a list of available Wallet Type  in Mobiquity should display in Default Wallet Type .");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);

        framework.pageObjects.mfsProviderWalletTypeMaster.AddWallet_Page1 page1 = new framework.pageObjects.mfsProviderWalletTypeMaster.AddWallet_Page1(t1);
        page1.navigateToMFSProviderWalletTypeMaster();
        page1.selectProviderName(providerName);
        List<WebElement> wallets = page1.getAllWallets();

        CurrencyProviderMapping.init(t1).verifyAllWalletTypes(wallets);

    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_453
     * DESC : MFS Provider Wallet Type Master:
     * To verify that atleast one service should be selected against each linked Wallet Type for successful addition of Wallet Type for MFS Provider
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.MFS_PROVIDER_WALLET})
    public void P1_TC_453() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_453",
                "MFS Provider Wallet Type Master:\n" +
                        "To verify that atleast one service should be selected against each linked Wallet Type for successfull addition of Wallet Type for MFS Provider");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saAddWallet);
        startNegativeTest();
        CurrencyProviderMapping.init(t1).initiateMFSProviderWalletMapping(providerName, normalWallet);

        String errorMsg = Assertion.checkErrorPresent();
        if (errorMsg != null) {
            if (errorMsg.contains(MessageReader.getMessage("Mapping.done.for.this.mfsProvider", null))) {
                t1.warning("Mapping is Already done for Provider : " + providerName);
            }
        } else {
            AddWallet_Page1 page1 = AddWallet_Page1.init(t1);
            page1.clickOnSubmit();
            Assertion.verifyErrorMessageContain("Please.select.atleast.one.service.par.wallet", "Verify Error Message When No Services Are Selected", t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_455
     * DESC : Modify MFS Provider Wallet Types:
     * "To verify that a list of available Wallet Types in Mobiquity should display in Default Wallet Types Drop Down list.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.MFS_PROVIDER_WALLET, FunctionalTag.PVG_P1})
    public void P1_TC_455() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_455",
                "Modify MFS Provider Wallet Types:\n" +
                        "To verify that a list of available Wallet Types in Mobiquity should display in Default Wallet Types Drop Down list.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saModifyWallet);

        framework.pageObjects.mfsProviderWalletTypeMaster.ModifyDeleteWallet_Page1 page1 = new framework.pageObjects.mfsProviderWalletTypeMaster.ModifyDeleteWallet_Page1(t1);
        page1.navigateToModifyDeleteWallet();
        page1.selectProviderRadio(providerName);
        page1.clickOnmodify1();

        List<WebElement> wallets = page1.getAllWallets();

        CurrencyProviderMapping.init(t1).verifyAllWalletTypes(wallets);
    }

    /**
     * TEST : POSITIVE
     * ID : P1_TC_117
     * DESC : MFS Provider Wallet Type Master:
     * To verify that list of services against each Wallet Type should display in Wallet Service selection window.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.MFS_PROVIDER_WALLET})
    public void P1_TC_117() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_117",
                "Add MFS Provider Wallet Type Master:\n" +
                        "To verify that list of services against each Wallet Type should display in Wallet Service selection window.");

        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.MFS_PROVIDER_WALLET);

        Login.init(t1).loginAsSuperAdmin(saAddWallet);
        startNegativeTest();
        CurrencyProviderMapping.init(t1).initiateMFSProviderWalletMapping(providerName, normalWallet);

        String errorMsg = Assertion.checkErrorPresent();
        if (errorMsg != null) {
            if (errorMsg.contains(MessageReader.getMessage("Mapping.done.for.this.mfsProvider", null))) {
                t1.warning("Mapping is Already done for Provider : " + providerName);
            }
        } else {
            CurrencyProviderMapping.init(t1).verifyServicesForWallet(Constants.NORMAL_WALLET);
        }
    }

}
