package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.accessManagement.AccessManagement;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by surya.dhal on 6/21/2018.
 */
public class SuiteSystemResetPassword extends TestInit {

    private OperatorUser networkAdmin, channelAdmin, CCE;
    private User whs;

    /**
     * SETUP
     */

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        channelAdmin = new OperatorUser(Constants.CHANNEL_ADMIN);
        CCE = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
        whs = new User(Constants.WHOLESALER);
    }

    /**
     * TEST : POSITIVE
     * ID: P1_TC_559
     * DESC : To verify that Network Admin should be able to reset the password of anybody below him/her like Channel Admin, CCE, and Channel Users
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_559_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_559_1 :Reset Password", "To verify that Network Admin should be able to reset the password of Channel Admin.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);


        OperatorUserManagement.init(t1).createOptUser(channelAdmin, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

        networkAdmin = DataFactory.getOperatorUserWithAccess("SR_PASS");

        Login.init(t1).login(networkAdmin);
        AccessManagement.init(t1).resetPassword(channelAdmin);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_559_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_559_2 :Reset Password", "To verify that Network Admin should be able to reset the password of CCE.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        OperatorUserManagement.init(t1).createOptUser(CCE, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

        networkAdmin = DataFactory.getOperatorUserWithAccess("SR_PASS");

        Login.init(t1).login(networkAdmin);

        AccessManagement.init(t1).resetPassword(CCE);

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_559_3() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_559_3 :Reset Password", "To verify that Network Admin should be able to reset the password of Channel User( Wholesaler).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);

        ChannelUserManagement.init(t1).createChannelUser(whs);

        networkAdmin = DataFactory.getOperatorUserWithAccess("SR_PASS");

        Login.init(t1).login(networkAdmin);

        AccessManagement.init(t1).resetPassword(whs, Constants.BAR_CHANNEL_CONST);
    }


    /**
     * TEST : POSITIVE
     * ID: P1_TC_560
     * DESC : To verify that Network Admin should be able to reset the password of anybody below him/her like Channel Admin, CCE, and Channel Users
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_560() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("P1_TC_560\n :Reset Password SMS", "To verify that On successful password reset system sends the new password to the user in a SMS.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1);
        networkAdmin = DataFactory.getOperatorUserWithAccess("SR_PASS");

        Login.init(t1).login(networkAdmin);
        /**
         * TODO remove Hard coded message in Assertion
         */

        AccessManagement.init(t1).resetPassword(channelAdmin);
        try {
            String dbSMS = MobiquityGUIQueries.getMobiquityUserMessage(channelAdmin.MSISDN, "desc");
            DesEncryptor obj = new DesEncryptor();
            String newDBSms = obj.decrypt(dbSMS);

            Assertion.verifyContains(newDBSms, "Your password has been reinitialised to", "Check DB Message", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

    }
}
