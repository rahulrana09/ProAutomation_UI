package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by shubham.kumar1 on 7/12/2017.
 */
public class BillPaymentTestSuite extends TestInit {
    private OperatorUser networkAdmin;


    @Test(priority = 1)
    public void setup() throws Exception {
        try {
            networkAdmin = DataFactory.getOperatorUsersWithAccess("UTL_MCAT", Constants.NETWORK_ADMIN).get(0);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    /**
     * Test:                Description:                                            date:             author:
     * TC390                To verify that while creating subscriber,               12-07-2017
     * on confirm page all the non editable
     * information should be correct.
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void TC390() throws Exception {
        ExtentTest extentTest = pNode.createNode("TC390", "To verify that while creating subscriber, on confirm page all the non editable information should be correct.");

        Login.init(extentTest).
                login(networkAdmin);

    }


    @Test
    public void test() throws Exception {

        ExtentTest extentTest = pNode.createNode("extentTest", "To verify that while creating subscriber, on confirm page all the non editable information should be correct.");

        User subUsr = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        User whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        Transactions.init(extentTest)
                .initiateCashIn(subUsr, whsUsr, new BigDecimal("11"));

    }
}
