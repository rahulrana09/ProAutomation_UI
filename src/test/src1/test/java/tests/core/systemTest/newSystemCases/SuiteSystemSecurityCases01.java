package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.loginPasswordManagement.LoginPasswordManagement;
import framework.features.securityFeatures.SecurityFeatures;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contains the cases for Security
 *
 * @author navin.pramanik
 */
public class SuiteSystemSecurityCases01 extends TestInit {

    private SuperAdmin saMapWallet;
    private OperatorUser netAdmin;
    private User whs;
    private String validSecret = "Autom@08135", simpleSecret = "abcdefgh";


    /**
     * Before Class Run
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        saMapWallet = DataFactory.getSuperAdminWithAccess("CAT_PREF");
        netAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

    }


    /**
     * TEST: SYS_TC_0432
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0432() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0432", "To verify that when User has Logged out then clicking on Back button should not take user to Landing page.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).loginAsSuperAdmin(saMapWallet);

        SecurityFeatures.init(t1).
                goBack(Constants.CONSTANT_LOGOUT);

    }

    /**
     * SYS_TC_0434
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0434() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0434", "To verify that once the user has logged in and clicks on Browser back button then the application should get Logged out.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).loginAsSuperAdmin(saMapWallet);

        SecurityFeatures.init(t1).
                goBack(Constants.CONSTANT_LOGIN);

    }

    /**
     * SYS_TC_0436
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0436() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0436", "To Do.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        SecurityFeatures.init(t1).
                checkAutoComplete();

    }


    /**
     * SYS_TC_0444
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0444() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0444", "To verify that System should not allow to change Password if New Password and Confirm Password does not match.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(whs);

        LoginPasswordManagement.init(t1).startNegativeTest().changePassword(netAdmin.Password, validSecret, "ghy0989");

        Assertion.verifyErrorMessageContain("systemparty.error.passnotsame", "Invalid Change Password", t1);

    }

    /**
     * SYS_TC_0445
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0445() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0445", "To verify that System should not allow to change Password if Confirm Password is kept blank.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(whs);

        LoginPasswordManagement.init(t1).startNegativeTest().changePassword(netAdmin.Password, validSecret, Constants.BLANK_CONSTANT);

        Assertion.verifyErrorMessageContain("changePassword.error.confirmPasswordEmpty", "Confirm Password Empty", t1);

    }

    /**
     * SYS_TC_0446
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST})
    public void SYS_TC_0446() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0446", "To verify that System should not allow to change Password if Confirm Password is kept blank.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(whs);

        LoginPasswordManagement.init(t1).startNegativeTest().changePassword(netAdmin.Password, Constants.BLANK_CONSTANT, validSecret);

        Assertion.verifyErrorMessageContain("changePassword.error.newPasswordEmpty", "Confirm Password Empty", t1);

    }

    /**
     * SYS_TC_0447
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECURITY_TEST}, enabled = false)
    public void SYS_TC_0447() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0447", "TO DO.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(whs);

        LoginPasswordManagement.init(t1).startNegativeTest().changePassword(netAdmin.Password, simpleSecret, simpleSecret);

        String expected = MessageReader.getDynamicMessage("changePassword.error.ispasswordsComplex", AppConfig.minWebPasswordLength, AppConfig.maxWebPasswordLength, AppConfig.minUserLengthNotInPass);

        Assertion.verifyDynamicErrorMessageContain(expected, "Complex Password Required", t1);
    }


    /**
     * SYS_TC_0448 //TODO Complete the test case
     *
     * @throws Exception
     */
    @Test(priority = 8, enabled = false)
    public void SYS_TC_0448() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_0448", "TO DO.");

        t1.assignCategory(FunctionalTag.SECURITY_TEST, FunctionalTag.PVG_SYSTEM);

        Login.init(t1).login(whs);

        LoginPasswordManagement.init(t1).startNegativeTest().
                changePassword(netAdmin.Password, simpleSecret, simpleSecret);

        Assertion.verifyErrorMessageContain("channeluser.validation.complexpassword", "Complex Password Required", t1);

    }


}
