package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 8/14/2017.
 */
public class Suite_UAP_TransferRule_01 extends TestInit {

    private OperatorUser usrTRuleCreator, usrTRuleApprover, usrO2CTransferRule;
    private ServiceCharge tRule;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            User ssa = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            tRule = new ServiceCharge(Services.AUTO_O2C, opt, ssa, null, null, null, null);
            usrTRuleCreator = DataFactory.getOperatorUserWithAccess(Roles.TRANSFER_RULES);
            usrTRuleApprover = DataFactory.getOperatorUserWithAccess(Roles.TRANSFER_RULE_APPROVAL);
            usrO2CTransferRule = DataFactory.getOperatorUserWithAccess(Roles.O2C_TRANSFER_RULES);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP,
            FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0165 ",
                "To verify that user is able to Create Transfer Rule ")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0,
                        FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {

            TransferRuleManagement.init(t1).
                    configureTransferRule(tRule);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0162 ",
                "To verify that the valid user can view transfer rule in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrTRuleCreator);
            TransferRuleManagement.init(t1)
                    .viewTransferRule(tRule);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0163", "To verify that the valid user can edit/modify transfer rule in the system.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_SIT_5_0);
        try {


            Login.init(t1).login(usrTRuleCreator);

            TransferRuleManagement.init(t1)
                    .modifyTransferRule(tRule);

            Login.init(t1).login(usrTRuleApprover);

            TransferRuleManagement.init(t1).approveTransferRule(tRule.TransferRuleID, Constants.STATUS_UPDATE_INITIATE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void Test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0164 ", "To verify that the valid user can delete transfer rule in the system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrTRuleCreator);

            TransferRuleManagement.init(t1)
                    .deleteTransferRule(tRule);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            TransferRuleManagement.init(t1)
                    .configureTransferRule(tRule);
        }
        Assertion.finalizeSoftAsserts();
    }


    // this test is already part of Base Setup, more elaborate tests can be added to test Adding O2C Transfer rules [rahul rana, 3 Oct 2018]
    @Test(priority = 5, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void Test_05() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0166 ", "To verify that user is able to add O2C Transfer Rule ");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0);
        try {
            //Please do't change the Group Role and User below
            Login.init(t1).login(usrO2CTransferRule);

            TransferRuleManagement.init(t1).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
