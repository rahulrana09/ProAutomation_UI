package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.collect.ImmutableMap;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.ConstantsProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.CommonOperations.setTxnConfig;

/**
 * Created on 10-07-2017.
 */
public class Suite_UAP_TransactionCorrection_01 extends TestInit {

    private static String transID = null;
    private User cashinUser, cashoutUser, subs;
    private User c2cpayer, c2cpayee;


    public void checkPreTransactionDBValues(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPostTransactionDBValues(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        cashinUser = DataFactory.getChannelUserWithAccess(Roles.CASH_IN);
        cashoutUser = DataFactory.getChannelUserWithAccess(Roles.CASH_OUT);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        /**
         * In few opco it is P2P_WEB
         */
        c2cpayer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, Constants.WHOLESALER, 0);
        c2cpayee = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER, Constants.WHOLESALER, 1);
    }

    /**
     * Test Case : Transaction correction of CASHIN transaction (Initiated by Channel User with initiation role
     * and Approved by Channel User with approval role)
     * <p>
     * To verify that the valid channel user can perform transaction correction initiation and approval for a Cashin transaction
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void txnCorrectionCashinPositiveTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0161 : Transaction Correction: CASHIN",
                "To verify that the valid channel user can perform transaction correction initiation and approval for a Cashin transaction.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHIN));
            if (!ConfigInput.isCoreRelease) {
                t1.info("Checking Service Charge for the CashIn Service (for transaction correction test)");
                ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, cashinUser, subs, null, null, null, null);
                ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);
            }


            checkPreTransactionDBValues(cashinUser, subs);

            Login.init(t1).login(cashinUser);

            transID = TransactionManagement.init(t1).performCashIn(subs, "100", DataFactory.getDefaultProvider().ProviderName);

            checkPostTransactionDBValues(cashinUser, subs);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            t1.info("Checking Service Charge for the Transaction Correction service");

            ServiceCharge sCharge2 = new ServiceCharge(Services.TXN_CORRECTION, subs, cashinUser, null, null, null, null);

            if (transID != null) {
                checkPreTransactionDBValues(cashinUser, subs);
                ServiceChargeManagement.init(t1).configureServiceCharge(sCharge2);
                TransactionCorrection.init(t1).initAndApproveTxnCorrection(transID, false, false);
                checkPostTransactionDBValues(cashinUser, subs);
                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
            } else {
                t1.fail("Fail to perform Transaction Correction As Transaction Initiation Failed.");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * Cashout
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void txnCorrectionCashOutTest() throws Exception {

        cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB");

        ExtentTest t1 = pNode.createNode("TC0159: Transaction Correction CASHOUT",
                "To verify that the valid user can perform transaction correction (Cash OUT).")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
                        FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0,
                        FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));

            if (!ConfigInput.isCoreRelease) {
                t1.info("Checking Service Charge for the Cash-Out Service (for transaction correction test)");

                //Performing Cashin transaction
                ServiceCharge sCharge = new ServiceCharge(Services.CASHOUT, cashoutUser, subs, null, null, null, null);

                TransferRuleManagement.init(t1)
                        .configureTransferRule(sCharge);
                if (!ConfigInput.isCoreRelease) {
                    ServiceChargeManagement.init(t1)
                            .configureServiceCharge(sCharge);
                }
            }

            Login.init(t1).login(cashoutUser);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(Constants.MIN_CASHOUT_AMOUNT));

            transID = TransactionManagement.init(t1).performCashOut(subs, Constants.MIN_CASHOUT_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            if (transID != null) {
                String requestID = MobiquityGUIQueries.dbGetServiceRequestId(transID);
                Transactions.init(t1).cashoutApprovalBysubs(subs, requestID);

                //Performing Transaction Correction using Cashin transaction id
                t1.info("Checking Service Charge for the Transaction Correction service");
                ServiceCharge sCharge2 = new ServiceCharge(Services.TXN_CORRECTION, cashoutUser, subs, null, null, null, null);

//                ServiceChargeManagement.init(t1)
//                        .configureServiceCharge(sCharge2);

                TransferRuleManagement.init(t1).configureTransferRule(sCharge2);

                String txnCorrID = TransactionCorrection.init(t1).
                        initAndApproveTxnCorrection(transID, false, false);

                SentSMS.verifyPayerPayeeDBMessage(subs, cashoutUser, Services.TXN_CORRECTION, txnCorrID, t1);


            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void txnCorrP2P() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0160 : Transaction Correction: P2P", "To verify that the valid user can perform transaction correction (P2P).")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            c2cpayer = DataFactory.getChannelUserWithAccess("P2P");

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.C2C));

            t1.info("Checking Service Charge for the Cashin Service (for transaction correction test)");

            ServiceCharge sCharge = new ServiceCharge(Services.C2C, c2cpayer, c2cpayee, null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);
            checkPreTransactionDBValues(c2cpayer, c2cpayee);
            Login.init(t1).login(c2cpayer);
            transID = TransactionManagement.init(t1).performC2C(c2cpayee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());
            checkPostTransactionDBValues(c2cpayer, c2cpayee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            t1.info("Checking Service Charge for the Transaction Correction service");
            ServiceCharge sCharge2 = new ServiceCharge(Services.TXN_CORRECTION, c2cpayee, c2cpayer, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(sCharge2);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge2);

            if (transID != null) {
                checkPreTransactionDBValues(c2cpayer, c2cpayee);

                TransactionCorrection.init(t1).initAndApproveTxnCorrection(transID, false, false);

                checkPostTransactionDBValues(c2cpayer, c2cpayee);

                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
            } else {
                t1.fail("Fail to perform Transaction Correction As Transaction Initiation Failed.");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0692() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0692",
                " To verify that the Transaction correction initiated doesn't have approval within time limit then" +
                        " transaction correction should fail.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            c2cpayer = DataFactory.getChannelUserWithAccess(Roles.CHANNEL_TO_CHANNEL_TRANSFER);

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.C2C));

            t1.info("Checking Service Charge for the C2C Service (for transaction correction test)");

            ServiceCharge sCharge = new ServiceCharge(Services.C2C, c2cpayer, c2cpayee, null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);
            checkPreTransactionDBValues(c2cpayer, c2cpayee);
            ConstantsProperties.getInstance().getProperty("TIMEOUT_TRANSACTION_AVERAGE_TIME");

            Login.init(t1).login(c2cpayer);
            transID = TransactionManagement.init(t1).performC2C(c2cpayee, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());
            checkPostTransactionDBValues(c2cpayer, c2cpayee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            t1.info("Checking Service Charge for the Transaction Correction service");
            ServiceCharge sCharge2 = new ServiceCharge(Services.TXN_CORRECTION, c2cpayee, c2cpayer, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(sCharge2);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge2);

            if (transID != null) {
                checkPreTransactionDBValues(c2cpayer, c2cpayee);
                Utils.putThreadSleep(1000);
                TransactionCorrection.init(t1).initAndApproveTxnCorrection(transID, false, false);

                checkPostTransactionDBValues(c2cpayer, c2cpayee);

                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
            } else {
                t1.fail("Fail to perform Transaction Correction As Transaction Initiation Failed.");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
