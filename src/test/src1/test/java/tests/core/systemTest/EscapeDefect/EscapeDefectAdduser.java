package tests.core.systemTest.EscapeDefect;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 11/17/2017.
 */
public class EscapeDefectAdduser extends TestInit {


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT})
    public void SYS_TC_0735() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_0735 : EscapeDefect_BB_CAS2090_Add User ", "To verify that valid user should not be able to add same MSISDN as multiple users.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT);

        User user1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        User user2 = new User(Constants.WHOLESALER);
        user2.setMSISDN(user1.MSISDN);

        Login.init(t1).login(DataFactory.getOperatorUsersWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN));
        ChannelUserManagement.init(t1).startNegativeTest().initiateChannelUser(user2);


        String expected = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", user2.MSISDN);
        String actual = Assertion.getErrorMessage();
        Assertion.verifyEqual(actual, expected, "Verify error message", t1);
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT})
    public void SYS_TC_0082() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_0082 :Escape Defect_TIGOSV_CAS2060_ Duplicate External", "Two different MSISDN should not be registered into TM with same external code");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT);

        User user1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        User user2 = new User(Constants.WHOLESALER);
        user2.setExternalCode(user1.ExternalCode);

        Login.init(t1).login(DataFactory.getOperatorUsersWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN));
        ChannelUserManagement.init(t1).startNegativeTest().initiateChannelUser(user2);

        Assertion.verifyErrorMessageContain("subs.error.uniextcoderequired", "Verify error message", t1);
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT})
    public void SYS_TC_0075() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_0075 : EscapeDefect_TIGOHN_CAS1748_Subs Reg", "To verify that default message display while performing subscriber registration through Web or SSA.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ESCAPEDEFECT);

        User sub = new User(Constants.SUBSCRIBER);
        //UserManagement.init(t1).createSubscriber(sub,false);

        /**
         * TODO for Prashant -- Remove hard code for MSISDN
         */
        sub.setMSISDN("920010329");

        SubscriberManagement.init(t1).deleteSubscriberByAgent(sub, DataFactory.getDefaultProvider().ProviderId);
    }


}
