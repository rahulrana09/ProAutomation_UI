package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Contain the cases for Biller Management
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BillerManagement_01 extends TestInit {


    private static String categoryName, categoryCode;
    private OperatorUser categoryCreator;
    private Biller billerObj;
    private BillerCategory billerCategory;
    private CurrencyProvider providerOne;
    private BillValidation billValidation;
    private BillNotification billNotification;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        try {
            providerOne = GlobalData.defaultProvider;
            categoryName = "Automation" + DataFactory.getRandomNumber(3);
            categoryCode = "Auto" + DataFactory.getRandomNumber(3);
            billerCategory = new BillerCategory(categoryCode, categoryName, "Automation");
            billerObj = new Biller(providerOne.ProviderName, categoryName, Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_BOTH);
            billValidation = new BillValidation(null, Constants.BILL_TYPE_NUMERIC, "1", "10", null, null, Constants.BILL_REFTYPE_REGISTRATION);
            billNotification = new BillNotification(Constants.BILL_NOTIFICATION_TYPE_GENERATE_DATE_AFTER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP,
            FunctionalTag.BILLER_CATEGORY_MANAGEMENT,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void billerCategoryAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0016  :Add new Biller Category",
                "To verify that the valid user can add new biller category in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.BILLER_CATEGORY_MANAGEMENT, FunctionalTag.BILLER_MANAGEMENT,
                        FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            categoryCreator = DataFactory.getOperatorUsersWithAccess("UTL_MCAT").get(0);
            Login.init(t1).login(categoryCreator);
            BillerManagement.init(t1)
                    .addBillerCategory(billerCategory);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC0017 :Modify Biller Category",
                "To verify that the valid user can update biller category in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            categoryCreator = DataFactory.getOperatorUsersWithAccess("UTL_MCAT").get(0);
            Login.init(t2).login(categoryCreator);

            billerCategory.Description = "updated";

            BillerManagement.init(t2).updateBillerCategory(billerCategory);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC0019 : Add Biller Registration",
                "To verify that the valid user can add new biller in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.BILLER_REGISTRATION, FunctionalTag.BILLER_MANAGEMENT,
                        FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            BillerManagement.init(t3)
                    .initAndApproveBillerRegistration(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("TC0020 : Biller Modification",
                "To verify that the valid user can modify biller in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);


        try {
            BillerManagement.init(t4).modifyBiller(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        ExtentTest t5 = pNode.createNode("TC0022 : Biller Suspend",
                "To verify that the valid user can suspend biller in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.BILLER_REGISTRATION, FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            BillerManagement.init(t5)
                    .suspendBiller(billerObj)
                    .suspendApproveBiller(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }

        ExtentTest t6 = pNode.createNode("TC0023 : Biller Resume ",
                "To verify that the valid user can resume biller in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_REGISTRATION,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            Utils.putThreadSleep(5000);
            BillerManagement.init(t6).
                    activateBiller(billerObj);
            BillerManagement.init(t6)
                    .approveActivateBiller(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }

        ExtentTest t7 = pNode.createNode("TC0024 :Add Biller Validation ",
                "To verify that the valid user can add biller validations in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_VALIDATION_MANAGEMENT,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            BillerManagement.init(t7).
                    addBillerValidation(billerObj, billValidation);
        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }

        ExtentTest t8 = pNode.createNode("TC0025 : Biller Validation Modify",
                "To verify that the valid user can modify biller validations in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_VALIDATION_MANAGEMENT,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            BillerManagement.init(t8).
                    modifyBillerValidation(billerObj, "9", "12");
        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }


        ExtentTest t9 = pNode.createNode("TC0026 : Biller Validation Delete",
                "To verify that the valid user delete modify biller validations in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_VALIDATION_MANAGEMENT,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            BillerManagement.init(t9).
                    deleteBillerValidation(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t9);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP,
            FunctionalTag.BILL_NOTIFICATION_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.DEPENDENCY_TAG})
    public void billerNotficnAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0014 : Add Bill Notification ",
                "To verify that the valid user can add bill notification to the billers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.BILL_NOTIFICATION_MANAGEMENT, FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Biller billerObj = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            BillerManagement.init(t1).
                    addBillerNotification(billNotification);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC0011 :Association Bill Notification TO BILLER ",
                "To verify that the valid user can associate bill notification to the billers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILL_NOTIFICATION_ASSOCIATION,
                        FunctionalTag.BILLER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        Biller billerObj = BillerManagement.init(t2)
                .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);
        try {
            BillerManagement.init(t2).
                    addBillerNotificationAssociation(billNotification, billerObj.LoginId);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC0012 : Biller Notification Association Modify",
                "To verify that the valid user can update bill notification to the billers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILL_NOTIFICATION_ASSOCIATION, FunctionalTag.BILLER_MANAGEMENT);

        try {
            BillerManagement.init(t3).
                    updateBillerNotificationAssociation(billNotification, Constants.STATUS_ACTIVE);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("TC0015  Bill Pay-> Biller Notification Modify",
                "To verify that the valid user can update/delete bill notification to the billers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILL_NOTIFICATION_ASSOCIATION, FunctionalTag.BILLER_MANAGEMENT);

        try {
            BillerManagement.init(t4).
                    modifyBillerNotification(billNotification, Constants.STATUS_ACTIVE);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        ExtentTest t5 = pNode.createNode("TC0013 : Biller Notification Delete",
                "To verify that the valid user can delete notification association to the billers.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILL_NOTIFICATION_ASSOCIATION, FunctionalTag.BILLER_MANAGEMENT);

        Utils.putThreadSleep(Constants.TWO_SECONDS);

        try {
            BillerManagement.init(t5).
                    updateBillerNotificationAssociation(billNotification, Constants.STATUS_DELETE);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.DEPENDENCY_TAG},
            priority = 3, alwaysRun = true)
    public void billerDeleteTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0021  Biller Delete",
                "To verify that the valid user can delete biller in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
                        FunctionalTag.BILLER_MANAGEMENT);

        try {
            BillerManagement billerMgmtObj = BillerManagement.init(t1);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_DELETION_INITIATION);
            billerMgmtObj.initiateBillerDeletion(billerObj);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BILLER_DELETION_APPROVAL);
            billerMgmtObj.approveBillerDeletion(billerObj);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.BILLER_CATEGORY_MANAGEMENT, FunctionalTag.DEPENDENCY_TAG}, priority = 17, dependsOnMethods = "billerDeleteTest", alwaysRun = true)
    public void billerCategoryDeleteTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0018 :Delete Biller Category",
                "To verify that the valid user can delete biller category in the system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT, FunctionalTag.BILLER_MANAGEMENT);

        try {
            categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
            Login.init(t1).login(categoryCreator);
            BillerManagement.init(t1).updateBillerCategory(categoryCode, "updated desc", Constants.STATUS_DELETE);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
