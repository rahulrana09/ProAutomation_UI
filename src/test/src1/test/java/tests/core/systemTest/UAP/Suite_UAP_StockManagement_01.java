package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.SysWalletBalance;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * This class will contain all the Positive of Stock management
 * Positive case Means only Single positive flow.
 * So that Upon these methods further negative of System cases can be build
 * Modules Included :
 * IMT STOCK   -   Initiate , Approve 1 , Approve 2
 * STOCK LIMIT SET
 * STOCK EA LIMIT SET
 * STOCK INITIATE and APPROVE
 * STOCK EA INITIATE and APPROVE
 * STOCK REIMBURSEMENT
 * STOCK REIMBURSEMENT STATUS
 */
public class Suite_UAP_StockManagement_01 extends TestInit {

    private static String refNo;
    private OperatorUser stockLimiter, stockInitiator, stockReimbUser, stockEAInitiator;
    private MobiquityGUIQueries dbHandler;

    /**
     * This method will load all the users used
     */
    @BeforeClass(alwaysRun = true)
    public void loadData() throws Exception {
        dbHandler = new MobiquityGUIQueries();
        refNo = DataFactory.getTimeStamp();
    }

    /**
     * TEST: TC110 - Stock Limit
     * DESC : To verify that the valid user (Network Admin) can set stock limit
     * This test will set the Stock Limit and also it reverts the Limit if previous limit is different than current limit set
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 1)
    public void testStockLimit() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0129 : Stock Limit", "To verify that the valid user (Network Admin) can set stock limit.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            stockLimiter = DataFactory.getOperatorUserWithAccess("STOCK_LIMIT", Constants.NETWORK_ADMIN);

            Login.init(t1).login(stockLimiter);

            BigDecimal prevStockLimit = StockManagement.init(t1).addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName,
                    Constants.STOCK_LIMIT_AMOUNT);

            if (!prevStockLimit.equals(Constants.STOCK_LIMIT_AMOUNT))
                StockManagement.init(t1).addNetworkStockLimit(DataFactory.getDefaultProvider().ProviderName, prevStockLimit.toString());
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * TEST: TC111 - Stock Initiation
     * DESC : To verify that the valid user can initiate and approve stock in system
     * <p>
     * DB Assertion is also included like Transaction Status , Pre and Post Balance of Payee and Payer
     *
     * @throws Exception
     */
    //todo  [rahulrana: disbaled case, as verifyPrePostBalanceForPayerAndPayee need to be refactored]
    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 2)
    public void testStockInitiation() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0136",
                "Stock Initiation To verify that the valid user can initiate and approve stock in system.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT,
                        FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            stockInitiator = DataFactory.getOperatorUserWithAccess("STOCK_INIT");

            Login.init(t1).login(stockInitiator);

            String payerWallet = DataFactory.getBankId(GlobalData.defaultBankName);

            SysWalletBalance preBalance = new SysWalletBalance(payerWallet, Constants.WALLET_101_101, GlobalData.defaultProvider.ProviderId);

            StockManagement.init(t1)
                    .initiateAndApproveNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDefaultBankNameForDefaultProvider(),
                            Constants.STOCK_TRANSFER_AMOUNT);

            SysWalletBalance postBalance = new SysWalletBalance(payerWallet, Constants.WALLET_101_101, GlobalData.defaultProvider.ProviderId);
            //Added for Verification of DB Assertions post transaction
            DBAssertion.init(t1)
                    .verifyPrePostBalanceForPayerAndPayee(preBalance, postBalance, Constants.STOCK_TRANSFER_AMOUNT);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST : Stock Limit EA
     * DESC : To verify that the valid user can set stock transfer limit for EA.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 3)
    public void testStockEALimit() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0130: Stock Limit EA", " To verify that the valid user can set stock transfer limit for EA.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            OperatorUser optEASetLimit = DataFactory.getOperatorUsersWithAccess("STOCKTR_LIMIT").get(0);

            Login.init(t1).login(optEASetLimit);

            String prevEALimit = StockManagement.init(t1).addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, Constants.STOCK_LIMIT_AMOUNT);

            if (!prevEALimit.equalsIgnoreCase(Constants.STOCK_LIMIT_AMOUNT))
                StockManagement.init(t1).addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, prevEALimit);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * TEST : StockTransferEA
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 4)
    public void testStockTransferEa() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0137 :StockTransferEA", "To verify that the valid user can initiate stock to EA in system.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            StockManagement stockManagement = StockManagement.init(t1);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("STR_INIT"));


            BigDecimal preReconBalance = MobiquityDBAssertionQueries.getReconBalance();
            String eaTxnID = stockManagement
                    .initiateEAStock(refNo, Constants.STOCK_EA_TRANSFER_AMOUNT, "Stock EA initiation",
                            DataFactory.getDefaultProvider().ProviderName);

            if (eaTxnID != null) {
                stockManagement.approveEAStockL1(eaTxnID);
                DBAssertion.init(t1)
                        .verifyPrePostBalanceForTransaction(eaTxnID, Constants.STOCK_EA_TRANSFER_AMOUNT, preReconBalance);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0}, priority = 5)
    public void testStockReimb() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0138:Stock Reimbursement",
                "To verify that the valid user can initiate reimbursement.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            StockManagement stockManagement = StockManagement.init(t1);

            OperatorUser optReim = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            OperatorUser optReimApprover = DataFactory.getOperatorUserWithAccess("STOCK_REMB");

            if (optReim == null || optReimApprover == null) {
                t1.fail("Operator User not found with access Reimbursement or Reimbursement Approval. Please check Operator User sheet.");
                Assert.fail("Operator User not found with access Reimbursement or Reimbursement Approval. Please check Operator User sheet.");
            }

            OperatorUser operatorUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            ServiceCharge sCharge = new ServiceCharge
                    (Services.OPERATOR_WITHDRAW, operatorUser, operatorUser, null, null, null, null);

            TransferRuleManagement.init(t1).
                    configureTransferRule(sCharge);

            Login.init(t1).login(optReim);

            BigDecimal preReconBalance = MobiquityDBAssertionQueries.getReconBalance();

            String remtid = stockManagement
                    .initiateStockReimbursement(operatorUser, refNo, Constants.REIMBURSEMENT_AMOUNT, "reimbursement", null);

            if (remtid != null) {
                //Login.resetLoginStatus();
                Login.init(t1).login(optReimApprover);
                stockManagement.approveReimbursement(remtid.trim());

                BigDecimal requestedAmount = MobiquityDBAssertionQueries.getRequestedAmount(remtid);

                DBAssertion.verifyDBAssertionEqual(requestedAmount.toString(), Constants.REIMBURSEMENT_AMOUNT, "Verify Transaction Amount", t1);

                //Utils.putThreadSleep(10000);
                DBAssertion.init(t1)
                        .verifyPrePostBalanceForTransaction(remtid, Constants.REIMBURSEMENT_AMOUNT, preReconBalance);

            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 6)
    public void reimbursementStatusTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0135",
                "Reimbursement Status: To verify that Network Admin can view the status of any Stock Reimbursement.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT,
                        FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);


        try {
            OperatorUser remInit = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            OperatorUser remAprov = DataFactory.getOperatorUserWithAccess("STOCK_INIT");
            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(remInit, DataFactory.getRandomNumberAsString(5), "5", "test", null);

            //Login method is included
            Login.init(t1).login(remAprov);
            txnId = StockManagement.init(t1)
                    .approveReimbursement(txnId);

            if (txnId != null) {
                String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnId);

                Login.init(t1).login(remInit);
                StockManagement.init(t1)
                        .verifyReimbursementGUIAndDBStatus(txnId, dbTxnStatus);
            } else {
                Assertion.failAndStopTest("Failed to get The reimbursement Transaction ID", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * TEST: TC118 - Stock IMT
     * DESCRIPTION: Stock Management >>Stock Transfer To verify that Network admin is able to initiate IMT STOCK
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT}, priority = 7)
    public void testStockIMT() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0139", "Stock IMT: To verify that Network Admin is able to initiate and approve IMT Stock ");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {
            BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OperatorUser imtInitiator = DataFactory.getOperatorUserWithAccess("STK_IMT_INI");
            OperatorUser imtApprover = DataFactory.getOperatorUserWithAccess("STK_IMT_APPROVAL1");

            if (imtInitiator == null || imtApprover == null) {
                t1.fail("Operator User not found with IMT Initiate or Approve access.Please check Operator user sheet.");
                Assert.fail("Operator User not found with IMT Initiate or Approve access.Please check Operator user sheet.");
            }

            Login.init(t1).login(imtInitiator);

            String txnID = StockManagement.init(t1)
                    .initiateIMT(refNo, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, Constants.REMARKS);

            if (txnID != null) {
                Login.init(t1).login(imtApprover);
                StockManagement.init(t1).approveIMTStockL1(txnID);
                DBAssertion.init(t1)
                        .verifyPrePostBalanceForTransaction(txnID, Constants.STOCK_TRANSFER_LEVEL1_AMOUNT, preRecon);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    /**
     * stockWithdrawalTest
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void stockWithdrawalTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC119 : Stock Withdraw", "To verify that valid user should be able to perform Stock Withdrawal");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            OperatorUser naInitiator = DataFactory.getOperatorUserWithAccess("STK_WITHDRAW", Constants.NETWORK_ADMIN);
            Bank trustBank = DataFactory.getAllTrustBanksLinkedToProvider(GlobalData.defaultProvider.ProviderName).get(0);

            Login.init(t1).loginAsSuperAdmin("STK_WITHDRAW");
            String txnId = StockManagement.init(t1)
                    .initiateStockWithdrawal("10", GlobalData.defaultProvider.ProviderId, trustBank.BankName);

            Login.init(t1).loginAsSuperAdmin("STK_WITHDRAW_APP");
            StockManagement.init(t1)
                    .approveRejectStockWithdrawal(txnId, true);

            BigDecimal preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.init(t1)
                    .verifyPrePostBalanceForTransaction(txnId, Constants.STOCK_WITHDRAW_AMOUNT, preRecon);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

}
