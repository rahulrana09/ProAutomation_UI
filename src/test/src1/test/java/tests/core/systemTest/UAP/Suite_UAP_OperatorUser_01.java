package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created on 10-07-2017.
 */
public class Suite_UAP_OperatorUser_01 extends TestInit {

    private OperatorUser netAdmin, cce;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        netAdmin = new OperatorUser(Constants.NETWORK_ADMIN);
        cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
    }

    /**
     * Test: TC197
     * Description: To verify that  valid user can add Operator
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.DEPENDENCY_TAG})
    public void addOptUser() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0115: Add Operator", "To verify that the valid user can add Operator User.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_SIT_5_0);

            OperatorUserManagement.init(t1)
                    .createAdmin(netAdmin);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Removed dependency because it gets executed after all cases executed
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0})
    public void modifyOptUser() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0111: Modify Operator", "To verify that the valid user can modify Operator user.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

            OperatorUserManagement.init(t1)
                    .initAndApproveModifyOrDeleteOptUsr(netAdmin, true);
        } finally {
            Assertion.finalizeSoftAsserts();
        }


    }


    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void deleteOptUser() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0112 : Delete Operator", "To verify that the valid user can delete Operator User.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_SIT_5_0);

            OperatorUserManagement.init(t1)
                    .initAndApproveModifyOrDeleteOptUsr(netAdmin, false);

        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    /**
     * TEST : View Self Details
     * DSEC : To verify that the valid user can delete subscriber.
     *
     * @throws Exception
     */


    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void viewSelfDetails() throws Exception {

        try {
            ExtentTest t1 = pNode.createNode("TC0113: View Self details", "To verify that the valid user can view Self details.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_SIT_5_0);

            OperatorUser viewSelf = DataFactory.getOperatorUserWithAccess("PTY_VSELF");

            if (viewSelf != null) {
                Login.init(t1).login(viewSelf);
                OperatorUserManagement.init(t1).viewSelfDetails(viewSelf);
            } else {
                t1.fail("Operator User not found with Access 'PTY_VSELF'. Please check Operator user and RnR Sheet.");
            }
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    /**
     * TC0114: View Operator details
     * DESC :To verify that the valid user can view Operator details.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void viewOptDetails() throws Exception {

        try {

            ExtentTest t1 = pNode.createNode("TC0114: View Operator details", "To verify that the valid user can view Operator details.");

            t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.OPERATER_USER, FunctionalTag.ECONET_SIT_5_0);

            OperatorUser viewOptDet = DataFactory.getOperatorUserWithAccess("PTY_VSU");

            Login.init(t1).login(viewOptDet);

            if (cce != null) {
                OperatorUserManagement.init(t1).viewOperatorDetails(cce);

            } else {
                t1.fail("Users not Found with Category CCE. Please check Operator User sheet and RnR sheet.");
            }
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
