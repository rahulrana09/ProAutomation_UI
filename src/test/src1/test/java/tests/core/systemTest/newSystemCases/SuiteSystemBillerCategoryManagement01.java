/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name:
 *  Date: 2-Jan-2018
 *  Purpose: Biller Category Management System Cases
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.BillerCategory;
import framework.entity.OperatorUser;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/**
 * Contains the cases for Biller Category Management
 *
 * @author navin.pramanik
 */
public class SuiteSystemBillerCategoryManagement01 extends TestInit {

    private static String description, updatedDesc;
    private static String categoryName, categoryCode;
    private static boolean isCatExist = false;
    private OperatorUser categoryCreator;
    private BillerCategory billerCategory, autBillCat;

    /**
     * prerequisite : Run Before Class
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        categoryCreator = DataFactory.getOperatorUserWithAccess("UTL_MCAT");
        categoryName = "Raftaar" + DataFactory.getRandomNumber(3);
        categoryCode = "CatCode" + DataFactory.getRandomNumber(3);
        description = "Automation";
        updatedDesc = "updated";
        billerCategory = new BillerCategory(categoryCode, categoryName, description);
        autBillCat = new BillerCategory(Constants.AUT_BILLER_CATEGORY);
    }

    /**
     * beforeMethod
     */
    @BeforeMethod(alwaysRun = true)
    public void beforeMethod() {
        billerCategory.setBillerCategoryName(categoryName);
        billerCategory.setBillerCategoryCode(categoryCode);
        billerCategory.setDescription(description);
        billerCategory.setStatus(Constants.STATUS_ACTIVE);

        autBillCat.setBillerCategoryName(Constants.AUT_BILLER_CATEGORY);
        autBillCat.setBillerCategoryCode(Constants.AUT_BILLER_CATEGORY);
        autBillCat.setDescription(Constants.AUT_BILLER_CATEGORY);
        autBillCat.setStatus(Constants.STATUS_ACTIVE);
    }


    /**
     * To check the Biller Category exist
     *
     * @param t1 Extent Test
     * @throws Exception
     */
    public void checkForBillerCategory(ExtentTest t1) throws Exception {

        Login.init(t1).login(categoryCreator);

        //it will check if Biller category already present or not
        if (!isCatExist)
            isCatExist = BillerManagement.init(t1).
                    checkBillerCategoryExist(autBillCat.BillerCategoryName);
        //If Biller Category not already present then add Biller Category
        if (!isCatExist)
            BillerManagement.init(t1).addBillerCategory(autBillCat);
    }

    public void checkForBillerCategory(BillerCategory billCat, ExtentTest t1) throws Exception {

        Login.init(t1).login(categoryCreator);

        //it will check if Biller category already present or not
        if (!isCatExist)
            isCatExist = BillerManagement.init(t1).
                    checkBillerCategoryExist(billCat.BillerCategoryName);
        //If Biller Category not already present then add Biller Category
        if (!isCatExist)
            BillerManagement.init(t1).addBillerCategory(billCat);
    }

    /**
     * TEST TYPE   : POSITIVE
     * TEST CASE   : SYS_TC_BILL_CATEGORY_N_0001 : Biiler Category Add Positive
     * DESCRIPTION : To verify that "Network Admin" should be able to add "Biller Category" in the system successfully with all valid inputs.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerCategoryAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0001 : Biiler Category Add Positive",
                "To verify that \"Network Admin\" should be able to add \"Biller Category\" in the system successfully with all valid inputs.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        categoryCreator = DataFactory.getOperatorUsersWithAccess("UTL_MCAT").get(0);

        Login.init(t1).login(categoryCreator);

        BillerManagement.init(t1).addBillerCategory(billerCategory);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0002 : Biller Category Negative
     * DESCRIPTION : To verify that "Network Admin" should not be able to add "Biller Category" in the system when "Category Name" field is left blank
     * in "Add Biller Category Page"  and Proper Error Message should be displayed at the GUI.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0002 : Biller Category Negative", "To verify that \"Network Admin\" should not be able to add \"Biller Category\" " +
                "in the system when \"Category Name\" field is left blank in \"Add Biller Category Page\"  and Proper Error Message should be displayed at the GUI.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        billerCategory.setBillerCategoryName(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.categoryName", "Negative", t1);
    }

    /**
     * TEST TYPE :  NEGATIVE
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0003 : Biller Category Negative
     * DESCRIPTION : To verify that "Network Admin" should not be able to add "Biller Category" in the system when
     * "Category Code" field is left blank in "Add Biller Category Page" .
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0003 : Biller Category Negative",
                "To verify that \"Network Admin\" should not be able to add \"Biller Category\" in the system when \"Category Code\" field is left blank in \"Add Biller Category Page\" .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        billerCategory.setBillerCategoryCode(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.categoryCode", "Negative", t1);
    }

    /**
     * TEST TYPE :
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0004 : Biller Category Negative
     * DESCRIPTION :To verify that "Network Admin" should not be able to add "Biller Category" in the system when
     * "Description" field is left blank in "Add Biller Category Page".
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative4() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0004 : Biller Category Negative", "To verify that \"Network Admin\" " +
                "should not be able to add \"Biller Category\" in the system when \"Description\" field is left blank in \"Add Biller Category Page\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        billerCategory.setDescription(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.description", "Negative", t1);
    }


    /**
     * TEST TYPE :
     * TEST CASE :SYS_TC_BILL_CATEGORY_N_0005 : Biller Category Negative
     * DESCRIPTION :To verify that "Network Admin" should not be able to add "Biller Category"
     * in the system when special characters are entered "Category Name" field .
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative5() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0005 : Biller Category Negative",
                "To verify that \"Network Admin\" should not be able to add \"Biller Category\" in the system when special characters are entered \"Category Name\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        billerCategory.setBillerCategoryName(Constants.SPECIAL_CHARACTER_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation().
                addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.allowed.alphanum.categoryName", "Negative", t1);
    }

    /**
     * TEST TYPE :
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0006 : Biller Category Negative
     * DESCRIPTION :To verify that "Network Admin" should not be able to add "Biller Category" in the system when special characters are entered in "Category Code" field .
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative6() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0006 : Biller Category Negative", "To verify that \"Network Admin\" should not be able to add \"Biller Category\" in the system when special characters are entered in \"Category Code\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        billerCategory.setBillerCategoryCode(Constants.SPECIAL_CHARACTER_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.allowed.alphanum.categoryCode", "Negative", t1);
    }


    /**
     * TEST TYPE : NEGATIVE
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0007 : Biller Category Negative
     * DESCRIPTION :To verify that when "Network Admin" click on "Back" button on "Add Biller Category" Page then it should navigate to Previous page.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative7() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0007 : Biller Category Negative", "To verify that when \"Network Admin\" " +
                "click on \"Back\" button on \"Add Biller Category\" Page then it should navigate to Previous page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .addBillerCategoryModuleButtonTest(billerCategory, Button.BACK);

    }

    /**
     * TEST TYPE : NEGATIVE
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0008 : Biller Category Negative
     * DESCRIPTION :To verify that when "Network Admin" click on "Back" button on "Add Biller Category ->Confirm"
     * Page then it should successfully navigate to Previous page.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0008 : Biller Category Negative", "To verify that when \"Network Admin\" " +
                "click on \"Back\" button on \"Add Biller Category ->Confirm\" Page then it should navigate to Previous page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .addBillerCategoryModuleButtonTest(billerCategory, Button.CONFIRM_BACK);

    }


    /**
     * TEST TYPE :
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0009
     * DESCRIPTION :To verify that "Network Admin" should not be able to add "Biller Category" in the system when "Category Name"  already exist in the system.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void SYS_TC_BILL_CATEGORY_N_0009() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0009 : Biller Category Negative", "To verify that \"Network Admin\" " +
                "should not be able to add \"Biller Category\" in the system when \"Category Name\"  already exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        checkForBillerCategory(billerCategory, t1);

        billerCategory.setBillerCategoryCode(DataFactory.getRandomNumberAsString(5));

        BillerManagement.init(t1).startNegativeTest()
                .addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.categoryName.alreadyexist", "Negative", t1);
    }

    /**
     * TEST TYPE :
     * TEST CASE :
     * DESCRIPTION :To verify that "Network Admin" should not be able to add "Biller Category" in the system when "Category Code"  already exist in the system.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billerNegative10() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0010 : Biller Category Negative",
                "To verify that \"Network Admin\" should not be able to add \"Biller Category\" in the system when \"Category Code\"  already exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        checkForBillerCategory(billerCategory, t1);

        //setting different category name that doesn't exist in the system
        billerCategory.setBillerCategoryName("zbc" + DataFactory.getRandomNumberAsString(3));

        BillerManagement.init(t1).startNegativeTest()
                .addBillerCategory(billerCategory);

        Assertion.verifyErrorMessageContain("utility.merCat.categoryCode.alreadyexist", "Negative Category Code Exist", t1);
    }


    /**
     * TEST TYPE :  Positive
     * TEST CASE :  SYS_TC_BILL_CATEGORY_N_0011 : Biller Category Update Negative
     * DESCRIPTION :To verify that the same values are visible in "Update Biller Category -> Confirm " Page  as in the Update Biller Page"
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg11() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0011 : Biller Category Update Positive",
                "To verify that the same values are visible in \"Update Biller Category -> Confirm \" Page  as in the Update Biller Page\"");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        autBillCat.setStatus(Constants.STATUS_ACTIVE);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategory(autBillCat)
                .verifyUpdateCategoryConfirmPageDetails(autBillCat);


    }


    /**
     * TEST TYPE :
     * TEST CASE :
     * DESCRIPTION :To verify that "Network Admin" should not be able to modify "Biller Category" in the system
     * if no modifications has been made in either name/description/status fields .
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg12() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0012 : Biller Category Update Negative", "To verify that \"Network Admin\" should not be able to modify \"Biller Category\" in the system if no modifications has been made in either name/description/status fields .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        Login.init(t1).login(categoryCreator);

        checkForBillerCategory(t1);

        BillerManagement.init(t1).startNegativeTest()
                .updateBillerCategory(autBillCat);

        Assertion.verifyErrorMessageContain("utility.merCat.updateCategoty.editSomething", "Negative", t1);
    }


    /**
     * TEST TYPE :
     * TEST CASE :
     * DESCRIPTION :To verify that "Network Admin" should not be able to modify "Biller Category" in the system when "Category Name" field is left blank
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg13() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0013 : Biller Category Update Negative",
                "To verify that \"Network Admin\" should not be able to modify \"Biller Category\" in the system when \"Category Name\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        autBillCat.setBillerCategoryName(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategory(autBillCat);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.categoryName", "Negative Case", t1);
    }


    /**
     * TEST TYPE :
     * TEST CASE :
     * DESCRIPTION :To verify that "Network Admin" should not be able to modify "Biller Category" in the system when "Description" field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg14() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0014 : Biller Category Negative", "To verify that \"Network Admin\" should not be able to modify \"Biller Category\" in the system when \"Description\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        autBillCat.setDescription(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategory(autBillCat);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.description", "Negative Case", t1);
    }


    /**
     * TEST TYPE: Negative
     * TEST NAME :SYS_TC_BILL_CATEGORY_N_0016
     * DESCRIPTION:To verify that "Network Admin" should not be able to modify "Biller Category" in the system when special characters are entered in  "Category Name" field .
     *
     * @throws Exception
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg15() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0015 : Biller Category Negative", "To verify that \"Network Admin\" should not be able to modify \"Biller Category\" in the system when special characters are entered in  \"Category Name\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        autBillCat.setBillerCategoryName(Constants.SPECIAL_CHARACTER_CONSTANT);

        BillerManagement.init(t1).startNegativeTest()
                .updateBillerCategory(autBillCat);

        Assertion.verifyErrorMessageContain("utility.merCat.allowed.alphanum.categoryName", "Negative Case", t1);
    }


    /**
     * TEST TYPE: Negative
     * TEST NAME :SYS_TC_BILL_CATEGORY_N_0016
     * DESCRIPTION:To verify that "Network Admin" should not be able to modify "Biller Category" in the system when "Status" field is not Selected.
     *
     * @throws Exception
     */
    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void billCategoryNeg16() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0016 : Biller Category Negative", "To verify that \"Network Admin\" should not be able to modify \"Biller Category\" in the system when \"Status\" field is not Selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        autBillCat.setStatus(Constants.BLANK_CONSTANT);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategory(autBillCat);

        Assertion.verifyErrorMessageContain("utility.merCat.empty.status", "Negative Case", t1);
    }


    /**
     * TEST TYPE : NEGATIVE
     * TEST CASE : SYS_TC_BILL_CATEGORY_N_0017
     * DESCRIPTION :To verify that when "Network Admin" click on "Back" button on "Modify Biller Category" Page
     * then it should navigate to Previous page.
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void categoryNegative17() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0017 : Biller Category Negative", "To verify that when \"Network Admin\" click on \"Back\" button on \"Modify Biller Category\" " +
                "Page then it should navigate to Previous page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategoryModuleButtonTest(autBillCat, Button.BACK);
    }


    /**
     * TEST TYPE : NEGATIVE
     * TEST CASE : SYS_TC_BILL_CATEGORY_N_0018
     * DESCRIPTION :To verify that when "Network Admin" click on "Back" button on
     * "Modify Biller Category ->Confirm" Page then it should navigate to Previous page.
     *
     * @throws IOException
     * @throws InvalidFormatException
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void categoryNegative18() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_BILL_CATEGORY_N_0018 : Biller Category Negative", "To verify that when \"Network Admin\" click on \"Back\" button on \"Modify Biller Category ->Confirm\" " +
                "Page then it should navigate to Previous page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);

        checkForBillerCategory(t1);

        Login.init(t1).login(categoryCreator);

        BillerManagement.init(t1).startNegativeWithoutConfirmation()
                .updateBillerCategoryModuleButtonTest(autBillCat, Button.CONFIRM_BACK);
    }

}
