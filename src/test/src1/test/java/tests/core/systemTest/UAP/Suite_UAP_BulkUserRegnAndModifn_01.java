package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.bulkUserRegnAndModification.BulkUserRegistrationAndModification;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Contain the cases for Bulk User registration and Modification
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BulkUserRegnAndModifn_01 extends TestInit {

    private static String usrCatCode;
    private static int noOfUsers = 2;
    private List<User> listOfUsersForBulkUpload;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        try {
            usrCatCode = Constants.WHOLESALER;

            listOfUsersForBulkUpload = getListOfUserObjectsByPassingNumberOfUser(noOfUsers);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * This method is to get list of User Objects (Fresh objects not created user list)
     *
     * @param noOfUsers : Pass no of User objects you need
     * @return
     * @throws Exception
     */
    private List<User> getListOfUserObjectsByPassingNumberOfUser(int noOfUsers) throws Exception {
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < noOfUsers; i++) {
            User userToAddInList = new User(usrCatCode);
            userList.add(userToAddInList);
        }
        return userList;
    }

    /**
     * TEST : TC0028 =>  Add Bulk User Registration and Modification
     * DESC : To verify that the valid user can add channel user through bulk.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void bulkUserRegnTestNew() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0028",
                "Add Bulk User Registration and Modification: To verify that the valid user can add channel user through bulk.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.BULK_CHANNEL_USER_REGISTRATION_AND_MODIFICATION, FunctionalTag.ECONET_UAT_5_0,
                        FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {


            BulkUserRegistrationAndModification bulkRegnModnObj = BulkUserRegistrationAndModification.init(t1);

            String filename = bulkRegnModnObj.
                    generateBulkUserRegnModnCsvFile(listOfUsersForBulkUpload, Constants.BULK_USER_STATUS_ADD, Constants.BULK_USER_STATUS_ADD);

            bulkRegnModnObj.initiateAndApproveBulkRegnOrModificn(filename, true);

            String status = MobiquityDBAssertionQueries
                    .getUserDBStatus(listOfUsersForBulkUpload.get(0));

            Assertion.verifyEqual(status, "Y", "Verify DB Status of newly created User", t1, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP,
            FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.ECONET_UAT_5_0, FunctionalTag.MONEY_SMOKE}, dependsOnMethods = "bulkUserRegnTestNew")
    public void bulkUserModnTestNew() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0029 : Add Bulk User Registration and Modification", "To verify that the valid user can MODIFY channel user through bulk.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.BULK_CHANNEL_USER_REGISTRATION_AND_MODIFICATION, FunctionalTag.ECONET_UAT_5_0);
        try {

            BulkUserRegistrationAndModification bulkRegnModnObj = BulkUserRegistrationAndModification.init(t1);

            String filename = bulkRegnModnObj.
                    generateBulkUserRegnModnCsvFile(listOfUsersForBulkUpload, Constants.BULK_USER_STATUS_MODIFY, Constants.BULK_USER_STATUS_MODIFY);

            bulkRegnModnObj.initiateAndApproveBulkRegnOrModificn(filename, true);

            for (User user : listOfUsersForBulkUpload) {
                String status = MobiquityDBAssertionQueries
                        .getUserDBStatus(user);

                Assertion.verifyEqual(status, "Y", "Verify DB Status of newly created User", t1, false);

            }

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
