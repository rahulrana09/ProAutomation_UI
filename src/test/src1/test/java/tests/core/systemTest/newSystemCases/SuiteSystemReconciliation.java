/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 28-Dec-2017
 *  Purpose: Test Of Reconciliation
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.reconciliationManagement.ReconciliationManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by surya.dhal on 12/28/2017.
 */
public class SuiteSystemReconciliation extends TestInit {

    private OperatorUser netAdmin;
    private User cashinUser, payee;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            netAdmin = DataFactory.getOperatorUserWithAccess("CHECK_ALL");
            cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);
            payee = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * To verify the Summary of Balances should be Zero after each and every Transaction.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECONCILIATION, FunctionalTag.PVG_P1, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_558() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_558" + getTestcaseid(),
                "To verify the Summary of Balances should be Zero after each and every Transaction");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECONCILIATION, FunctionalTag.PVG_P1);
        try {
            Login.init(t1).login(netAdmin);
            String summaryBalanceBefore = ReconciliationManagement.init(t1).getSummaryBalance();

            Login.init(t1).login(cashinUser);
            startNegativeTest();
            TransactionManagement.init(t1).performCashIn(payee,
                    Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            Login.init(t1).login(netAdmin);
            String summaryBalanceAfter = ReconciliationManagement.init(t1).getSummaryBalance();

            Assertion.verifyEqual(summaryBalanceAfter, summaryBalanceBefore, "Verify Summary Balance", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * To verify that difference between recon balance before and after transaction should be same.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.SYSTEM_TEST, FunctionalTag.RECONCILIATION, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_Reconciliation_S_0002() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_Reconciliation_S_0002" + getTestcaseid(),
                "To verify that difference between recon balance before and after transaction should be same.");
        t1.assignCategory(FunctionalTag.SYSTEM_TEST, FunctionalTag.RECONCILIATION);

        try {
            BigDecimal beforeReconBalance = MobiquityGUIQueries.getReconBalance();

            t1.info("Before Transaction Recon Balance is :" + beforeReconBalance);

            Login.init(t1).login(cashinUser);
            startNegativeTest();
            TransactionManagement.init(t1).performCashIn(payee,
                    Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            BigDecimal afterReconBalance = MobiquityGUIQueries.getReconBalance();

            t1.info("After Transaction Recon Balance is :" + afterReconBalance);

            Assertion.verifyEqual(afterReconBalance.toString(), beforeReconBalance.toString(), "Verify Reconciliation Balance", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * Test Method to verify Reconciliation On the fly
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.RECONCILIATION})
    public void P1_TC_184_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_184_1" + getTestcaseid(),
                "Reconciliation >> Reconciliation\n" +
                        "To verify Tally of \"Reconciliation\" of On the Fly(On Hold) before and after creating a subscriber.");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.RECONCILIATION);
        Login.init(t1).login(netAdmin);
        String beforeOnTheFlyOnHold = ReconciliationManagement.init(t1).getOnTheFlyOnHold();
        SubscriberManagement.init(t1).createSubscriberDefaultMapping(new User(Constants.SUBSCRIBER), true, false);
        Login.init(t1).login(netAdmin);
        String AfterOnTheFlyOnHold = ReconciliationManagement.init(t1).getOnTheFlyOnHold();
        Assertion.verifyEqual(beforeOnTheFlyOnHold, AfterOnTheFlyOnHold, "Verify On The Fly (On Hold)", t1);
    }

    /**
     * TEST  : POSITIVE
     * DESC : To verify Reconciliation of On The Fly Balance(D).
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.RECONCILIATION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_184_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_184_2" + getTestcaseid(),
                "Reconciliation >> Reconciliation\n" +
                        "To verify Tally of \"Reconciliation\" of On The Fly Balance(D) Before and After creating a subscriber.");
        t1.assignCategory(FunctionalTag.PVG_P1, FunctionalTag.RECONCILIATION);
        try {
            Login.init(t1).login(netAdmin);
            String beforeOnTheFlyBalance = ReconciliationManagement.init(t1).getOnTheFlyBalance();
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(new User(Constants.SUBSCRIBER), true, false);
            Login.init(t1).login(netAdmin);
            String afterOnTheFlyBalance = ReconciliationManagement.init(t1).getOnTheFlyBalance();
            Assertion.verifyEqual(beforeOnTheFlyBalance, afterOnTheFlyBalance, "Verify On The Fly Balance", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * To verify that admin user is able to view the reconciliation screen successfully
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.RECONCILIATION, FunctionalTag.MONEY_SMOKE})
    public void SYS_TC_Reconciliation_S_0004() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_Reconciliation_S_0004" + getTestcaseid(),
                "To verify that admin user is able to view the reconciliation screen successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.RECONCILIATION);

        try {
            Login.init(t1).login(netAdmin);

            ReconciliationManagement.init(t1).getSummaryBalance();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
