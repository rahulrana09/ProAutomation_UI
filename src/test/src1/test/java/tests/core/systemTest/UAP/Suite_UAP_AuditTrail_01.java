package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.adminTrailManagement.AuditAdminTrail;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/9/2017.
 */
public class Suite_UAP_AuditTrail_01 extends TestInit {

    public static String domainCode, categoryCode, fromDate, toDate;
    public static SuperAdmin saAuditTrail, saAdminTrail, saSysPref;
    private static String sysPrefCode, sysPrefValue;
    private static User cashinUsr, subs;
    private static OperatorUser operatorUser;

    /**
     * SETUP
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            categoryCode = Constants.WHOLESALER;
            domainCode = DataFactory.getDomainCode(categoryCode);
            //TODO Change from date and to date
            fromDate = new DateAndTime().getDate(0);
            toDate = new DateAndTime().getDate(0);

            saAuditTrail = DataFactory.getSuperAdminWithAccess("AUDIT_TRAIL");
            saAdminTrail = DataFactory.getSuperAdminWithAccess("ADMIN_TRAIL");
            cashinUsr = DataFactory.getChannelUserWithAccess("CIN_WEB", Constants.WHOLESALER);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            operatorUser = DataFactory.getOperatorUserWithAccess("PREF001");
            sysPrefCode = "CLIENT_SITE";
            sysPrefValue = AppConfig.clientSite;
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.MONEY_SMOKE})
    public void auditTrailTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC137 : Audit Trail",
                "To verify that the valid user can see details in Audit Trail and able to download file..")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.AUDIT_TRAIL);
        try {
            //make sure Channel user has sufficient balance
            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(cashinUsr);

            Login.init(t1).login(cashinUsr);

            String transID = TransactionManagement.init(t1).
                    performCashIn(subs, Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            if (transID == null) {
                Assertion.markAsFailure("Cannot proceed further as Cash In has Failed");
            } else {
                Login.init(t1).loginAsSuperAdmin(saAuditTrail);
                AuditAdminTrail.init(t1)
                        .verifyAuditTrail(cashinUsr, fromDate, toDate, transID);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM})
    public void adminTrailTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC138 : Admin Trail",
                "To verify that the valid user can see details in Admin Trail and able to download file..")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.AUDIT_TRAIL);

        try {
            Login.init(t1).login(operatorUser);

            Preferences.init(t1).modifySystemPreferences(sysPrefCode, sysPrefValue);

            Login.init(t1).loginAsSuperAdmin(saAdminTrail);

            String actionType = "Preferences Updated";

            AuditAdminTrail.init(t1).
                    adminTrailForPreferenceUpdate(operatorUser, actionType, fromDate, toDate, sysPrefCode, sysPrefValue);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


}
