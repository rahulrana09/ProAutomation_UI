package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.commissionDisbursement.CommissionDisbursement;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 10/30/2017.
 */
public class Suite_UAP_Commission_Disbursement extends TestInit {

    private OperatorUser netAdmin;
    private User chUser;

    public static void checkPostTxnDBValues(User payee) {
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, Constants.COMMISSION_WALLET, null);
        DBAssertion.postRecon = MobiquityGUIQueries.getReconBalance();
    }

    private void preRequisite(ExtentTest test) throws Exception {
        netAdmin = DataFactory.getOperatorUserWithAccess("COMMDIS_INITIATE");
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);

        if (netAdmin == null) {
            test.fail("Operator User not found with Role 'COMMDIS_INITIATE'. Please check Operator User sheet and RnR Sheet.");
            Assert.fail("Operator User not found with Role 'COMMDIS_INITIATE'. Please check Operator User sheet and RnR Sheet.");
        }
        if (chUser == null) {
            test.fail("Channel User not found with Category 'Wholesaler'. Please check Channel User sheet and RnR Sheet.");
            Assert.fail("Operator User not found with Category 'Wholesaler'. Please check Channel User sheet and RnR Sheet.");
        }
    }

    public void checkPreTxnDBValues(User user) {
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(user, Constants.COMMISSION_WALLET, null);
        DBAssertion.preRecon = MobiquityGUIQueries.getReconBalance();
    }

    /**
     * Test Method to Perform Commission Disbursement.
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM}, priority = 1)
    public void Commission_Disbursement() throws Exception {
        ExtentTest t1 = pNode.createNode("TC0050 :Commission Disbursement Procedure ",
                "To verify that the valid user can perform Commission Disbursement Procedure.\n").assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT);

        preRequisite(t1);

        checkPreTxnDBValues(chUser);

        TransactionManagement.init(t1)
                .performO2CforCommissionWallet(chUser, "100", "Test");

        Login.init(t1).login(netAdmin);

        CommissionDisbursement.init(t1).initiateCommissionDisbursement(chUser);

        Thread.sleep(5000);
        checkPostTxnDBValues(chUser);

        DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);
    }
}
