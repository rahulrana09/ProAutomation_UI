package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.stockManagement.StockTransferEaEnquiry_page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by navin.pramanik on 8/4/2017.
 */
public class Suite_UAP_StockManagement_02 extends TestInit {

    private static String stockEnqTxnID;
    private static String stockEnqAmount;
    private static String stockEAEnqTxnID;
    private static String stockEAEnqAmount;
    private static String fromDate;
    private static String toDate;
    private OperatorUser stockEnquiryUser, stockEAEnquiryUser, stockEAInitiator;
    private MobiquityGUIQueries dbHandler;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            stockEAInitiator = DataFactory.getOperatorUserWithAccess("STR_INIT");
            stockEnquiryUser = DataFactory.getOperatorUserWithAccess("STOCK_ENQ");
            stockEAEnquiryUser = DataFactory.getOperatorUserWithAccess("STOCKTR_ENQ");
            dbHandler = new MobiquityGUIQueries();
            stockEnqTxnID = dbHandler.dbGetLastTransID(Constants.STOCK_SERVICE_TYPE);
            stockEAEnqTxnID = dbHandler.dbGetLastTransID(Constants.EASTOCK_SERVICE_TYPE);
            stockEnqAmount = dbHandler.dbGetRequestedAmount(stockEnqTxnID);
            stockEAEnqAmount = dbHandler.dbGetRequestedAmount(stockEAEnqTxnID);
            fromDate = new DateAndTime().getDate(-30);
            toDate = new DateAndTime().getDate(0);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * @throws Exception
     */
    @Test(priority = 0, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0})
    public void stockEnquiryTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0131 : Stock Enquiry using TxnID",
                "To verify that valid user should be able to do Stock Enquiry").
                assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).login(stockEnquiryUser);

        StockManagement.init(t1).stockEnquiry(stockEnqTxnID);

    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void stockEAEnquiryTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0133", "Stock EA Enquiry using TxnID:To verify that valid user should be able to do Stock EA Enquiry");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(stockEAEnquiryUser);

        StockManagement.init(t1).initiateStockEATransferEnquiry(stockEAEnqTxnID);

    }

    @Test(priority = 1, groups = {FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC0133_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0133_a", "Stock EA Enquiry using TxnID:To verify that valid user should be able to do Stock EA Enquiry");

        t1.assignCategory(FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(stockEAInitiator);
            String Amount, RefNum, Remarks, defaultProvider;
            Amount = "12";
            RefNum = DataFactory.getRandomNumberAsString(4);
            Remarks = "Automation check";
            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String txnId = StockManagement.init(t1).initiateEAStock(RefNum, Amount, Remarks, DataFactory.getDefaultProvider().ProviderName);
            Login.init(t1).login(stockEAEnquiryUser);
            StockManagement.init(t1).initiateStockEATransferEnquiry(txnId);
            StockTransferEaEnquiry_page2 EnqPage2 = new StockTransferEaEnquiry_page2(pNode);
            String req = EnqPage2.getRequester();
            Assertion.verifyEqual(req, stockEAInitiator.getFirstNameAndLastName(), "Verify Requester", t1);
            String amt = EnqPage2.getRequestedAmount();
            Assertion.verifyEqual(amt, Amount, "Verify amount", t1);
            String date = EnqPage2.getRequestedDate();
            Assertion.verifyEqual(date, DateAndTime.getCurrentDate("dd/MM/yy"), "Verify Requested Date", t1);
            String refno = EnqPage2.getRefNo();
            Assertion.verifyEqual(refno, RefNum, "Verify Reference No.", t1);
            String txnId1 = EnqPage2.getTransactionID();
            Assertion.verifyEqual(txnId1, txnId, "Verify transaction id", t1);
            String status = EnqPage2.getCurrentStatus();
            Assertion.verifyEqual(status, "Transaction Initiated", "Verify status", t1);
            String provider = EnqPage2.getMFSProvider();
            Assertion.verifyEqual(provider, defaultProvider, "Verify provider", t1);
            String modifiedOn = EnqPage2.getModifiedOn();
            Assertion.verifyEqual(modifiedOn, "Modified On", "Verify modified on", t1);
            String remarks = EnqPage2.getRemarks();
            Assertion.verifyEqual(remarks, Remarks, "Verify remarks", t1);
            String mainAccountBalance = EnqPage2.getMainAccountBalance();
            BigDecimal ind01Balance = MobiquityGUIQueries.getBalanceSystemWallet("101", "101IND01");
            Assertion.verifyEqual(mainAccountBalance, ind01Balance.toString(), "Verify main account balance", t1);
            String earningAccountBalance = EnqPage2.getEarningAccountBalance();
            BigDecimal ind03Balance = MobiquityGUIQueries.getBalanceSystemWallet("101", "IND03");

            Assertion.verifyEqual(earningAccountBalance, ind03Balance.toString(), "Verify earning account balance", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void stockEnquiryDateRangeTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0132: Stock Transfer Enquiry Date Range", "To verify that valid user should be able to do Stock Enquiry using Date Range");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(stockEnquiryUser);

        StockManagement.init(t1).stockEnquiry_date(fromDate, toDate, MobiquityGUIQueries.dbGetTransactionStatus(stockEnqTxnID), stockEnqTxnID);
    }

    @Test(enabled = false, priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1})
    public void stockEAEnquiryDateRangeTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0134",
                "Stock EA Transfer Enquiry Date Range:To verify that valid " +
                        "user should be able to do Stock EA Enquiry using Date Range ")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).login(stockEAEnquiryUser);

        StockManagement.init(t1)
                .stockTransferEaEnquiry_Date(fromDate, toDate, Constants.TXN_STATUS_SUCCESS, stockEAEnqTxnID);
    }
}
