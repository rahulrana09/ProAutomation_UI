/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Automation Team
 *  Date: 9-Dec-2017
 *  Purpose: Feature of BlackListManagement
 */

package tests.core.systemTest.newSystemCases;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * /**
 * Created by febin.thomas on 11/24/2017.
 */
public class SuiteSystemServiceCharge extends TestInit {


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void serviceChargeAdd() throws Exception {

        ExtentTest t1 = pNode.createNode("TC028: serviceChargeAdd", "To verify that NetworkAdmin can add service charge.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);
        //This will initiate and approve Service charge

        try {
            User payer = new User(Constants.WHOLESALER);
            User payee = new User(Constants.RETAILER);
            ServiceCharge sCharge = new ServiceCharge(Services.C2C, payer, payee, null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}