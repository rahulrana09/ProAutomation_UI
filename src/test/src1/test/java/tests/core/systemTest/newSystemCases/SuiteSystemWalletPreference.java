package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.entity.WalletPreference;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by surya.dhal on 6/25/2018.
 */
public class SuiteSystemWalletPreference extends TestInit {

    private User subscriber;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        subscriber = new User(Constants.SUBSCRIBER);
    }

    /**
     * TEST : POSITIVE
     * ID: P1_TC_021
     * DESC : To verify that "Superadmin" should be able to configure the default TCP, Grade and Mobile Group role to the wallets of the Customer who has registered in the mobiquity® system through "Self Registration" mode
     * and Verify Subscriber getting that TCP Grade and Mobile Group Role.
     */
    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.WALLET_PREFERENCES})
    public void P1_TC_021_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_021_1", "To verify that \"Superadmin\" should be able to configure the default TCP, Grade and Mobile Group role to the wallets of the Customer who has registered in the mobiquity® system through \"Self Registration\" mode " +
                "and Verify Subscriber getting that TCP Grade and Mobile Group Role.");

        SuperAdmin superAdmin = DataFactory.getSuperAdminWithAccess("CAT_PREF");

        subscriber = new User(Constants.SUBSCRIBER);

        WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                Constants.REGTYPE_SUBS_INIT_USSD,
                DataFactory.getDefaultWallet().WalletName, true,
                null, subscriber.GradeName);

        Login.init(t1).loginAsSuperAdmin(superAdmin);
        Preferences.init(t1).configureWalletPreferences(wPref);

        Transactions.init(t1).selfRegistrationForSubscriber(subscriber);

        String actualTCPID = MobiquityGUIQueries.getUserTCPName(subscriber);
        String actualGrade = MobiquityGUIQueries.dbGetUserGrade(subscriber);

        Assertion.verifyEqual(actualTCPID, wPref.TCP, "Verify Default TCP Applied Or not", t1, false);
        Assertion.verifyEqual(actualGrade, wPref.GradeName, "Verify Default GradeName Applied Or not", t1, false);

        String actualMobileGroupRole = MobiquityGUIQueries.dbGetSubsMobileGroupRole(subscriber);
        Assertion.verifyEqual(actualMobileGroupRole, wPref.mobileRole, "Verify Default Mobile Group  Applied Or not", t1, false);


    }

    /**
     * TEST : POSITIVE
     * ID: P1_TC_022_1
     * DESC : To verify that "Superadmin" should be able to configure the default TCP, Grade and Mobile Group role
     * to the wallets of the Customer who has registered in the mobiquity® system by any "Channel User"  and Verify Subscriber getting that TCP Grade and Mobile Group Role.
     */
    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.WALLET_PREFERENCES})
    public void P1_TC_021_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_021_2", "To verify that \"Superadmin\" should be able to configure the default TCP, Grade and Mobile Group role " +
                "to the wallets of the Customer who has registered in the mobiquity® system by any \"Channel User\"  and Verify Subscriber getting that TCP Grade and Mobile Group Role.");

        t1.assignCategory(FunctionalTag.WALLET_PREFERENCES, FunctionalTag.PVG_P1);

        SuperAdmin superAdmin = DataFactory.getSuperAdminWithAccess("CAT_PREF");

        subscriber = new User(Constants.SUBSCRIBER, DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(1).GradeName);

        WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                Constants.REGTYPE_NO_KYC,
                DataFactory.getDefaultWallet().WalletName, true,
                null, subscriber.GradeName);


        Login.init(t1).loginAsSuperAdmin(superAdmin);
        Preferences.init(t1).configureWalletPreferences(wPref);

        Transactions.init(t1).SubscriberRegistrationByChannelUserWithKinDetails(subscriber);

        String actualTCPID = MobiquityGUIQueries.getUserTCPName(subscriber);

        Assertion.verifyEqual(actualTCPID, wPref.TCP, "Verify Default TCP Applied Or not", t1, false);

        String actualGrade = MobiquityGUIQueries.dbGetUserGrade(subscriber);
        Assertion.verifyEqual(actualGrade, wPref.GradeName, "Verify Default GradeName Applied Or not", t1, false);

        String actualMobileGroupRole = MobiquityGUIQueries.dbGetSubsMobileGroupRole(subscriber);
        Assertion.verifyEqual(actualMobileGroupRole, wPref.mobileRole, "Verify Default Mobile Group  Applied Or not", t1, false);


    }


    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.WALLET_PREFERENCES})
    public void P1_TC_022_1() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_022_1", "To Verify that When \"Super Admin\"  selects Domain and Category as Dealer  then the system will not ask you to select the registration type.");

        t1.assignCategory(FunctionalTag.WALLET_PREFERENCES, FunctionalTag.PVG_P1);

        SuperAdmin superAdmin = DataFactory.getSuperAdminWithAccess("CAT_PREF");

        WalletPreference wPref = new WalletPreference(Constants.WHOLESALER,
                Constants.REGTYPE_SUBS_INIT_USSD,
                DataFactory.getDefaultWallet().WalletName, true,
                null, null);

        Login.init(t1).loginAsSuperAdmin(superAdmin);
        WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(t1);
        page.navMapWalletPreferences();
        page.selectDomainName(wPref.DomainName);
        page.selectCategoryName(wPref.CategoryName);
        page.checkElementEnabledOrDesabled();
    }


    @Test(groups = {FunctionalTag.PVG_P1, FunctionalTag.WALLET_PREFERENCES})
    public void P1_TC_022_2() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_022_2", "To Verify that When \"Super Admin\"  selects Domain and Category as  merchant then the system will not ask you to select the registration type.");

        t1.assignCategory(FunctionalTag.WALLET_PREFERENCES, FunctionalTag.PVG_P1);

        SuperAdmin superAdmin = DataFactory.getSuperAdminWithAccess("CAT_PREF");

        WalletPreference wPref = new WalletPreference(Constants.MERCHANT,
                Constants.REGTYPE_SUBS_INIT_USSD,
                DataFactory.getDefaultWallet().WalletName, true,
                null, null);

        Login.init(t1).loginAsSuperAdmin(superAdmin);
        WalletPreferences_Pg1 page = WalletPreferences_Pg1.init(t1);
        page.navMapWalletPreferences();
        page.selectDomainName(wPref.DomainName);
        page.selectCategoryName(wPref.CategoryName);
        page.checkElementEnabledOrDesabled();
    }

}
