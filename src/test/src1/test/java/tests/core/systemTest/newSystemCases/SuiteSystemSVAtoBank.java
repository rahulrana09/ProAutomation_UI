/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *   Author Name: Febin Thomas
 *  Date: 9-Dec-2017
 *  Purpose: Feature of BlackListManagement
 */

package tests.core.systemTest.newSystemCases;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.svaToOwnBankTransfer.svaToOwnBankTransfer_Page1;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.SentSMS;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * /**
 * Created by febin.thomas on 11/24/2017.
 */
public class SuiteSystemSVAtoBank extends TestInit {


    public static String provider, walletName, bankName;

    User chUser, commonUser;
    Biller susBiller;
    OperatorUser optUser, bankApprover, channelModify;


    /**
     * SETUP
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        try {
            provider = DataFactory.getDefaultProvider().ProviderName;
            walletName = DataFactory.getDefaultWallet().WalletName;
            bankName = DataFactory.getDefaultBankNameForDefaultProvider();
            commonUser = new User(Constants.WHOLESALER);
            optUser = DataFactory.getOperatorUserWithAccess("CHECK_ALL");
            ExtentTest t1 = pNode.createNode("Create Common Channel user.");

            //ChannelUserManagement.init(t1).createChannelUser(commonUser, true);
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");
            channelModify = DataFactory.getOperatorUsersWithAccess("PTY_MCU").get(0);
//        susBiller= CommonUserManagement.init(pNode).getSuspendedBiller("WBILLMER","","GWS");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }


    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_016() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_016",
                "To verify that proper prompt should get displayed once user click on Cancel button on confirmation screen of Transfer now page");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(pNode);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "10");

        page1.clickCancel();

        Assertion.alertAssertion("confirmation.scheduledTransfer.now.cancel", t1);

    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_002 :" + getTestcaseid() + " SVA To Bank", "To verify that \"Wallet Balance\" field must be disabled at \"SVA to Own Bank Account\" page.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVAtoBankWalletBalance(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "Monday");
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_3() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_003 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Total Balance\" option in amount type, the user is able to submit the request for gold dealer at \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Total_Balance(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "Monday");
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_041() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_041", "To verify that all Remarks field characters are getting visible in 'SVA to Own Bank Transfer' page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        Login.init(t1).login(chUser);

        String remarks = "My Remarks Ok";

        svaToOwnBankTransfer_Page1 page1 = new svaToOwnBankTransfer_Page1(t1);

        //TODO - Input values to fetch from sheet
        page1.navigateToSVA2OwnBankTransferInWeb();
        page1.selectTransferType(Constants.SVA_TRANSFER_NOW);
        page1.clickNext();

        page1.selectProviderName(provider);
        page1.selectWallet(walletName);

        page1.selectAccountByIndex();
        page1.selectAmountType();
        page1.setTransferAmount("10");

        page1.setRemarks(remarks);
        page1.clickSubmit();
        page1.clickBack();

        Assertion.verifyEqual(page1.getRemarksTextFromGui(), remarks, "Check Remarks Entered", t1);
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void svaSystem_5() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_042_1", "To verify that fixed amount should not accept alphabets while doing Transfer Now transaction in web by logged-in channel user.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.ALPHABET_CONSTANT);

        Assertion.verifyErrorMessageContain("error.amount.invalid", "Invalid Data entered ", t1);
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_042_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_042_2", "To verify that fixed amount should not accept Special Characters while doing Transfer Now transaction in web by logged-in channel user.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SPECIAL_CHARACTER_CONSTANT);

        Assertion.verifyErrorMessageContain("error.amount.invalid", "Invalid Data entered ", t1);
    }


    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1})
    public void P1_TC_108() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_108",
                "To verify that If user selects the option to go back in transfer now confirmation page, system should take the user to previous screen with amount field filled with entered value.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        String txnAmount = "1";

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, txnAmount);

        svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(t1);

        page1.clickConfirmBack();

        Assertion.verifyEqual(page1.checkSubmitDisplayed(), true, "Check Submit Visible After Back Button Click on Confirm Page", t1);

        Assertion.verifyEqual(page1.getAmountFromGui(), txnAmount, "Verify Amount", t1);
    }


    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_7() throws Exception {

        Map<String, User> channelUserSet = DataFactory.getChannelUserSetUniqueGrade();
        List<User> payerList = new ArrayList<User>();

        for (User user : channelUserSet.values()) {
            if (!(user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER) || user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE))) {
                payerList.add(user);
            }
        }

        for (User payer : payerList) {
            try {
                ExtentTest t1 = pNode.createNode("SYS_TC_F_0007" + getTestcaseid() + " : SVA To Bank",
                        "To verify that \"SVA to Own bank Schedule Transfer\" transfer should be successful for " + DataFactory.getGradeName(payer.GradeCode));

                ServiceCharge scharge = new ServiceCharge(Services.SVA_TO_BANK, payer, payer, null, null, null, null);

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

                ServiceChargeManagement.init(t1).configureServiceCharge(scharge);

                ConfigInput.isConfirm = true;
                //For Login with valid user
                Login.init(t1).login(payer);

                TransactionManagement svaTxnObj = TransactionManagement.init(t1);

                svaTxnObj.initiateScheduledSVAtoBank(payer, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);

                svaTxnObj.SVA_Schedule_Delete(payer);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_8() throws Exception {

        Map<String, User> channelUserSet = DataFactory.getChannelUserSetUniqueGrade();
        List<User> payerList = new ArrayList<User>();

        for (User user : channelUserSet.values()) {
            if (!((user.CategoryCode.equalsIgnoreCase(Constants.SUBSCRIBER)) || (user.CategoryCode.equalsIgnoreCase(Constants.ENTERPRISE)))) {
                payerList.add(user);
            }
        }

        for (User payer : payerList) {
            try {
                ExtentTest t1 = pNode.createNode("SYS_TC_F_0008" + getTestcaseid() + ": SVA To Bank",
                        "To verify that \"SVA to Own bank Transfer Now\" transfer should be successful for " + payer.GradeName);

                ServiceCharge scharge = new ServiceCharge(Services.SVA_TO_BANK, payer, payer, null, null, null, null);

                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

                ServiceChargeManagement.init(t1).configureServiceCharge(scharge);
                //For Login with valid user
                Login.init(t1).login(payer);

                String transID = TransactionManagement.init(t1).initiateSVAtoBank(payer, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);

                SentSMS.verifyPayeeDBMessage(payer, payer, Services.SVA_TO_BANK, transID, t1);

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_19() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_019 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting Fixed Amount option in amount type, the user is able to enter the amount to be transferred and submit the request for gold dealer at \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
    }


    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_20() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_020 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"blank\" value for field \"fixed amount\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.BLANK_CONSTANT);
        Assertion.verifyErrorMessageContain("error.amount.invalid", "Invalid Data entered ", t1);
    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_21() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_021 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"Decimal till 3 places\" value for field \"fixed amount\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.DECIMAL_SPECIAL);
        Assertion.verifyErrorMessageContain("si.system.error.corrDecFormat", "Invalid Data entered ", t1);
    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_22() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_022 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user doesn’t select value for field \"Account to be credited\"   at \"SVA to Own Bank Account\" page.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_BlankAC(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
        Assertion.verifyErrorMessageContain("error.select.accountNo", "No bank account selected ", t1);
    }


    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_23() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_023:" + getTestcaseid() + "  SVA To Bank", "To verify that user should be able to go back to \"Transfer type selection\" page on clicking \"cancel\" button.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Cancel_Prompt_1(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "Monday");
    }


    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_24() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_024:" + getTestcaseid() + "  SVA To Bank", "To verify that user shoulde be able to go to \"SVA to Own bank transfer\" page on clicking \"Next\" on the \"Transfer type selection\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_NextCheck(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "Monday");
    }



    /*
    SVA to bank Scheduled transfer*/

    @Test(priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_25() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_025:" + getTestcaseid() + "  SVA To Bank", "To verify that user should be able to go back to \"Transfer type selection\" page on clicking \"cancel\" button.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Cancel_Prompt_1(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday");

    }


    @Test(priority = 16, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_26() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_026:" + getTestcaseid() + "  SVA To Bank", "To verify that user should be able to go to \"SVA to Own bank transfer\" page on clicking \"Next\" on the \"Transfer type selection\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_NextCheck(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday");
    }

    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_27() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_027 :" + getTestcaseid() + "  SVA To Bank", "To verify that \"Wallet Balance\" field must be disabled at \"SVA to Own Bank Account\" page.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVAtoBankWalletBalance(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday");
    }


    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_28() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_028 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Total Balance\" option in amount type, the user is able to submit the request for gold dealer at \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Total_Balance(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday");
    }

    @Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_29() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_029 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting Fixed Amount option in amount type, the user is able to enter the amount to be transferred and submit the request for gold dealer at \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).SVA_Schedule_Delete(chUser);
    }

    @Test(priority = 20, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_30() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_030 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"blank\" value for field \"fixed amount\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.BLANK_CONSTANT, Constants.SVA_SCHEDULE_OCCURANCE);
        Assertion.verifyErrorMessageContain("error.amount.invalid", "Please enter fixed amount to be transfered ", t1);
    }

    /*@Test(priority = 19, groups = {FunctionalTag.PVG_SYSTEM,FunctionalTag.SVA})
    public void svaSystem_trial() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_Blocked : SVA To Bank", "To Verify that \"channel user\" should not be able to  perform \"SVA to Bank\" when \"PAYER\" is \"Barred as sender\".\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        ChannelUserManagement.init(t1).barChannelUser(commonUser, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_SENDER);

        //TransactionManagement.init(t1).initiateScheduledSVAtoBank(commonUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT);
        Login.init(t1).tryLogin(commonUser.LoginId, commonUser.Password);
        Login.resetLoginStatus();

        Assertion.verifyErrorMessageContain("00036", "Verify Error Message", t1);

        Login.init(t1).login(optUser);
        ChannelUserManagement.init(t1).unBarChannelUser(commonUser, Constants.USER_TYPE_CHANNEL);

    }
*/
    @Test(priority = 21, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_31() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_031 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"Decimal till 3 places\" value for field \"fixed amount\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.DECIMAL_SPECIAL, Constants.SVA_SCHEDULE_OCCURANCE);

        Assertion.verifyErrorMessageContain("si.system.error.corrDecFormat", "Please enter fixed amount to be transfered ", t1);
    }

    @Test(priority = 22, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_32() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_032" + getTestcaseid() + "  : SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"blank\" value for field \"Number of occurance\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.BLANK_CONSTANT);
        Assertion.verifyErrorMessageContain("error.occurrence.required", "Please enter valid Number of occurrences", t1);
    }

    @Test(priority = 23, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_33() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_033 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error message is displayed when \"Dealer\" user enters \"Decimal\" value for field \"Number of occurance\" at \"Schedule Transfer\" on \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, ".2");
        Assertion.verifyErrorMessageContain("error.occurrence.numeric", "Please enter valid Number of occurrences", t1);
    }

    @Test(priority = 24, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_34() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_034 :" + getTestcaseid() + "  SVA To Bank", "To verify that propper error message is displayed when \"Dealer\" user doesn’t select value for field \"Account to be credited\"   at \"SVA to Own Bank Account\" page.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).SVA_Schedule_BlankAC(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);

        Assertion.verifyErrorMessageContain("error.select.accountNo", "No bank account selected", t1);
    }

    @Test(priority = 25, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_35() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_035 :" + getTestcaseid() + "  SVA To Bank", "To verify that total amount is transferred on selecting \"Total Amount\" on \"SVA to own bank Account page\".\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_TotalBalance(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);

    }

    @Test(priority = 26, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_36() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_036 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Weekly\" under scheduled transfer , \"Days\" dropdown is enabled.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).SVA_Schedule_Delete(chUser);
    }


    @Test(priority = 27, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_37() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_037 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Daily\" under scheduled transfer user is able to perform \"SVA To bank transfer\".");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Daily(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).SVA_Schedule_Delete(chUser);
    }


    @Test(priority = 28, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_38() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_038 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Monthly\" under scheduled transfer , \"Days\" and \"Months\"  dropdown is enabled.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Monthly(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).SVA_Schedule_Delete(chUser);

    }

    @Test(priority = 29, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_39() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_039 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"Yearly\" under scheduled transfer user is able to perform \"SVA To bank transfer\".\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Yearly(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).SVA_Schedule_Delete(chUser);

    }

    @Test(priority = 30, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_40() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_040 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error is thrown on entering \"Junk data\"(&^%$#) in \"Number of occurance\" text field  of \"SVA to own bank Account page\"\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, "%$");
        Assertion.verifyErrorMessageContain("error.occurrence.numeric", "Please enter valid Number of occurrences", t1);
    }


    @Test(priority = 31, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_41() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_041 :" + getTestcaseid() + "  SVA To Bank", "To verify that proper error is thrown on entering \"alphabets\" in \"Number of occurance\" text field  of \"SVA to own bank Account page\"\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, "as");
        Assertion.verifyErrorMessageContain("error.occurrence.numeric", "Please enter valid Number of occurrences", t1);
    }


    @Test(priority = 32, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_42() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_042 :" + getTestcaseid() + "  SVA To Bank", "To verify that \"Schedule transfer\" is initiated on clicking \"confirm\" button on the \"SVA to bank confirm page\"\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).
                initiateScheduledSVAtoBank(chUser, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE).
                SVA_Schedule_Delete(chUser);
    }

    @Test(priority = 33, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_43() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_043 :" + getTestcaseid() + "  SVA To Bank", "To verify that \"Schedule transfer\" is canceled on clicking \"cancel\" button on the \"SVA to bank confirm page\"\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Cancel(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);
    }

    @Test(priority = 34, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_44() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_044 :" + getTestcaseid() + "  SVA To Bank", "To verify that on selecting \"End Date\" under \"End scheduler transfer type\" of \"SVA to own bank Account page\" Calender is enabled.\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_CheckCalvisi(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);
    }


    @Test(priority = 35, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_45() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_045 :" + getTestcaseid() + "  SVA To Bank", "To verify that user is able to go back to the \"SVA to bank schedule transfer page\" on clicking \"back\" button on the \"SVA to bank confirm page\"\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).SVA_Schedule_Confirm_Back(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", Constants.SVA_AMOUNT, Constants.SVA_SCHEDULE_OCCURANCE);
    }


    /*@Test(priority = 36, groups = {FunctionalTag.PVG_SYSTEM,FunctionalTag.SVA})
    public void svaSystem_46() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_046 : SVA To Bank", "To verify that propper error is displayed when \"transfer amount\" is more than the \"wallet balance\" on \"SVA to own bank Account bank\" page\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        TransactionManagement.init(t1).startNegativeTest().negativeTestWithoutConfirm().initiateScheduledSVAtoBank(chUser, Constants.SVA_SCHEDULE_TRANSFER, provider, walletName, bankName, "Monday", "20000000", Constants.SVA_SCHEDULE_OCCURANCE);*//*.SVA_Schedule_Delete(chUser);*//*
        Assertion.verifyErrorMessageContain("fixed.amt.more", "Fixed amount more than the wallet balance", t1);

    }*/


    @Test(priority = 36, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void svaSystem_46() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_046 :" + getTestcaseid() + "  SVA To Bank", "To verify that propper error is displayed when \"transfer amount\" is more than the \"wallet balance\" on \"SVA to own bank Account bank\" page\n");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        startNegativeTestWithoutConfirm();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, "2000000");
        Assertion.verifyErrorMessageContain("fixed.amt.more", "Fixed amount more than the wallet balance", t1);
    }


    @Test(priority = 37, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_168() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_168", "Check the reconciliation screen to verify the mismatch found.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        BigDecimal prebal = MobiquityGUIQueries.getReconBalance();

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);

        BigDecimal postBalFromDB = MobiquityGUIQueries.getReconBalance();

        Assertion.verifyEqual(prebal, postBalFromDB, "Verify the Recon balance ", t1);
    }


    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_048
     * DESC : Perform "SVA to Bank" when amount is less that "MIN service charge" defined.
     *
     * @throws Exception
     */
    @Test(priority = 38, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void SYS_TC_F_048() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_048",
                "To Verify that \"channel user\" should not be able to  perform \"SVA to Bank\" when amount is less that \"MIN service charge\" defined.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, chUser, chUser, null, null, null, null);

        ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

        BigDecimal minValue = MobiquityGUIQueries.getMinTransferValue(sCharge.ServiceChargeName);
        minValue = minValue.divide(new BigDecimal("2"));

        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, minValue.toString());
        Assertion.verifyErrorMessageContain("sva.error.less.amount.servicecharge", "Verify SVA to Bank Transfer when amount is less that \"MIN service charge\" defined.", t1);
    }

    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_049
     * DESC : Perform "SVA to Bank" when amount is more than "MAX service charge" defined.
     *
     * @throws Exception
     */
    @Test(priority = 39, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void SYS_TC_F_049() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_049",
                "To Verify that \"channel user\" should not be a`ble to  perform \"SVA to Bank\" when amount is more than \"MAX service charge\" defined.");

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, chUser, chUser, null, null, null, null);

        ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

        BigDecimal maxValue = MobiquityGUIQueries.getMaxTransferValue(sCharge.ServiceChargeName);
        maxValue = maxValue.add(new BigDecimal("2"));

        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, maxValue.toString());
        Assertion.verifyErrorMessageContain("sva.fixed.amount.more.than.wallet.balance", "Verify SVA to Bank Transfer", t1);
    }

    /**
     * TEST : NEGATIVE
     * TCID : P1_TC_517_1
     * DESC : Perform "SVA to Bank " when Channel User"  is "Barred as sender
     *
     * @throws Exception
     */
    @Test(priority = 40, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_517_1() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_517_1",
                "To Verify that \"channel user\" should not be able to perform \"SVA to Bank\" when \"the same Channel User\" is \"Barred as sender\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);


        User user = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
//        User user = new User(Constants.WHOLESALER);

        //user = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER,Constants.BAR_AS_SENDER,null);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(user.LoginId, user.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("00036", "Verify SVA to Bank Transfer Channel User is Barred as sender", t1);
        } else {
            startNegativeTest();
            TransactionManagement.init(t1).initiateSVAtoBank(user, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("SVA.error.bankAccountexits.required", "Verify SVA to Bank Transfer Channel User is Barred as sender", t1);
        }

    }

    /**
     * TEST : NEGATIVE
     * TCID : P1_TC_517_2
     * DESC : Perform "SVA to Bank " when Channel User"  is "Barred as Both"
     *
     * @throws Exception
     */
    @Test(priority = 40, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_517_2() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_517_2",
                "To Verify that \"channel user\" should not be able to perform \"SVA to Bank\" when \"the same Channel User\" is \"Barred as Both\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        User user = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_BOTH, null);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(user.LoginId, user.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("00036", "Verify SVA to Bank Transfer Channel User is Barred as Both", t1);
        } else {
            startNegativeTest();
            TransactionManagement.init(t1).initiateSVAtoBank(user, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("SVA.error.bankAccountexits.required", "Verify SVA to Bank Transfer Channel User is Barred as sender", t1);
        }

    }

    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_052
     * DESC : Perform "SVA to Bank " when Channel User"  is suspended
     *
     * @throws Exception
     */
    @Test(priority = 42, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void SYS_TC_F_052() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_052",
                "To Verify that \"channel user\" should not be able to  perform \"SVA to Bank\" when  \" the same channel user\" is \"Suspended\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        chUser = CommonUserManagement.init(t1)
                .getSuspendedUser(Constants.WHOLESALER, null);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(chUser.LoginId, chUser.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("user.inactive", "Verify SVA to Bank Transfer Channel User is Suspended", t1);
        } else {
            startNegativeTestWithoutConfirm();
            TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("sususer.message.required", "Verify SVA to Bank Transfer Channel User is suspended", t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_051
     * DESC : Perform "SVA to Bank " when Channel User"  is "Barred as receiver
     *
     * @throws Exception
     */
    @Test(priority = 41, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA})
    public void SYS_TC_F_051() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_F_051",
                "To Verify that \"channel user\" should not be able to  perform \"SVA to Bank\" when \"the same Channel User\"  is \"Barred as Receiver\".");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        chUser = CommonUserManagement.init(t1)
                .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(chUser.LoginId, chUser.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("00036", "Verify SVA to Bank Transfer Channel User is Barred as sender", t1);
        } else {

            startNegativeTestWithoutConfirm();
            TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("error.amount.maxValue", "Verify SVA to Bank Transfer Channel User is Barred as receiver", t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_054
     * DESC : Perform "SVA to Bank " when bank is not associated with the biller.
     *
     * @throws Exception
     */
    @Test(priority = 43, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_165() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_165",
                "To verify that biller should not be able to do SVA to Self Bank accounts transfer transaction if bank is not associated with the biller.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        Biller biller = BillerManagement.init(t1).getDefaultBiller();

        Login.init(t1).login(biller);

        svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);
        page1.navigateToSVA2OwnBankTransferInWeb();
        page1.selectTransferType(Constants.SVA_TRANSFER_NOW);
        page1.clickNext();
        Assertion.verifyErrorMessageContain("SVA.error.bankAccountexits.required", "when bank is not associated with the Biller.", t1);

    }

    /**
     * TEST : NEGATIVE
     * TCID :
     * DESC :
     *
     * @throws Exception
     */
    @Test(priority = 44, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_515() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_515",
                "To verify that channel user should not able to do SVA to Self Bank accounts transfer transaction if bank account of channel user  is suspended.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);
        //Suspend Channel User Bank Account Status
        Login.init(t1).login(optUser);

        commonUser.MSISDN = "0689621207";
        commonUser.FirstName = "WHS0689621207";
        commonUser.LastName = "WHS0689621207";
        commonUser.LoginId = "WHS0689621207";

        ChannelUserManagement channelUserManagement = ChannelUserManagement.init(t1);

        channelUserManagement.initiateChannelUserModification(commonUser);

        CommonChannelUserPage page = CommonChannelUserPage.init(pNode);

        FunctionLibrary fl = FunctionLibrary.init(t1);

        if (fl.elementIsDisplayed(page.nextUserInfo)) {
            page.clickNextUserDetail();
        }

        if (fl.elementIsDisplayed(page.nextHierarcy)) {
            page.clickNextUserHeirarcy();
        }

        if (fl.elementIsDisplayed(page.nextGroupRole)) {
            page.clickNextWebRole();
        }

        if (fl.elementIsDisplayed(page.nextWalletMap)) {
            page.clickNextWalletMap();
        }

        if (fl.elementIsDisplayed(page.nextBankPref)) {
            page.clickNextBankMapModification();
        }

        Login.init(t1).login(channelModify);
        channelUserManagement.modifyUserApproval(commonUser);

        Login.init(t1).login(bankApprover);
        CommonUserManagement.init(t1)
                .approveAllAssociatedBanks(commonUser);

        Login.init(t1).login(commonUser);

        TransactionManagement.init(t1).initiateSVAtoBank(commonUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
        Assertion.verifyErrorMessageContain("SVA.error.bankAccountexits.required", "when bank accont of Biller  is suspended.", t1);

        //Resume Channel User Bank Account Status
        /*Login.init(t1).login(optUser);
        ChannelUserManagement.init(t1).initiateChannelUserModification(commonUser).
                completeChannelUserModification().
                modifyUserApproval(commonUser);*/
    }


    @Test(priority = 45, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_516() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_516",
                "To verify that channel user should not able to do SVA to Self Bank accounts transfer transaction if Wallet of channel user  is suspended.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        chUser = CommonUserManagement.init(t1).getSuspendedUser("WHS", "GWS");
        //chUser = BarSuspendUser.getSuspendedChannelUser().get(0);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(chUser.LoginId, chUser.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("user.inactive", "Verify SVA to Bank Transfer Channel User is Suspended", t1);
        } else {
            startNegativeTestWithoutConfirm();
            TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("sususer.message.required", "Verify SVA to Bank Transfer Channel User is suspended", t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * TCID :
     * DESC : Perform "SVA to Bank " when Biller is suspended
     *
     * @throws Exception
     */
    @Test(priority = 45, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_167() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_167",
                "To verify that biller should not able to do SVA to Self Bank accounts transfer transaction if Biller is suspended in the system");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        //TODO 3 cases pending , suspend biller, suspend bank account of biller and channel user
        //susBiller= CommonUserManagement.init(t1).getSuspendedBiller("WBILLMER",Constants.SUSPEND,null);

        if (ConfigInput.is4o9Release) {
            Login.init(t1).tryLogin(susBiller.LoginId, susBiller.Password);
            Login.resetLoginStatus();
            Assertion.verifyErrorMessageContain("user.inactive", "Verify SVA to Bank Transfer Channel User is Suspended", t1);
        } else {
            startNegativeTestWithoutConfirm();
            TransactionManagement.init(t1).initiateSVAtoBank(susBiller, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
            Assertion.verifyErrorMessageContain("sususer.message.required", "Verify SVA to Bank Transfer Channel User is suspended", t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * TCID : P1_TC_514
     * DESC : Perform "SVA to Bank " when bank is not associated with the channel user.
     *
     * @throws Exception
     */
    @Test(priority = 46, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_514() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_514",
                "To verify that Channel user should not able to do SVA to Self Bank accounts transfer transaction if bank is not associated with the channel user.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        User userWithoutBankAssoc = new User(Constants.WHOLESALER);

        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(userWithoutBankAssoc, false);

        svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);

        Login.init(t1).login(userWithoutBankAssoc);

        page1.navigateToSVA2OwnBankTransferInWeb();
        page1.selectTransferType(Constants.SVA_TRANSFER_NOW);
        page1.clickNext();

        Assertion.verifyErrorMessageContain("SVA.error.bankAccountexits.required", "when bank is not associated with the channel user.", t1);
    }


    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_058
     * DESC : Perform "SVA to Bank Schedule transfer" when already exist.
     *
     * @throws Exception
     */
    @Test(priority = 47, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1})
    public void P1_TC_203() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_203",
                "To verify that system should successfully perform the identical transaction.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA, FunctionalTag.PVG_P1);

        chUser = DataFactory.getChannelUserWithAccess("SVATOBANK");

        System.out.println(chUser.FirstName);

        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);

        startNegativeTest();
        TransactionManagement.init(t1).initiateSVAtoBank(chUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);

        Assertion.assertActionMessageContain("sva.success.transfernow.message", "Identical Transaction successful", t1);

    }

    /**
     * TEST : NEGATIVE
     * TCID : SYS_TC_F_059
     * DESC : Perform "SVA to Bank " when transfer rule is suspended.
     *
     * @throws Exception
     */
    @Test(priority = 48, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA}, enabled = false)
    public void P1_TC_166() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_166",
                "To verify that a valid user can not transfer or receive money if transfer rule is suspended for SVA to Own Bank Account Transfer for Biller service.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SVA);

        optUser = DataFactory.getOperatorUserWithAccess("T_RULES");

        ServiceCharge sCharge = new ServiceCharge(Services.SVA_TO_BANK, commonUser, commonUser, null, null, null, null);

        Login.init(t1).login(optUser);
        TransferRuleManagement.init(t1).deleteTransferRule(sCharge);

        Login.init(t1).login(commonUser);
        startNegativeTestWithoutConfirm();
        TransactionManagement.init(t1).initiateSVAtoBank(commonUser, Constants.SVA_TRANSFER_NOW, provider, walletName, bankName, Constants.SVA_AMOUNT);
        Assertion.verifyErrorMessageContain("o2c.error.transferRule.unavaible", "when transfer rule is suspended", t1);

        Login.init(t1).login(optUser);
        TransferRuleManagement.init(t1).addInitiateTransferRule(sCharge);

    }


}