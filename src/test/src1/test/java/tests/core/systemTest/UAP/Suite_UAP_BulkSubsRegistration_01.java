package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkSubscriberRegistration.BulkSubscriberRegistration;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Contain the cases for Bulk Subscriber Registration & Modification
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BulkSubsRegistration_01 extends TestInit {

    List<User> userListForBulkUpload;
    private OperatorUser bulkUserAccess;
    private int noOfSubs = 2;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        bulkUserAccess = DataFactory.getOperatorUserWithAccess("SUBBUREG");
        userListForBulkUpload = getListOfUserObjectsByPassingNumberOfUser(noOfSubs);
    }


    public List<User> getListOfUserObjectsByPassingNumberOfUser(int noOfUsers) throws Exception {
        List<User> userlist = new ArrayList<>();
        for (int i = 0; i < noOfUsers; i++) {
            User userToAddInList = new User(Constants.SUBSCRIBER);
            userlist.add(userToAddInList);
        }
        return userlist;
    }


    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void bulkSubsUploadTestNew() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0034 : Bulk Add Subscriber", "To verify that the NetWork Admin can add subscriber through bulk.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.BULK_SUBSCRIBER_REGISTRATION, FunctionalTag.ECONET_UAT_5_0);

        BulkSubscriberRegistration bulkRegObj = BulkSubscriberRegistration.init(t1);

        String filename = bulkRegObj.
                generateBulkSubsRegnModnCsvFile(userListForBulkUpload, Constants.BULK_USER_STATUS_ADD, Constants.BULK_USER_STATUS_ADD);

        Login.init(t1).login(bulkUserAccess);

        BulkSubscriberRegistration.init(t1).
                initiateBulkSubscriberUpload(filename);

        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0}, dependsOnMethods = "bulkSubsUploadTestNew")
    public void bulkSubsModifyTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0035 : Bulk Modify Subscriber", "To verify that the valid user can MODIFY subscriber through bulk.");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SUBSCRIBER_REGISTRATION, FunctionalTag.ECONET_UAT_5_0);

        String filename = BulkSubscriberRegistration.init(t1).
                generateBulkSubsRegnModnCsvFile(userListForBulkUpload, Constants.BULK_USER_STATUS_MODIFY, Constants.BULK_USER_STATUS_MODIFY);

        Login.init(t1).login(bulkUserAccess);

        BulkSubscriberRegistration.init(t1).initiateBulkSubscriberUpload(filename);
    }
}
