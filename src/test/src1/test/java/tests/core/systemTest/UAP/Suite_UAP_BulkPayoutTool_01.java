package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/**
 * Contain the cases for Bulk Payout Tool
 *
 * @author navin.pramanik
 */
public class Suite_UAP_BulkPayoutTool_01 extends TestInit {

    private OperatorUser optUser;
    private User whs1, whs2;
    private User subs;


    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        optUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        whs1 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        whs2 = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
    }

    /**
     * This test case is disabled as this service has been moved to New Bulk Payout Tool
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM}, enabled = false, priority = 1)
    public void testBulkO2C() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0033 :  Bulk Payout O2C ", "To verify that the nwAdmin can perform Bulk Payout tool. (O2C)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);
        String fileName = bulkPayObj.generateBulkO2CCsvFile(optUser.MSISDN, whs1.MSISDN);

        bulkPayObj.initiateAndApproveBulkPayout(Services.BULK_O2C, fileName);
    }

    /**
     * This test case is disabled as this service has been moved to New Bulk Payout Tool
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM}, enabled = false, priority = 2)
    public void testBulkP2P() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0032 : Bulk Payout P2P", "To verify that the nwAdmin can perform Bulk Payout tool. (P2P)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        String fileName = bulkPayObj.generateBulkP2PCsvFile(whs1.MSISDN, whs2.MSISDN, "10");

        bulkPayObj.initiateAndApproveBulkPayout(Services.BULK_P2P, fileName);

    }

    /**
     * This test case is disabled as this service has been moved to New Bulk Payout Tool
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0}, enabled = false)
    public void testBulkCashin() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0030 : Bulk Payout Cashin", "To verify that the nwAdmin can perform Bulk Payout tool. (Cash IN)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        String fileName = bulkPayObj.generateBulkCashinCsvFile(whs1.MSISDN, subs.MSISDN);

        bulkPayObj.initiateAndApproveBulkPayout(Services.BULK_CASHIN, fileName);
    }

    /**
     * This test case is disabled as this service has been moved to New Bulk Payout Tool
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM}, enabled = false, priority = 5)
    public void testNewBulkCashin() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0030 : New Bulk Payout Cashin", "To verify that the nwAdmin can perform Bulk Payout tool. (Cash IN)");

        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.BULK_PAYOUT_TOOL);

        OperatorUser optBulkPay = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");

        Login.init(t1).login(optBulkPay);

        BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

        String fileName = bulkPayObj.generateBulkCashinCsvFile(whs1.MSISDN, subs.MSISDN);

        bulkPayObj.initiateNewBulkPayout("CASH IN", fileName);
    }
}
