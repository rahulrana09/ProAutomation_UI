package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.CustomerTCP;
import framework.entity.InstrumentTCP;
import framework.features.systemManagement.TCPManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/8/2017.
 */
public class Suite_UAP_TCPManagement_01 extends TestInit {

    private static InstrumentTCP instTCP;

    @BeforeClass(alwaysRun = true)
    public void setupBeforeClass() {
        try {
            instTCP = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
                    "WALLET", DataFactory.getDefaultWallet().WalletName);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void custTCPAddTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0156 :Customer TCP Add", "To verify that the valid user can add Customer TCP");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
            CustomerTCP tcp = new CustomerTCP(DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER), Constants.REGTYPE_NO_KYC);
            TCPManagement.init(t1).addApproveCustomerTCP(tcp);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0})
    public void custTCPViewTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0170 :Customer TCP View", "To verify that the valid user can view Customer TCP");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0);

            TCPManagement.init(t1).viewCustomerTCP(DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER), Constants.REGTYPE_NO_KYC_TEXT);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    //This Test is disabled

    /**
     * TEST ID: Instrument_TCP_Add
     * TEST DESC : To verify that the valid user can add Instrument TCP.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void instTCPAddTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0169\n :Instrument_TCP_Add", "To verify that the valid user can add Instrument TCP");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

            TCPManagement.init(t1).isSmokeCase().addInstrumentTCP(instTCP);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST ID: Instrument_TCP_View
     * TEST DESC : To verify that the valid user can view Instrument TCP.
     * instTCPViewTest
     * //TODO add depends on
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void instTCPViewTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0172", "To verify that the valid user can view Instrument TCP");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0);

            TCPManagement.init(t1).viewInstrumentTCP(instTCP);
        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST ID: Instrument_TCP_Modify
     * TEST DESC : To verify that the valid user can edit Instrument TCP.
     *
     * @throws Exception //TODO add depends on
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void instTCPModifyTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0173", "To verify that the valid user can edit Instrument TCP");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0);

            TCPManagement.init(t1).modifyInitiateInstrumentTCP(instTCP, Constants.MIN_THRESHOLD, Constants.MIN_THRESHOLD, Constants.MIN_THRESHOLD, Constants.MIN_THRESHOLD).
                    modifyApproveInstrumentTCP(instTCP);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * instTCPDeleteTest
     * TEST ID: Instrument_TCP_Delete
     * TEST DESC : To verify that the valid user can delete Instrument TCP.
     *
     * @throws Exception Exception //TODO add depends on
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_SIT_5_0})
    public void instTCPDeleteTest() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC0174", "To verify that the valid user can delete Instrument TCP.");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0);

            TCPManagement.init(t1).deleteInitiateInstrumentTCP(instTCP).
                    deleteApproveInstrumentTCP(instTCP);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
