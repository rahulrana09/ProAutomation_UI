/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 04-Jun-18
 *  Purpose: Bill payment Cases (For P1 Test)
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 04-Jun-18.
 */
public class SuiteSystemBillPayment01 extends TestInit {

    private OperatorUser optBillUploadUser, optBulkUtilBillReg;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        optBulkUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
    }

    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_344() throws Exception {

        ExtentTest p1Tc344 = pNode.createNode("P1_TC_344 : BULK BILLER ASSOCIATION WITH CUSTOMER",
                "To verify that system generates a log \n" +
                        "file with error messages \n" +
                        "if subscriber is barred as Both").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT);

        User barredAsBothSubs = CommonUserManagement.init(p1Tc344).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_BOTH, null);

        Biller biller = BillerManagement.init(p1Tc344).getDefaultBiller();

        Login.init(p1Tc344).login(optBulkUtilBillReg);

        try {

            String accNum = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(barredAsBothSubs.MSISDN, accNum);

            BillerManagement.init(p1Tc344)
                    .initiateBulkBillerRegistration(biller)
                    .downloadBulkRegistrationLogFile();

            String logMessage = BillerManagement.getLogEntry(accNum);

            Assertion.verifyContains(logMessage, MessageReader.getDynamicMessage("bulk.biller.association.customer.barrede", barredAsBothSubs.MSISDN),
                    "Verify Subscriber Biller Association for accnum:" + accNum, p1Tc344);
        } catch (Exception e) {
            markTestAsFailure(e, p1Tc344);
        } finally {
            biller.removeAllBills();
        }

        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_518() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_518 : Pay Bill",
                "To verify that Subscriber is able to make Bill Payment of exact amount.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT);
        try {
            Biller biller = null;
            biller = BillerManagement.init(t1).getDefaultBiller();
            User subsUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subsUser.MSISDN, billAccNumber);

            BillerManagement.init(t1).performBillPayment(subsUser.MSISDN, biller.BillerCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT})
    public void P1_TC_329() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_329 : VIEW BILL",
                "  To verify that Subscriber is not able to view Bill when  the Bill is deleted.").
                assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.BILL_PAYMENT);

        Biller biller = null;
        biller = BillerManagement.init(t1).getDefaultBiller();

        User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        String accNo1 = DataFactory.getRandomNumberAsString(5);

        Login.init(t1).login(optBillUploadUser);

        String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);
        BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

        Transactions.init(t1).viewBillBySubscriber(biller, subs, accNo1);

        String filename2 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1, "D");
        BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename2);

        startNegativeTest();
        TxnResponse response = Transactions.init(t1).viewBillBySubscriber(biller, subs, accNo1);
        response.assertStatus("00292");
    }

}
