package tests.core.systemTest.oldSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.stockManagement.IMTinitiation_page1;
import framework.pageObjects.stockManagement.IMTinitiation_page2;
import framework.pageObjects.stockManagement.StockTransferInitiate_pg1;
import framework.pageObjects.stockManagement.StockTransferToEAapproval1_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * To initiate Stock in the System and Approve it to level 1 and level 2
 *
 * @author sapan.dang
 * Refactored by Gurudatta
 */
public class Suite_System_StockManagement_01 extends TestInit {

    private static String refNo;
    private static String walletBal_101;
    private OperatorUser stockLimiter, stockInitiator, stockIMTInitiator;

    @BeforeClass(alwaysRun = true)
    public void beforeTest() throws Exception {

        try {
            // Operator Users for
            stockLimiter = DataFactory.getOperatorUserWithAccess("STOCKTR_LIMIT");
            stockInitiator = DataFactory.getOperatorUserWithAccess("STR_INIT");
            stockIMTInitiator = DataFactory.getOperatorUserWithAccess("STK_IMT_INI");
            refNo = DataFactory.getTimeStamp();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    /**
     * To Verify that operator user is able to initiate EA Stock
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01",
                "Positive Test: To Verify that operator user is able to initiate EA Stock, verify IND01 after Successful EA Stock Transfer");

        try {
            String defProvider = DataFactory.getDefaultProvider().ProviderName;
            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);


            BigDecimal pre_bal_s = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            // amount to be approved with 2 levels must be above Constants.STOCK_LIMIT_AMOUNT
            BigDecimal amount = new BigDecimal(Constants.STOCK_LIMIT_AMOUNT).add(new BigDecimal(1));

            Login.init(t1).login(stockInitiator);
            StockManagement.init(t1)
                    .initiateApproveEAStock(DataFactory.getTimeStamp(), amount.toString(), "EA Stock Transfer", defProvider);

            BigDecimal post_bal_s = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            // Validation
            Assertion.verifyAccountIsDebited(pre_bal_s, post_bal_s, amount, "Verify that Stock from IND01 system Wallet is debited", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_02                  Stock Management >>Stock Transfer to EA Initiation
     * To verify that Network Admin cannot initiate the stock transfer
     * if Transfer Amount containing special characters
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_02", "Stock Management >>Stock Transfer to EA Initiation To verify that " +
                "Network Admin cannot initiate the stock transfer if Transfer Amount containing special characters ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTest();
            StockManagement.init(t1);

            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "*1^/", "initiate Stock");

            Assertion.verifyEqual(getAlertMessage(), MessageReader.getMessage("negative.stock.transfer.invalidamount", null),
                    "Special Character is not Accepted as Transfer Amount", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_03                  Stock Management >>Stock Transfer to EA   Initiation
     * To verify that Network Admin can't initiate  stock transfer
     * when reference number is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_03", "Stock Management >>Stock Transfer to EA Initiation  To verify that " +
                "Network Admin can't initiate  stock transfer when reference number is left blank. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);

            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock("", Constants.STOCK_TRANSFER_AMOUNT, "stock initiation");

            Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Reference Number Can't be left Blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_04                  Stock Management >>Stock Transfer to EA   Initiation
     * To verify that Network Admin can't initiate  stock transfer
     * when Amount is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_04", "Stock Management >>Stock Transfer to EA Initiation " +
                "To verify that Network Admin can't initiate  stock transfer when Amount is left blank. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);

            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "", "stock initiation");

            Assertion.verifyErrorMessageContain("stockInitiation.blank.requestedQuantity", "Transfer Amount Can't be left Blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_05                  Stock Management >>Stock Transfer to EA   Initiation
     * To verify that Network Admin can't initiate  stock transfer
     * when Transfer Amount is equal to ZERO.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_05() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_05", "Stock Management >>Stock Transfer to EA Initiation " +
                "To verify that Network Admin can't initiate  stock transfer when Transfer Amount is equal to ZERO. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "0", "stock initiation");

            Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.greaterThanZero", "Transaction Amount Can't be ZERO", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_06                  Stock Management >>Stock Transfer to EA   Initiation
     * To verify that Network Admin can't initiate  stock transfer
     * when Transfer Amount is negative.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_06() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_06", "Stock Management >>Stock Transfer to EA Initiation " +
                "To verify that Network Admin can't initiate  stock transfer when Transfer Amount is Negative. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "-1", "stock initiation");

            Assertion.verifyEqual(getAlertMessage(), MessageReader.getMessage("negative.stock.transfer.invalidamount", null),
                    "Transaction Amount Can't be Negative Integer", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_06                  Stock Management >>Stock Transfer to EA   Initiation
     * To verify that Network Admin can't initiate  stock transfer
     * when Transfer Amount is negative.
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void Test_07() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_07", "Stock Management >>Stock Transfer to EA Initiation" +
                " To verify that Network Admin can't initiate  stock transfer when requested amount is more than the available stock. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            BigDecimal pre_bal_s = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            Login.init(t1).login(stockInitiator);
            startNegativeTest();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, pre_bal_s.add(new BigDecimal(1)).toString(), "stock initiation");

            Assertion.verifyErrorMessageContain("balance.cannot.be.negative", "Transaction Amount Can't be more than Available Network Stock", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 9, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_09() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_09", "Stock Management >> Stock Initiation To verify that " +
                "Network Admin cannot initiate the stock when reference number containing special characters ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock("-123^", Constants.STOCK_TRANSFER_AMOUNT, "stock initiation");

            Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "Reference number Can't have special Character", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_10                  Stock Management >> Stock Initiation
     * "To verify that Network Admin cannot
     * initiate the stock when reference number containing more than 10 digits
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_10() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_10", "\"Stock Management >> Stock Initiation\n" +
                "To verify that Network Admin cannot initiate the stock when reference number containing more than 10 digits  ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
        /*
        As system is not allowed to enter reference number more than 10 digit.
        validate reference number accept 10 digits only even after passing more than 10 digits
         */
            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(t1);
            page.navStockTransferInitiatePage();
            page.refNo_SetText("012345678912345");
            page.requestAmount_SetText(Constants.STOCK_EA_TRANSFER_AMOUNT);
            page.remark_SetText("stock initiation");

            String refNum = page.refNo.getAttribute("value");

            if (refNum.length() == 10) {
                page.clickOnSubmitTransferInitiate();
                if (ConfigInput.isConfirm)
                    page.clickOnConfirmTransferInitiate();
            }
            Assertion.verifyActionMessageContain("stock.transfer.success",
                    "verify that Network Admin cannot initiate the stock when reference number containing more than 10 digits", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_11                  Stock Management >>Stock Transfer to EA Initiation To verify that
     * Network Admin cannot initiate the stock transfer if Transfer Amount
     * containing special characters
     *
     * @throws Exception
     */
    @Test(priority = 11, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_11() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_11", "Stock Management >>Stock Transfer to EA Initiation To verify that Network Admin cannot initiate the stock transfer if Transfer Amount containing special characters. ");
/*
    validate the alert box text after entering only special characters
 */
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "123!@#", "stock initiation");

            Assertion.verifyEqual(getAlertMessage(), MessageReader.getMessage("negative.stock.transfer.invalidamount", null),
                    "Transfer Amount containing special characters", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_12                  Stock Management >>Stock Transfer to EA Initiation To verify that
     * Network Admin cannot initiate the stock transfer if Transfer Amount
     * containing special characters
     *
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_12() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_12", "Stock Management >> Stock Initiation\n" +
                "To verify that Network Admin cannot initiate the stock when requested amount containing more than 10 digits before decimal");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);

            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "123456327890.243", "stock initiation");
            Assertion.verifyErrorMessageContain("escrowInitiation.decimal.validation", "amount containing more than 10 digits before decimal", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_13                  Stock Management >>Stock Transfer to EA Initiation To verify that
     * Network Admin cannot initiate the stock transfer if Transfer Amount
     * containing special characters
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_13() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_13", "Stock Management >> Stock Initiation" +
                "To verify that Network Admin cannot  initiate the stock if entered amount " +
                "containing more than 4 digits after decimal");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {
            Login.init(t1).login(stockInitiator);
            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(t1);

            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "1234.1234567", "stock initiation");

            Assertion.verifyEqual(getAlertMessage(), MessageReader.getMessage("negative.stock.transfer.invalidamount", null),
                    "Amount Accepts upto 4 decimals only", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_14                  Stock Management >> Stock Initiation To verify that  Network Admin
     * cannot initiate the stock if all fields are left blank.
     *
     * @throws Exception
     */
    @Test(priority = 14, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_14() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_14", "Stock Management >> Stock Initiation To verify that " +
                " Network Admin cannot initiate the stock if all fields are left blank");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(t1);
            page.navStockTransferInitiatePage();
            page.clickOnSubmitTransferInitiate();
            Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "Stock cannot be initiated successfully if all fields " +
                    "are left blank", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_15                  "Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock
     * if all the required fields containing maximum length allowed  "
     *
     * @throws Exception note enable false: as UI can only take max character,
     */
    @Test(priority = 15, enabled = false)
    public void Test_15() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_15", "\"Stock Management >> Stock Initiation To verify that " +
                "Network Admin can initiate the stock if all the required fields containing maximum length allowed ");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTest();
            StockManagement.init(t1)
                    .initiateApproveEAStock("123456790", "100", "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQR");
            Assertion.verifyActionMessageContain("stock.transfer.success", "Network Admin can initiate the stock if all the required fields containing maximum length allowed", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_16                  "Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock
     * if all the required fields containing minimum length allowed  "
     *
     * @throws Exception
     */
    @Test(priority = 16, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_16() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_16", "\"Stock Management >> Stock Initiation To verify that " +
                "Network Admin can initiate the stock if all the required fields containing minimum length allowed ");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t1).login(stockInitiator);
        StockManagement.init(t1)
                .initiateEAStock("2", "1", "s");

    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_17                  "Stock Management >>Stock Transfer to EA Initiation To verify that Network Admin
     * can initiate  stock transfer when Remarks field  is left blank  "
     *
     * @throws Exception
     */
    @Test(priority = 17, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_17() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_17", "Stock Management >>Stock Transfer to EA Initiation" +
                " To verify that Network Admin can initiate  stock transfer when Remarks field  is left blank.");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t1).login(stockInitiator);

        StockManagement.init(t1)
                .initiateEAStock(refNo, "1", "");

    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_18                  "Stock Management >>Stock Transfer to EA Initiation To verify that Network Admin
     * can initiate  stock transfer when Remarks field  is filled with special characters"
     *
     * @throws Exception
     */
    @Test(priority = 18, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_18() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_18", "Stock Management >>Stock Transfer to EA Initiation To verify " +
                "that Network Admin cannot initiate  " +
                "stock transfer when Remarks field  is filled with special characters.");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, Constants.EA_STOCK_LIMIT_AMOUNT, "$%#@$#%");

            Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Stock cannot be initiated successfully if Remarks field is filled with special characters", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_19                  Stock Management >> Stock Initiation To verify that Network Admin cannot initiate the stock
     * if requested quantity is non numeric value
     *
     * @throws Exception
     */
    @Test(priority = 19, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_19() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_19", "Stock Management >> Stock Initiation To verify that Network" +
                " Admin cannot initiate the stock if requested quantity is non numeric value");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "amount", "EA Stock Initiation");

            Assertion.verifyEqual(getAlertMessage(), MessageReader.getMessage("negative.stock.transfer.invalidamount", null),
                    "Only Numeric Value Allowed", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_20                  "Stock Management >> Stock Initiation
     * To verify that System Should'nt allow more than 200 characters in remarks field "
     *
     * @throws Exception
     */
    @Test(priority = 20, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_20() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_20", "\"Stock Management >> Stock Initiation\n" +
                "To verify that System Should'nt allow more than 200 characters in remarks field ");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t1).login(stockInitiator);
            startNegativeTestWithoutConfirm();
            StockManagement.init(t1)
                    .initiateEAStock(refNo, "100", "123456789012345678901234567890123456789012345678901234567" +
                            "8901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234" +
                            "5678901234567890123456789012345678901234567890123456789012345678901234567890");

            Assertion.verifyErrorMessageContain("youcannot.entermore.thancharacters.forremarks", "Stock cannot be initiated successfully if remark " +
                    "field contain more than 200 characters", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_21                  "Stock Management >>Stock Transfer to  Initiation To verify that Network Admin
     * can initiate  stock transfer when Remarks field  is filled with special characters"
     *
     * @throws Exception
     */
    @Test(priority = 21, enabled = false) //duplicate to Test_18
    public void Test_21() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_21", "Stock Management >>Stock Transfer to EA Initiation To verify that Network Admin cannot initiate  " +
                "stock transfer when Remarks field  is filled with special characters.");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t1).login(stockInitiator);

        StockTransferInitiate_pg1 page = StockTransferInitiate_pg1.init(t1);

        page.navStockTransferInitiatePage();
        page.refNo_SetText(refNo);
        page.requestAmount_SetText("100");
        page.remark_SetText("$%#@$#%");
        page.clickOnSubmitTransferInitiate();

        Assertion.verifyErrorMessageContain("stockInitiation.special.remarks", "Stock cannot be initiated successfully if Remarks field is filled with special characters", t1);

    }


    /**
     * To Verify that operator user is able to initiate EA Stock
     *
     * @throws Exception
     */
    @Test(priority = 22, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_22() throws Exception {

        ExtentTest t22 = pNode.createNode("Test_22", "Stock Management >> Stock Approval-1 To verify that Network" +
                "  Admin can successfully approve the stock at level-1 and request will close at this level when " +
                "requested quantity is less than Approval limit 1.")
                .assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {

            OperatorUser approver = DataFactory.getOperatorUserWithAccess("STOCKTR_APP1", Constants.NETWORK_ADMIN);

            BigDecimal pre_bal_s = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
            t22.info("Pre - Balance " + pre_bal_s);

            // amount to be approved with 1 levels must be less than Constants.STOCK_LIMIT_AMOUNT
            BigDecimal amount = new BigDecimal(Constants.STOCK_LIMIT_AMOUNT).subtract(new BigDecimal(1));

            Login.init(t22).login(stockInitiator);
            String txnId = StockManagement.init(t22)
                    .initiateEAStock(DataFactory.getTimeStamp(), amount.toString(), "EA stock Initiation", DataFactory.getDefaultProvider().ProviderName);

            /*
            Approve Level 1
             */
            Login.init(t22).login(approver);
            new StockTransferToEAapproval1_page1(t22)
                    .navigateToLink()
                    .selectTransactionID(txnId)
                    .submitBtn_Click()
                    .submitBtn_Click_pg2();

            if (Assertion.verifyActionMessageContain("stock.approve.success", "Admin can successfully approve the stock at level-1 " +
                    "and request will close at this level when requested quantity is less than Approval limit 1", t22, txnId)) {
                BigDecimal post_bal_s = MobiquityGUIQueries
                        .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

                Assertion.verifyAccountIsDebited(pre_bal_s, post_bal_s, amount, "Verify that Stock from IND01 system Wallet is debited" +
                        " After tranacting for amount less tah EA Stock and Approving ate First level", t22);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t22);
        }

        Assertion.finalizeSoftAsserts();

    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_21                  "Stock Management >>Stock Transfer to  Initiation To verify that Network Admin
     * can initiate  stock transfer when Remarks field  is filled with special characters"
     *
     * @throws Exception
     */
    @Test(priority = 24, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_24() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_24",
                "Stock Management >>Stock Transfer to  Initiation To verify that Network Admin" +
                        "can not initiate  stock transfer when Remarks field  is filled with special characters");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t1).login(stockInitiator);
        startNegativeTest();
        StockManagement.init(t1)
                .addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, "!@#$");

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "cannot initiate  stock transfer when Remarks field  is filled with special characters", t1);

    }

    /**
     * Stock Transfer Limit for  EA" +
     * "To verify that Network Admin can not set the Stock Limit when approval  Limit-1 is left blank
     *
     * @throws Exception
     */
    @Test(priority = 25, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_25() throws Exception {

        ExtentTest t25 = pNode.createNode("Test_25", "Stock Management >>Stock Transfer Limit for  EA" +
                "To verify that Network Admin can not set the Stock Limit when approval  Limit-1 is left blank.");

        t25.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t25).login(stockLimiter);

        startNegativeTest();
        StockManagement.init(t25)
                .addEAStockLimit(DataFactory.getDefaultProvider().ProviderName, "");

        Assertion.verifyErrorMessageContain("approvalLimit1andapprovalLimit.error.numericpositiveinteger", "Network Admin can not set the Stock Limit when approval  Limit-1 is left blank", t25);
    }

    /**
     * TEST:  Test_26
     * DESCRIPTION: Stock Management >>Stock Initiation
     * "To verify that Network Admin cannot
     * initiate the stock if requested quantity is non numeric value"
     *
     * @throws Exception
     */
    @Test(priority = 26, enabled = false) //duplicate to Test_19
    public void Test_26() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_26", "Stock Management >> Stock Initiation " +
                "To verify that Network Admin cannot initiate the stock if requested quantity is non numeric value\n");

        t1.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t1).login(stockInitiator);
        startNegativeTest();
        StockManagement.init(t1)
                .initiateEAStock("2345", "amount", "Remarks", DataFactory.getDefaultProvider().ProviderName);

        String message = driver.switchTo().alert().getText();
        driver.switchTo().alert().accept();
        Assertion.verifyContains(message, "Amount Accepts upto 4 decimals only", "Network Admin cannot initiate the stock if requested quantity is non numeric value", t1);

    }

    /**
     * To Verify that operator user is able to initiate EA Stock
     * * @throws Exception
     */
    @Test(priority = 27, enabled = false) //duplicate to Test_07
    public void Test_27() throws Exception {

        ExtentTest t27 = pNode.createNode("Test_27", "Stock Management >> EA Stock Initiation\n" +
                "To verify that Network Admin can not initiate the stock when requested" +
                " amount is more than the available stock");

        try {
            t27.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

            BigDecimal pre_bal_s = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
            t27.info("Current Balance: " + pre_bal_s);

            Login.init(t27)
                    .login(stockInitiator);
            startNegativeTest();
            StockManagement.init(t27)
                    .initiateEAStock("2345", pre_bal_s.add(new BigDecimal(1)).toString(), "Remarks", DataFactory.getDefaultProvider().ProviderName);

            Assertion.verifyErrorMessageContain("balance.cannot.be.negative", "amount is higher than stock amount", t27);
        } catch (Exception e) {
            markTestAsFailure(e, t27);
        }

        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_28                  ""Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock of exact/partial amount of 1010.1"
     *
     * @throws Exception
     */
    @Test(priority = 28, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_28() throws Exception {
        ExtentTest t28 = pNode.createNode("Test_28", "Stock Management >> Stock Initiation" +
                "To verify that Network Admin can initiate the stock of exact/partial amount of 1010.1");

        t28.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String providerID = DataFactory.getDefaultProvider().ProviderId;
            Login.init(t28).login(stockInitiator);
            StockManagement.init(t28)
                    .initiateNetworkStock(providerName, DataFactory.getDefaultBankName(providerID), "1010.1", refNo);
        } catch (Exception e) {
            markTestAsFailure(e, t28);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_29                  ""Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock of exact/partial amount of 10.01"
     *
     * @throws Exception
     */
    @Test(priority = 29, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_29() throws Exception {
        ExtentTest t29 = pNode.createNode("Test_29", "Stock Management >> Stock Initiation" +
                "To verify that Network Admin can initiate the stock of exact/partial amount of 10.01");

        t29.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        try {
            Login.init(t29).login(stockInitiator);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String providerID = DataFactory.getDefaultProvider().ProviderId;

            StockManagement.init(t29)
                    .initiateNetworkStock(providerName, DataFactory.getDefaultBankName(providerID), "10.01", refNo);
        } catch (Exception e) {
            markTestAsFailure(e, t29);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_30                  ""Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock of exact/partial amount of 1010.10 "
     *
     * @throws Exception
     */
    @Test(priority = 30, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_30() throws Exception {
        ExtentTest t30 = pNode.createNode("Test_30", "\"Stock Management >> Stock Initiation\n" +
                "To verify that Network Admin can initiate the stock of exact/partial amount of 1010.10");

        t30.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {
            Login.init(t30).login(stockInitiator);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String providerID = DataFactory.getDefaultProvider().ProviderId;

            StockManagement.init(t30)
                    .initiateNetworkStock(providerName, DataFactory.getDefaultBankName(providerID), "1010.10", refNo);
        } catch (Exception e) {
            markTestAsFailure(e, t30);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_31                  ""Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock of exact/partial amount of 200.5667"
     *
     * @throws Exception
     */
    @Test(priority = 31, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_31() throws Exception {
        ExtentTest t31 = pNode.createNode("Test_31", "\"Stock Management >> Stock Initiation\n" +
                "To verify that Network Admin can initiate the stock of exact/partial amount of 200.5667");

        t31.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        try {
            Login.init(t31).login(stockInitiator);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String providerID = DataFactory.getDefaultProvider().ProviderId;

            StockManagement.init(t31)
                    .initiateNetworkStock(providerName, DataFactory.getDefaultBankName(providerID), "200.5667", refNo);
        } catch (Exception e) {
            markTestAsFailure(e, t31);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_32                  ""Stock Management >> Stock Initiation
     * To verify that Network Admin can initiate the stock of exact/partial amount of 200.56670"
     *
     * @throws Exception
     */
    @Test(priority = 32, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_32() throws Exception {
        ExtentTest t32 = pNode.createNode("Test_32", "\"Stock Management >> Stock Initiation\n" +
                "To verify that Network Admin can initiate the stock of exact/partial amount of 200.56670");

        t32.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);
        Login.init(t32).login(stockInitiator);
        try {
            Login.init(t32).login(stockInitiator);
            String providerName = DataFactory.getDefaultProvider().ProviderName;
            String providerID = DataFactory.getDefaultProvider().ProviderId;

            StockManagement.init(t32)
                    .initiateNetworkStock(providerName, DataFactory.getDefaultBankName(providerID), "200.56670", refNo);
        } catch (Exception e) {
            markTestAsFailure(e, t32);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_33                  "Stock Management >>Stock Transfer EA
     * Enquiry  To verify that Network admin can
     * enquire the details of Stock Transfer to EA   based on Transaction ID"
     *
     * @throws Exception
     */
    @Test(priority = 33, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_33() throws Exception {
        ExtentTest t33 = pNode.createNode("Test_33", "Stock Management >>Stock Transfer EA   Enquiry  To verify " +
                "that Network admin can enquire the details of Stock Transfer to EA   based on Transaction ID")
                .assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t33).login(stockInitiator);

        String txnID = StockManagement.init(t33)
                .initiateEAStock(refNo, "1", "stock initiation");

        // perform EA enquiry
        StockManagement.init(t33)
                .initiateStockEATransferEnquiry(txnID);
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_34                  "Stock Management >>Stock Transfer EA
     * Enquiry  To verify that Network admin can
     * enquire the details of Stock Transfer to EA   based on Transaction ID"
     *
     * @throws Exception
     */
    @Test(priority = 34, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_34() throws Exception {
        ExtentTest t34 = pNode.createNode("Test_34", "Stock Management >>Stock Transfer EA Enquiry " +
                " To verify that Network admin cannot enquire the details of Stock Transfer to EA  " +
                " if wrong Transaction ID is entered\n");

        t34.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t34).login(stockInitiator);

        Login.init(t34).login(stockInitiator);
        // perform EA enquiry
        startNegativeTestWithoutConfirm();
        StockManagement.init(t34)
                .initiateStockEATransferEnquiry("TXNID1234");
        Assertion.verifyErrorMessageContain("btsl.error.msg.notxnId", "Invalid Transaction ID", t34);

    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_35                  "Stock Management >>Stock Transfer
     * Enquiry  To verify that Network admin can
     * enquire the details of Stock Transfer to    based on Transaction ID"
     *
     * @throws Exception
     */
    @Test(priority = 35, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_35() throws Exception {
        ExtentTest t35 = pNode.createNode("Test_35", "Stock Management >>Stock Transfer Enquiry " +
                " To verify that Network admin cannot enquire the details of Stock Transfer to   " +
                " if blank Transaction ID is entered\n");

        t35.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t35).login(stockInitiator);

        startNegativeTestWithoutConfirm();
        StockManagement.init(t35).stockEnquiry("");

        Assertion.verifyErrorMessageContain("stock.enquiry.error.noStatus", "Transaction ID cannot be blank", t35);

    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_36                  "Stock Management >>Stock Transfer To verify that" +
     * Network admin is cannot initiate IMT STOCK if
     * amount is not numeric"
     *
     * @throws Exception
     */
    @Test(priority = 36, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_36() throws Exception {
        ExtentTest t36 = pNode.createNode("Test_36", "Stock Management >>Stock Transfer To verify that" +
                " Network admin is cannot initiate IMT STOCK if amount is not numeric");
        t36.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);


        Login.init(t36).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        StockManagement.init(t36)
                .initiateIMT(refNo, "abcd", "IMT Initiate");

        Assertion.verifyErrorMessageContain("stock.error.requestedQuantity.numericPositiveNumber", "Amount Can't be non numeric ", t36);
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_37                  "Stock Management >>Stock Transfer To verify that Network admin is able to initiate IMT STOCK"
     *
     * @throws Exception
     */
    @Test(enabled = false) //invalid tc
    public void Test_38() throws Exception {
        ExtentTest t38 = pNode.createNode("Test_38", "To verify that Network admin is not able to initiate IMT STOCK " +
                "if transaction id is wrong");
        t38.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t38).login(stockIMTInitiator);
        //Login.init(t1).loginAsSuperAdmin();
        StockManagement stockManagement = StockManagement.init(t38);

        /*
        Invalid TC
        initiate IMT will generate Transaction ID
        */
        stockManagement.initiateIMT("23123", "5000", "initiated");

        Assertion.verifyErrorMessageContain("btsl.error.srock.enq.muliple.search.options", "Wrong txn id is passed ", t38);
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_39                  "Stock Management >>Stock Transfer To verify that Network admin is able to initiate IMT STOCK"
     *
     * @throws Exception
     */
    @Test(priority = 39, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_39() throws Exception {
        ExtentTest t39 = pNode.createNode("Test_39", "To verify that Network admin is not able to" +
                " initiate IMT STOCK if amount is blank");

        t39.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t39).login(stockInitiator);
        startNegativeTestWithoutConfirm();
        StockManagement.init(t39)
                .initiateIMT(refNo, "", "IMT Initiate");

        Assertion.verifyErrorMessageContain("stockInitiation.blank.requestedQuantity", "Amount field cannot be blank ", t39);
    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_40                 "Stock Management >>Stock Transfer To verify that Network admin is able to initiate IMT STOCK"
     *
     * @throws Exception
     */
    @Test(priority = 40, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_40() throws Exception {
        ExtentTest t40 = pNode.createNode("Test_40", "To verify that Network admin is not able" +
                " to initiate IMT STOCK if remark field contain more than 200 characters");
        t40.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t40).login(stockIMTInitiator);

        /*
        As system is not allowing to enter characters more than 200
        validate remark textbox accept less than or equals to 200 even after passing more than 200 characters
         */
        IMTinitiation_page1 page1 = IMTinitiation_page1.init(t40);

        page1.navigateToLink();
        page1.remittancePartner_SelectIndex(1);
        page1.refNo_SetText(refNo);
        page1.requestedAmount_SetText("1");
        page1.remark_SetText("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        int remarkLength = page1.remark.getAttribute("value").length();
        if (remarkLength == 200) {
            page1.submitButton_Click();

            new IMTinitiation_page2(t40)
                    .confirmBtn_Click();
        }

    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_41                 "Stock Management >>Stock Transfer To verify that Network admin is able to initiate IMT STOCK"
     *
     * @throws Exception
     */
    @Test(priority = 41, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_41() throws Exception {
        ExtentTest t41 = pNode.createNode("Test_41", "To verify that Network admin is " +
                " able to initiate IMT STOCK if remark field blank")
                .assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t41).login(stockIMTInitiator);
        StockManagement.init(t41)
                .initiateIMT(refNo, "1", "");

    }


    /**
     * TEST:                    DESCRIPTION:
     * Test_43                 "Stock Management >>Stock Transfer To verify
     * that Network admin is  not able to initiate IMT
     * stock if reference number is non numeric"
     *
     * @throws Exception
     */
    @Test(priority = 42, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_42() throws Exception {
        ExtentTest t43 = pNode.createNode("Test_42", "Stock Management >>Stock Transfer" +
                " To verify that Network admin is  not able to initiate IMT stock if reference number is non numeric");

        t43.assignCategory(FunctionalTag.OLD_SYSTEM_TEST, FunctionalTag.NEGATIVE_TEST, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t43).login(stockIMTInitiator);
        startNegativeTestWithoutConfirm();
        StockManagement.init(t43)
                .initiateIMT("!@#$", "1", "Initiate IMT");

        Assertion.verifyErrorMessageContain("stockInitiation.special.refNo", "RefNum cannot be non numeric ", t43);
    }

    /**
     * TEST:                    DESCRIPTION:
     * Test_44                 "Stock Management >>Stock Transfer To verify
     * that Network admin is  not able to initiate IMT
     * stock if reference number is non numeric"
     *
     * @throws Exception
     */
    @Test(priority = 43, groups = {FunctionalTag.OLD_SYSTEM_TEST})
    public void Test_43() throws Exception {
        ExtentTest t44 = pNode.createNode("Test_43", "Stock Management >>Stock Transfer" +
                " To verify that Network admin is  not able to initiate IMT stock if reference number is blank");
        t44.assignCategory(FunctionalTag.NEGATIVE_TEST, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT);

        Login.init(t44).login(stockIMTInitiator);
        startNegativeTestWithoutConfirm();
        StockManagement.init(t44)
                .initiateIMT("", "1", "Initiate IMT");

        Assertion.verifyErrorMessageContain("stockInitiation.blank.refNo", "RefNum cannot be blank ", t44);
    }

    /**
     * Get The alert message
     *
     * @return
     */
    public String getAlertMessage() {
        WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
        return wait.until(ExpectedConditions.alertIsPresent()).getText();
    }
}
