/*
 * ******************************************************************************
 *  COPYRIGHT(c) 2016: Comviva Technologies Pvt. Ltd.
 *  <p>
 *  This software is the sole property of Comviva and is protected by copyright
 *  law and international treaty provisions. Unauthorized reproduction or
 *  redistribution of this program, or any portion of it may result in severe
 *  civil and criminal penalties and will be prosecuted to the maximum extent
 *  possible under the law. Comviva reserves all rights not expressly granted.
 *  You may not reverse engineer, decompile, or disassemble the software, except
 *  and only to the extent that such activity is expressly permitted by
 *  applicable law notwithstanding this limitation.
 *  <p>
 *  THIS SOFTWARE IS PROVIDED TO YOU "AS IS" WITHOUT WARRANTY OF ANY KIND,
 *  EITHER EXPRESS OR IMPLIED,INCLUDING BUT NOT LIMITED TO THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
 *  YOU ASSUME THE ENTIRE RISK AS TO THE ACCURACY AND THE USE OF THIS SOFTWARE.
 *  Comviva SHALL NOT BE LIABLE FOR ANY DAMAGES WHATSOEVER ARISING OUT OF THE
 *  USE OF OR INABILITY TO USE THIS SOFTWARE, EVEN IF Comviva HAS BEEN ADVISED
 *  OF THE POSSIBILITY OF SUCH DAMAGE.
 *  ******************************************************************************
 *	Author Name: navin.pramanik
 *  Date: 28-Dec-2017
 *  Purpose: Grade Management Related Cases (For System Test)
 */
package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.gradeManagement.GradeAddPage;
import framework.pageObjects.gradeManagement.GradeDeletePage;
import framework.pageObjects.gradeManagement.GradeModifyPage;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Button;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.NumberConstants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


public class SuiteSystemGradeManagement01 extends TestInit {

    private static SuperAdmin saAddGrade, saModifyGrade, saDeleteGrade;
    private static String gradeName, gradeCode, domainCode, catCode, expectedMsg, dbGradeCodeSubs, dbGradeNameSubs;
    private Grade grade;
    private ExtentTest setupNode, teardownNode;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        grade = new Grade(Constants.SUBSCRIBER);
        saAddGrade = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
        saModifyGrade = DataFactory.getSuperAdminWithAccess("MODIFY_GRADES");
        saDeleteGrade = DataFactory.getSuperAdminWithAccess("DELETE_GRADES");

        //Get all values and store in variable to reinitialize the object
        gradeName = grade.getGradeName();
        gradeCode = grade.getGradeCode();
        domainCode = grade.getDomainCode();
        catCode = grade.getCategoryCode();

        dbGradeCodeSubs = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0).GradeCode;
        dbGradeNameSubs = DataFactory.getGradeName(gradeCode);

        setupNode = pNode.createNode("Setup", "Setup for Grade Management");
        teardownNode = pNode.createNode("TearDown", " TearDown for Grade Management");

    }


    @BeforeMethod(alwaysRun = true)
    public void resetGrade() {
        grade.setGradeName(gradeName);
        grade.setGradeCode(gradeCode);
        grade.setDomainCode(domainCode);
        grade.setCategoryCode(catCode);
    }


    /**
     * NAME: SYS_TC_GRADE_MGMT_N_0001
     * TYPE : NEGATIVE
     * DESC: To verify that "Super Admin" should not be able to add Grade in the system when Grade Name field is left blank.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest1() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0001", "To verify that \"Super Admin\" should " +
                "not be able to add Grade in the system when \"Grade Name\" field is left blank.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        grade.GradeName = Constants.BLANK_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1).
                addGrade(grade);

        expectedMsg = MessageReader.getDynamicMessage("channel.grade.gradename.required", NumberConstants.CONST_1);

        Assertion.verifyDynamicErrorMessageContain(expectedMsg, "Grade Name", t1);
    }

    /**
     * NAME : SYS_TC_GRADE_MGMT_N_0002
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add Grade in the system
     * when special characters are entered in "Grade Name" field .
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest2() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0002", "To verify that \"Super Admin\" should not be" +
                " able to <b>add Grade</b> in the system when special characters are entered in \"Grade Name\" field .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        grade.GradeName = Constants.SPECIAL_CHARACTER_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGrade(grade);

        Assertion.verifyErrorMessageContain("channel.grade.gradeNames", "Grade Code", t1);
    }


    /**
     * NAME : SYS_TC_GRADE_MGMT_N_0003
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to Add "Grade" in the system when "Grade Code" field is left blank.
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest3() throws Exception {

        ExtentTest t3 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0003", "To verify that \"Super Admin\" should not be able to add Grade in the system when" +
                " Grade Code field is left blank.");

        t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t3).loginAsSuperAdmin(saAddGrade);

        grade.GradeCode = Constants.BLANK_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t3)
                .addGrade(grade);

        expectedMsg = MessageReader.getDynamicMessage("channel.grade.gradecode.required", NumberConstants.CONST_1);

        Assertion.verifyDynamicErrorMessageContain(expectedMsg, "Grade Code", t3);
    }

    /**
     * NAME :SYS_TC_GRADE_MGMT_N_0004
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add "Grade" in the system when
     * special characters are entered in Grade Code field.
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest4() throws Exception {

        ExtentTest t4 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0004", "To verify that \"Super Admin\" should not be able to add " +
                "\"Grade\" in the system when special characters are entered in Grade Code field.");

        t4.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t4).loginAsSuperAdmin(saAddGrade);

        grade.GradeCode = Constants.SPECIAL_CHARACTER_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t4)
                .addGrade(grade);

        Assertion.verifyErrorMessageContain("channel.grade.gradeCodeOrgradeName", "Grade Code", t4);
    }

    /**
     * NAME :SYS_TC_GRADE_MGMT_N_0005
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add Grade in the system when Domain Code is not selected.
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest5() throws Exception {

        ExtentTest t5 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0005", "To verify that \"Super Admin\" should not be able to add " +
                "Grade in the system when Domain Name is not selected.");

        t5.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t5).loginAsSuperAdmin(saAddGrade);

        grade.DomainCode = Constants.BLANK_CONSTANT;
        grade.CategoryCode = Constants.BLANK_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t5)
                .addGrade(grade);

        Assertion.verifyErrorMessageContain("channel.grade.domain.required", "Grade Code", t5);
    }

    /**
     * NAME : SYS_TC_GRADE_MGMT_N_0006
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add Grade in the system when Category Code is not selected.
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest6() throws Exception {

        ExtentTest t6 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0006", "To verify that \"Super Admin\" should not be able to " +
                "add Grade in the system when Category Name is not selected.");

        t6.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t6).loginAsSuperAdmin(saAddGrade);

        grade.CategoryCode = Constants.BLANK_CONSTANT;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t6)
                .addGrade(grade);

        Assertion.verifyErrorMessageContain("channel.grade.category.required", "Grade Code", t6);
    }


    /**
     * NAME :SYS_TC_GRADE_MGMT_N_0007
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add Grade in the system when Grade Name is already exist in the system.
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest7() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0007", "To verify that <b>\"Super Admin\"</b> should not be able to " +
                "add Grade in the system when Grade Name is already exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        String dbGradeName = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0).GradeName;

        grade.GradeName = dbGradeName;

        String guiGradeRowNum = GradeManagement.init(t1).getGradeRowNumberFromGui(dbGradeName);

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGrade(grade);

        expectedMsg = MessageReader.getDynamicMessage("channel.grade.gradeNamePresent", guiGradeRowNum);

        Assertion.verifyDynamicErrorMessageContain(expectedMsg, "Grade Name Alreay Exists", t1);

    }

    /**
     * NAME :SYS_TC_GRADE_MGMT_N_0008
     * TYPE : Negative
     * DESC : To verify that "Super Admin" should not be able to add Grade in the system when Grade Code is already exist in the system.
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest8() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0008", "To verify that \"Super Admin\" should not be able to " +
                "add Grade in the system when Grade Code is already exist in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        String dbGradeCode = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0).GradeCode;

        grade.GradeCode = dbGradeCode;

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGrade(grade);

        Assertion.verifyErrorMessageContain("channel.grade.gradeCodePresent", "Grade Code Already Exists", t1);
    }

    //TODO Correct Test Case

    /**
     * NAME :SYS_TC_GRADE_MGMT_N_0009
     * TYPE : Negative
     * DESC :
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT})
    public void gradeNegativeTest9() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_GRADE_MGMT_N_0009", "To verify that when \"Super Admin\"  " +
                "clicks on \"Add More Grades\" button then one extra row should come on \"Add Grade Page\". ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGradeModuleButtonTest(grade, Button.ADD_MORE_GRADE);

    }


    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1})
    public void gradeNegativeTest10() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_014 :Grade Management - Add Grade Confirm Back", "To verify that when 'Super admin' click on Back button on Add grade Confirmation Page then the application should navigate to previous Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGradeModuleButtonTest(grade, Button.CONFIRM_BACK);

    }

    /**
     * TEST : P1_TC_030 :Grade Management - Add Grade Save
     * DESC : To verify the working of 'Save Button' on 'Add Grade' Page.
     * @throws Exception
     */
    /**
     * @throws Exception
     */
    /*@Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM,FunctionalTag.GRADE_MANAGEMENT,FunctionalTag.PVG_P1})
    public void gradeNegativeTest12() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_030 :Grade Management - Add Grade Save", "To verify the working of 'Save Button' on 'Add Grade' Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT,FunctionalTag.PVG_P1);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGradeModuleButtonTest(grade, Button.SAVE);
    }*/
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1})
    public void gradeNegativeTest13() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_407 :Grade Management - Add Button Functionality", "To verify that application should allow the user to click on 'Add Button' on Add Grade first page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).loginAsSuperAdmin(saAddGrade);

        startNegativeTestWithoutConfirm();

        GradeManagement.init(t1)
                .addGradeModuleButtonTest(grade, Button.ADD);
    }


    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1, "Test"})
    public void gradeModNegativeTest() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_097 :Grade Management - Modify Grade",
                "To verify that Super Admin should  can only modify the Grade Name and following information should be displayed on Modify Grade Confirmation Page\n" +
                        "• Domain name\n" +
                        "• Category name\n" +
                        "• Grade code\n" +
                        "• Grade name\n" +
                        "Button to click \n" +
                        "• Confirm \n" +
                        "• Back");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);
        Grade gradeNew = new Grade(Constants.MERCHANT);


        try {
            Login.init(t1).loginAsSuperAdmin(saAddGrade);
            GradeManagement.init(setupNode).addGrade(gradeNew);
            Login.init(t1).loginAsSuperAdmin(saModifyGrade);
            GradeModifyPage page = new GradeModifyPage(t1);
            page.navigateToLink();
            page.selectGrade(gradeNew.GradeCode);

            boolean isGradeCodeModifiable = page.checkGradeCodeEditable();
            page.modifyGradeName(gradeNew.GradeCode, "Modify");
            page.modifyButtonClick();

            Assertion.verifyEqual(isGradeCodeModifiable, true, "Check Only Grade Name is Modifiable", t1);

            Object[] expectedHeaders = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr[1]/td");
            Object[] actualHeaders = UserFieldProperties.getLabels("label.grade.");

            Assertion.verifyArrayEquals(expectedHeaders, actualHeaders, "Verify the Header Labels", t1);

            Assertion.verifyEqual(page.isConfirmButtonDisplayed(), true, "Visibility of Confirm Button", t1);
            Assertion.verifyEqual(page.isBackButtonDisplayed(), true, "Visibility of Back Button", t1);

            page.confirmButtonClick();

        } finally {
            Login.init(teardownNode).loginAsSuperAdmin(saDeleteGrade);
            GradeManagement.init(teardownNode).deleteGrade(gradeNew, true);
        }


        /*GradeModifyPage page = new GradeModifyPage(t1);
        page.navigateToLink();

        String dbGradeCode = DataFactory.getGradesForCategory(Constants.SUBSCRIBER).get(0).GradeCode;
        grade.GradeCode = dbGradeCode;
        grade.GradeName = DataFactory.getGradeName(dbGradeCode);

        page.selectGrade(grade.GradeCode);

        boolean isGradeCodeModifiable = page.checkGradeCodeEditable();
        page.modifyGradeName(grade.GradeCode, "Modify");
        page.modifyButtonClick();*/


    }

    /**
     * P1 Cases
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_096() throws Exception {

        ExtentTest tc064 = pNode.createNode("P1_TC_096: Grade Management-Modify Grade Page Information",
                "To verify that Application should display the following information on Modify Grade First page:\n" +
                        "• Domain name \n" +
                        "• Category Name\n" +
                        "• Grade code\n" +
                        "• Grade name\n" +
                        "Button to click:\n" +
                        "• Modify \n" +
                        "Check box to click").assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(tc064).loginAsSuperAdmin(saModifyGrade);

        GradeModifyPage page = new GradeModifyPage(tc064);
        page.navigateToLink();

        Object[] expectedHeaders = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr[1]/td");
        Object[] actualHeaders = UserFieldProperties.getLabels("label.grade.");

        Assertion.verifyArrayEquals(expectedHeaders, actualHeaders, "Verify Labels", tc064);

        Assertion.verifyEqual(page.isModifyButtonDisplayed(), true, "Visibility of Modify Button", tc064);
        Assertion.verifyEqual(page.isCheckBoxDisplayed(), true, "Visibility of Check Box Button", tc064);

    }


    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0})
    public void P1_TC_098() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_098 :Grade Management - Delete Grade page Information",
                "To verify that Application displays the following information :\n" +
                        "• Domain name\n" +
                        "• Category name\n" +
                        "• Grade code\n" +
                        "• Grade name\n" +
                        "  Button to click \n" +
                        "• Confirm \n" +
                        "• Back\n" +
                        "on Delete Grade Confirm Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0);
        Login.init(t1).loginAsSuperAdmin(saDeleteGrade);


        GradeDeletePage gradeDeletePage = new GradeDeletePage(t1);

        gradeDeletePage.navigateToLink();
        gradeDeletePage.selectGrade(dbGradeCodeSubs);
        gradeDeletePage.clickDeleteButton();

        Object[] expectedHeaders = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr[1]/td");
        Object[] actualHeaders = UserFieldProperties.getLabels("label.grade.");

        Assertion.verifyArrayEquals(expectedHeaders, actualHeaders, "Verify Labels", t1);

        Assertion.verifyEqual(gradeDeletePage.isConfirmButtonDisplayed(), true, "Visibility of Confirm Button", t1);
        Assertion.verifyEqual(gradeDeletePage.isBackButtonDisplayed(), true, "Visibility of Back Button", t1);
    }


    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_410() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_410 :Grade Management - Delete Grade Confirm page Information", "To verify that Application displays the following information :\n" +
                "• Domain name\n" +
                "• Category name\n" +
                "• Grade code\n" +
                "• Grade name\n" +
                "  Button to click \n" +
                "• Confirm \n" +
                "• Back\n" +
                "on Delete Grade Confirm Page.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).loginAsSuperAdmin(saDeleteGrade);

        GradeDeletePage gradeDeletePage = new GradeDeletePage(t1);

        gradeDeletePage.navigateToLink();

        Object[] expectedHeaders = PageInit.fetchLabelTexts("//table[@class = 'wwFormTableC']/tbody/tr[1]/td");
        Object[] actualHeaders = UserFieldProperties.getLabels("label.grade.");

        Assertion.verifyArrayEquals(expectedHeaders, actualHeaders, "Verify Labels", t1);

        Assertion.verifyEqual(gradeDeletePage.isDeleteButtonDisplayed(), true, "Visibility of Delete Button", t1);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1})
    public void P1_TC_411() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_411 :Grade Management - Delete Grade Confirmation Check", "To verify that  after clicking on Confirm button on Delete Grade page , " +
                "the application should ask the user to confirm about the deletion of Grade.").assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.PVG_P1);

        Login.init(t1).loginAsSuperAdmin(saDeleteGrade);

        Grade newGrade = new Grade(Constants.MERCHANT);

        try {
            GradeManagement.init(t1).addGrade(newGrade);

            GradeDeletePage gradeDeletePage = new GradeDeletePage(t1);

            gradeDeletePage.navigateToLink();
            gradeDeletePage.selectGrade(newGrade.GradeCode);
            gradeDeletePage.clickDeleteButton();
            gradeDeletePage.confirmDelete();

            String alertText = AlertHandle.getAlertText(t1);

            Assertion.verifyMessageContain(alertText, "grade.delete.confirm", "Confirmation of Grade Delete", t1);

            AlertHandle.rejectAlert(t1);

        } finally {
            GradeManagement.init(t1).deleteGrade(newGrade, true);
        }

    }

    @Test(priority = 13, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.SYSTEM_TEST})
    public void TC_ECONET_SIT_0048() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_SIT_0048 : Grade Add Test", "To verify that Super Admin should be able to Add grade in system\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_P1, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).loginAsSuperAdmin("ADD_GRADES");

        GradeAddPage gradeAddPage1 = new GradeAddPage(t1);

        String gradeCode = DataFactory.getRandomString(50);
        String gradeName = DataFactory.getRandomString(50);

        gradeAddPage1.navigateToLink();
        gradeAddPage1.clickAddButton();
        Utils.putThreadSleep(NumberConstants.SLEEP_1000);
        gradeAddPage1.selectDomainByValue(grade.DomainCode);
        gradeAddPage1.selectCategoryByValue(grade.CategoryCode);
        gradeAddPage1.enterGradeName(gradeName);
        gradeAddPage1.enterGradeCode(gradeCode);

        gradeAddPage1.clickSaveButton();
        // If Condition Added for Negative case for which Confirm Click is not required
        if (ConfigInput.isConfirm) {
            gradeAddPage1.clickConfirmButton();
        }


        if (ConfigInput.isAssert) {
            String expected = MessageReader.getDynamicMessage("channel.grade.add.success", gradeCode.substring(0, 10), gradeName.substring(0, 40));
            Assertion.verifyDynamicActionMessageContain(expected, "Group Role Add", pNode);
        }

    }


}
