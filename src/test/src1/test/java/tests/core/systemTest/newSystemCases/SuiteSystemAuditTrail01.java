package tests.core.systemTest.newSystemCases;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.adminTrailManagement.AuditAdminTrail;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/9/2017.
 */
public class SuiteSystemAuditTrail01 extends TestInit {


    private String domainCode, categoryCode, fromDate, toDate;
    private User cashinUser, subs;
    private OperatorUser netAdmin;

    /**
     * SETUP
     */
    @BeforeMethod(alwaysRun = true)
    public void setup() throws Exception {
        try {
            categoryCode = Constants.WHOLESALER;
            domainCode = DataFactory.getDomainCode(Constants.WHOLESALER);
            fromDate = new DateAndTime().getDate(-20);
            toDate = new DateAndTime().getDate(0);
            cashinUser = DataFactory.getChannelUserWithAccess("CIN_WEB");
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : SYS_TC_AuditTrail_S_0009
     * DESC : To verify that "Network Admin" should be able to the see details in Admin Trail and able to download file.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0009() throws Exception {
        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0009", "To verify that \"Network Admin\" should be able to see details in Audit Trail and able to download file.");
        t1.assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.AUDIT_TRAIL);

        try {
            netAdmin = DataFactory.getOperatorUserWithAccess("ADMIN_TRAIL", Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdmin);
            AuditAdminTrail.init(t1).verifyAuditTrail(domainCode, categoryCode, fromDate, toDate);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : POSITIVE
     * ID : SYS_TC_AuditTrail_S_0002
     * DESC : To verify that all the financial transaction done within the number of days mentioned in the "from date" and "to date" range entered should be displayed in the audit trail.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_561() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_561",
                "To verify that all the financial transaction done within the number of days mentioned in the \"from date\" and \"to date\" range entered should be displayed in the audit trail.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_P1);

        try {
            Login.init(t1).login(cashinUser);

            String transID = TransactionManagement.init(t1)
                    .performCashIn(subs,
                            "10",
                            defaultProvider.ProviderName);

            Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");
            AuditAdminTrail.init(t1)
                    .verifyAuditTrail(cashinUser, fromDate, toDate, transID);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0003
     * DESC : To verify that "Network Admin" should not be able to view the details when "Domain name" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0003() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0003", "To verify that \"Network Admin\" should not be able to view the Audit Trail details when \"Domain name\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");
        cashinUser.DomainCode = null;
        cashinUser.CategoryCode = null;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);

        Assertion.verifyErrorMessageContain("audit.domain.required", "Verify Error Message When Domain Name is not selected.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0004
     * DESC : To verify that "Network Admin" should not be able to view the details when "Domain name" is not selected.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0004() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0004", "To verify that \"Network Admin\" should not be able to view the details when \"Category name\" is not selected.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");

        cashinUser.CategoryCode = null;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);

        Assertion.verifyErrorMessageContain("audit.category.required", "Verify Error Message When Category Name is not selected.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0005
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is not Entered.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0005() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0005",
                "To verify that \"Network Admin\" should not be able to view the details when \"From Date\" is not Entered.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");

        fromDate = Constants.BLANK_CONSTANT;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);
        Assertion.verifyErrorMessageContain("audittrail.validation.fromdate", "Verify Error Message When From Date is not Entered.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0006
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is not Entered.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0006() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0006",
                "To verify that \"Network Admin\" should not be able to view the details when \"To Date\" is not Entered.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");

        toDate = Constants.BLANK_CONSTANT;

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);
        Assertion.verifyErrorMessageContain("audittrail.validation.todate", "Verify Error Message When \"To Date\" is not Entered.", t1);
    }


    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0007
     * DESC : To verify that "Network Admin" should not be able to view the details when "From Date" is the future date.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0007() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0007",
                "To verify that \"Network Admin\" should not be able to view the details when \"From Date\" is the future date.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");

        fromDate = new DateAndTime().getDate(+1);

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);
        Assertion.verifyErrorMessageContain("btsl.error.msg.fromdatebeforecurrentdate", "Verify Error Message When \"From Date\" is Future Date.", t1);
    }

    /**
     * TEST : NEGATIVE
     * ID : SYS_TC_AuditTrail_S_0008
     * DESC : To verify that "Network Admin" should not be able to view the details when "To Date" is the future date.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.AUDIT_TRAIL, FunctionalTag.PVG_SYSTEM})
    public void SYS_TC_AuditTrail_S_0008() throws Exception {

        ExtentTest t1 = pNode.createNode("SYS_TC_AuditTrail_S_0008",
                "To verify that \"Network Admin\" should not be able to view the details when \"To Date\" is the future date.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUDIT_TRAIL);

        Login.init(t1).loginAsSuperAdmin("AUDIT_TRAIL");

        toDate = new DateAndTime().getDate(+1);

        AuditAdminTrail.init(t1).negativeTestWithoutConfirm().verifyAuditTrail(cashinUser, fromDate, toDate, null);
        Assertion.verifyErrorMessageContain("btsl.error.msg.todatebeforecurrentdate", "Verify Error Message When \"To Date\" is Future Date.", t1);
    }
}
