package tests.core.systemTest.UAP;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 8/8/2017.
 */

public class Suite_UAP_GradeManagement_01 extends TestInit {

    private Grade grade;

    /**
     * SETUP
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        grade = new Grade(Constants.BILLER);
    }


    /**
     * TEST : Grade Add Test
     * DESC : To verify that Super Admin should be able to Add grade in system.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void gradeAddTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0074 : Grade Add Test", "To verify that Super Admin should be able to Add grade in system")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_P1, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).loginAsSuperAdmin("ADD_GRADES");

        GradeManagement.init(t1).addGrade(grade);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0})
    public void gradeModifyTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0072 : Grade Modify Test", "To verify that Super Admin should be able to Modify grade in system\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).loginAsSuperAdmin("MODIFY_GRADES");

        if (grade.isCreated) {
            GradeManagement.init(t1).modifyGrade(grade);
        } else {
            t1.skip("Skipping the Case as Grade creation failed");
        }
    }


    /**
     * TEST : GRADE DELETE
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, alwaysRun = true)
    public void gradeDeleteTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0073 : Grade Delete Test", "To verify that Super Admin should be able to Delete grade in system\n");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_P1, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        Login.init(t1).loginAsSuperAdmin("DELETE_GRADES");

        if (grade.isCreated) {
            GradeManagement.init(t1).deleteGrade(grade, true);
        } else {
            t1.skip("Skipping the Case as Grade creation failed");
        }
    }
}