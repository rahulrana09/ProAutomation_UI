package tests.core;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class DummyTest extends TestInit {
    @Test
    public void run() throws Exception {
        User chUser = DataFactory.getChannelUserWithAccess(Constants.WHOLESALER);
        User subUser = DataFactory.getChannelUserWithAccess(Constants.SUBSCRIBER);
        ExtentTest t1 = pNode.createNode("test", "Description");

        User barredAsSender = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
        User churnInitiated = CommonUserManagement.init(t1).getChurneInitiatedUser(Constants.WHOLESALER, null);
        User churned = CommonUserManagement.init(t1).getChurnedUser(Constants.WHOLESALER, null);
        User deleted = CommonUserManagement.init(t1).getDeletedUser(Constants.SUBSCRIBER, null);
        User deleteInitiated = CommonUserManagement.init(t1).getDeleteInitiatedUser(Constants.WHOLESALER, null);
        User suspended = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);

        User entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
        OperatorUser entAdmin = DataFactory.getBulkPayerWithAccess("ADD_EMP", entUser);


    }

    @Test(groups = {FunctionalTag.MONEY_SMOKE})
    public void testCashIn() {
        ExtentTest t1 = pNode.createNode("MON-XXX-01", "Test that Cash in service is working fine");
        try {
            User wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            User subsUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge tRuleCashIn = new ServiceCharge(Services.CASHIN,
                    wholeSaler,
                    subsUser,
                    null, null, null, null);

            // configure Transfer Rule
            TransferRuleManagement.init(t1).configureTransferRule(tRuleCashIn);

            // make sure Channel user has sufficient balance
            TransactionManagement.init(t1).makeSureLeafUserHasBalanceUI(wholeSaler);

            // Perform Cash In
            Login.init(t1).login(wholeSaler);
            TransactionManagement.init(t1).performCashIn(subsUser, Constants.MAX_CASHIN_AMOUNT, defaultProvider.ProviderName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}

