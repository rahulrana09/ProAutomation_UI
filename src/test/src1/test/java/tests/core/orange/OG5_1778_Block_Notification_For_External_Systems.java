package tests.core.orange;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.blockNotification.BlockNotificationConfiguration;
import framework.features.blockNotification.ExternalSystemDefinitions;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;


public class OG5_1778_Block_Notification_For_External_Systems extends TestInit {

    private static String SERVICE_TYPE = "Cash in";
    private static String[] BLOCK_NOTIFICATION_TEMPLATE = {"CASHIN.transactionIsSuccessful.sender",
            "CASHIN.transactionIsSuccessful.receiver",
            "CASHIN.transactionIsSuccessful.transactor"};

    //@Test(groups = {FunctionalTag.MONEY_SMOKE})
    @Test()
    public void OG5_1778_01() {
        ExtentTest t1 = pNode.createNode("OG5_1778_01", "Verify that the user is able to add, modify and delete a external source");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        String newSourceName = "NewSourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName)
                    .modifyExternalSystem(sourceName, newSourceName)
                    .deleteExternalSystem(newSourceName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_02() {
        ExtentTest t1 = pNode.createNode("OG5_1778_02", "Verify that user should not be able to save a new source with an existing source name");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions externalSystemDefinitions = ExternalSystemDefinitions.init(t1);

            externalSystemDefinitions.saveExternalSystem(sourceName);

            startNegativeTest();
            externalSystemDefinitions.saveExternalSystem(sourceName);

            Assertion.verifyErrorMessageContain("message.source.already.exists", "Verify system is throwing appropriate error message", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_03() {
        ExtentTest t1 = pNode.createNode("OG5_1778_03", "verify source name is NOT case sensitive and system throws an appropriate error when user tries to save a new source with an existing name but in a different character case");
        String lowercaseSourceName = "sourcetest" + DataFactory.getRandomNumberAsString(3);
        String uppercaseSourcreName = "SOURCETEST" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions externalSystemDefinitions = ExternalSystemDefinitions.init(t1);
            externalSystemDefinitions.saveExternalSystem(lowercaseSourceName);

            startNegativeTest();
            externalSystemDefinitions.saveExternalSystem(uppercaseSourcreName);

            Assertion.verifyErrorMessageContain("message.source.already.exists", "Verify system is throwing appropriate error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test()
    public void OG5_1778_04() {
        ExtentTest t1 = pNode.createNode("OG5_1778_04", "verify that user is able to add source name with alphanumeric characters, space and special character (-) allowed");
        String sourceName = "Source Test -" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_05() {
        ExtentTest t1 = pNode.createNode("OG5_1778_05", "verify user is able to add source name with alphanumeric characters, space and special character (/) allowed");
        String sourceName = "Source Test /" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_06() {
        ExtentTest t1 = pNode.createNode("OG5_1778_06", "verify user should get an appropriate error while adding source name with special character except - and / ");
        String sourceName = "Source Test @" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            startNegativeTest();
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName);
            Assertion.verifyErrorMessageContain("source.name.allowed", "Verify system is throwing appropriate error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_07() {
        ExtentTest t1 = pNode.createNode("OG5_1778_07", "verify that user will not be able to enter source name with more than 50 characters");
        String sourceName = DataFactory.getRandomNumberAsString(3) + "Source Test should not enter more than 50 characters";
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            startNegativeTest();
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName)
                    .sourceNameSize(sourceName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_08() {
        ExtentTest t1 = pNode.createNode("OG5_1778_08", "Verify user should get a appropriate error message while modifying a source name with an already existing name");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions externalSystemDefinitions = ExternalSystemDefinitions.init(t1);
            externalSystemDefinitions.saveExternalSystem(sourceName);
            externalSystemDefinitions.saveExternalSystem(sourceName + '0');

            startNegativeTest();
            externalSystemDefinitions.modifyExternalSystem(sourceName, sourceName + '0');
            Assertion.verifyErrorMessageContain("message.source.already.exists", "Verify system is throwing appropriate error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test
    public void OG5_1778_09() {
        ExtentTest t1 = pNode.createNode("OG5_1778_09",
                "Verify user is able to block a notification template and while unblocking the " +
                        "status changes from 'Y' to 'N' and blocking the same template will change the status from 'N' to 'Y' ");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {

            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYSCONF");
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName);
            BlockNotificationConfiguration blockNotificationConfiguration = BlockNotificationConfiguration.init(t1);
            blockNotificationConfiguration.blockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);
            String[] status = MobiquityGUIQueries.getStatusOfTemplateBlacklist(sourceName, BLOCK_NOTIFICATION_TEMPLATE);
            for (int i = 0; i < status.length; i++) {
                t1.info("Template '" + BLOCK_NOTIFICATION_TEMPLATE[i] + "', current status'" + status[i] + "'");
                Assertion.verifyEqual(status[i].equalsIgnoreCase("N"), false,
                        "Verify DB status for the Block Notification template, must not be N", t1);
            }
            blockNotificationConfiguration.unblockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);
            status = MobiquityGUIQueries.getStatusOfTemplateBlacklist(sourceName, BLOCK_NOTIFICATION_TEMPLATE);
            for (int i = 0; i < status.length; i++) {
                t1.info("Template '" + BLOCK_NOTIFICATION_TEMPLATE[i] + "', current status'" + status[i] + "'");
                Assertion.verifyEqual(status[i].equalsIgnoreCase("Y"), false,
                        "Verify DB status for the Block Notification template, must not be Y", t1);
            }
            blockNotificationConfiguration.blockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);
            status = MobiquityGUIQueries.getStatusOfTemplateBlacklist(sourceName, BLOCK_NOTIFICATION_TEMPLATE);
            for (int i = 0; i < status.length; i++) {
                t1.info("Template '" + BLOCK_NOTIFICATION_TEMPLATE[i] + "', current status'" + status[i] + "'");
                Assertion.verifyEqual(status[i].equalsIgnoreCase("N"), false,
                        "Verify DB status for the Block Notification template, must not be N", t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_10() {
        ExtentTest t1 = pNode.createNode("OG5_1778_10",
                "verify that user operation is cancelled when user clicks on 'Cancel' button on confirmation dialog and template changes are not saved");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions.init(t1)
                    .saveExternalSystem(sourceName);
            BlockNotificationConfiguration.init(t1)
                    .blockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE)
                    .unblockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE)
                    .cancelBlockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);
            String[] status = MobiquityGUIQueries.getStatusOfTemplateBlacklist(sourceName, BLOCK_NOTIFICATION_TEMPLATE);
            for (int i = 0; i < status.length; i++) {
                t1.info("Template '" + BLOCK_NOTIFICATION_TEMPLATE[i] + "', current status'" + status[i] + "'");
                Assertion.verifyEqual(status[i].equalsIgnoreCase("Y"), false,
                        "Verify DB status for the Block Notification template, must not be Y", t1);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_11() {
        ExtentTest t1 = pNode.createNode("OG5_1778_11", "verify that user will not be able to delete the source name if record with that source name exists in the notification_blacklist table");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(t1).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions externalSystemDefinitions = ExternalSystemDefinitions.init(t1);
            externalSystemDefinitions.saveExternalSystem(sourceName);
            BlockNotificationConfiguration.init(t1)
                    .blockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);

            startNegativeTest();
            externalSystemDefinitions.deleteExternalSystem(sourceName);
            Assertion.verifyErrorMessageContain("message.record.not.delete", "Verify system is throwing appropriate error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test
    public void OG5_1778_12() {
        ExtentTest test = pNode.createNode("OG5_1778_12", "End to end testing - Verify no notification is sent to sender on CASHIN success when notification template for CASHIN 'success' is blacklisted for the given source");
        String sourceName = "SourceTest" + DataFactory.getRandomNumberAsString(3);
        try {
            Login.init(test).loginAsSuperAdmin("BLOCK_NOTIFY_SYS");
            ExternalSystemDefinitions.init(test)
                    .saveExternalSystem(sourceName);
            BlockNotificationConfiguration.init(test)
                    .blockNotificationTemplate(sourceName, SERVICE_TYPE, BLOCK_NOTIFICATION_TEMPLATE);

            User sender = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User receiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            String txnId = Transactions.init(test).initiateCashInWithSource(receiver, sender, new BigDecimal("1"), sourceName.toLowerCase()).TransactionId;
            String senderResentNotification = Transactions.init(test).fetchRecentNotification(sender.MSISDN);

            Assertion.verifyEqual(senderResentNotification.contains(txnId), false,
                    "Verify that the Sender did not get the notification as the template is Blacklisted", test, false);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

}

