package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.dataEntity.UserWallets;
import framework.dataEntity.Wallet;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.walletPreference.WalletPreferences_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.util.List;

public class OG5_182_WalletPreference_GUI_Modification extends TestInit {

    private User whs, subs;
    private List<String> providerIds;
    private int numOfProvider;
    private String[] payIdArr;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        try {
            whs = new User(Constants.WHOLESALER);
            subs = new User(Constants.SUBSCRIBER);
            providerIds = DataFactory.getAllProviderId();
            numOfProvider = providerIds.size();
            payIdArr = DataFactory.getPayIdApplicableForSubs();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*
    test 1, verify is primary is removed only for the User Registration service
     */
    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void OG5_182_01() {
        ExtentTest t1 = pNode.createNode("OG5_182_01", "Verify that the Option to select is primary wallet" +
                "is no longer available only for User Registration Wallet mapping");
        try {
            WalletPreferences_Pg1 p1 = WalletPreferences_Pg1.init(t1);
            Login.init(t1).loginAsSuperAdmin("CAT_PREF");
            p1.navMapWalletPreferences();
            p1.selectPreferenceTypeVyVal(Constants.PREFERENCE_TYPE_USER_REGISTRATION);
            p1.selectDomainName(whs.DomainName);
            p1.selectCategoryName(whs.CategoryName);
            p1.selectRegTypeByVal(Constants.REGTYPE_FULL_KYC);
            Utils.captureScreen(t1);
            // verify that Is primary for wallet is not available


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void OG5_182_02() {
        ExtentTest t1 = pNode.createNode("OG5_182_02_CUSTREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using self registration (CUSTREG), and the pay id  is passed of the " +
                        "default system wallet, the subscriber must have only default wallet post creation");

        ExtentTest t2 = pNode.createNode("OG5_182_03_REGST",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration (REGST), and the pay id  is passed of the " +
                        "default system wallet, the subscriber must have only default wallet post creation");

        ExtentTest t3 = pNode.createNode("OG5_182_04_RSUBREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration(RSUBREG), and the pay id  is passed of the " +
                        "default system wallet, the subscriber must have only default wallet post creation");
        try {
            Wallet defaultWallet = DataFactory.getDefaultWallet();

            User subs_01 = new User(Constants.SUBSCRIBER);
            User subs_02 = new User(Constants.SUBSCRIBER);
            User subs_03 = new User(Constants.SUBSCRIBER);

            // map all the valid wallets for Subscriber
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(subs_01, payIdArr);

            SubscriberManagement.init(t1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subs_01, defaultWallet.WalletId, false);

            List<UserWallets> currentWalletList = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_01);
            reportAssociatedWallets(currentWalletList, t1);

            // if there are multiple currency provided, the default wallet must be multiple of providers
            Assertion.verifyEqual(currentWalletList.size(), numOfProvider,
                    "Verify that the number of wallets associated is correct After Subscriber Self Registration - CUSTREG", t1);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList, defaultWallet.WalletId, providerId, "CUSTREG", t1);
            }

            // create subscriber using REGST
            Transactions.init(t2)
                    .subRegistrationByAS400(subs_02, defaultWallet.WalletId);

            List<UserWallets> currentWalletList1 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_02);
            reportAssociatedWallets(currentWalletList, t2);

            // if there are multiple currency provided, the default wallet must be multiple of providers
            Assertion.verifyEqual(currentWalletList1.size(), numOfProvider,
                    "Verify that the number of wallets associated is correct After Subscriber Registration - REGST", t2);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList1, defaultWallet.WalletId, providerId, "REGST", t2);
            }

            // create subscriber using RSUBREG
            Transactions.init(t3)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subs_03, defaultWallet.WalletId);

            List<UserWallets> currentWalletList2 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_03);
            reportAssociatedWallets(currentWalletList, t3);

            // if there are multiple currency provided, the default wallet must be multiple of providers
            Assertion.verifyEqual(currentWalletList2.size(), numOfProvider,
                    "Verify that the number of wallets associated is correct After Subscriber Registration - RSUBREG", t3);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList, defaultWallet.WalletId, providerId, "RSUBREG", t3);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void OG5_182_03() {
        ExtentTest t1 = pNode.createNode("OG5_182_05_CUSTREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using self registration (CUSTREG), and the pay id  is passed of the " +
                        "Non default system wallet, the subscriber must have both " +
                        "default and the non default wallet post creation");

        ExtentTest t2 = pNode.createNode("OG5_182_06_REGST",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration (REGST), and the pay id  is passed of the " +
                        "default system wallet, the subscriber must have only default wallet post creation");

        ExtentTest t3 = pNode.createNode("OG5_182_07_RSUBREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration(RSUBREG), and the pay id  is passed of the " +
                        "default system wallet, the subscriber must have only default wallet post creation");
        try {
            int expectedWalletsCount = numOfProvider * 2; // default and non default
            Wallet defaultWallet = DataFactory.getDefaultWallet();
            String nonDefaultPayId = DataFactory.getNonDefaultPayIdExceptSpecialWallet().get(0);

            User subs_01 = new User(Constants.SUBSCRIBER);
            User subs_02 = new User(Constants.SUBSCRIBER);
            User subs_03 = new User(Constants.SUBSCRIBER);

            // map all the valid wallets for Subscriber
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(subs_01, payIdArr);

            SubscriberManagement.init(t1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subs_01, nonDefaultPayId, false);

            List<UserWallets> currentWalletList = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_01);
            reportAssociatedWallets(currentWalletList, t1);

            Assertion.verifyEqual(currentWalletList.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t1);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList, defaultWallet.WalletId, providerId, "CUSTREG", t1);
                verifyWalletIsAssociated(currentWalletList, nonDefaultPayId, providerId, "CUSTREG", t1);
            }

            // create subscriber using REGST
            Transactions.init(t2)
                    .subRegistrationByAS400(subs_02, nonDefaultPayId);

            List<UserWallets> currentWalletList1 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_02);
            reportAssociatedWallets(currentWalletList, t2);

            Assertion.verifyEqual(currentWalletList1.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t2);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList1, defaultWallet.WalletId, providerId, "CUSTREG", t2);
                verifyWalletIsAssociated(currentWalletList1, nonDefaultPayId, providerId, "CUSTREG", t2);
            }

            // create subscriber using RSUBREG
            Transactions.init(t3)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subs_03, nonDefaultPayId);

            List<UserWallets> currentWalletList2 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_03);
            reportAssociatedWallets(currentWalletList, t3);

            Assertion.verifyEqual(currentWalletList2.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t3);

            for (String providerId : providerIds) {
                verifyWalletIsAssociated(currentWalletList2, defaultWallet.WalletId, providerId, "CUSTREG", t3);
                verifyWalletIsAssociated(currentWalletList2, nonDefaultPayId, providerId, "CUSTREG", t3);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, enabled = false)
    public void OG5_182_04() {
        ExtentTest t1 = pNode.createNode("OG5_182_08_CUSTREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using self registration (CUSTREG), and no PayId is passed." +
                        "Verify that user gets all the wallets from mapped during wallet preference");

        ExtentTest t2 = pNode.createNode("OG5_182_09_REGST",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration (REGST), and no PayId is passed." +
                        "default system wallet, the subscriber must have only default wallet post creation");

        ExtentTest t3 = pNode.createNode("OG5_182_10_RSUBREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using registration(RSUBREG), and and no PayId is passed." +
                        "default system wallet, the subscriber must have only default wallet post creation");
        try {
            int expectedWalletsCount = numOfProvider * payIdArr.length; //All Wallets
            Wallet defaultWallet = DataFactory.getDefaultWallet();
            String nonDefaultPayId = DataFactory.getNonDefaultPayIdExceptSpecialWallet().get(0);

            User subs_01 = new User(Constants.SUBSCRIBER);
            User subs_02 = new User(Constants.SUBSCRIBER);
            User subs_03 = new User(Constants.SUBSCRIBER);

            // map all the valid wallets for Subscriber
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(subs_01, payIdArr);

            SubscriberManagement.init(t1)
                    .createSubscriberWithSpecificPayIdUsingAPI(subs_01, "", false);

            List<UserWallets> currentWalletList = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_01);
            reportAssociatedWallets(currentWalletList, t1);


            // if there are multiple currency provided, the default wallet must be multiple of providers
            Assertion.verifyEqual(currentWalletList.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t1);

            for (String providerId : providerIds) {
                for (String payId : payIdArr) {
                    verifyWalletIsAssociated(currentWalletList, payId, providerId, "RSUBREG", t1);
                }
            }

            // create subscriber using REGST
            Transactions.init(t2)
                    .subRegistrationByAS400(subs_02, "");

            List<UserWallets> currentWalletList1 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_02);
            reportAssociatedWallets(currentWalletList, t2);

            Assertion.verifyEqual(currentWalletList1.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t2);

            for (String providerId : providerIds) {
                for (String payId : payIdArr) {
                    verifyWalletIsAssociated(currentWalletList1, payId, providerId, "REGST", t2);
                }
            }

            // create subscriber using RSUBREG
            Transactions.init(t3)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subs_03, nonDefaultPayId);

            List<UserWallets> currentWalletList2 = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(subs_03);
            reportAssociatedWallets(currentWalletList, t3);

            Assertion.verifyEqual(currentWalletList2.size(), expectedWalletsCount,
                    "Verify that the number of wallets associated is correct", t3);

            for (String providerId : providerIds) {
                for (String payId : payIdArr) {
                    verifyWalletIsAssociated(currentWalletList1, payId, providerId, "RSUBREG", t3);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void OG5_182_05() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5_182_011",
                "Verify once single wallet is mapped in th wallet preference and subscriber is created by providing other pay ID, " +
                        "operation must fail with appropriate error message");

        User subs_01 = new User(Constants.SUBSCRIBER);

        try {

            String nonDefaultPayId = DataFactory.getNonDefaultPayIdExceptSpecialWallet().get(0);

            MobiquityGUIQueries.deleteKYCPreferenceForWallet(Constants.SUBSCRIBER, Constants.PREFERENCE_TYPE_USER_REGISTRATION, nonDefaultPayId);

            ConfigInput.isAssert = false;
            Transactions.init(t1)
                    .subsSelfRegistrationWithPayId(subs, nonDefaultPayId)
                    .verifyMessage("90001", "Verify that KYC Preference is not Defined");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(subs_01, payIdArr);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 6, enabled = false)
    public void OG5_182_06() {
        ExtentTest t1 = pNode.createNode("OG5_182_12_REGST",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using service 'REGST',  NO pay id is specified, it must get all the mapped wallets'");
        try {
            Wallet defaultWallet = DataFactory.getDefaultWallet();

            User sub2 = new User(Constants.SUBSCRIBER);

            // map all the valid wallets for Subscriber
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(sub2, payIdArr);

            // create subscriber using REGST
            Transactions.init(t1)
                    .subRegistrationByAS400(sub2, "");

            List<UserWallets> currentWalletList = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(sub2);
            reportAssociatedWallets(currentWalletList, t1);

            // if there are multiple currency provided, the default wallet must be multiple of providers * mapped wallets
            Assertion.verifyEqual(currentWalletList.size(), numOfProvider * payIdArr.length,
                    "Verify that the number of wallets associated is correct After Subscriber Self Registration", t1);

            for (String providerId : providerIds) {
                for (String payId : payIdArr) {
                    verifyWalletIsAssociated(currentWalletList, payId, providerId, "RSUBREG", t1);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, enabled = false)
    public void OG5_182_07() {
        ExtentTest t1 = pNode.createNode("OG5_182_13_RSUBREG",
                "Verify once all the valid wallets are mapped in wallet preference," +
                        "and Subscriber is created using service 'RSUBREG', " +
                        "it must get all the mapped wallets'");
        try {
            Wallet defaultWallet = DataFactory.getDefaultWallet();

            User sub2 = new User(Constants.SUBSCRIBER);

            // map all the valid wallets for Subscriber
            CurrencyProviderMapping.init(t1)
                    .mapWalletPreferencesUserRegistration(sub2, payIdArr);

            // create subscriber using RSUBREG
            Transactions.init(t1)
                    .SubscriberRegistrationByChannelUserWithKinDetails(sub2, "");

            List<UserWallets> currentWalletList = MobiquityGUIQueries.getActiveWalletsAssociatedWithUser(sub2);
            reportAssociatedWallets(currentWalletList, t1);

            // if there are multiple currency provided, the default wallet must be multiple of providers * mapped wallets
            Assertion.verifyEqual(currentWalletList.size(), numOfProvider * payIdArr.length,
                    "Verify that the number of wallets associated is correct After Subscriber Self Registration", t1);

            for (String providerId : providerIds) {
                for (String payId : payIdArr) {
                    verifyWalletIsAssociated(currentWalletList, payId, providerId, "RSUBREG", t1);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    private void reportAssociatedWallets(List<UserWallets> walletList, ExtentTest t1) {
        for (UserWallets wallet : walletList) {
            String[][] code = {{"PayId", wallet.payId}, {"Provider Id", wallet.providerId}, {"IsPrimary", wallet.isPrimaryStr}};
            Markup m = MarkupHelper.createTable(code);
            t1.info(m);
        }
    }

    private void verifyWalletIsAssociated(List<UserWallets> walletList, String walletId, String providerId, String serviceCode, ExtentTest t1) throws IOException {
        boolean isFound = false, isPrimary = false;
        if (walletId.equals(GlobalData.defaultWallet.WalletId))
            isPrimary = true;
        try {
            for (UserWallets wallet : walletList) {
                if (wallet.providerId.equals(providerId) && wallet.payId.equals(walletId) && wallet.isPrimary == isPrimary) {
                    isFound = true;
                    t1.info("Wallet match!");
                    t1.info("Provider Id:" + wallet.providerId + " WalletId: " + wallet.payId + " Is Primary: " + wallet.isPrimary);
                }
            }

            Assertion.verifyEqual(isFound, true,
                    "SERVICE: " + serviceCode + ", Verify that Requested Wallet with id: " + walletId + " and Provider Id " + providerId + " is Associated with the User", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }


    }

}
