package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UserWallets;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.bulkUserDeletion.BulkUserDeletion;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.*;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.*;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.junit.Ignore;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.*;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

public class OG5_1021_BulkUserDeletion extends TestInit {
    private OperatorUser bulkAdmin, admin, o2cInitiator, o2cApprover1, o2cApprover2, optBankApprover, optBankInitiator;
    private User deleteWhs, deleteSubs, wholesaler, silverSubscriber;
    private RechargeOperator RCoperator;
    private Bank defaultBank;
    private String custIdLength, subCustId, whsCustId, prefValue, prefValue1;

    //TODO - OG5-1906 - NOT AUTOMATED : Selenium framework doesn't support multi currency end to end
    //TODO - Creating users with multiple currency and multiple SVA's and performing services where balance will be in FROZEN_AMOUNT & FIC column

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ExtentTest s = pNode.createNode("Setup", "Setup Specific to this Script");

        try {
            defaultBank = DataFactory.getAllTrustBanksLinkedToProvider(defaultProvider.ProviderName).get(0);
            bulkAdmin = DataFactory.getOperatorUserWithAccess(Roles.BULK_USER_DELETION);
            RCoperator = OperatorUserManagement.init(s).getRechargeOperator(defaultProvider.ProviderId);
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            silverSubscriber = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            admin = new OperatorUser(Constants.NETWORK_ADMIN);
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            o2cApprover2 = DataFactory.getOperatorUserWithAccess("O2C_APP2");
            optBankInitiator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            optBankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");

            User rechargeReceiver = new User(Constants.ZEBRA_MER);

            custIdLength = MobiquityGUIQueries.fetchDefaultValueOfPreference("CUSTOMER_ID_LENGTH");
            prefValue1 = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");

            /**
             * JIRA test case ID - OG5-1905
             * Verify that Bulk User Delete is always single step even if ACCOUNT_CLOSURE_TWO_STEP = Y in the system
             */
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("ACCOUNT_CLOSURE_TWO_STEP");
            if (!prefValue.equalsIgnoreCase("Y")) {
                SystemPreferenceManagement.init(pNode).updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "Y");
            }

            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MODIFY_SUBS_MAKER_CHECKER_ALLOWED");
            if (!prefValue.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(pNode).updateSystemPreference("MODIFY_SUBS_MAKER_CHECKER_ALLOWED", "TRUE");
            }

            //Performing O2C to Commission wallet of Wholesaler [Tax to be deducted during Delete customer By Agent service]
            SfmResponse o2cResponse = Transactions.init(pNode).initiateO2C(o2cInitiator, wholesaler, Constants.minimun_AMOUNT, Constants.COMMISSION_WALLET,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(pNode).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, wholesaler.MSISDN)) {
                Transactions.init(pNode).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            //Creating Transfer Rule for Account Closure By Agent
            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, silverSubscriber, wholesaler, null,
                    null, null, null);
            ServiceChargeManagement.init(pNode).configureServiceCharge(accClosure);

            //Creating Transfer Rule for Recharge - Self for Wholesaler
            ServiceCharge whsScRc = new ServiceCharge(Services.SELF_RECHARGE, wholesaler, rechargeReceiver, null, null, null,
                    null);
            TransferRuleManagement.init(pNode).configureTransferRule(whsScRc);

            //Creating Transfer Rule for Recharge - Self for Subscriber
            ServiceCharge subScRc = new ServiceCharge(Services.SELF_RECHARGE, silverSubscriber, rechargeReceiver, null, null, null,
                    null);
            TransferRuleManagement.init(pNode).configureTransferRule(subScRc);

            /*
             * - JIRA test case ID - OG5-1908
             * - Deleting non-financial service charge if exist for Get Banks for Subscriber
             */
            ServiceCharge nfscSCharge = new ServiceCharge(Services.GET_BANKS_PER_PROVIDER, silverSubscriber, admin, null, null, null,
                    null);
            ServiceChargeManagement.init(pNode).deleteNFSChargeAllVersions(nfscSCharge);
            /*
             * - JIRA test case ID - OG5-1904 & OG5-1911
             * - JIRA Defect ID - OG5-2205
             * - ACQUISITION FESS service charge can be deleted if ACQUISITION_PAID != N for any of the active silverSubscriber in MTX_PARTY_ACCESS table
             * - Creating Acquisition Fess service charge with charge and commission value as 1 [value more than 0] so that acquisition_paid & comm_paid is N
             */
            /*ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(pNode).deleteServiceChargeAllVersions(sCharge);*/ // TODO - JIRA Defect ID - OG5-2205

            ServiceCharge sCharge1 = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);

            //creating transfer rules for Transaction Correction
            ServiceCharge scTxnCorrect = new ServiceCharge(Services.TXN_CORRECTION, silverSubscriber, wholesaler, null, null,
                    null, null);
            TransferRuleManagement.init(pNode).configureTransferRule(scTxnCorrect);

            //creating transfer rules for Transaction Correction
            ServiceCharge scTxnCorrect1 = new ServiceCharge(Services.TXN_CORRECTION, wholesaler, admin, null, null,
                    null, null);
            TransferRuleManagement.init(pNode).configureTransferRule(scTxnCorrect1);

            //creating transfer rules for P2P - send money to unknown
            ServiceCharge charge = new ServiceCharge(Services.P2PNONREG, silverSubscriber, admin, null, null, null, null);
            TransferRuleManagement.init(pNode).configureTransferRule(charge);

            //Creating transfer rule for Stock Reimbursement
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, silverSubscriber, admin, null, null, null,
                    null);
            TransferRuleManagement.init(pNode).configureTransferRule(obj);

            //Creating transfer rule for Stock Reimbursement
            ServiceCharge obj1 = new ServiceCharge(Services.OPERATOR_WITHDRAW, wholesaler, admin, null, null, null,
                    null);
            TransferRuleManagement.init(pNode).configureTransferRule(obj1);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            /*
             * - JIRA Defect ID - OG5-2205
             * - ACQUISITION FESS service charge can be deleted if ACQUISITION_PAID != N for any of the active silverSubscriber in MTX_PARTY_ACCESS table
             * - Creating Acquisition Fess service charge with charge and commission value as 0 so that acquisition_paid & comm_paid is Y
             */
            /*ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(tearDown).deleteServiceChargeAllVersions(sCharge);*/ // TODO - JIRA Defect ID - OG5-2205

            ServiceCharge sCharge1 = new ServiceCharge(Services.ACQFEE, silverSubscriber, admin, null, null,
                    null, null);
            ServiceChargeManagement.init(tearDown).configureServiceCharge(sCharge1);

            ServiceCharge nfscSCharge = new ServiceCharge(Services.GET_BANKS_PER_PROVIDER, silverSubscriber, admin, null, null, null,
                    null);
            ServiceChargeManagement.init(tearDown).configureNonFinancialServiceCharge(nfscSCharge);

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1890() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1890", "Verify that barred channel user & subscriber is deleted successfully through Bulk User Delete");
        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);

        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {
                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, Constants.minimun_AMOUNT);

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            Transactions.init(t1).userBarUnbar(deleteWhs, "BAR", Constants.BAR_AS_BOTH);
            Transactions.init(t1).userBarUnbar(deleteSubs, "BAR", Constants.BAR_AS_BOTH);

            UsrBalance preSubBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBalance.Balance.intValue() + preWhsBalance.Balance.intValue());

            String subDbStatus = MobiquityGUIQueries.getBarredUserDetails(subsUserId);
            System.out.println("whsDbStatus" + subDbStatus);

            String whsDbStatus = MobiquityGUIQueries.getBarredUserDetails(whsUserId);
            System.out.println("whsDbStatus" + whsDbStatus);

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            //once user is deleted, records should be removed from mtx_party_black_list table

            if (subDbStatus == null) {
                t1.pass("Deleted Subscriber details is not available in mtx_party_black_list table");
            } else {
                t1.fail("Deleted Subscriber details is still available in mtx_party_black_list table");
            }

            if (whsDbStatus == null) {
                t1.pass("Deleted Channel User details is not available in mtx_party_black_list table");
            } else {

                t1.fail("Deleted Channel User details is still available in mtx_party_black_list table");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1891() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1891", "Verify that suspend initiated " +
                "channel user is deleted successfully through through Bulk User Delete");
        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);

        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);

            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {

                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_SCU"));
            ChannelUserManagement.init(t1).initiateSuspendChannelUser(deleteWhs);

            String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus.equalsIgnoreCase(Constants.STATUS_SUSPEND_INITIATE)) {
                Assertion.logAsPass("status of the user is " + dbStatus, t1);
            }

            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preWhsBalance.Balance.intValue());

            //once user is deleted, records should be removed from temporary table user_m
            dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus == null) {
                Assertion.logAsPass("user record is removed from user_m table as expected", t1);
            } else {

                t1.fail("user record is not removed from user_m table ");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1892() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1892", "Verify that suspended channel user & silverSubscriber is deleted successfully through through Bulk User Delete");
        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(pNode).createSubscriberDefaultMapping(deleteSubs, true, false);

            Transactions.init(t1).suspendResumeUser(deleteWhs, "SUSPEND");
            Transactions.init(t1).suspendResumeUser(deleteSubs, "SUSPEND");

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            UsrBalance preSubBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBalance.Balance.intValue() + preWhsBalance.Balance.intValue());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1893() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1893", "Verify that system should not allow to delete the delete initiated channel user & silverSubscriber through Bulk User Delete");

        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            deleteSubs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "7");

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_DCU"));
            ChannelUserManagement.init(t1).initiateChannelUserDelete(deleteWhs);

            String txnID = SubscriberManagement.init(t1).deleteSubscriberByAgent(deleteSubs, DataFactory.getDefaultProvider().ProviderId, true);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), Constants.STATUS_DELETE_INITIATE,
                    "Verify Subscriber Status", t1);

            String dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus.equalsIgnoreCase(Constants.DELETE_INITIATE_CONSTANT)) {
                Assertion.logAsPass("Verify Wholesaler Status : " + dbStatus, t1);
            }

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), Constants.STATUS_DELETE_INITIATE,
                    "Verify Subscriber Status", t1);

            dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus.equalsIgnoreCase(Constants.DELETE_INITIATE_CONSTANT)) {
                Assertion.logAsPass("Verify Wholesaler Status : " + dbStatus, t1);
            }

            ChannelUserManagement.init(t1).approveRejectDeleteChannelUser(deleteWhs, true);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            Transactions.init(t1).accountClosurebyAgentConfirmation(deleteSubs.MSISDN, txnID);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            dbStatus = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus == null) {
                Assertion.logAsPass("user record is removed from user_m table as expected", t1);
            } else {
                t1.fail("user record is not removed from user_m table");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1894() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1894", "Verify that modify initiated channel user & silverSubscriber is deleted successfully through Bulk User Delete");
        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_MCU"));
            ChannelUserManagement.init(t1).initiateChannelUserModification(deleteWhs);

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(deleteSubs);

            SubscriberManagement.init(t1).modifyInitiateSubscriber(wholesaler, deleteSubs, "Passport", deleteSubs.MSISDN);

            String dbStatus_whs = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus_whs.equalsIgnoreCase(Constants.STATUS_MODIFY_INITIATE_CHANNEL)) {
                Assertion.logAsPass("Status of the user is " + dbStatus_whs, t1);
            }
            String dbStatus_subs = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode);
            if (dbStatus_subs.equalsIgnoreCase(Constants.STATUS_MODIFY_INITIATE_SUBS)) {
                Assertion.logAsPass("Status of the user is " + dbStatus_subs, t1);
            }
            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "N", "Verify Wholesaler Status", t1);

            dbStatus_whs = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode);
            if (dbStatus_whs == null) {
                Assertion.logAsPass("user record is removed from user_m table as expected", t1);
            }
            dbStatus_subs = MobiquityGUIQueries.dbGetUserInitiatedStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode);
            if (dbStatus_subs == null) {
                Assertion.logAsPass("user record is removed from mtx_party_m table as expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1895() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1895", "Verify that system should not allow to delete the churn initiated channel user & subscriber through Bulk User Delete");
        t1.assignCategory(FunctionalTag.BULK_USER_DELETION);
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(pNode).createSubscriberDefaultMapping(deleteSubs, true, false);

            //Churn initiate the user
            String batchId = CommonUserManagement.init(t1).churnInitiateUser(deleteWhs);
            String batchId1 = CommonUserManagement.init(t1).churnInitiateUser(deleteSubs);

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("1930", null);
            String expMsg_whs = MessageReader.getMessage("1930", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);

            //POST test case verification churned the user
            CommonUserManagement.init(t1).approveRejectChurnInitiate(deleteWhs, batchId, true);
            CommonUserManagement.init(t1).approveRejectChurnInitiate(deleteSubs, batchId1, true);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "N", "Verify Wholesaler Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "N", "Verify Subscriber Status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1896_1897_1898_1899() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1896", "Verify that system should not allow to delete the channel user & subscriber through Bulk User Delete whose " +
                "bank account addition is pending for approval");
        HashMap<String, String> users = null;
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            if (!prefValue1.equalsIgnoreCase("N")) {
//                subCustId = DataFactory.getRandomNumberAsString(Integer.valueOf(custIdLength)); //TODO - changes if custIdLength value is more than 9
                subCustId = DataFactory.getRandomNumberAsString(9);
                whsCustId = DataFactory.getRandomNumberAsString(9);
            } else {
                subCustId = deleteSubs.MSISDN;
                whsCustId = deleteWhs.MSISDN;
            }

            // make sure Bank Account link KYC Preference is Mapped
            CurrencyProviderMapping.init(t1).linkPrimaryBankWalletPreference(deleteSubs);
            CurrencyProviderMapping.init(t1).linkPrimaryBankWalletPreference(deleteWhs);

            Login.init(t1).login(optBankInitiator);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName, subCustId, subCustId, false);
            BankAccountAssociation.init(t1)
                    .initiateBankAccountRegistration(deleteWhs, defaultProvider.ProviderName, defaultBank.BankName, whsCustId, whsCustId, false);

            users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("991235", null);
            String expMsg_whs = MessageReader.getMessage("991235", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        ExtentTest t2 = pNode.createNode("OG5-1899", "Verify that system should not allow to delete the channel user & subscriber through Bulk User Delete if user " +
                "is having bank account linked");
        try {
            //Approving the add initiated bank accounts
            Login.init(t2).login(optBankApprover);
            CommonUserManagement.init(t2).approveAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName);
            CommonUserManagement.init(t2).approveAssociatedBanksForUser(deleteWhs, defaultProvider.ProviderName, defaultBank.BankName);

            Login.init(t2).login(bulkAdmin);
            BulkUserDeletion.init(t2).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("3500", null);
            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t2);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("OG5-1898", "Verify that system should not allow to delete the channel user & subscriber through Bulk User Delete whose " +
                "bank account modification is pending for approval");
        try {
            //Modifying the bank account(s) of silverSubscriber
            SubscriberManagement.init(t3).navigateToModifySubscriberWalletAssociationPage(deleteSubs);
            ModifySubscriber_page4.init(t3).clickSubmitPg4(); //Click on Next button in wallet association page
            SubscriberManagement.init(t3).modifySubscriberExistingBankStatus("Suspended", subCustId);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_MCU"));
            BankAccountAssociation.init(t1).modifyBankStatusForChannelUser(deleteWhs, "Suspended", whsCustId);

            Login.init(t3).login(bulkAdmin);
            BulkUserDeletion.init(t3).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("3500", null);
            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t3);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t3);

            SubscriberManagement.init(t3).modifyApprovalSubs(deleteSubs);

            //Rejecting the modify initiated bank accounts
            Login.init(t1).login(optBankApprover);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName, subCustId);
            CommonUserManagement.init(t1)
                    .rejectAssociatedBanksForUser(deleteWhs, defaultProvider.ProviderName, defaultBank.BankName, whsCustId);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("OG5-1897", "Verify that system should not allow to delete the channel user & subscriber through Bulk User Delete whose " +
                "bank account deletion is pending for approval");
        try {
            //Deleting the bank account(s) of silverSubscriber
            SubscriberManagement.init(t4).navigateToModifySubscriberWalletAssociationPage(deleteSubs);
            ModifySubscriber_page4.init(t4).clickSubmitPg4(); //Click on Next button in wallet association page
            SubscriberManagement.init(t4).modifySubscriberExistingBankStatus("Deleted", subCustId);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_MCU"));
            BankAccountAssociation.init(t1).modifyBankStatusForChannelUser(deleteWhs, "Deleted", whsCustId);

            Login.init(t4).login(bulkAdmin);
            BulkUserDeletion.init(t4).performBulkUserDeletion(fileName);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t4);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t4);

            //Approving the delete initiated bank accounts
            Login.init(t4).login(optBankApprover);
            CommonUserManagement.init(t4).approveAssociatedBanksForUser(deleteSubs, defaultProvider.ProviderName, defaultBank.BankName);
            CommonUserManagement.init(t4).approveAssociatedBanksForUser(deleteWhs, defaultProvider.ProviderName, defaultBank.BankName);

            Login.init(t4).login(bulkAdmin);
            BulkUserDeletion.init(t4).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("3500", null);
            String expMsg_whs = MessageReader.getMessage("3500", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "N", "Verify Subscriber Status", t4);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "N", "Verify Wholesaler Status", t4);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

    }

    @Test(priority = 8, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1900() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1900", "Verify that system should not allow to delete the channel user & subscriber through Bulk User Delete if " +
                "there are transactions in ambiguous state for the user where user is the payer/payee");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(pNode).createSubscriberDefaultMapping(deleteSubs, true, false);

            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, "5", DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);

            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {

                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }
            //Performing recharge
            SfmResponse whsRecharge = Transactions.init(t1).rechargeSelf(deleteWhs, RCoperator.OperatorId, "3", Constants.NORMAL_WALLET);
            SfmResponse subRecharge = Transactions.init(t1).rechargeSelf(deleteSubs, RCoperator.OperatorId, "3", Constants.NORMAL_WALLET);

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(subRecharge.TransactionId), Constants.TXN_STATUS_AMBIGUOUS, "Transaction Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(whsRecharge.TransactionId), Constants.TXN_STATUS_AMBIGUOUS, "Transaction Status", t1);

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("991234", null);
            String expMsg_whs = MessageReader.getMessage("991234", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteSubs.MSISDN, deleteSubs.CategoryCode), "Y", "Verify Subscriber Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, deleteWhs.CategoryCode), "Y", "Verify Wholesaler Status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 9, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1901_1912() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1901", "Verify that system should allow to delete the subscriber through Bulk User Delete when user is having pending " +
                "transaction [frozen_amount] waiting for approval [incase of subscriber even if FAIL_FROZEN_TXN_ON_DEL = TRUE in the system]");

        ExtentTest t2 = pNode.createNode("OG5-1912", "Verify that system should allow to delete the channel user through Bulk User Delete when user is having pending " +
                "transaction [frozen_amount] waiting for approval");

        try {
            //Creating Subscriber
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(pNode).createSubscriberDefaultMapping(deleteSubs, true, false);

            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            //Performing 1st Cash In service
            String transID = Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5").TransactionId;
            //Performing 2nd Cash In service
            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "7");

            //1st O2C transaction
            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {
                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            //2nd O2C transaction
            SfmResponse o2cResponse2 = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse3 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse2.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {
                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse3.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }

            //Performing Stock Reimbursement
            String reimSubTxnId = StockManagement.init(t1)
                    .initiateStockReimbursement(deleteSubs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3), "3",
                            "reimbursement", DataFactory.getDefaultWallet().WalletId);

            //Performing Stock Reimbursement
            String reimWhsTxnId = StockManagement.init(t1)
                    .initiateStockReimbursement(deleteWhs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3), "3",
                            "reimbursement", DataFactory.getDefaultWallet().WalletId);

            //Subscriber Pre Balance
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, null, null);
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, null, null);

            setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
            setTxnProperty(of("txn.reversal.allowed.services", "CASHIN,O2C"));

            //Initiating Transaction Correction for the 1st Cash In
            String txnInitiateId = TransactionCorrection.init(t1).initiateTxnCorrection(transID, false, false);

            //Initiating Transaction Correction for the O2C transaction
            String txnInitiateId1 = TransactionCorrection.init(t1).initiateTxnCorrection(o2cResponse2.TransactionId, false, false);
            Thread.sleep(1000);

            //Subscriber Post Balance
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(deleteSubs, null, null);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, null, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            Assert.assertEquals(preSubsBalance.frozenBalance.intValue() + 8, postSubsBalance.frozenBalance.intValue());

            Assert.assertEquals(preWhsBalance.frozenBalance.intValue() + 8, postWhsBalance.frozenBalance.intValue());

            String value = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_FROZEN_TXN_ON_DEL");
            if (!value.equalsIgnoreCase("TRUE")) {
                Login.init(t1).loginAsSuperAdmin(DataFactory.getSuperAdminWithAccess("PREF001"));
                Preferences.init(t1).modifySystemPreferences("FAIL_FROZEN_TXN_ON_DEL", "TRUE");
            }

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubsBalance.Balance.intValue() + preWhsBalance.Balance.intValue());

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnInitiateId), Constants.TXN_STATUS_FAIL, "Verify Transaction Correction Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnInitiateId1), Constants.TXN_STATUS_FAIL, "Verify Transaction Correction Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(reimSubTxnId), Constants.TXN_STATUS_FAIL, "Verify Reimbursement Transaction Status", t1);
            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(reimWhsTxnId), Constants.TXN_STATUS_FAIL, "Verify Reimbursement Transaction Status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 10, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1902() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1902", "Verify that system should allow to delete silverSubscriber through Bulk User Delete when there is any P2P " +
                "unregistered transaction(s) for which the money is underlying in IND02 wallet even if FAIL_OTF_ON_DEL = TRUE in the system");
        try {
            //Creating Subscriber
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(deleteSubs, true, false);

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);

            //Performing 1st Cash In service
            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5");

            //Performing P2P - Send Money to unknown service
            User nonRegSubs = new User(Constants.SUBSCRIBER);
            String txnId = Transactions.init(t1).p2pNonRegTransaction(deleteSubs, nonRegSubs, new BigDecimal("3")).TransactionId;

            String value = MobiquityGUIQueries.fetchDefaultValueOfPreference("FAIL_OTF_ON_DEL");
            if (!value.equalsIgnoreCase("TRUE")) {
                Login.init(t1).loginAsSuperAdmin(DataFactory.getSuperAdminWithAccess("PREF001"));
                Preferences.init(t1).modifySystemPreferences("FAIL_OTF_ON_DEL", "TRUE");
            }

            UsrBalance preSubBal = MobiquityGUIQueries.getUserBalance(deleteSubs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            BigDecimal preIND02Bal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND02");

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.dbGetTransactionStatus(txnId), Constants.TXN_STATUS_FAIL, "Verify Transaction Status", t1);

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            BigDecimal postIND02Bal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND02");

            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preSubBal.Balance.intValue() + 3);
            Assert.assertEquals(postIND02Bal.intValue(), preIND02Bal.intValue() - 3);

            //Post Delete Customer check
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(nonRegSubs, true, false);
            UsrBalance nonSubBal = MobiquityGUIQueries.getUserBalance(nonRegSubs, GlobalData.defaultWallet.WalletId, null);
            Assert.assertEquals(nonSubBal.Balance.intValue(), 0);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 11, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1907() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1907", "Verify that if channel user & subscriber is having multiple wallets and funds are available in non-primary " +
                "wallet(s), then funds from non-primary wallet(s) should move to the primary wallet of that MFS Provider and then to IND01 of that respective currency");
        try {

            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).initiateAndApproveChUser(deleteWhs, false);

            String[] payIdArr = DataFactory.getPayIdApplicableForChannelUser();
            String[] walletIds = (payIdArr.length > 0) ? payIdArr : new String[]{DataFactory.getDefaultWallet().WalletId};
            for (String walletId : walletIds) {
                SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, walletId, defaultProvider.ProviderId);
                SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
                if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                        Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {
                    Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                            .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
                }
            }

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            SubscriberManagement.isBankRequired = false;
            SubscriberManagement.init(t1).createSubscriber(deleteSubs);

            String[] payIdArr1 = DataFactory.getPayIdApplicableForSubs();
            String[] walletIds1 = (payIdArr1.length > 0) ? payIdArr1 : new String[]{DataFactory.getDefaultWallet().WalletId};
            for (String walletId : walletIds1) {
                Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, "5", walletId);
            }

            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_whs = MessageReader.getMessage("205", null);
            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            String whsStatus = MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, Constants.WHOLESALER);
            DBAssertion.verifyDBAssertionEqual(whsStatus, "N", "Verification of user status", t1);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 12, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1913() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1913", "Verify that system should not allow to delete the channel user through Bulk User Delete who has active child users");
        try {
            User HMER = new User(Constants.HEAD_MERCHANT);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(HMER, false);

            HashMap<String, String> users = new HashMap<>();
            users.put(HMER.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_whs = MessageReader.getMessage("", null);
            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(HMER.MSISDN, expMsg_whs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(HMER.MSISDN, HMER.CategoryCode), "Y", "Verify User Status", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 13, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1914() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1914", "Verify that all the pseudo user & employee under the channel user is deleted when channel user is deleted " +
                "through Bulk User Delete");
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            SfmResponse o2cResponse = Transactions.init(t1).initiateO2C(o2cInitiator, deleteWhs, Constants.minimun_AMOUNT, DataFactory.getDefaultWallet().WalletId,
                    defaultProvider.ProviderId);
            SfmResponse o2cResponse1 = Transactions.init(t1).approveO2C(o2cApprover1, o2cResponse.ServiceRequestId);
            if (!o2cResponse1.verifySuccessMessage("o2c.complete.success",
                    Constants.minimun_AMOUNT, o2cInitiator.LoginId, deleteWhs.MSISDN)) {
                Transactions.init(t1).approveO2C(o2cApprover2, o2cResponse1.ServiceRequestId)
                        .verifyStatus(Constants.TXN_STATUS_SUCCEEDED);
            }
            //Create an employee
            Transactions.init(t1).createEmployee(deleteWhs, "1");

            //Create a pseudo user
            PseudoUser psUser = new PseudoUser();
            psUser.setParentUser(deleteWhs);
            PseudoUserManagement.init(t1).createCompletePseudoUser(psUser);

            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(deleteWhs, GlobalData.defaultWallet.WalletId, null);
            BigDecimal preOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_whs = MessageReader.getMessage("205", null);
            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }

            BigDecimal postOptBal = MobiquityGUIQueries.fetchOperatorBalance(defaultProvider.ProviderId, "IND01");
            Assert.assertEquals(postOptBal.intValue(), preOptBal.intValue() + preWhsBalance.Balance.intValue());

            //status of the employee should be N
            String status = MobiquityGUIQueries.getEmployeeStatus(whsUserId);
            DBAssertion.verifyDBAssertionEqual(status, "N", "Status of employee in mtx_employee is N", t1);

            //Status of the pseudouser should be N
            status = MobiquityGUIQueries.getUserStatus(psUser.MSISDN, psUser.PseudoCategoryCode);
            DBAssertion.verifyDBAssertionEqual(status, "N", "Status of pseudoUser in users is N", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 14, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1903() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1903", "Verify that system should allow to delete the channel user & subscriber through Bulk User Delete even if the " +
                "user has not changed his MPIN/PIN after registration");
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));

            ChannelUserManagement.init(t1).initiateAndApproveChUser(deleteWhs, false);
            CommonUserManagement.init(pNode)
                    .changeFirstTimePassword(deleteWhs);

            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            Transactions.init(t1).subsSelfRegistrationWithPayId(deleteSubs, GlobalData.defaultWallet.WalletId);

            Transactions.init(t1).initiateCashIn(deleteSubs, wholesaler, Constants.minimun_AMOUNT);
            String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
            String whsUserId = MobiquityGUIQueries.getUserID(deleteWhs.MSISDN, Constants.WHOLESALER);

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");
            users.put(deleteSubs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            //verify the log file
            String expMsg_subs = MessageReader.getMessage("205", null);
            String expMsg_whs = MessageReader.getMessage("205", null);

            Map<String, String> userDetails = new LinkedHashMap<>();
            userDetails.put(deleteWhs.MSISDN, expMsg_whs);
            userDetails.put(deleteSubs.MSISDN, expMsg_subs);

            BulkUserDeletion.init(t1).verifyLogs(userDetails);

            List<UserWallets> whsWalletList = MobiquityGUIQueries.getUserDetails(deleteWhs, whsUserId);
            for (UserWallets whsWallet : whsWalletList) {
                Assertion.verifyEqual(whsWallet.partyStatus, "N", "Status in users is N", t1);
                Assertion.verifyEqual(whsWallet.partyAccessStatus, "N", "Status in user_phones is N", t1);
                Assertion.verifyEqual(whsWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(whsWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }
            List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
            for (UserWallets subWallet : subWalletList) {
                Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 15, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1904() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1903", "Verify that system should allow to delete the subscriber through Bulk User Delete even if the subscriber has " +
                "not paid his joining fees");
        try {
            deleteSubs = new User(Constants.SUBSCRIBER, DataFactory.getGradeName(Constants.SILVER_SUBSCRIBER));
            Transactions.init(t1)
                    .selfRegistrationForSubscriberWithKinDetails(deleteSubs);

            Transactions.init(t1)
                    .initiateCashIn(deleteSubs, wholesaler, Constants.minimun_AMOUNT);

            String query = "select ACQUISITION_PAID from mtx_party_access where msisdn='" + deleteSubs.MSISDN + "'";
            String acqPayStatus = MobiquityGUIQueries.executeQueryAndReturnResult(query, "ACQUISITION_PAID");

            if (Constants.STATUS_DELETE.equalsIgnoreCase(acqPayStatus)) {

                String subsUserId = MobiquityGUIQueries.getUserID(deleteSubs.MSISDN, Constants.SUBSCRIBER);
                HashMap<String, String> users = new HashMap<>();
                users.put(deleteSubs.MSISDN, "SUBSCRIBER");

                String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);
                Login.init(t1).login(bulkAdmin);
                BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

                //verify the log file
                String expMsg_subs = MessageReader.getMessage("205", null);

                Map<String, String> userDetails = new LinkedHashMap<>();
                userDetails.put(deleteSubs.MSISDN, expMsg_subs);

                BulkUserDeletion.init(t1).verifyLogs(userDetails);

                List<UserWallets> subWalletList = MobiquityGUIQueries.getUserDetails(deleteSubs, subsUserId);
                for (UserWallets subWallet : subWalletList) {
                    Assertion.verifyEqual(subWallet.partyStatus, "N", "Status in mtx_party is N", t1);
                    Assertion.verifyEqual(subWallet.partyAccessStatus, "N", "Status in mtx_party_access is N", t1);
                    Assertion.verifyEqual(subWallet.walletStatus, "N", "Status in mtx_wallet is N", t1);
                    Assertion.verifyEqual(subWallet.balance.equals(new BigDecimal(0)), true, "Verify Wallet balance is 0", t1);
                }
            } else {
                t1.skip("Skipping this case as JOINING FEES has already been paid. Unable to test this functionality");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 16, groups = {FunctionalTag.BULK_USER_DELETION, FunctionalTag.UNSTABLE})
    public void TC_OG5_1915() throws Exception {
        ExtentTest t1 = pNode.createNode("OG5-1915", "Verify that if there are any transaction(s) that have been initiated by the user, they should be marked as failed" +
                " during channel user & subscriber deletion through Bulk User Delete");
        try {
            deleteWhs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(deleteWhs, false);

            Login.init(t1).login(deleteWhs);

            String txnID = TransactionManagement.init(t1)
                    .inverseC2C(wholesaler, Constants.minimun_AMOUNT);

            String dbTxnIdStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
            DBAssertion.verifyDBAssertionEqual(dbTxnIdStatus, Constants.TXN_STATUS_INITIATED, "Transaction marked as Failed", t1);

            HashMap<String, String> users = new HashMap<>();
            users.put(deleteWhs.MSISDN, "CHANNEL");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).login(bulkAdmin);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            String whsStatus = MobiquityGUIQueries.getUserStatus(deleteWhs.MSISDN, Constants.WHOLESALER);
            DBAssertion.verifyDBAssertionEqual(whsStatus, "N", "Verification of user status", t1);

            dbTxnIdStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
            DBAssertion.verifyDBAssertionEqual(dbTxnIdStatus, "TF", "Transaction marked as Failed", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

}
