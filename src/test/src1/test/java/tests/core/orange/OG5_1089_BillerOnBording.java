package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.features.billerManagement.BillerManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class OG5_1089_BillerOnBording extends TestInit {

    @Test(priority = 1)
    public void Test_01() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(defaultProvider.ProviderName,
                            Constants.BILLER_CATEGORY,
                            BillerAttribute.PROCESS_TYPE_ONLINE,
                            BillerAttribute.SERVICE_LEVEL_ADHOC,
                            BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                            BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE,
                            BillerAttribute.SUB_TYPE_NOT_APPLICABLE);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void Test_02() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_ADHOC, BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                            BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE, BillerAttribute.SUB_TYPE_NOT_APPLICABLE);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4)
    public void Test_04() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void Test_05() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_STANDARD, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6)
    public void Test_6() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7)
    public void Test_07() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8)
    public void Test_08() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9)
    public void Test_09() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_EXACT_PAYMENT, BillerAttribute.SUB_TYPE_NOT_APPLICABLE);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10)
    public void Test_10() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11)
    public void Test_11() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_DELUX, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_UNDER_PAYMENT);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12)
    public void Test_12() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(defaultProvider.ProviderName,
                            Constants.BILLER_CATEGORY,
                            BillerAttribute.PROCESS_TYPE_ONLINE,
                            BillerAttribute.SERVICE_LEVEL_DELUX,
                            BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                            BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE,
                            BillerAttribute.SUB_TYPE_NOT_APPLICABLE);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13)
    public void Test_13() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(defaultProvider.ProviderName,
                            Constants.BILLER_CATEGORY,
                            BillerAttribute.PROCESS_TYPE_ONLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM,
                            BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                            BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE,
                            BillerAttribute.SUB_TYPE_NOT_APPLICABLE);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14)
    public void Test_14() {
        ExtentTest test = pNode.createNode("Biller Creation");
        try {
            BillerManagement.init(test)
                    .getBillerFromAppData(defaultProvider.ProviderName,
                            Constants.BILLER_CATEGORY,
                            BillerAttribute.PROCESS_TYPE_ONLINE,
                            BillerAttribute.SERVICE_LEVEL_STANDARD,
                            BillerAttribute.BILLER_TYPE_BILL_UNAVAILABLE,
                            BillerAttribute.BILL_AMOUNT_NOT_APPLICABLE,
                            BillerAttribute.SUB_TYPE_NOT_APPLICABLE);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }


}
