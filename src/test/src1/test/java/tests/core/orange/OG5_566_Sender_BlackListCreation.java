package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class OG5_566_Sender_BlackListCreation extends TestInit {
    private OperatorUser cceBlackList, naUnBlackList;
    private User subSender1, subReceiver;
    private CurrencyProvider providerOne;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this test");
        try {
            providerOne = DataFactory.getDefaultProvider();
            subSender1 = new User(Constants.SUBSCRIBER);
            subReceiver = new User(Constants.SUBSCRIBER);
            cceBlackList = DataFactory.getOperatorUserWithAccess("PTY_BLKL", Constants.CUSTOMER_CARE_EXE);
            naUnBlackList = DataFactory.getOperatorUserWithAccess("PTY_PUL", Constants.NETWORK_ADMIN);

            SubscriberManagement.init(s1).createDefaultSubscriberUsingAPI(subSender1);
            SubscriberManagement.init(s1).createDefaultSubscriberUsingAPI(subReceiver);

            TransactionManagement.init(s1).makeSureLeafUserHasBalance(subSender1);
            TransactionManagement.init(s1).makeSureLeafUserHasBalance(subReceiver);

            OperatorUser opt = new OperatorUser(Constants.OPERATOR);
            ServiceCharge sCharge = new ServiceCharge(Services.P2P_BLACKLIST_COMMISSION,
                    subSender1, opt, null, null, null, null);

            ServiceCharge sCharge1 = new ServiceCharge(Services.P2P_OPT_COMMISISON,
                    subSender1, opt, null, null, null, null);

            ServiceCharge sCharge2 = new ServiceCharge(Services.P2P,
                    subSender1, subReceiver, null, null, null, null);

            ServiceChargeManagement.init(s1).configureNonFinancialServiceCharge(sCharge);
            ServiceChargeManagement.init(s1).configureNonFinancialServiceCharge(sCharge1);
            TransferRuleManagement.init(s1).configureTransferRule(sCharge2);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {"test"})
    public void test_01() {
        ExtentTest t1 = pNode.createNode("successBlackListCustomerAsSenderP2P",
                "Verify that CCE can successfully black list a customer for Sender P2P");
        try {
            Login.init(t1)
                    .login(cceBlackList);

            t1.info("Blacklist Sender 1");
            BlackListManagement.init(t1)
                    .doBlacklistP2PSender(subReceiver.MSISDN, subSender1.MSISDN, providerOne.ProviderName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(dependsOnMethods = "test_01", groups = {"test"})
    public void test_02() {
        ExtentTest t1 = pNode.createNode("senderCanNotbeBlackListedTwice",
                "Verify that Senderr can not be blacklisted twice");
        try {
            Login.init(t1)
                    .login(cceBlackList);

            ConfigInput.isConfirm = false;
            BlackListManagement.init(t1)
                    .doBlacklistP2PSender(subReceiver.MSISDN, subSender1.MSISDN, providerOne.ProviderName);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(dependsOnMethods = "test_01", groups = {"test"})
    public void test_03() {
        ExtentTest t1 = pNode.createNode("failP2PSendMoneyAsSenderIsBlackListed",
                "Verify that P2P send money failes when Sender is Black listed");
        try {
            ConfigInput.isAssert = false;
            Transactions.init(t1).performP2PSendMoneySubs(subSender1, subReceiver, "10")
                    .verifyMessage("p2p.sender.is.blacklisted")
                    .verifyStatus("01053");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(dependsOnMethods = "test_01", groups = {"test"})
    public void test_04() {
        ExtentTest t1 = pNode.createNode("succfullyUnBarP2PBlackListSender",
                "Verify that CCE user can successfully un blackList a P2P Blacklist as sender");
        try {
            String[] blackListed = new String[]{subSender1.MSISDN};
            Login.init(t1)
                    .login(naUnBlackList);
            BlackListManagement.init(t1)
                    .unBlacklistP2P(subReceiver.MSISDN, providerOne.ProviderName, blackListed);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(dependsOnMethods = "test_01", groups = {"test"})
    public void test_05() {
        ExtentTest t1 = pNode.createNode("successfullyPefromP2PfromListOfUnBlackListedSenders",
                "Verify that Receiver can not receive P2P Send money from the list of Un BlackListed Senders");
        try {
            Transactions.init(t1).performP2PSendMoneySubs(subSender1, subReceiver, "2");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
