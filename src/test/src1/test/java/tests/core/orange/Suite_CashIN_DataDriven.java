package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_CashIN_DataDriven extends TestInit {
    private Object[][] testData;

    @DataProvider(name = "testData")
    public static Object[][] getTestData() throws Exception {
        String filePath = FilePath.dirTestData + "Suite_CashIN_DataDriven.xlsx";
        return ExcelUtil.getTableArray(filePath, 0);
    }

    @Test(priority = 1, dataProvider = "testData", groups = {FunctionalTag.MONEY_SMOKE})
    public void run(String serviceCode, String testId, String testName, String isExecute, String payerCode,
                    String payerStatus, String payeeCode, String payeeStatus, String amount,
                    String expectedStatus, String messageCode, String expError) throws Exception {
        try {
            User payer = null, payee = null;
            ConfigInput.isAssert = true; // make sure negative test is disabled

            if (isExecute.equalsIgnoreCase("yes")) {
                ExtentTest t1 = pNode.createNode(toCamelCase(testName + " Payer " + DataFactory.getCategoryName(payerCode) +
                        " Payee " + DataFactory.getCategoryName(payeeCode)), testId);
                // get the specific payer
                if (payerStatus.equals("")) {
                    payer = DataFactory.getChannelUserWithCategory(payerCode);
                } else if (payerStatus.equalsIgnoreCase("BAR_AS_SENDER")) {
                    payer = CommonUserManagement.init(t1).getBarredUser(payerCode, Constants.BAR_AS_SENDER, null);
                } else if (payerStatus.equalsIgnoreCase("BAR_AS_RECIEVER")) {
                    payer = CommonUserManagement.init(t1).getBarredUser(payerCode, Constants.BAR_AS_RECIEVER, null);
                } else if (payerStatus.equalsIgnoreCase("BAR_AS_BOTH")) {
                    payer = CommonUserManagement.init(t1).getBarredUser(payerCode, Constants.BAR_AS_BOTH, null);
                } else if (payerStatus.equalsIgnoreCase("STATUS_DELETE_INITIATE")) {
                    payer = CommonUserManagement.init(t1).getDeleteInitiatedUser(payerCode, null);
                } else if (payerStatus.equalsIgnoreCase("CHURNED")) {
                    payer = CommonUserManagement.init(t1).getChurnedUser(payerCode, null);
                } else if (payerStatus.equalsIgnoreCase("STATUS_SUSPEND")) {
                    payer = CommonUserManagement.init(t1).getSuspendedUser(payerCode, null);
                }

                // get the specific payee
                if (payeeStatus.equals("")) {
                    payee = DataFactory.getChannelUserWithCategory(payeeCode);
                } else if (payeeStatus.equalsIgnoreCase("BAR_AS_SENDER")) {
                    payer = CommonUserManagement.init(t1).getBarredUser(payeeCode, Constants.BAR_AS_SENDER, null);
                } else if (payeeStatus.equalsIgnoreCase("BAR_AS_RECIEVER")) {
                    payee = CommonUserManagement.init(t1).getBarredUser(payeeCode, Constants.BAR_AS_RECIEVER, null);
                } else if (payeeStatus.equalsIgnoreCase("BAR_AS_BOTH")) {
                    payee = CommonUserManagement.init(t1).getBarredUser(payeeCode, Constants.BAR_AS_BOTH, null);
                } else if (payeeStatus.equalsIgnoreCase("STATUS_DELETE_INITIATE")) {
                    payee = CommonUserManagement.init(t1).getDeleteInitiatedUser(payeeCode, null);
                } else if (payeeStatus.equalsIgnoreCase("CHURNED")) {
                    payee = CommonUserManagement.init(t1).getChurnedUser(payeeCode, null);
                } else if (payeeStatus.equalsIgnoreCase("STATUS_SUSPEND")) {
                    payee = CommonUserManagement.init(t1).getSuspendedUser(payeeCode, null);
                }


                /**
                 * Start Verification
                 */

                if (payer == null || payee == null) {
                    Assertion.failAndStopTest("Failed to fetch Required user from appdata", t1);
                }

                // start validation
                if (serviceCode.equals(Services.CASHIN)) {

                    // make sure that the Payer has sufficient balance
                    if (payerStatus.equals(""))
                        TransactionManagement.init(t1).makeSureChannelUserHasBalance(payer);

                    // Make sure Transfer rule is configured
                    ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, payer, payee, null, null, null, null);
                    TransferRuleManagement.init(t1)
                            .configureTransferRule(sCharge);

                    // check for negative Test
                    if (!payeeStatus.equals("") || !payerStatus.equals(""))
                        startNegativeTest(); // disable default assertion

                    // perform transaction
                    SfmResponse response = Transactions.init(t1)
                            .initiateCashIn(payee, payer, new BigDecimal(amount))
                            .verifyStatus(expectedStatus);

                    // if negative test, verify error message
                    if (!ConfigInput.isAssert) {
                        response.verifyErrorMessage(messageCode);
                        response.verifyErrorCode(expError);
                    }
                }
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

}
