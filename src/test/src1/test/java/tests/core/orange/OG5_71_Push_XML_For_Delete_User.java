package tests.core.orange;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jayway.restassured.response.ValidatableResponse;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.DeleteSubscriber_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.jigsaw.CommonOperations;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

public class OG5_71_Push_XML_For_Delete_User extends TestInit {
    private String stubUrl;
    private User delSubs;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() throws Exception {
        delSubs = DataFactory.getChannelUserWithAccess("SUBSDEL");
        // No generic Implementation was provided by Orange team.
        stubUrl = "http://172.25.25.176:4444/MFS_stub/getUSSDPush/+91"; // todo for any change in country code, update this line
    }

    @Test(groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_01() {
        ExtentTest t1 = pNode.createNode("TEST_01",
                "Verify Push XML is sent to the configured notification," +
                        "for Subscriber user deletion when ACCOUNT_CLOSURE_TWO_STEP is set to 'N'");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "N");

            User subDel01 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(subDel01);

            SubscriberManagement.init(t1)
                    .deleteSubscriber(subDel01, GlobalData.defaultProvider.ProviderId);

            // verify that the Push XML is sent
            String uri = stubUrl + subDel01.MSISDN;
            t1.info("URI: " + uri);
            ValidatableResponse response = CommonOperations.executeStubForGetOperation(uri);
            verifyPushXml(response, subDel01, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_02() {
        ExtentTest t1 = pNode.createNode("TEST_02",
                "Verify Push XML is sent to the configured notification," +
                        "for Channel User user deletion when ACCOUNT_CLOSURE_TWO_STEP is set to 'N'");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "N");

            User whsDel01 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(whsDel01, false);

            ChannelUserManagement.init(t1)
                    .deleteChannelUser(whsDel01);

            // verify that the Push XML is sent
            String uri = stubUrl + whsDel01.MSISDN;
            t1.info("URI: " + uri);
            ValidatableResponse response = CommonOperations.executeStubForGetOperation(uri);
            verifyPushXml(response, whsDel01, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_03() {
        ExtentTest t1 = pNode.createNode("TEST_03",
                "Verify Push XML is sent to the configured notification," +
                        "for Subscriber user deletion when ACCOUNT_CLOSURE_TWO_STEP is set to 'Y'");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "Y");

            User subDel02 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(subDel02);

            Login.init(t1).login(delSubs);
            DeleteSubscriber_Page1 page = DeleteSubscriber_Page1.init(t1);
            page.navSubscriberDeleteInitiate();
            page.selectProviderByValue(GlobalData.defaultProvider.ProviderId);
            page.setMSISDN(subDel02.MSISDN);
            page.confirmSubmit();
            page.confirmDelete();

            String actual = Assertion.getActionMessage();

            String txnid = actual.split("id:")[1].trim();
            t1.info(MarkupHelper.createLabel("Transaction ID :" + txnid, ExtentColor.ORANGE));
            if (Assertion.verifyActionMessageContain("accClosure.label.accsuccess",
                    "Verify Account closure is Initiated Successfully when Account closure is TWO Step", t1,
                    subDel02.FirstName, subDel02.LastName, txnid)) {

                /*
                Verify that Push XML is yet not sent
                 */
                String uri = stubUrl + subDel02.MSISDN;
                t1.info("URI: " + uri);
                String status = CommonOperations.executeStubForGetOperation(uri).extract().jsonPath().getString("status");
                Assertion.verifyEqual(status, "500",
                        "Verify That Push XML is not sent on User deletion initiation, Verify txn STATUS as - ", t1);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test_04() {
        ExtentTest t2 = pNode.createNode("TEST_04",
                "Verify Push XML is not sent on delete initiation when ACCOUNT_CLOSURE_TWO_STEP is set to 'Y'");
        try {

            User subDel03 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t2)
                    .createDefaultSubscriberUsingAPI(subDel03);

            String uri = stubUrl + subDel03.MSISDN;
            t2.info("URI: " + uri);
            SubscriberManagement.init(t2).deleteSubscriber(subDel03, GlobalData.defaultProvider.ProviderId);
            // verify that the Push XML is sent
            ValidatableResponse response = CommonOperations.executeStubForGetOperation(uri);
            verifyPushXml(response, subDel03, t2);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CRITICAL_CASES_TAG})
    public void test05() {
        ExtentTest t1 = pNode.createNode("TEST_05",
                "Verify Push XML is sent to the configured notification," +
                        "for Channel user deletion when ACCOUNT_CLOSURE_TWO_STEP is set to 'Y'");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "Y");

            User whsDel02 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(whsDel02, false);

            ChannelUserManagement.init(t1)
                    .deleteChannelUser(whsDel02);

            // verify that the Push XML is sent
            String uri = stubUrl + whsDel02.MSISDN;
            t1.info("URI: " + uri);
            ValidatableResponse response = CommonOperations.executeStubForGetOperation(uri);
            verifyPushXml(response, whsDel02, t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    private void verifyPushXml(ValidatableResponse res, User deleted, ExtentTest t1) throws IOException {
        /*DocumentBuilderFactory dbf =
                DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(x));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("data");*/
        try {
            String payLoad = res.extract().jsonPath().getString("payload");
            t1.info("Payload: " + payLoad);
            Assertion.verifyEqual(payLoad.contains(deleted.LoginId), true,
                    "Verify PUSH XML is sent and the deleted user's Login ID is available in the Payload", t1);

            Assertion.verifyEqual(payLoad.contains(deleted.MSISDN), true,
                    "Verify that Deleted User's MSISDN is available in the Payload", t1);
        } catch (Exception e) {
            t1.fail("Puhs XML for User deletion is not sent, please re check manually");
        }
    }


}
