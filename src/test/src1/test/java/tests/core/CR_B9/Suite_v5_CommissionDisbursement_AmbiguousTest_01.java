package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Map;

import static framework.util.globalConstant.Constants.TXN_STATUS_SUCCEEDED;

public class Suite_v5_CommissionDisbursement_AmbiguousTest_01 extends TestInit {

    private String amountTx;
    private BigDecimal amountTxLong;
    private OperatorUser NwAdmin1, usrTRuleCreator;
    private User merchant;
    private String usrAccNum;
    private Wallet commWallet;
    private CurrencyProvider providerOne;


    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Throwable {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            BigDecimal minBalance = new BigDecimal("10");
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            providerOne = DataFactory.getDefaultProvider();
            NwAdmin1 = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            usrTRuleCreator = DataFactory.getOperatorUserWithAccess("O2C_TRULES");

            amountTx = "2";
            amountTxLong = new BigDecimal(amountTx);

            //Create ChannelUser
            merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);

            //Login as networkadmin to set transfer limit and define O2C
            Login.init(eSetup).login(usrTRuleCreator);

            //Define Transfer rules //* commenting as below transfer rule is taken care in BaseSetupo
           /* TransferRuleManagement.init(eSetup).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.MERCHANT),
                    DataFactory.getCategoryName(Constants.MERCHANT),
                    Constants.PAYINST_WALLET_CONST, commWallet.WalletName);*/

            //Make sure user has sufficient Balance
            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalanceInWallet(merchant, commWallet.WalletId, minBalance);

            String defaultBankId = DataFactory.getDefaultBankIdForDefaultProvider();

            Map<String, String> accDetails = MobiquityGUIQueries
                    .dbGetAccountDetails(merchant, providerOne.ProviderId, defaultBankId);

            DesEncryptor d = new DesEncryptor();

            usrAccNum = d.decrypt(accDetails.get("ACCOUNT_NO"));
            eSetup.info("USer Account num:" + usrAccNum);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * MON-4088
     * 14. To verify that when transaction is ambiguous admin should be
     * able to download the ambiguous transactions from the UI and in the downloaded file
     * along with transaction ID numeric transaction also should be available in the downloaded file-
     *
     * @throws Throwable
     */

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void test_1() throws Throwable {
        ExtentTest t1 = pNode.createNode("MON-4088_SC01",
                "To verify that when transaction is ambiguous admin should be " +
                        " able to download the ambiguous transactions from the UI and in the downloaded file " +
                        " along with transaction ID numeric transaction also should be available")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            SfmResponse response = Transactions.init(t1)
                    .commissionDisbursement(NwAdmin1, merchant, usrAccNum)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(merchant)
                    .verifyAmbiguousTransactionFileForBank(response);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * MON-4088
     * 15.(a) To verify that when transaction is ambiguous state, admin should be
     * able to download the ambiguous transactions and upload as success
     *
     * @throws Throwable
     */

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void test_2() throws Throwable {
        ExtentTest t1 = pNode.createNode("MON-4088_SC02", "15. To verify that when transaction is ambiguous state, " +
                "admin should be able to download the ambiguous transactions and upload as either success").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            UsrBalance whsPreCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance preBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");


            SfmResponse response = Transactions.init(t1).commissionDisbursement(NwAdmin1, merchant, usrAccNum);
            //Moving transaction from ambiguous to TS

            UsrBalance whsPostCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance postBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");

            AmbiguousTransactionManagement.init(t1)
                    .initiateAmbiguousBalanceValidation(whsPreCommissionBanalnce, preBankFICBalance, whsPostCommissionBanalnce, postBankFICBalance, amountTxLong);

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(merchant)
                    .verifyAmbiguousTransactionFileForBank(response);

            AmbiguousTransactionManagement.init(t1)
                    .setTransactionStatus(response, true)
                    .uploadUpatedAmbiguousTxnFile(response, merchant);

            Thread.sleep(140000); // wait for the Ambiguous Transaction to Get effective and processed

            // Check Balance & Db Status
            AmbiguousTransactionManagement.init(t1)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

            UsrBalance whsPostprocessCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance BankAvailableBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");

            AmbiguousTransactionManagement.init(t1)
                    .initiatePostProcessBalanceValidation(true, whsPreCommissionBanalnce, preBankFICBalance, whsPostprocessCommissionBanalnce, BankAvailableBalance, amountTxLong);

            Utils.closeUntitledWindows();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * MON-4088
     * 15.(b) To verify that when transaction is ambiguous state, admin should be
     * able to download the ambiguous transactions and upload as failure
     *
     * @throws Throwable
     */


    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void test_3() throws Throwable {
        ExtentTest t1 = pNode.createNode("MON-4088_SC03", "15. To verify that when transaction is ambiguous state, " +
                "admin should be able to download the ambiguous transactions and upload as failure");

        try {

            UsrBalance whsPreCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance preBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");

            SfmResponse response = Transactions.init(t1).commissionDisbursement(NwAdmin1, merchant, usrAccNum);

            UsrBalance whsPostCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance postBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");

            AmbiguousTransactionManagement.init(t1)
                    .initiateAmbiguousBalanceValidation(whsPreCommissionBanalnce, preBankFICBalance, whsPostCommissionBanalnce, postBankFICBalance, amountTxLong);
            //Moving transaction from ambiguous to Tf

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(merchant)
                    .verifyAmbiguousTransactionFileForBank(response);

            AmbiguousTransactionManagement.init(t1)
                    .setTransactionStatus(response, false)
                    .uploadUpatedAmbiguousTxnFile(response, merchant);

            Thread.sleep(140000); // wait for the Ambiguous Transaction to Get effective and processed

            AmbiguousTransactionManagement.init(t1)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

            UsrBalance whsPostprocessCommissionBanalnce = MobiquityGUIQueries.getUserBalance(merchant, commWallet.WalletId, null);
            UsrBalance BankAvailableBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");

            AmbiguousTransactionManagement.init(t1)
                    .initiatePostProcessBalanceValidation(false, whsPreCommissionBanalnce, preBankFICBalance, whsPostprocessCommissionBanalnce, BankAvailableBalance, amountTxLong);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
