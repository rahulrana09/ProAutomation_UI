package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : InterfaceID_3315 test cases
 * Author Name      : Shivakumar G/ refactored [rahul rana: sep 26, 2018]
 * Created Date     : 25/07/2018
 */
public class InterfaceID_3315 extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_6276() throws Exception {

        ExtentTest t1 = pNode.createNode("MON-3315",
                "Verify that when a biller is created  biller ID is added to the Interface ID field in DB");
        try {
            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);
            String interfaceId = MobiquityGUIQueries.getInterfaceId(biller);
            Assertion.verifyEqual(biller.BillerCode, interfaceId,
                    "Verify that when a biller is created  biller ID is added to the Interface ID field in DB", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_6277() throws Exception {

        ExtentTest t2 = pNode.createNode("MON-3315",
                "Verify that when a channel user is created  Merchant Code is added to the Interface ID field in DB");
        try {

            //create Channel User
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            String agentCode = MobiquityGUIQueries.getAgentCode(channeluser.MSISDN);
            String interfaceID = MobiquityGUIQueries.getInterfaceId(channeluser);
            Assertion.verifyEqual(agentCode, interfaceID,
                    "Verify that when a channel user is created  Merchant Code is added to the Interface ID field in DB", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void MON_6275() throws Exception {

        ExtentTest t3 = pNode.createNode("MON-3315", "Verify that when a Bank is created Bank ID is added to the Interface ID field in DB");
        try {
            //access A bank
            String bankName = GlobalData.defaultBankName;
            String bankId = DataFactory.getBankId(bankName);
            String interfaceId = MobiquityGUIQueries.getInterfaceId(bankName);

            Assertion.verifyEqual(bankId,
                    interfaceId, "Verify that when a Bank is created Bank ID is added to the Interface ID field in DB", t3);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }

}





