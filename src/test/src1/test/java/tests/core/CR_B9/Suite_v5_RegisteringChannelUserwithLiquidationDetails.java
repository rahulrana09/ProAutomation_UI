package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Liquidation;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.LiquidationDetailsApprovalPage;
import framework.pageObjects.userManagement.LiquidationDetailsConfirmPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Registering ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 27/02/2018
 */

public class Suite_v5_RegisteringChannelUserwithLiquidationDetails extends TestInit {

    private OperatorUser usrCreator, bankApprover;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        try {
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void createChannelUserWithLiquiBank() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4239_1",
                "Verify Operator user should able to register channel user along with Non-partner(liquidation) banking details. ");

        try {
            //Create ChannelUser With Liquidation Bank
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void addLiquidationDetailsShouldBeEnabled() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_2/3",
                "Verify After selecting Add liquidation bank checkbox in Add channel user-->>Linked Bank Page" +
                        "  all below fields should be displayed in UI" +
                        "      MFS Provider (M)" +
                        "      Bank name (Show list of Non-Partner Banks) (M)" +
                        "      Bank account number (M)" +
                        "      Branch Name (M)" +
                        "      Account holder name (M)" +
                        "      Frequency Trading Name (O)" +
                        "      List of Frequency types  ");

        try {

            //Create ChannelUser With Liquidation Bank

            User channeluser = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(channeluser)
                    .assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser)
                    .mapDefaultWalletPreferences(channeluser);

            //Verifying fields are available in Liquidation Coloumn
            Utils.captureScreen(t1);
            WebElement liquidationBank = driver.findElement(By.name("action:confirm2_addMoreLiquidationBank"));

            if (liquidationBank.isDisplayed()) {
                t1.info("Liquidation Field is Displayed");
            } else {
                Assert.fail("Liquidation field is not displayed");
            }

            liquidationBank.click();

            Utils.captureScreen(t1);
            Object[] expected = PageInit.fetchLabelTexts("(//tbody[@id='liquidationDetails']/tr[1]/td/b)[position()<7]");
            Object[] actual = UserFieldProperties.getLabels("liquidation.details");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                t1.info("Expected:" + expected.toString());
                t1.info("Actual:" + actual.toString());
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void createChannelUserForWeeklyFrequency() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_4", "Verify Operator user should able to register channel user along with Non-partner(liquidation) banking details for weekly frequency ");

        try {

            //Create ChannelUser With Liquidation Bank With Weekly Frequency

            User channeluser = new User(Constants.WHOLESALER);
            channeluser.setLiquidationFrequency("Weekly");
            channeluser.setLiquidationDay("Monday");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void createChannelUserForMonthlyFrequency() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_6", "Verify Operator user should able to register channel user along with Non-partner(liquidation) banking details for Monthly frequency ");

        try {

            //Create ChannelUser With Liquidation Bank With Monthly Frequency
            User channeluser = new User(Constants.WHOLESALER);
            channeluser.setLiquidationFrequency("Monthly");
            channeluser.setLiquidationDay("LAST-DAY");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void channelUserWithLiquidationDetailsWithSameMFSProvider() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_8", "Verify each user should be able to set up oneliquidation bank details for each MFS Provider");
        try {

            //Create ChannelUser With Liquidation Bank

            AddChannelUser_pg5 page1 = new AddChannelUser_pg5(t1);
            User channeluser = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser).mapDefaultWalletPreferences(channeluser);

            for (int i = 0; i < 2; i++) {

                page1.liquidationclick();

                WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + i + "]"));
                WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[" + i + "]"));
                WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[" + i + "]"));
                WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[" + i + "]"));

                // UI elements

                new Select(driver.findElement(By.id("liquidationProvider" + i + ""))).selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
                new Select(driver.findElement(By.id("liquidationBankListId" + i + ""))).selectByVisibleText(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
                Liquidation liq = new Liquidation();
                liqaccountNum.sendKeys(liq.LiquidationBankAccountNumber);
                liqbranchnam.sendKeys(liq.LiquidationBankBranchName);
                liqaccountholdernam.sendKeys(liq.AccountHolderName);
                freqtradingnam.sendKeys(liq.FrequencyTradingName);

            }
            page1.selectLiquidationFrequency(channeluser.LiquidationFrequency, channeluser.LiquidationDay);
            page1.clickNext(channeluser.CategoryCode);
            AlertHandle.acceptAlert(t1);
            AlertHandle.acceptAlert(t1);

            Assertion.verifyErrorMessageContain("LiquidationBank.one.permfsprovider", "Only one Bank per mfs provider is allowed", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void liquidationDetailsInAddDeleteModifyApprovalPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_9", "Verify After initiating add channel user with liquidation bank,Liquidation bank details should be populated in Add/modify/delete bank account approval page.");

        try {

            //Create ChannelUser With Liquidation Bank
            User channeluser = new User(Constants.WHOLESALER);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser).mapDefaultWalletPreferences(channeluser);

            CommonUserManagement.init(t1)
                    .mapBankPreferences(channeluser, false);
            CommonUserManagement.init(t1).addInitiatedApproval(channeluser, true);
            //Verifying if Bank Is Available For Channel Admin To Approve

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).approveAssociatedBanksForUser(channeluser, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void channelAdminRejectsBankOnApproveBankPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_10", " Verify Channel Admin able to reject the liquidation Bank account");

        try {

            //Create ChannelUser With Liquidation Bank

            User channeluser = new User(Constants.WHOLESALER);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser).mapDefaultWalletPreferences(channeluser);

            CommonUserManagement.init(t1)
                    .mapBankPreferences(channeluser, false);
            CommonUserManagement.init(t1).addInitiatedApproval(channeluser, true);
            //Verifying if Bank Is Available For Channel Admin To Approve

            Login.init(t1).login(bankApprover);
            //Channel Admin Rejects Bank
            CommonUserManagement.init(t1).rejectAssociatedBanksForUser(channeluser, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String status = MobiquityGUIQueries.getStatusOfLiquidationBank(channeluser.MSISDN);
            String status1 = MobiquityGUIQueries.getBankAccountStatusWithLoginId(channeluser.LoginId);
            DBAssertion.verifyDBAssertionEqual(status, "N", "Verify Bank Status", t1);
            DBAssertion.verifyDBAssertionEqual(status1, "N", "Verify Bank Status", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void liquidationDetailsInConfirmAndApprovalPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4239_11/12", " Verify Liquidation bank details should be populated in Confirm page and Approval page. ");

        try {
            //Create ChannelUser With Liquidation Bank

            User channeluser = new User(Constants.WHOLESALER);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);
            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser);

            CommonUserManagement.init(t1)
                    .mapDefaultWalletPreferences(channeluser);
            AddChannelUser_pg5 page5 = AddChannelUser_pg5.init(t1);
            page5.addBankPreferences(channeluser)
                    .clickNext(channeluser.CategoryCode);

            String liquidationBankAccountNumber = channeluser.getLiquidationBankAccountNumber();
            String liquidationBankBranchName = channeluser.getLiquidationBankBranchName();
            String accountHolderName = channeluser.getAccountHolderName();
            String frequencyTradingName = channeluser.getFrequencyTradingName();

            //Verify Details In Confirm Page

            LiquidationDetailsConfirmPage liqCon = new LiquidationDetailsConfirmPage(t1);
            liqCon.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liquidationBankAccountNumber, liquidationBankBranchName, accountHolderName, frequencyTradingName);
            page5.completeUserCreation(channeluser, false);

            //Verify Details In Approval Page

            Login.init(t1).login(usrCreator);
            startNegativeTestWithoutConfirm();
            CommonUserManagement.init(t1).addInitiatedApproval(channeluser, true);
            LiquidationDetailsApprovalPage liqApp = new LiquidationDetailsApprovalPage(t1);
            liqApp.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liquidationBankAccountNumber, liquidationBankBranchName, accountHolderName, frequencyTradingName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
