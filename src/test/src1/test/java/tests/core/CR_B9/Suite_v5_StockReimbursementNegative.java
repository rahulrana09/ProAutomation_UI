package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.stockManagement.Reimbursement_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_StockReimbursement_NetworkSuspend
 * Author Name      : PushpaLatha
 * Created Date     : 24/07/2018
 */
public class Suite_v5_StockReimbursementNegative extends TestInit {

    private OperatorUser barredOptUsr, optModifyUser, optInitReimbursement;
    private String salaryWalletId;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific to this test");
        try {
            salaryWalletId = DataFactory.getWalletUsingAutomationCode(Wallets.SALARY).WalletId;
            optInitReimbursement = DataFactory.getOperatorUserWithAccess("STOCK_REINIT", Constants.NETWORK_ADMIN);
            optModifyUser = DataFactory.getOperatorUserWithAccess("PTY_MCU", Constants.NETWORK_ADMIN);
            barredOptUsr = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    // de-scoped in 5.0 Barred user cannot login
    @Test(priority = 1, groups = {FunctionalTag.v5_0}, enabled = false)
    public void NetworkAdmin_Barred() throws Exception {

        ExtentTest t1 = pNode.createNode("SC16",
                "Verify that network admin is not able to initiate reimbursement service for any user when network admin is barred in the system");

        try {
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, barredOptUsr, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            Login.init(t1).login(barredOptUsr);
            startNegativeTest();
            StockManagement.init(t1)
                    .expectErrorB4Confirm()
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(6), "5", "reimbursement", null, false);

            Assertion.verifyErrorMessageContain("transactor.barred.as.sender.and.receiver", "Barred User not able to Initiate", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void Subscriber_Barred_Sender() throws Exception {

        ExtentTest t1 = pNode.createNode("SC19",
                "Verify that network admin is not able to initiate reimbursement service when sender (subscriber/ channel/ biller) is barred as sender in the system");
        try {

            User subs = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_SENDER, null);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, subs, optInitReimbursement, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            startNegativeTest();
            StockManagement.init(t1).expectErrorB4Confirm().initiateStockReimbursement(subs, DataFactory.getRandomNumberAsString(3), "5", "test", null);

            Thread.sleep(1000);
            Assertion.verifyErrorMessageContain("payer.barred.as.sender", "Payer barred as sender not able to Initiate", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void ChannelUser_Suspended() throws Exception {

        ExtentTest t1 = pNode.createNode("SC25",
                "Verify that network admin is not able to initiate reimbursement service when sender (channel user) is suspended in the system");
        try {
            User rt = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, rt, optInitReimbursement, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            startNegativeTestWithoutConfirm();
            Login.init(t1).login(optInitReimbursement);
            StockManagement.init(t1).initiateStockReimbursement(rt, DataFactory.getRandomNumberAsString(3), "5", "test", null);

            Thread.sleep(1000);
            Assertion.verifyErrorMessageContain("o2c.error.noUser", "Suspended User not able to Initiate", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0, FunctionalTag.DESCOPED_5dot0})
    public void ChannelUser_SVA_Delete() throws Exception {

        ExtentTest t1 = pNode.createNode("SC26",
                "Verify that network admin is not able to initiate reimbursement service when sender’s (subscriber) SVA account is delete initiated in the system");

        try {

            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs, DataFactory.getWalletUsingAutomationCode(Wallets.SALARY).WalletId);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, optInitReimbursement, Constants.SALARY_WALLET, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            Login.init(t1).login(optModifyUser);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Deleted", Constants.SALARY_WALLET);

            startNegativeTest();
            StockManagement.init(t1).expectErrorB4Confirm()
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(3), "5", "test", Constants.SALARY_WALLET);
            Thread.sleep(1000);
            Assertion.verifyErrorMessageContain("reversal.sender.account.delete.initiated", "Sender account delete initiate not able to Initiate", t1);

            stopNegativeTest();
            Login.init(t1).login(optModifyUser);
            ChannelUserManagement.init(t1).approveChannelUserModification(whs);

            Reimbursement_page1 reimbursement_page1 = new Reimbursement_page1(t1);
            reimbursement_page1.navigateToLink();
            reimbursement_page1.type_SelectValue("CHANNEL");
            reimbursement_page1.msisdn_SetText(whs.MSISDN);
            Thread.sleep(2000);
            //reimbursement_page1.provider_SelectIndex();
            reimbursement_page1.wallet_CheckValue("SALARY");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void ChannelUser_SVA_Suspend() throws Exception {

        ExtentTest t1 = pNode.createNode("SC27",
                "Verify that network admin is not able to initiate reimbursement service when sender’s (Channel User) SVA account is suspended in the system");

        try {
            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            //Changed savings wallet to salary wallet,todo:need to handle wallet based on configInput
            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs, salaryWalletId);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, optInitReimbursement, salaryWalletId, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            Login.init(t1).login(optModifyUser);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Suspended", Constants.SALARY_WALLET);
            ChannelUserManagement.init(t1).approveChannelUserModification(whs);

            startNegativeTest();
            StockManagement.init(t1).expectErrorB4Confirm()
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(3), "5", "test", salaryWalletId);

            Thread.sleep(1000);
            Assertion.verifyErrorMessageContain("reversal.sender.account.suspended", "SVA suspend not able to Initiate", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
