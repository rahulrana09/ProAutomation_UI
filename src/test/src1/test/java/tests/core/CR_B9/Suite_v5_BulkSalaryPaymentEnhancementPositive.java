package tests.core.CR_B9;


import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Bulk Salary Payment Enhancement (Positive)User Story MON-3982
 * Author Name      : Jyoti Katiyar
 * Created Date     : 12/03/2018
 */

public class Suite_v5_BulkSalaryPaymentEnhancementPositive extends TestInit {
    private User entUser, sub;
    private OperatorUser bpAdmin;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            bpAdmin = DataFactory.getOperatorUserWithCategory(Constants.BULK_PAYER_ADMIN);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            TransactionManagement.init(s1)
                    .makeSureChannelUserHasBalance(entUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }

    }


    /*To verify that Enterprise user registered with BP mode as registered user should be able to perform
    bulk payment for registered user also added as bulk payee by entering first name and last name and national Id
    different for case (upper or lower) which was entered during the on-boarding of the user*/

    @Test(enabled = true, groups = {FunctionalTag.v5_0, FunctionalTag.SYSTEM_TEST})
    public void Enterpisepaymentwithdifferentcaseupperorlower_4271() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4271", "To verify that Enterprise user registered with BP mode as registered user should be able to perform\n" +
                "    bulk payment for registered user also added as bulk payee by entering first name and last name and national Id");
        List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
        try {
            //Login as Enterprise
            Login.init(t1)
                    .login(entUser);

            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1",
                    "",
                    "",
                    sub.ExternalCode));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*To verify that Enterprise user registered with BP mode as unregistered should be able to perform
    bulk payment for registered user by entering first name and last name*/

    @Test(enabled = true, groups = {FunctionalTag.v5_0})
    public void BulkEnterprisePaymentWithFirstNameAndLastName_4270() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4270", "Bulk Enterprise payment with first name and last name");

        List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
        try {
            //Login as Enterprise
            Login.init(t1)
                    .login(entUser);

            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1",
                    sub.FirstName,
                    sub.LastName,
                    ""));
            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, true, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
