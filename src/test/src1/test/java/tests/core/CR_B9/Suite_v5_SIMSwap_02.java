package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : SIM Swap / Replacement
 * Author Name      : Nirupama MK
 * Created Date     : 14/06/2018
 */
public class Suite_v5_SIMSwap_02 extends TestInit {

    User subscriber, subscriber1;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Sim Swap");
        try {
            subscriber = new User(Constants.SUBSCRIBER);

            CurrencyProviderMapping.init(eSetup)
                    .mapPrimaryWalletPreference(subscriber);

            SystemPreferenceManagement.init(eSetup).updateSystemPreference
                    ("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /* MON_6138
     create user who is both channel and subscriber
     Verify that the value of SIM_SWAPPED is 'y' and SIM_SWAPPED_ON is 'current date'after the sim swap
     */
    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_6138() throws Exception {

        ExtentTest MON_6138 = pNode.createNode("MON-6138", "sim swapped status and sim swapped_on for " +
                "channel user and subscriber is updated after the sim swap, when user is both channel user and subscriber");

        try {

            //create the user who is both channel user and subscriber
            User chnUser = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            if (!(MobiquityGUIQueries.checkMsisdnExistInSubscribers(chnUser.MSISDN))) {
                subscriber.setMSISDN(chnUser.MSISDN);
            } else {
                chnUser = new User(Constants.RETAILER);
                ChannelUserManagement.init(MON_6138).createChannelUserDefaultMapping(chnUser, false);
                subscriber.setMSISDN(chnUser.MSISDN);
            }
            SubscriberManagement.init(MON_6138).
                    createDefaultSubscriberUsingAPI(subscriber);

            //sim status for Channel User before sim swap
            Map<String, String> chUser_simStatus = MobiquityGUIQueries
                    .fetchSIMSwappedStatus(chnUser, chnUser.MSISDN);

            String sim_Swapped = chUser_simStatus.get("SIM_Swapped");
            String sim_Swapped_On = chUser_simStatus.get("SIM_SwappedOn");

            Assertion.verifyEqual((sim_Swapped == null) && (sim_Swapped_On == null), true,
                    "Verify for Channel User that Initially 'Sim_Swapped' and 'Sim_Swapped_ON' are Not Set", MON_6138);

            //sim status for Subscriber before sim swap
            Map<String, String> sub_simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(subscriber, subscriber.MSISDN);

            String sim_Swapped1 = sub_simStatus.get("SIM_Swapped");
            String sim_Swapped_On1 = sub_simStatus.get("SIM_SwappedOn");

            Assertion.verifyEqual((sim_Swapped1 == null) && (sim_Swapped_On1 == null), true,
                    "Verify for Subscriber that Initially 'Sim_Swapped' and 'Sim_Swapped_ON' are Not Set", MON_6138);

            //perform sim swap; both channel user and subscriber is reflected
            Transactions.init(MON_6138)
                    .SIMSwap(chnUser.MSISDN)
                    .verifyStatus("200");

            String currentDate = new DateAndTime().getCurrentDate();

            //sim status for Subscriber after sim swap
            sub_simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(subscriber, subscriber.MSISDN);

            sim_Swapped1 = sub_simStatus.get("SIM_Swapped");
            String[] simSwapped_On1 = sub_simStatus.get("SIM_SwappedOn").split(" ");

            Assertion.verifyEqual((sim_Swapped1.equalsIgnoreCase("Y")) && (currentDate.equals(simSwapped_On1[0])), true,
                    "Verify for subscriber after SIM Swap the Values for 'Sim_Swapped' is:" + sim_Swapped1 + ". 'Sim_Swapped_ON':" + simSwapped_On1[0], MON_6138);

            //sim status for channel user after sim swap
            chUser_simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(chnUser, chnUser.MSISDN);

            sim_Swapped = chUser_simStatus.get("SIM_Swapped");
            String[] simSwapped_On = chUser_simStatus.get("SIM_SwappedOn").split(" ");

            Assertion.verifyEqual((sim_Swapped.equalsIgnoreCase("Y")) && (currentDate.equals(simSwapped_On[0])), true,
                    "Verify for channel user, after SIM Swap the Values for 'Sim_Swapped' is:" + sim_Swapped + ". 'Sim_Swapped_ON':" + simSwapped_On[0], MON_6138);

        } catch (Exception e) {
            markTestAsFailure(e, MON_6138);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_6250() throws Exception {

        ExtentTest MON_6250 = pNode.createNode("MON-6250", "Unsuccessful SIMSwap when " +
                "invalid msisdn provided, verify proper error message ");
        try {

            Transactions.init(MON_6250)
                    .SIMSwap("invalid")
                    .verifyStatus("00066")
                    .assertMessage("userenq.user.error");


        } catch (Exception e) {
            markTestAsFailure(e, MON_6250);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void MON_4832() throws Exception {
        /*
            MON_5981
            Create a Channel User
            Verify that the initial value is NULL (Before Swapping the SIM)
            Verify that the sim swap is successfull
            fetch the value of SIM_SWAPPED is 'y' and SIM_SWAPPED_ON is 'current date'after the sim swap

            MON_5982
            login as NwAdmin
            navi gate Enquiries > Customer care Executive > reset Credentials
            Verify the error msg whiling trying to reset pin

        */

        ExtentTest MON_5981 = pNode.createNode("MON-5981", " verify Sim swap is successfull " +
                "for both channel user and subscriber ");

        try {

            subscriber1 = new User(Constants.SUBSCRIBER);

            Transactions.init(MON_5981)
                    .selfRegistrationForSubscriberWithKinDetails(subscriber1)
                    .assertStatus(Constants.TXN_SUCCESS);

            Map<String, String> simStatus = MobiquityGUIQueries.fetchSIMSwappedStatus(subscriber1, subscriber1.MSISDN);

            String Sim_Swapped = simStatus.get("SIM_Swapped");
            String Sim_Swapped_On = simStatus.get("SIM_SwappedOn");

            if ((Sim_Swapped == null) && (Sim_Swapped_On == null)) {

                MON_5981.info("'Sim_Swapped' and 'Sim_Swapped_ON' holds Null value initially");
                MON_5981.pass("SUCCESS");

            } else {
                MON_5981.fail("'Sim_Swapped' and 'Sim_Swapped_ON' is not Null initially");
            }

            Transactions.init(MON_5981)
                    .SIMSwap(subscriber1.MSISDN)
                    .verifyStatus("200")
                    .verifyMessage("SUCCESS");

            SystemPreferenceManagement.init(MON_5981).updateSystemPreference
                    ("CHANNEL_USER_AS_SUBS_ALLOWED", "N");

            ExtentTest MON_5982 = pNode.createNode("MON-5982", "  verify the error message " +
                    "while trying to reset the pin through Enquiries > Customer Care Executive ");

            OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);
            Login.init(MON_5982).login(netAdmin);

            Enquiries.init(MON_5982)
                    .startNegativeTest()
                    .resetCredentials(Constants.SUBSCRIBER_REIMB,
                            Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, subscriber1.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5981);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

}