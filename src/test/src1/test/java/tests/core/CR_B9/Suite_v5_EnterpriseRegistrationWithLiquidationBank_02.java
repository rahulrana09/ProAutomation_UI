package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.userManagement.AddEnterprise_Pg3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Enterprise Registration With Liquidation Details
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 21/03/2018
 */

public class Suite_v5_EnterpriseRegistrationWithLiquidationBank_02 extends TestInit {
    private OperatorUser usrCreator, bankApprover;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
    }

    //This is part of generic Enterprise Creation Method, Bank Mapping
    @Test(priority = 2, groups = {FunctionalTag.v5_0}, enabled = false)
    public void addLiquidationFieldShouldBeEnabled() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_2", "Verify Add liquidation bank checkbox should be populated in Add channel user-->>Linked Bank Page  ");

        try {
            //Create ChannelUser With Liquidation Bank

            User channeluser = new User(Constants.ENTERPRISE);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser);

            ChannelUserManagement.init(t1).
                    setEnterprisePreference().setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1).mapDefaultWalletPreferences(channeluser);

            AddEnterprise_Pg3 page = AddEnterprise_Pg3.init(t1);

            page.setEnterpriseLimit2(channeluser.EnterpriseLimit);
            page.clickOnBulkRegistrationIsRequired();
            page.selectBulkPayerType();

            page.setCompCode(DataFactory.getRandomNumberAsString(5)); // company code should be
            page.clickNext();

            //Verifying fields are available in Liquidation Coloumn

            WebElement liquidationBank = driver.findElement(By.name("action:confirm2_addMoreLiquidationBank"));

            if (liquidationBank.isDisplayed()) {
                t1.info("Liquidation Field is Displayed");
            } else {
                Assert.fail("Liquidation field is not displayed");
            }

            liquidationBank.click();

            Object[] expected = PageInit.fetchLabelTexts("(//tbody[@id='liquidationDetails']/tr[1]/td/b)[position()<7]");
            Object[] actual = UserFieldProperties.getLabels("liquidation.details");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    //This is part of generic Enterprise Creation Method, Bank approval
    @Test(priority = 1, groups = {FunctionalTag.v5_0}, enabled = false)
    public void liquidationDetailsInAddDeleteModifyApprovalPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4780_7",
                "Verify After initiating add channel user with liquidation bank," +
                        "Liquidation bank details should be populated in Add/modify/delete bank account approval page.");

        try {

            //Create ChannelUser With Liquidation Bank
            User channeluser = new User(Constants.ENTERPRISE);

            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(channeluser).assignHierarchy(channeluser);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(channeluser);

            ChannelUserManagement.init(t1).
                    setEnterprisePreference().setEnterpriseCategoryWalletCombination();

            CommonUserManagement.init(t1).mapDefaultWalletPreferences(channeluser);

            ChannelUserManagement.init(t1).setEnterprisePreference(channeluser, true);
            CommonUserManagement.init(t1).addInitiatedApproval(channeluser, true);
            //Verifying if Bank Is Available For Channel Admin To Approve

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).approveAssociatedBanksForUser(channeluser, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
