package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Withdrawal_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Commission Management(Negative)User Story MON-4441
 * Author Name      : Jyoti Katiyar
 * Created Date     : 20/03/2018
 */

public class Suite_v5_CommissionManagementNegative_01 extends TestInit {

    private User wholesaler;
    private OperatorUser OptUser2, networkAdmin;


    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Get Required Base Set Users.  " +
                "Creating Wholesaler having money in his Commission Wallet. " +
                "Defining transfer rule and perform O2C");
        try {

            OptUser2 = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            networkAdmin = DataFactory.getOperatorUserWithAccess("COMMDISINITIATE");

            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Login as networkadmin to set transfer limit and define O2C
            Login.init(eSetup).login(OptUser2);

            //Define Transfer rules
            TransferRuleManagement.init(eSetup).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    wholesaler.DomainName,
                    wholesaler.CategoryName,
                    Constants.PAYINST_WALLET_CONST,
                    DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName);

            //Perform O2C to commission wallet
            String txn = TransactionManagement.init(eSetup)
                    .initiateO2CForSpecificWallet(wholesaler,
                            DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName,
                            DataFactory.getDefaultProvider().ProviderId,
                            Constants.MIN_O2C_AMOUNT, "5646");
            TransactionManagement.init(eSetup).o2cApproval1(txn);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*1. To verify that appropriate error message is displayed when none of the users
     are selected and clicked on " Export for Commission Withdrawal" button*/

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void UsersAreNotSelectedAndClickedOnExportForCommissionWithdrawalButton_4943() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4943", "To verify that " +
                "appropriate error message is displayed when none of the users are " +
                "selected and clicked on \" Export for Commission Withdrawal\" button");
        try {
            //login with network admin
            Login.init(t1).login(networkAdmin);

            //Navigate to Commission Management Page 1

            Commission_Withdrawal_Page1.init(t1).NavigateToLink().SelectProvider()
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .ClickOnSubmit();

            //Download the Commission Withdrawal CSV file without selecting any user
            Commission_Withdrawal_Page2.init(t1).downloadCommissionWithdrawal().getCommissionWithdrawalFile();
            String text = driver.switchTo().alert().getText();
            Assertion.verifyEqual(text, "select atleast one row to export", "Error Message verified when none of the user is selected", t1);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

}
