package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.RechargeOperator;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_StockReimbursement_NetworkSuspend
 * Author Name      : PushpaLatha
 * Created Date     : 24/07/2018
 */
public class Suite_v5_StockReimbursementPositive_01 extends TestInit {

    private User subs, whs, rechargeReceiver, enterp, mer;
    private OperatorUser app, opt, init;
    private Biller biller;
    private RechargeOperator RCoperator;
    private ArrayList<String> errors = new ArrayList<String>();
    private CurrencyProvider providerOne;
    private ExtentTest eSetup;


    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        eSetup = pNode.createNode("Setup", "StockReimbursement");

        try {
            providerOne = DataFactory.getDefaultProvider();
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("CURRENCY_FACTOR");

            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_BANK_REQUIRED");

            if (pref.equalsIgnoreCase("TRUE")) {
                SystemPreferenceManagement.init(eSetup).updateSystemPreference("IS_BANK_REQUIRED", "FALSE");
            }

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            init = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
            app = DataFactory.getOperatorUserWithAccess("STK_REAP");
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            RCoperator = OperatorUserManagement.init(eSetup).getRechargeOperator(providerOne.ProviderId);
            enterp = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            mer = DataFactory.getChannelUserWithCategory(Constants.HEAD_MERCHANT);

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(enterp)
                    .makeSureChannelUserHasBalance(mer)
                    .makeSureChannelUserHasBalance(whs)
                    .makeSureLeafUserHasBalance(subs);

            //Creating Transfer Rule
            rechargeReceiver = new User(Constants.ZEBRA_MER);
            ServiceCharge recharge = new ServiceCharge(Services.SELF_RECHARGE, whs, rechargeReceiver, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(recharge);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, subs, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(obj);

            ServiceCharge obj1 = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(obj1);

            ServiceCharge obj3 = new ServiceCharge(Services.OPERATOR_WITHDRAW, rechargeReceiver, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(obj3);

            ServiceCharge enterW = new ServiceCharge(Services.OPERATOR_WITHDRAW, enterp, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(enterW);

            ServiceCharge merW = new ServiceCharge(Services.OPERATOR_WITHDRAW, mer, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(merW);

            ServiceCharge ind03 = new ServiceCharge(Services.OPERATOR_WITHDRAW, opt, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(ind03);

            biller = BillerManagement.init(eSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            String accNo = DataFactory.getRandomNumberAsString(5);
            ServiceCharge adhocBill = new ServiceCharge(Services.AgentAssistedBillPayment, whs, biller, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(adhocBill);
            Transactions.init(eSetup).initiateOfflineBillPaymentForAdhoc(biller, whs, "100", accNo);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void verifyThatNetworkAdminIsAbleToInitiateReimbursementFromSubscribersNormalWallet() throws Exception {
        ExtentTest t1 = pNode.createNode("SC01",
                "Verify that network admin is able to initiate reimbursement from Subscriber’s Normal wallet");

        try {
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalance = preSubsBalance.Balance.intValue();
            int prefrozenbalance = preSubsBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance(DataFactory.getDefaultProvider().ProviderId, "IND01");

            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(subs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(3), "5", "test", Constants.NORMAL_WALLET);

            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInit = subsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = subsBalanceAfterInit.frozenBalance.intValue();

            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Thread.sleep(1000);
            Assert.assertEquals(balanceAfterInit, preBalance);

            OperatorUser app = DataFactory.getOperatorUserWithAccess("STK_REAP");
            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postBalance = postSubsBalance.Balance.intValue();
            int postFrozenbalance = postSubsBalance.frozenBalance.intValue();

            Thread.sleep(1000);
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Thread.sleep(1000);
            Assert.assertEquals(postBalance, balanceAfterInit - 5);
            t1.info("Expected: " + (balanceAfterInit - 5) + " Actual: " + postBalance);
            Thread.sleep(1000);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 5);
            t1.info("Expected: " + (preOperatorBalance.intValue() + 5 + " Actual: " + postOperatorBalance));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToInitiateReimbursementFromRetailersNormalWallet() throws Exception {
        ExtentTest t1 = pNode.createNode("SC07",
                "Verify that network admin is able to initiate reimbursement from retailer’s Normal wallet");

        try {
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int preBalance = preWhsBalance.Balance.intValue();
            int prefrozenbalance = preWhsBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(2), "5", "test", Constants.NORMAL_WALLET);

            UsrBalance whsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int balanceAfterInit = whsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = whsBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);

            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int postBalance = postWhsBalance.Balance.intValue();
            int postFrozenbalance = postWhsBalance.frozenBalance.intValue();

            Thread.sleep(2000);
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postBalance, balanceAfterInit - 5);
            t1.info("Expected: " + (balanceAfterInit - 5) + " Actual: " + postBalance);
            Thread.sleep(2000);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 5);
            t1.info("Expected: " + (preOperatorBalance.intValue() + 5 + " Actual: " + postOperatorBalance));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToInitiateReimbursementFromSystemWalletIND03() throws Exception {
        ExtentTest t1 = pNode.createNode("SC03",
                "Verify that network admin is able to initiate reimbursement from system wallet IND03");
        try {

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, opt, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            BigDecimal preBalanceOfIND01 = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");
            t1.info("Pre-Balance of IND01 is: " + preBalanceOfIND01);

            BigDecimal preBalanceOfIND03 = MobiquityGUIQueries.fetchOperatorBalance("101", "IND03");
            t1.info("Pre-Balance of IND03 is: " + preBalanceOfIND03);

            Login.init(t1).login(init);
            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(opt, DataFactory.getRandomNumberAsString(2) + DataFactory.getRandomString(3), "2", "test", null);
            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            BigDecimal postBalanceOfIND01 = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");
            t1.info("Pre-Balance of IND01 is: " + postBalanceOfIND01);
            Thread.sleep(2000);
            BigDecimal postBalanceOfIND03 = MobiquityGUIQueries.fetchOperatorBalance("101", "IND03");
            t1.info("Pre-Balance of IND03 is: " + postBalanceOfIND03);

            Assert.assertTrue(postBalanceOfIND03.intValue() == preBalanceOfIND03.intValue() - 2);
            t1.info("Expected: " + (preBalanceOfIND03.intValue() - 2) + " Actual: " + postBalanceOfIND03.intValue());
            Thread.sleep(2000);
            Assert.assertTrue(postBalanceOfIND01.intValue() == preBalanceOfIND01.intValue() + 2);
            t1.info("Expected: " + (preBalanceOfIND01.intValue() + 2) + " Actual: " + postBalanceOfIND01.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToInitiateReimbursementFromBillerNormalWallet() throws Exception {
        ExtentTest t1 = pNode.createNode("SC07",
                "Verify that network admin is able to initiate reimbursement from Biller");
        try {

            ServiceCharge obj2 = new ServiceCharge(Services.OPERATOR_WITHDRAW, biller, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj2);

            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int preBalance = preBillerBalance.Balance.intValue();
            int prefrozenbalance = preBillerBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, biller, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(biller, DataFactory.getRandomNumberAsString(1) + DataFactory.getRandomString(3), "5", "test", null);

            Thread.sleep(2000);
            UsrBalance billerBalanceAfterInit = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int balanceAfterInit = billerBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = billerBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);

            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int postBalance = postBillerBalance.Balance.intValue();
            int postFrozenbalance = postBillerBalance.frozenBalance.intValue();

            Thread.sleep(2000);
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postBalance, balanceAfterInit - 5);
            t1.info("Expected: " + (balanceAfterInit - 5) + " Actual: " + postBalance);
            Thread.sleep(2000);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 5);
            t1.info("Expected: " + (preOperatorBalance.intValue() + 5 + " Actual: " + postOperatorBalance));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToInitiateReimbursementFromRechargeOperatorNormalWallet() throws Exception {
        ExtentTest t1 = pNode.createNode("SC07",
                "Verify that network admin is able to initiate reimbursement from recharge operator");

        try {

            //Performing recharge
            SfmResponse response = Transactions.init(t1).rechargeSelf(whs, RCoperator.OperatorId, "20", Constants.NORMAL_WALLET);

            //Moving transaction from ambiguous to TS
            Transactions.init(t1).performEIGTransaction(response.ServiceRequestId, "true", Services.SELF_RECHARGE_RESUME);

            UsrBalance preRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int preBalance = preRCOptBalance.Balance.intValue();
            int prefrozenbalance = preRCOptBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, rechargeReceiver, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(RCoperator, DataFactory.getRandomNumberAsString(3) + DataFactory.getRandomString(1), "5", "test", null);

            Thread.sleep(2000);
            UsrBalance rcOptBalanceAfterInit = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int balanceAfterInit = rcOptBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = rcOptBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);

            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int postBalance = postRCOptBalance.Balance.intValue();
            int postFrozenbalance = postRCOptBalance.frozenBalance.intValue();

            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Assert.assertEquals(postBalance, balanceAfterInit - 5);
            t1.info("Expected: " + (balanceAfterInit - 5) + " Actual: " + postBalance);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 5);
            t1.info("Expected: " + (preOperatorBalance.intValue() + 5 + " Actual: " + postOperatorBalance));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void verifyThatCommissionPolicyIsNotApplicableForReimbursementService() throws Exception {
        ExtentTest t1 = pNode.createNode("SC14",
                "Verify that commission policy is not applicable for reimbursement service");
        try {
            //Check for Commision Page
            boolean sValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("COMMISSION", "STOCK REIMBURSEMENT");
            Assert.assertTrue(sValue == false);
            t1.info("Stock Reimbursement is not displayed in Commission page");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void verifyThatNetworkAdminIsAbleToRejectTheInitiatedReimbursementRequest() throws Exception {
        ExtentTest t1 = pNode.createNode("SC13",
                "Verify that network admin is able to reject the initiated reimbursement request");
        try {
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int preBalance = preWhsBalance.Balance.intValue();
            int prefrozenbalance = preWhsBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            Login.init(t1).login(init);
            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(2) + DataFactory.getRandomString(1), "5", "test", null);

            UsrBalance whsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int balanceAfterInit = whsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = whsBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);

            Login.init(t1).login(app);
            StockManagement.init(t1).rejectReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            int postBalance = postWhsBalance.Balance.intValue();
            int postFrozenbalance = postWhsBalance.frozenBalance.intValue();

            Thread.sleep(1000);
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue());
            t1.info("Expected: " + preOperatorBalance.intValue() + " Actual: " + postOperatorBalance);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void verifyThatReimbursementIsSuccessfulFromSenderSVAWhenHeIsRegisteredInTheSystemBothAsChannelAndSubscriber() throws Exception {
        ExtentTest t1 = pNode.createNode("SC45",
                "Verify that reimbursement transaction is successful from the sender’s SVA account when he is " +
                        "registered in the system both as channel and subscriber");
        try {

            MobiquityGUIQueries m = new MobiquityGUIQueries();

            String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("CHANNEL_USER_AS_SUBS_ALLOWED");

            if (pref.equalsIgnoreCase("N")) {
                SystemPreferenceManagement.init(eSetup).updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");
            }

            User channel = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channel, false);

            User subs = new User(Constants.SUBSCRIBER);
            subs.setMSISDN(channel.MSISDN);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            Thread.sleep(1000);
            Transactions.init(t1).initiateCashInWithRole(subs, whs, "70");
            Thread.sleep(1000);

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalance = preSubsBalance.Balance.intValue();
            int prefrozenbalance = preSubsBalance.frozenBalance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, subs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            Login.init(t1).login(init);
            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(subs, DataFactory.getRandomNumberAsString(3), "5", "test", null);

            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInit = subsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = subsBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + 5);
            t1.info("Expected: " + (prefrozenbalance + 5) + " Actual: " + frozenbalanceAfterInit);
            Thread.sleep(1000);
            Assert.assertEquals(balanceAfterInit, preBalance);
            t1.info("Expected: " + (preBalance) + " Actual: " + balanceAfterInit);

            Login.init(t1).login(app);
            StockManagement.init(t1).approveReimbursement(txnId);

            Thread.sleep(2000);
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postBalance = postSubsBalance.Balance.intValue();
            int postFrozenbalance = postSubsBalance.frozenBalance.intValue();

            Thread.sleep(1000);
            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - 5);
            t1.info("Expected: " + (frozenbalanceAfterInit - 5) + " Actual: " + postFrozenbalance);
            Thread.sleep(1000);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 5);
            t1.info("Expected: " + (preOperatorBalance.intValue() + 5 + " Actual: " + postOperatorBalance));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void verifyThatReimbursementIsSuccessfulThroughBulkPayoutTool() throws Exception {
        ExtentTest t1 = pNode.createNode("SC1",
                "verify That Reimbursement Is Successful Through Bulk Payout Tool");

        try {

            String agentCode = MobiquityGUIQueries.getAgentCode(mer.MSISDN);

            //fetch the previous balance of users
            //Subscriber
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preSBalance = preSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance preMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int preMBalance = preMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int preEBalance = preEntBalance.Balance.intValue();

            //Biller
            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int preBBalance = preBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance preRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int preBalance = preRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance preIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int pre03Balance = preIND03Balance.Balance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            String fileName = BulkPayoutTool.init(t1).generateFileForReimbursement(agentCode, subs, biller, RCoperator.OperatorId, enterp);

            Login.init(t1).login(app);

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, fileName);
            Thread.sleep(5000);

            //Login with user who has bulk payout approve access
            // Login.init(t1).loginAsSuperAdmin("BULK_APPROVE");
            Login.init(t1).login(DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(1));

            //Approve bulk payout
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, id, true);

            //check for the status in BulkPayout Dashboard screen
            String file = BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, id, true, true);

            ArrayList<String> idValue = new ArrayList<String>();
            idValue.add(agentCode);
            idValue.add(subs.MSISDN);
            idValue.add(biller.LoginId);
            idValue.add(RCoperator.OperatorId);
            idValue.add(enterp.LoginId);
            idValue.add("IND03B");

            for (int i = 0; i < idValue.size(); i++) {
                BulkPayoutTool.init(t1).checkLog(i, idValue.get(i), file);
            }

            //Subscriber
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postSBalance = postSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance postMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int postMBalance = postMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int postEBalance = postEntBalance.Balance.intValue();

            //Biller
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int postBBalance = postBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance postRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int postRCBalance = postRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance postIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int postI03Balance = postIND03Balance.Balance.intValue();

            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postSBalance, preSBalance - 2);
            Assert.assertEquals(postMBalance, preMBalance - 2);
            Assert.assertEquals(postEBalance, preEBalance - 2);
            Assert.assertEquals(postBBalance, preBBalance - 2);
            Assert.assertEquals(postRCBalance, preBalance - 2);
            Assert.assertEquals(postI03Balance, pre03Balance - 2);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue() + 12);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0})
    public void verifyThatReimbursementIsRejectedSuccessfulThroughBulkPayoutTool() throws Exception {
        ExtentTest t1 = pNode.createNode("SC3",
                "verify That Reimbursement Is Rejected Successful Through BulkPayout Tool");

        try {

            String agentCode = MobiquityGUIQueries.getAgentCode(mer.MSISDN);

            //fetch the previous balance of users
            //Subscriber
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preSBalance = preSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance preMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int preMBalance = preMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int preEBalance = preEntBalance.Balance.intValue();

            //Biller
            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int preBBalance = preBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance preRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int preBalance = preRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance preIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int pre03Balance = preIND03Balance.Balance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            String fileName = BulkPayoutTool.init(t1).generateFileForReimbursement(agentCode, subs, biller, RCoperator.OperatorId, enterp);

            Login.init(t1).login(app);

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, fileName);
            Thread.sleep(5000);

            //Login with user who has bulk payout approve access
            Login.init(t1).login(DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(1));

            //Approve bulk payout
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, id, false);

            //check for the status in BulkPayout Dashboard screen
            //BulkPayoutTool.init(t1).verifyBulkPayoutDashboard();

            //Subscriber
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postSBalance = postSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance postMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int postMBalance = postMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int postEBalance = postEntBalance.Balance.intValue();

            //Biller
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int postBBalance = postBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance postRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int postRCBalance = postRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance postIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int postI03Balance = postIND03Balance.Balance.intValue();

            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postSBalance, preSBalance);
            Assert.assertEquals(postMBalance, preMBalance);
            Assert.assertEquals(postEBalance, preEBalance);
            Assert.assertEquals(postBBalance, preBBalance);
            Assert.assertEquals(postRCBalance, preBalance);
            Assert.assertEquals(postI03Balance, pre03Balance);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0})
    public void verifyThatProperErrorMessagesAreDisplayedForTheRecordsOnInitiatingReimbursementThroughBulkPayoutTool() throws Exception {

        ExtentTest t1 = pNode.createNode("SC2",
                "verify That Proper Error Messages Are Displayed For The Records On Initiating Reimbursement Through BulkPayout Tool");

        try {

            Transactions.init(t1).userBarUnbar(enterp, "BAR", Constants.BAR_AS_SENDER);

            ServiceCharge merW = new ServiceCharge(Services.OPERATOR_WITHDRAW, mer, opt, null, null, null, null);

            Login.init(t1).login(opt);
            TransferRuleManagement.init(t1).deleteTransferRuleForSpecificGrade(merW, Constants.GRADE_ALL, Constants.GRADE_ALL);

            String agentCode = MobiquityGUIQueries.getAgentCode(mer.MSISDN);

            //fetch the previous balance of users
            //Subscriber
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preSBalance = preSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance preMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int preMBalance = preMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance preEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int preEBalance = preEntBalance.Balance.intValue();

            //Biller
            UsrBalance preBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int preBBalance = preBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance preRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int preBalance = preRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance preIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int pre03Balance = preIND03Balance.Balance.intValue();

            BigDecimal preOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            String fileName = BulkPayoutTool.init(t1)
                    .generateErrorFileForReimbursement(agentCode, subs, biller, RCoperator.OperatorId, enterp, String.valueOf(preBalance + 3));

            Login.init(t1).login(app);

            //Initiate bulk payout for Stock Reimbursement
            String id = BulkPayoutTool.init(t1).initiateNewBulkPayout("Stock Reimbursement", fileName);
            Thread.sleep(5000);

            //Login with user who has bulk payout approve access
            Login.init(t1).login(DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(1));

            //Approve bulk payout
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, id, true);

            //check for the status in BulkPayout Dashboard screen
            String file = BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_REIMBURSEMENT, id, true, false);

            ArrayList<String> idValue = new ArrayList<String>();
            idValue.add(agentCode);
            idValue.add(subs.MSISDN);
            idValue.add(biller.LoginId);
            idValue.add(RCoperator.OperatorId);
            idValue.add(enterp.LoginId);
            idValue.add("IND03B");

            for (int i = 0; i < idValue.size(); i++) {
                String error = BulkPayoutTool.init(t1).checkLog(i, idValue.get(i), file);
                errors.add(error);
            }

            Assert.assertEquals(errors.get(0), "Transfer rule for this service has not been defined");
            Assert.assertEquals(errors.get(1), "Party is not registered or allowed as a sender for this transaction in the system");
            Assert.assertEquals(errors.get(2), "Sender is invalid");
            Assert.assertEquals(errors.get(3), "There is insufficient balance to perform this transaction");
            Assert.assertEquals(errors.get(4), "Payer has been barred as sender");
            Assert.assertEquals(errors.get(5), "Requested account can not be reimbursed");

            //Subscriber
            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postSBalance = postSubsBalance.Balance.intValue();

            //Merchant
            UsrBalance postMerBalance = MobiquityGUIQueries.getUserBalance(mer, null, null);
            int postMBalance = postMerBalance.Balance.intValue();

            //Enterprise
            UsrBalance postEntBalance = MobiquityGUIQueries.getUserBalance(enterp, null, null);
            int postEBalance = postEntBalance.Balance.intValue();

            //Biller
            UsrBalance postBillerBalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            int postBBalance = postBillerBalance.Balance.intValue();

            //Rechrage Operator
            UsrBalance postRCOptBalance = MobiquityGUIQueries.getRechargeOperatorBalance(RCoperator.OperatorId);
            int postRCBalance = postRCOptBalance.Balance.intValue();

            //IND03
            UsrBalance postIND03Balance = MobiquityGUIQueries.dbGetAllNetworkStock1("101IND03");
            int postI03Balance = postIND03Balance.Balance.intValue();

            BigDecimal postOperatorBalance = MobiquityGUIQueries.fetchOperatorBalance("101", "IND01");

            Assert.assertEquals(postSBalance, preSBalance);
            Assert.assertEquals(postMBalance, preMBalance);
            Assert.assertEquals(postEBalance, preEBalance);
            Assert.assertEquals(postBBalance, preBBalance);
            Assert.assertEquals(postRCBalance, preBalance);
            Assert.assertEquals(postI03Balance, pre03Balance);
            Assert.assertEquals(postOperatorBalance.intValue(), preOperatorBalance.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Transactions.init(t1).userBarUnbar(enterp, "UNBAR", Constants.BAR_AS_SENDER);
        }
        Assertion.finalizeSoftAsserts();
    }

}
