package tests.core.CR_B9;


import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Subscriber should be able to change his default currency
 * Author Name      : Nirupama MK
 * Created Date     : 21/05/2018
 */
public class Suite_v5_MON_5877 extends TestInit {
/*
    MON_5877
    Create User with multiple currencies,
    Fetch the default Currency Code
    Run the API to Change the Default Currency Code
    Verify that the Default Currency Code id Changed Successfully

 */

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void MON_5877() throws Exception {

        ExtentTest MON_5877 = pNode.createNode("MON-5877", "Verify that subscriber / channel " +
                "user can change the default currency from one currency to another " +
                "(API name: Change Default Currency, service code: CNGDEFCUR)").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(MON_5877).createSubscriberDefaultMapping(sub,
                    true, false);

            TxnResponse response = Transactions.init(MON_5877)
                    .fetchListOfCurrenciesAssociatedWithUser(sub.MSISDN, Constants.CUSTOMER_REIMB)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            //fetch default currency code brforing changing
            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);
            MON_5877.info("Existing Default Currency code of the User - " + defaultCurrencyCodeOfUser);

            Boolean isResponseHavingListOfCurrencies = response.getResponse().extract().jsonPath()
                    .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").contains(",");

            if (isResponseHavingListOfCurrencies) {

                List<String> currencyListfromAPI = response.getResponse().extract().jsonPath()
                        .getList("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                for (String currencyCode : currencyListfromAPI) {

                    String providerID = MobiquityGUIQueries.getProviderIdAssociatedWithCurrency(currencyCode);

                    if (!currencyCode.equalsIgnoreCase(defaultCurrencyCodeOfUser)) {

                        //run the API to change the Default Currency Code
                        Transactions.init(MON_5877)
                                .changeCurrencyCode(sub, currencyCode, providerID)
                                .assertStatus(Constants.TXN_SUCCESS);

                        //Fetch the Default Currency Code after changing
                        defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

                        //verify that the Default Currency Code is Changed
                        Assertion.assertEqual(defaultCurrencyCodeOfUser, currencyCode,
                                "Default Currency Code is Changed", MON_5877);

                        break;
                    }
                }

            } else {

                String currencyFromAPI = response.getResponse().extract().jsonPath()
                        .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                if ((!currencyFromAPI.equalsIgnoreCase(defaultCurrencyCodeOfUser))) {

                    String providerID = MobiquityGUIQueries.getProviderIdAssociatedWithCurrency(currencyFromAPI);

                    Transactions.init(MON_5877)
                            .changeCurrencyCode(sub, currencyFromAPI, providerID)
                            .assertStatus(Constants.TXN_SUCCESS);

                    defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

                    Assertion.assertEqual(defaultCurrencyCodeOfUser, currencyFromAPI,
                            "Default Currency Code is Changed", MON_5877);
                } else {
                    Assertion.logAsPass("User is having only one Currency which is " +
                            "already set as Default Currency", MON_5877);
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, MON_5877);
        }
        Assertion.finalizeSoftAsserts();
    }
}

