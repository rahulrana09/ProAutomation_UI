package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Search user based on Identification Number in CCE
 * Portal when user has multiple mobile numbers with same national id
 * Author Name      : Nirupama MK
 * Created Date     : 05/06/2018
 */
public class Suite_v5_SearchUserBasedOnIdentificationNumber_Negative extends TestInit {

    private User chanlUser, sub1, sub;
    private OperatorUser netAdmin, optUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup Specific to this Script");
        try {

            netAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            chanlUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser = new OperatorUser(Constants.NETWORK_ADMIN);

            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE, sub,
                    optUser, null, null, null, null);

            ServiceChargeManagement.init(eSetup).
                    configureServiceCharge(accClosure);

            CurrencyProviderMapping.init(eSetup)
                    .mapPrimaryWalletPreference(sub);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MON_5758() throws Exception {
        /*

        Create Subscriber

        perform CASH-IN
        Delete the subscriber

        Login as netAdmin.
        Follow the links "Enquiries -> Customer Care Executive"
        Verify that deleted subscriber is not displayed in the list of subscribers with same identification number

        */
        ExtentTest MON_5758 = pNode.createNode("MON-5758", "deleted subscriber is not displayed in the " +
                " list of all subscribers with the matching national id when user type" +
                " selected is Subscriber and the account identifier selected is KYC ID");

        String mainWindow = null;
        try {

            User subs = CommonUserManagement.init(MON_5758).getDeletedUser(Constants.SUBSCRIBER, null);

            Login.init(MON_5758).login(netAdmin);
            mainWindow = Enquiries.init(MON_5758)
                    .startNegativeTest()
                    .initiateGlobalSearch(Constants.SUBSCRIBER_REIMB, Constants.CCE_KYC_ID, subs.ExternalCode);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5758);
        } finally {
            if (mainWindow != null) {
                Utils.closeWindowsExceptOne(mainWindow);
            }
            Assertion.finalizeSoftAsserts();
        }

    }

    /**
     * Disabled Duplicate Test
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void MON_5762() throws Exception {
        /*
           Set system preference FREQ_EXT_CODE_N_SUBS = 1(default value)
           Create the Subscriber with the EXTERNAL_CODE of existing Subscriber.
           Error msg is displayed
        */
        ExtentTest MON_5762 = pNode.createNode("MON-5762", "Multiple subscribers cannot be registered" +
                " with same national id (EXTERNAL_CODE) when System preference FREQ_EXT_CODE_N_SUBS is set to '1' ").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        String preferenceCode = "FREQ_EXT_CODE_N_SUBS";
        String defaultPreferenceValue = MobiquityGUIQueries.fetchDefaultValueOfPreference(preferenceCode);
        String defaultModifyAllowed = MobiquityGUIQueries.fetchmodifyAllowedFromGUI(preferenceCode);

        try {

            MobiquityGUIQueries
                    .updateModifyAllowed(preferenceCode, Constants.STATUS_ACTIVE);

            SystemPreferenceManagement.init(MON_5762)
                    .updateSystemPreference(preferenceCode, Constants.EXTERNAL_CODE_DEFAULT);

            User existingUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            User sub1 = new User(Constants.SUBSCRIBER);
            // set the external code of this nes user to the existing External Code
            sub1.setExternalCode(existingUser.ExternalCode);

            Transactions.init(MON_5762)
                    .startNegativeTest()
                    .selfRegistrationForSubscriberWithKinDetails(sub1)
                    .assertStatus("90004")
                    .assertMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, MON_5762);

        } finally {
            SystemPreferenceManagement.init(MON_5762).updateSystemPreference(preferenceCode, defaultPreferenceValue);
            MobiquityGUIQueries.updateModifyAllowed(preferenceCode, defaultModifyAllowed);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_SIT_5_0})
    public void MON_5782() throws Exception {
        /*
        Login as netAdmin.
        Follow the links "Enquiries -> Customer Care Executive"
        Verify Error msg when invalid(wrong) identification number is entered
        */
        ExtentTest MON_5782 = pNode.createNode("MON-5782", "Error msg is displayed when invalid " +
                "identification number is entered").assignCategory(FunctionalTag.ECONET_SIT_5_0);
        String mainWindow = null;
        try {

            Login.init(MON_5782).login(netAdmin);

            mainWindow = Enquiries.init(MON_5782)
                    .startNegativeTest()
                    .initiateGlobalSearch(Constants.SUBSCRIBER_REIMB,
                            Constants.CCE_KYC_ID, "77914590233556");

        } catch (Exception e) {
            markTestAsFailure(e, MON_5782);
        } finally {
            if (mainWindow != null) {
                Utils.closeWindowsExceptOne(mainWindow);
            }
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void MON_5894() throws Exception {
        /*
        Login as netAdmin.
        Follow the links "Enquiries -> Customer Care Executive"
        Verify Error msg when trying to search for churned User
        */
        ExtentTest MON_5894 = pNode.createNode("MON-5894", "Error msg is displayed when trying " +
                "to search for customer who is churned");

        String mainWindow = null;
        try {

            User sub1 = CommonUserManagement.init(MON_5894).getChurnedUser(Constants.SUBSCRIBER, null);

            Login.init(MON_5894).login(netAdmin);

            mainWindow = Enquiries.init(MON_5894)
                    .startNegativeTest()
                    .initiateGlobalSearch(Constants.SUBSCRIBER_REIMB, Constants.CCE_KYC_ID, sub1.ExternalCode);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5894);
        } finally {
            if (mainWindow != null) {
                Utils.closeWindowsExceptOne(mainWindow);
            }
            Assertion.finalizeSoftAsserts();
        }
    }

}
