package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.stockManagement.StockLiquidationApproval_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 26/03/2018
 */

public class Suite_v5_StockLiquidationApprovalLevel1 extends TestInit {

    OperatorUser networkAdmin;

    @BeforeClass(alwaysRun = true)

    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        networkAdmin = DataFactory.getOperatorUsersWithAccess("STK_LIQ_APPROVAL1", Constants.NETWORK_ADMIN).get(0);
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void stockLiquidationApprovalFields() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4091_1", "verify after clicking the Stock Liquidation Approval 1,the below fields should be populated \n" +
                "     a.From Date \n" +
                "     b.To Date \n" +
                "     c.MFS Provider \n" +
                "     d.Liquidation Bank \n" +
                "     e.Download Button ");

        try {
            //Login in to Stock Liquidation Approval Page

            Login.init(t1).login(networkAdmin);
            StockLiquidationApproval_page1 page = StockLiquidationApproval_page1.init(pNode);

            page.NavigateToLink();
            Thread.sleep(3000);
            //Verifying fields
            WebElement downloadbutton = driver.findElement(By.id("apprSearch_button_download"));

            Object[] expected = PageInit.fetchLabelTexts("//form[@id='apprSearch']/table/tbody/tr/td[1]/label");
            List<Object> expectedList = Arrays.asList(expected);
            ArrayList<Object> ex = new ArrayList<>(expectedList);
            ArrayList<String> expectedListString = (ArrayList<String>) (ArrayList<?>) ex;

            Object[] actual = UserFieldProperties.getLabels("stock.liquidation.approval");
            List<Object> actualList = Arrays.asList(actual);
            ArrayList<Object> ac = new ArrayList<>(actualList);
            ArrayList<String> actualListString = (ArrayList<String>) (ArrayList<?>) ac;


            if (Arrays.equals(expectedListString.toArray(), actualListString.toArray())) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }

            if (downloadbutton.isDisplayed()) {
                t1.info("Download Field is Displayed");
            } else {
                Assert.fail("Download field is not displayed");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void stockLiquidationApprovalCSVFields() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4091_2", "verify network admin/channel admin/super admin able to download csv file containing scheduled liquidations based on filter criteria.");

        try {

            Login.init(t1).login(networkAdmin);
            StockManagement.init(t1).downloadStockLiquidationCSV
                    (DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), DataFactory.getDefaultProvider().ProviderId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void stockLiquidationApprovalCSVHeaderFields() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4091_3", "verify below fields in downloaded csv file containing scheduled liquidations based on filter criteria.\n" +
                "     a.Sno.\n" +
                "     b.Batch ID\n" +
                "     c.Transaction ID\n" +
                "     d.Trasnaction Date\n" +
                "     e.Transaction Time\n" +
                "     f.User category\n" +
                "     g.Channel user name\n" +
                "     h.Channel user mobile number (In case of billers, this will be NA))\n" +
                "     i.User code (Agent / Biller Code)\n" +
                "     j.Bank Account Number\n" +
                "     k.Branch name\n" +
                "     l.MFS Provider\n" +
                "     m.Liquidation amount (The user's Available wallet balance)\n" +
                "     n.Approved (M) (Accepted values: Y/N, when user downloads the csv it will be prefilled with Y, but user can change it to N)\n" +
                "     o.Remarks (Mandatory if value of \"Approved\" column is \"N\") ");

        try {
            //Login in to Stock Liquidation Approval Page
            OperatorUser networkAdmin = DataFactory.getOperatorUsersWithAccess("STK_LIQ_APPROVAL1", Constants.NETWORK_ADMIN).get(0);
            Login.init(t1).login(networkAdmin);
            StockManagement.init(t1).downloadStockLiquidationCSV(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), DataFactory.getDefaultProvider().ProviderId);

            //Verifying Headers in CSV are correct
            ArrayList fields = new ArrayList();
            fields.add("Serial Number*");
            fields.add("Liquidation Transaction ID*");
            fields.add("Batch ID*");
            fields.add("Transaction Date*");
            fields.add("Transaction Time*");
            fields.add("User Category*");
            fields.add("User Name*");
            fields.add("Mobile Number*");
            fields.add("User Code*");
            fields.add("Bank Account Number*");
            fields.add("Branch Name*");
            fields.add("MFS Provider*");
            fields.add("Liquidation Amount*");
            fields.add("Product Id*");
            fields.add("Approved(Y/N)*");
            fields.add("Remarks");
            List<String> records = new ArrayList<String>();
            BufferedReader br = new BufferedReader(new FileReader(FilePath.dirFileDownloads + "Stock_Liquidation.csv"));
            String line = "";
            //Read to skip the header
            //Reading from the second line
            while ((line = br.readLine()) != null) {
                records.add(line);
            }

            String[] headerDetails = records.get(0).split(",");

            for (int i = 0; i < headerDetails.length; i++) {
                Assert.assertEquals(headerDetails[i], fields.get(i));
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
