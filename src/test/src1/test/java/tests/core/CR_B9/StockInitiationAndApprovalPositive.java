package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.stockManagement.StockLimit_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Stock Initation and Approval (Positive) User Story MON-3744
 * Author Name      : Jyoti Katiyar
 * Created Date     : 03/04/2018
 */
public class StockInitiationAndApprovalPositive extends TestInit {
    private OperatorUser opt;
    private String fromDate;
    private String toDate;
    private BigDecimal Stock_TransferLimit;
    private BigDecimal Stock_TransferLevel1;
    private BigDecimal Stock_TransferLevel2;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
        ExtentTest setup = pNode.createNode("Pre-conditions for Stock Initiation And Approval", "Get Required Base Set Users. " +
                "Select Start and End Date. " +
                "Set stock initation limit. ");
        fromDate = new DateAndTime().getDate(-30);
        toDate = new DateAndTime().getDate(0);
        System.out.println("From Date :" + fromDate);
        System.out.println("To Date : " + toDate);

        try {
            //Operator user login,We are taking first Operator user because other operator user will initiate n approve.
            OperatorUser opt = DataFactory.getOperatorUserWithAccess("STOCK_LIMIT");
            Login.init(setup).login(opt);

            /*//Adding stock limit
            StockManagement stockManagement = StockManagement.init(setup);
            stockManagement.addNetworkStockLimit(defaultProvider, Constants.Stock_TransferLimit);*/

            //Get Stock Limit from StockLimit Page1 from UI
            StockLimit_Page1 page1 = StockLimit_Page1.init(setup);
            page1.navigateToStockLimitPage();
            Stock_TransferLimit = page1.getStockLimitUI();

            //Set Stock Limit level 1 and Stock Limit level 2
            Stock_TransferLevel2 = Stock_TransferLimit.add(new BigDecimal(1));
            Stock_TransferLevel1 = Stock_TransferLimit.subtract(new BigDecimal(1));

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*To verify that network should be able to transfer stock from UI with level 2 approval */
    //Re written by rahul rana - 9/12/2018
    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockTransferWithLevel2Approval_TC01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_08", "To verify that network should be able to transfer stock from UI with level 2 approval");
        try {
            String bankName = GlobalData.defaultBankName;
            String bankId = DataFactory.getBankId(bankName);

            // Pre Bank and Primary Wallet balance
            BigDecimal preBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            BigDecimal prePrimaryWalletBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            // perform Stock Transfer
            StockManagement.init(t1)
                    .initiateAndApproveNetworkStock(GlobalData.defaultProvider.ProviderName, bankName, Stock_TransferLevel2.toString());

            // Post Bank and Primary wallet Balance
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            BigDecimal postPrimaryWalletBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

            Assertion.verifyAccountIsDebited(preBankBalance, postBankBalance, Stock_TransferLevel2, "Verify Bank Balance is Debited", t1);
            Assertion.verifyAccountIsCredited(prePrimaryWalletBalance, postPrimaryWalletBalance, Stock_TransferLevel2, "Verify Primary Wallet is Credited", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*To verify that network should be reject the transfer stock from UI at level 2 approval
     * Bug:MON-6408//fixed
     * Re written by rahul rana - 9/12/2018
     * */

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockTransferRejectionAtLevel2Approval_TC02() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_09", "Verify that Network admin can Reject the Level 2 stock approval," +
                "Verify that no stock is debited from Bank neither the Primary system wallet is credited.");
        try {

            String bankName = GlobalData.defaultBankName;
            String bankId = DataFactory.getBankId(bankName);

            // Pre Bank and Primary Wallet balance
            BigDecimal preBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            BigDecimal prePrimaryWalletBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);
            OperatorUser stockInitiator = DataFactory.getOperatorUserWithAccess("STOCK_INIT");
            Login.init(t1).login(stockInitiator);
            // perform Stock Transfer, and do first level Approval
            String txnId = StockManagement.init(t1)
                    .initiateNetworkStock(GlobalData.defaultProvider.ProviderName, bankName, Stock_TransferLevel2.toString());

            if (txnId != null) {
                startNegativeTest();
                StockManagement.init(t1)
                        .approveNetworkStockL1(txnId);

                if (Assertion.verifyActionMessageContain("stock.approval.secondLevelNeeded", "First Level approval is Successful", t1)) {
                    // reject the 2nd Level approval
                    stopNegativeTest();
                    StockManagement.init(t1)
                            .rejectNetworkStockL2(txnId);

                    // Post Bank and Primary wallet Balance
                    BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
                    BigDecimal postPrimaryWalletBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, Constants.WALLET_101_101);

                    // Validation
                    Assertion.verifyEqual(preBankBalance, postBankBalance, "Verify that Bank Stock Balance is Not debited", t1);
                    Assertion.verifyEqual(postPrimaryWalletBalance, prePrimaryWalletBalance, "Verify that Primary wallet is Not Credited", t1);
                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /*To verify that Network Admin can enquire the Stocks based on Transaction ID */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockEnquiryBasedOnTransactionId_TC03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_16", "To verify that network should be" +
                " able to transfer stock from UI with level2 approval To verify that Network Admin can enquire the Stocks based on Transaction ID ");
        try {
            //login with operator user having Group role for adding channel user
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            Login.init(t3).login(opt);

            StockManagement stockManagement = StockManagement.init(t3);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());

            //Approval @ level 1 and level 2
            stockManagement.approveNetworkStockL1(txnID);
            System.out.println(txnID);

            // Stock Enquiry for the above stock initiation based on txn id
            stockManagement.stockEnquiry(txnID);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();

    }


    /*To verify that Network Admin can enquire the Stocks based on Transaction Status & Date i.e.
    a) For Failed Transaction
     */
    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockEnquiryForFailedTransaction_TC04_A() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_17_A", "To verify that Network Admin can enquire the Stocks based on Transaction Status & Date" +
                " i.e for Failed Transaction");
        try {

            //login with operator user having Group role for adding channel user
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            Login.init(t4).login(opt);
            StockManagement stockManagement = StockManagement.init(t4);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel1.toString());
            System.out.println(txnID);

            //Approval 1 and Approval 2
            stockManagement.rejectNetworkStockL1(txnID);

            // Stock Enquiry for the above stock initiation based on txn id
            stockManagement.stockEnquiry_date(fromDate, toDate, Constants.TXN_STATUS_FAIL, txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();

    }


    /*To verify that Network Admin can enquire the Stocks based on Transaction Status & Date i.e.
    b) For Success Transaction
    */
    @Test(priority = 5, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockEnquiryForSuccessfulTransaction_TC04_B() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_17_B", "To verify that Network Admin can enquire the Stocks based on Transaction Status & Date i.e for Successful Transaction");
        try {
            //login with operator user having Group role for adding channel user
            opt = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            Login.init(t4).login(opt);
            StockManagement stockManagement = StockManagement.init(t4);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());
            System.out.println(txnID);

            //Approval 1 and Approval 2
            stockManagement.approveNetworkStockL1(txnID);

            // Stock Enquiry for the above stock initiation based on txn id
            stockManagement.stockEnquiry_date(fromDate, toDate, Constants.TXN_STATUS_SUCCESS, txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*
    To verify that Network Admin can enquire the Stocks based on Transaction Status & Date i.e.
    c) Approved at level 1 transaction

    */
    @Test(priority = 6, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockEnquiryForLevel1ApprovedTransaction_TC04_C() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_17_C", "To verify that Network Admin can enquire the Stocks based on Transaction Status & Date" +
                " i.e Approved at level 1 transaction");

        try {
            //login with operator user having Group role for adding channel user
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            Login.init(t4).login(opt);
            StockManagement stockManagement = StockManagement.init(t4);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel2.toString());
            System.out.println(txnID);

            //Approval 1
            ConfigInput.isAssert = false;
            stockManagement.approveNetworkStockL1(txnID);

            //Verify success message for approve level 1
            String actualMessage = Assertion.getActionMessage();
            if (actualMessage.contains(MessageReader.getMessage("stock.approval.secondLevelNeeded", null))) {
                t4.pass("Successfully Approved Level 1, Level 2 Approval Is Required!");
                Assertion.verifyActionMessageContain("stock.approval.secondLevelNeeded", "Second Level Approval Needed", t4);
            }
            // Stock Enquiry for the above stock initiation based on txn id
            stockManagement.stockEnquiry_date(fromDate, toDate, Constants.TXN_STATUS_APPROVED1, txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }


    /*
    To verify that Network Admin can enquire the Stocks based on Transaction Status & Date i.e.d) Transaction initiated and pending for approval-1
    d) Transaction initiated and pending for approval-1
    */
    @Test(priority = 7, groups = {FunctionalTag.MONEY_SMOKE})
    public void StockEnquiryForTransactionInitiatedTransaction_TC04_D() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_17_D", "To verify that Network Admin can enquire the Stocks based on Transaction Status & Date" +
                " i.e Transaction initiated and pending for approval-1");
        try {

            //login with operator user having Group role for adding channel user
            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("STOCK_INIT").get(0);
            Login.init(t4).login(opt);
            StockManagement stockManagement = StockManagement.init(t4);

            //Initiate Stock
            String txnID = stockManagement.initiateNetworkStock(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultBankNameForDefaultProvider(),
                    Stock_TransferLevel1.toString());
            System.out.println(txnID);

            // Stock Enquiry for the above stock initiation based on txn id
            stockManagement.stockEnquiry_date(fromDate, toDate, Constants.TXN_STATUS_INITIATED, txnID);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }


}