package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Subscriber should be able to change his default currency
 * Author Name      : Nirupama MK
 * Created Date     : 21/05/2018
 */

/*
    MON_5869
    create subscriber
    check the Default_currency_code
    change the Default_currency_code through API
    verify that the user is able to choose any currency as default

    MON_5870
    create subscriber
    check the newly added collumn Default_currency_code
    verify that the newly added collumn Default_currency_code holds the value other than NULL

    MON_5871
    create subscriber tru web
    Fetch the default currency code of the user created from Db
    Login as Netwrkadmin
    navigate to System Preference
    chk the preference code "default currency"
    verify that the new column "Default_currency_code" holds value
            as that of the "default currency" in system preference

     */
public class Suite_v5_ChangeTheDefaultCurrency_01 extends TestInit {

    private TxnResponse response;
    private User sub;


    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void MON_5869() throws Exception {

        ExtentTest MON_5869 = pNode.createNode("MON-5869", "Verify that user is able to choose" +
                " any of the associated currency as the default currency").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //Fetch the List of Currencies Associated with user
            response = Transactions.init(MON_5869)
                    .fetchListOfCurrenciesAssociatedWithUser(chUser.MSISDN, Constants.CHANNEL_REIMB)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            //fetch default currency code beforing changing
            String defaultCurrencyCode = MobiquityGUIQueries.getDefaultCurrencyCode(chUser);
            MON_5869.info("Existing Default Currency code  - " + defaultCurrencyCode);

            Boolean isResponseHavingListOfCurrencies = response.getResponse().extract().jsonPath()
                    .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY").contains(",");

            if (isResponseHavingListOfCurrencies) {

                List<String> currencyListfromAPI = response.getResponse().extract().jsonPath()
                        .getList("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                for (String currencyCode : currencyListfromAPI) {

                    String providerID = MobiquityGUIQueries.getProviderIdAssociatedWithCurrency(currencyCode);

                    if (!currencyCode.equalsIgnoreCase(defaultCurrencyCode)) {

                        //run the API to change the Default Currency Code
                        Transactions.init(MON_5869)
                                .changeCurrencyCode(chUser, currencyCode, providerID)
                                .assertStatus(Constants.TXN_SUCCESS);

                        //Fetch the Default Currency Code after changing
                        defaultCurrencyCode = MobiquityGUIQueries.getDefaultCurrencyCode(chUser);

                        //verify that the Default Currency Code is Changed
                        Assertion.assertEqual(defaultCurrencyCode, currencyCode,
                                "Default Currency Code is Changed", MON_5869);

                        break;
                    }
                }

            } else {

                String currencyFromAPI = response.getResponse().extract().jsonPath()
                        .getString("COMMAND.CURRENCYCODES.CURRENCYCODE.CURRENCY");

                if ((!currencyFromAPI.equalsIgnoreCase(defaultCurrencyCode))) {

                    String providerID = MobiquityGUIQueries.getProviderIdAssociatedWithCurrency(currencyFromAPI);

                    Transactions.init(MON_5869)
                            .changeCurrencyCode(chUser, currencyFromAPI, providerID)
                            .assertStatus(Constants.TXN_SUCCESS);

                    defaultCurrencyCode = MobiquityGUIQueries.getDefaultCurrencyCode(chUser);

                    Assertion.assertEqual(defaultCurrencyCode, currencyFromAPI,
                            "Default Currency Code is Changed", MON_5869);
                } else {
                    Assertion.logAsPass("User is having only one Currency which is " +
                            "already set as Default Currency", MON_5869);

                }
            }

        } catch (Exception e) {
            markTestAsFailure(e, MON_5869);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_5870() throws Exception {

        ExtentTest MON_5870 = pNode.createNode("MON-5870", "Verify that default_currency_code " +
                " is the new column created to store the default currency " +
                "(Default_Currency_Code is newly added column, which should not be null)");

        try {
            sub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(MON_5870).createDefaultSubscriberUsingAPI(sub);

            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

            Assertion.assertEqual(defaultCurrencyCodeOfUser.isEmpty(), false,
                    "default_currency_code is not null", MON_5870);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5870);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void MON_5871() throws Exception {

        ExtentTest MON_5871 = pNode.createNode("MON-5871", "Verify that when " +
                "subscriber is onboarded (through web), the default currency of the system " +
                " will be the default currency of the user");

        try {

            String preferenceCode = "DEFAULT_CURRENCY";
            sub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(MON_5871).createDefaultSubscriberUsingAPI(sub);

            //'DEFAULT_CURRENCY' is a new collumn created to store the default currency, which should be same as set in Sytem Preference
            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

            String defaultCurrencyCodeSetInPreference = MobiquityGUIQueries.fetchDefaultValueOfPreference(preferenceCode);

            Assertion.assertEqual(defaultCurrencyCodeSetInPreference, defaultCurrencyCodeOfUser,
                    "the default currency of the system " +
                            "is same as the default currency of the user", MON_5871);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5871);
        }
        Assertion.finalizeSoftAsserts();
    }
}