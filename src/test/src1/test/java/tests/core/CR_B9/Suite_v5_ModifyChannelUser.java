package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Liquidation;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.pageObjects.userManagement.LiquidationDetailsApprovalPage;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 13/03/2018
 */


public class Suite_v5_ModifyChannelUser extends TestInit {

    private User channeluser;
    private OperatorUser modifyuser, bankApprover, usrCreator;
    private Liquidation liq;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            channeluser = new User(Constants.WHOLESALER);
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            modifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");

            ChannelUserManagement.init(eSetup)
                    .createChannelUserDefaultMapping(channeluser, true);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void channelUserWithLiquiBankFieldInModify() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4242_1",
                "Verify Add liquidation bank checkbox should be populated in Modify channel user-->>Linked Bank Page ");

        try {

            // Modifying Channel User
            Login.init(t1).login(modifyuser);
            AddChannelUser_pg5 page1 = new AddChannelUser_pg5(t1);
            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(channeluser);

            CommonUserManagement.init(t1)
                    .navModifyToBankMapping(channeluser);
            //Verifying Liquidation Field

            Assertion.verifyEqual(page1.isLiquidationFieldDisplayed(), true,
                    "Verify that the Liquidation Field is displayed, " +
                            "as liquidation button is clicked", t1, true);

            page1.modifyLiquidationclick();

            Object[] expected = PageInit.fetchLabelTexts("(//tbody[@id='liquidationDetails']/tr[1]/td/b)[position()<7]");
            Object[] actual = UserFieldProperties.getLabels("liquidation.details");

            if (Arrays.equals(expected, actual)) {
                for (int i = 0; i < expected.length; i++) {
                    t1.pass("label Validated Successfully: " + expected[i]);
                }
            } else {
                t1.fail("Label Validation Failed");
                Utils.captureScreen(t1);
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void liquidationFieldsDisplayedinModifyPage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4242_3",
                "Verify After approving add channel user with liquidation bank," +
                        "Liquidation bank details should be populated in modify channel user page.");

        try {
            //Create Channel User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser);

            CommonUserManagement.init(t1).navModifyToBankMapping(channeluser);
            try {
                WebElement element = driver.findElement(By.xpath("//tbody[@id='liquidationDetails']/tr[1]"));
            } catch (Exception e1) {
                t1.pass("Fields are not Displayed");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void operatorUserabletoModifyOnlyLiqFreqDetails() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4242_4", " Verify After approving add channel user with liquidation bank,Operator users not able to modify Liquidation bank details other than frequency type in modify channel user page.");

        try {
            // Modifying Channel User
            Login.init(t1).login(modifyuser);

            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(channeluser);

            CommonUserManagement.init(t1).
                    modifyExistingFrequencyForLiquidationBank(channeluser, "Weekly", "Wednesday", null, null, true);
            t1.pass("Frequency Can Be Changed By OperatorUser");


            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void operatorUsernotabletoModifyLiqdetails() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4242_5", " Verify After approving add channel user with liquidation bank,Operator users not able to modify Liquidation bank details other than frequency type in modify channel user page. ");

        try {

            // Modifying Channel User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(channeluser);

            //Verifying Operator User Not Able To Modify Liq Freq Details
            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser);

            LiquidationDetailsApprovalPage page1 = new LiquidationDetailsApprovalPage(t1);
            page1.verifyHeadersDetailsinModifyPageareDisabled();
            t1.pass("Fields Are Disabled");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void liquidationDetailsGettingDisplayedOnModifyApprovalpage() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4242_6", "Verify After modify channel user with liquidation bank,Liquidation bank details should be populated in modify channel user approval page.");
        try {
            // Create Channel User
            User channeluser2 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).
                    createChannelUserDefaultMapping(channeluser2, false);

            //Modify Channel User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(channeluser2);

            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser2);

            Liquidation liq1 = new Liquidation();
            CommonUserManagement.init(t1).
                    mapDefaultBankPreferencesforliquidation(channeluser2, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liq1, true);

            CommonChannelUserPage page = CommonChannelUserPage.init(t1);
            ChannelUserManagement.init(t1).expectErrorB4Confirm().
                    approveChannelUserModification(channeluser2);

            //Verifying Details Present Or Not
            LiquidationDetailsApprovalPage page1 = new LiquidationDetailsApprovalPage(t1);
            page1.verifyHeadersDetails(DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId), liq1.LiquidationBankAccountNumber, liq1.LiquidationBankBranchName, liq1.AccountHolderName, liq1.FrequencyTradingName);
            page.clickSuspendApprove();


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void operatorUserAbletoSuspendAndDeleteLiquidationBank() throws Exception {


        ExtentTest t1 = pNode.createNode("MON_4242_7", "Verify Admin user can able to suspend and delete Liquidation bank account details of channel user.");
        try {
            // Create Channel User
            User channeluser3 = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(t1).
                    createChannelUserDefaultMapping(channeluser3, true);
            // Suspending The  Liquidation Bank
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser3);

            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser3).
                    modifyExistingStatusOfLiquidationBank(channeluser3, "SUSPEND");

            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser3);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).rejectAssociatedBanksForUser(channeluser3, DataFactory.getDefaultProvider().
                    ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            String status = MobiquityGUIQueries.getStatusOfLiquidationBank(channeluser3.MSISDN);
            DBAssertion.verifyDBAssertionEqual(status, "Y", "Verify Bank Status", t1);

            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser3);

            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser3).
                    modifyExistingStatusOfLiquidationBank(channeluser3, "SUSPEND");

            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser3);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).approveAssociatedBanksForUser(channeluser3, DataFactory.
                    getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            t1.info("Bank Account is Suspended");

            String status1 = MobiquityGUIQueries.getStatusOfLiquidationBank(channeluser3.MSISDN);
            DBAssertion.verifyDBAssertionEqual(status1, "S", "Verify Bank Status", t1);

            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser3);

            CommonUserManagement.init(t1).navModifyToBankMapping(channeluser3).
                    modifyExistingStatusOfLiquidationBank(channeluser3, "DELETE");

            ChannelUserManagement.init(t1).approveChannelUserModification(channeluser3);

            t1.info("Bank Account is Delete Initiated");

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).rejectAssociatedBanksForUser(channeluser3, DataFactory.getDefaultProvider().
                    ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            String status2 = MobiquityGUIQueries.getStatusOfLiquidationBank(channeluser3.MSISDN);
            DBAssertion.verifyDBAssertionEqual(status2, "Y", "Verify Bank Status", t1);

            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser3);

            CommonUserManagement.init(t1).
                    navModifyToBankMapping(channeluser3).
                    modifyExistingStatusOfLiquidationBank(channeluser3, "DELETE");

            ChannelUserManagement.init(t1).
                    approveChannelUserModification(channeluser3);

            Login.init(t1).login(bankApprover);

            CommonUserManagement.init(t1).approveAssociatedBanksForUser(channeluser3, DataFactory.
                    getDefaultProvider().ProviderName, DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));

            t1.info("Bank Account is Deleted");

            String status3 = MobiquityGUIQueries.getStatusOfLiquidationBank(channeluser3.MSISDN);
            DBAssertion.verifyDBAssertionEqual(status3, "N", "Verify Bank Status", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 5, groups = {FunctionalTag.v5_0})//MON-4892
    public void operatorUserAbletoAddOnlyOneLiquidationBankPerMFSProvider() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4242_10", ".Verify each user can set up only 1 liquidation bank per MFS Provider including liquidation banks which channel user is already associated.");

        try {
            // Create Channel User
            User channeluser3 = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(t1).
                    createChannelUserDefaultMapping(channeluser3, true);

            AddChannelUser_pg5 page1 = new AddChannelUser_pg5(t1);

            //Modify Channel User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(channeluser3);

            //Add Two Liquidation Bank
            CommonUserManagement.init(t1).navModifyToBankMapping(channeluser3);
            for (int i = 1; i < 2; i++) {

                page1.modifyLiquidationclick();

                WebElement liqaccountNum = driver.findElement(By.name("liquidationBankAccountNo1[" + i + "]"));
                WebElement liqbranchnam = driver.findElement(By.name("liquidationBranchName1[" + i + "]"));
                WebElement liqaccountholdernam = driver.findElement(By.name("liquidationAccHolderName1[" + i + "]"));
                WebElement freqtradingnam = driver.findElement(By.name("liquidationTradingName1[" + i + "]"));

                // UI elements


                new Select(driver.findElement(By.id("liquidationProvider" + (i) + ""))).selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
                new Select(driver.findElement(By.id("liquidationBankListId" + (i) + ""))).selectByVisibleText(DataFactory.getDefaultLiquidationBankName(DataFactory.getDefaultProvider().ProviderId));
                Liquidation liq = new Liquidation();
                liqaccountNum.sendKeys(liq.LiquidationBankAccountNumber);
                liqbranchnam.sendKeys(liq.LiquidationBankBranchName);
                liqaccountholdernam.sendKeys(liq.AccountHolderName);
                freqtradingnam.sendKeys(liq.FrequencyTradingName);

            }
            //page1.selectLiquidationFrequency(channeluser3.LiquidationFrequency, channeluser3.LiquidationDay);
            page1.clickNext(channeluser3.CategoryCode);

            Assertion.verifyErrorMessageContain("LiquidationBank.one.permfsprovider", "Only one Bank per mfs provider is allowed", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
