package tests.core.CR_B9;


import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Enquiry
 * Author Name      : shiva | Refactored [rahul rana: sep 26, 2018]
 * Created Date     : 24/07/2018
 */
public class EnquiryAPI extends TestInit {

    private String prefUserAsSub, prefUsrEnqResponse;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        prefUserAsSub = MobiquityGUIQueries.fetchDefaultValueOfPreference("CHANNEL_USER_AS_SUBS_ALLOWED");
        prefUsrEnqResponse = MobiquityGUIQueries.fetchDefaultValueOfPreference("USER_ENQUIRY_RESPONSE");
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4842", "get Channel user details");
        try {

            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            Transactions.init(t1)
                    .userenquiryapi(channeluser, "CHANNEL")
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("userenq.user.success");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("MON-4842", "get Subscriber details");
        try {
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Transactions.init(t2)
                    .userenquiryapi(subs, "CUSTOMER")
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("MON-4842",
                "Verify that transaction should fail when usr is channel user and user role is CUSTOMER");
        try {
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            if (MobiquityGUIQueries.checkMsisdnExistInSubscribers(channeluser.MSISDN)) {
                channeluser = new User(Constants.RETAILER);
                ChannelUserManagement.init(t3).createChannelUserDefaultMapping(channeluser, false);
            }
            Transactions.init(t3).userenquiryapi(channeluser, "CUSTOMER")
                    .verifyStatus("00066")
                    .assertMessage("userenq.user.error");

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("MON-4842",
                "Verify that transaction should fail when usr is Subscriber and user role is CHANNEL");
        try {
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Transactions.init(t4).userenquiryapi(subs, "CHANNEL")
                    .verifyStatus("00066")
                    .assertMessage("userenq.user.error");

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void test_05() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this test");
        ExtentTest t5 = pNode.createNode("MON-4842-Y",
                "Verify that when USER_ENQUIRY_RESPONSE is Y then " +
                        "return details of both subscriber and channel user" +
                        " if mobile number is registered as both.");

        ExtentTest t6 = pNode.createNode("MON-4842-N",
                "Verify that when USER_ENQUIRY_RESPONSE is N then " +
                        "the existing behaviour should continue: first check if " +
                        "mobile number is registered channel user and if found " +
                        "return the user details, if user is not registered as channel user, " +
                        "check if he is subscriber and if found return the user details");

        try {

            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("USER_ENQUIRY_RESPONSE", "Y");

            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            User subs = new User(Constants.SUBSCRIBER);
            if (MobiquityGUIQueries.checkMsisdnExistInSubscribers(channeluser.MSISDN)) {
                channeluser = new User(Constants.RETAILER);
                ChannelUserManagement.init(t5).createChannelUserDefaultMapping(channeluser, false);
            }
            subs.setMSISDN(channeluser.MSISDN);
            SubscriberManagement.init(t5)
                    .createDefaultSubscriberUsingAPI(subs);

            Transactions.init(t5)
                    .userEnquiryApiWithouUserRoletab(channeluser)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

            /**
             * Test For preference N
             */
            // Update the system Preference
            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("USER_ENQUIRY_RESPONSE", "N");
            Transactions.init(t6)
                    .userEnquiryApiWithouUserRoletab(channeluser)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .assertMessage("userenq.user.success");

        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("USER_ENQUIRY_RESPONSE", prefUserAsSub);
            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", prefUsrEnqResponse);
        }
        Assertion.finalizeSoftAsserts();
    }

}


