package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : WalletPreferencesEnhancements
 * Author Name      : Pushpalatha
 * Created Date     : 25/07/2018
 */
public class Suite_v5_WalletPreferencesEnhancements_MON_6006 extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void verifyThatProperErrorIsDisplayedWhenKYCPreferenceIsNotSetForSubscriberJoinSavingsClubAndSubscriberSelfRegistration() throws Exception {
        ExtentTest t1 = pNode.createNode("SC01",
                "Verify that proper error is displayed when KYC Preference is not set " +
                        "for subscriber join savings club and subscriber Self Registration");
        String bankId = null;
        User subs = null;
        try {
            bankId = DataFactory.getDefaultBankIdForDefaultProvider();
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            // update System Preference
            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_BANK_MANDATORY_FOR_SAVING_CLUB", "TRUE");
            SystemPreferenceManagement.init(t1).updateSystemPreference("CLUB_BANK_ID", bankId);

            MobiquityGUIQueries.deleteKYCPreferenceForWallet(Constants.SUBSCRIBER, "SAV_CLUB_CREATION", Constants.SAVINGS_CLUB);
            MobiquityGUIQueries.deleteKYCPreferenceForBank(Constants.SUBSCRIBER, "SAV_CLUB_CREATION", bankId, Constants.REGTYPE_NO_KYC);
            MobiquityGUIQueries.deleteKYCPreferenceForBank(Constants.SUBSCRIBER, "PREFERENCE_TYPE_BANK_ACCOUNT_LINKING", bankId, Constants.REGTYPE_NO_KYC);

            // verify savings club creation must fail
            SavingsClub sClub = new SavingsClub(subs, Constants.CLUB_TYPE_BASIC, bankId, true, true);
            Transactions.init(t1)
                    .addSavingClub(sClub)
                    .verifyMessageWithInfo("90001", "Verify Savings Club creation must fail, as no KYC preferences are defined");

            // check that wallet preference fos subscriber creation is
            List<String> walletList = DataFactory.getPayInstrumentApplicableForSubs();

            for (String wallet : walletList) {
                String walletId = DataFactory.getWalletId(wallet);
                // delete wallet Preference
                MobiquityGUIQueries.deleteKYCPreferenceForWallet(Constants.SUBSCRIBER, Constants.PREFERENCE_TYPE_USER_REGISTRATION, walletId);
                User tempUser = new User(Constants.SUBSCRIBER);
                ConfigInput.isAssert = false;
                Transactions.init(t1)
                        .selfRegistrationForSubscriberWithKinDetails(tempUser, walletId)
                        .verifyMessageWithInfo("90001",
                                "Verify that Subscriber Self Registration fails when KYC preference " +
                                        "is not defined for Wallet Saving Club");
            }

            ConfigInput.isAssert = true; // for positive cases

            // Map the wallet Preferences and create susbcriber, must be created successfully
            for (String wallet : walletList) {
                String walletId = DataFactory.getWalletId(wallet);
                // delete wallet Preference
                User sub1 = new User(Constants.SUBSCRIBER);
                CurrencyProviderMapping.init(t1).mapWalletPreferencesUserRegistration(sub1, walletId);

                Transactions.init(t1)
                        .selfRegistrationForSubscriberWithKinDetails(sub1, walletId)
                        .verifyMessageWithInfo("subscriber.self.registration.success",
                                "Verify That Subscriber is successfuly created with Pay ID:" + walletId,
                                sub1.MSISDN);
            }


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            // Set SVC Preference
            Preferences.init(t1).setSVCPreferences(subs, bankId);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyThatProperErrorIsDisplayedWhenKYCPreferenceIsNotSetForCommissionWalletForChannelUser() throws Exception {
        ExtentTest t1 = pNode.createNode("SC02",
                "Verify that proper error is displayed when KYC Preference is not set for Commission wallet for Channel user");

        try {
            OperatorUser net = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            OperatorUser usrCreator = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

            boolean commReq = AppConfig.isCommissionWalletRequired;
            if (!commReq) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_COMMISSION_WALLET_REQUIRED", "Y");
            }

            MobiquityGUIQueries.deleteKYCPreferenceForWallet(Constants.WHOLESALER, "USER_REGISTRATION", Constants.COMMISSION_WALLET);
            Login.init(t1).login(usrCreator);
            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).startNegativeTest().addChannelUser(whs);
            Assertion.verifyErrorMessageContain("wallet.preference.not.defined", "Error for KYC preference not set", t1, DataFactory.getDefaultProvider().ProviderName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}

