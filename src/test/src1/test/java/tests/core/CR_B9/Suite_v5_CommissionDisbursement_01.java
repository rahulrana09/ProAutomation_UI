package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.commissionDisbursement.CommissionDisbursement;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.bulkPayoutTool.BulkPayoutApprove_page1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page1;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Author Name      : Amith B V
 * Created Date     : 06/02/2018
 */


public class Suite_v5_CommissionDisbursement_01 extends TestInit {

    private User wholesaler, retailer;
    private OperatorUser networkAdmin;
    private CurrencyProvider defProvider;
    private Wallet commWallet;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Defining transfer rule and perform O2C");
        try {
            defProvider = DataFactory.getDefaultProvider();
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            BigDecimal minBalance = new BigDecimal("100");
            networkAdmin = DataFactory.getOperatorUserWithAccess("COMMDISINITIATE");

            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalanceInWallet(wholesaler, commWallet.WalletId, minBalance);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4234",
                "To verify that appropriate error message should be displayed if " +
                        "admin filters by domain and category to view the eligible " +
                        "users for commission disbursement and if no users are available " +
                        "with filter criteria");

        //Login as NetworkAdmin
        Login.init(t1).login(networkAdmin);
        retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
        //verify commission disbursement error message
        try {
            new Commission_Disbursement_Page1(t1)
                    .navigateToLink()
                    .selectProvider(defProvider.ProviderName)
                    .SelectDomain(retailer.DomainName)
                    .SelectCategory(retailer.CategoryName)
                    .clickOnSubmit();

            Assertion.verifyErrorMessageContain("comm.error.disbursement.nouser", "comm.error.disbursement.nouser", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, enabled = true, groups = {FunctionalTag.v5_0})
    public void test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4238", "To verify that disbursement method and wallet type should be loaded for the all the user listed in commission disbursement page");
        try {

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            //To verify that disbursement method loaded with Wallet by default and Wallet type is NORMAL
            new Commission_Disbursement_Page1(t1)
                    .navigateToLink()
                    .selectProvider(defProvider.ProviderName)
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName)
                    .clickOnSubmit();

            String disbursementMethod = new Commission_Disbursement_Page2(t1)
                    .getDisbursementMethod();
            String walletType = new Commission_Disbursement_Page2(t1)
                    .getWalletType();

            Assertion.assertEqual(disbursementMethod, "Wallet", "disbursementMethodDefaultWallet", t1);
            Assertion.assertEqual(walletType, DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName, "Wallet type is NORMAL", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.MONEY_SMOKE})
    public void test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4222/MON-4220",
                "To verify that admin should be able approve the commission " +
                        "disbursement request pending for approval " +
                        "To verify that admin should be able to download the CSV " +
                        "and override the amount in the excel and " +
                        "upload the file successfully and after upload the transaction " +
                        "should be pending for approval");
        try {

            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            double preBalance = channelUsrBalance.Balance.doubleValue();
            t1.info(wholesaler.LoginId + "'s Pre Balance in Wallet:" + commWallet.WalletName + ": " + preBalance);

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            String fileName = CommissionDisbursement.init(t1)
                    .exportCommission_Disbursement(wholesaler, defProvider.ProviderName);

            new Commission_Disbursement_Page2(t1)
                    .updateCsvWithNewAmount("4");

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true, true);


            UsrBalance channelUsrBalance1 = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            double postBalance = channelUsrBalance1.Balance.doubleValue();
            System.out.println(postBalance);

            Assert.assertTrue(preBalance > postBalance, "pre balance is greater than postbalance");
            t1.info(preBalance + " is greater than " + postBalance + " and commission disbursement is successful");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * To verify that commission disbursement should not be successful appropriate error code &
     * error message should be displayed when admin override's the commission amount to zero and
     * performs commission disbursement
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4236", "To verify that commission disbursement should not be successful when admin override's " +
                "the commission amount to zero and performs commission disbursement");
        try {

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            //Overide commission amount to zero

            String fileName = CommissionDisbursement.init(t1).exportCommission_Disbursement(wholesaler, defProvider.ProviderName);

            //override's the commission amount to zero
            new Commission_Disbursement_Page2(t1).updateCsvWithNewAmount("0.01");


            //initiate BulkPayout
            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void test_05() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4231", "To verify the commission disbursement should list the user" +
                " with balance more than minimum amount entered in filter criteria");
        int flag = 0;
        try {

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            //verify user balance is more than minimum amount entered
            new Commission_Disbursement_Page1(t1).navigateToLink()
                    .selectProvider(defProvider.ProviderName)
                    .SelectDomain(wholesaler.DomainName)
                    .SelectCategory(wholesaler.CategoryName);

            new Commission_Disbursement_Page1(t1).enterMinAmount(Constants.SVA_AMOUNT).clickOnSubmit();

            List<WebElement> v = new Commission_Disbursement_Page2(t1).checkAmt();
            for (WebElement b : v) {
                String amtValue = b.getAttribute("value");
                if (Double.parseDouble(amtValue) > (double) Integer.parseInt(Constants.SVA_AMOUNT)) {
                    flag = 1;
                    continue;
                } else {
                    flag = 0;
                    break;
                }
            }
            if (flag == 1) {
                t1.pass("Users with balance more than minimum amount entered in filter criteria are displayed");
            } else {
                t1.fail("Users with balance more than minimum amount entered in filter criteria are not displayed");
            }
            Utils.captureScreen(t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void test_06() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4235", "To verify that commission disbursement should not be successful " +
                " when admin override's the commission amount more that the available balance" +
                " and performs commission disbursement");

        try {

            //get pre Balance of the User
            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            double preBalance = channelUsrBalance.Balance.doubleValue();
            System.out.println(preBalance);

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            //Overide commission amount more than the available balance
            String amount = CommissionDisbursement.init(t1)
                    .getUserCommissionBalance(wholesaler, defProvider.ProviderName);
            double a = Double.parseDouble(amount) + 10;

            // download the csv file and update the amount with new Amount 'a'
            new Commission_Disbursement_Page2(t1)
                    .selectCommissionDisbursement(wholesaler.MSISDN)
                    .downloadCommissionDisbursement()
                    .updateCsvWithNewAmount(Double.toString(a));

            //Initiate Commission disbursement
            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, FilePath.fileCommissionDisbursement);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true, false);

            //get post Balance of the User
            UsrBalance channelUsrBalance1 = MobiquityGUIQueries.getUserBalance(wholesaler, commWallet.WalletId, null);
            double postBalance = channelUsrBalance1.Balance.doubleValue();

            Assert.assertTrue(preBalance == postBalance);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void test_07() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4237", "To verify that same admin who initiated the request for commission disbursement should not be able " +
                "approve the commission disbursement request pending for approval" +
                "error message should be displayed");

        try {
            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            String fileName = CommissionDisbursement.init(t1).exportCommission_Disbursement(wholesaler, defProvider.ProviderName);
            new Commission_Disbursement_Page2(t1).updateCsvWithNewAmount("2");

            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1)
                    .startNegativeTest()
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //verify initiator should not be able to approve
            String actual = BulkPayoutApprove_page1.init(t1).verfiy_errorMsg();
            Assertion.verifyMessageContain(actual, "bulkupload.error.message", "List does not contain request", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0})
    public void test_08() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4225", "To verify that admin should be able reject the commission disbursement request pending for approval");

        try {

            String fileName = CommissionDisbursement.init(t1)
                    .exportCommission_Disbursement(wholesaler, defProvider.ProviderName);

            new Commission_Disbursement_Page2(t1).updateCsvWithNewAmount("2");
            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, fileName);

            //reject bulk payout for commission disbursement
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0})
    public void test_9() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4217", "To verify that admin should be able to filter by domain and category to view the eligible users for commission disbursement");

        try {

            OperatorUser networkAdmin = DataFactory.getOperatorUsersWithAccess("COMMDISINITIATE").get(0);

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(networkAdmin);

            CommonUserManagement.init(t1).verifyProviders();
            CommonUserManagement.init(t1).verifyCategoryDetails();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
