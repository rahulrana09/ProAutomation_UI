package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Subscriber should be able to change his default currency
 * Author Name      : Nirupama MK
 * Created Date     : 21/05/2018
 */
public class Suite_v5_ChangeTheDefaultCurrency_02 extends TestInit {
    /*
        MON_5873
        Create the Subscriber through Self Register
        Fetch the value from newly added Collumn Default_Currency_Code from the table "mtx_party" (not null)
        Fetch the "DEFAULT_VALUE" from the table "mtx_system_preferences"
        verify that the Default_currency_code is same as Default_value of System Preference

        MON_5874
        Create Subscriber Tru Agent
        Fetch the value from newly added Collumn "Default_Currency_Code" from the table "User/mtx_party" (not null)
        Login as Ntadmin
        Fetch the "DEFAULT_VALUE" from the system preferences"
        verify that the Default_currency_code is same as Default value of System Preference

        MON_5875
        Create Channel User
        Fetch the Default value of newly added Collumn "Default_Currency_Code" from the table "User" (not null)
        Fetch the "DEFAULT_VALUE" from the table "mtx_system_preferences"
        verify that the Default_currency_code is same as Default_value of System Preference

     */

    private String defaultCurrencyCodeSetInPreference;
    private String preferenceCode = "DEFAULT_CURRENCY";
    private User sub;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Changing the Default Currency");
        defaultCurrencyCodeSetInPreference = MobiquityGUIQueries.fetchDefaultValueOfPreference(preferenceCode);
        sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0})
    public void MON_5873() throws Exception {

        ExtentTest MON_5873 = pNode.createNode("MON-5873", "Verify that when subscriber " +
                "is self registered through USSD (CUSTREG), the default currency of the system " +
                "will be the default currency of the user").assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {

            sub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(MON_5873)
                    .createDefaultSubscriberUsingAPI(sub);

            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

            Assertion.assertEqual(defaultCurrencyCodeOfUser, defaultCurrencyCodeSetInPreference,
                    "the default currency of the system is same as the default currency of the user", MON_5873);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5873);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void MON_5874() throws Exception {

        ExtentTest MON_5874 = pNode.createNode("MON-5874", "Verify that when a agent registers " +
                "a subscriber, the default currency of the system will be the default currency of the user");

        try {
            sub = new User(Constants.SUBSCRIBER);

            Transactions.init(MON_5874)
                    .SubscriberRegistrationByChannelUserWithKinDetails(sub);

            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(sub);

            Assertion.assertEqual(defaultCurrencyCodeSetInPreference, defaultCurrencyCodeOfUser,
                    "the default currency of the system" +
                            " is same as the default currency of the user", MON_5874);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5874);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void MON_5875() throws Exception {

        ExtentTest MON_5875 = pNode.createNode("MON-5875", "Verify that when channel user is " +
                "onboarded (through web), the default currency of the system will be the default currency of the user");

        try {

            User chnlUser = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(MON_5875)
                    .createChannelUserDefaultMapping(chnlUser, false);

            String defaultCurrencyCodeOfUser = MobiquityGUIQueries.getDefaultCurrencyCode(chnlUser);

            Assertion.assertEqual(defaultCurrencyCodeSetInPreference, defaultCurrencyCodeOfUser,
                    "the default currency of the system is same as the default currency of the user", MON_5875);

        } catch (Exception e) {
            markTestAsFailure(e, MON_5875);
        }
        Assertion.finalizeSoftAsserts();
    }
}
