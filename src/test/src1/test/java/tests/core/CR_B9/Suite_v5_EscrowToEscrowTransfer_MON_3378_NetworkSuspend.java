package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.escrowToEscrowTransfer.EscrowToEscrowTransfer;
import framework.features.master.Master;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.junit.Ignore;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_EscrowToEscrowTransfer_MON_3378_NetworkSuspend
 * Author Name      : Pushpalatha
 * Created Date     : 4/02/2018
 */

@Ignore("BUG:MON-4698")
public class Suite_v5_EscrowToEscrowTransfer_MON_3378_NetworkSuspend extends TestInit {

    private SuperAdmin supradm1, suadm1;

    @AfterMethod(enabled = false)
    public void postCondition() throws Exception {

        ExtentTest tTeardown = pNode.createNode("Teardown", "Resume Network");
        try {
            Login.init(tTeardown).
                    loginAsSuperAdmin(suadm1);

            Master.init(tTeardown).suspendNetworkStatus();

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, enabled = false)
    public void MON_3378_NetworkSuspended_EscrowToEscrowInitiate() throws Exception {

        ExtentTest t1 = pNode.createNode("SC13",
                "Verify that Stock Transfer between Escrow Accounts transaction is not successful when network is suspended in the system");
        try {
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String bank1 = DataFactory.getNonTrustBankName(defaultProvider);
            String bank2 = DataFactory.getTrustBankName(defaultProvider);
            String refNo = DataFactory.getRandomNumberAsString(5);

            supradm1 = DataFactory.getSuperAdminWithAccess("MNT_STS");

            //Suspend Network
            suadm1 = new SuperAdmin(supradm1.LoginId, "MNT_STS");
            Login.init(t1).
                    loginAsSuperAdmin(suadm1);

            Master.init(t1).suspendNetworkStatus();

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).startNegativeTest().initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);
            Assertion.verifyErrorMessageContain("network.suspend", "Network suspended", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, enabled = false)
    public void MON_3378_NetworkSuspended_EscrowToEscrowApprove() throws Exception {

        ExtentTest t1 = pNode.createNode("SC16",
                "Verify that Escrow to Escrow transfer approval is not successful when network is suspended in the system and system should display proper error message");
        try {
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String bank1 = DataFactory.getNonTrustBankName(defaultProvider);
            String bank2 = DataFactory.getTrustBankName(defaultProvider);
            String refNo = DataFactory.getRandomNumberAsString(5);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            //Suspend Network
            supradm1 = DataFactory.getSuperAdminWithAccess("MNT_STS");
            suadm1 = new SuperAdmin(supradm1.LoginId, "MNT_STS");

            Login.init(t1).
                    loginAsSuperAdmin(suadm1);

            Master.init(t1).suspendNetworkStatus();

            //approve Escrow to Escrow
            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            EscrowToEscrowTransfer.init(t1)
                    .approveEscrowToEscrowTransfer(String.valueOf(txnId2), "test");

            //Resume Network
            Login.init(t1).
                    loginAsSuperAdmin(suadm1);
            Master.init(t1).suspendNetworkStatus();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, enabled = false)
    public void verifyThatSuperAdminCheckerOrNetworkAdminIsAbleToRejectTheInitiatedEscrowtoEscrowTransfer() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4467_SC5_SC7",
                "5.Verify that superadmin (checker) or network admin is able to reject the initiated Escrow to Escrow Transfer and proper message should display %n" +
                        "7.Verify that upon Escrow to Escrow Transfer approval proper credit debit should happen from the respective banks");
        try {
            String defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            String bank1 = DataFactory.getNonTrustBankName(defaultProvider);
            String bank2 = DataFactory.getTrustBankName(defaultProvider);
            String refNo = DataFactory.getRandomNumberAsString(5);

            Login.init(t1).loginAsSuperAdmin("ESCROW_INIT");
            String txnId2 = EscrowToEscrowTransfer.init(t1).initiateEscrowToEscrowTransfer(defaultProvider, bank1, bank2, refNo, Constants.MIN_TRANSACTION_AMT);

            //Suspend Network
            SuperAdmin supradm1 = DataFactory.getSuperAdminWithAccess("MNT_STS");
            SuperAdmin suadm1 = new SuperAdmin(supradm1.LoginId, "MNT_STS");
            Login.init(t1).
                    loginAsSuperAdmin(suadm1);

            Master.init(t1).suspendNetworkStatus();
            OperatorUser netAdmn = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(netAdmn);
            EscrowToEscrowTransfer.init(t1)
                    .rejectEscrowToEscrowTransfer(String.valueOf(txnId2), "test");

            //Resume Network
            Login.init(t1).
                    loginAsSuperAdmin(suadm1);
            Master.init(t1).suspendNetworkStatus();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }
}
