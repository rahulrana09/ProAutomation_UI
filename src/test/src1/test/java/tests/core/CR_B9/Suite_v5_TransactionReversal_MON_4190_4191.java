package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.globalConstant.Constants.TXN_SUCCESS;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : TransactionReversal
 * Author Name      : PushpaLatha
 * Created Date     : 24/07/2018
 */
public class Suite_v5_TransactionReversal_MON_4190_4191 extends TestInit {

    private OperatorUser opt, o2cApprover1;
    private User subs;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "TransactionReversal");
        try {
            //Get wholesaler
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(whs, new BigDecimal("500"));

            //Get subscriber
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge cashIn1 = new ServiceCharge(Services.CASHIN, whs, subs, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(cashIn1);

            Transactions.init(eSetup).initiateCashIn(subs, whs, new BigDecimal(200));

            //Get operater user
            opt = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");

            //Configure service charge for Merchant Payment for normal wallet
            ServiceCharge merchPayN = new ServiceCharge(Services.MERCHANT_PAY, subs, whs, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(merchPayN);

            //Configure service charge for Transaction Reversal from whs to subs for normal wallet
            ServiceCharge txnCrctN = new ServiceCharge(Services.TXN_CORRECTION, whs, subs, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(txnCrctN);

            //Configure service charge for Merchant Payment for Savings club wallet
            ServiceCharge merchPay = new ServiceCharge(Services.MERCHANT_PAY, subs, whs, null, Constants.SALARY_WALLET, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(merchPay);

            //Configure service charge for Transaction Reversal from whs to subs
            ServiceCharge txnCrct = new ServiceCharge(Services.TXN_CORRECTION, whs, subs, Constants.SALARY_WALLET, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(txnCrct);

            //Configure service charge for self reimbursement
            ServiceCharge selfReImb = new ServiceCharge(Services.SELF_REIMBURSEMENT, whs, opt, Constants.SALARY_WALLET, null, null, null);
            ServiceChargeManagement.init(eSetup).configureServiceCharge(selfReImb);

            //Configure service charge for Transaction Reversal from opt to whs
            ServiceCharge txnCrct1 = new ServiceCharge(Services.TXN_CORRECTION, opt, whs, null, Constants.SALARY_WALLET, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(txnCrct1);

            //Configure service charge for self reimbursement for Normal Wallet
            ServiceCharge selfReImbN = new ServiceCharge(Services.SELF_REIMBURSEMENT, whs, opt, null, null, null, null);
            ServiceChargeManagement.init(eSetup).configureServiceCharge(selfReImbN);

            //Configure service charge for Transaction Reversal from opt to whs
            ServiceCharge txnCrctN1 = new ServiceCharge(Services.TXN_CORRECTION, opt, whs, null, null, null, null);
            TransferRuleManagement.init(eSetup).configureTransferRule(txnCrctN1);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenReceiverIsChurnInitiated() throws Exception {

        ExtentTest t1 = pNode.createNode("SC36",
                "Verify that transaction reversal initiation is " +
                        "not successful when transactor of transaction reversal " +
                        "(receiver in original transaction) is churn initiated");
        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(whs, false);

            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayment(whs, subs, "5");
            //response.verifyStatus(TXN_SUCCESS);
            String txnId = response.TransactionId;

            Login.init(t1).login(opt);
            CommonUserManagement.init(t1).churnInitiateUser(whs);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnId, Constants.NORMAL_WALLET, Services.MERCHANT_PAY);
            res.verifyMessage("reversal.initiator.churn");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenSenderIsChurnInitiated() throws Exception {

        ExtentTest t1 = pNode.createNode("SC28",
                "Verify that transaction reversal initiation is " +
                        "not successful when transactor of transaction reversal is churn initiated");

        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs, new BigDecimal("100"));

            TxnResponse response = Transactions.init(t1).selfReimbursement(whs, 5);
            response.verifyStatus(TXN_SUCCESS);
            String txnid = response.TxnId;

            Login.init(t1).login(opt);
            CommonUserManagement.init(t1).churnInitiateUser(whs);

            SfmResponse res = Transactions.init(t1)
                    .initiateTransactionReversalByChannelUser(whs, txnid, Constants.NORMAL_WALLET, Services.MERCHANT_PAY);
            res.verifyMessage("reversal.initiator.churn");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, enabled = false, groups = {FunctionalTag.v5_0})
    public void MON_4190_Receiver_TransactionReversalDeleteInitiated() throws Exception {
        ExtentTest t1 = pNode.createNode("SC16",
                "Verify that transaction reversal initiation is not successful when user is delete initiated");

        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);

            //User subs = DataFactory.getChannelUser(Constants.SUBSCRIBER).get(0);
            //TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs,new BigDecimal(50));

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            ServiceCharge selfReImb = new ServiceCharge(Services.SELF_REIMBURSEMENT, whs, opt, null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(selfReImb);

            TxnResponse response = Transactions.init(t1).selfReimbursement(whs, 50);
            response.verifyStatus(TXN_SUCCESS);
            String txnid = response.TxnId;

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).initiateChannelUserDelete(whs);

            ServiceCharge txnCrct = new ServiceCharge(Services.TXN_CORRECTION, opt, whs, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(txnCrct);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.NORMAL_WALLET, Services.MERCHANT_PAY);
            res.verifyMessage("reversal.initiator.churn");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenSenderAccountIsSuspended() throws Exception {
        ExtentTest t1 = pNode.createNode("SC31",
                "Verify that transaction reversal initiation is not " +
                        "successful when transactor of transaction reversal " +
                        "(sender in original transaction) account is suspended");

        try {

            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs, Constants.SALARY_WALLET);

            TxnResponse response = Transactions.init(t1).selfReimbursement(whs, 5, Constants.SALARY_WALLET);
            response.verifyStatus(TXN_SUCCESS);
            String txnid = response.TxnId;

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Suspended", Constants.SALARY_WALLET);
            ChannelUserManagement.init(t1).modifyUserApproval(whs);

            SfmResponse res = Transactions.init(t1)
                    .initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.SELF_REIMBURSEMENT);
            res.verifyMessage("reversal.receiver.account.suspended");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenSenderAccountIsDeleteInitiated() throws Exception {
        ExtentTest t1 = pNode.createNode("SC32",
                "Verify that transaction reversal initiation is not successful when transactor of transaction reversal (sender in original transaction) account is deleted");

        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(whs,
                            Constants.SALARY_WALLET);

            TxnResponse response = Transactions.init(t1).selfReimbursement(whs, 10, Constants.SALARY_WALLET);
            response.verifyStatus(TXN_SUCCESS);
            String txnid = response.TxnId;

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Deleted", Constants.SALARY_WALLET);

            Thread.sleep(2000);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.SELF_REIMBURSEMENT);
            res.verifyMessage("reversal.receiver.account.delete.initiated");

            Thread.sleep(2000);

            ChannelUserManagement.init(t1).modifyUserApproval(whs);

            SfmResponse res1 = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.SELF_REIMBURSEMENT);
            res1.verifyMessage("reversal.receiver.account.deleted");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenReceiverAccountIsSuspended() throws Exception {
        ExtentTest t1 = pNode.createNode("SC39",
                "Verify that transaction reversal initiation is not successful when transactor of transaction reversal (receiver in original transaction) account is suspended ");

        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            SfmResponse response = Transactions.init(t1).initiateMerchantPayment(whs, subs, "5", Constants.SALARY_WALLET);
            response.assertStatus(Constants.TXN_STATUS_SUCCEEDED, t1);
            String txnid = response.TransactionId;

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Suspended", Constants.SALARY_WALLET);
            ChannelUserManagement.init(t1).modifyUserApproval(whs);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.MERCHANT_PAY);
            res.verifyMessage("reversal.sender.account.suspended");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0})
    public void transactionReversalNotSuccesfulWhenReceiverAccountIsDeleteInitiated() throws Exception {
        ExtentTest t1 = pNode.createNode("SC40",
                "Verify that transaction reversal initiation is not successful when transactor of transaction reversal (receiver in original transaction) account is delete initiated ");
        try {
            User whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUser(whs, false);

            SfmResponse response = Transactions.init(t1).initiateMerchantPayment(whs, subs, "5", Constants.SALARY_WALLET);
            response.assertStatus(Constants.TXN_STATUS_SUCCEEDED, t1);
            String txnid = response.TransactionId;

            Login.init(t1).login(opt);
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(whs, "Deleted", Constants.SALARY_WALLET);

            Thread.sleep(2000);

            SfmResponse res = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.MERCHANT_PAY);
            res.verifyMessage("reversal.sender.account.delete.initiated");

            Thread.sleep(2000);

            ChannelUserManagement.init(t1).modifyUserApproval(whs);

            SfmResponse res1 = Transactions.init(t1).initiateTransactionReversalByChannelUser(whs, txnid, Constants.SALARY_WALLET, Services.MERCHANT_PAY);
            res1.verifyMessage("reversal.receiver.account.deleted");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
