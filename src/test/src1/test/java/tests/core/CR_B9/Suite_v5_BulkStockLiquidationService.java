package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkStockLiquidationCSV;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Modify ChannelUser with LiquidationDetails
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 10/04/2018
 */

public class Suite_v5_BulkStockLiquidationService extends TestInit {


    private User chUser;
    private String defaultBankName, liquidationBank;
    private CurrencyProvider providerOne;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            defaultBankName = GlobalData.defaultBankName;
            providerOne = GlobalData.defaultProvider;
            liquidationBank = DataFactory.getDefaultLiquidationBankName(providerOne.ProviderId);

            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            opt = DataFactory.getOperatorUserWithAccess("STK_LIQ_APPROVAL1", Constants.NETWORK_ADMIN);


            ServiceCharge stockliq = new ServiceCharge
                    (Services.STOCK_LIQUIDATION_SERVICE, chUser, opt, null, null, null, null);
            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(stockliq);

            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(chUser);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    // Disabled due to Development of CR - MON-7578
    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.SYSTEM_TEST}, enabled = false)
    public void operatorUserInitiate_ApproveBulkStockLiquidation() {
        ExtentTest t1 = pNode.createNode("MON_4451_1_2_3",
                "Verify SuperAdmin/1st NetworkAdmin should able to " +
                        "initiate/Approve/View/Dashboard Bulk stock Liquidation(Bulk Payout tool-->>" +
                        "New Bulk payout initiate)");
        try {

            /*
            Initiate Stock Liquidation
            download the csv file from stock liquidation approval Page1
             */
            String batchId = "BA." + DataFactory.getRandomNumberAsString(5);
            Transactions.init(t1).stockLiquidationGeneration(chUser, batchId);
            String liqTxnId = MobiquityGUIQueries.getLiquidationId(chUser.MSISDN, batchId);

            Login.init(t1).login(opt);
            String fileName = StockManagement.init(t1)
                    .downloadStockLiquidationCSV(liquidationBank, providerOne.ProviderId);

            /*
            Once the file is successfully downloaded, get the information only relevant to chUser
            Update the csv file
            Make sure that liquidation amount is a little less than available balance, as service charges are configured,
            initiation / approval can yield error
             */
            BulkStockLiquidationCSV entry1 = StockManagement.init(t1).getStockLiquidationDetailsForUser(batchId, fileName);
            entry1.liquidationAmount = new BigDecimal(entry1.liquidationAmount).divide(new BigDecimal(2)).toString();
            List<BulkStockLiquidationCSV> stockLiquidationList = new ArrayList<>();
            stockLiquidationList.add(entry1);

            // ***
            // NOTE:  same approach can be used to add multiple entries to the template file
            // ***

            String fileName1 = BulkPayoutTool.init(t1)
                    .downloadAndUpdateBulkStockLiquidationCsv(stockLiquidationList);

            String batchId1 = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, fileName1);

            //Approve bulk payout for Stock Liquidation
            BulkPayoutTool.init(t1)
                    .approveNewBulkPayoutForStockLiquidation(opt, batchId1, defaultBankName, true, 1);

            //Check Bulk in Dashboard
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_STOCK_LIQUIDATION, batchId1, true, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void operatorUserAbleToViewFailedBatchStatusInDashBoard() {
        ExtentTest t1 = pNode.createNode("MON_4451_4",
                "Verify SuperAdminc/SuperAdminm/NetworkAdmin able to view failed " +
                        "Batch stock liquidation requests(Bulk Payout tool-->>New Bulk payout DashBoard");
        try {
            //Download Template File
            OperatorUser networkAdmin = DataFactory.getOperatorUsersWithAccess("STK_LIQ_APPROVAL1", Constants.NETWORK_ADMIN).get(0);
            Login.init(t1).login(networkAdmin);

            BulkPayoutTool.init(t1).downloadTemplateFile("Stock Liquidation");
            String fileName = FilePath.dirFileDownloads + "BULK_STKLIQREQ-template.csv";
            //Initiate Bulk Payout
            BulkPayoutTool.init(t1).startNegativeTest().
                    initiateNewBulkPayout("Stock Liquidation", fileName);
            //Check Bulk Payout
            BulkPayoutTool.init(t1).checkBulkPayoutDashboardforSelectedStatus(false);
            WebElement initiator = driver.findElement
                    (By.xpath("//span[text()='" + networkAdmin.LoginId + "']"));
            if (initiator.isDisplayed()) {
                t1.pass("Initiator can view details in Dashboard");
            } else {
                Assert.fail("Initiator cannot view details in Dashboard");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void verifyCSVHeaderFields() {
        ExtentTest t1 = pNode.createNode("MON_4451_6", "Verify All Fields in CSV");
        try {
            //Download Template File
            OperatorUser networkAdmin = DataFactory.getOperatorUsersWithAccess("STK_LIQ_APPROVAL1", Constants.NETWORK_ADMIN).get(0);
            Login.init(t1).login(networkAdmin);
            BulkPayoutTool.init(t1).downloadTemplateFile("Stock Liquidation");
            //Verify Header Fields
            ArrayList fields = new ArrayList();
            fields.add("Serial Number*");
            fields.add("Liquidation Transaction ID*");
            fields.add("Batch ID*");
            fields.add("Transaction Date*");
            fields.add("Transaction Time*");
            fields.add("User Category*");
            fields.add("User Name*");
            fields.add("Mobile Number*");
            fields.add("User Code*");
            fields.add("Bank Account Number*");
            fields.add("Branch Name*");
            fields.add("MFS Provider*");
            fields.add("Liquidation Amount*");
            fields.add("Product Id*");
            fields.add("Approved(Y/N)*");
            fields.add("Remarks");

            List<String> records = new ArrayList<String>();
            Thread.sleep(10000);

            BufferedReader br = new BufferedReader(new FileReader(FilePath.dirFileDownloads + "BULK_STKLIQREQ-template.csv"));
            String line = "";
            //Read to skip the header
            //Reading from the second line
            while ((line = br.readLine()) != null) {
                records.add(line);
            }

            String[] headerDetails = records.get(0).split(",");

            for (int i = 0; i < headerDetails.length; i++) {

                Assert.assertEquals(headerDetails[i], fields.get(i));
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
