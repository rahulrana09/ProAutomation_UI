package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.master.Master;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_StockReimbursement_NetworkSuspend
 * Author Name      : PushpaLatha
 * Created Date     : 24/07/2018
 */
public class Suite_v5_StockReimbursement_NetworkSuspend extends TestInit {

    @BeforeClass(enabled = false)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "NetworkSuspend");
        //Suspend Network
        Login.init(pNode).
                loginAsSuperAdmin("MNT_STS");

        Master.init(pNode).suspendNetworkStatus();
    }

    @AfterClass(enabled = false)
    public void postCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Resume Network");
        //Resume Network
        Login.init(pNode).
                loginAsSuperAdmin("MNT_STS");

        Master.init(pNode).suspendNetworkStatus();
    }

    @Test(priority = 1, enabled = false, groups = {FunctionalTag.v5_0})
    public void NetworkSuspended_StockReimbursement() throws Exception {

        ExtentTest t1 = pNode.createNode("SC17", "Verify that network admin is not able to initiate reimbursement service when network is suspended");

        try {
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            OperatorUser opt = DataFactory.getOperatorUserWithAccess("STOCK_REINIT", Constants.NETWORK_ADMIN);

            ServiceCharge obj = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, opt, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(obj);

            startNegativeTest();
            StockManagement.init(t1).initiateStockReimbursement(whs, DataFactory.getRandomNumberAsString(3), "5", "test", null);
            Thread.sleep(1000);
            Assertion.verifyErrorMessageContain("network.suspend", "Network Suspend check", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
