package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Author Name      : Amith
 * Created Date     : 10/05/2018
 */

public class Suite_v5_TransferFundsBetweenSelfWallets_01 extends TestInit {

    SoftAssert softAssert;
    private User wholesaler;


    /**
     * MON-5332,MON-5347
     * To verify commission policy should not be displayed in the UI for Funds Transfer between own wallets service in UI for commission policy
     * To verify service charge should not be displayed in the UI for Funds Transfer between own wallets service in UI for service charge policy
     *
     * @throws Exception
     */

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void fundsTransferBetweenOwnWallets_5332_5347() throws Exception {
        softAssert = new SoftAssert();
        ExtentTest t1 = pNode.createNode("MON_5455_5347",
                "To verify commission policy should not be displayed in the UI for Funds Transfer between own wallets service in UI for commission policy" +
                        "To verify service charge should not be displayed in the UI for Funds Transfer between own wallets service in UI for service charge policy");
        try {
            //Login as a network admin > navigate to Pricing engine page > Select "Service Charge" > Search for  Funds Transfer between own wallets service
            boolean sValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("SERVICE CHARGE", "FUNDS TRANSFER BETWEEN OWN WALLETS");
            if (sValue) {
                t1.fail(" Funds Transfer between own wallets service  is displayed in Pricing Engine UI for SERVICE CHARGE");

            } else {
                t1.pass(" Funds Transfer between own wallets service  is NOT displayed in Pricing Engine UI for SERVICE CHARGE");
            }

            //Login as a network admin > navigate to Pricing engine page > Select "COMMISSION" > Search for  Funds Transfer between own wallets service
            boolean cValue = PricingEngine.init(t1).checkServiceInPricingEnginePage("COMMISSION", "FUNDS TRANSFER BETWEEN OWN WALLETS");
            if (cValue) {
                t1.fail(" Funds Transfer between own wallets service  is displayed in Pricing Engine UI for COMMISSION");
                softAssert.fail(" Funds Transfer between own wallets service  is displayed in Pricing Engine UI for COMMISSION");

            } else {
                t1.pass(" Funds Transfer between own wallets service  is NOT displayed in Pricing Engine UI for COMMISSION");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * MON-5337
     * To verify that commission wallet should not be listed in the dropdown of instrument type in transfer rule page in UI
     *
     * @throws Exception
     */
    //BUG MON-5462//fixed
    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void verifyCommissionWalletNotListedWhileDefiningTransferRule() throws Exception {
        softAssert = new SoftAssert();
        ExtentTest t1 = pNode.createNode("MON-5337", "To verify that commission wallet should not be listed in the dropdown of instrument type in transfer rule page in UI");

        wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        try {
            ServiceCharge transferFunds = new ServiceCharge(Services.TRANSFER_FUNDS, wholesaler, wholesaler, null, null, null, null);
            //commission wallet should not be listed in the dropdown of instrument type
            TransferRuleManagement.init(t1).verifyPaymentInstrumentType(transferFunds, DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName);
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

}
