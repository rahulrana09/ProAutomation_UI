package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Suite_v5_GradeBasedTransaferRule_MON_4825
 * Author Name      : Pushpalatha
 * Created Date     : 4/02/2018
 */

public class Suite_v5_GradeBasedTransaferRule_MON_4825 extends TestInit {

    private User subs, whs;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {
            subs = new User(Constants.SUBSCRIBER, "Gold Subscriber");
            SubscriberManagement.init(eSetup).createSubscriberDefaultMapping(subs, true, false);

            whs = new User(Constants.WHOLESALER, "Silver Wholesaler");
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(eSetup).createChannelUserDefaultMapping(whs, false);

            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(whs, new BigDecimal("300"));
            Transactions.init(eSetup).initiateCashIn(subs, whs, new BigDecimal(150));

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void createTransferRuleBasedOnGrade() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4825_1",
                "Transaction should be successful when Transfer Rule is defined for the user grades involved in the transaction");
        try {

            ServiceCharge merchPay = new ServiceCharge(Services.MERCHANT_PAY, subs, whs, null, null, null, null);
            merchPay.setPayerGrade(Constants.GOLD_SUBSCRIBER);
            merchPay.setPayeeGrade(Constants.SILVER_WHOLESALER);

            TransferRuleManagement.init(t1).deleteTransferRuleForSpecificGrade(merchPay, Constants.GRADE_ALL, Constants.GRADE_ALL);
            Thread.sleep(5000);

            TransferRuleManagement.init(t1).configureTransferRule(merchPay);
            Thread.sleep(5000);

            SfmResponse response = Transactions.init(t1).initiateMerchantPayment(whs, subs, "5");
            String txnid = response.TransactionId;
            response.verifyMessage("Transaction.success.message", "Merchant Payment", "5", subs.MSISDN, whs.MSISDN, txnid);

            TransferRuleManagement.init(t1).deleteTransferRuleForSpecificGrade(merchPay, Constants.GOLD_SUBSCRIBER, Constants.SILVER_WHOLESALER);

            SfmResponse response1 = Transactions.init(t1).initiateMerchantPaymentForFailure(whs, subs, "5");
            response1.verifyMessage("transferule.not.defined");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void viewAndEditGradesOfCreatedTransferRule() throws Exception {

        ExtentTest t1 = pNode.createNode("MON_4825_2",
                "Verify that user is able to View and edit the grades of Transfer Rule");

        try {

            ServiceCharge cashIn = new ServiceCharge(Services.CASHIN, whs, subs, null, null, null, null);
            cashIn.setPayerGrade(Constants.SILVER_WHOLESALER);
            cashIn.setPayeeGrade(Constants.GOLD_SUBSCRIBER);

            TransferRuleManagement.init(t1).configureTransferRule(cashIn);
            Thread.sleep(1000);

            TransferRuleManagement.init(t1).deleteTransferRuleForSpecificGrade(cashIn, Constants.GRADE_ALL, Constants.GRADE_ALL);

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(opt);
            TransferRuleManagement.init(t1).viewTransferRule(cashIn);

            TransferRuleManagement.init(t1).modifySpecificTransferRule(cashIn);
            TransferRuleManagement.init(t1).approvePendingTRules();

            SfmResponse response = Transactions.init(t1).initiateCashIn(subs, whs, new BigDecimal(5));
            String txnid = response.TransactionId;
            response.verifyMessage("Transaction.success.message", "Cash In", "5", whs.MSISDN, subs.MSISDN, txnid);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tTeardown = pNode.createNode("Teardown", "After Class Methods for Current Suite");

        try {
            //Creating transfer rule with Grade All for Merchant Pay and CashIn
            ServiceCharge merchPay1 = new ServiceCharge(Services.MERCHANT_PAY, subs, whs, null, null, null, null);
            TransferRuleManagement.init(tTeardown).configureTransferRule(merchPay1);

            ServiceCharge cashIn1 = new ServiceCharge(Services.CASHIN, whs, subs, null, null, null, null);
            TransferRuleManagement.init(tTeardown).configureTransferRule(cashIn1);

        } catch (Exception e) {
            markTestAsFailure(e, tTeardown);
        }

        Assertion.finalizeSoftAsserts();
    }
}
