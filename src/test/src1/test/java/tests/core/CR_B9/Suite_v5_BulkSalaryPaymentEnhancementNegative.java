package tests.core.CR_B9;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Bulk Salary Payment Enhancement (Negative)User Story MON-3982
 * Author Name      : Jyoti Katiyar
 * Refactored       : rahul rana [09/24/2018]
 * Created Date     : 06/03/2018
 */

public class Suite_v5_BulkSalaryPaymentEnhancementNegative extends TestInit {

    private User entUser, sub;
    private OperatorUser bpAdmin;

    @BeforeClass(alwaysRun = true)
    public void preTest() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            bpAdmin = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            TransactionManagement.init(s1)
                    .makeSureChannelUserHasBalance(entUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }

    }

    /*To verify that appropriate error message is displayed when first name length is greater than 80*/
    @Test(enabled = true, groups = {FunctionalTag.v5_0})
    public void BulkEnterprisePaymentWithFirstNameLengthGreaterThanMaxLength_4418() throws Exception {
        ExtentTest t1 = pNode.createNode("MON-4418",
                "Bulk Enterprise payment with first name length greater than max length");

        try {

            User sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1",
                    "123456789a123456789B123456789c123456789D123456789e123456789f123456789g123456789h1",
                    sub.LastName, ""));

            //  user with same msisdn
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1",
                    sub.FirstName,
                    sub.LastName, ""));

            // user with unique msisdn, but first name is not correct
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub1.MSISDN, "1",
                    sub.FirstName,
                    sub1.LastName, ""));

            // user with Last name missing
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub1.MSISDN, "1",
                    sub1.FirstName,
                    "", ""));

            //Login as Enterprise
            Login.init(t1)
                    .login(entUser);

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            startNegativeTest();
            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, true, false);

            EnterpriseManagement.init(t1).verifyErrorLogs("ent.pay.error.beneficiary.firstname.notvalid",
                    "ent.pay.error.msisdn.not.unique",
                    "ent.pay.error.beneficiary.lastname.notvalid");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /*To verify that appropriate error code and message is displayed if national ID is entered does not
    match the registered user national ID in the system and national Id is selected as mandatory while uploading the bulk CSV file
      */
    @Test(enabled = true, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void InvalidNationalIdWhenMandatory_4275() throws Exception {
        ExtentTest t2 = pNode.createNode("MON-4275", "To verify that appropriate error code and message is displayed if national ID is entered does not\n" +
                "match the registered user national ID in the system and national Id is selected as mandatory while uploading the bulk CSV file");

        try {
            Login.init(t2)
                    .login(entUser);

            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1",
                    sub.FirstName,
                    sub.LastName,
                    "!234"));

            // Second Entry
            User sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub1.MSISDN, "1",
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t2)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            startNegativeTest();
            EnterpriseManagement.init(t2)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, true);

            EnterpriseManagement.init(t2).verifyErrorLogs("ent.pay.error.beneficiary.nationalId.notvalid");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();

    }

}
