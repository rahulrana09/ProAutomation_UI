package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Subscriber Modification
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/18/2018.
 */
public class Suite_P1_Subscriber_Modification extends TestInit {

    private User chUsr, sub;
    private ExtentTest P1_TC_592_P1_TC_596;

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        /*
        create a subscriber by channel user through API
        modify the subscriber
        modification approval should be successful
         */
        ExtentTest P1_TC_591 = pNode.createNode("P1_TC_591", "To verify that any valid user can modify the " +
                "subscriber details which is registered  through Channel user from USSD.");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub = new User(Constants.SUBSCRIBER);

            Transactions.init(P1_TC_591)
                    .SubscriberRegistrationByChannelUserWithKinDetails(sub);

            Transactions.init(P1_TC_591)
                    .subscriberAcquisition(sub);

            P1_TC_592_P1_TC_596 = pNode.createNode("P1_TC_592_P1_TC_596", "To Verify the modify approval should successful.");

            Login.init(P1_TC_592_P1_TC_596).login(chUsr);

            SubscriberManagement.init(P1_TC_592_P1_TC_596)
                    .modifySubscriber(chUsr, sub);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_591);
        }
    }

}
