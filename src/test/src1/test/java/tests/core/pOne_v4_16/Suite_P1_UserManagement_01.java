package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.userManagement.ApproveOperatorUser_pg1;
import framework.pageObjects.userManagement.ModifyApprovalOperator_Page;
import framework.pageObjects.userManagement.ViewSelfDetails_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: User Management Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/8/2018
 */

public class Suite_P1_UserManagement_01 extends TestInit {
    private OperatorUser optUsr;
    private OperatorUser bankUsr;

    @Test(priority = 1)
    public void Test_01() throws Exception {
        /*
            login as bank admin
            create a bank user
            reject the newly created user from approval
            validate the success message
         */
        ExtentTest TC176 = pNode.createNode("TC176", "To verify that Bank Admin can reject the initiated Bank User creation.");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU", Constants.BANK_ADMIN);
            Login.init(TC176).login(optUsr);

            bankUsr = new OperatorUser(Constants.BANK_USER);
            OperatorUserManagement.init(TC176).initiateOperatorUser(bankUsr);

            ApproveOperatorUser_pg1 ap1 = ApproveOperatorUser_pg1.init(TC176);

            ap1.Navigate();
            ap1.loginID_SetText(bankUsr.LoginId);
            ap1.clickSubmitPage1();

            ap1.clickSubmitPage2();
            ap1.clickReject();

            Assertion.verifyActionMessageContain("systemparty.message.rejected", "User Rejected", TC176);
        } catch (Exception e) {
            markTestAsFailure(e, TC176);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {
        /*
        login as bank admin
        modify an existing bank user
        validate initiations success message
        approve the modified user approval
        validate the success message
         */
        ExtentTest TC177 = pNode.createNode("TC177", "To verify that Bank Admin can initiate modification of an existing Bank User");
        try {

            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU", Constants.BANK_ADMIN);
            Login.init(TC177).login(optUsr);

            OperatorUser bankUsr = DataFactory.getOperatorUserListWithCategory(Constants.BANK_USER).get(0);

            bankUsr.setIdentificationNumber("77920564342289");
            bankUsr.setContactNum("7792056434");

            OperatorUserManagement.init(TC177)
                    .modifyOperatorUser(bankUsr);

            Assertion.verifyActionMessageContain("systemparty.message.updationapproval", "User Initiated For Approval", TC177);

            ExtentTest TC178 = pNode.createNode("TC178", "verify that Bank Admin can Approve modification of an existing Bank User");
            OperatorUserManagement.init(TC178).approveModifyOperatorUser(bankUsr, true);

            Assertion.verifyActionMessageContain("systemparty.message.updation", "User Updated Successfully", TC178);
        } catch (Exception e) {
            markTestAsFailure(e, TC177);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() throws Exception {
        /*
        login as bank admin
        modify an existing bank user
        reject the modified user approval
        validate the success message
         */
        ExtentTest TC179 = pNode.createNode("TC179", "To verify that Bank Admin can reject modification request of a Bank User");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU", Constants.BANK_ADMIN);
            Login.init(TC179).login(optUsr);

            OperatorUser bankUsr = DataFactory.getOperatorUsersWithAccess(Constants.BANK_USER).get(1);

            bankUsr.setIdentificationNumber(bankUsr.ExternalCode);
            bankUsr.setContactNum(bankUsr.ContactNum);

            OperatorUserManagement.init(TC179).modifyOperatorUser(bankUsr);

            ModifyApprovalOperator_Page page = ModifyApprovalOperator_Page.init(TC179);
            page.navigateToNALink();
            page.selectUserTypeByValue(bankUsr.CategoryCode);

            page.enterLoginID(bankUsr.LoginId);
            page.clickSubmitButton();
            page.selectUserFromList(bankUsr.MSISDN);
            page.clickFinalSubmitButton();
            page.clickRejectButton();

            Assertion.verifyActionMessageContain("systemparty.message.rejected", "Update Approval Rejected", TC179);
        } catch (Exception e) {
            markTestAsFailure(e, TC179);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4)
    public void Test_04() throws Exception {
        /*
        login as bank admin
        delete an existing bank user
        validate initiations success message
        approve the delete user approval
        validate the success message
         */
        ExtentTest TC180 = pNode.createNode("TC180", "To verify that Bank Admin can initiate deletion of an existing Bank User");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU", Constants.BANK_ADMIN);
            Login.init(TC180).login(optUsr);

            OperatorUser bankUsr = new OperatorUser(Constants.BANK_USER);
            OperatorUserManagement.init(TC180).initiateOperatorUser(bankUsr).approveOperatorUser(bankUsr);

            OperatorUserManagement.init(TC180)
                    .initModifyOrDeleteOperator(bankUsr, false);

            ExtentTest TC181 = pNode.createNode("TC181", " verify that Bank Admin can approve the initiate deletion request of an existing Bank User");

            OperatorUserManagement.init(TC181)
                    .approveDeletionOperatorUser(bankUsr, true);
        } catch (Exception e) {
            markTestAsFailure(e, TC180);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest TC182_TC166 = pNode.createNode("TC182_TC166",
                "To verify that Bank Admin/Super Admin/Network Admin/Channel Admin can view their self details .");
        /*
        login as Bank Admin/Super Admin/Network Admin/Channel Admin
        navigate to view self details
        validate first name, last name, msisdn and login id
         */
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU", Constants.BANK_ADMIN);
            Login.init(TC182_TC166).login(optUsr);

            ViewSelfDetails_Page1 page = ViewSelfDetails_Page1.init(TC182_TC166);
            page.navigateToLink();

            String actualMsisdn = page.getMSISDNLabelText();
            String actualLoginID = page.getWebLoginIDText();
            String actualFirstName = page.getFirstName();
            String actualLastName = page.getLastName();

            Assertion.verifyEqual(actualMsisdn, optUsr.MSISDN, "Verify MSISDN", TC182_TC166);
            Assertion.verifyEqual(actualLoginID, optUsr.LoginId, "Verify WEB Login ID", TC182_TC166);
            Assertion.verifyEqual(actualFirstName, optUsr.FirstName, "Verify First Name", TC182_TC166);
            Assertion.verifyEqual(actualLastName, optUsr.LastName, "Verify Last Name", TC182_TC166);
        } catch (Exception e) {
            markTestAsFailure(e, TC182_TC166);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, enabled = false)
    public void Test_06() throws Exception {
        /*
        login as super admin
        create a bank admin with a specific time to allow access to application
        login with the newly created bank admin outside of the mentioned time period and verify error message
         */
        ExtentTest TC183 = pNode.createNode("TC183", "To verify that Network/Bank Admin can not log in the Operator other than the defined time by the super admin");
        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            Login.init(TC183).loginAsSuperAdmin(sm);

            optUsr = new OperatorUser(Constants.BANK_ADMIN);
            OperatorUserManagement.init(TC183).initiateOperatorUserWithAllowedTimeAndIP(optUsr, "10:00", "10:01", " ");

            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            Login.init(TC183).loginAsSuperAdmin(sc);
            OperatorUserManagement.init(TC183).approveOperatorUser(optUsr);

            Login.init(TC183).openApplication();
            Login.init(TC183).setUserName(optUsr.LoginId);
            Login.init(TC183).setPassword("Com@246");
            Login.init(TC183).login();

            Assertion.verifyErrorMessageContain("loginevents.user.timingsnotallow", "Login not allowed due to Allow Days/Time Reasons", TC183);

        } catch (Exception e) {
            markTestAsFailure(e, TC183);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7, enabled = false)
    public void Test_7() {
        /*
        login as super admin maker
        create a bank admin with a specific Ip Address to allow access to application
        login as super admin checker
        approve the newly created bank/network admin
        login with the newly created bank admin with a different IP address and verify error message
         */
        ExtentTest TC158_TC160 = pNode.createNode("TC158_TC160",
                "To verify that Network/Bank Admin can not log in the Operator other than the defined time by the super admin");
        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            Login.init(TC158_TC160).loginAsSuperAdmin(sm);

            optUsr = new OperatorUser(Constants.BANK_ADMIN);
            OperatorUserManagement.init(TC158_TC160).initiateOperatorUserWithAllowedTimeAndIP(optUsr, " ", " ", "172.19.3.219");

            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            Login.init(TC158_TC160).loginAsSuperAdmin(sc);
            OperatorUserManagement.init(TC158_TC160).approveOperatorUser(optUsr);

            Login.init(TC158_TC160).openApplication();
            Login.init(TC158_TC160).setUserName(optUsr.LoginId);
            Login.init(TC158_TC160).setPassword("Com@246");
            Login.init(TC158_TC160).login();

            Assertion.verifyErrorMessageContain("loginevents.user.invalidrequesturl", "Invalid IP address", TC158_TC160);

        } catch (Exception e) {
            markTestAsFailure(e, TC158_TC160);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8)
    public void Test_08() throws Exception {
        /*
        Login as super admin maker
        add bank/network admin
        validate initiation success message
        login as super admin checker
        reject the newly created bank/network admin
        validate reject success message
         */
        ExtentTest TC159 = pNode.createNode("TC159",
                "To verify that super admin can initiate add request of Network admin/ Bank admin creation");
        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            Login.init(TC159).loginAsSuperAdmin(sm);

            optUsr = new OperatorUser(Constants.BANK_ADMIN);
            OperatorUserManagement.init(TC159)
                    .initiateOperatorUser(optUsr);

            ExtentTest TC161 = pNode.createNode("TC161", "To verify that super admin can reject the initiated Network/Bank Admin creation.");

            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            Login.init(TC161).loginAsSuperAdmin(sc);

            ApproveOperatorUser_pg1 ap1 = ApproveOperatorUser_pg1.init(TC161);
            ap1.Navigate();
            ap1.loginID_SetText(optUsr.LoginId);
            ap1.clickSubmitPage1();
            ap1.clickSubmitPage2();
            ap1.clickReject();

            Assertion.verifyActionMessageContain("systemparty.message.rejected", "Approval Rejected", TC161);
        } catch (Exception e) {
            markTestAsFailure(e, TC159);
        }
    }

    @Test(priority = 9)
    public void Test_09() throws Exception {
        /*
        Login as super admin maker
        modify an existing bank/network admin
        validate update initiation success message
        login as super admin checker
        reject the modified approval created bank/network admin
        validate reject success message
        And
        approve the modified approval created bank/network admin
        validate reject success message
         */
        ExtentTest TC162_TC163 = pNode.createNode("TC162_TC163", "To verify that super admin can reject and approve modification request of an Network/Bank Admin");
        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            Login.init(TC162_TC163).loginAsSuperAdmin(sm);

            optUsr = DataFactory.getOperatorUserListWithCategory(Constants.BANK_ADMIN).get(0);

            optUsr.setIdentificationNumber("77920563511730");
            optUsr.setContactNum("7792056351");
            OperatorUserManagement.init(TC162_TC163).initModifyOrDeleteOperator(optUsr, true);

            ExtentTest TC163 = pNode.createNode("TC163", "To verify that super admin can reject modification request of an Network/Bank Admin");

            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            Login.init(TC163).loginAsSuperAdmin(sc);

            OperatorUserManagement.init(TC163).approveModifyOperatorUser(optUsr, false);

            Login.init(TC162_TC163).loginAsSuperAdmin(sm);

            OperatorUserManagement.init(TC162_TC163).initModifyOrDeleteOperator(optUsr, true);

            ExtentTest TC162 = pNode.createNode("TC162", "To verify that super admin can approve modification request of an Network/Bank Admin");

            Login.init(TC162).loginAsSuperAdmin(sc);

            OperatorUserManagement.init(TC162).approveModifyOperatorUser(optUsr, true);
        } catch (Exception e) {
            markTestAsFailure(e, TC162_TC163);
        }
    }

    @Test(priority = 10)
    public void Test_10() throws Exception {
        ExtentTest TC164_TC165 = pNode.createNode("TC164_TC165", "To verify that super admin can initiate and approve deletion of an existing Network/Bank Admin");
        /*
        Login as super admin maker
        delete an existing bank/network admin
        validate delete initiation success message
        login as super admin checker
        approve the delete approval created bank/network admin
        validate reject success message
         */
        try {

            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            Login.init(TC164_TC165).loginAsSuperAdmin(sm);

            optUsr = new OperatorUser(Constants.BANK_ADMIN);

            OperatorUserManagement.init(TC164_TC165).initiateAndApproveOptUser(optUsr);

            ExtentTest TC162 = pNode.createNode("TC162", "To verify that super admin can initiate deletion of an existing Network/Bank Admin");
            Login.init(TC162).loginAsSuperAdmin(sm);

            OperatorUserManagement.init(TC162).initModifyOrDeleteOperator(optUsr, false);

            ExtentTest TC163 = pNode.createNode("TC163", "To verify that super admin can approve deletion of an existing Network/Bank Admin");

            SuperAdmin sc = DataFactory.getSuperAdminWithAccess("PTY_ASUA");
            Login.init(TC163).loginAsSuperAdmin(sc);

            OperatorUserManagement.init(TC163).approveDeletionOperatorUser(optUsr, true);
        } catch (Exception e) {
            markTestAsFailure(e, TC164_TC165);
        }
    }

    @Test(priority = 11)
    public void Test_11() throws Exception {
        /*
        login as operator user
        create a cce
        reject from approval
        validate success message
        create a cce
        approve from approval
        validate success message
         */
        ExtentTest TC168 = pNode.createNode("TC168", "To verify that Network admin can initiate and reject Channel Admin/CCE.");
        Login.init(TC168).login(optUsr);
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU");
            OperatorUser cce = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
            OperatorUserManagement.init(TC168).initiateOperatorUser(cce);
            ApproveOperatorUser_pg1 ap1 = ApproveOperatorUser_pg1.init(TC168);
            ap1.Navigate();
            ap1.loginID_SetText(cce.LoginId);
            ap1.clickSubmitPage1();
            ap1.clickSubmitPage2();
            ap1.clickReject();

            Assertion.verifyActionMessageContain("systemparty.message.rejected", "Approval Rejected", TC168);

            ExtentTest TC167 = pNode.createNode("TC167", "To verify that Network admin can initiate and approve Channel Admin/CCE.");

            Login.init(TC167).login(optUsr);
            OperatorUserManagement.init(TC167).initiateOperatorUser(cce).approveOperatorUser(cce);
        } catch (Exception e) {
            markTestAsFailure(e, TC168);
        }
    }

    @Test(priority = 12)
    public void Test_12() throws Exception {
        /*
        login using operator user
        modify an existing CCE with MSISDN and Contact Num
        validate update initiate success message
        navigate to modify approval page
        reject the modification approval
        validate success message
        again modify an existing CCE with MSISDN and Contact Num
        navigate to modify approval page
        approve the modification approval
        validate success message
         */
        optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU");

        OperatorUser cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);

        ExtentTest TC169 = pNode.createNode("TC169", "To verify that Network Admin can initiate modification request of an existing Channel admin/CCE");
        Login.init(TC169).login(optUsr);
        // update the CCE object
        cce.setIdentificationNumber("77920563532479");
        cce.setContactNum("7792056353");
        OperatorUserManagement.init(TC169).initModifyOrDeleteOperator(cce, true);

        ExtentTest TC170 = pNode.createNode("TC170", "To verify that Network Admin can reject modification request of an existing Channel admin/CCE");

        OperatorUserManagement.init(TC170).approveModifyOperatorUser(cce, false);

        ExtentTest TC171 = pNode.createNode("TC171", "To verify that Network Admin can approve modification request of an existing Channel admin/CCE");

        OperatorUserManagement.init(TC171).initModifyOrDeleteOperator(cce, true);
        OperatorUserManagement.init(TC171).approveModifyOperatorUser(cce, true);
    }

    @Test(priority = 13)
    public void Test_13() throws Exception {
        /*
        Login as operator user
        create a CCE
        initiate delete the created CCE
        validate initiation success message
        approve the delete initiation
        validate success message
         */
        optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU");

        ExtentTest TC172 = pNode.createNode("TC172", "To verify that Network admin can initiate deletion request of an existing channel admin/CCE");
        Login.init(TC172).login(optUsr);

        OperatorUser cce = new OperatorUser(Constants.CUSTOMER_CARE_EXE);
        OperatorUserManagement.init(TC172).initiateOperatorUser(cce).approveOperatorUser(cce);
        OperatorUserManagement.init(TC172).initModifyOrDeleteOperator(cce, false);

        ExtentTest TC173 = pNode.createNode("TC173", "To verify that Network admin can approve initiate deletion request of an existing channel admin/CCE");
        OperatorUserManagement.init(TC173).approveDeletionOperatorUser(cce, true);

    }
}
