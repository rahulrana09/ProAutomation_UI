package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.serviceCharge.ServiceChargeDeleteModify_Pg1;
import framework.pageObjects.serviceCharge.ServiceChargeModify_Pg2;
import framework.pageObjects.serviceCharge.ServiceChargeModify_Pg3;
import framework.pageObjects.serviceCharge.ServiceChargeModify_Pg4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Modify Service Charge
 * Author Name      : Gurudatta Praharaj; Refactored: rahul rana / 07/16/2018
 * Date             : 6/27/2018.
 */
public class Suite_P1_ModifyServiceCharge_01 extends TestInit {

    private ServiceCharge sCharge;
    private User usrSubs, usrCh;
    private OperatorUser optModSCharge, usrSChargeView;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest sTest = pNode.createNode("Setup", "Setup specific for this suite");
        try {
            optModSCharge = DataFactory.getOperatorUserWithAccess("SVC_MOD");
            usrSChargeView = DataFactory.getOperatorUserWithAccess("SVC_VIEW");
            usrSubs = DataFactory.getChannelUserWithCategoryAndGradeCode(Constants.SUBSCRIBER, Constants.GOLD_SUBSCRIBER);
            usrCh = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, usrSubs, usrCh, null, null, null, null);

            // Make sure that the service charge is configured
            ServiceChargeManagement.init(sTest)
                    .configureServiceCharge(sCharge);
        } catch (Exception e) {
            markTestAsFailure(e, sTest);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_425() {
        ExtentTest t1 = pNode.createNode("P1_TC_425",
                "Verify that User with modify permission can initiate Service charge modification");
        try {
            Login.init(t1)
                    .login(optModSCharge);

            ServiceChargeDeleteModify_Pg1 page = ServiceChargeDeleteModify_Pg1.init(t1);

            page.navModifyDeleteServiceCharge();

            Assertion.verifyEqual(fl.elementIsDisplayed(page.getSelectServicCharge()), true,
                    "Verify that option to select an existing Service charge and initiate Modify s available", t1, true);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.getBtnUpdate()), true,
                    "Verify that option to submit the Service charge for Modify initiate is shown", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_426() {
        ExtentTest t1 = pNode.createNode("P1_TC_426",
                "Verify Modify Service charge page for UI fields and available actions");
        try {
            Login.init(t1)
                    .login(optModSCharge);

            ServiceChargeDeleteModify_Pg1.init(t1)
                    .navModifyDeleteServiceCharge()
                    .selectServiceCharge(sCharge.ServiceChargeName)
                    .clickUpdate();

            // verify that the option for editing the tax and services is avaialble
            ServiceChargeModify_Pg2 page2 = ServiceChargeModify_Pg2.init(t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page2.getImgEditCommission()), true, "Verify that the option for adding commission slab is available", t1, true);

            // click on the image and Verify that the user can now edit the fields
            ServiceChargeModify_Pg3 page3 = page2.clickEditCommissionSlabs();
            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getSelCreditEntity()), true, "Verify that the Option to Select Credit Entity is enabled", t1, true);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getSelPayingEntity()), true, "Verify that the Option to Select Paying Entity is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getTxtMinServiceCharge()), true, "Verify that the Option to set Minimum Service charge amount is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getTxtMaxServiceCharge()), true, "Verify that the Option to set max service charge amount  is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getTxtMinPercentageCommission()), true, "Verify that the Option to set min commission percentage is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getTxtMaxPercentageCommission()), true, "Verify that the Option to set Max Commission percentage is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getBtnModify()), true, "Verify that the Option to modify the changes is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page3.getBtnCancel()), true, "Verify that the Option to cancel the midified values is available", t1);

            ServiceChargeModify_Pg4 page4 = page3.clickOnModify();
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1, true);
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1);
            Assertion.verifyEqual(fl.elementIsEnabled(page4.getTxtCommissionTaxPerList()), true, "Verify that the Option to set Commission Tax per list is enabled", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_427() {
        ExtentTest t1 = pNode.createNode("P1_TC_427",
                "Verify that User is asked for confirmation once modifying and submitting an existing service charge");
        try {
            Login.init(t1)
                    .login(optModSCharge);

            ServiceChargeModify_Pg4 page4 = ServiceChargeDeleteModify_Pg1.init(t1)
                    .navModifyDeleteServiceCharge()
                    .selectServiceCharge(sCharge.ServiceChargeName)
                    .clickUpdate()
                    .clickEditCommissionSlabs()
                    .clickOnModify()
                    .clickModify();

            Assertion.verifyEqual(fl.elementIsDisplayed(page4.getBtnSaveModify()), true,
                    "Verify that option to save the modification is available", t1, true);

            page4.clickSaveModify();

            Assertion.verifyEqual(fl.elementIsDisplayed(page4.getBtnConfirmModify()), true,
                    "Verify that User is asked for confirming the modification to the existing service charge", t1, true);

            page4.clickConfirmModify();
            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyActionMessageContain("servicechargeprofile.message.initiatesuccessmodifymessage",
                    "Verify that Service charge is successfully modify initiated and need approval", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, dependsOnMethods = "P1_TC_427", groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT})
    public void P1_TC_428() {
        ExtentTest t1 = pNode.createNode("P1_TC_428",
                "Verify that a modified service charge creates a new version. Old version is also available");
        try {
            Login.init(t1)
                    .login(optModSCharge);

            ServiceChargeManagement.init(t1)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);

            // View Service Charge verify that modification are now shown as Modifications are Approved
            Login.init(t1).login(usrSChargeView);
            ServiceChargeManagement.init(t1)
                    .viewServiceChargesLatestVersion(sCharge, "1");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
