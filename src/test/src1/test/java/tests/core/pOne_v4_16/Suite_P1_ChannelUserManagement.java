package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.channelUserManagement.HierarchyBranchMovementPage;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Channel User Registration
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/22/2018.
 */
public class Suite_P1_ChannelUserManagement extends TestInit {

    private User newMerchant, merchant, chUsr, enpUsr;
    private OperatorUser optUsr1, optUsr2, optUsr3;
    private ExtentTest P1_TC_501;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        try {
            newMerchant = new User(Constants.MERCHANT);
            enpUsr = new User(Constants.ENTERPRISE);
            merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUsr1 = DataFactory.getOperatorUserWithAccess("HRY_MOV");
            optUsr2 = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            optUsr3 = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        ExtentTest P1_TC_159 = pNode.createNode("P1_TC_159", "To verify the channel member Hierarchy branch " +
                "movement cant update invalid details are entered.");
        /*
        login as operator user
        navigate to channel user management > Hierachy Branch Movement
        enter invalid MSISDN & validate error message
        enter a valid MSISDN and click next button
        without entering any filed click submit button
        validate error messages
         */
        try {
            Login.init(P1_TC_159).login(optUsr1);

            HierarchyBranchMovementPage page = new HierarchyBranchMovementPage(P1_TC_159);
            page.navHierarchyMovement();
            page.setMsisdnforBranchMovement(newMerchant.MSISDN);
            page.submitforBranchMovement();

            Assertion.verifyErrorMessageContain("channeluser.hierarchy.noOwner", "Invalid MSISDN", P1_TC_159);

            page.setMsisdnforBranchMovement(merchant.MSISDN);
            page.submitforBranchMovement();
            page.submitBranchMovementPageOne();

            Assertion.verifyErrorMessageContain("system.owner.name", "enter owner name", P1_TC_159);
            Assertion.verifyErrorMessageContain("system.parentcategory", "enter category", P1_TC_159);
            Assertion.verifyErrorMessageContain("system.parent.name", "select parent", P1_TC_159);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_159);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.P1CORE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.P1CORE})
    public void Test_02() throws Exception {
        ExtentTest P1_TC_039 = pNode.createNode("P1_TC_039", "To Verify that after successful creation " +
                "of channel user Each Channel User(Merchant Domain) when created will be assigned an " +
                "Merchant code (or Agent code)");

        P1_TC_039.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.P1CORE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.P1CORE);
        /*
        create a channel user(merchant)
        validate whether he has been assigned with an agent code/merchant code or not
         */

        SystemPreferenceManagement.init(P1_TC_039)
                .updateSystemPreference("IS_RANDOM_PASS_ALLOW", "Y");

        try {
            AppConfig.isRandomPasswordAllowed = true;
            ChannelUserManagement.init(P1_TC_039)
                    .addChannelUser(newMerchant);

            ChannelUserManagement.init(P1_TC_039)
                    .approveChannelUser(newMerchant);

            Thread.sleep(Constants.MAX_WAIT_TIME);
            String agentCode = MobiquityGUIQueries.getAgentCode(newMerchant.MSISDN);

            boolean status = false;
            if (agentCode != null)
                status = true;

            Assertion.verifyEqual(status, true, "" + agentCode + " :Agent Code is assigned to new Agent", P1_TC_039);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_039);
        } finally {
            SystemPreferenceManagement.init(P1_TC_039)
                    .updateSystemPreference("IS_RANDOM_PASS_ALLOW", "N");
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.P1CORE, FunctionalTag.ECONET_UAT_5_0})
    //depends on Test_02
    public void Test_03() throws Exception {
        ExtentTest P1_TC_500 = pNode.createNode("P1_TC_500", "To verify that while creating channel user if " +
                "preference IS_RANDOM_PASS_ALLOW is set as Y then password is automatically generated.");
        P1_TC_500.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.P1CORE, FunctionalTag.ECONET_UAT_5_0);
        /*
        set preference is_random_password_allowed to Y
        create a channel user(merchant)
        validate whether default password(0000) has been sent to newly created user by sms
        set preference back to N
         */

        try {
            SMSReader.init(P1_TC_500)
                    .verifyNotificationContain(newMerchant.MSISDN, "emp.default.password");
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_500);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.P1CORE})
    public void Test_04() {
        P1_TC_501 = pNode.createNode("P1_TC_501", "To verify that while creating channel user " +
                "if preference IS_RANDOM_PASS_ALLOW is set as N then password field will be mandatory");
        /*
        set preference is_random_password_allowed to N
        check while creating channel user
        password and confirm password will enable and mandatory
         */
        try {
            Login.init(P1_TC_501).login(optUsr2);

            AddChannelUser_pg1.init(P1_TC_501)
                    .navAddChannelUser();

            Thread.sleep(Constants.MAX_WAIT_TIME);

            boolean password = Utils.checkElementPresent("confirm2_confirmAddChannelUser_webPassword", Constants.FIND_ELEMENT_BY_ID);
            boolean confirmPassword = Utils.checkElementPresent("confirm2_confirmAddChannelUser_confWebPassword", Constants.FIND_ELEMENT_BY_ID);

            Assertion.verifyEqual(password, true, "Password Field Available", P1_TC_501);
            Assertion.verifyEqual(confirmPassword, true, "Confirm Password Field Available", P1_TC_501, true);

            AddChannelUser_pg1.init(P1_TC_501).clickNext();

            Assertion.verifyErrorMessageContain("channeluser.error.password.required",
                    "Password Is Mandatory", P1_TC_501);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_501);
        }
    }

    @Test(enabled = false, priority = 5, groups = {FunctionalTag.P1CORE})
    public void Test_05() {
        ExtentTest P1_TC_503 = pNode.createNode("P1_TC_503", "To verify that when creating Enterprise " +
                "the bank screen will not show up. Bank Association screen will not be displayed.");
        /*
        Descoped after 5.0: we can assign bank to enterprise user

        login as operator user
        create a channel user(enterprise)
        validate bank association page should not display after associating wallet
         */

        try {
            Login.init(P1_TC_503).login(optUsr2);
            ChannelUserManagement.init(P1_TC_503)
                    .initiateChannelUser(enpUsr)
                    .assignHierarchy(enpUsr)
                    .assignWebGroupRole(enpUsr);

            assignEnterprisePreference();
            ChannelUserManagement.init(P1_TC_503)
                    .setEnterpriseCategoryWalletCombinationforNormal();

            CommonUserManagement.init(P1_TC_503)
                    .mapDefaultWalletPreferences(enpUsr);

            startNegativeTestWithoutConfirm();
            ChannelUserManagement.init(P1_TC_503)
                    .setEnterprisePreferenceforUnregisteredPayee(enpUsr);

            Thread.sleep(Constants.MAX_WAIT_TIME);

            boolean bankField = Utils.checkElementPresent("bankCounterList[0].paymentTypeSelected", Constants.FIND_ELEMENT_BY_NAME);

            Assertion.verifyEqual(bankField, false, "Bank Field Is Not Available", P1_TC_503, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_503);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE})
    public void Test_06() throws Exception {
        ExtentTest P1_TC_315 = pNode.createNode("P1_TC_315", "To verify that suspend channel user is not successful" +
                " if initiated administrative requests is in pending for approval (modification/deletion/activation).");

        /*
            Login as operator user
            initiate modification of a channel user
            try to suspend the channel user
            validate error message
         */

        try {

            Login.init(P1_TC_315).login(optUsr2);
            User usr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(P1_TC_315)
                    .createChannelUserDefaultMapping(usr, false);


            Login.init(P1_TC_315).login(optUsr2);
            ChannelUserManagement.init(P1_TC_315)
                    .initiateChannelUserModification(usr)
                    .completeChannelUserModification();

            ConfigInput.isAssert = false;
            ChannelUserManagement.init(P1_TC_315)
                    .initiateSuspendChannelUser(usr);

            Assertion.verifyErrorMessageContain("channeluser.request.not.allowed.already.modification.initiated",
                    "User is in modified initiated state", P1_TC_315);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_315);
        }
    }

    private void assignEnterprisePreference() {
        driver.findElement(By.id("enterpriseCategoryWallet-1")).click();
        driver.findElement(By.id("confirm2_getEnterpriseDepositCategory_button_next")).click();
    }
}
