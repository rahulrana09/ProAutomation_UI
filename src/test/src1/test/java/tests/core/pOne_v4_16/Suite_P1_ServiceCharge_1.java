package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that valid user can initiate modification of an existing Channel user
 * Author Name      : Nirupama mk
 * Created Date     : 27/02/2018
 */

public class Suite_P1_ServiceCharge_1 extends TestInit {
    private OperatorUser optAdd, approver, optMod, optView;
    private User subs, chUser;
    private ServiceCharge sCharge;

    @BeforeClass(alwaysRun = true)
    public void beforeTest() throws Exception {
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        optAdd = DataFactory.getOperatorUserWithAccess("SVC_ADD");
        optMod = DataFactory.getOperatorUserWithAccess("SVC_MOD");
        approver = DataFactory.getOperatorUserWithAccess("SVC_APP");
        optView = DataFactory.getOperatorUserWithAccess("SVC_VIEW");
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_421_420_429_430() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_421", " To verify that Network Admin should be able to set a new\n" +
                " service charge and commission Profile for a particular service.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);
        try {

            sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subs, chUser,
                    null, null, null, null);

            Login.init(t1).login(optAdd);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(sCharge);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            ExtentTest t2 = pNode.createNode("P1_TC_420", " To verify that Network Admin should be able to set a new\n" +
                    " service charge and commission Profile for a particular service.");

            Login.init(t2).login(optMod);

            ServiceChargeManagement.init(t2).modifyServiceChargeGeneralInfo(sCharge);

            ExtentTest t4 = pNode.createNode("TC098", " To verify that Network Admin should be able to approve\n" +
                    " service charge and commission Profile for a particular service, Total number of last days entered should be 999");

            Login.init(t2).login(approver);
            ServiceChargeManagement.init(t4)
                    .approveServiceCharge(sCharge, Constants.SERVICE_CHARGE_APPROVE_MODIFY);


            ExtentTest t5 = pNode.createNode("P1_TC_429", " Service charge>view service charge\n" +
                    "To verify network admin is able to view service charge.");

            Login.init(t5).login(optView);
            ServiceChargeManagement.init(t5).viewServiceChargesLatestVersion(sCharge, "999");

            Login.init(t1).loginAsOperatorUserWithRole(Roles.SUSPEND_RESUME_SERVICE_CHARGE);
            ExtentTest t6 = pNode.createNode("P1_TC_430", " Service charge>suspend/resume service charge\n" +
                    "To verify network admin is able to suspend/resume service charge.");
            ServiceChargeManagement.init(t6).suspendServiceCharge(sCharge);
            ServiceChargeManagement.init(t6).resumeServiceCharge(sCharge);

            ExtentTest t7 = pNode.createNode("P1_TC_431_A", "To verify network admin is able to delete service charge.");
            ServiceChargeManagement.init(t7).deleteServiceCharge(sCharge);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
