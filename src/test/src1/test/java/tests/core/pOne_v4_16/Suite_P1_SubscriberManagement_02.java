package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.billerManagement.SubsBillerAssociation_pg1;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ApprovalSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify various Fields and Buttons in Subscriber Management (12-P1 Cases)
 * Author Name      : Jyoti Katiyar
 * Created Date     : 09/02/2018
 */
public class Suite_P1_SubscriberManagement_02 extends TestInit {

    private User chAddSubs, chApproveSubs, sub02_03_04_05, sub06_07_08, chModifySubs, chModifySubsApprove, sub09, delSubs;
    private FunctionLibrary fl;
    private CurrencyProvider defProvider;
    private Wallet defWallet;
    private OperatorUser optBillReg, optSsuspendResumeSubs;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Get Required Base Set Users. " +
                "Map wallet preference for Subscriber. " +
                "Make sure ACQ Fee service charge is configured. " +
                "Make sure Account Closure Service Charge is configured. " +
                "Make Sure that Channel User is having sufficient Funds.");
        try {
            defProvider = DataFactory.getDefaultProvider();
            defWallet = DataFactory.getDefaultWallet();
            OperatorUser optuser = new OperatorUser(Constants.OPERATOR);
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
            chModifySubs = DataFactory.getChannelUserWithAccess("SUBSMOD");
            chModifySubsApprove = DataFactory.getChannelUserWithAccess("SUBSMODAP");
            optBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            optSsuspendResumeSubs = DataFactory.getOperatorUserWithAccess("SR_USR");
            delSubs = DataFactory.getChannelUserWithAccess("SUBSDEL");

            User subSetup = new User(Constants.SUBSCRIBER);

            // Map Wallet preference for subscriber needed in Case of Creating SUBS via USSD
            /*CurrencyProviderMapping.init(setup)
                    .mapPrimaryWalletPreference(subSetup.CategoryCode, subSetup.GradeName);*/

            //settings service charge for acq fee
            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subSetup, optuser, null, null, null, null);

            // Configure the Service Charge for acq fee
            ServiceChargeManagement.init(setup)
                    .configureServiceCharge(sCharge);

            //settings service charge for Account Closure
            ServiceCharge accountClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE, subSetup, optuser, null, null, null, null);

            // Configure the Service Charge for Account Closure
            ServiceChargeManagement.init(setup)
                    .configureServiceCharge(accountClosure);

            //O2C Transaction
            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(chAddSubs);


            /*Its in development stage so commenting for time being

            //settings service charge cash in
           ServiceCharge cashIN = new ServiceCharge(Services.CASHIN, whsUser, sub, null, null, null, null);

            // Configure the Service Charge
           ServiceChargeManagement.init(setup)
                   .configureServiceCharge(cashIN);

                   */
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    /*to do field validation using data provider....no of elements are diff in both pages that we hv to compare*/
    //@Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_036", "Add Subscriber Approval:Verify details on Add Subscriber " +
                "Approval screen (View All Data link).Field name should be same on both Add subscriber screen & View All data screen");

        try {


            if (AppConfig.isSubsMakerCheckerAllowed) {
                Login.init(t1).login(chAddSubs);

                //creating subscriber
                User sub01 = new User(Constants.SUBSCRIBER);

                //navigate to add subscriber page

                List<WebElement> allLabels = AddSubscriber_Page1.init(t1)
                        .navAddSubscriber()
                        .getAllLabels("AddSubscriber_Page1");

                for (int i = 0; i < allLabels.size(); i++) {
                    t1.info("LABELS FROM SUBSCRIBER CREATION PAGE");
                    t1.info(allLabels.get(i).getText());
                    // TODO verification is missing [remark by rahul]
                }

                // Add the Subscriber
                SubscriberManagement.init(t1)
                        .createDefaultSubscriberUsingAPI(sub01);

                // Navigate to Subscriber Approval Page and get all the Labels
                Login.init(t1).login(chApproveSubs);

                List<WebElement> allLabels2 = ApprovalSubscriber_Page1.init(t1)
                        .navSubscriberApproval()
                        .viewUserApprovalDetails(sub01.MSISDN)
                        .getAllLabels("Add Subscriber Approval View All");
                t1.info("matched");
            } else {
                t1.skip("Test could not be continued as preference isSubsMakerCheckerAllowed is set to False");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("P1_TC_037", "To verify that after successful registration of subscriber," +
                "subscriber MSISDN and subscriber details should be present in database.");

        //login as Channel user
        try {
            //creating subscriber
            sub02_03_04_05 = new User(Constants.SUBSCRIBER);

            //navigate to add subscriber page
            SubscriberManagement.init(t2)
                    .createSubscriberDefaultMapping(sub02_03_04_05, true, false);

            //dB connection and checking created MSISDN exists in dB
            Assertion.assertEqual(MobiquityGUIQueries.checkMsisdnExistMobiquity(sub02_03_04_05.MSISDN), true, "Verify Subscriber registered in db", t2);
            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
    }

    //@Test(priority = 3) not part of P1
    public void P1_TC_133() throws Exception {
        ExtentTest t3 = pNode.createNode("P1_TC_133", "To verify that Channel user (whsUser/Retailer)" +
                " should be able to approve modification request of a subscriber.");

        try {
            String newIdentificationNumber = sub02_03_04_05.FirstName + DataFactory.getRandomString(4);
            sub02_03_04_05.IDNum = newIdentificationNumber;
            Login.init(t3).login(chModifySubs);

            ModifySubscriber_page1.init(t3)
                    .navSubscriberModification()
                    .setMSISDN(sub02_03_04_05.MSISDN)
                    .clickOnSubmitPg1()
                    .externalCode_SetText(sub02_03_04_05.IDNum)
                    .clickOnNextPg2();
            ModifySubscriber_page3.init(t3)
                    .nextPage()
                    .clickSubmitPg4()
                    .clickOnConfirmPg4()
                    .clickFinalConfirm();

            Assertion.verifyActionMessageContain("subs.modify.message.initiateBy",
                    "Verify Subscriber User modification", t3, chModifySubs.FirstName, chModifySubs.LastName);

            SubscriberManagement.init(t3)
                    .modifyApprovalSubs(sub02_03_04_05);
            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
    }

    @Test(priority = 4, dependsOnMethods = "Test_02", groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_139() throws Exception {
        ExtentTest t4 = pNode.createNode("P1_TC_139",
                "To verify that Channel user (whsUser/Retailer) should not be able" +
                        " to delete initiate a subscriber who is barred in the system and not having balance in the wallet");

        //login as Channel user
        try {
            //Bar Subscriber User
            startNegativeTest();
            ChannelUserManagement.init(t4)
                    .barChannelUser(sub02_03_04_05, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);

            Assertion.verifyActionMessageContain("user.bar.success.sender", "User Barred As Sender", t4);

            SubscriberManagement.init(t4)
                    .startNegativeTest()
                    .setIsConfirmActionRequired(false)
                    .deleteSubscriber(sub02_03_04_05, defProvider.ProviderId);

            Assertion.verifyErrorMessageContain("subs.blacklist.msg.oneorbothexist",
                    "Subscriber Cannot be deleted as it's barred", t4);
            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_137() throws Exception {
        ExtentTest t5 = pNode.createNode("P1_TC_137", "To verify that Channel user (whsUser/Retailer) should not be able " +
                "to delete initiate a subscriber who is barred in the system and having balance in the wallet");
        //login as Channel user
        User sub = null;
        try {

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
//            SubscriberManagement.init(t5).createDefaultSubscriberUsingAPI(sub);
//            Transactions.init(t5).initiateCashIn(sub,chApproveSubs,new BigDecimal("10"));

            Login.init(t5).login(chAddSubs);

            SubscriberManagement.init(t5).barSubscriber(sub, Constants.BAR_AS_BOTH);

            startNegativeTest();
            SubscriberManagement.init(t5)
                    .initiateDeleteSubscriber(sub, chAddSubs, defProvider.ProviderId);

//            SubscriberManagement.init(t5)
//                    .startNegativeTest()
//                    .setIsConfirmActionRequired(false) // as the error is expected just after clicking on Submit
//                    .deleteSubscriber(sub02_03_04_05, defProvider.ProviderId);

            Assertion.verifyErrorMessageContain("subs.blacklist.msg.oneorbothexist",
                    "Subscriber Cannot be deleted As it has balance and is Barred", t5);
            Assertion.finalizeSoftAsserts();
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            SubscriberManagement.init(t5).unBarSubscriber(sub);

        }
    }


    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_141() {
        ExtentTest t6 = pNode.createNode("P1_TC_141", "Account Closure:To verify that a Channel user (whsUser/Retailer) " +
                "can not initiate the account closure request for the customer if subscriber account balance is " +
                "less than the commission & service charge that is being paid by subscriber for this service");
        try {
            //creating subscriber
            sub06_07_08 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t6)
                    .createDefaultSubscriberUsingAPI(sub06_07_08);

            startNegativeTest();
            //Delete initiate
            SubscriberManagement.init(t6)
                    .deleteSubscriber(sub06_07_08, defProvider.ProviderId);

            Assertion.verifyErrorMessageContain("subs.transaction.fail.insufficient.funds",
                    "Subscriber Cannot be deleted As User doesn't Have Sufficient Fund", t6);

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }

    }

    @Test(priority = 7, dependsOnMethods = "P1_TC_141", groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_140() throws Exception {
        ExtentTest t7 = pNode.createNode("P1_TC_140", "Delete Subscriber-To verify that Channel user (whsUser/Retailer) should" +
                " not be able to delete initiate a subscriber who is suspended in the system and not having balance in " +
                "the wallet");
        try {

            Login.init(t7).login(optSsuspendResumeSubs);
            // Suspend the Subscriber
            SubscriberManagement.init(t7)
                    .suspendSubscriber(sub06_07_08, Constants.USER_TYPE_SUBS);

            SubscriberManagement.init(t7)
                    .startNegativeTest()
                    .setIsConfirmActionRequired(false)
                    .deleteSubscriber(sub06_07_08, defProvider.ProviderId);

            Assertion.verifyErrorMessageContain("subs.message.notinsystem",
                    "Subscriber Cannot be deleted As it Suspended", t7);
            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t7);
        } finally {
            ConfigInput.isAssert = true; // reset the flag for further assertions
            Login.init(t7).login(optSsuspendResumeSubs);
            SubscriberManagement.init(t7)
                    .resumeSubscriber(sub06_07_08, Constants.USER_TYPE_SUBS);
        }
    }

    @Test(priority = 8, dependsOnMethods = "P1_TC_141", groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_142() throws Exception {
        ExtentTest t8 = pNode.createNode("P1_TC_142",
                "Account Closure:To verify that subscriber can not perform any" +
                        " transaction if account closure service for the subscriber is in initiated state");
        //login as Channel user
        //make account closure 2 step
        SystemPreferenceManagement.init(t8)
                .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "Y");

        Transactions.init(t8)
                .initiateCashIn(sub06_07_08, delSubs, "10");
        try {
            SubscriberManagement.init(t8)
                    .initiateDeleteSubscriber(sub06_07_08, delSubs, defProvider.ProviderId);

            // perform CashIn in subscribers account by wholesaler
            startNegativeTest();
            TransactionManagement.init(t8)
                    .performCashIn(sub06_07_08, "10");

            Assertion.verifyErrorMessageContain("receiver.status.is.delete.initiated",
                    "Subscriber Cannot Perform any action as it is account closure initiated", t8);

        } catch (Exception e) {
            markTestAsFailure(e, t8);
        } finally {
            SystemPreferenceManagement.init(t8)
                    .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", "N");
        }

    }

    @Test(priority = 9, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_138() {
        ExtentTest t12 = pNode.createNode("P1_TC_138", "Account Closure:To verify that subscriber can not be" +
                " suspended if account closure service for the subscriber is in initiated state");

        //login as Channel user
        try {
            User sub = new User(Constants.SUBSCRIBER);

            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, sub,
                    delSubs, null, null, null, null);

            TransferRuleManagement.init(t12)
                    .configureTransferRule(accClosure);

            ServiceChargeManagement.init(t12).
                    configureServiceCharge(accClosure);

            SubscriberManagement.init(t12)
                    .createDefaultSubscriberUsingAPI(sub);

            Transactions.init(t12)
                    .initiateCashIn(sub, chApproveSubs, new BigDecimal("10"));

            SubscriberManagement.init(t12)
                    .deleteSubscriberByAgent(sub, defProvider.ProviderId);

            Login.init(t12).login(optSsuspendResumeSubs);

            startNegativeTest();
            // Suspend the Subscriber
            SubscriberManagement.init(t12)
                    .setIsConfirmActionRequired(false)
                    .suspendSubscriber(sub, Constants.USER_TYPE_SUBS);

            Assertion.verifyErrorMessageContain("suspRes.error.noRecords",
                    "Subscriber Cannot be suspended as it is account closure initiated", t12);

        } catch (Exception e) {
            markTestAsFailure(e, t12);
        }
    }

    @Test(priority = 10, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_001() {
        ExtentTest t10 = pNode.createNode("P1_TC_001", "To verify that Network Admin should not be able to " +
                "registered Subscribers to get particular Biller Company's Notifications if account no./MSISDN is not entered.");

        //login as Channel user
        try {
            Login.init(t10)
                    .login(optBillReg);

            SubsBillerAssociation_pg1.init(t10)
                    .navAddBillerValidation()
                    .selectProvider(defProvider.ProviderName)
                    .setSubscriberMsisdn("")
                    .clickOnSubmit();

            Assertion.verifyErrorMessageContain("billpay.error.msisdnEmpty",
                    "Cannot register SUBS to get particular Biller Company if MSISDN is empty", t10);

        } catch (Exception e) {
            markTestAsFailure(e, t10);
        }
    }

    @Test(priority = 11, enabled = true, groups = {FunctionalTag.P1CORE, FunctionalTag.SUBSCRIBER_MANAGEMENT})
    public void P1_TC_136_1() {
        ExtentTest t11 = pNode.createNode("P1_TC_136_1", "B) Network Admin should not be able to registered " +
                "Subscribers to get particular Biller Company's Notifications if MFS provider/Company name is " +
                "not selected/account no./MSISDN is not entered.");
        //login as Channel user
        try {
            Login.init(t11)
                    .login(optBillReg);

            SubsBillerAssociation_pg1.init(t11)
                    .navAddBillerValidation()
                    .setSubscriberMsisdn("")
                    .clickOnSubmit();

            Assertion.verifyErrorMessageContain("billpay.error.msisdnEmpty",
                    "Cannot register SUBS to get particular Biller Company if Provider and MSISDN is Empty", t11);

        } catch (Exception e) {
            markTestAsFailure(e, t11);
        }
    }
}













