package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: KPI
 * Author Name      : Gurudatta Praharaj
 * Date             : 7/17/2018.
 */

public class Suite_P1_Service_Charge_in_Same_Hierarchy extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void P1_TC_066() throws Exception {
        ExtentTest P1_TC_066 = pNode.createNode("P1_TC_066", "To verify the Service Charge Limit when amount  is less " +
                "than min. transfer value or greater than max. transfer value defined in service charge.");

        /*
        perform any service with the normal wallet balance is less than the minimum amount for the service.
        validate error message
         */
        try {
            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User sub = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(P1_TC_066).createDefaultSubscriberUsingAPI(sub);

            Login.init(P1_TC_066).login(chUser);
            startNegativeTest();
            SubscriberManagement.init(P1_TC_066)
                    .deleteSubscriber(sub, DataFactory.getDefaultProvider().ProviderId);

            Assertion.verifyErrorMessageContain("user.delete.fail",
                    "normal wallet balance is less than the minimum service charge", P1_TC_066);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_066);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
