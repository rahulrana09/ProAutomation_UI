package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.pageObjects.gradeManagement.GradeAdd_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Grade Management button validation
 * Author Name      : Gurudatta Praharaj
 */
public class Suite_P1_AddGrade extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.GRADE_MANAGEMENT, FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TC062_TC061", "To verify that Super Admin should be able to Add grade in system");
        Grade grade = null;

        try {
            SuperAdmin sm = DataFactory.getSuperAdminWithAccess("ADD_GRADES");
            Login.init(t1).loginAsSuperAdmin(sm);

            grade = new Grade(Constants.WHOLESALER);

            GradeAdd_Page1 page = GradeAdd_Page1.init(t1);
            page.navigateToLink();
            page.addgrade();
            page.selectdomainByValue(grade.DomainCode);
            page.selectcategoryByValue(grade.CategoryCode);
            page.gradename(grade.GradeName);
            page.gradecode(grade.GradeCode);

            Utils.captureScreen(t1);
            Assertion.verifyEqual(page.getAddMoreButtonStatus(), true, "Add more Button Available", t1);
            Assertion.verifyEqual(page.getSaveButtonStatus(), true, "Save Button Available", t1);
            Assertion.verifyEqual(page.getBackButtonStatus(), true, "Back Button Available", t1);
            page.clickSaveButton();
            Assertion.verifyEqual(page.getConfirmButtonStatus(), true, "Confirm Button Available", t1);
            page.confirm();
            if(Assertion.verifyActionMessageContain("channel.grade.add.success", "Verify successfully added New Grade", t1, grade.GradeCode, grade.GradeName)){
                GradeManagement.init(t1).deleteGrade(grade, true);
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}
