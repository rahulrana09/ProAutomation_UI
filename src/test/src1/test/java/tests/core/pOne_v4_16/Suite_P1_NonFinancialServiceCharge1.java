package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.pageObjects.nonFinancialServiceCharge.NFCCalculator_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Non Financial Service Charge
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/14/2018.
 */
public class Suite_P1_NonFinancialServiceCharge1 extends TestInit {
    private OperatorUser optUsr;
    private User subscriber;
    private ServiceCharge balEnq;
    private ExtentTest P1_TC_438;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            optUsr = DataFactory.getOperatorUserWithAccess("CHARGENON_CALC");
            subscriber = new User(Constants.SUBSCRIBER);

            balEnq = new ServiceCharge(
                    Services.BALANCE_ENQUIRY, subscriber, optUsr,
                    null, null,
                    null, null);

            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceCharge(balEnq);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.PVG_SYSTEM, FunctionalTag.P1CORE,
            FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0})
    public void Test_01() throws Exception {
        /*
        Login as operator user
        Navigate to NFC > Service Charge Calculator
        Enter Category, Grade, Instrument Type and Service Type
        Click Submit
        Validate Whether NFC Details are displayed
         */
        P1_TC_438 = pNode.createNode("P1_TC_438", "To verify network admin is able to View " +
                "Service charge calculator(Non Financial- charging)");

        P1_TC_438.assignCategory(FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(P1_TC_438).login(optUsr);

            NFCCalculator(subscriber, Services.BALANCE_ENQUIRY);

            Assertion.verifyEqual(Assertion.isErrorInPage(P1_TC_438), false,
                    "NFC details are available", P1_TC_438, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_438);
        } finally {
            ServiceChargeManagement.init(pNode)
                    .deleteNFSChargeAllVersions(balEnq);
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * @param user
     * @param service
     * @throws Exception
     */
    private void NFCCalculator(User user, String service) throws Exception {
        NFCCalculator_Page1.init(P1_TC_438)
                .navToNFCcalculatorPage()
                .selectCategory(user.CategoryCode)
                .selectSenderGrade(user.GradeCode)
                .selectInstrumentType(Constants.NORMAL_WALLET)
                .selectService(service)
                .clickSubmitButton1()
                .clickSubmitButton2();
    }
}
