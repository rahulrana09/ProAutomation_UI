package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.enterpriseManagement.Enterprise_Management;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.Enterprise_Management.EntBulkPayDashBoard_Pg1;
import framework.pageObjects.userManagement.AddChannelUser_pg3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Enterprise Management
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/13/2018.
 */
public class Suite_P1_EnterpriseManagement_02 extends TestInit {
    private User sub1, sub2, eu, newEnpUsr;
    private OperatorUser optUsr1, optUsr2, bpAdmin;
    private String sub1UniqueCode, sub2UniqueCode;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest eSetup = pNode.createNode("Setup", "Setup Specific to this suite");

        try {
            eu = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            bpAdmin = DataFactory.getOperatorUserWithCategory(Constants.BULK_PAYER_ADMIN);
            sub1UniqueCode = DataFactory.getRandomNumberAsString(5);
            sub2UniqueCode = DataFactory.getRandomNumberAsString(6);
            optUsr1 = DataFactory.getOperatorUserWithAccess("PREF001");
            optUsr2 = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            newEnpUsr = new User(Constants.ENTERPRISE);

            sub1 = new User(Constants.SUBSCRIBER);
            sub2 = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(eSetup)
                    .createDefaultSubscriberUsingAPI(sub1);

            SubscriberManagement.init(eSetup)
                    .createDefaultSubscriberUsingAPI(sub2);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void P1_TC_351() {
        /*
        login as enterprise user
        navigate to add bulk payee
        add two subscribers as with different unique code.
        navigate to modify bulk payee
        modify one subscriber's unique code which is already assigned to a another subscriber
        validate error message
         */
        ExtentTest P1_TC_351 = pNode.createNode("P1_TC_351", " Modify Employee To verify that Enterprise" +
                " Company cannot modify an (Its existing employee) employee if already existing unique code associated with other payee is entered as an input.");
        try {
            Login.init(P1_TC_351).login(eu);

            Enterprise_Management.init(P1_TC_351)
                    .AddBulkPayee(sub1.MSISDN, sub1UniqueCode);

            Thread.sleep(Constants.TWO_SECONDS);

            Enterprise_Management.init(P1_TC_351)
                    .AddBulkPayee(sub2.MSISDN, sub2UniqueCode);

            startNegativeTest();
            Enterprise_Management.init(P1_TC_351).ModifyBulkPayee(sub2.MSISDN, sub1UniqueCode);
            Assertion.verifyErrorMessageContain("payroll.error.employeeExists", "Employee already exists with this unique code", P1_TC_351);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_351);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void P1_TC_352() throws Exception {
        /*
        login as enterprise user
        navigate to delete bulk employee
        delete employee
        validate success message
         */
        ExtentTest P1_TC_352 = pNode.createNode("P1_TC_352", "To verify that Enterprise Company can delete an employee.");
        try {
            Login.init(P1_TC_352).login(eu);

            startNegativeTest();
            Enterprise_Management.init(P1_TC_352).DeleteBulkPayee(sub1.MSISDN, sub1UniqueCode);

            stopNegativeTest();
            Enterprise_Management.init(P1_TC_352).DeleteBulkPayee(sub2.MSISDN, sub2UniqueCode);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_352);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void P1_TC_353() {
         /*
        login as enterprise user
        add an employee
        navigate to add bulk payee
        add an employee with a same unique code which is already registered
        validate error message
         */
        ExtentTest P1_TC_353 = pNode.createNode("P1_TC_353", "To verify that Enterprise company can not upload the Employee in bulk where entered " +
                "employee code is already associated with other MSISDN.");
        try {
            Login.init(P1_TC_353).login(eu);

            Enterprise_Management.init(P1_TC_353).AddBulkPayee(sub1.MSISDN, sub1UniqueCode);
            Assertion.verifyActionMessageContain("enterprise.label.addSuccess", "User Add success", P1_TC_353, sub1UniqueCode);

            startNegativeTest();
            Enterprise_Management.init(P1_TC_353).addBulkPayeeBatch(sub2, sub1UniqueCode);
            Assertion.verifyErrorMessageContain("employee.error.employees.empty", "Duplicate Unique code", P1_TC_353);

            stopNegativeTest();
            Enterprise_Management.init(P1_TC_353).DeleteBulkPayee(sub1.MSISDN, sub1UniqueCode);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_353);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void P1_TC_354() {
         /*
        login as enterprise user
        add an employee
        navigate to add bulk payee
        add an employee with a same MSISDN which is already registered
        validate error message
         */
        ExtentTest P1_TC_354 = pNode.createNode("P1_TC_354", "To verify that Enterprise company can not upload the Employee in bulk " +
                "when the entered mobile number is already existing as an employee in the system.");
        try {
            Login.init(P1_TC_354).login(eu);

            Enterprise_Management.init(P1_TC_354).AddBulkPayee(sub1.MSISDN, sub1UniqueCode);
            Assertion.verifyActionMessageContain("enterprise.label.addSuccess", "User Add success", P1_TC_354, sub1UniqueCode);

            startNegativeTest();
            Enterprise_Management.init(P1_TC_354).addBulkPayeeBatch(sub1, sub2UniqueCode);
            Assertion.verifyErrorMessageContain("employee.error.employees.empty", "Duplicate Mobile Number", P1_TC_354);

            stopNegativeTest();
            Enterprise_Management.init(P1_TC_354).DeleteBulkPayee(sub1.MSISDN, sub1UniqueCode);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_354);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void Test_05() throws Exception {
        /*
        Login as operator user
        navigate to preference > System preference
        set IS_LOGO_UPLOAD_ALLOWED to "N"
        login using channel admin
        initiate enterprise user creation
        validate logo upload field should be non mandatory
        set IS_LOGO_UPLOAD_ALLOWED back to "Y"
         */
        ExtentTest P1_TC_374 = pNode.createNode("P1_TC_374", "To verify that logo upload for enterprise should become non mandatory field when " +
                "channel admin registers an enterprise if preference IS_LOGO_UPLOAD_ALLOWED is set as 'N'.");
        try {
            SystemPreferenceManagement.init(P1_TC_374)
                    .updateSystemPreference("IS_LOGO_UPLOAD_ALLOWED", "N");

            Login.init(P1_TC_374).login(optUsr2);
            User enterprise = new User(Constants.ENTERPRISE);

            ChannelUserManagement.init(P1_TC_374)
                    .initiateChannelUser(newEnpUsr).assignHierarchy(newEnpUsr);
            CommonUserManagement.init(P1_TC_374);

            AddChannelUser_pg3 groupRole = AddChannelUser_pg3.init(P1_TC_374);
            groupRole.selectWebGroupRole(newEnpUsr.WebGroupRole);
            groupRole.clickNext();

            Assertion.verifyEqual(Assertion.isErrorInPage(P1_TC_374), false,
                    "Upload field is not available(non mandatory)", P1_TC_374, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_374);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void Test_06() {
        /*
        Login as operator user
        navigate to preference > System preference
        set IS_LOGO_UPLOAD_ALLOWED to "Y"
        login as channel admin
        navigate to create channel admin
        validate logo upload field should be available while creating enterprise user
        login with the newly created enterprise user and validate uploaded logo should displayed on home page
         */
        ExtentTest P1_TC_373 = pNode.createNode("P1_TC_373", "To verify that channel admin should be able to associate company logo to an enterprise by clicking on browse button, " +
                "if preference IS_LOGO_UPLOAD_ALLOWED is set as 'Y'.");

        ExtentTest P1_TC_375 = pNode.createNode("P1_TC_375", "To verify that company logo (if attached) should be displayed on login by enterprise user " +
                "if the preference IS_LOGO_UPLOAD_ALLOWED is set as 'Y'.");
        try {
            SystemPreferenceManagement.init(P1_TC_373)
                    .updateSystemPreference("IS_LOGO_UPLOAD_ALLOWED", "Y");

            Login.init(P1_TC_373).login(optUsr2);

            ChannelUserManagement.init(P1_TC_373)
                    .addChannelUser(newEnpUsr)
                    .approveChannelUser(newEnpUsr);

            CommonUserManagement.init(P1_TC_373)
                    .changeFirstTimePassword(newEnpUsr);
            Thread.sleep(Constants.TWO_SECONDS);

            boolean logo = Utils.checkElementPresent("//img[@alt = 'SomtelLogo']", Constants.FIND_ELEMENT_BY_XPATH);

            Assertion.verifyEqual(logo, true, "Enterprise Logo", P1_TC_375, true);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_375);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7, dependsOnMethods = "Test_06", groups = {FunctionalTag.P1CORE, FunctionalTag.BILL_PAYMENT})
    public void Test_07() throws Exception {
        /*
        Login as operator user
        navigate to Operator user > bar user
        bar a enterprise user as both
        login as barred enterprise user
        add a subscriber as bulk payee
        navigate to enterprise payment initiation and initiate a payment
        navigate to enterprise payment dashboard
        select status as failed
        select the resent entry and validate error message
         */
        ExtentTest P1_TC_570 = pNode.createNode("P1_TC_570", "Enterprise Manager(Reg Users): To verify that " +
                "valid enterprise manager cannot perform money transfer for registered customer if enterprise manager is barred as sender or both.");
        try {
            Login.init(P1_TC_570).login(optUsr1);

            TransactionManagement.init(P1_TC_570)
                    .makeSureChannelUserHasBalance(newEnpUsr);

            ChannelUserManagement.init(P1_TC_570)
                    .barChannelUser(newEnpUsr, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_BOTH);

            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Login.init(P1_TC_570).login(newEnpUsr);

            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1", "", "", ""));

            String fileName = EnterpriseManagement.init(P1_TC_570)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            startNegativeTest();
            EnterpriseManagement.init(P1_TC_570)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            String expMsg = driver.findElement(By.xpath("//span[contains(text(),'barred')]")).getText();
            Assertion.verifyContains(expMsg, "Initiator has been barred as both", "Barred User", P1_TC_570, true);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_570);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    // @Test(priority = 8)
    public void Test_08() throws Exception {
         /*
        Login as operator user
        navigate to Channel User Management > Suspend user
        Suspend an enterprise
        login as Suspended enterprise user
        add a subscriber as bulk payee
        navigate to enterprise payment initiation and initiate a payment
        navigate to enterprise payment dashboard
        select status as failed
        select the resent entry and validate error message
        TODO [ script need to be fixed, verifying dashboard data is not correct]
         */
        ExtentTest TC604 = pNode.createNode("TC604", "To verify that valid enterprise manager cannot perform " +
                "money transfer for registered customer if enterprise manager is suspended.");
        try {
            ChannelUserManagement.init(TC604)
                    .suspendChannelUser(eu);

            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(TC604).login(eu);

            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, "1", "", "", ""));
            String fileName = EnterpriseManagement.init(TC604)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            startNegativeTest();
            EnterpriseManagement.init(TC604)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EntBulkPayDashBoard_Pg1.init(TC604)
                    .navigateToPayMentDashboard()
                    .selectStatus("FAILED");

            String message = EntBulkPayDashBoard_Pg1.init(TC604)
                    .selectEntryAndFetchMessage(0);

            Assertion.verifyContains(message, "", "Enterprise User Suspended", TC604);

            //teardown delete bulk payee and resume enterprise user
            Enterprise_Management.init(TC604)
                    .DeleteBulkPayee(sub1.MSISDN, sub1UniqueCode);

            Login.init(TC604).login(optUsr1);
            ChannelUserManagement.init(TC604)
                    .resumeChannelUser(eu);
        } catch (Exception e) {
            markTestAsFailure(e, TC604);
        }
        Assertion.finalizeSoftAsserts();
    }

    private String generateBulkFileNew(String msisdn, String Amount) throws IOException {
        String COMMA_DELIMITER = ",";
        String NEW_LINE_SEPARATOR = "\n";

        String csvFileHeader = "MFS Provider*,Mobile Number*,Amount*,First Name,Last Name,National Id,ID Number";

        String filePath = "uploads/" + "salary" + DataFactory.getTimeStamp();

        File directory = new File("uploads");
        if (!directory.exists()) {
            directory.mkdir();
        }

        String fileName = filePath + ".csv";

        File f = new File(fileName);
        FileWriter fileWriter = new FileWriter(f);

        try {
            //FileWriter fileWriter = new FileWriter(f);

            fileWriter.append(csvFileHeader);
            fileWriter.append(NEW_LINE_SEPARATOR);
            fileWriter.append("101");

            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(msisdn);

            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(Amount);

            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(COMMA_DELIMITER);
            fileWriter.append(COMMA_DELIMITER);

//        fileWriter.append("\n");

        } catch (Exception e) {
            System.out.println("Error in CsvFileWriter !!!");
        } finally {
            fileWriter.flush();
            fileWriter.close();
        }
        return f.getAbsolutePath();
    }
}
