package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.pageObjects.nonFinancialServices.EmailNotificationConfig_Page;
import framework.pageObjects.preferences.NotificationConfiguration_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;
import java.util.NoSuchElementException;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Network Admin should be able to understand how to modify\n" +
 " an existing SMS in the system through WEB
 * Author Name      : Nirupama / refactored by rahul rana on 7/12/2018
 * Created Date     : 12/02/2018
 */
public class Suite_P1_Email_NotificationConfiguration extends TestInit {

    private OperatorUser optUser;
    private String serviceType = "Suspend Channel User Approval"; // change this for testing other services

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        optUser = DataFactory.getOperatorUserWithAccess("SMS_CR");
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_086() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_086",
                "Verify Email Notification Configuration Page");


        try {
            Login.init(t1).login(optUser);
            NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(t1);
            page.navigateToEmailConfig();
            Utils.captureScreen(t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.emailRadio), true,
                    "Verify that an option to check radio Email is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.serviceCode), true,
                    "Verify that an option to Select Service Code is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.txtEmailSubject), true,
                    "Verify that an option to Email Notification Subject is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.languageDdown), true,
                    "Verify that an option to select language is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.transactionDataCode), true,
                    "Verify that an option to Select transaction Data Code is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.addButton), true,
                    "Verify that an option to add Transaction Data Code is available as an ADD Button", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.messageTextArea), true,
                    "Verify that Message Text area is available", t1);

            Assertion.verifyEqual(fl.elementIsDisplayed(page.submitButton), true,
                    "Verify that an option to submit any changes in SMS configuration page is available", t1);

        } catch (NoSuchElementException e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_026() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_026",
                "Verify that Email notification message and message subject are shown on selecting any respective service type");

        Login.init(t1).login(optUser);
        NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(t1);
        try {
            page.navigateToSMSConfig();
            page.clickEmailRadioBtn();
            page.selectServiceByValue(serviceType);
            page.selectLanguage();

            Assertion.verifyEqual(page.getEmailSubject(), MessageReader.getMessage("notification.email.subject.suspend.approval.channeluser", null),
                    "Verify that the Email Notification subject for the service '" + serviceType + "' is shown correctly", t1, true);

            Assertion.verifyEqual(page.getMessageTextArea(), MessageReader.getMessage("notification.email.msg.suspend.approval.channeluser", null),
                    "Verify that the Email Notification Message for the service '" + serviceType + "' is shown correctly", t1, true);

        } catch (NoSuchElementException e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.EMAIL_NOTIFICATION})
    public void P1_TC_027() throws Exception {

        String newSubject = "P1_TC_027: this subject is set for Testing purpose";
        ExtentTest t1 = pNode.createNode("P1_TC_027",
                "Verify successfully updating the Email Notification Subject");

        Login.init(t1).login(optUser);
        NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(t1);
        try {
            page.navigateToEmailConfig();
            page.selectServiceByValue(serviceType);
            page.selectLanguage();
            page.setEmailSubject(newSubject);
            page.clickSubmit();

            Assertion.verifyActionMessageContain("action.message.common.success.update",
                    "Verify that email notification configuration is successfully updated", t1);

            // verify that the new message is reflected
            page.navigateToEmailConfig();
            page.selectServiceByValue(serviceType);
            page.selectLanguage();

            Assertion.verifyEqual(page.getEmailSubject(), newSubject,
                    "Verify that the Email Notification subject for the service '" + serviceType + "' is updated and new subject is reflect in UI", t1, true);

        } catch (NoSuchElementException e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).login(optUser);
            Preferences.init(t1).updateEmailNotification(serviceType,
                    MessageReader.getMessage("notification.email.subject.suspend.approval.channeluser", null),
                    null
            );
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.EMAIL_NOTIFICATION})
    public void P1_TC_028() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_028",
                "Verify successfully updating the dynamic message text and user can add the dynamic fields using add button");

        Login.init(t1).login(optUser);
        NotificationConfiguration_pg1 page = new NotificationConfiguration_pg1(t1);
        try {
            page.navigateToEmailConfig();
            page.selectServiceByValue(serviceType);
            page.selectLanguage();

            String messageTextDynamic = page.getMessageTextArea();
            // get the options from the transaction data Code select
            List<String> transactionDataCode = page.getTransactionDataCode();
            if (transactionDataCode.size() > 0) {
                String txCode = page.getTransactionDataCode().get(0);
                page.selectTransactionCodeByValue(txCode);
                page.clickAddButton();
                messageTextDynamic += txCode; // added code to the expected string
            }


            page.clickSubmit();

            Assertion.verifyActionMessageContain("action.message.common.success.update",
                    "Verify that email notification configuration is successfully updated", t1);

            //verify that the new dynamic message is updated
            page.navigateToEmailConfig();
            page.selectServiceByValue(serviceType);
            page.selectLanguage();
            String currentMessage = page.getMessageTextArea();

            Assertion.verifyEqual(currentMessage, messageTextDynamic,
                    "Verify that the Email Notification message for the service '" + serviceType + "' is updated and new subject is reflect in UI", t1, true);

        } catch (NoSuchElementException e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).login(optUser);
            Preferences.init(t1).updateEmailNotification(serviceType,
                    null,
                    MessageReader.getMessage("notification.email.msg.suspend.approval.channeluser", null)
            );
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE})
    public void P1_TC_115() throws Exception {
        ExtentTest test = pNode.createNode("P1_TC_115", "To verify that  'Email Notification Configuration' " +
                "shall be provided in the menu for Network Admin. All the service/Action which is performing from Web " +
                "that involve first or second level approvals shall be included in the email notification process.");
        try {
            OperatorUser opUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            Login.init(test).login(opUsr);

            EmailNotificationConfig_Page page = new EmailNotificationConfig_Page(test);

            page.navigateToLink()
                    .updateSmsEmailService("REQ_CASH", true, true);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}