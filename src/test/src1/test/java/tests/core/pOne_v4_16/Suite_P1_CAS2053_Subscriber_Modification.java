package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: CAS2053 Subscriber Modification
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/18/2018.
 */
public class Suite_P1_CAS2053_Subscriber_Modification extends TestInit {

    private User chUsr, sub;
    private ExtentTest P1_TC_592;

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {
        /*
        create a subscriber by channel user through API
        modify the subscriber
        modification approval should be successful
         */
        ExtentTest P1_TC_591 = pNode.createNode("P1_TC_591", "To verify that any valid user can modify the " +
                "subscriber details which is registered  through Channel user from USSD.");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub = new User(Constants.SUBSCRIBER);

            Transactions.init(P1_TC_591)
                    .SubscriberRegistrationByChannelUserWithKinDetails(sub);

            Transactions.init(P1_TC_591).subscriberAcquisition(sub);

            P1_TC_592 = pNode.createNode("P1_TC_592", "To Verify the modify approval should successful.");

            Login.init(P1_TC_592).login(chUsr);

            SubscriberManagement.init(P1_TC_592).modifySubscriber(chUsr, sub);

            SubscriberManagement.init(P1_TC_592)
                    .modifyApprovalSubs(sub);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_592);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    private void completeModificationSub(User user) throws Exception {

        driver.findElement(By.name("doc1")).sendKeys(sub.IDProof);
        driver.findElement(By.name("doc2")).sendKeys(sub.AddressProof);
        driver.findElement(By.name("doc3")).sendKeys(sub.PhotoProof);

        new Select(driver.findElement(By.name("proofType1"))).selectByIndex(1);
        new Select(driver.findElement(By.name("proofType2"))).selectByIndex(2);
        new Select(driver.findElement(By.name("proofType3"))).selectByIndex(3);

        ModifySubscriber_page2.init(P1_TC_592)
                .setNamePrefix()
                .setIdType()
                .clickOnNextPg2();

        ModifySubscriber_page3.init(P1_TC_592)
                .selectWebGroupRole(user.WebGroupRole);

        ModifySubscriber_page3.init(P1_TC_592)
                .nextPage();

        ModifySubscriber_page4.init(P1_TC_592)
                .clickSubmitPg4()
                .clickOnConfirmPg4()
                .nextFinalConfirm_click();
    }
}
