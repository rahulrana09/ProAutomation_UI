package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.customerCareManagement.CustomerCareExecutive_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Customer Care Executive
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/13/2018.
 */
public class Suite_P1_CustomerExecutive extends TestInit {
    private User sub;
    private OperatorUser cce;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception, MoneyException {
        ExtentTest setup = pNode.createNode("Setup", "Create a Customer user[subscriber] for Testing");
        try {
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(setup)
                    .createSubscriberDefaultMapping(sub, true, false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest TC520 = pNode.createNode("TC520",
                "To verify that valid user can change the consumer channel user status from Active to Bar.");

        try {
            Login.init(TC520).login(cce);

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Enquiries.init(TC520)
                    .barUser(Constants.USR_TYPE_SUBS,
                            Constants.MOBILENUMBER,
                            sub.MSISDN,
                            Constants.BAR_AS_SENDER);

            CustomerCareExecutive_page1.init(TC520)
                    .verifyNotificationMessage("successfully barred")
                    .clickOkButton_Bar();

            CustomerCareExecutive_page1.init(TC520).switchToWindow(0);
        } catch (Exception e) {
            markTestAsFailure(e, TC520);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_02() throws Exception {
        ExtentTest TC521 = pNode.createNode("TC521",
                "To verify that valid user can change the consumer channel user status from Bar to Active.");
        Login.init(TC521).login(cce);
        try {
            Enquiries.init(TC521)
                    .unBarUser(Constants.USR_TYPE_SUBS,
                            Constants.MOBILENUMBER,
                            sub.MSISDN,
                            Constants.UNBAR);

            CustomerCareExecutive_page1.init(TC521)
                    .verifyNotificationMessage("Unbarred Successfully")
                    .clickOkButtonUnbar();
        } catch (Exception e) {
            markTestAsFailure(e, TC521);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_03() {
        ExtentTest TC522 = pNode.createNode("TC522", "To verify that valid user can change the consumer channel user status from Suspended to Active ");
        try {
            Login.init(TC522).login(cce);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Enquiries.init(TC522)
                    .suspend_resume_user(Constants.USR_TYPE_SUBS,
                            Constants.MOBILENUMBER,
                            sub.MSISDN,
                            Constants.SUSPEND);

            CustomerCareExecutive_page1.init(TC522)
                    .verifyNotificationMessage("Success")
                    .clickSuspend_Resume_Ok();

            Enquiries.init(TC522)
                    .suspend_resume_user(Constants.USR_TYPE_SUBS,
                            Constants.MOBILENUMBER,
                            sub.MSISDN,
                            Constants.RESUME);

            CustomerCareExecutive_page1.init(TC522)
                    .verifyNotificationMessage("Success")
                    .clickSuspend_Resume_Ok();
        } catch (Exception e) {
            markTestAsFailure(e, TC522);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
