package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Non Financial service charge>Add Non financial service charge\n" +
 "To verify network admin is able to Add Non financial service charge
 * Author Name      : Nirupama mk
 * Created Date     : 12/02/2018
 */

public class Suite_P1_NonFinancialServiceCharge extends TestInit {
    private ServiceCharge balance;
    private User channeluse;
    private OperatorUser usrNFSChargeCreator, usrNFSChargeSuspResume, usrNFSChargeApprover, usrNFSChargeModify, usrNFSChargeView;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        usrNFSChargeCreator = DataFactory.getOperatorUserWithAccess("CHARGENON");
        usrNFSChargeApprover = DataFactory.getOperatorUserWithAccess("CHARGENON_APP");
        usrNFSChargeModify = DataFactory.getOperatorUserWithAccess("CHARGENON_MOD");
        usrNFSChargeSuspResume = DataFactory.getOperatorUserWithAccess("CHARGENON_SUS");
        usrNFSChargeView = DataFactory.getOperatorUserWithAccess("CHARGENON_VIEW");
        channeluse = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_432() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_432", " Non Financial service charge>Add Non financial service charge\n" +
                "To verify network admin is able to Add Non financial service charge");
        try {

            balance = new ServiceCharge(Services.BALANCE_ENQUIRY, channeluse, usrNFSChargeCreator, null, null, null, null);

            // initiate Add NON-financial service charge
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);
            ExtentTest t2 = pNode.createNode("P1_TC_435", " Non Financial service charge>Modify Non financial service charge\n" +
                    "To verify network admin is able to Modify Non financial service charge");

            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0);
            // initiate MODIFY NON-financial service charge

            Login.init(t1).login(usrNFSChargeModify);
            ServiceChargeManagement.init(t2)
                    .modifyNFSChargeInitiate(balance);


            Login.init(t1).login(usrNFSChargeApprover);
            balance.setIsNFSC();
            ServiceChargeManagement.init(t2)
                    .approveServiceCharge(balance, Constants.SERVICE_CHARGE_APPROVE_MODIFY);

            ExtentTest t4 = pNode.createNode("P1_TC_437", " Non Financial service charge>View (Non financial) service charge.\n" +
                    "To verify network admin is able to View (Non financial) service charge.");

            //view NFSC
            Login.init(t1).login(usrNFSChargeView);
            ServiceChargeManagement.init(t4)
                    .viewNFSChargeLatestVersion(balance, "999");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, dependsOnMethods = "P1_TC_432", groups = {FunctionalTag.P1CORE, FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0})
    public void P1_TC_436() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_436", " Non Financial service charge>Suspend/Resume  Non financial service charge\n" +
                "To verify network admin is able to Suspend/Resume Non financial service charge");
        try {


            Login.init(t1).login(usrNFSChargeModify);
            ServiceChargeManagement.init(t1).
                    suspendNFSCharge(balance);

            ServiceChargeManagement.init(t1).
                    resumeNFSCharge(balance);

            ExtentTest P1_TC_434_P1_TC_435 = pNode.createNode("P1_TC_434_P1_TC_435", " Non Financial service charge>Delete Non financial service charge\n" +
                    "To verify network admin is able to delete Non financial service charge");

            P1_TC_434_P1_TC_435.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NON_FINANCIAL_SERVICE_CHARGE, FunctionalTag.ECONET_UAT_5_0);

            ServiceChargeManagement.init(P1_TC_434_P1_TC_435)
                    .deleteNFSChargeAllVersions(balance);

//            ServiceChargeManagement.init(t2)
//                    .deleteNFSChargeInitiate(balance);
//
//            ExtentTest t3 = pNode.createNode("P1_TC_435", " Non Financial service charge>Approve  Non financial service charge\n" +
//                    "To verify network admin is able to Approve Non financial service charge");
//
//            balance.setIsNFSC();
//            ServiceChargeManagement.init(t3)
//                    .approveServiceCharge(balance, Constants.SERVICE_CHARGE_APPROVE_DELETE);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

