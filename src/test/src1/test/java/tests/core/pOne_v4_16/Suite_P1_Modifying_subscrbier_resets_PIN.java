package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;

/**
 * Created by gurudatta.praharaj on 6/18/2018.
 */
public class Suite_P1_Modifying_subscrbier_resets_PIN extends TestInit {

    private User chUsr, sub1, sub2;
    private String providerID, providerName, createdOn1, createdOn2;
    private ExtentTest TC717; //invalid TC

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub1 = new User(Constants.SUBSCRIBER);
            sub2 = new User(Constants.SUBSCRIBER);
            providerID = DataFactory.getDefaultProvider().ProviderId;
            providerName = DataFactory.getDefaultProvider().ProviderName;

            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "Y");

            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(chUsr);

            Transactions.init(setup)
                    .selfRegistrationForSubscriberWithKinDetails(sub1);

            createdOn1 = getCreatedDateAndTimeBasedOnMsisdn(sub1.MSISDN);

            Transactions.init(setup)
                    .selfRegistrationForSubscriberWithKinDetails(sub2);

            Transactions.init(setup)
                    .initiateCashIn(sub1, chUsr, "5");

            Login.init(setup).login(chUsr);

            ConfigInput.isAssert = false;

            SubscriberManagement.init(setup)
                    .deleteSubscriber(sub1, providerID);

            String actMsg = Assertion.getActionMessage();
            if (actMsg.contains(MessageReader.getMessage("sub.acc.close" + sub1.FirstName, null))) {
                setup.pass(actMsg);
            } else {
                setup.fail(Assertion.getErrorMessage());
                Utils.captureScreen(pNode);
            }

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE}, enabled = false)
    public void Test_01() {
        /*
        login as channel user
        navigate to subscriber modification page
        modify a subscriber MSISDN with
        another subscriber MSISDN which was deleted from the system.
        verify CREATED_ON column of deleted subscriber
        will not get updated in MTX_PARTY_ACCESS TABLE
         */
        TC717 = pNode.createNode("P1_TC_077", "To verify that Tables(MTX_PARTY_ACCESS) details " +
                "should not get updated with existing deleted subscriber details on modification for the same MSISDN.");
        try {
            Login.init(TC717).login(chUsr);

            SubscriberManagement.init(TC717)
                    .initiateSubscriberModification(sub2);

            modifyMSISDNofSub(sub1.MSISDN);

            createdOn2 = getCreatedDateAndTimeBasedOnMsisdn(sub1.MSISDN);

            if (!createdOn1.equals(createdOn2)) {
                TC717.pass("Values are preserved for the deleted customer: " + sub1.FirstName);
            } else {
                TC717.fail("Values are updated for the deleted customer: " + sub1.FirstName);
            }
        } catch (Exception e) {
            markTestAsFailure(e, TC717);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE}, enabled = false) //invalid tc
    public void Test_02() throws Exception {
        /*
        create subscriber with same MSISDN which was deleted from the system before
        modify the subscriber.
        perform a transaction to verify pin is not expired.
         */
        ExtentTest P1_TC_078 = pNode.createNode("P1_TC_078", "To verify that if deleted Subscriber whose PIN is " +
                "going to expiry is registered again then newly registered subscriber(with same MSISDN) PIN should not " +
                "get expiry after modification of newly registered subscriber.");
        try {
            Transactions.init(P1_TC_078)
                    .selfRegistrationForSubscriberWithKinDetails(sub1);

            Login.init(P1_TC_078).login(chUsr);

            String txnID = TransactionManagement.init(P1_TC_078)
                    .performCashIn(sub1, "5", providerName);

            String txnStatus = MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(txnID);

            Assertion.verifyEqual(txnStatus, Constants.TXN_STATUS_SUCCESS, "Transaction Success With Same Pin", P1_TC_078);

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_078);
        } finally {
            SystemPreferenceManagement.init(pNode)
                    .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "N");
            Assertion.finalizeSoftAsserts();
        }
    }

    private void modifyMSISDNofSub(String msisdn) throws Exception {

        ModifySubscriber_page1.init(TC717)
                .setMSISDN(msisdn);
        ModifySubscriber_page2.init(TC717)
                .clickOnNextPg2();
        ModifySubscriber_page3.init(TC717)
                .nextPage();
        ModifySubscriber_page4.init(TC717)
                .clickSubmitPg4()
                .clickOnConfirmPg4()
                .clickFinalConfirm();
    }

    private String getCreatedDateAndTimeBasedOnMsisdn(String msisdn) {
        String res = null;
        String query = "select to_char(CREATED_ON,'DD-MM-YYYY HH:SS') from MTX_PARTY_ACCESS where MSISDN = '" + msisdn + "';";
        OracleDB dbConn = new OracleDB();
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("MSISDN");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
