package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import tests.core.base.TestInit;
/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify various Fields and Buttons in Security Management (2-P1 Cases)
 * Author Name      : Jyoti Katiyar
 * Created Date     : 16/02/2018
 */

public class Suite_P1_Security extends TestInit {
    private OperatorUser NetworkAdm;

    @Test(enabled = false)// duplicate
    public void Test_01() {
        ExtentTest t1 = pNode.createNode("TC141", "1.Open the Browser Application and enter the application URL \n" +
                "2.Logout the user & again try to access page using Back option of internet explorer.");
        try {
            //Login as a operator user
            NetworkAdm = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");
            Login.init(t1).login(NetworkAdm);
            Login.init(t1).performLogout();
            driver.navigate().back();
            Thread.sleep(5000);
            Assertion.verifyErrorMessageContain("login.index.label.successlogoutxss", "Invalid Access", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_02() {
        ExtentTest t2 = pNode.createNode("TC147", "Web page of an application should not be accessible without any authentication.");
        try {
            //Login as a operator user
            NetworkAdm = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            Login.init(t2).login(NetworkAdm);

            //get the current URL
            String url = driver.getCurrentUrl();
            t2.info("Current Page url After Successful Login:" + url);

            //Open a new tab/window
            Actions act = new Actions(driver);
            act.sendKeys(Keys.chord(Keys.CONTROL, "t")).build().perform();

            //Enter the URL
            t2.info("Open a new Tab and try accessing the URL");
            driver.get(url);
            Thread.sleep(3000);
            driver.switchTo().frame(0);

            //Get the Error message and verify it
            String actual = Assertion.getErrorMessage();
            System.out.println(actual);
            Assertion.verifyErrorMessageContain("login.index.label.successlogoutxss",
                    "Verify that Application pages can not be accessed without providing credentials.", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
