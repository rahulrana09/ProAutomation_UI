package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Channel user Management >> Add channel user>>To verify that while\n"+
 " creating channel user if preference IS_RANDOM_PIN_ALLOW is set as Y then password is automatically\n"+
 " generated
 * Author Name      : Nirupama mk
 * Created Date     : 19/02/2018
 */

public class Suite_P1_ChannelUserManagement_04 extends TestInit {

    private OperatorUser optUser;
    private String defaultValue;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        optUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");
    }

    @Test(priority = 1)
    public void TC266() throws Exception {

        ExtentTest t1 = pNode.createNode("TC266", " Channel user Management >> Add channel user>>To verify that while\n" +
                " creating channel user if preference IS_RANDOM_PIN_ALLOW is set as Y then password is automatically\n" +
                " generated");

        MobiquityGUIQueries.updateDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW", "Y");
        User whs266 = new User(Constants.WHOLESALER);

        Login.init(t1).login(optUser);
        ChannelUserManagement.init(t1).createChannelUserDefaultMappingWithoutChangingTpin(whs266);
        SMSReader.init(t1).verifyRecentNotification(whs266.MSISDN, "automatic password generation");
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC263() throws Exception {

        ExtentTest t2 = pNode.createNode("P1_TC_371", " Channel user Management >> Add channel user>>To verify that while\n" +
                " creating channel user if preference IS_RANDOM_PIN_ALLOW is set as Y then pin is automatically\n" +
                " generated");
        try {
            String defaultPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("USE_DEFAULT_PIN");
            String randomPin = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW");

            HashMap<String, String> pref = new HashMap<>();
            pref.put("USE_DEFAULT_PIN", "N");
            pref.put("IS_RANDOM_PIN_ALLOW", "Y");

            if (defaultPin.equalsIgnoreCase("Y") || randomPin.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("USE_DEFAULT_PIN", "Y");
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_RANDOM_PIN_ALLOW", "Y");

                SystemPreferenceManagement.init(pNode).updateMultipleSystemPreference(pref);
            }

            //MobiquityGUIQueries.updateDefaultValueOfPreference("IS_RANDOM_PIN_ALLOW", "N");
            User whs263 = new User(Constants.WHOLESALER);

            Login.init(t2).login(optUser);
            ChannelUserManagement.init(t2).createChannelUserDefaultMappingWithoutChangingTpin(whs263, false);
            SMSReader.init(t2).verifyNotificationContain(whs263.MSISDN, "random.pin.generation");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
    }


    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest test = pNode.createNode("teardown");
        try {

            Map<String, String> map = new LinkedHashMap<>();
            map.put("IS_RANDOM_PIN_ALLOW", "N");
            map.put("USE_DEFAULT_PIN", "Y");

            SystemPreferenceManagement.init(test)
                    .updateMultipleSystemPreferences(map);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }
}