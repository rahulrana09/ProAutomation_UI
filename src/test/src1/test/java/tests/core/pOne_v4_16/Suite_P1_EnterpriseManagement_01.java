package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.Enterprise_Management;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Enterprise Management
 * Author Name      : Gurudatta Praharaj
 * Date             : 7/18/2018.
 */
public class Suite_P1_EnterpriseManagement_01 extends TestInit {

    private User enUser, employee;
    private String employeeCode;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            enUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            employee = new User(Constants.SUBSCRIBER);
            employeeCode = DataFactory.getRandomNumberAsString(5);

            SubscriberManagement.init(setup)
                    .createDefaultSubscriberUsingAPI(employee);

            Login.init(setup)
                    .login(enUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE,
            FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_569() throws Exception {
        ExtentTest P1_TC_569 = pNode.createNode("P1_TC_569", "Employee Bulk Registration\n" +
                "To verify that Enterprise Company can add  employees using bulk upload.");

        P1_TC_569.assignCategory(FunctionalTag.ECONET_UAT_5_0);
        try {
            Enterprise_Management.init(P1_TC_569)
                    .addBulkPayeeBatch(employee, employeeCode);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_569);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void P1_TC_567() {
        ExtentTest P1_TC_567 = pNode.createNode("P1_TC_567", "Modify Employee\n" +
                "To verify that Enterprise Company can modify an (Its existing employee) employee.");
        try {
            employeeCode = DataFactory.getRandomNumberAsString(5);

            User employee1 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(P1_TC_567)
                    .createDefaultSubscriberUsingAPI(employee1);

            Enterprise_Management.init(P1_TC_567)
                    .addBulkPayeeBatch(employee1, employeeCode);

            Enterprise_Management.init(P1_TC_567)
                    .ModifyBulkPayee(employee1.MSISDN, employeeCode);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_567);

        } finally {
            Assertion.finalizeSoftAsserts();

        }
    }

    @Test(priority = 3, groups = {FunctionalTag.P1CORE, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_568() throws Exception {
        ExtentTest P1_TC_568 = pNode.createNode("P1_TC_568", "Add Employee" +
                "To verify that Enterprise Company can add an employee with employee code which has been deleted before.");
        try {

            User employee1 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(P1_TC_568)
                    .createDefaultSubscriberUsingAPI(employee1);
            employeeCode = DataFactory.getRandomNumberAsString(5);

            Login.init(P1_TC_568).login(enUser);
            Enterprise_Management.init(P1_TC_568)
                    .AddBulkPayee(employee1.MSISDN, employeeCode);

            Enterprise_Management.init(P1_TC_568)
                    .DeleteBulkPayee(employee1.MSISDN, employeeCode);

            Login.init(P1_TC_568).login(enUser);
            Enterprise_Management.init(P1_TC_568)
                    .AddBulkPayee(employee1.MSISDN, employeeCode);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_568);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
