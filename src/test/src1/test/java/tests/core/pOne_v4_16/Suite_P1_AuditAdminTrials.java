package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.PageInit;
import framework.pageObjects.auditTrail.AdminTrail_Page1;
import framework.pageObjects.auditTrail.AuditTrail_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Audit Admin Fields and Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/6/2018.
 */
public class Suite_P1_AuditAdminTrials extends TestInit {
    private User usrSubs, usrCIN_WEB, whsUsr;
    private OperatorUser auditTrail, adminTrail;
    private String txnAmount = Constants.MIN_CASHIN_AMOUNT;

    @BeforeClass(alwaysRun = true)
    public void setup() {
        try {
            // get user with Cash in rights
            usrCIN_WEB = DataFactory.getChannelUserWithAccess("CIN_WEB");
            usrSubs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            auditTrail = DataFactory.getOperatorUserWithAccess("AUDIT_TRAIL");
            adminTrail = DataFactory.getOperatorUserWithAccess("ADMIN_TRAIL");

            // make sure that the User with Cash In rights has sufficient Balance
            TransactionManagement.init(pNode)
                    .makeSureChannelUserHasBalance(usrCIN_WEB);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * Audit Trail, test Field Validations
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() {
        ExtentTest TC552 = pNode.createNode("TC552", "All the financial transaction done within the number of days " +
                "mentioned in the from date and to date range entered should be displayed");

        try {
            /*
            Perform Cash in to the subscriber User
            Get the txn id
            Verify that this txn id against the Audit Trail page value
             */
            for (String provider : DataFactory.getAllProviderNames()) {

                Login.init(TC552).login(usrCIN_WEB);
                String txnId = TransactionManagement.init(TC552)
                        .performCashIn(usrSubs, txnAmount, provider);

                // Login as Super admin with Audit Trail rights
                Login.init(TC552).login(auditTrail);

                boolean isTxnIdAvailable = AuditTrail_Page1.init(TC552)
                        .navigateToLink()
                        .selectDomainByValue(usrCIN_WEB.DomainCode)
                        .selectCategoryByValue(usrCIN_WEB.CategoryCode)
                        .fromDateSetText(new DateAndTime().getDate(0))
                        .toDateSetText(new DateAndTime().getDate(0))
                        .submitButtonClick()
                        .checkIfTxnIdIsAvailable(txnId);

                // verify that txn Id is shown for selected date
                Assertion.verifyEqual(isTxnIdAvailable, true,
                        "Verify If TxnId: " + txnId + " is available on Audit trail page for selected Date for Currency Provider:" + provider, TC552);


            }
        } catch (Exception e) {
            markTestAsFailure(e, TC552);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * Audit Trail Field Validation
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.EMAIL_NOTIFICATION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_110() {
        ExtentTest TC553 = pNode.createNode("P1_TC_110",
                "Verify Audit Admin Trail page, verify for fields, actions and labels");
        try {
            /*
            login using net admin
            navigate to audit trial page
            verify the table headers labels
             */
            Login.init(TC553).login(auditTrail);

            AuditTrail_Page1.init(TC553)
                    .navigateToLink()
                    .selectDomainByValue(DataFactory.getDomainCode(Constants.WHOLESALER))
                    .selectCategoryByValue(Constants.WHOLESALER)
                    .fromDateSetText(new DateAndTime().getDate(0))
                    .toDateSetText(new DateAndTime().getDate(0))
                    .submitButtonClick();

            Object[] actualLabelText = PageInit.fetchLabelTexts("//table[@id= 'table']/thead/tr/th");
            Object[] expectedLabelText = UserFieldProperties.getLabels("aud.trial.page2");

            if (Arrays.equals(actualLabelText, expectedLabelText)) {
                for (int i = 0; i < actualLabelText.length; i++) {
                    TC553.pass(actualLabelText[i] + " :Is Present In The Audit Trial Table");
                }
            } else {
                TC553.fail("Label Validation Failed");
            }
            Utils.captureScreen(TC553);
        } catch (Exception e) {
            markTestAsFailure(e, TC553);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() {
        ExtentTest TC554 = pNode.createNode("TC554", "To verify that admin trail should display the detail of admin/system users created in the system with in the date range mentioned in From date and To date");
        try {
            /*
            Create a subscriber
            Verify that subscriber's username against the Admin Trail page value
             */
            whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(TC554).login(whsUsr);

            usrSubs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(TC554).createSubscriber(usrSubs);

            Login.init(TC554).login(adminTrail);

            AdminTrail_Page1 page = AdminTrail_Page1.init(TC554);

            page.navigateToLink();
            page.selectDomainByValue(DataFactory.getDomainCode(Constants.WHOLESALER));
            Thread.sleep(2000);
            page.selectCategoryByValue(Constants.WHOLESALER);

            String fromDate = new DateAndTime().getDate(0);
            String toDate = new DateAndTime().getDate(0);

            page.fromDateSetText(fromDate);
            page.toDateSetText(toDate);
            page.submitButtonClick();
            boolean isPartUsernameAvailable = page.checkIfUsernameIsAvailable(usrSubs.FirstName);

            Assertion.verifyEqual(isPartUsernameAvailable, true,
                    "Verify If partyUsername: " + usrSubs.FirstName + " is available on Admin trail page for selected Date for Currency Provider:", TC554);
        } catch (Exception e) {
            markTestAsFailure(e, TC554);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4)
    public void Test_04() {
        /**
         * Admin Trail Field Validation
         *
         * @throws Exception
         */
        ExtentTest TC555 = pNode.createNode("TC555", "Admin Trail file should be opened successfully and following fields should be displayed");
        try {
            Login.init(TC555).login(adminTrail);

            String fromDate = new DateAndTime().getDate(0);
            String toDate = new DateAndTime().getDate(0);

            AdminTrail_Page1 page = AdminTrail_Page1.init(TC555);

            page.navigateToLink();
            page.selectDomainByValue(DataFactory.getDomainCode(Constants.WHOLESALER));
            Thread.sleep(2000);
            page.selectCategoryByValue(Constants.WHOLESALER);
            page.fromDateSetText(fromDate);
            page.toDateSetText(toDate);
            page.submitButtonClick();

            Object[] actualLabelText = PageInit.fetchLabelTexts("//table[@id= 'table']/thead/tr/th");
            Object[] expectedLabelText = UserFieldProperties.getLabels("adm.trial.page2");

            if (Arrays.equals(actualLabelText, expectedLabelText)) {
                TC555.pass("All Labels Validated Successfully");
            } else {
                TC555.fail("Label Validation Failed");
                Utils.captureScreen(TC555);
            }
        } catch (Exception e) {
            markTestAsFailure(e, TC555);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
