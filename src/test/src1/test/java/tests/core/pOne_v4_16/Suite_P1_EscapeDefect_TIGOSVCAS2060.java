package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Duplicate External Code
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/13/2018.
 */
public class Suite_P1_EscapeDefect_TIGOSVCAS2060 extends TestInit {

    private User sub1, sub2;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            SystemPreferenceManagement.init(setup)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "1");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.ESCAPEDEFECT})
    public void P1_TC_063() {
        ExtentTest P1_TC_063 = pNode.createNode("P1_TC_063", "Two different MSISDN should not be registered into TM with same external code");
        try {
            sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            sub2 = new User(Constants.SUBSCRIBER);
            sub2.setExternalCode(sub1.ExternalCode);

            startNegativeTest();
            TxnResponse result = Transactions.init(P1_TC_063)
                    .selfRegistrationForSubscriberWithKinDetails(sub2);

            result.assertStatus("90004");
            result.assertMessage("duplicate.externalcode");

        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_063);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(tearDown)
                    .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "2");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
