package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.channelEnquiry.ChannelEnquiry_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that while doing enquiry of operator to channel transfer Wallet Type
 should be displayed for both Normal and commission wallet once submit button is clicked
 and again on click of Back button in the dropdown of the wallet type
 * Author Name      : Nirupama mk
 * Created Date     : 06/02/2018
 */

public class Suite_P1_O2C_TransferEnquiry extends TestInit {
    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.O2C})
    public void P1_TC_017() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_017",
                " To verify that while doing enquiry of operator to channel transfer Wallet Type\n" +
                        " should be displayed for both Normal and commission wallet once submit button is clicked\n" +
                        " and again on click of Back button in the dropdown of the wallet type");

        try {

            User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            OperatorUser o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            OperatorUser o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");
            Wallet wNormal = DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL);
            Wallet wCommission = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);

            Login.init(t1).login(o2cInitiator);

            //O2C for Commission wallet
            String transID = TransactionManagement.init(t1).
                    initiateO2CForSpecificWallet(chUsr,
                            wCommission.WalletName,
                            DataFactory.getDefaultProvider().ProviderId,
                            "1", null);

            if (transID != null) {
                Login.init(t1).login(o2cApprover1);
                TransactionManagement.init(t1).o2cApproval1(transID);
            }

            //O2CTransfer Enquiry of Commission wallet
            /*ChannelEnquiry.init(t1).doO2CTransferEnquiryUsingMobileNumberforSpecificWallet(chUsr.MSISDN,
                    wCommission.WalletId, transID);*/
            doO2CTransferEnquiryUsingMobileNumberFomCommisionWallet(chUsr.MSISDN, transID, t1);
            ChannelEnquiry_Page1 page = ChannelEnquiry_Page1.init(t1);

            page.clickOnBack();
            page.clickOnBack1();

            Utils.putThreadSleep(Constants.WAIT_TIME);
            List<String> walletOptions = page.getWalletOptions();
            Assertion.verifyListContains(walletOptions, wNormal.WalletName, "Verify that Normal Wallet is available in the Wallet Dropdown", t1);
            Assertion.verifyListContains(walletOptions, wCommission.WalletName, "Verify that Commission Wallet is available in the Wallet Dropdown", t1);

        } catch (Exception e) {
            pNode.fail("Element not Present");
            Assertion.raiseExceptionAndStop(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.O2C})
    public void P1_TC_059() throws Exception {

        ExtentTest t1 = pNode.createNode("P1_TC_059", " O2C transaction should be rejected ");
        try {

            User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            OperatorUser o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            OperatorUser o2cApprover1 = DataFactory.getOperatorUserWithAccess("O2C_APP1");

            Login.init(t1).login(o2cInitiator);

            String txnId = TransactionManagement.init(t1)
                    .initiateO2C(chUsr, "50", "50");

            if (txnId != null) {
                TransactionManagement.init(t1)
                        .o2cApproval1(txnId, true);
                Utils.captureScreen(t1);
            } else {
                t1.fail("Failed to initiate O2C Transaction!");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private void doO2CTransferEnquiryUsingMobileNumberFomCommisionWallet(String msisdn, String txnID, ExtentTest test) throws Exception {

        try {
            OperatorUser optUsr = DataFactory.getOperatorUsersWithAccess("O2C_ENQ").get(0);
            Login.init(test).login(optUsr);

            ChannelEnquiry_Page1 page = new ChannelEnquiry_Page1(test);

            page.navChannelEnquiry();
            page.selectMFSProvider(DataFactory.getDefaultProvider().ProviderId);
            page.selectPayInstrument(Constants.PAYINST_WALLET_CONST);

            page.selectPayInstrumentType(Constants.COMMISSION_WALLET);

            page.setMSISDN(msisdn);
            Utils.putThreadSleep(1000);
            page.clickOnSubmit();

            if (ConfigInput.isAssert) {
                page.selectTxnID(txnID);
                String webStatus = page.extractTxnStatus(txnID);
                page.clickOnFinalSubmit();
                String dbStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnID);
                Assertion.verifyEqual(webStatus, dbStatus, "Verify Transaction Status", test, true);
            }
        } catch (Exception e) {

            Assertion.raiseExceptionAndContinue(e, test);
        }
    }
}