package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.pageObjects.tcp.AddCustomerTCP_pg1;
import framework.pageObjects.tcp.AddCustomerTCP_pg2;
import framework.util.common.Assertion;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Customer Cumulative TCP Labels and Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/1/2018.
 */

public class Suite_P1_CustomerCumulativeTCP extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.TCP})
    public void Test_01() throws Exception {

        FunctionLibrary fl = new FunctionLibrary(DriverFactory.getDriver());

        ExtentTest t015 = pNode.createNode("P1_TC_015", "Verify Transfer Control home page, verify for various options available such as edit delete and add new profile.");
        ExtentTest t413 = pNode.createNode("P1_TC_413", "To verify that valid user should be able to initiate transfer control profile creation/edit");
        ExtentTest t414 = pNode.createNode("P1_TC_414", "Verify Valid user can set threshold for service at grade level and asked for confirmation.");
        ExtentTest t415 = pNode.createNode("P1_TC_415", "Verify that once confirming the TCP creation, Tcp is pending for approval");
        ExtentTest t416 = pNode.createNode("P1_TC_416", "Verify the newly created TCP is shown in the list of pending for approval");

        // create TCP object
        try {
            Login.init(t015).loginAsSuperAdmin("TCP_USER");

            AddCustomerTCP_pg1 page = AddCustomerTCP_pg1.init(t015);
            page.navAddCustomerTcp();

            boolean view = Utils.checkElementPresent("(//abbr[@title = 'View'])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(view, true, "View Button Validation", t015);

            boolean edit = Utils.checkElementPresent("(//abbr[@title = 'Edit'])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(edit, true, "Edit Button Validation", t015);

            boolean delete = Utils.checkElementPresent("(//abbr[@title = 'Delete'])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(delete, true, "Delete Button Validation", t015);

            boolean copy = Utils.checkElementPresent("(//abbr[@title = 'Copy'])[1]", Constants.FIND_ELEMENT_BY_XPATH);
            Assertion.verifyEqual(copy, true, "Copy Button Validation", t015, true);

            /*
            click on edit button of an existing tcp and validate fields present(TC071)
             */
            page.availableOperations(UserFieldProperties.getField("edit")).click();
            Thread.sleep(2000);


            /**
             * tEST 413
             */
            page = AddCustomerTCP_pg1.init(t413);

            boolean domainField = Utils.checkElementPresent("domain", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(domainField, true, "Domain Dropdown Field Validation", t413);

            boolean categoryField = Utils.checkElementPresent("category", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(categoryField, true, "Category Dropdown Field Validation", t413);

            boolean registrationTypeField = Utils.checkElementPresent("registrationType", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(registrationTypeField, true, "registration Type Dropdown Field Validation", t413);

            boolean profile_nameField = Utils.checkElementPresent("profile_name", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(profile_nameField, true, "Profile Name Field Validation", t413, true);

            page.clickEditButton();

            /**
             * Test 414
             */
            page = AddCustomerTCP_pg1.init(t414);
            page.set_cumulative_count_per_week_debit();
            Thread.sleep(Constants.TWO_SECONDS);

            AddCustomerTCP_pg2 page2 = AddCustomerTCP_pg2.init(t414);
            page2.submit();
            Thread.sleep(Constants.MAX_WAIT_TIME);

            Assertion.verifyEqual(fl.elementIsDisplayed(page2.Confirm), true, "Verify that User is asked for a confirmation", t414, true);

            /**
             * test 415
             */
            page2 = AddCustomerTCP_pg2.init(t415);
            page2.confirm();
            Thread.sleep(5000);
            Assertion.verifyEqual(fl.elementIsDisplayed(driver.findElement(By.id("add_action_message"))), true,
                    "Verify that Transfer control profile is successfully created/edited", t415, true);

            String expectedInitiatedMessage = "category has been initiated and sent for approval";
            Assertion.verifyContains(page.addActionMessage.getText(), expectedInitiatedMessage, "TCP initiated", t415);

            //approving the modified tcp and validating success message(TC076, TC077, TC078)

            /**
             * test 416
             */
            TCPManagement.init(t416).approveAllPendingCustomerTCP();
            Assertion.verifyEqual(fl.elementIsDisplayed(driver.findElement(By.id("action_message"))), true,
                    "Verify that success message is shown when approving TCP", t416, true);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
