package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Pending Transaction Registration
 * Author Name      : Gurudatta Praharaj
 * Date             : 6/21/2018.
 */
public class Suite_P1_PendingTxnRegistration extends TestInit {

    private OperatorUser initTxnCorrection, appTxnCorrection;
    private User subSender, subReceiver;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        try {

            subSender = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            subReceiver = new User(Constants.SUBSCRIBER);

            initTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");
            appTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRAPP");

            ServiceCharge charge = new ServiceCharge(Services.P2PNONREG, subSender, initTxnCorrection,
                    null, null,
                    null, null);

            TransferRuleManagement.init(pNode)
                    .configureTransferRule(charge);

            charge = new ServiceCharge(Services.TXN_CORRECTION, initTxnCorrection, subReceiver,
                    null, null,
                    null, null);

            TransferRuleManagement.init(pNode)
                    .configureTransferRule(charge);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE})
    public void Test_01() {

        ExtentTest P1_TC_053 = pNode.createNode("P1_TC_053", "To verify that the Transaction status should be TS in the respective " +
                "tables in DB after performing transaction correction for two step service P2P On the Fly .");
        /*
        Perform one p2p transaction to a non registered customer.
        reverse the transaction.
        validate whether the Transaction status in
        MTX_OnFly_Reg_Detail table has been changed to PS from PI.
         */
        try {
            BigDecimal p2pAmt = new BigDecimal("1");

            SfmResponse status = Transactions.init(P1_TC_053)
                    .p2pNonRegTransaction(subSender, subReceiver, p2pAmt);

            String txnID = status.TransactionId;

            String serviceID = Transactions.init(P1_TC_053)
                    .initiateTransactionReversalByOperator(initTxnCorrection, txnID,
                            Constants.NORMAL_WALLET, Services.P2PNONREG,
                            "true", "true").ServiceRequestId;

            Transactions.init(P1_TC_053)
                    .approveTransactionReversal(serviceID, appTxnCorrection);

            String status_header_table = MobiquityGUIQueries.dbGetTransactionStatus(txnID);

            DBAssertion.verifyDBAssertionEqual(status_header_table, Constants.TXN_STATUS_SUCCESS,
                    "P2P Transaction Success", P1_TC_053);

//            if (status_header_table.equals(Constants.TXN_STATUS_SUCCESSSuite_P1_PendingTxnRegistration)) {
//                P1_TC_053.pass(txnID + " transaction is successful: " + status_header_table);
//            } else {
//                P1_TC_053.fail(txnID + " transaction is successful: " + status_header_table);
//            }
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_053);
        }
    }
}

