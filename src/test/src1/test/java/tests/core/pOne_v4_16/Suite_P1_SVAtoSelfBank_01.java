package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.svaToOwnBankTransfer.svaToOwnBankTransfer_Page1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that proper prompt should get displayed
 once user click on Cancel button on confirmation screen of  transfer now page
 * Author Name      : Nirupama mk
 * Created Date     : 09/02/2018
 */
public class Suite_P1_SVAtoSelfBank_01 extends TestInit {

    private User chUser;
    private OperatorUser optUser;
    private String transferAmount = "5";
    private CurrencyProvider providerOne;
    private Wallet walletOne;
    private ServiceCharge sCharge;
    private String bankId;

    @BeforeClass(alwaysRun = true)
    public void precondition() {
        ExtentTest eSetp = pNode.createNode("Setup", "Initiating Test");
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            optUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            providerOne = DataFactory.getDefaultProvider();
            walletOne = DataFactory.getDefaultWallet();
            bankId = defaultBank.BankID;

            sCharge = new ServiceCharge(Services.SVA_TO_BANK, chUser, chUser, null, null, null, null);

            TransferRuleManagement.init(eSetp)
                    .configureTransferRule(sCharge);

            TransactionManagement.init(eSetp)
                    .makeSureChannelUserHasBalance(chUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.SVA})
    public void P1_TC_164() throws MoneyException, Exception {

        ExtentTest t4 = pNode.createNode("P1_TC_164",
                "To verify that a valid user can not transfer or " +
                        "receive money if transfer rule is suspended for " +
                        "SVA to Own Bank Account Transfer for Biller service");

        String providerName = DataFactory.getDefaultProvider().ProviderName;
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransferRuleManagement.init(t4)
                    .initiateApproveSuspenedTransferRule(sCharge, Constants.SUSPENDED, true);

            Login.init(t4).login(chUser);
            startNegativeTest();

            Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(chUser, DataFactory.getDefaultProvider().ProviderId, bankId);
            String subaccount = AccountNum1.get("ACCOUNT_NO");

            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String banAccNum = d1.decrypt(subaccount);

            initiateSVAtoBank(chUser, banAccNum,
                    Constants.SVA_TRANSFER_NOW,
                    providerName,
                    DataFactory.getDefaultWallet().WalletName,
                    defaultBank.BankName,
                    "5", t4);
            Assertion.verifyErrorMessageContain("error.transferRule.unavaible",
                    "SVA to bank cannot complete as transfer rule is suspended", t4);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            TransferRuleManagement.init(t4)
                    .resumeTransferRule(sCharge);
            Assertion.finalizeSoftAsserts();
        }
    }


    //@Test(priority = 1)
    public void TC326() throws Exception {

        ExtentTest t1 = pNode.createNode("TC326", " To verify that proper prompt should get displayed" +
                " once user click on Cancel button on confirmation screen of transfer now page");

        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            Login.init(t1).login(chUser);
            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(t1);
            initiateSVAtoBankTransfer(page1);
            page1.setTransferAmount(transferAmount);

            // click on cancel button, accept the alert
            page1.clickCancel();
            if (wait.until(ExpectedConditions.alertIsPresent()) != null) {
                t1.pass("Successfully verified that the alert is open, once trying to cancel the Transaction");
                AlertHandle.rejectAlert(t1);
            }

            // click on cancel button, reject the alert
            page1.clickCancel();
            AlertHandle.acceptAlert(t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    //@Test(priority = 2)
    public void test_02() throws Exception {

        ExtentTest t2 = pNode.createNode("TC351", " To verify that fixed amount should not accept" +
                " alplhabets and special characters while doing Transfer Now transaction in web by logged-in channel user");

        Login.init(pNode).login(chUser);
        svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(pNode);
        initiateSVAtoBankTransfer(page1);

        // invalid amount - alphabets and specal char
        page1.setTransferAmount("Hello@");
        page1.clickSubmit();
        Assertion.verifyErrorMessageContain("si.error.improper.amount", "Initiate SVA to Bank", t2);

    }

    //@Test(priority = 3)
    public void TC325() throws MoneyException, Exception {

        ExtentTest t3 = pNode.createNode("TC325", " To verify that If user selects the option to go" +
                " back in transfer now confirmation page, system should take the user to previous screen with all the\n" +
                " fields with the latest filled values.");
        try {
            Login.init(t3).login(chUser);
            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(t3);

            Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(chUser, DataFactory.getDefaultProvider().ProviderId, bankId);
            String subaccount = AccountNum1.get("ACCOUNT_NO");

            //decrypt the account no.
            DesEncryptor d1 = new DesEncryptor();
            String banAccNum = d1.decrypt(subaccount);

            startNegativeTestWithoutConfirm();
            initiateSVAtoBank(chUser, banAccNum,
                    Constants.SVA_TRANSFER_NOW,
                    DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDefaultWallet().WalletName,
                    defaultBank.BankName,
                    "5", t3);


            page1.clickBack();

            String txt = driver.findElement(By.id("tranferBalance")).getAttribute("value");

            if (transferAmount.equals(txt)) {
                t3.pass(" transfer amount is same as defined : " + txt);
                t3.pass("Verified successfully that user is navigated back to the previous page");
            } else
                t3.fail("Failed to verify that on click on Back button user is navigated back to previous page");

            Utils.captureScreen(t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
    }

    //@Test(priority = 5)
    public void TC351() throws Exception {

        ExtentTest t5 = pNode.createNode("TC351",
                "To verify that fixed amount should not accept Zero as transfer amount");
        try {
            Login.init(t5).login(chUser);
            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(t5);

            initiateSVAtoBankTransfer(page1);

            // invalid amount - Zero
            // shound display different error msg
            page1.setTransferAmount("0");
            page1.clickSubmit();
            Assertion.verifyErrorMessageContain("error.amount.greater.than.zero",
                    "To verify that fixed amount should not accept Zero as transfer amount", t5);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }

    }

    //@Test(priority = 6, enabled = false)
    public void TC313() throws Exception {

        ExtentTest t6 = pNode.createNode("TC313", "To verify that all Remarks field characters are" +
                " getting visible in 'SVA to Own Bank Transfer' page.");

        Login.init(t6).login(chUser);
        svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(t6);
        initiateSVAtoBankTransfer(page1);
        page1.setTransferAmount(transferAmount);
        page1.enterRemarks();
        page1.clickSubmit();

        if (driver.findElement(By.id("remarks")).isDisplayed()) {
            t6.pass(" Element displayed ");
        } else {
            t6.fail(" Element not dispalyed");
        }
    }

    private void initiateSVAtoBankTransfer(svaToOwnBankTransfer_Page1 page1) throws Exception {

        try {
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(Constants.SVA_TRANSFER_NOW);
            page1.clickNext();

            page1.selectProviderName(providerOne.ProviderName);
            page1.selectWallet(walletOne.WalletName);
            page1.selectBankName(defaultBank.BankName);
            page1.selectAccountByIndex();
            page1.selectAmountType();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String initiateSVAtoBank(User user, String bankAccNum, String transferType, String provider, String walletName, String bankName, String amount, ExtentTest test) throws Exception {
        String transID = null;
        try {
            Login.init(test).login(user);

            svaToOwnBankTransfer_Page1 page1 = svaToOwnBankTransfer_Page1.init(test);

            //TODO - Input values to fetch from sheet
            page1.navigateToSVA2OwnBankTransferInWeb();
            page1.selectTransferType(transferType);
            page1.clickNext();

            page1.selectProviderName(provider);
            page1.selectWallet(walletName);
            page1.selectBankName(bankName);
            Thread.sleep(2000);
            page1.selectAccountNum(bankAccNum);
            page1.selectAmountType();
            page1.setTransferAmount(amount);
            page1.clickSubmit();
            if (ConfigInput.isConfirm) {
                page1.clickConfirm();
                Utils.putThreadSleep(10000);
            }

            if (ConfigInput.isAssert) {
                String msg = Assertion.getActionMessage();
                transID = msg.split("with ")[1].trim().split(" ")[0];
                Assertion.verifyActionMessageContain("sva.ambiguous.message", "Initiate SVA to Bank", test, transID);

            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, test);
        }
        return transID;
    }
}
