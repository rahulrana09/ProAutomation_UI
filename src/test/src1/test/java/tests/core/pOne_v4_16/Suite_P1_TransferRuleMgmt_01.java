package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that active network admin can reject\n"+
 " the add initiated Transfer Rules between the same domains.
 * Author Name      : Nirupama mk
 * Created Date     : 19/02/2018
 */

public class Suite_P1_TransferRuleMgmt_01 extends TestInit {

    private static OperatorUser usrTRuleCreator, usrTRuleApprover;
    private String payIns = Constants.SALARY_WALLET, service;
    private ServiceCharge sChargeAccClose;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Make sure that the Transfer Rule under test is initially Deleted");
        try {
            usrTRuleCreator = DataFactory.getOperatorUserWithAccess("T_RULES");
            usrTRuleApprover = DataFactory.getOperatorUserWithAccess("T_RULESAPP");

            sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT,
                    new User(Constants.SUBSCRIBER), new User(Constants.WHOLESALER), null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(sChargeAccClose)
                    .deleteTransferRule(sChargeAccClose);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.TRANSFER_RULE})
    public void Test_01() throws Exception {


        ExtentTest t1 = pNode.createNode("P1_TC_439",
                "To verify that active network admin can reject " +
                        " the add initiated Transfer Rules between the same domains.");
        try {
            Login.init(t1).login(usrTRuleCreator);

            TransferRuleManagement.init(t1)
                    .setSenderReceiverDetailsForTransferRule(sChargeAccClose)
                    .setCategoryDetailsForTransferRule(sChargeAccClose)
                    .addInitiateTransferRule(sChargeAccClose);

            Login.init(t1).forceLogin(usrTRuleApprover);

            //reject transfer Rule
            TransferRuleManagement.init(t1)
                    .approveRejectTransferRule(sChargeAccClose.TransferRuleID, false);

            ExtentTest t2 = pNode.createNode("P1_TC_440", "To verify that active network admin can approve" +
                    " modified initiated Transfer Rules between the two Domains for particular MFS provider.");

            Login.init(t2).forceLogin(usrTRuleCreator);
            TransferRuleManagement.init(t2)
                    .setSenderReceiverDetailsForTransferRule(sChargeAccClose)
                    .setCategoryDetailsForTransferRule(sChargeAccClose)
                    .addInitiateTransferRule(sChargeAccClose);

            Login.init(t2).forceLogin(usrTRuleApprover);
            //Approve transfer Rule
            TransferRuleManagement.init(t2)
                    .approveRejectTransferRule(sChargeAccClose.TransferRuleID, true);

            // Modify Transfer rule and approve
            Login.init(t2).forceLogin(usrTRuleCreator);
            TransferRuleManagement.init(t2)
                    .modifyTransferRule(sChargeAccClose);

            Login.init(t2).forceLogin(usrTRuleApprover);
            TransferRuleManagement.init(t2)
                    .approveRejectTransferRule(sChargeAccClose.TransferRuleID, true);

            ExtentTest t3 = pNode.createNode("P1_TC_444", "To verify that network admin can approve" +
                    " an suspend initiated transfer rule.");

            // Modify Transfer rule to Suspend Transfer Rule and approve
            Login.init(t3).forceLogin(usrTRuleApprover);
            TransferRuleManagement.init(t3)
                    .initiateApproveSuspenedTransferRule(sChargeAccClose, "Suspended", true);
//
            TransferRuleManagement.init(t3)
                    .resumeTransferRule(sChargeAccClose);

            ExtentTest t4 = pNode.createNode("P1_TC_441", "To verify that active network admin can accept\n" +
                    " deletion of the initiated Transfer Rules between the Domains for particular MFS provider.");

            // initiate delete and approve transfer rule
            TransferRuleManagement.init(t4)
                    .deleteTransferRule(sChargeAccClose);

            ExtentTest t5 = pNode.createNode("P1_TC_443", "To verify that network admin" +
                    " can modify the Approval Limit of any existing Transfer Rules.");

            Login.init(t5).forceLogin(usrTRuleApprover);

            TransferRuleManagement.init(t5)
                    .setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                            DataFactory.getDomainName(Constants.WHOLESALER),
                            DataFactory.getCategoryName(Constants.WHOLESALER),
                            Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}