package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by gurudatta.praharaj on 6/15/2018.
 */
public class Suite_P1_CAS2019_ASIACELL_Modifying_subscrbier_Details_preserved extends TestInit {

    private User chUsr, sub;

    //Covered In P1_TC_591
    @Test(priority = 1, groups = {FunctionalTag.P1CORE}, enabled = false)
    public void Test01() throws Exception {
        ExtentTest TC719 = pNode.createNode("P1_TC_079", "Subscriber Should successfully modify Initiated");
        ExtentTest TC720 = pNode.createNode("P1_TC_080", "Subscriber Should successfully Approved");
        try {
            chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            Login.init(TC719).login(chUsr);

            SubscriberManagement.init(TC719).modifySubscriber(chUsr, sub);

            SubscriberManagement.init(TC720)
                    .modifyApprovalSubs(sub);


        } catch (Exception e) {
            markTestAsFailure(e, TC719);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
