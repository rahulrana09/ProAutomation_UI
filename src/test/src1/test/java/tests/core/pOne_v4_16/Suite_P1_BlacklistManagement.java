package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.blackListManagement.BlackListManagement;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.blacklist.CustomerBlacklist_Page;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.OracleDB;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Customer Black List Fields and Functionality Validation
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 2/8/2018.
 */

public class Suite_P1_BlacklistManagement extends TestInit {
    private OperatorUser optUsr;
    private User sub;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Get operator User with Role BLK_ABL. " +
                "Make Sure service Charge ACQFEE is configured. " +
                "Configure Wallet Preference for SUBS.");

        try {
            optUsr = DataFactory.getOperatorUserWithAccess("BLK_ABL");
            Login.init(setup).login(optUsr);

            sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(setup)
                    .createDefaultSubscriberUsingAPI(sub);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.BLACKLIST_MANAGEMENT,
            FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        /*
        Login as operator user
        navigate to subscriber management
        navigate to add customer to blacklist
        verify fields and
        verify Network admin must be able to blacklist a customer by subscriber management module
         */
        ExtentTest P1_TC_556 = pNode.createNode("P1_TC_556", "To verify that network admin should be able to blacklist a customer by subscriber management module");
        ExtentTest P1_TC_557 = pNode.createNode("P1_TC_557", "To verify that Black Listing should be done on basis of the following parameters–First Name , Last Name ,Date Of Birth");
        ExtentTest P1_TC_182 = pNode.createNode("P1_TC_182", "To verify that an option to upload the black listed customers in the mobiquity® system should be placed under the Subscriber Management module, via network admin");
        ExtentTest P1_TC_183 = pNode.createNode("P1_TC_183", "To verify that System should check if the Customer is already Blacklisted. If not then it will black list the Customer");
        try {
            Login.init(P1_TC_556).login(optUsr);

            CustomerBlacklist_Page page = new CustomerBlacklist_Page(P1_TC_556);

            page.navigateToLink();

            boolean status = Utils.checkElementPresent("addBlackList_add_firstName", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "First Name Field", P1_TC_557);

            status = Utils.checkElementPresent("addBlackList_add_lastName", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "Last Name Field", P1_TC_557);

            status = Utils.checkElementPresent("dojo.dateOfBirth", Constants.FIND_ELEMENT_BY_NAME);
            Assertion.verifyEqual(status, true, "DOB Field", P1_TC_557);

            status = Utils.checkElementPresent("addBlackList_add_bulkUploadFile", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "Upload File Field", P1_TC_182, true);

            BlackListManagement.init(P1_TC_183)
                    .doCustomerBlacklist(sub.FirstName, sub.LastName, sub.DateOfBirth);

            Utils.putThreadSleep(Constants.TWO_SECONDS);

            Assertion.verifyEqual(isSubsBlacklisted(sub.FirstName), true,
                    "User entry found in MTX_CUSTOMER_BLACKLIST table", P1_TC_183);
        } catch (Exception e) {
            markTestAsFailure(e, P1_TC_556);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private boolean isSubsBlacklisted(String firstName) {
        String res = null;
        String query = "select blacklist_id from mtx_party_customer_blacklist where first_name = '" + firstName + "'";
        Utils.putThreadSleep(Constants.TWO_SECONDS);
        OracleDB dbConn = new OracleDB();
        try {
            ResultSet result = dbConn.RunQuery(query);
            while (result.next()) {
                res = result.getString("BLACKLIST_ID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res != null;
    }
}
