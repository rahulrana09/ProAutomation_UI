package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.transactionCorrection.TxnCorrectionApproval_page1;
import framework.util.common.*;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;

/*
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : P1: Transaction corrections
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 02/2/2018.
 */

public class Suite_P1_TransactionCorrection extends TestInit {
    private User usrSubs, usrWhs, usrRetailer;
    private CurrencyProvider defaultProvider;
    private OperatorUser initTxnCorrection, appTxnCorrection;
    private FunctionLibrary fl;

    @BeforeClass(alwaysRun = true)
    public void preReq() throws Exception {
        fl = new FunctionLibrary(DriverFactory.getDriver());
        ExtentTest eSetup = pNode.createNode("Setup", "Make sure that the Transaction correction service charge " +
                "is configured in the system. Make sure that Channel user has sufficient Balance");
        try {
            // users from app data
            defaultProvider = DataFactory.getDefaultProvider();
            usrWhs = DataFactory.getChannelUserWithAccess("CIN_WEB");
            usrSubs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            initTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRECTION");
            appTxnCorrection = DataFactory.getOperatorUserWithAccess("TXN_CORRAPP");

            // Service charge object
            ServiceCharge sCharge1 = new ServiceCharge(Services.TXN_CORRECTION, usrSubs, usrWhs, null, null, null, null);

            // make sure that the service charge is configured
            ServiceChargeManagement.init(eSetup)
                    .configureServiceCharge(sCharge1);

            TransactionManagement.init(eSetup)
                    .makeSureChannelUserHasBalance(usrWhs);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.P1CORE, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.MONEY_SMOKE})
    public void P1_TC_046() throws Exception {
        ExtentTest t1 = pNode.createNode("P1_TC_046", "Verify Transaction Correction Approval Page");

        t1.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", Services.CASHIN));

        Login.init(t1)
                .login(usrWhs);

        String txnId = TransactionManagement.init(t1)
                .performCashIn(usrSubs, Constants.MIN_CASHIN_AMOUNT, defaultProvider.ProviderName);

        if (txnId == null) {
            markTestAsFailure("Txn Id is null, exiting the test", t1);
            Assert.fail("Txn Id is null, exiting the test");
        }

        t1.info(txnId);

        //initiate Transaction Correction
        String txnCorrectionID = TransactionCorrection.init(t1)
                .initiateTxnCorrection(txnId, false, false);

        if (txnCorrectionID == null) {
            markTestAsFailure("Txn Correction Id is null, exiting the test", t1);
            Assert.fail("Txn Correction Id is null, exiting the test");
        }

        // Login as Transaction correction approver
        Login.init(t1).login(appTxnCorrection);

        TxnCorrectionApproval_page1 page = TxnCorrectionApproval_page1.init(t1);

        try {
            page.navigateToTxnCorrApproval();
            page.selectTransactionId(txnCorrectionID);
            page.clickSubmit();

            Utils.captureScreen(t1);

            // verify the Approval Page
            Assertion.verifyEqual(page.getTransactionIdUI(), txnCorrectionID, "Verify the Transaction Id", t1);
            Assertion.verifyEqual(page.getTransferAmountUI(), Constants.MIN_CASHIN_AMOUNT, "Verify the Transaction amount", t1);
            Assertion.verifyEqual(page.getActualTransactionIdUI(), txnId, "Verify the Transaction correction Id", t1);
            Assertion.verifyEqual(page.getPayeeDomainUI(), usrWhs.DomainName, "Verify the Payee Domain Name", t1);
            Assertion.verifyEqual(page.getPayeeMsisdnUI(), usrWhs.MSISDN, "Verify the Payee MSISDN", t1);
            Assertion.verifyEqual(page.getPayerDomainUI(), usrSubs.DomainName, "Verify the Payer Domain name", t1);
            Assertion.verifyEqual(page.getPayerMsisdnUI(), usrSubs.MSISDN, "Verify the Payer MSISDN", t1);

            // Verify The UI Actions available
            Assertion.verifyEqual(fl.elementIsDisplayed(page.approveButton), true,
                    "Verify that Option for approving the transaction correction is Available", t1);
            Assertion.verifyEqual(fl.elementIsDisplayed(page.rejectButton), true,
                    "Verify that Option for Rejecting the transaction correction is available", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
