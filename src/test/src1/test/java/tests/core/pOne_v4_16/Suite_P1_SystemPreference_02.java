package tests.core.pOne_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.pageObjects.preferences.NotificationConfiguration_pg1;
import framework.pageObjects.preferences.SystemPreferences_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify System Preference, in the Sub Menu
 * Author Name      : Nirupama mk
 * Created Date     : 12/02/2018
 */
public class Suite_P1_SystemPreference_02 extends TestInit {

    private UserFieldProperties fields;
    private OperatorUser opUsr;
    private WebDriverWait wait;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        opUsr = DataFactory.getOperatorUserWithAccess("PREF001");
        wait = new WebDriverWait(DriverFactory.getDriver(), 8);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.SERVICE_CHARGE_MANAGEMENT,
            FunctionalTag.P1CORE, FunctionalTag.SYSTEM_PREFERENCES})
    public void TC031() throws Exception {
        ExtentTest t1 = pNode.createNode("TC031", " To verify System Preference, in the Sub Menu");
        try {
            Login.init(t1).login(opUsr);
            SystemPreferences_Page1 page1 = new SystemPreferences_Page1(t1);

            t1.info("Verify the Left Navigation Links under the preferences are shown");

            page1.navigateToSystemPreference();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            boolean status = Utils.checkElementPresent("MPREF_PREF001", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "System Preference Link Is Available", t1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.P1CORE, FunctionalTag.SYSTEM_PREFERENCES})
    public void TC032() throws Exception {

        ExtentTest t2 = pNode.createNode("TC032", "Verify the SMS Configuration & Email Configuration Fields");

        try {
            Login.init(t2).login(opUsr);
            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(t2);

            page1.navigateToSMSConfig();
            Utils.captureScreen(t2);
            t2.info("Verify the SMS Configuration Page");
            verifyIfLabelExist(UserFieldProperties.getField("sms.config"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("sms.config.msg"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("sms.config.lng"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("sms.config.trna"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("sms.config.message"), t2);

            page1.clickEmailRadioBtn();
            Utils.captureScreen(t2);
            t2.info("Verify the Email Configuration Page");
            verifyIfLabelExist(UserFieldProperties.getField("email.s"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("email.l"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("email.sub"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("email.p"), t2);
            verifyIfLabelExist(UserFieldProperties.getField("email.b"), t2);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.P1CORE, FunctionalTag.SYSTEM_PREFERENCES})
    public void TC033() throws Exception {

        ExtentTest t4 = pNode.createNode("TC033", "Email Configuration: Verify Message specified for a Service");

        try {
            Login.init(t4).login(opUsr);
            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(t4);

            page1.navigateToSMSConfig();
            page1.clickEmailRadioBtn();

            page1.selectServiceForEmailByValue("Add Channel User Approval");
            page1.selectLanguage();

            Assertion.verifyMessageContain(page1.getEmailSubject(),
                    "sms.config.email.subject.approve.channeluser",
                    "Verify that Message Subject for Selected Service is shown", t4);

            Assertion.verifyMessageContain(page1.getMessageTextArea(),
                    "sms.config.email.subject.approve.channeluser.message",
                    "Verify that Message Subject for Selected Service is shown", t4);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.P1CORE, FunctionalTag.SYSTEM_PREFERENCES})
    public void TC027() throws Exception {

        ExtentTest t5 = pNode.createNode("P1_TC_023",
                "SMS Configuration: Verify on selecting Message Code corresponding Message is show");

        try {
            Login.init(t5).login(opUsr);
            NotificationConfiguration_pg1 page1 = new NotificationConfiguration_pg1(pNode);

            page1.navigateToSMSConfig();
            page1.selectServiceByValue("O2C");
            page1.selectMessageByValue("01043");
            page1.selectLanguage();
            Assertion.verifyMessageContain(page1.getMessageTextArea(),
                    "01043",
                    "Verify that Message for Selected Service code is shown", t5);

        } catch (Exception e) {
            markTestAsFailure(e, t5);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private void verifyLeftNavigation(String labelName, ExtentTest t1) throws IOException {
        try {
            wait.until(ExpectedConditions.
                    visibilityOf(DriverFactory.getDriver().findElement(By.xpath("//a[contains(text(),'" + labelName + "')]"))));
            t1.pass("Successfully verified the Link in Left Navigation- " + labelName);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            t1.fail("Failed to find the Link in Left Navigation - " + labelName);
            Utils.captureScreen(t1);
        }
    }

    private void verifyIfLabelExist(String labelName, ExtentTest t1) throws IOException {
        try {
            wait.until(ExpectedConditions.
                    visibilityOf(DriverFactory.getDriver().findElement(By.xpath("//label[contains(text(),'" + labelName + "')]"))));
            t1.pass("Successfully verified the availability for Label - " + labelName);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            t1.fail("Failed to verify the availability for Label - " + labelName);
            Utils.captureScreen(t1);
        }
    }

}




