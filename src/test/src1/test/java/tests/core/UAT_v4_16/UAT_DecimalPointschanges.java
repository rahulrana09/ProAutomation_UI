package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Transactions With Decimal Amount
 * Author Name      : Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_DecimalPointschanges extends TestInit {
    private User subUser, whsUser;
    private ServiceCharge cashIn;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        subUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        cashIn = new ServiceCharge(Services.CASHIN, whsUser, subUser, null, null, null, null);
    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest test = pNode.createNode("TUNG47074", " To verify that application should allow to enter " +
                "decimal while creating or modifying financial/non-financial service charge and tcp.\n" +
                "Also the system should allow to do transaction like Cash In, Cash Out, C2C, St ock transfer, " +
                "O2C etc in decimal points based on the preference CURRENCY_FACTOR");
        try {
            TransferRuleManagement.init(test).configureTransferRule(cashIn);

            UsrBalance balance = MobiquityGUIQueries
                    .getUserBalance(subUser, null, null);

            BigDecimal initialUserBalance = balance.Balance;

            Transactions.init(test)
                    .initiateCashIn(subUser, whsUser, new BigDecimal("10.5"));

            balance = MobiquityGUIQueries.getUserBalance(subUser, null, null);
            BigDecimal currentUserBalance = balance.Balance;

            BigDecimal amount = currentUserBalance.subtract(initialUserBalance);
            Assertion.verifyAccountIsCredited(initialUserBalance, currentUserBalance, amount,
                    "Cash In Done Successfully For Decimal Amount Of " + amount + " To The User: " + subUser.FirstName + "", test);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}