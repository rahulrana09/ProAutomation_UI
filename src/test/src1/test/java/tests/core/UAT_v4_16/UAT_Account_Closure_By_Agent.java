package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Account_Closure_By_Agent
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_Account_Closure_By_Agent extends TestInit {

    private User subUser, whsUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            subUser = new User(Constants.SUBSCRIBER);
            whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(setup).makeSureChannelUserHasBalance(whsUser);

            ServiceCharge accClosure = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subUser,
                    whsUser, null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(accClosure);

            ServiceChargeManagement.init(setup).
                    configureServiceCharge(accClosure);

            TransactionManagement.init(setup)
                    .performO2CforCommissionWallet(whsUser, "10", "Test");

            SubscriberManagement.init(setup).createDefaultSubscriberUsingAPI(subUser);
            Transactions.init(setup).initiateCashIn(subUser, whsUser, new BigDecimal("5"));
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.SUBSCRIBER_MANAGEMENT,
            FunctionalTag.SERVICE_CHARGE_MANAGEMENT,
            FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.MONEY_SMOKE})
    public void UAT_Account_Closure_By_Agent_TUNG53915() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG53915",
                "To verify that account closure by agent is successful and subscriber is deleted\n" +
                        "from the application and service charge is paid to the agent who has deleted the subscriber");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {

            //Check the initial balance of Channel user
            UsrBalance walletBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null);
            BigDecimal initialUserBalance = walletBalance.Balance;

            //Initiate Subscriber by Agent
            Login.init(t1).login(whsUser);
            String txnID = SubscriberManagement.init(t1).
                    deleteSubscriberByAgent(subUser, DataFactory.getDefaultProvider().ProviderId, false);

            //Confirmation by Subscriber since it is 2 step
            Transactions.init(t1).
                    accountClosurebyAgentConfirmation(subUser.MSISDN, txnID);

            //get the balance of Wholesaler
            walletBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null);
            BigDecimal currentUserBalance = walletBalance.Balance;

            //Check service charge is added to agent
            BigDecimal amount = currentUserBalance.subtract(initialUserBalance);

            Assertion.verifyAccountIsCredited(initialUserBalance, currentUserBalance, amount,
                    "Service Charge Of Amount: " + amount + " Added To Agent", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
