package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.Biller;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_RetailerBased_BillPayment_02
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Ambiguous_Transaction_01 extends TestInit {

    private Biller biller;
    private User sub;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest test = pNode.createNode("Setup", "Initiating Test");
        try {
            biller = BillerManagement.init(test)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_ADHOC);

            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge tRule = new ServiceCharge(Services.BILL_PAYMENT, sub, biller,
                    null, null, null, null);

            TransferRuleManagement.init(test)
                    .configureTransferRule(tRule);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void UAT_Ambiguous_Transaction_TUNG6825() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG6825",
                "To verify that Bill payment service is not successful and transaction status is TF in DB,\n" +
                        " if all the valid fields are not entered.");
        try {
            //get the FIC balance of biller
            BigDecimal preBillerFICbalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode).FIC;

            startNegativeTest();

            SfmResponse response = Transactions.init(t1)
                    .initiateOnlineBillerPayment(biller, sub, "0", false);

            //perform online bill payment
            String transferId = response.TransactionId;

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            //fetch transaction status from DB
            String txnStatus = MobiquityGUIQueries.dbGetTransactionStatus(transferId);
            t1.info("Transaction status is " + txnStatus);

            Assertion.verifyEqual(txnStatus, "TF", "transaction status is TF", t1);

            BigDecimal postBillerFICbalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode).FIC;

            Assertion.verifyEqual(preBillerFICbalance, postBillerFICbalance,
                    "there is no change in FIC balance before and after transaction", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
