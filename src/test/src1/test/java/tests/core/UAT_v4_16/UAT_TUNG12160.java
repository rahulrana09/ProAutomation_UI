package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Churn_Management_TUNG12171
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_TUNG12160 extends TestInit {

    private User subUser, whsUser;
    private OperatorUser optUser;

    @BeforeClass(alwaysRun = true)
    public void pre_Condition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {
            subUser = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(setup).createDefaultSubscriberUsingAPI(subUser);
            whsUser = CommonUserManagement.init(setup).getChurnedUser(Constants.WHOLESALER, null);
            optUser = DataFactory.getOperatorUserWithAccess("CHURN_SETTLE_INIT");

            //Modify TCP for O2S Transfer Daily limit
            TCPManagement.init(setup).editInstrumenttcp("90");
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void UAT_TUNG12160() {
        ExtentTest t1 = pNode.createNode("TUNG12160",
                "To verify that valid user should not able to perform settlement, if Receiver subscriber\n" +
                        " Weekly limit reached at TCP Customer/Role level for Settlement service.");
        try {
            Login.init(t1).login(optUser);
            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .initiateChurnSettlement(whsUser, subUser);

            Assertion.verifyErrorMessageContain("churn.settle.error.daily.limit", "Daily limit reached", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    @AfterClass(alwaysRun = true)
    public void post_Condition() throws Exception {

        ExtentTest tearDown = pNode.createNode("Teardown", "Concluding Test");
        try {
            //Resetting TCP for O2S Transfer back to its original value
            TCPManagement.init(tearDown).editInstrumenttcp("99999");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}