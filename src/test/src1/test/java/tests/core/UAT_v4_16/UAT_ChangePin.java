package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel user/subscriber will not be notified
 by a SMS if the service name of change PIN is not present in the tag list
 of services for which the message would be sent.
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */

public class UAT_ChangePin extends TestInit {


    private String requestType = "CCPNREQ", ExistingPrefCode;
    private OperatorUser usrCreator;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExistingPrefCode = MobiquityGUIQueries.fetchPayerPayeeSMSreq(requestType);
        usrCreator = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51580 ", " WEB : To verify that channel user/subscriber\n" +
                " will not be notified by a SMS if the service name of change PIN is not present in the tag list\n" +
                "of services for which the message would be sent.");

        MobiquityGUIQueries.updatePayerPayeeSMS_REQ(requestType, "Y");

        User chUser = new User(Constants.WHOLESALER);
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1).createChannelUser(chUser, false);

        SMSReader.init(t1).verifyNotificationContain(chUser.MSISDN, "pin.change");

        Utils.captureScreen(t1);
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        MobiquityGUIQueries.updatePayerPayeeSMS_REQ(requestType, ExistingPrefCode);
    }
}