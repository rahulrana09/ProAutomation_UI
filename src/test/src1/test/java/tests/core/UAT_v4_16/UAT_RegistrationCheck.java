package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.JsonPathOperation.*;
import static framework.util.jigsaw.CommonOperations.setTxnProperty;
import static framework.util.jigsaw.JigsawOperations.performServiceRequestAndWaitForSuccessForSyncAPI;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : ChannelUser and Subscriber have Same MSISDN
 * Author Name      : Saraswathi Annamalai, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */


public class UAT_RegistrationCheck extends TestInit {

    private User chUser, subUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setUp = pNode.createNode("Setup", "Setup Specific to this script");
        try {
            SystemPreferenceManagement.init(setUp)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

            chUser = new User(Constants.WHOLESALER);
            subUser = new User(Constants.SUBSCRIBER);

            ChannelUserManagement.init(setUp)
                    .createChannelUserDefaultMapping(chUser, false);

            TransactionManagement.init(setUp)
                    .initiateAndApproveO2C(chUser, "10", "Test");

            subUser.setMSISDN(chUser.MSISDN);

            Transactions.init(setUp).selfRegistrationForSubscriber(subUser);

            Transactions.init(setUp).
                    changeCustomerMpinTpin(subUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * //todo - Test Case is not complete
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.MONEY_SMOKE})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9064", "To verify that CashIn Transaction through\n" +
                " USSD should be performed successfully when channel user & subscriber having same MSISDN").assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(chUser, null, null);
            BigDecimal preBalance = balance.Balance;

            ExtentTest t2 = pNode.createNode("TUNG45137", "\"To verify Roles associated with Channel\n" +
                    " user should not get impacted on Roles associated with subscriber,Channel user & subscriber\n" +
                    " having same MSISDN when channel user as subscriber preference set as Y\n" +

                    "A. Cash IN should not be successful if Role of Cash IN service is not selected in Mobile Group\n" +
                    "Roles associated with the Channel user having same MSISDN as subscriber" +

                    "B. CashIN should be successful performed between subscriber having same MSISDN as channel user\n" +
                    " & different channel user having all the roles associated to him").assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0);

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            initiateCashIn(chUser, chUser, subUser.ExternalCode, new BigDecimal(1), t1);

            balance = MobiquityGUIQueries.getUserBalance(chUser, null, null);
            BigDecimal postBalance = balance.Balance;

            BigDecimal amount = postBalance.subtract(preBalance);
            Assertion.verifyAccountIsDebited(preBalance, postBalance, amount, "Account Debited", t2);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tearDown = pNode.createNode("teardown", "Concluding Test");
        try {
            SystemPreferenceManagement.init(tearDown)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * @param receiver
     * @param transactor
     * @param txnAmt
     * @param test
     * @return
     * @throws Exception
     */
    private SfmResponse initiateCashIn(User receiver, User transactor, String identificationNum, BigDecimal txnAmt, ExtentTest test) throws Exception {

        SfmResponse res = null;
        Markup m = MarkupHelper.createLabel("initiate CashIn: API when channel user and subscriber are having same MSISDN", ExtentColor.PINK);
        test.info(m); // Method Start Marker

        String serviceFlow = Services.CASHIN;

        test.info("Set txn properties : txn.reversal.allowed.services");
        setTxnProperty(of("txn.reversal.ignore.legacyRule", "true"));
        setTxnProperty(of("txn.reversal.allowed.services", serviceFlow));

        ValidatableResponse response = performServiceRequestAndWaitForSuccessForSyncAPI(serviceFlow,
                set("transactor.idValue", transactor.MSISDN),
                set("transactor.mpin", ConfigInput.mPin),
                set("transactor.pin", ConfigInput.mPin),
                set("transactor.tpin", ConfigInput.tPin),
                set("transactor.password", transactor.Password),
                put("transactor", "userRole", "Channel"),

                set("transactionAmount", txnAmt),
                set("receiver.idValue", receiver.MSISDN),
                set("receiver.identificationNo", identificationNum),
                set("receiver.mpin", ConfigInput.mPin),
                set("receiver.pin", ConfigInput.mPin),
                set("receiver.tpin", ConfigInput.tPin),
                set("receiver.password", receiver.Password),
                put("receiver", "userRole", "Customer"),
                delete("depositor")
        );
        res = new SfmResponse(response, test);
        if (ConfigInput.isAssert) {
            res.assertMessage("txn.cashin.successful", DataFactory.getDefaultProvider().Currency, txnAmt.toString(), transactor.MSISDN, receiver.MSISDN, res.TransactionId);
        }
        return res;
    }
}

