package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : Modify_Delete_operator_user_approval
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_User_Management extends TestInit {

    private OperatorUser optUsr, chAdm, bankAdm;
    private SuperAdmin adminM, adminC;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        optUsr = DataFactory.getOperatorUserWithAccess("PTY_ASU");
        chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
        adminM = DataFactory.getSuperAdminWithAccess("PTY_MSU");
        adminC = DataFactory.getSuperAdminWithAccess("PTY_ASUD");
        bankAdm = DataFactory.getOperatorUserWithCategory(Constants.BANK_ADMIN);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 1)
    public void Modify_operator_user_approval() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51269",
                "To verify that Network admin can reject modification request of an Channel admin / CCE");

        try {
            //get the user with access to create Channel Admin
            Login.init(t1).login(optUsr);

            OperatorUser chAdm = new OperatorUser(Constants.CHANNEL_ADMIN);

            OperatorUserManagement.init(t1).createOptUser(chAdm, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.MODIFY_OPERATOR_USER);

            OperatorUserManagement.init(t1).modifyOperatorUser(chAdm);

            //Reject the approval and compare the values
            OperatorUserManagement.init(t1)
                    .approveModifyOperatorUser(chAdm, false);

            OperatorUserManagement.init(t1).viewOperatorDetails(chAdm);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51277",
                "To verify that super admin can reject initiate deletion of an existing Bank Admin");

        try {
            Login.init(t1).
                    loginAsSuperAdmin(adminM);

            OperatorUserManagement.init(t1).
                    initModifyOrDeleteOperator(bankAdm, false);

            Login.init(t1).
                    loginAsSuperAdmin(adminC);

            OperatorUserManagement.init(t1).
                    approveDeletionOperatorUser(bankAdm, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
