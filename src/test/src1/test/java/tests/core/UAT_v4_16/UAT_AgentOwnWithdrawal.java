package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that pseudo user is not able to perform agent own withdraw from USSD.
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_AgentOwnWithdrawal extends TestInit {

    private User whsUsr;
    private PseudoUser pseudoUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initializing Users");
        try {
            whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(setup).makeSureChannelUserHasBalance(whsUsr);

            pseudoUser = new PseudoUser();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void selfReimburseByPseusoUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG48023", "To verify that pseudo user is not able to\n" +
                " perform agent own withdraw from USSD.");

        //Creating Pseudo User
        try {
            pseudoUser.setParentUser(whsUsr);
            PseudoUserManagement.init(t1)
                    .createCompletePseudoUser(pseudoUser);

            Transactions.init(t1)
                    .selfReimbursementforPseudoUser(pseudoUser, Constants.REIMBURSEMENT_AMOUNT);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

