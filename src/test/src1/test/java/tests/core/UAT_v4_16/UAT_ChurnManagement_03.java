package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.pageObjects.userManagement.BarUser_pg1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : ChurnManagement test cases
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */
public class UAT_ChurnManagement_03 extends TestInit {
    private OperatorUser baruser, operatoruser;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {

        try {
            baruser = DataFactory.getOperatorUserWithAccess("PTY_BLKL");
            operatoruser = DataFactory.getOperatorUserWithAccess("CHURNMGMT_MAIN");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1)
    public void churnInitiationandBar() throws Exception {
        ExtentTest r1 = pNode.createNode("TUNG12127", "To verify that system" +
                " should not allow to bar the channel user, if user is already in churn Initiation");

        try {
            //Create Channel User
            User churnedUser = CommonUserManagement.init(r1).getChurnedUser(Constants.WHOLESALER, null);

            Login.init(r1).login(baruser);
            if (!ConfigInput.isCoreRelease) {
                ChannelUserManagement.init(r1)
                        .startNegativeTestWithoutConfirm()
                        .barChannelUser(churnedUser, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_SENDER);

                Assertion.verifyErrorMessageContain("user.error.bar.churn.initiated",
                        "User cannot be barred as User is initiated for churn", r1);
            } else {
                BarUser_pg1 pageOne = BarUser_pg1.init(r1);

                pageOne.navigateNAToBarUserLink();
                pageOne.selectUserTypeByValue(Constants.USER_TYPE_CHANNEL);
                pageOne.setMsisdn(churnedUser.MSISDN);
                pageOne.barType(Constants.BAR_AS_SENDER);
                String msg = AlertHandle.getAlertText(r1);
                if (msg != null) {
                    AlertHandle.acceptAlert(r1);
                    Assertion.verifyMessageContain(msg, "blacklist.error.no.active.users",
                            "Verify that the Churned User cannot be Barred", r1);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, r1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void ChurnInitiationAgain() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12134", "The System should not be able to churn" +
                " the same subscriber again and again.");

        try {
            User subs = CommonUserManagement.init(t1).getChurnedUser(Constants.SUBSCRIBER, null);

            //Churn Subscriber
            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(subs);

            String messageCode = (ConfigInput.isCoreRelease) ? "00066" : "churn.error.already.initiate";
            ChurnUser_Page1.init(t1)
                    .checkLogFileSpecificMessage(subs.MSISDN, messageCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void channeluserBarred() throws Exception {
        ExtentTest r1 = pNode.createNode("TUNG12118", "To verify that the system should be" +
                " able to initiate the churn, if channel user is barred/Blocked");

        String batchId = null;
        User usrBarAsSender = CommonUserManagement.init(r1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
        try {

            // initiating the churn
            batchId = CommonUserManagement.init(r1)
                    .churnInitiateUser(usrBarAsSender);
            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Churn Initiation Successfully for Barred User", r1);

        } catch (Exception e) {
            markTestAsFailure(e, r1);
        } finally {
            if (batchId != null) {
                CommonUserManagement.init(r1).approveRejectChurnInitiate(usrBarAsSender, batchId, false);
            }
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4)
    public void settlementBlockedUser() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG12147", "To verify that churn settlement\n" +
                " should not be successful if the payee is suspended/deleted/ blocked/ barred");
        try {
            OperatorUser operatoruser1 = DataFactory.getOperatorUsersWithAccess("SR_USR").get(0);

            //Create channeluser

            User subs1 = CommonUserManagement.init(t1).getSuspendedUser(Constants.SUBSCRIBER, null);
            User subs2 = CommonUserManagement.init(t1).getBarredUser(Constants.SUBSCRIBER, Constants.BAR_AS_RECIEVER, null);
            User subs3 = CommonUserManagement.init(t1).getChurnedUser(Constants.SUBSCRIBER, null);
            User channeluser = CommonUserManagement.init(t1).getChurnedUser(Constants.WHOLESALER, null);
            // Create suspend subscriber
            Login.init(t1).login(operatoruser1);

            CommonUserManagement.init(t1).startNegativeTest()
                    .initiateChurnSettlement(channeluser, subs1);
            Assertion.verifyErrorMessageContain("churn.receiver.suspended",
                    "Receiver Suspended", t1);


            CommonUserManagement.init(t1).startNegativeTest().initiateChurnSettlement(channeluser, subs2);
            Assertion.verifyErrorMessageContain("churn.error.receivermsisdn.barred",
                    "User Barred as Receiver", t1);


            CommonUserManagement.init(t1).startNegativeTest().initiateChurnSettlement(channeluser, subs3);
            Assertion.verifyErrorMessageContain("churn.error.receivermsisdn.delete",
                    "Subscriber MSISDN is not present or Active in the System for provider MFS1", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5)
    public void churnInitiateDeleteInitUser() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12132",
                "Valid user should fail to do churn initiation for delete initiated users.");

        // Create Channel User
        try {
            User channeluser = CommonUserManagement.init(t1).getDeleteInitiatedUser(Constants.WHOLESALER, null);

            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(channeluser);

            ChurnUser_Page1.init(t1)
                    .checkLogFileSpecificMessage(channeluser.MSISDN, "churn.error.deleteinitiateduser");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6)
    public void churnInitiationForOperator() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12121", "To verify that system is not allowing\n" +
                " to initiate the churn for operator user.");
        try {
            Login.init(t1).login(operatoruser);
            ChurnUser_Page1.init(t1)
                    .navUserChurnInitiate()
                    .downloadChurnSampleFile()
                    .setChurnUserMsisdn(operatoruser.MSISDN)
                    .uploadChurnUserExcel();
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            ChurnUser_Page1.init(t1).checkLogFileSpecificMessage(operatoruser.MSISDN, "user.not.found");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7)
    public void churnInitiationForSubscriberBarred() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12119", "To verify that the system should be" +
                " able to initiate the churn, if subscriber is barred/Blocked.");

        User subs = new User(Constants.SUBSCRIBER);
        String batchId = null;
        try {
            //Create subscriber
            //Create New Subscriber as once the user gets churned, cannot be used further and also since already barred user is associated with bank, not able to churn

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);
            Login.init(t1).login(operatoruser);
            SubscriberManagement.init(t1).barSubscriber(subs, Constants.BAR_AS_RECIEVER);

            //Initiate the churn
            batchId = CommonUserManagement.init(t1)
                    .churnInitiateUser(subs);
            Assertion.verifyActionMessageContain("churn.success.churnUploadFile",
                    "Verify that Barred Subscribed can be Churned", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 8)
    public void setTCPLimit() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG12121", "To verify that valid user should" +
                " not able to perform settlement if Receiver subscriber Min Transaction Amount limit" +
                " reached at TCP Role level for Settlement service.");

        try {
            User channeluser = CommonUserManagement.init(t1).getChurnedUser(Constants.WHOLESALER, null);

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(subs);

            // Edit tcp
            TCPManagement.init(t1).EdittcpforSubs("99999", true, false, t1);

            // Churn Initiate channel user
            CommonUserManagement.init(t1).startNegativeTest()
                    .initiateChurnSettlement(channeluser, subs);

            Assertion.verifyErrorMessageContain("churn.settle.minimumamount",
                    "Payee minimum transaction amount limit reached", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            TCPManagement.init(t1).EdittcpforSubs(Constants.MIN_TRANSACTION_AMT, true, false, t1);
            Assertion.finalizeSoftAsserts();
        }
    }
}
