package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that transaction should be successful
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_BankCashOut_02 extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void bankCashOut() throws Throwable {
        ExtentTest t1 = pNode.createNode("TUNG51505", "To verify that transaction should be succesful" +
                "1) When both the combination is defined like Service Charge % and fixed and Commission % and fixed" +
                "2) All the Tax Applicable on Txn Amt. check boxes are selected and both % and fixed taxes are define");
        try {
            String defaultProvider = DataFactory.getDefaultBankIdForDefaultProvider();
            User channeluser = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);

            MobiquityGUIQueries m = new MobiquityGUIQueries();

            ServiceCharge bankcash = new ServiceCharge(Services.BANK_CASH_OUT, subs, channeluser,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(bankcash);
            TransferRuleManagement.init(t1).configureTransferRule(bankcash);

            Map<String, String> AccountNum = MobiquityGUIQueries.dbGetAccountDetails(channeluser, DataFactory.getDefaultProvider().ProviderId, defaultProvider);

            String channaccount = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String decchanacc = d.decrypt(channaccount);
            System.out.println(decchanacc);

            Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subs, DataFactory.getDefaultProvider().ProviderId, defaultProvider);

            String subaccount = AccountNum1.get("ACCOUNT_NO");
            DesEncryptor d1 = new DesEncryptor();
            String decsubacc = d1.decrypt(subaccount);
            System.out.println(decsubacc);

            String txnid = Transactions.init(t1).bankCashOut(subs, channeluser,
                    decchanacc, decsubacc, Constants.MIN_CASHOUT_AMOUNT);

        /*SMSReader sms=new SMSReader();
        sms.verifyNotificationContain(subs.MSISDN,"Subs.Bank.CashOut.Debit",t1,txnid);
        sms.verifyNotificationContain(channeluser.MSISDN,"Channeluser.Bank.CashOut.Credit",t1,txnid);*/

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}
