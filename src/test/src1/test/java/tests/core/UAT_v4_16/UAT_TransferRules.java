package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : O2C Transfer Rules To verify that valid user can modify the Approval Limit of any existing Transfer Rules
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_TransferRules extends TestInit {

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51325", "O2C Transfer Rules To verify that valid user can modify the Approval Limit of any existing Transfer Rules.");

        try {
            OperatorUser usrTRuleCreator = DataFactory.getOperatorUsersWithAccess("T_RULES").get(0);
            //Login as network admin
            Login.init(t1).login(usrTRuleCreator);

            //verify modification of Approval Limit of any existing Transfer Rules
            TransferRuleManagement.init(t1).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
