package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Last N Transactions service is performed.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_IND03_Split_LastNTransactions extends TestInit {
    private User sub, wholeSaler;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific for this script");
        try {

            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Make Sure that the Channel User has balance more than the threshold amount
            TransactionManagement.init(setup)
                    .makeSureChannelUserHasBalance(wholeSaler);

            //creating transfer rules for cash in
            ServiceCharge CashIn = new ServiceCharge(Services.CASHIN, wholeSaler, sub, null, null, null, null);
            TransferRuleManagement.init(setup).configureTransferRule(CashIn);

            //Modify Service charge and commission for lastNTransactions
            ServiceCharge Last_N_Txn_His1 = new ServiceCharge(Services.LAST_N_TRANSACTION, sub, opt, null, null, null, null);
            ServiceChargeManagement.init(setup)
                    .configureNonFinancialServiceChargewithCommission(Last_N_Txn_His1);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void test_01() throws Exception {
        ExtentTest t = pNode.createNode("TUNG50306", "To verify that Proper Service charge should be credited to IND03 wallet & debited from IND03B wallet when Last N Transactions service is performed.");
        t.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {

            //check initial balance of INDO3
            BigDecimal optPreBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t.info("Initial system balance is " + optPreBalanceIND03);

            //check initial balance of INDO3B
            BigDecimal optPreBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t.info("Initial system balance is " + optPreBalanceIND03B);

            //perform 5 cash in

            for (int i = 1; i <= 5; i++) {
                Transactions.init(t).initiateCashIn(sub, wholeSaler, new BigDecimal(Integer.toString(i)));
                Utils.putThreadSleep(Constants.TWO_SECONDS);
            }

            //check Last_n_Transaction service
            Transactions.init(t).Last_n_Transaction(sub.MSISDN);

            //check post balance of INDO3
            BigDecimal optPostBalanceIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t.info("Current system balance is " + optPostBalanceIND03);

            //check post balance of IND03B
            BigDecimal optPostBalanceIND03B = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t.info("current system balance is " + optPostBalanceIND03B);

            BigDecimal amount = optPreBalanceIND03.subtract(optPostBalanceIND03);
            Assertion.verifyAccountIsCredited(optPreBalanceIND03, optPostBalanceIND03, amount,
                    "Service Charge Id Credited To IND03 Wallet", t);

            amount = optPreBalanceIND03B.subtract(optPostBalanceIND03B);
            Assertion.verifyAccountIsDebited(optPreBalanceIND03B, optPostBalanceIND03B, amount,
                    "Commission Is Debited From IND03B Wallet", t);

        } catch (Exception e) {
            markTestAsFailure(e, t);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
