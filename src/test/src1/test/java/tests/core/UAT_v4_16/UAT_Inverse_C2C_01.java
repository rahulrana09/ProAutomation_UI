package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.tcp.EditTCP;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.MobileRoles;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.JigsawOperations;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

//import static framework.features.apiManagement.AuthenticationRequestContract.enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer;

//import static framework.features.apiManagement.AuthenticationRequestContract.enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Inverse_C2C_01
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Inverse_C2C_01 extends TestInit {
    private User whs, retailer;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        ExtentTest setUp = pNode.createNode("Initiating Test");

        try {
            //enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer();
            String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("EXT_REQ_CAT");


            Assertion.verifyEqual(pref.contains(Constants.RETAILER), true, "Code is present in the preference", setUp);

            retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            GroupRoleManagement.init(setUp)
                    .addOrRemoveSpecificMobileRole(retailer, "Normal", MobileRoles.WALLET_INVERSE_C2C,
                            DataFactory.getDefaultProvider().ProviderName, false);

            ServiceCharge inversec2c = new ServiceCharge(Services.INVERSEC2C, new User(Constants.RETAILER), new User(Constants.WHOLESALER),
                    null, null, null, null);

            TransferRuleManagement.init(setUp)
                    .configureTransferRule(inversec2c);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void UAT_Inverse_C2C_4_4_TUNG9798() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9798_TUNG46008",
                "To verify that Payment ID field should be displayed when category code of channel\n" +
                        " user performing transaction is present in the preference EXT_REQ_CAT");
        try {
            //Initiate Inverse C2C
            Login.init(t1).login(whs);
            String txnid1 = TransactionManagement.init(t1).inverseC2C(retailer, "1");
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);

            ExtentTest t2 = pNode.createNode("TUNG46008",
                    "To verify that the Channel user cannot confirm the initiated\n" +
                            " Inverse channel to channel transaction if mobile roles are not associated with payer,\n" +
                            " or the Role of Inverse C2C is not selected in Mobile group associated with payer.");

            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);
            try {
                JigsawOperations.setUseServiceInBody(false); // todo  add comment
                SfmResponse response = Transactions.init(t2)
                        .inversec2cConfirmation(retailer, reqID, false);
                response.assertStatus("FAILED");
                response.verifyMessage("user.not.allowed.payer");

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void post_condition() throws Exception {

        ExtentTest tearDown = pNode.createNode("Concluding Test");

        try {
            // webGroupRole = new WebGroupRole(Constants.RETAILER, "MOB" + DataFactory.getRandomNumber(3), applicableRole, 1);
            GroupRoleManagement.init(tearDown).addOrRemoveSpecificMobileRole(retailer,
                    "Normal", "Inverse C2C Sender", DataFactory
                            .getDefaultProvider().ProviderName, true);
        } finally {
            updateTCPforInvC2c(tearDown);
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
        For Updating Inst TCP Inv c2c value
    */
    private void updateTCPforInvC2c(ExtentTest test) throws Exception {
        OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        Login.init(test).login(optUsr);

        EditTCP pageOne = EditTCP.init(test);

        pageOne.navAddInstrumentTcp();
        driver.findElement(By.xpath("//table[@id='myTable']/tbody/tr[@class='list']/td[.='Wholesaler - Retailer']/following-sibling::td[.='Normal']/following-sibling::td[.='Gold Retailer']/following-sibling::td[4]")).click();
        pageOne.clickedit();

        WebElement element = driver.findElement(By.xpath("(//tr[td[text()= 'Inverse C2C Transfer']])[1]/following-sibling::tr[4]/td[3]/input"));
        element.clear();
        element.sendKeys("99999");
        Utils.putThreadSleep(4000);
        WebElement element1 = driver.findElement(By.xpath("(//tr[td[text()= 'Inverse C2C Transfer']])[2]/following-sibling::tr[4]/td[3]/input"));
        element1.clear();
        element1.sendKeys("99999");

        pageOne.clickNext();
        pageOne.clickconfirm();
        pageOne.navInstrumentTcpApproval();
        pageOne.clickdetails();
        pageOne.clickonApprove();

    }

}

