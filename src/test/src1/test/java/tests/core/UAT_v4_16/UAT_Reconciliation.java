package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.reconciliation.Reconciliation_Page;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To Observe the reconciliation screen for mismatch in wallet balances
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */
public class UAT_Reconciliation extends TestInit {
    private UserFieldProperties fields;


    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51345 ", "To Observe the reconciliation screen for\n" +
                " mismatch in wallet balances");

        OperatorUser opUser = DataFactory.getOperatorUsersWithAccess("MN_REC").get(0);
        Login.init(t1).login(opUser);
        Reconciliation_Page reconciliationPage = new Reconciliation_Page(t1);
        reconciliationPage.navToReconciliationPage();
        // Reconciliation_Page.init(t1).validateFieldsInReconcilationScreen();

        verifyIfLabelExists(UserFieldProperties.getField("recon.s"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.st"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.to"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.bn"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.th"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.mfs1"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.ser"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.val"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.fly"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.brk"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.int"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.rew"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.chur"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.del"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.tx"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.tax"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.t"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.com"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.ch"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.sub"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.bil"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.fund"), t1);
        verifyIfLabelExists(UserFieldProperties.getField("recon.sum"), t1);
    }

    private void verifyIfLabelExists(String labelName, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By
                        .xpath("//form[@id='reconciliation_loadReconciliation']//td[1][contains(.,'" + labelName + "')]"))
                .isDisplayed()) {

            t1.pass("Successfuuly verified the Label for element - " + labelName);
        } else {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }

    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51252 ", "To verify that the transaction should be succesful");
        try {
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //  O2C transaction
            OperatorUser o2cinit = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            Login.init(t1).login(o2cinit);
            String txnid = TransactionManagement.init(t1).initiateO2C(channeluser, "50", "1234");
            TransactionManagement.init(t1).o2cApproval1(txnid);

            //  Navigate to Reciliation Page to check Reconciled Result
            Reconciliation_Page reconciliationPage = new Reconciliation_Page(t1);
            reconciliationPage.navToReconciliationPage();
            reconciliationPage.validateFieldsInReconcilationScreen();

            verifyIfLabelExist(UserFieldProperties.getField("reconciliation.success.result"), t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    private void verifyIfLabelExist(String labelName, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//form[@id='reconciliation_loadReconciliation']//td[@class='tableft']")).isDisplayed()) {
            t1.pass("Successfuuly verified the Label for element - " + labelName);
        } else {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }
}