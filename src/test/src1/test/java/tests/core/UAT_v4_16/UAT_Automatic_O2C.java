package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.transferRule.O2CTransferRule_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Automatic_O2C
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Automatic_O2C extends TestInit {


    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void UAT_Automatic_O2C_TUNG51478() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51478", "Valid channel user should be able to\n" +
                " do Automated O2C service when channel user is registered through Web");
        try {
            //Fetching a SSA user details
            User ssa = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT);

            setO2CTransferLimitForSSAForCommissionWallet(t1);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(ssa, Constants.COMMISSION_WALLET, new BigDecimal("9"));

            String bankWalletNo = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());

            //get the initial balance of channel user
            UsrBalance prebalance = MobiquityGUIQueries.
                    getUserBalance(ssa, null, null);

            BigDecimal initUsrBalance = prebalance.Balance;
            t1.info("Initial user balance is " + initUsrBalance);

            //get the initial balance of IND04
            BigDecimal preBankBalance = MobiquityGUIQueries
                    .getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankWalletNo);


            //create service charge and transfer rule for Automatic O2C
            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            ServiceCharge autoO2C = new ServiceCharge(Services.AUTO_O2C, opt, ssa, null,
                    null, null, null);

            ServiceChargeManagement.init(t1).
                    configureServiceCharge(autoO2C);

            //perform Automatic O2C
            Transactions.init(t1).automaticO2C(ssa, "5",
                    DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider()), DataFactory.getRandomNumberAsString(3));

            //check user balance got credited
            UsrBalance postbalance = MobiquityGUIQueries.
                    getUserBalance(ssa, null, null);

            BigDecimal currentUsrBalance = postbalance.Balance;

            ChannelUserManagement.init(t1).
                    checkIfUserBalanceisCredited(initUsrBalance, currentUsrBalance, false, "ChannelUser");

            //check IND04 got debited
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankWalletNo);
            t1.info("Pre /Post  IND04 Balnce:  " + preBankBalance + " / " + postBankBalance);

            // bank balance is always negative and hence the comprison is inverse
            Assertion.verifyEqual((preBankBalance.compareTo(postBankBalance) > 0), true,
                    "Balance Got Debited", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    private void setO2CTransferLimitForSSAForCommissionWallet(ExtentTest test) {
        try {
            OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(test).login(optUsr);

            O2CTransferRule_Pg1 rulePg1 = O2CTransferRule_Pg1.init(test);

            rulePg1.navAddTransferRule();
            rulePg1.selectDomainName("Wholesaler");
            rulePg1.selectCategoryName("Special super agent");
            rulePg1.selectPayInstrumentType(DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION).WalletName);
            rulePg1.setFirstApprovalLimit("99999");

            rulePg1.submit();
            rulePg1.confirm();
            Assertion.verifyEqual(Assertion.isErrorInPage(test), false,
                    "Successfully defined O2C Transfer Limit", test, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
