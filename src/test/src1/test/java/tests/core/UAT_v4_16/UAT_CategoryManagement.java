package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Category;
import framework.entity.Domain;
import framework.entity.OperatorUser;
import framework.features.categoryManagement.CategoryManagement;
import framework.features.common.Login;
import framework.features.domainCategoryManagement.DomainManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Notifications should be sent to valid user(Channel Admin) while a new category is approved and to display message category is already exist.
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */
public class UAT_CategoryManagement extends TestInit {

    @Test(priority = 1, enabled = false)
    public void NewCategory() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG5782", "To verify that Notifications should be\n" +
                " sent to valid user(Channel Admin) while a new category is approved");

        //Add Domain
        try {
            CategoryManagement catMgmtObj = CategoryManagement.init(t1);
            OperatorUser networkadmin = DataFactory.getOperatorUserWithAccess("DOM_ADD");
            Domain dom = new Domain();
            Login.init(t1).login(networkadmin);

            DomainManagement.init(t1).addDomain(dom);
            String domname = dom.domainName;
            System.out.println(domname);

            //Adding New Category
            OperatorUser channeladmin = DataFactory.getOperatorUserWithAccess("ADD_CAT");
            Category cat = new Category(Constants.WHOLESALER);
            Login.init(t1).login(channeladmin);

            catMgmtObj.addCategory(cat, dom.domainName);
            String catname = cat.getCategoryName();

            //Approve Category
            Login.init(t1).login(networkadmin);
            catMgmtObj.approveCategory(domname, catname);

            //Notification Verification
            SMSReader.init(t1)
                    .verifyRecentNotification(channeladmin.MSISDN, "category.approved");

            ExtentTest t2 = pNode.createNode("TUNG5759", "To verify that channel admin\n" +
                    " can not initiate the add  category if that Category name already exists in the system.");

            Login.init(t1).login(channeladmin);

            startNegativeTestWithoutConfirm();

            catMgmtObj.addCategory(cat, dom.domainName);

            Assertion.verifyErrorMessageContain("category.catname.catname",
                    "Category name is already existed", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void NewCategory1() throws Exception {

        ExtentTest t1 = pNode.createNode("OG5", "To verify that when category is add initiated" +
                "for a perticular domain, it restricts the addition of another category to the same domain");

        //Add Domain
        try {
            CategoryManagement catMgmtObj = CategoryManagement.init(t1);
            OperatorUser networkadmin = DataFactory.getOperatorUserWithAccess("DOM_ADD");
            Domain dom = new Domain();
            Login.init(t1).login(networkadmin);

            DomainManagement.init(t1).addDomain(dom);
            String domname = dom.domainName;
            t1.info(domname);

            //Add initiate New Category
            OperatorUser channeladmin = DataFactory.getOperatorUserWithAccess("ADD_CAT");
            Category cat01 = new Category(Constants.WHOLESALER);
            Login.init(t1).login(channeladmin);

            catMgmtObj.addCategory(cat01, dom.domainName);
            String catname = cat01.getCategoryName();

            //try to Add different category again to same domain; it should through error,
            //as previous category is in "add initiated" state.
            Category cat02 = new Category(Constants.WHOLESALER);
            Login.init(t1).login(channeladmin);

            startNegativeTestWithoutConfirm();
            catMgmtObj.addCategory(cat02, dom.domainName);

            Assertion.verifyErrorMessageContain("category.AI.exists",
                    "Category name is already existed", t1);

            //Approve Category
            Login.init(t1).login(networkadmin);
            catMgmtObj.approveCategory(domname, catname);

//            Category cat03 = new Category(Constants.WHOLESALER);
//            Login.init(t1).login(channeladmin);
//
//            catMgmtObj.addCategory(cat03, dom.domainName);
//            String catname03 = cat03.getCategoryName();
//
//            //Approve Category
//            Login.init(t1).login(networkadmin);
//            catMgmtObj.approveCategory(domname, catname03);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
