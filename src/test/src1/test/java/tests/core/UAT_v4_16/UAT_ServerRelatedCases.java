package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.CMDExecutor;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that agent own withdrawal can be made one step or two step
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_ServerRelatedCases extends TestInit {


    private String unregPref, displayAllowedReg, modifyAllowedReg, regisPref, domesPref;
    private boolean remiPref;

    @Test(priority = 1)
    public void selfreimbursement() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG3977", "Agent Own Withdrawal >> To verify that agent own withdrawal can be made one step or two step on the basis of parameter AGNTWITHDRAWAL_ONE_STEP value.If the parameter value is set as Y then agent own withdrawal will be a one step service therefore no confirmation need to be done, for two step transaction approval need to be done from network admin.");
        try {
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            unregPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_TAX_INCLUSIVE_IN_SC");
            displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("IS_TAX_INCLUSIVE_IN_SC");
            System.out.println(displayAllowedReg);
            modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("IS_TAX_INCLUSIVE_IN_SC");
            System.out.println(modifyAllowedReg);

            OperatorUser opt = DataFactory.getOperatorUsersWithAccess("PTY_BLKL").get(0);
            //creating the channeluser

            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, false);

            //O2C

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(channeluser);

            //Creating ServiceCharge and TransferRule

            ServiceCharge sCharge = new ServiceCharge(Services.SELF_REIMBURSEMENT, channeluser, opt, null, null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            //Performing self reimbursement

            ExtentTest t2 = pNode.createNode("TUNG13377", "\"Agent Own Withdrawal >> To verify that service charge can be made configurable (inclusive (including) and exclusive (excluding) service charge) on transaction amount on the basis of parameter 'IS_SC_INCLUDED_IN_TXN_AMT'.\n" +
                    "If the parameter value is set as 'Y' service charge would be included in transaction amount.\n" +
                    "If parameter value is set as 'N' service charge would be deducted excluding the service charge amount.\n" +
                    "\n" +
                    "Check for inclusive and exclusive commission and tax on the basis of parameter value 'IS_COMM_INCLUDED_IN_TXN_AMT' and 'IS_TAX_INCLUDED_IN_TXN_AMT'\"\n");

            TxnResponse response = Transactions.init(t2).selfReimbursement(channeluser, 5);


            String txnid = response.TxnId;
            System.out.println(txnid);
            String msg = response.Message;

            String msg1 = msg.split("charges: ")[1].trim();
            String msg2 = msg.split("Service Charge Tax:")[1].trim();

            String charge = msg1.substring(0, 4);
            String Scharge = msg2.substring(0, 4);

            double charges = Double.parseDouble(charge);
            double Scharges = Double.parseDouble(Scharge);

            //Checking if tax is included or not

            if (unregPref.equalsIgnoreCase("N")) {
                if (charges >= Scharges) {
                    t1.info("Tax is not included in Service charge");
                } else {
                    t1.info("Tax is  included in Service charge");
                    Assert.fail();

                }
            }

            //Changing the default preference

            if (unregPref.equalsIgnoreCase("N")) {

                if (displayAllowedReg.equalsIgnoreCase("N") || modifyAllowedReg.equalsIgnoreCase("N")) {
                    MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_TAX_INCLUSIVE_IN_SC", "Y");
                }

                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_TAX_INCLUSIVE_IN_SC", "Y");

            }


            //Performing selfreimbursement after changing preference

            TxnResponse response1 = Transactions.init(t2).selfReimbursement(channeluser, 6);
            String txnid1 = response1.TxnId;

            String msg3 = response1.Message;

            String msg4 = msg3.split("charges: ")[1].trim();
            String msg5 = msg3.split("Service Charge Tax:")[1].trim();

            String charge1 = msg4.substring(0, 4);
            String Scharge1 = msg5.substring(0, 4);

            double charges1 = Double.parseDouble(charge1);
            double Scharges1 = Double.parseDouble(Scharge1);

            //Checking if tax is included or not

            if (charges1 <= Scharges1) {
                t1.info("Tax is  included in Service charge");
            } else {
                t1.info("Tax is not included in Service charge");
                Assert.fail();

            }

            //Updating in to default preference

            SystemPreferenceManagement.init(t1).updateSystemPreference("IS_TAX_INCLUSIVE_IN_SC", "N");

            if (displayAllowedReg.equalsIgnoreCase("Y") || modifyAllowedReg.equalsIgnoreCase("Y")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_TAX_INCLUSIVE_IN_SC", "N");
            }


            //changing the Agntwithdrawal preference to N


            String cmdText = "sed -i 's/AGNTWITHDRAWAL_ONE_STEP=Y/AGNTWITHDRAWAL_ONE_STEP=N/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText);
            CMDExecutor.performServerRestart(t1);
            Thread.sleep(5000);

            UsrBalance balance = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            double initialusrbalance = balance.Balance.doubleValue();
            System.out.println(initialusrbalance);

            double selfreimamount = 7;
            TxnResponse trn1 = Transactions.init(t2).selfReimbursement(channeluser, selfreimamount);
            String trn = trn1.TxnId;

            Login.init(t1).login(opt);
            StockManagement.init(t1).approveReimbursement(trn);

            UsrBalance balance1 = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            double finalusrbalance = balance1.Balance.doubleValue();
            System.out.println(finalusrbalance);
            double currentusrbalance = finalusrbalance - selfreimamount;

            //Checking if service charge is included in required amount

            String value = MobiquityGUIQueries.getSCincludedStatusupdated(Services.SELF_REIMBURSEMENT);
            if (value.equals("N")) {
                if (currentusrbalance < initialusrbalance) {
                    t1.info("Service charge is not included in required amount");
                } else {
                    t1.info("Service charge is included in required amount");
                    Assert.fail();
                }
            } else {
                if (initialusrbalance < currentusrbalance) {
                    t1.info("Service charge is included in required amount");
                } else {
                    t1.info("Service charge is not included in required amount");
                    Assert.fail();
                }
            }


            //Checking if Barred user can do self reimbursement

            OperatorUser baruser = DataFactory.getOperatorUsersWithAccess("PTY_BLKL").get(0);
            TxnResponse trn2 = Transactions.init(t1).selfReimbursement(channeluser, 10);
            String trn3 = trn2.TxnId;

            //Barring the user
            ExtentTest t3 = pNode.createNode("TUNG48020", "Agent Own Withdrawal >> To verify that network admin will not be able to approve the withdrawal initiated by the channel user, if channel user is barred as receiver or both.\n");
            Login.init(t3).login(baruser);
            ChannelUserManagement.init(t3).barChannelUser(channeluser, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_BOTH);
            AlertHandle.acceptAlert(t1);
            //Approving the reimbursement
            startNegativeTest();
            StockManagement.init(t1).approveReimbursement(trn3);
            Assertion.verifyErrorMessageContain("stock.reimburse.approve.barred", "Payer Barred as both", t1);

            //Changing the prefernce to default value

            String cmdText1 = "sed -i 's/AGNTWITHDRAWAL_ONE_STEP=N/AGNTWITHDRAWAL_ONE_STEP=Y/g' /app/mobiquity/txnserver-apache-tomcat-8.0.43/apache-tomcat-8.0.43/webapps/TxnWebapp/WEB-INF/classes/project.properties";
            CMDExecutor.executeCmd(cmdText1);
            CMDExecutor.performServerRestart(t1);
            Thread.sleep(5000);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}

