package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.HashMap;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Loyalty_Promotion
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_Loyalty_Promotion extends TestInit {


    @Test(priority = 1)
    public void UAT_Loyalty_Promotion_TUNG9997() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9997",
                "To verify that after successful loyalty stock transaction equivalent loyalty points\n" +
                        " are getting credit in IND09 wallet as per preference 'CONVERSION_FACTOR'");

        try {
            //fetch the conversion factor prefernce value
            String defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("CONVERSION_FACTOR");

            //fetch the balances of loyalty stock before RA Stock transfer
            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            Login.init(t1).login(opt);
            HashMap<String, String> preStockInfo = StockManagement.init(t1).getLoyaltyReConciliation();

            //Initiate and approve RA Stock Transfer
            String amount = "2";
            StockManagement.init(t1)
                    .initiateApproveRAStock(DataFactory.getRandomNumberAsString(3), amount);

            //calculate the conversion amount
            double totalStock = Double.parseDouble(preStockInfo.get("totalStock")) + Double.parseDouble(amount);

            int convertedAmount = Integer.parseInt(amount) * Integer.parseInt(defaultValue)
                    + Integer.parseInt(preStockInfo.get("convertedLoyaltyStock"));

            //Fetch the balances from Promotion reconciliation screen
            HashMap<String, String> postStockInfo = StockManagement.init(t1).getLoyaltyReConciliation();

            //verify that IND09 Wallet is credited loyalty points as per "CONVERSION_FACTOR" preference and noMismatch in reconciliation screen
            boolean status = false;
            if (Integer.toString(convertedAmount).equals(postStockInfo.get("convertedLoyaltyStock"))
                    && Double.toString(totalStock).equals(postStockInfo.get("totalStock"))
                    && Integer.toString(convertedAmount).equals(postStockInfo.get("IND09Balance"))
                    && postStockInfo.get("ReconciliatioRresult").equals("(No Mismatch found)")) {
                status = true;
            }
            Assertion.verifyEqual(status, true, "Amount that is converted based on conversion factor is equal to the Loyalty stock in reconciliation screen", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
