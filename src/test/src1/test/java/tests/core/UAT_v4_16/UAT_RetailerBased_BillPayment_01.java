package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.BillPayment_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_RetailerBased_BillPayment_01
 * Author Name      : Pushpalatha Pattabiraman, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_RetailerBased_BillPayment_01 extends TestInit {
    private User whs, subs;
    private Biller biller;
    private OperatorUser opt;
    private String providerName, billAccNum;
    private ExtentTest t1;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");

        try {
            opt = DataFactory.getOperatorUserWithAccess("BILLUP");

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            TransactionManagement.init(setup).makeSureLeafUserHasBalance(subs);

            billAccNum = DataFactory.getRandomNumberAsString(5);
            providerName = DataFactory.getDefaultProvider().ProviderName;

            //Do not use the existing biller from app data
            biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            BillerManagement.init(setup).createBiller(biller);

            whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(setup)
                    .createChannelUserDefaultMapping(whs, false);

            TransactionManagement.init(setup)
                    .initiateAndApproveO2C(whs, "250", "Test");

            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment,
                    whs, biller, null, null, null, null);

            TransferRuleManagement.init(setup).configureTransferRule(bill);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG5564() {

        t1 = pNode.createNode("TUNG5564",
                "WEB:To verify that channel member is able to make Standard Bill Payment of\n" +
                        "  amount when Service is modified from premium to standard.");
        try {
            String filename = BillerManagement.init(t1).generateBillForUpload(biller, billAccNum);

            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename);

            //Perform Bill Payment
            Login.init(t1).login(whs);

            billPayment(biller, Constants.BILL_PROCESS_TYPE_OFFLINE,
                    Services.AgentAssistedPresentmentBillPayment, billAccNum, "100", subs);

            //Modify the biller service level from premium to Standard
            biller.setServiceLevel(Constants.BILL_SERVICE_LEVEL_STANDARD);
            biller.setBillAmount(Constants.BILL_EXACT_PAYMENT);
            BillerManagement.init(t1).modifyBiller(biller);

            billAccNum = DataFactory.getRandomNumberAsString(5);
            String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, billAccNum);

            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

            //Perform Bill Payment
            Login.init(t1).login(whs);
            billPayment(biller, Constants.BILL_PROCESS_TYPE_OFFLINE,
                    Services.AgentAssistedPresentmentBillPayment, billAccNum,  "100", subs);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    private void billPayment(Biller biller, String processType, String service, String accNo, String amount, User usr) throws Exception {
        try {
            BillPayment_pg1 page = new BillPayment_pg1(t1);

            page.navigateBillPaymentLink()
                    .selectProcess(processType)
                    .selectServices(service)
                    .selectBillerCode(biller.BillerCode)
                    .selectProvider(DataFactory.getDefaultProvider().ProviderName)
                    .setBillerAccNo(accNo)
                    .setAmount(amount);

            if (service.equalsIgnoreCase(Services.AgentAssistedPresentmentBillPayment)) {
                page.setBillNumber(accNo);
            }
            page.setReferenceMSISDN(usr.MSISDN)
                    .clickSubmit();

            if (ConfigInput.isConfirm) {
                page.clickConfirm();
                if (ConfigInput.isAssert) {
                    String actual = Assertion.getActionMessage();
                    String txnId = actual.split("ID: ")[1].trim();
                    String expected = MessageReader.getDynamicMessage("agent.assisten.presentment.bill.success", MobiquityGUIQueries.getCurrencyCode(DataFactory.getDefaultProvider().ProviderId), amount, whs.LoginId, biller.BillerCode, txnId);
                    Assertion.verifyEqual(actual, expected, "Bill Payment", t1, true);
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }
}

