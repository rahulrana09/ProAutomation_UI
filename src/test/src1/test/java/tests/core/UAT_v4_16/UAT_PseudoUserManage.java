package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : PseudoUser created By ChannelUser
 * Author Name      : Saraswathi Annamalai, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */


public class UAT_PseudoUserManage extends TestInit {
    private User channelUser;
    private PseudoUser pseudoUser;
    private OperatorUser chnlAdmin;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup specific for this script");
        try {
            chnlAdmin = DataFactory.getOperatorUserWithAccess("PSEUDO_ADD2");
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            pseudoUser = new PseudoUser();

            TransactionManagement.init(s1).makeSureChannelUserHasBalance(channelUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG51469() {
        ExtentTest t1 = pNode.createNode("TUNG51469,", "To Verify that a valid user can add Pseudo Wallet user below its hierarchy in category management System. This category has to be below the lowest category of owned wallet user");
        try {

            pseudoUser.setParentUser(channelUser);
            PseudoUserManagement.init(t1).createCompletePseudoUser(pseudoUser);

            //Creating Multiple PseudoUser Under same Parent
            ExtentTest t2 = pNode.createNode("TUNG51470", "To verify that wallet of owner or parent channel user can be assigned to multiple pseudo users");

            PseudoUser pseudoUser1 = new PseudoUser();
            pseudoUser1.setParentUser(channelUser);
            PseudoUserManagement.init(t2).createCompletePseudoUser(pseudoUser1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, dependsOnMethods = "TUNG51469")
    public void TUNG51468() {

        ExtentTest t1 = pNode.createNode("TUNG51468",
                "To verify that on performing the service by pseudo user " +
                        "service charge for the owner wallet user or parent " +
                        "channel user should be applied on the transaction amount " +
                        "and the service being performed");
        try {
            BigDecimal txnAmount = new BigDecimal(10);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            UsrBalance preBalance = MobiquityGUIQueries.getUserBalance(channelUser, null, null);

            //Performing Transaction
            Transactions.init(t1).initiateCashInthroughPseudoUser(subs, pseudoUser, txnAmount);

            UsrBalance postBalance = MobiquityGUIQueries.getUserBalance(channelUser, null, null);
            Assertion.verifyAccountIsDebited(preBalance.Balance, postBalance.Balance, null,
                    "Verify Owner User is debited with service charge", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

