package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.RechargeOperator;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : UAT_SpecialSuperAgent should be able to perform  Recharge
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */
public class UAT_SSARecharge extends TestInit {
    private RechargeOperator RCoperator;

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void rechargeBySSA() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG7087", "To verify that Special Super agent can\n" +
                "recharge others accounts using Retailer based mobile topup (other) service through Webservice Bearer");

        try {
            User SSA = DataFactory.getChannelUserWithCategory(Constants.SPECIAL_SUPER_AGENT);
            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(SSA, new BigDecimal(100));

            //Creating Transfer Rule
            User rechargeReceiver = new User(Constants.ZEBRA_MER);
            ServiceCharge recharge = new ServiceCharge(Services.Recharge_Others, SSA, rechargeReceiver, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(recharge);

            //getting OperatorID
            RCoperator = OperatorUserManagement.init(t1)
                    .getRechargeOperator(GlobalData.defaultProvider.ProviderId);

            //Performing recharge
            SfmResponse response = Transactions.init(t1).rechargeMobilebychanneluser(new OperatorUser(Constants.NETWORK_ADMIN),
                    SSA, rechargeReceiver, 3, RCoperator.OperatorId);

            //Moving transaction from ambiguous to TS
            AmbiguousTransactionManagement.init(t1).downloadAmbiguousTxSheetforRecharge(rechargeReceiver, RCoperator)
                    .setTransactionStatus(response, true)
                    .uploadUpatedAmbiguousTxnFileforRecharge(response, rechargeReceiver);

            Thread.sleep(180000); // wait for the Ambiguous Transaction to Get effective and processed

            AmbiguousTransactionManagement.init(t1)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
