package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.BillerAttribute;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.Assert;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_Retailer_Based_Bill_Payment_TUNG51589_TUNG51447_TUNG6824
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_RetailerBased_BillPayment_02 extends TestInit {

    private Biller biller;
    private User subs, whs;
    private OperatorUser opt;
    private UsrBalance userbalance, billerbalance, userbalance1, billerbalance1;
    private BigDecimal prebalance, prebillerbalance, prebillerFICbalance, postbalance, postbillerbalance, postbillerFICbalance;


    @Test(priority = 1)
    public void UAT_Retailer_Based_Bill_Payment_TUNG51589() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG51589",
                "USSD:To verify that channel member is able to make Standard Bill Payment of  amount when\n" +
                        " Service is modified from premium to standard_USSD");

        ExtentTest t2 = pNode.createNode("TUNG51447",
                "To verify that system will allow to pay the bill  if the customer paid amount is equal \n" +
                        " to bill amount.");

        ExtentTest t3 = pNode.createNode("TUNG6824",
                "To verify that Bill Payment service is successful if all the valid fields are entered \n" +
                        " and transaction status will be TS in DB when EIG API Response is successful");

        try {

            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(
                            defaultProvider.ProviderName, Constants.BILLER_CATEGORY, BillerAttribute.PROCESS_TYPE_OFFLINE,
                            BillerAttribute.SERVICE_LEVEL_PREMIUM, BillerAttribute.BILLER_TYPE_BILL_AVAILABLE,
                            BillerAttribute.BILL_AMOUNT_PART_PAYMENT, BillerAttribute.SUB_TYPE_OVER_PAYMENT);

            /*//create biller
            biller = new Biller(DataFactory.getDefaultProvider().ProviderName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.setPaymentSubType(Constants.BILL_SUBTYPE_OVERPAY);
            BillerManagement.init(t1).
                    createBiller(biller);*/

            //creating a subscriber
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Perform Cashin for the subscriber
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            //Generate file for Bill upload
            String accNo = DataFactory.getRandomNumberAsString(5);
            String filename = BillerManagement.init(t1).generateBillForUpload(biller, accNo);

            //Upload bill
            opt = DataFactory.getOperatorUsersWithAccess("BILLUP").get(0);
            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename);

            //create a offline channel user
            whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);

            //Check channel user has enough balance
            TransactionManagement.init(t1).initiateAndApproveO2C(whs, "100", "Test");
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs, new BigDecimal(10));

            //create transfer rule for Bill Payment
            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment, whs,
                    biller, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(bill);

            //Perform Bill Payment
            Transactions.init(t1).initiateOfflineBillPayment(biller, whs, "10", accNo);

            //Modify the biller service level from premium to Standard
            biller.setServiceLevel(Constants.BILL_SERVICE_LEVEL_STANDARD);
            biller.setBillAmount(Constants.BILL_EXACT_PAYMENT);
            startNegativeTest();
            BillerManagement.init(t1).modifyBiller(biller);
            Assertion.verifyActionMessageContain("biller.modify.initiate", "Biller Modification Initiated", t1, biller.BillerCode);
            stopNegativeTest();

            BillerManagement.init(t1)
                    .modifyBillerApproval(biller, true);

            //fetch channel user balance and FIC balance
            userbalance = MobiquityGUIQueries.getUserBalance(whs, null, null);
            prebalance = userbalance.Balance;

            //fetch biller balance and FIC balance
            billerbalance = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            prebillerbalance = billerbalance.Balance;
            prebillerFICbalance = billerbalance.FIC;

            //Generate file for Bill upload
            String accNo1 = DataFactory.getRandomNumberAsString(5);
            String filename1 = BillerManagement.init(t1).generateBillForUpload(biller, accNo1);

            //Upload bill
            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename1);

            //Perform Bill Payment
            SfmResponse response = Transactions.init(t2).initiateOfflineBillPayment(biller, whs, "10", accNo1);
            String txnid = response.TransactionId;

            //fetch channel user balance and FIC balance after successful bill payment
            userbalance1 = MobiquityGUIQueries.getUserBalance(whs, null, null);
            postbalance = userbalance1.Balance;

            //fetch biller balance and FIC balance after successful bill payment
            billerbalance1 = MobiquityGUIQueries.getBillerBalance(biller.BillerCode);
            postbillerbalance = billerbalance1.Balance;
            postbillerFICbalance = billerbalance1.FIC;

            //check that there is no change in FIC balance for biller
            Assert.assertEquals(prebillerFICbalance, postbillerFICbalance);
            t3.info("There is no change in biller FIC balance");

            //Checck channel user wallet is debited after bill payment
            ChannelUserManagement.init(t3).checkIfUserBalanceisCredited(prebalance, postbalance,
                    true, "ChannelUser");

            //check biller wallet is credited after bill payment
            ChannelUserManagement.init(t3).checkIfUserBalanceisCredited(prebillerbalance,
                    postbillerbalance, false, "Biller");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

