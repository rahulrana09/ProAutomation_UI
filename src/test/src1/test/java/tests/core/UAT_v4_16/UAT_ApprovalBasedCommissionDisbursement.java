package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that while doing balance enquiry for Commission wallet then non-financial service should be successful
 * Author Name      : Saraswathi Annamalai, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_ApprovalBasedCommissionDisbursement extends TestInit {
    private User whsUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        ExtentTest setup = pNode.createNode("Setup", "Setting Up Service Charge");

        try {
            ServiceCharge balEnq = new ServiceCharge(Services.BALANCE_ENQUIRY, new User(Constants.WHOLESALER), new OperatorUser(Constants.NETWORK_ADMIN),
                    Constants.NORMAL_WALLET, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(balEnq);

            whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            TransactionManagement.init(setup).makeSureChannelUserHasBalance(whsUsr);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void balanceEnquiryforCommisionWallet() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51362", "To verify that while doing balance enquiry\n" +
                " for Commission wallet/Normal Wallet then non-financial service should be successful");

        /*
        get the pre normal wallet balance of a channel user.
        perform balance enquiry
        get the post normal wallet balance of a channel user.
        verify that service charge must deduct from normal wallet
         */

        try {
            UsrBalance walletBalance = MobiquityGUIQueries.getUserBalance(whsUsr,
                    DataFactory.getDefaultWallet().WalletId, null);

            BigDecimal initialUserBalance = walletBalance.Balance;
            t1.info("Initial Balance: " + initialUserBalance);

            Transactions.init(t1).BalanceEnquiry(whsUsr);

            walletBalance = MobiquityGUIQueries.getUserBalance(whsUsr,
                    DataFactory.getDefaultWallet().WalletId, null);

            BigDecimal postUserBalance = walletBalance.Balance;
            t1.info("Post Balance: " + postUserBalance);

            BigDecimal amount = initialUserBalance.subtract(postUserBalance);
            Assertion.verifyAccountIsDebited(initialUserBalance, postUserBalance, amount,
                    "Verify Service Charge Deducted", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
