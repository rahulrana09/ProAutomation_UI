package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.Employee;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.setServiceValidatorProperties;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To Verify Employee can perform  Service Successfully
 * Author Name      : Saraswathi Annamalai, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */


public class UAT_ShiftBasedShopManagement_03 extends TestInit {

    private User retailer;
    private OperatorUser netAdm;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Initiating Test");
        try {

            retailer = new User(Constants.RETAILER);
            netAdm = new OperatorUser(Constants.NETWORK_ADMIN);
            setServiceValidatorProperties(of("identical.txn.enabled.CASHIN", false));

            ServiceCharge createemp = new ServiceCharge(Services.CREATE_EMPLOYEE, retailer, netAdm,
                    null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(createemp);

            ServiceCharge changempinemp = new ServiceCharge(Services.CHANGE_MPIN, retailer, netAdm,
                    null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(changempinemp);

            ServiceCharge changetpinemp = new ServiceCharge(Services.CHANGE_TPIN, retailer, netAdm,
                    null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(changetpinemp);

            ServiceCharge unBar = new ServiceCharge(Services.UnBar_Emp, retailer, netAdm,
                    null, null, null, null);

            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(unBar);

            ServiceCharge cashin = new ServiceCharge(Services.CASHIN, retailer, new User(Constants.SUBSCRIBER),
                    null, null, null, null);

            TransferRuleManagement.init(setup).configureTransferRule(cashin);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    //@Test
    public void cashIn() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51558",
                "To Verify Employee can perform CashIn Service Successfully");

        //Create ChannelUser
        User retailer = new User(Constants.RETAILER);
        ChannelUserManagement.init(t1).createChannelUserDefaultMapping(retailer, true);

        //O2C transfer for channeluser
        TransactionManagement.init(t1).makeSureChannelUserHasBalance(retailer);

        //Checking Initial User balance
        UsrBalance balance = MobiquityGUIQueries.getUserBalance(retailer, null, null);
        double initialUserBalance = balance.Balance.doubleValue();

        //Create Employee
        Employee emp = new Employee(retailer.MSISDN, retailer.CategoryCode);
        Transactions.init(t1).createEmployee(emp);

        Transactions.init(t1).changeempUserMPinTPin(retailer, emp);

        //Obtaining Subscriber
        User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);


        //Performing CashinViaEmployee
        Transactions.init(t1).initiateCashInthroughEmployee(subs, retailer, emp, 2);

        //Checking User Final Balance
        UsrBalance balance1 = MobiquityGUIQueries.getUserBalance(retailer, null, null);
        double currentUserBalance = balance1.Balance.doubleValue();
        System.out.println(currentUserBalance);

        if (currentUserBalance < initialUserBalance) {
            t1.info("Service charge is deducted properly");
        } else {
            t1.info("Service charge is not deducted properly");
            Assert.fail();
        }
    }

    @Test(priority = 1)
    public void TUNG51556() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51556", "Unbar Employee:" +
                "Verify that non-financial service charge defined for Unbar Employee service is deducted properly:" +
                "1) When Service Charge fixed is defined and Commission fixed is defined" +
                "2) All the Tax Applicable on service charge & commission check boxes are selected" +
                " and both % and fixed taxes are define");

        try {
            //Create Retailer
            User retailer = new User(Constants.RETAILER);
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(retailer, false);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalance(retailer);

            //Checking User Balance
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(retailer, null, null);
            BigDecimal initialUserBalance = balance.Balance;
            t1.info("Initial User Balance: " + initialUserBalance);

            //Create Employee
            Employee emp = new Employee(retailer.MSISDN, retailer.CategoryCode);
            Transactions.init(t1).createEmployee(emp);

            Transactions.init(t1).changeempUserMPinTPin(retailer, emp);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Performing Invalid Transaction
            Transactions.init(t1).initiateCashInthroughEmployeeWithIncorrectMpin(subs, retailer, emp,
                    Constants.MIN_CASHIN_AMOUNT, "111").verifyMessage("000680");

            Transactions.init(t1)
                    .initiateCashInthroughEmployeeWithIncorrectMpin(subs, retailer, emp,
                            Constants.MIN_CASHIN_AMOUNT, "11a").verifyMessage("000680");

            Transactions.init(t1).initiateCashInthroughEmployeeWithIncorrectMpin(subs, retailer, emp,
                    Constants.MIN_CASHIN_AMOUNT, "12a");

            Transactions.init(t1).initiateCashInthroughEmployeeWithIncorrectMpin(subs, retailer, emp,
                    Constants.MIN_CASHIN_AMOUNT, "11");

            SfmResponse resp = Transactions.init(t1).initiateCashInthroughEmployeeWithIncorrectMpin(subs, retailer, emp,
                    Constants.MIN_CASHIN_AMOUNT, "1");

            if (Assertion.verifyMessageContain(resp.Message.toLowerCase(), "you have been blocked", "Verify Employee is Blocked", t1)) {
                //Unbarring Employee
                Transactions.init(t1)
                        .unBarEmployee(emp);

                //Checking User Final Balance
                balance = MobiquityGUIQueries.getUserBalance(retailer, null, null);
                BigDecimal currentUserBalance = balance.Balance;
                t1.info("Current User Balance: " + currentUserBalance);

                BigDecimal amount = initialUserBalance.subtract(currentUserBalance);
                Assertion.verifyAccountIsDebited(initialUserBalance, currentUserBalance, amount,
                        "Service Charge Deducted", t1);

            } else {
                t1.fail("Employee is not blocked");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
