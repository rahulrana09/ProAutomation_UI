//package tests.core.UAT_v4_16;
//
//import com.aventstack.extentreports.ExtentTest;
//import com.comviva.common.DesEncryptor;
//import framework.entity.OperatorUser;
//import framework.entity.ServiceCharge;
//import framework.entity.User;
//import framework.features.apiManagement.Transactions;
//import framework.features.systemManagement.ServiceChargeManagement;
//import framework.features.userManagement.SubscriberManagement;
//import framework.util.common.Assertion;
//import framework.util.common.DataFactory;
//import framework.util.dbManagement.MobiquityGUIQueries;
//import framework.util.globalConstant.Constants;
//import framework.util.globalConstant.Services;
//import framework.util.globalVars.ConfigInput;
//import framework.util.globalVars.FunctionalTag;
//import org.testng.annotations.Test;
//import tests.core.base.TestInit;
//
//import java.util.Map;
//
///***
// * Company Name     : Comviva Technologies Ltd.
// * Application Name : Mobiquity 4.6.0
// * Objective        : To verify that active subscriber can add/delete bank account no. as a beneficiary for different MFS provider.
// * Author Name      : Saraswathi Annamalai
// * Created Date     : 02/02/2018
// */
//
//
//public class UAT_AddDeleteBeneficiary extends TestInit {
//    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
//    public void adddeleteBeneficiarybysubs() throws Throwable {
//        ExtentTest t1 = pNode.createNode("TUNG51475", "To verify that active subscriber can add bank account no. as a beneficiary for different MFS provider.");
//        if (ConfigInput.isSecondryProvider) {
//            try {
//                User subs1 = new User(Constants.SUBSCRIBER);
//                User subs2 = new User(Constants.SUBSCRIBER);
//                User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
//                String defaultProvider = DataFactory.getBankId(DataFactory.getDefaultBankNameForDefaultProvider());
//                String nondefaultProvider = DataFactory.getBankId(DataFactory.getNonDefaultProvider().getDefaultBankName());
//
//                //Create Subscriber 1
//                SubscriberManagement.init(t1).createSubscriber(subs1);
//                Transactions.init(t1).subscriberAcquisitionfornondefaultprovide(subs1);
//
//                //Create Subscriber 2
//                SubscriberManagement.init(t1).createSubscriber(subs2);
//                Transactions.init(t1).subscriberAcquisitionfornondefaultprovide(subs2);
//
//                //Fetching Account Number for MFS1
//                Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subs2, DataFactory.getDefaultProvider().ProviderId, defaultProvider);
//                String subaccount = AccountNum1.get("ACCOUNT_NO");
//                DesEncryptor d1 = new DesEncryptor();
//                String decsubacc = d1.decrypt(subaccount);
//                System.out.println(decsubacc);
//                OperatorUser o2cinit = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
//
//                ServiceCharge addbene = new ServiceCharge(Services.ADD_BENE_BANK, subs1, o2cinit, null, null, null, null);
//                ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(addbene);
//
//                ServiceCharge deleteben = new ServiceCharge(Services.DELETE_BENE_BANK, subs1, o2cinit, null, null, null, null);
//                ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(deleteben);
//
//
//                //Performing Addbeneficiary
//
//                Transactions.init(t1).addBeneficiaryfordefaultprovider(subs1, subs2, decsubacc);
//
//                //Fetching Account Number for MFS2
//                Map<String, String> AccountNum2 = MobiquityGUIQueries.dbGetAccountDetails(subs2, DataFactory.getNonDefaultProvider().ProviderId, nondefaultProvider);
//                String subaccount1 = AccountNum2.get("ACCOUNT_NO");
//                DesEncryptor d2 = new DesEncryptor();
//                String decsubacc1 = d2.decrypt(subaccount1);
//                System.out.println(decsubacc1);
//
//                //Performing Addbeneficiary
//
//                Transactions.init(t1).addBeneficiaryfornondefaultprovider(subs1, subs2, decsubacc1);
//
//                ExtentTest t2 = pNode.createNode("TUNG51474", "To verify that active subscriber registered with multiple MFS provider & associated with Bank account can delete Beneficiary for Bank Account for selected MFS provider if user is Barred in  MFS provider.\n");
//                Transactions.init(t2).initiateCashIn(subs1, channeluser, "5");
//                //Barring Subscriber
//
//                SubscriberManagement.init(t2).barSubscriber(subs1, Constants.BAR_AS_BOTH);
//
//                //DeleteBenificiary
//                Transactions.init(t2).deleteBeneficiary(subs1);
//
//
//            } catch (Exception e) {
//                markTestAsFailure(e, t1);
//            } finally {
//                Assertion.finalizeSoftAsserts();
//            }
//        } else {
//            t1.skip("Required 2nd MFS Provider");
//        }
//    }
//}
//
//
//
//
