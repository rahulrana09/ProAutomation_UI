package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : UAT_SpecialSuperAgent should be able to perform cashinothers
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */


public class UAT_SpecialSuperAgent extends TestInit {
    private String regisPref, remiPref, domesPref, displayAllowedReg, modifyAllowedReg, displayAllowedDom, modifyAllowedDom;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        //REGISTERINGSUB_THRESHOLD

        ExtentTest setup = pNode.createNode("setup", "Initiating Test");
        try {
            displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("REGISTERINGSUB_THRESHOLD");
            modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("REGISTERINGSUB_THRESHOLD");

            if (displayAllowedReg.equalsIgnoreCase("N") || modifyAllowedReg.equalsIgnoreCase("N")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("REGISTERINGSUB_THRESHOLD", "Y");
            }

            MobiquityGUIQueries m = new MobiquityGUIQueries();
            regisPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("REGISTERINGSUB_THRESHOLD");
            SystemPreferenceManagement.init(setup).updateSystemPreference("REGISTERINGSUB_THRESHOLD", "RULE1");

            //IS_REMITTANCE_WALLET_REQUIRED
            remiPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_REMITTANCE_WALLET_REQUIRED");

            if (remiPref.equalsIgnoreCase("FALSE")) {
                displayAllowedDom = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("DOMESTIC_REMIT_WALLET_PAYID");
                modifyAllowedDom = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID");

                if (displayAllowedDom.equalsIgnoreCase("N") || modifyAllowedDom.equalsIgnoreCase("N")) {
                    MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID", "Y");
                }

                domesPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("DOMESTIC_REMIT_WALLET_PAYID");
                String walletId = DataFactory.getDefaultWallet().WalletId;
                SystemPreferenceManagement.init(setup).updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", walletId);

            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.CASHINOTHERS,
            FunctionalTag.ECONET_UAT_5_0,
            FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.MONEY_SMOKE})
    public void cashinotherThroughSSA() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG7096", "To verify that Special Super agent can perform Cash In Other (Pay remittance) service through WEBSERVICE bearer.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHINOTHERS, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            User SSA = new User(Constants.SPECIAL_SUPER_AGENT);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(SSA, false);

            //O2C transfer for channeluser
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(SSA);

            //Creating Transfer Rule
            User depositor = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            User receiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            ServiceCharge cashinoth = new ServiceCharge(Services.Cash_In_Others, SSA, receiver, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(cashinoth);

            //Performing Cashinothers
            Transactions.init(t1).initiateCashInOthers(receiver, SSA, depositor, "10");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {
        ExtentTest teatDown = pNode.createNode("teardown", "Concluding Test");
        try {

            SystemPreferenceManagement.init(pNode).updateSystemPreference("REGISTERINGSUB_THRESHOLD", regisPref);
            if (remiPref.equalsIgnoreCase("FALSE")) {
                SystemPreferenceManagement.init(teatDown).updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", domesPref);
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }
}
