package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.pageObjects.cashInCashOut.CashIn_page1;
import framework.pageObjects.groupRole.GroupRole_Page2;
import framework.pageObjects.groupRole.GroupRole_Page4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that when a service is removed from group roles then it shall no longer be possible to perform the service
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_GroupRoleManagement_01 extends TestInit {
    private SuperAdmin sAdmin;
    private User channelUser;
    private User subUser;
    private List<String> role;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        try {
            sAdmin = DataFactory.getSuperAdminWithAccess("GRP_ROL");
            channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            subUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    // [rahul rana] - taken care in SuiteSystemGroupRoleManagement
    @Test(priority = 1, enabled = false)
    public void Test_01() throws Exception {
        ExtentTest t = pNode.createNode("TUNG51909", "To verify that when a service is removed from\n" +
                " group roles then it shall no longer be possible to perform the service");
        try {
            role = new ArrayList<>(Arrays.asList("CIN_WEB"));

            //removing cash in service from web role
            updateSpecificWebRole(Constants.WHOLESALER, channelUser.WebGroupRole, role, t);

            Login.init(t).loginAsSuperAdmin(sAdmin);
            Login.init(t).login(channelUser);

            CashIn_page1 cashIn_page1 = CashIn_page1.init(t);
            cashIn_page1.navigateToLink();

            boolean cashInLink = Utils.checkElementPresent("MTRANS_CIN_WEB", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(cashInLink, false, "Cash in Service Is Not Available", t, true);

        } catch (Exception e) {
            markTestAsFailure(e, t);
        } finally {
            Login.init(t).loginAsSuperAdmin(sAdmin);
            updateSpecificWebRole(Constants.WHOLESALER, channelUser.WebGroupRole, role, t); //enabling cashin service for web role
        }
        Assertion.finalizeSoftAsserts();
    }

    private void updateSpecificWebRole(String catCode, String webRoleName, List<String> roles, ExtentTest test) throws Exception {
        Markup m = MarkupHelper.createLabel("updateSpecificWebRole", ExtentColor.ORANGE);
        test.info(m); // Method Start Marker
        try {
            GroupRoleManagement.init(test)
                    .openWebRolePage(catCode);

            GroupRole_Page2.init(test)
                    .initiateUpdateGroupRole(webRoleName);

            for (String role : roles) {
                driver.findElement(By.xpath("//input[@value='" + role + "']")).click();
            }
            GroupRole_Page4.init(test).updateGroupRole();

            Assertion.verifyActionMessageContain("grouprole.updated", "Update Group Role", test);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, test);
        }
    }
}
