package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.ModifyChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel admin cannot initiate Channel user addition if all Mandatory fields are left blank..
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */
public class UAT_ChannelUserCreation_02 extends TestInit {

    private User chUser;
    private OperatorUser addChUser, modifyChUser;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest s1 = pNode.createNode("Setup", "Setup specific for this script");
        try {
            addChUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            modifyChUser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51302",
                "To verify that channel admin cannot initiate or modify  Channel user addition\n" +
                        " if all Mandatory fields are left blank.");

        try {
            Login.init(t1).login(addChUser);
            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(t1);

            //navigate to Add channel user and Leave all mandatory fields blank
            pageOne.navAddChannelUser();
            pageOne.clickNext();

            //verify Error message
            List<String> actualErrorMsg = Assertion.getAllErrorMessages();

            //verify Error message
            LinkedList<String> errorCodes = new LinkedList<>();
            errorCodes.addAll(Arrays.asList("channeluser.validation.userName", "user.error.lastnamerequired",
                    "subs.error.emailId", "systemparty.label.garson.relationship.ui", "subs.error.genderrequired",
                    "subs.externalCode.required", "pls.enter.validweb.loginid", "subsreg.error.doc1",
                    "subsreg.error.doc2", "subsreg.error.doc3", "subsreg.error.proof1", "subsreg.error.proof2",
                    "subsreg.error.proof3", "subsreg.error.id.type", "subsreg.error.id.no", "channeluser.validation.webLoginId",
                    "channeluser.error.password.required", "channeluser.validation.passwdlength.ui", "user.error.preflangrequired"));

            for (String code : errorCodes) {
                String expectedMsg = MessageReader.getMessage(code, null);
                Assertion.verifyListContains(actualErrorMsg, expectedMsg, "Verify Error Message", t1);
            }
            Utils.captureScreen(t1);

            //Login as Channel Admin
            Login.init(t1).login(modifyChUser);

            //Modifying channel user by clearing mandatory fields
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(chUser);
            ModifyChannelUser_pg1.init(t1).clearAllMandatoryFields();

            actualErrorMsg = Assertion.getAllErrorMessages();

            errorCodes = new LinkedList<>();
            errorCodes.addAll(Arrays.asList("systemparty.validation.prefix", "channeluser.validation.userName",
                    "user.error.lastnamerequired", "subs.error.emailId", "user.error.genderprefixrequired",
                    "channeluser.validation.externalCode.empty", "subsreg.error.id.type", "subsreg.error.id.no",
                    "channeluser.validation.webLoginId", "systemparty.validation.loginlength.ui", "channeluser.validation.password",
                    "systemparty.validation.confirmpassword"));

            for (String code : errorCodes) {
                String expectedErrorMessage = MessageReader.getMessage(code, null);
                Assertion.verifyListContains(actualErrorMsg, expectedErrorMessage, "Validating Error Messages", t1);
            }
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
