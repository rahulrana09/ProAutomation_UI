package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.InstrumentTCP;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg2;
import framework.pageObjects.userManagement.AddChannelUser_pg4;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel admin can reject the Channel user creation.
 * To verify that After rejection of user, any other user can be created with the same Login ID.
 * Author Name      : Nirupama mk, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_ChannelUserCreation_01 extends TestInit {

    private OperatorUser usrCreator;
    private User whsUsr;


    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        whsUsr = new User(Constants.WHOLESALER);
    }

    @Test(priority = 4)
    public void Test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51303", " WEB : To verify that channel user can register with \n " +
                "multiple wallets for a MFS Provider.");
        try {

            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(whsUsr)
                    .assignHierarchy(whsUsr);

            CommonUserManagement.init(t1)
                    .assignWebGroupRole(whsUsr)

                    //  User can have Multiple Wallet Wit One Primary Account
                    .mapWalletPreferences(whsUsr)
                    .mapBankPreferences(whsUsr)
                    .addInitiatedApproval(whsUsr, true);

            Assertion.verifyActionMessageContain("channeluser.message.approval", "Add Channel User \n" +
                    " is approved. Channel user is active now", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51304 ", " WEB : To verify that only one wallet\n" +
                " can be set as Primary wallet for each MFS Provider");
        try {
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).startNegativeTest()
                    .initiateChannelUser(whsUsr)
                    .assignHierarchy(whsUsr);

            CommonUserManagement.init(t1).assignWebGroupRole(whsUsr);
            Utils.putThreadSleep(Constants.TWO_SECONDS);
            mapWalletPreferences(whsUsr, t1);

            //  User Cannot Have Multiple Wallet with Multiple primary Account
            Assertion.verifyErrorMessageContain("cannot.have.multiple.primaryaccount.per.mfs.provider",
                    "Cannot have multiple primary accounts per mfs provider", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51305 ", " WEB : To verify that One Bank per\n" +
                " MFS Provider account has to be set as the Primary Account.");
        try {
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).startNegativeTest()
                    .initiateChannelUser(whsUsr)
                    .assignHierarchy(whsUsr);

            CommonUserManagement.init(t1).assignWebGroupRole(whsUsr)
                    .mapDefaultWalletPreferences(whsUsr)
                    .mapMultipletBankPreferencesWitMulPrimaryAcc(whsUsr, true);

            Assertion.verifyErrorMessageContain("cannot.have.multiple.primaryaccount.per.mfs.provider",
                    "Cannot have multiple primary accounts per mfs provider", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51301",
                "To verify that channel admin can reject the Channel user creation. " +
                        "To verify that After rejection of user, any other user can be created with the same Login ID.");
        try {
            // Initiate Channel User Creation and then reject at Approval Stage
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1)
                    .addChannelUser(whsUsr);

            CommonUserManagement.init(t1)
                    .addInitiatedApproval(whsUsr, false);

            //  Create ChannelUser2 with Rejected ChannelUser1's LoginID
            User chUser2 = new User(Constants.WHOLESALER);
            chUser2.setLoginId(whsUsr.LoginId); // set the Login ID as same as the rejected user

            ChannelUserManagement.init(t1)
                    .startNegativeTest()
                    .initiateChannelUser(chUser2);

            boolean status = AddChannelUser_pg2.init(t1).isHierarchyPageOpen();
            Assertion.verifyEqual(status, true, "Initiated a new channel user with the same login ID which was rejected before", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    private void mapWalletPreferences(User user, ExtentTest test) throws Exception {
        try {
            Markup m = MarkupHelper.createLabel("mapWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
            test.info(m); // Method Start Marker
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(test);
            /*
             * Provide and Wallet List
             */
            List<String> providerList = DataFactory.getAllProviderNames();
            List<String> paymentTypeList;
            if (user.CategoryCode.equals(Constants.SUBSCRIBER)) {
                paymentTypeList = DataFactory.getWalletForSubscriberUserCreation();
            } else {
                paymentTypeList = DataFactory.getWalletForChannelUserCreation();
            }
            /*
             * Map all the provides and wallets
             */
            int counter = 0;
            for (String provider : providerList) {
                boolean isPrimary = true;
                boolean isSkipped = false; // select just one primary account per currency provider
                for (String paymentType : paymentTypeList) {

                    if (counter != 0 && !isSkipped) {
                        try {
                            page4.clickAddMore(user.CategoryCode);
                        } catch (Exception e) {
                            if (e.getMessage().contains("element is not attached")) {
                                Thread.sleep(Constants.TWO_SECONDS);
                                page4.clickAddMore(user.CategoryCode);
                            }
                        }
                    }
                    // get instrument TCP
                    InstrumentTCP insTcp = DataFactory.getInstrumentTCP(user.DomainName, user.CategoryName, user.GradeName, provider, paymentType);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].providerSelected"))).selectByVisibleText(provider);
                    pNode.info("Selected Provider:" + provider);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].paymentTypeSelected"))).selectByVisibleText(paymentType);
                    pNode.info("Selected Payment Type:" + paymentType);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    new Select(driver.findElement(By.name("counterList[" + counter + "].channelGradeSelected"))).selectByVisibleText(user.GradeName);
                    pNode.info("Select Grade:" + user.GradeName);
                    Thread.sleep(Constants.MAX_WAIT_TIME);

                    if (insTcp != null) {
                        new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected"))).selectByVisibleText(insTcp.ProfileName);
                        pNode.info("Select Instrument TCP - " + insTcp.ProfileName);
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    } else {
                        Select sel = new Select(driver.findElement(By.name("counterList[" + counter + "].tcpSelected")));
                        if (sel.getOptions().size() > 0)
                            sel.selectByIndex(0);
                        else
                            sel.selectByIndex(1);
                        pNode.info("Select Instrument TCP By Index");
                        Thread.sleep(Constants.MAX_WAIT_TIME);
                    }

                    /*
                     * If the wallet type is not applicable then skip the same!
                     */
                    Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[" + counter + "].primaryAccountSelected")));

                    if (isPrimary && DataFactory.getDefaultWallet().WalletName.equals(paymentType)) {
                        selPrimaryAccount.selectByIndex(0);
                        isPrimary = false;
                    } else {
                        selPrimaryAccount.selectByIndex(1);
                    }
                    counter++;
                    isSkipped = false;
                }
            }
            new Select(driver.findElement(By.id("primaryAc1"))).selectByVisibleText("Yes");

            page4.clickNext(user.CategoryCode);
            if (ConfigInput.isAssert) {
                Assertion.assertErrorInPage(test);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, test);
        }
    }
}