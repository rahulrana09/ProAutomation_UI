package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.SystemBankAccount;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Wallet to bank service is in ambiguous state due to third party time out and transaction status is TA in response xml & transaction header.
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_Ambiguous_Transaction_02 extends TestInit {
    private User subscriber;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Pre-Condition,Setting preference");

        try {

            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge WtoB = new ServiceCharge(Services.SVA_TO_BANK, subscriber, subscriber,
                    null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(WtoB);

            TransactionManagement.init(setup)
                    .makeSureLeafUserHasBalance(subscriber);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TUNG13547", " To verify that Wallet to bank service\n" +
                " is in ambiguous state due to third party time out and transaction status is TA in response xml\n" +
                " & transaction header.");
        try {
            String bankId = DataFactory.getDefaultBankIdForDefaultProvider();
            UsrBalance subsPreBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal preBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            UsrBalance preBankBal = new UsrBalance(preBankBalance, new BigDecimal(0));

            BigDecimal txnAmt = new BigDecimal(1);

            //initiate wallet to bank service
            SfmResponse response = Transactions.init(t1)
                    .walletToBankService(subscriber, txnAmt.toString(), bankId)
                    .verifyMessage("sfm.ambiguous.initiated");

            ValidatableResponse validatableResponse = response.validatableResponse;
            validatableResponse.assertThat()
                    .body("txnStatus", Matchers.equalTo("TA"));


            //Check Balance
            UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal postBankBalance = MobiquityGUIQueries.getBalanceSystemWallet(GlobalData.defaultProvider.ProviderId, bankId);
            UsrBalance postBankBal = new UsrBalance(postBankBalance, new BigDecimal(0));


            SystemBankAccount subsAcc = new SystemBankAccount(DataFactory.getDefaultBankNameForDefaultProvider(),
                    DataFactory.getDefaultWallet().AutWalletCode);

            AmbiguousTransactionManagement.init(t1)
                    .downloadAmbiguousTxSheet(subsAcc)
                    .setTransactionStatus(response, true)
                    .uploadUpatedAmbiguousTxnFile(response, subsAcc);

            Thread.sleep(140000); // wait for the Ambiguous Transaction to Get effective and processed*/

            //Check Balance & Db Status
            AmbiguousTransactionManagement.init(t1)
                    .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
