package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that channel user should not be able to perform the service if user has not changed his PIN after registration
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */

public class UAT_DayTransactionSummary extends TestInit {

    private OperatorUser optUsr;
    private User whsUsr;

    @BeforeClass(alwaysRun = true)
    public void preCondition() {
        try {
            ExtentTest setup = pNode.createNode("setup", "Initiating Test");
            whsUsr = new User(Constants.WHOLESALER);
            optUsr = new OperatorUser(Constants.NETWORK_ADMIN);

            ServiceCharge sCharge = new ServiceCharge(Services.CH_DAY_TXN_SUM, whsUsr, optUsr, null, null, null, null);
            ServiceChargeManagement.init(setup).configureNonFinancialServiceCharge(sCharge);


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void channelUserTransaction() {
        ExtentTest t1 = pNode.createNode("TUNG4061", "To verify that channel user should not be able to " +
                "perform the service if user has not changed his PIN after registration");
        try {
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMappingWithoutChangingTpin(whsUsr);

            //Channel User performing Transactions
            Transactions.init(t1).dayTxnSummary(whsUsr, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}


