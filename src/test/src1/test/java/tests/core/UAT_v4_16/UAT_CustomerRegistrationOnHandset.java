package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.Test;
import tests.core.base.TestInit;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Channel user can register customer in Mobiquity system using USSD
 * Author Name      : Nirupama mk
 * Created Date     : 02/02/2018
 */

public class UAT_CustomerRegistrationOnHandset extends TestInit {

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG51517", "To verify that Channel user can register " +
                "customer in Mobiquity system using USSD ");

        try {
            SuperAdmin opUser = DataFactory.getSuperAdminWithAccess("CAT_PREF");
            User subs = new User(Constants.SUBSCRIBER);

            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, new OperatorUser(Constants.OPERATOR), null, null, null, null);

            // Configure the Service Charge
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(subs);

            /*SMSReader.init(t1)
                    .verifyNotificationContain(subs.MSISDN, "subs.reg.handset");*/ // TODO re verify if this message is correct for v 5.01

            SMSReader.init(t1)
                    .verifyNotificationContain(subs.MSISDN, "pin.change");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}