package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : Bulk Payment to unregistered customer
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */
public class UAT_BulkPayoutByEnterprise_UnregisteredCustomer extends TestInit {

    private String unregPref, displayAllowedReg, modifyAllowedReg, regisPref, domesPref;
    private boolean remiPref;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

        ExtentTest s = pNode.createNode("Setup", "Setup Specific to this Script");

        try {
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            unregPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("UNREGSUB_PAYER_UNIQUE");

            if (!unregPref.equalsIgnoreCase("RULE1")) {
                displayAllowedReg = MobiquityGUIQueries.fetchDisplayAllowedFromGUI("UNREGSUB_PAYER_UNIQUE");
                modifyAllowedReg = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("UNREGSUB_PAYER_UNIQUE");
                SystemPreferenceManagement.init(s)
                        .updateSystemPreference("UNREGSUB_PAYER_UNIQUE", "RULE1");
            }
            //IS_REMITTANCE_WALLET_REQUIRED and DOMESTIC_REMIT_WALLET_PAYID

            regisPref = AppConfig.regSubsThreshold;
            remiPref = AppConfig.isRemittanceRequired;
            domesPref = AppConfig.domesticRemmittancePayID;

            // Update the system preference
            SystemPreferenceManagement.init(s)
                    .updateSystemPreference("REGISTERINGSUB_THRESHOLD", "RULE2");

            if (!remiPref) {
                // set the preference to the default wallet ID -  this is reverted back in after class
                String walletId = DataFactory.getDefaultWallet().WalletId;

                SystemPreferenceManagement.init(s)
                        .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", walletId);
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void bulkpayouttoUnregisteredCustomer() throws Exception {
        ExtentTest t1 = pNode.createNode("BulkPayoutbyEnterprise_TUNG51462", "Bulk Payment to\n" +
                " unregistered customer: To verify that Enterprise user can transfer point to a non registered\n" +
                " customer (IND02 account) and passcode gets generated");

        try {

            User entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            User sub = new User(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);
            Thread.sleep(5000);

            String Passcode = MobiquityGUIQueries.dbGetPasscode(batchId);
            t1.info("Passcode: " + Passcode);
            if (batchId != null) {
                t1.pass("Successfully verified That Pass code is generated even the beneficiary is not registered in the system");
            } else {
                t1.fail("Failed to verify That Pass code is generated when beneficiary is not registered in the system");
                Assertion.markAsFailure("Fail");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2)
    public void run() throws Exception {
        ExtentTest t1 = pNode.createNode("Adding Web Group Role");
        try {
            Login.init(t1).loginAsSuperAdmin("GRP_ROL");
            for (WebGroupRole role : GlobalData.rnrDetails) {
                if (role.CategoryCode.equals(Constants.BULK_PAYER_ADMIN)) {
                    GroupRoleManagement.init(t1)
                            .addWebGroupRole(role);
                    role.writeDataToExcel();
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @AfterClass(alwaysRun = true)
    public void teardown() throws Exception {

        ExtentTest t = pNode.createNode("Teardown", "Teardown");
        try {
            SystemPreferenceManagement.init(t)
                    .updateSystemPreference("UNREGSUB_PAYER_UNIQUE", unregPref);

            SystemPreferenceManagement.init(t)
                    .updateSystemPreference("REGISTERINGSUB_THRESHOLD", regisPref);

            if (!remiPref) {
                SystemPreferenceManagement.init(t)
                        .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", domesPref);
            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }
}


