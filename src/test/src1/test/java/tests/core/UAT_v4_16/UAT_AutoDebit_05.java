package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.16.0
 * Objective        : UAT_AutoDebit_05
 * Author Name      : Pushpalatha Pattabiraman
 * Created Date     : 02/02/2018
 */

public class UAT_AutoDebit_05 extends TestInit {
    private Biller biller;
    private User subs, usr;
    private String defaultValue;
    private OperatorUser opt;
    private UsrBalance initialBal, currentBalance;
    private String txnId;

    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest eSuite = pNode.createNode("Setup", "Update System Preferences IS_CUSTOMER_CONFIRM_REQUIRED. " +
                "Get Default Biller from App Data.");
        try {

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_CUSTOMER_CONFIRM_REQUIRED");
            if (defaultValue.equalsIgnoreCase("FALSE")) {
                SystemPreferenceManagement.init(eSuite).
                        updateSystemPreference("IS_CUSTOMER_CONFIRM_REQUIRED", "TRUE");
            } else {
                eSuite.info("Preference is already set to TRUE");
            }

            //Creating a biller
            biller = BillerManagement.init(eSuite)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTO_DEBIT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void UAT_Auto_Debit_TUNG9458() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9458", "WEB: To verify that Channel user,CCE,Biller\n" +
                " and subscriber can initiate enable Auto debit.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTO_DEBIT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            usr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            //creating a subscriber
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            //Perform Cashin for the subscriber
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(10));

            //Define service charge for utility registration
            opt = DataFactory.
                    getOperatorUsersWithAccess("UTL_BILREG").get(0);

            Login.init(t1).login(opt);
            ServiceCharge utilityReg = new ServiceCharge(Services.UTILITY_REGISTRATION, subs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureNonFinancialServiceCharge(utilityReg);

            //Add bill for customer
            String billAccNumber = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subs.MSISDN, billAccNumber);
            BillerManagement.init(t1).initiateSubscriberBillerAssociation(biller);

            //Initial Balance of subscriber
            initialBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
            BigDecimal preBalance = initialBal.Balance;
            t1.info("Initial balance is: " + initialBal.Balance.doubleValue());

            //Define Service charge for enable Auto Debit
            ServiceCharge autoDebitEnable = new ServiceCharge(Services.ENABLE_AUTODEBIT, subs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureNonFinancialServiceCharge(autoDebitEnable);

            //Enable Auto Debit by channel user

            Login.init(t1).login(usr);
            txnId = ChannelUserManagement.init(t1)
                    .enableAutoDebitByChanneluser(biller, subs.MSISDN, billAccNumber, "5");

            //Confirmation for auto debit enable by subscriber
            Transactions.init(t1).AutoDebitConfirmation_Customer(subs.MSISDN, txnId);

            //Approve by biller
            Login.init(t1).login(biller);
            BillerManagement.init(t1)
                    .approveorRejectAutoDebitEnableByBiller(subs, true);

            //Cuurent Balance of subscriber
            currentBalance = MobiquityGUIQueries.
                    getUserBalance(subs, null, null);
            BigDecimal postBalance = currentBalance.Balance;

            t1.info("Current balance is: " + currentBalance.Balance.doubleValue());

            //Check service charge is deducted
            BigDecimal amount = preBalance.subtract(postBalance);
            Assertion.verifyAccountIsDebited(preBalance, postBalance, amount, "Service Charge Is Deducted From Subscriber Account", t1);

            //Check for Approval notification
            SMSReader.init(t1).verifyNotificationContain(subs.MSISDN, "autodebit.subsSelf.notification");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2)
    public void UAT_Auto_Debit_TUNG9458_by_USSD() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG9458",
                "USSD:To verify that Channel user,CCE,Biller and subscriber can initiate enable Auto debit");

        try {
            //creating a subscriber
            User subs = new User(Constants.SUBSCRIBER);
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs);

            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(subs);
            Transactions.init(t1).initiateCashIn(subs, whs, new BigDecimal("50"));

            //Perform Cashin for the subscriber
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(10));

            //Define service charge for utility registration
            opt = DataFactory.getOperatorUsersWithAccess("UTL_BILREG").get(0);
            Login.init(t1).login(opt);

            ServiceCharge utilityReg = new ServiceCharge(Services.UTILITY_REGISTRATION, subs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureNonFinancialServiceCharge(utilityReg);

            //Add bill for customer
            String billAccNumber2 = DataFactory.getRandomNumberAsString(5);
            biller.addBillForCustomer(subs.MSISDN, billAccNumber2);

            BillerManagement.init(t1).initiateSubscriberBillerAssociation(biller);

            //Initial Balance of subscriber
            initialBal = MobiquityGUIQueries.
                    getUserBalance(subs, null, null);
            t1.info("Initial balance is: " + initialBal.Balance);
            BigDecimal initialBalance = initialBal.Balance;

            //Define Service charge for enable Auto Debit
            ServiceCharge autoDebitEnable = new ServiceCharge(Services.ENABLE_AUTODEBIT, subs, opt,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).
                    configureNonFinancialServiceCharge(autoDebitEnable);

            //Enable Auto Debit by channel user
            usr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            txnId = Transactions.init(t1)
                    .AutoDebitEnableByChannelUser(usr, biller, subs.MSISDN, billAccNumber2, "5");

            //Confirmation for auto debit enable by subscriber
            Transactions.init(t1).AutoDebitConfirmation_Customer(subs.MSISDN, txnId);

            //Approve by biller
            Login.init(t1).login(biller);
            BillerManagement.init(t1).approveorRejectAutoDebitEnableByBiller(subs, true);

            //Check for Approval notification
            SMSReader.init(t1).verifyNotificationContain(subs.MSISDN, "autodebit.subsSelf.notification");

            //Current Balance of subscriber
            currentBalance = MobiquityGUIQueries.
                    getUserBalance(subs, null, null);

            BigDecimal postBalance = currentBalance.Balance;
            t1.info("Current balance is: " + currentBalance.Balance);

            BigDecimal amount = postBalance.subtract(initialBalance);
            Assertion.verifyAccountIsDebited(initialBalance, postBalance, amount,
                    "Service Charge is Debited", t1);

            //Check service charge is deducted
//            if (postbalance < initialBalance) {
//                float amount = (float) (postbalance - initialBalance);
//                Assertion.verifyNotEqual("Initial Balance: " + initialBalance, "Current Balance: " + postbalance + "",
//                        "Service charge of amount: " + amount + " is deducted from subscribers account", t1);
//            } else {
//                t1.fail("Service charge is not deducted from subscribers account");
//            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void PostCondition() throws Exception {

        ExtentTest test = pNode.createNode("teardown", "Concluding Test");
        try {

            String presentValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_CUSTOMER_CONFIRM_REQUIRED");
            if (!presentValue.equalsIgnoreCase(defaultValue)) {
                SystemPreferenceManagement.init(test).updateSystemPreference("IS_CUSTOMER_CONFIRM_REQUIRED", defaultValue);

            }
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
