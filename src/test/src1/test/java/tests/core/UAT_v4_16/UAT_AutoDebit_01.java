package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that Biller can initiate to enable the Auto-Debit for subscriber.
 *                  : To verify that CCE can initiate to disabling the Auto Debit for subscriber through Web.
 * Author Name      : Amith B V, Refactor By Gurudatta Praharaj
 * Created Date     : 02/02/2018
 */

public class UAT_AutoDebit_01 extends TestInit {
    private OperatorUser OptUser1, naUtilBillReg;
    private boolean defaultValue;
    private User wholeSaler, payer, sub;
    private Biller biller;
    private String prefValue, billAccNumber;

    /**
     * To verify that Auto-Debit Enable should get approved when IS_CUSTOMER_CONFIRM_REQUIRED=TRUE IN System Preferences.
     */
    @BeforeClass(alwaysRun = true)
    public void PreCondition() throws Exception {

        ExtentTest tSetup = pNode.createNode("Setup", "Set Preference IS_CUSTOMER_CONFIRM_REQUIRED. " +
                "Fetch the Base Set users for testing purpose. " +
                "Create a New Biller. " +
                "Make sure that the UTILITY_REGISTRATION service charge is configured. " +
                "Make sure that the EnableAuto_Debit service charge is configured.");

        try {
            //Set IS_CUSTOMER_CONFIRM_REQUIRED=TRUE IN System Preferences.
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_CUSTOMER_CONFIRM_REQUIRED");
            defaultValue = AppConfig.isCustConfirmRequired;

            if (!defaultValue) {
                SystemPreferenceManagement.init(tSetup).updateSystemPreference("IS_CUSTOMER_CONFIRM_REQUIRED", "TRUE");
            } else {
                tSetup.info("IS_CUSTOMER_CONFIRM_REQUIRED: Preference is already set to TRUE");
            }

            wholeSaler = DataFactory.getChannelUserWithAccess("CIN_WEB");
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            OptUser1 = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //Make Sure that the Channel User has balance more than the threshold amount
            TransactionManagement.init(tSetup)
                    .makeSureChannelUserHasBalance(wholeSaler);

            TransactionManagement.init(tSetup)
                    .makeSureLeafUserHasBalance(sub);

            ServiceCharge utility = new ServiceCharge(Services.UTILITY_REGISTRATION, sub, OptUser1, null, null, null, null);
            ServiceCharge autoDebit = new ServiceCharge(Services.EnableAuto_Debit, sub, OptUser1, null, null, null, null);

            ServiceChargeManagement.init(tSetup)
                    .configureNonFinancialServiceCharge(autoDebit);

            ServiceChargeManagement.init(tSetup)
                    .configureNonFinancialServiceCharge(utility);

            biller = BillerManagement.init(tSetup)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.DEPENDENCY_TAG})
    public void TUNG12600() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG12600", "To verify that Biller can initiate to\n" +
                " enable the Auto-Debit for subscriber.");
        try {
            Login.init(t1).login(naUtilBillReg);
            billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(sub.MSISDN, billAccNumber);

            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            //Initial Balance of subscriber
            UsrBalance bal = MobiquityGUIQueries.getUserBalance(sub, null, null);
            BigDecimal initialBal = bal.Balance;
            t1.info("Initial Balance: " + initialBal);

            //Login as Biller
            Login.init(t1)
                    .login(biller);

            //Enable Auto debit
            String txnId = BillerManagement.init(t1)
                    .enableAutoDebit(sub.MSISDN, billAccNumber);

            Utils.putThreadSleep(Constants.TWO_SECONDS);
            //Customer has to confirm the Auto Debit Enable via USSD
            Transactions.init(t1)
                    .AutoDebitConfirmation_Customer(sub.MSISDN, txnId);


            //Get the current balance of subscriber
            bal = MobiquityGUIQueries.getUserBalance(sub, null, null);
            BigDecimal currentBal = bal.Balance;
            t1.info("Current Balance: " + currentBal);

            BigDecimal amount = initialBal.subtract(currentBal);
            Assertion.verifyAccountIsDebited(initialBal, currentBal, amount,
                    "Service Charge Is Debited", t1);

            //Check for Approval notification
            SMSReader.init(t1)
                    .verifyNotificationContain(sub.MSISDN, "autodebit.approved.notification");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTO_DEBIT, FunctionalTag.ECONET_UAT_5_0}, dependsOnMethods = "TUNG12600")
    public void TUNG12615() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG12615",
                "To verify that CCE can initiate to disabling the Auto Debit for subscriber through Web.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTO_DEBIT, FunctionalTag.ECONET_UAT_5_0);
        try {
            OperatorUser CCE = DataFactory.getOperatorUserListWithCategory(Constants.CUSTOMER_CARE_EXE).get(0);

            //Login as CCE
            Login.init(t1).login(CCE);

            //Disable autoDebit by CCE
            BillerManagement.init(t1).disableAutoDebit(sub.MSISDN, biller.BillerCode, billAccNumber);

            //verify notification is sent to the customer.
            SMSReader.init(t1).verifyNotificationContain(sub.MSISDN, "autodebit.disabled.notification");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @AfterClass(alwaysRun = true)
    public void PostCondition() throws Exception {
        ExtentTest tearDown = pNode.createNode("Teardown", "set IS_CUSTOMER_CONFIRM_REQUIRED to " + prefValue);
        try {
            if (prefValue.equalsIgnoreCase("FALSE")) {
                SystemPreferenceManagement.init(tearDown).updateSystemPreference("IS_CUSTOMER_CONFIRM_REQUIRED", prefValue);
            } else {
                tearDown.info("IS_CUSTOMER_CONFIRM_REQUIRED: Preference is already set to " + prefValue);
            }
            CustomerBill bill = biller.getAssociatedBill();
            BillerManagement.init(tearDown)
                    .deleteSubsBillerAssociation(bill);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
