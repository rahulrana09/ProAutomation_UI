package tests.core.UAT_v4_16;


import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.AlertHandle;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify whether the system is displaying all settlement pending transaction for churn settlement.
 *                  : Churn settlement initiation should not be successful to an unregistered customer.
 *                  : To verify that user is able to re-initiate reactivation for the rejected record(s)
 *                  : To verify that respective churned wallet balance is moved to the user's default wallet after reactivation
 *                  : The System should Generate a properly Alligned Log File after approving a batch for Chur
 * Author Name      : Amith B V
 * Created Date     : 02/02/2018
 */
public class UAT_ChurnManagement_01 extends TestInit {

    private OperatorUser optUser, optUser2;
    private String defaultProvider;
    private User subsReceiver;

    @BeforeClass(alwaysRun = true)
    public void precondition() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");

        try {
            defaultProvider = DataFactory.getDefaultProvider().ProviderName;
            optUser = DataFactory.getOperatorUsersWithAccess("CHURNMGMT_MAIN").get(0);
            optUser2 = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);
            subsReceiver = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge sCharge = new ServiceCharge(Services.CHURN_REAC_SUB, optUser, new User(Constants.WHOLESALER), null, null, null, null);
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(sCharge);
        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void TUNG12174() throws Exception {
        ExtentTest t1 = pNode.createNode("TUNG12174", " The System should Generate a properly Alligned Log File after approving a batch for Churn.");

        try {
            //Create channeluser
            User subs2174 = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(subs2174);

            //Login as NetworkAdmin
            Login.init(t1).login(optUser);

            //perform Churn initiation
            String batchid = CommonUserManagement.init(t1)
                    .churnInitiateUser(subs2174);

            ChurnUser_Page1.init(t1).navUserChurnApproval()
                    .selectBatchID(batchid);

            ChurnUser_Page1.init(t1).clickOnSubmitBtn();
            AlertHandle.acceptAlert(t1);
            ChurnUser_Page1.init(t1)
                    .checkLogFileSpecificMessage(subs2174.MSISDN, batchid, "churn.approve.log.transaction.ss");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1)
    public void TUNG12173() throws Exception {
        ExtentTest t2 = pNode.createNode("TUNG12173", "To verify whether the system is displaying all settlement pending transaction for churn settlement.");

        try {

            User churnedWhs = CommonUserManagement.init(t2).getChurnedUser(Constants.WHOLESALER, null);
            User churnedSubs = CommonUserManagement.init(t2).getChurnedUser(Constants.SUBSCRIBER, null);

            //Initiate and approve churn settlement
            CommonUserManagement.init(t2).setChurnUserDetails(churnedWhs);
            verifyChurnSettlementPage(churnedWhs, t2);

            //verify for Subscriber
            CommonUserManagement.init(t2).setChurnUserDetails(churnedSubs);
            verifyChurnSettlementPage(churnedSubs, t2);

        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    private void verifyChurnSettlementPage(User user, ExtentTest t1) throws IOException {
        ChurnUser_Page1 page = ChurnUser_Page1.init(pNode);
        Assertion.verifyEqual(page.getUserNameUI(), user.LoginId, "Verify that Username of Churned user is Shown on Settlement Page", t1);
        Assertion.verifyEqual(page.getChurnedMSISDNUI(), user.MSISDN, "Verify that MSISDN of Churned user is Shown on Settlement Page", t1);
    }


    @Test(priority = 3)
    public void TUNG12142() throws Exception {
        ExtentTest t2 = pNode.createNode("TUNG12142", " Churn settlement initiation should not\n" +
                " be successful to an unregistered customer");

        //create channel user without bank association
        User channeluser = CommonUserManagement.init(t2).getChurnedUser(Constants.WHOLESALER, null);

        //Churn settlement initiation  to an unregistered customer
        startNegativeTest();
        CommonUserManagement.init(t2)
                .initiateChurnSettlement(channeluser, channeluser);

        Assertion.verifyErrorMessageContain("churn.settle.error.receiverMsisdn.not.exists",
                "Subscriber MSISDN is not present or Active in the System for provider MFS1", t2);
    }

    @Test(priority = 4)
    public void TUNG12208() throws Exception {
        ExtentTest t2 = pNode.createNode("TUNG12208", "To verify that user is able to\n" +
                " re-initiate reactivation for the rejected record(s).");
        try {
            User churnedUser = CommonUserManagement.init(t2).getChurnedUser(Constants.WHOLESALER, null);

            //Churn settlement initiation  to an active subscriber
            CommonUserManagement.init(t2)
                    .initiateChurnReactivation(churnedUser);

            //Reject and reactivation of churned records
            CommonUserManagement.init(t2)
                    .rejectChurnrecords(churnedUser);

            CommonUserManagement.init(t2)
                    .initiateChurnReactivation(churnedUser);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, enabled = false)
    public void TUNG12219() throws Exception {
        ExtentTest t2 = pNode.createNode("TUNG12219", " : To verify that respective churned" +
                " wallet balance is moved to the user's default wallet after reactivation.");
        try {
            //create channel user without bank association
            User channeluser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t2).createChannelUserDefaultMapping(channeluser, false);

            Login.init(t2).login(optUser2);

            //Perform O2C
            String txn = TransactionManagement.init(t2).initiateO2CWithProvider(channeluser,
                    Constants.MIN_O2C_AMOUNT, defaultProvider);

            TransactionManagement.init(t2).o2cApproval1(txn);

            //check the channeluser balance is credited
            UsrBalance balance = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            int userbalance = Integer.parseInt(balance.Balance.toString());
            t2.info("After o2c channel user balance is " + userbalance);

            Login.init(t2).login(optUser);

            //check initail churn balance
            BigDecimal churnwallet = MobiquityGUIQueries.fetchOperatorBalance("101", "IND10");
            int churnamt = churnwallet.intValue();
            t2.info("Initial amount in churnwallet " + churnwallet);

            // Churn initiation and approve
            CommonUserManagement.init(t2).churnInitiateAndApprove(channeluser);

            // re-initiate reactivation for the rejected record
            CommonUserManagement.init(t2).reactivationOfChurnRecord(channeluser);

            Assertion.assertActionMessageContain("churn.user.modify.message.initiate.ui",
                    "verified churn reactivation", t2);

            //Approve the churn re-activation initiated user
            CommonUserManagement.init(t2).approveChurnSettlement(channeluser.MSISDN, true);
            Assertion.assertActionMessageContain("churn.react.success.approval.ui",
                    "Churn reactivation Approved successfully", t2);

            //check churn balance after reactivation
            churnwallet = MobiquityGUIQueries.fetchOperatorBalance("101", "IND10");
            int churnamt1 = churnwallet.intValue();
            t2.info("After user is reactivated, amount in churnwallet " + churnwallet);

            //check the channeluser balance is credited
            balance = MobiquityGUIQueries.getUserBalance(channeluser, null, null);
            System.out.println(balance.Balance.intValue());
            int userbalance1 = balance.Balance.intValue();
            t2.info("After reactivation user balance is " + userbalance1);

            Assertion.verifyEqual(churnamt, churnamt1, "Same Wallet Balance", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}

