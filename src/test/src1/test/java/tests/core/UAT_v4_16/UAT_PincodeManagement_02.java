package tests.core.UAT_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 4.6.0
 * Objective        : To verify that a channel user cannot perform services through USSD when wrong tpin entered by valid user
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 02/02/2018
 */

public class UAT_PincodeManagement_02 extends TestInit {
    private User subs, channeluser;

    @Test(priority = 1)
    public void cashInWithInvalidTpin() throws Exception {

        ExtentTest t1 = pNode.createNode("TUNG8882", "To verify that a channel user cannot perform services through USSD when wrong tpin entered by valid user");
        try {
            //Create ChannelUser
            channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 2);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(channeluser);

            //Create subscriber
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            Transactions.init(t1)
                    .initiateCashInwithInvalidTpin(subs, channeluser, Constants.MIN_CASHIN_AMOUNT, "ra451")
                    .verifyStatus("FAILED");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Transactions.init(t1).initiateCashIn(subs, channeluser, new BigDecimal("1"));
            Assertion.finalizeSoftAsserts();
        }
    }
}
