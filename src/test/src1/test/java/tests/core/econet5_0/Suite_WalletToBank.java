package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MfsTestProperties;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.JigsawOperations.getNotificationMessage;

public class Suite_WalletToBank extends TestInit {

    private User subscriber, wholesaler;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Pre-Condition,Setting preference");

        try {

            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            ServiceCharge WtoB = new ServiceCharge(Services.SVA_TO_BANK, subscriber, subscriber,
                    null, null, null, null);

            TransferRuleManagement.init(setup)
                    .configureTransferRule(WtoB);

            //Transactions.init(setup).initiateCashIn(subscriber,wholesaler,new BigDecimal(5));

        } catch (Exception e) {
            markSetupAsFailure(e);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1237() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1237",
                "To verify that W2B transaction should get marked as failed in case of timeout and ambigous in case there is no response from bank.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0);

        try {

            String bankId = DataFactory.getDefaultBankIdForDefaultProvider();

            BigDecimal txnAmt = new BigDecimal(5);

            //initiate wallet to bank service
            SfmResponse response = Transactions.init(t1)
                    .walletToBankService(subscriber, txnAmt.toString(), bankId).verifyMessage("sfm.ambiguous.initiated");
            String serReqId = response.ServiceRequestId;
            String txnId = response.TransactionId;


            Transactions.init(t1).performEIGTransactionforFail(serReqId, "false", Services.RESUME_WALLET_TO_BANK);

            //SMSReader.init(t1).verifyNotificationContain(subscriber.MSISDN,"",txnId);
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0})
    public void TC_API_ECONET_044() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TC_API_ECONET_044",
                "To verify that the Proper SMS shoud be sent to Subscriber for Transaction Wallet to Bank.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0);

        try {

            //Subscriber Balance

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            int preBalanceForSubs = preSubsBalance.Balance.intValue();

            //Bank Balance

            BigDecimal bankBalance = MobiquityGUIQueries.getBalanceBankWallet(DataFactory.getDefaultBankNameForDefaultProvider());
            int bankbal = bankBalance.intValue();

            String bankId = DataFactory.getDefaultBankIdForDefaultProvider();

            BigDecimal txnAmt = new BigDecimal(5);

            //initiate wallet to bank service
            SfmResponse response = Transactions.init(t1)
                    .walletToBankService(subscriber, txnAmt.toString(), bankId).verifyMessage("sfm.ambiguous.initiated");

            String serReqId = response.ServiceRequestId;
            String txnId = response.TransactionId;


            SfmResponse response1 = Transactions.init(t1).performEIGTransaction(serReqId, "true", Services.RESUME_WALLET_TO_BANK);
            String txnId1 = response1.TransactionId;

            //Subscriber Balance

            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            int postBalanceForSubs = postSubsBalance.Balance.intValue();

            //Bank Post Balance

            BigDecimal bankPostBalance = MobiquityGUIQueries.getBalanceBankWallet(DataFactory.getDefaultBankNameForDefaultProvider());
            int bankPostbal = bankPostBalance.intValue();

            Assert.assertEquals(postBalanceForSubs, preBalanceForSubs - 5);
            Assert.assertEquals(bankPostbal, bankbal + 5);

            ValidatableResponse resp = getNotificationMessage(subscriber.MSISDN);

            String message = resp.extract().jsonPath().getString("message");

            Assertion.verifyContains(message, "Your wallet to bank account(" + subscriber.MSISDN + ") transfer of " + MfsTestProperties.getInstance().getProperty("mfs1.currency.code") + txnAmt.toString() + " was successful.Tran ID: " + txnId1 + ".", "", t1);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0})
    public void TC_973() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_973",
                "To verify that the Proper SMS shoud be sent to Subscriber for Transaction Account Statement.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.WALLET_TO_BANK, FunctionalTag.ECONET_UAT_5_0);

        try {

            String bankId = DataFactory.getDefaultBankIdForDefaultProvider();

            //initiate Account Statement API
            TxnResponse response = Transactions.init(t1)
                    .getAccountStatement(subscriber, bankId);

            String txnId = response.TxnId;

            ValidatableResponse resp = getNotificationMessage(subscriber.MSISDN);

            String message = resp.extract().jsonPath().getString("message");

            //Assertion.verifyContains(message,"Your wallet to bank account("+subscriber.MSISDN+") transfer of "+ MfsTestProperties.getInstance().getProperty("mfs1.currency.code")+txnAmt.toString()+" was successful.Tran ID: " +txnId1+".","",t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }
}

