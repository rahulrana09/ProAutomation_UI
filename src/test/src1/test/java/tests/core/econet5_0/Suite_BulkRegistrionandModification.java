package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import org.testng.annotations.Test;
import tests.core.base.TestInit;
//** created by CharanTeja Muthe on 03-25-2019*//

public class Suite_BulkRegistrionandModification extends TestInit {

    //login as a valid channel admin
    // click the bulkRegistration and Modification
    //Download the bulk channel user registration template .csv file formate
    //fill the manditory feilds in Template Sheet
    //Upload template  of channel userRegistration into application and click on submit button //

    @Test
    public void TC_ECONET_0882() throws Exception {
        ExtentTest test = pNode.createNode("TC_ECONET_0882", "To verify that Post processing of Bulk user registration file," +
                " Channel admin can download the error log file to view any errors that may have occurred while processing any record of the file.");
        try {
            OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

            User whs_03 = new User(Constants.WHOLESALER);
            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);


            Login.init(test).login(dwFileUser);

            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectChannelUserRegistrationService();

            ChannelUserManagement.init(test)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            whs_03,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false);

            page.uploadFile(FilePath.fileBulkChUserReg);

            page.submitCsv();

            Assertion.verifyErrorMessageContain("bulk.header.mismatch", "Bulk Header Missmatch", test);
        } catch (Exception e) {

            markTestAsFailure(e, test);
        }
    }
}
