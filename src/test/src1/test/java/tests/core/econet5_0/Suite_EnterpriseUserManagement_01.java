package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.MobileRoles;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Suite_EnterpriseUserManagement_01 extends TestInit {

    private User entUser;
    private OperatorUser bulkPayAdm;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            bulkPayAdm = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0644_0212() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0644",
                "To verify that Enterprise user is able to delete initiate a Bulk Payer Admin..");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        OperatorUser bulkPayAdm1 = new OperatorUser(Constants.BULK_PAYER_ADMIN);
        try {
            bulkPayAdm1.setWebGroupRole(DataFactory.getRoleNameFromRoleCode("SAL_AP1", Constants.BULK_PAYER_ADMIN));
            OperatorUserManagement.init(t1)
                    .createBulkPayerAdmin(bulkPayAdm1, entUser);

            Login.init(t1).login(entUser);
            OperatorUserManagement.init(t1)
                    .initModifyOrDeleteOperator(bulkPayAdm1, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC_ECONET_0793",
                "To verify that Enterprise user is able to reject the delete initiated Bulk Payer Admin.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT,
                        FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);
        try {

            OperatorUserManagement.init(t2)
                    .approveDeletionOperatorUser(bulkPayAdm1, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t3 = pNode.createNode("TC_ECONET_0212",
                "To verify that Enterprise user is able to approve the delete initiated Bulk Payer Admin.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(entUser);
            OperatorUserManagement.init(t1)
                    .initModifyOrDeleteOperator(bulkPayAdm1, false);

            OperatorUserManagement.init(t3)
                    .approveDeletionOperatorUser(bulkPayAdm1, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0791() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0791",
                "To verify that Enterprise user is able to reject the add initiated Bulk Payer Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            OperatorUser bulkPayAdmR = new OperatorUser(Constants.BULK_PAYER_ADMIN);

            Login.init(t1).login(entUser);
            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(bulkPayAdmR);

            OperatorUserManagement.init(t1)
                    .approveOperatorUser(bulkPayAdmR, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_0792() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0792", "To verify that Enterprise user is able to reject the modify initiated Bulk Payer Admin.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            Login.init(t1).login(entUser);
            OperatorUserManagement.init(t1)
                    .initModifyOrDeleteOperator(bulkPayAdm, true);

            OperatorUserManagement.init(t1)
                    .approveModifyOperatorUser(bulkPayAdm, false);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }


    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0221() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0221",
                "To verify that while adding Mobile group roles for Enterprise category Bulk Payment service is mandatory to be selected.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            startNegativeTest();
            GroupRoleManagement.init(t1)
                    .addOrRemoveSpecificMobileRole(entUser, "Normal", MobileRoles.WALLET_BULK_PAYMENT,
                            DataFactory.getDefaultProvider().ProviderName, false);

            Assertion.verifyErrorMessageContain("grouprole.select.BulkPayment.Service",
                    "Verification of Bulk Payment role for enterprise", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * //todo Complete the Test Case
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_UAT_0222() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_UAT_0222",
                "To verify that the Proper service charge and commission for both payer and payee should get deducted while performing Bulk Payment.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ENTERPRISE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {



        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0226() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0226", "To verify that while initiating Enterprise Payment the " +
                "following Message should be shown to the user.");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            t1.pass("Message is getting Displayed After successful Initiation of Bulk Payment");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
