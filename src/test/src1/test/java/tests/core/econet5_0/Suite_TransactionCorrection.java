package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.google.common.collect.ImmutableMap;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.*;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.*;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Transaction Correction
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 6/09/2018
 */

public class Suite_TransactionCorrection extends TestInit {

    private static User cashoutUser, subs, subs1, merchant, cashoutUser1;
    private Wallet commWallet;
    private OperatorUser netPricingEngAdd, opt;
    private Biller biller;
    private User whs;
    private ServiceCharge merchPayService;
    private BigDecimal minAmount;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific for this suite");

        try {
            minAmount = new BigDecimal(2);
            cashoutUser = DataFactory.getChannelUserWithAccess("COUT_WEB", 0);
            cashoutUser1 = DataFactory.getChannelUserWithAccess("COUT_WEB", 1);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            subs1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);
            merchant = new User(Constants.MERCHANT);
            netPricingEngAdd = DataFactory.getOperatorUserWithAccess(Roles.ADD_EDIT_DELETE_PRICING_POLICY_INITIATION, Constants.NETWORK_ADMIN);
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);


            biller = BillerManagement.init(t1).getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
            biller.setBillAmount(Constants.BILLER_EXACT_PAYMENT);
            biller.setBillerType(Constants.BILLER_TYPE_PRESENTMENT);


            TransactionManagement.init(t1).makeSureChannelUserHasBalance(cashoutUser);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(cashoutUser1);


            ServiceCharge cashoutService = new ServiceCharge(Services.CASHOUT, subs, cashoutUser, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(cashoutService);

            merchPayService = new ServiceCharge(Services.MERCHANT_PAY, subs, merchant, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(merchPayService);

            ServiceCharge Transcorrec = new ServiceCharge(Services.TXN_CORRECTION, cashoutUser, subs, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(Transcorrec);

            ServiceCharge Transcorrec2 = new ServiceCharge(Services.TXN_CORRECTION, cashoutUser, cashoutUser1, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(Transcorrec2);

            ServiceCharge Transcorrec3 = new ServiceCharge(Services.TXN_CORRECTION, cashoutUser, biller, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(Transcorrec3);

            //create transfer rule for Bill Payment
            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment, cashoutUser,
                    biller, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(bill);

            ServiceCharge Transcorrec1 = new ServiceCharge(Services.TXN_CORRECTION, merchant, subs, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(Transcorrec1);

            ServiceCharge Transcorrec4 = new ServiceCharge(Services.TXN_CORRECTION, biller, cashoutUser1, null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(Transcorrec4);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0501_0503() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0501",
                "To verify that the wholesaler should be able to perform Transaction Correction Initiation successfully from WEB.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0);

        try {

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));

            Login.init(t1).login(cashoutUser);
            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            //Wholesaler Balance

            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int preBalance = preWhsBalance.Balance.intValue();
            int prefrozenbalance = preWhsBalance.frozenBalance.intValue();


            //Subscriber Balance

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalanceForSubs = preSubsBalance.Balance.intValue();

            //Transaction Correction Initiation

            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            //Wholesaler Balance

            UsrBalance whsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int balanceAfterInit = whsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = whsBalanceAfterInit.frozenBalance.intValue();

            //Subscriber Balance

            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInitForSubs = subsBalanceAfterInit.Balance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + minAmount.intValue());
            t1.info("Expected: " + (prefrozenbalance + minAmount.intValue()) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);
            Assert.assertEquals(balanceAfterInitForSubs, preBalanceForSubs);

            //Transaction Correction Approval

            ExtentTest t2 = pNode.createNode("TC_ECONET_0503",
                    "To verify that the Frozen balance of user should debit in case Transaction correction initiation is approved.");

            TransactionCorrection.init(t2).approveOrRejectTxnCorrection(txnid, true);

            //Wholesaler Balance

            Thread.sleep(2000);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int postBalance = postWhsBalance.Balance.intValue();
            int postFrozenbalance = postWhsBalance.frozenBalance.intValue();

            //Subscriber Balance

            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postBalanceForSubs = postSubsBalance.Balance.intValue();

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - minAmount.intValue());
            t1.info("Expected: " + (frozenbalanceAfterInit - minAmount.intValue()) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postBalance, balanceAfterInit - minAmount.intValue());
            t1.info("Expected: " + (balanceAfterInit - minAmount.intValue()) + " Actual: " + postBalance);

            Assert.assertEquals(postBalanceForSubs, balanceAfterInitForSubs + minAmount.intValue());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Property File change required, Cannot be automated
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0692() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0692",
                "To verify that the Transaction correction initiated doesn't have approval within timelimit then transaction correction should fail.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {


            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            Login.init(t1).login(cashoutUser);
            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            //Wholesaler Balance
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int preBalance = preWhsBalance.Balance.intValue();
            int prefrozenbalance = preWhsBalance.frozenBalance.intValue();

            //Subscriber Balance
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalanceForSubs = preSubsBalance.Balance.intValue();
            //Transaction Correction Initiation

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));
            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            //Wholesaler Balance
            UsrBalance whsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int balanceAfterInit = whsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = whsBalanceAfterInit.frozenBalance.intValue();

            //Subscriber Balance
            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInitForSubs = subsBalanceAfterInit.Balance.intValue();

            Thread.sleep(1000);
            Assertion.verifyEqual(frozenbalanceAfterInit, prefrozenbalance + minAmount.intValue(), "Verify Frozen Balance", t1);
            t1.info("Expected: " + (prefrozenbalance + minAmount.intValue()) + " Actual: " + frozenbalanceAfterInit);

            Assertion.verifyEqual(balanceAfterInit, preBalance, "Verify Balance", t1);
            Assertion.verifyEqual(balanceAfterInitForSubs, preBalanceForSubs, "Verify Subs Balance", t1);

            Thread.sleep(5000);
            //Transaction Correction Approval
            startNegativeTest();
            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid, false);

            String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnid);
            Assert.assertEquals(dbTxnStatus, "TF");

            //Wholesaler Balance

            Thread.sleep(2000);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int postBalance = postWhsBalance.Balance.intValue();
            int postFrozenbalance = postWhsBalance.frozenBalance.intValue();

            //Subscriber Balance

            UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int postBalanceForSubs = postSubsBalance.Balance.intValue();

            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - minAmount.intValue());
            t1.info("Expected: " + (frozenbalanceAfterInit - minAmount.intValue()) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postBalance, balanceAfterInit);
            t1.info("Expected: " + (balanceAfterInit) + " Actual: " + postBalance);

            Assert.assertEquals(postBalanceForSubs, balanceAfterInitForSubs);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0693() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0693",
                "To verify if the wholesaler has done transaction on wrong number, and the receiver has used partial amount, then system should not be able to reverse the partial amount from the wrong recipient's wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0);

        try {


            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            //Subscriber Balance


            //Perform Cash In
            Login.init(t1).login(cashoutUser1);
            String txnId = TransactionManagement.init(t1).performCashIn(subs, "5");

            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int preBalanceForSubs = preSubsBalance.Balance.intValue();

            //Transaction Correction Initiation
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHIN));
            startNegativeTest();
            TransactionCorrection.init(t1).initiateTxnCorrection(txnId, false, false);
            Assertion.verifyErrorMessageContain("insufficient.balance", "Verify Error Message", t1);


            //Subscriber Balance

            UsrBalance subsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(subs, null, null);
            int balanceAfterInitForSubs = subsBalanceAfterInit.Balance.intValue();

            Thread.sleep(1000);

            Assertion.verifyEqual(balanceAfterInitForSubs, preBalanceForSubs, "Compare Post Balance", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1030() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1030",
                "To verify that the Frozen balance of Wholesaler should rollback to user wallet in case Transaction correction initiation is rejected.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0);

        try {
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            Login.init(t1).login(cashoutUser);
            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            //Wholesaler Balance

            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int preBalance = preWhsBalance.Balance.intValue();
            int prefrozenbalance = preWhsBalance.frozenBalance.intValue();


            //Transaction Correction Initiation
            String txnid = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            //Wholesaler Balance

            UsrBalance whsBalanceAfterInit = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int balanceAfterInit = whsBalanceAfterInit.Balance.intValue();
            int frozenbalanceAfterInit = whsBalanceAfterInit.frozenBalance.intValue();

            Thread.sleep(1000);
            Assert.assertEquals(frozenbalanceAfterInit, prefrozenbalance + minAmount.intValue());
            t1.info("Expected: " + (prefrozenbalance + minAmount.intValue()) + " Actual: " + frozenbalanceAfterInit);
            Assert.assertEquals(balanceAfterInit, preBalance);

            //Transaction Correction Approval

            TransactionCorrection.init(t1).rejectTxnCorrection(txnid);

            String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnid);
            Assert.assertEquals(dbTxnStatus, "TF");

            //Wholesaler Balance

            Thread.sleep(2000);
            UsrBalance postWhsBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, null, null);
            int postBalance = postWhsBalance.Balance.intValue();
            int postFrozenbalance = postWhsBalance.frozenBalance.intValue();


            Assert.assertEquals(postFrozenbalance, frozenbalanceAfterInit - minAmount.intValue());
            t1.info("Expected: " + (frozenbalanceAfterInit - minAmount.intValue()) + " Actual: " + postFrozenbalance);
            Thread.sleep(2000);
            Assert.assertEquals(postBalance, balanceAfterInit);
            t1.info("Expected: " + (balanceAfterInit) + " Actual: " + postBalance);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1031() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1031",
                "To verify that the Wholesaler can not perform transaction if user transaction amount is more than the available balance and less than the sum of current balance & frozen balance.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0);

        try {
            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));

            cashoutUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(cashoutUser, false);
            TransactionManagement.init(t1).initiateAndApproveO2C(cashoutUser, "5", "O2C");

            Login.init(t1).login(cashoutUser);
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            //Transaction Correction Initiation

            TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            //Perform Cash In
            Transactions.init(t1).initiateCashInwithInvalidTpin(subs1, cashoutUser, "10", ConfigInput.tPin).verifyMessage("60019");
            t1.pass("User is not able to perform transaction more than available balance");

            ExtentTest t2 = pNode.createNode("TC_ECONET_1032",
                    "To verify that the Only Current amount excluding frozen balance should displayed on USSD menu of Wholesaler and usuable.");

            //Perform Cash In
            Transactions.init(t2).initiateCashIn(subs1, cashoutUser, new BigDecimal(3));
            t2.pass("User is able to perform transaction with his available balance");


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0401() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0401",
                "To verify that the netadmin should able to perform Transaction correction for MERCHANT PAYMENT.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            setTxnProperty(of("txn.reversal.allowed.services", Services.MERCHANT_PAY));

            merchant = CommonUserManagement.init(t1).getTempChannelUser(Constants.MERCHANT, "OFFLINE", null);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(merchant);

            SfmResponse response = Transactions.init(t1).initiateMerchantPayment(merchant, subs, "10");
            response.assertStatus(Constants.TXN_STATUS_SUCCEEDED, t1);
            String txnid = response.TransactionId;

            String txnid1 = TransactionCorrection.init(t1).initiateTxnCorrection(txnid, true, true);

            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid1, true);

            String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnid1);
            Assert.assertEquals(dbTxnStatus, "TS");


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0691() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0691",
                "To verify that the Valid User should able to perform Transaction correction for C2C. (Including Service Charge and commission)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_UAT_5_0);

        try {

            setTxnProperty(of("txn.reversal.allowed.services", Services.C2C));

            ServiceCharge sCharge = new ServiceCharge(Services.C2C, cashoutUser1, cashoutUser, null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(sCharge);

            Login.init(t1).login(netPricingEngAdd);

            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "SERVICE", "mfs1.currency.code", "2");
            PricingEngine.init(t1).addApprovalForServiceCharge("C2C Transfer", "SERVICE", netPricingEngAdd.LoginId);
            PricingEngine.init(t1).addServiceChargeForDefaultCurrency(sCharge, "COMMISSION", "mfs1.currency.code", "1");
            PricingEngine.init(t1).addApprovalForServiceCharge("C2C Transfer", "COMMISSION", netPricingEngAdd.LoginId);

            BigDecimal initialServiceAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int initialServiceChargeAmount = initialServiceAmount.intValue();
            System.out.println("isca" + initialServiceChargeAmount);

            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, commWallet.WalletId, null);
            int preBalance = channelUsrBalance.Balance.intValue();
            System.out.println("prb" + preBalance);

            Login.init(t1).login(cashoutUser);

            String txnId = TransactionManagement.init(t1).performC2C(cashoutUser1, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            BigDecimal finalServiceAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalServiceChargeAmount = finalServiceAmount.intValue();
            System.out.println("fsca" + finalServiceChargeAmount);

            UsrBalance channelUsrPostBalance = MobiquityGUIQueries.getUserBalance(cashoutUser, commWallet.WalletId, null);
            int postBalance = channelUsrPostBalance.Balance.intValue();
            System.out.println("pob" + postBalance);

            Assert.assertEquals(initialServiceChargeAmount + 2, finalServiceChargeAmount);
            Assert.assertEquals(preBalance + 1, postBalance);

            String txnid1 = TransactionCorrection.init(t1).initiateTxnCorrection(txnId, true, true);

            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid1, true);

            String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnid1);
            Assertion.verifyEqual(dbTxnStatus, Constants.TXN_STATUS_SUCCESS, "Verify Transaction Status in DB", t1);

            BigDecimal finalServiceAmount1 = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalServiceChargeAmount1 = finalServiceAmount1.intValue();
            System.out.println("ffsca" + finalServiceChargeAmount1);

            UsrBalance channelUsrPostBalance1 = MobiquityGUIQueries.getUserBalance(cashoutUser, commWallet.WalletId, null);
            int postBalance1 = channelUsrPostBalance1.Balance.intValue();
            System.out.println("pob1" + postBalance1);

            Assert.assertEquals(initialServiceChargeAmount, finalServiceChargeAmount1);
            t1.pass("Service Charge Amount is getting reversed");
            Assert.assertEquals(preBalance, postBalance1);
            t1.pass("Commission is getting reversed");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1125() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1125",
                "To verify that valid user is able to revert Bill payment transactions successfully through transaction correction module.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSACTIONS_CORRECTION, FunctionalTag.ECONET_SIT_5_0);

        try {

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.AgentAssistedPresentmentBillPayment));

            //Generate file for Bill upload
            String accNo = DataFactory.getRandomNumberAsString(5);
            String filename = BillerManagement.init(t1).generateBillForUpload(biller, accNo);

            //Upload bill
            opt = DataFactory.getOperatorUsersWithAccess("BILLUP").get(0);
            Login.init(t1).login(opt);
            BillerManagement.init(t1).bulkBillerAssociationFileUpload(filename);

            //create a offline channel user
            whs = new User(Constants.WHOLESALER);
            whs.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);
            //whs.writeDataToExcel(true);
            //whs = DataFactory.getUserUsingMsisdn("7711110186");


            //Check channel user has enough balance
            TransactionManagement.init(t1).initiateAndApproveO2C(whs, "100", "Test");
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs, new BigDecimal(10));

            //create transfer rule for Bill Payment
            ServiceCharge bill = new ServiceCharge(Services.AgentAssistedPresentmentBillPayment, whs,
                    biller, null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(bill);

            //Perform Bill Payment
            SfmResponse response = Transactions.init(t1).initiateOfflineBillPayment(biller, whs, "10", accNo);
            String txnId = response.TransactionId;

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.AgentAssistedPresentmentBillPayment));

            String txnid1 = TransactionCorrection.init(t1).initiateTxnCorrection(txnId, true, true);

            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(txnid1, true);

            String dbTxnStatus = MobiquityGUIQueries.dbGetTransactionStatus(txnid1);
            Assert.assertEquals(dbTxnStatus, "TS");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1115_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1115_a", "To verify that the There shouldn’t be any pending transaction " +
                "for Channel User at the time of Churning. The pending Transactions should be settled prior to Churning.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHURN, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);
        try {

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.CASHOUT));

            Login.init(t1).login(cashoutUser);
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            String tid = TransactionManagement.init(t1).performCashOut(subs, minAmount.toString(), null);
            MobiquityGUIQueries m = new MobiquityGUIQueries();
            String referencenum = MobiquityGUIQueries.dbGetReferencenumber(tid);

            Transactions.init(t1).cashoutApprovalBysubs(subs, referencenum);

            //Transaction Correction Initiation

            String tid1 = TransactionCorrection.init(t1).initiateTxnCorrection(tid, true, true);

            //Churn the wholesaler
            CommonUserManagement.init(t1)
                    .startNegativeTest()
                    .churnInitiateUser(cashoutUser);

            ChurnUser_Page1.init(t1).checkLogFileSpecificMessage(cashoutUser.MSISDN, "not.churn.channel.existing.transaction");

            TransactionCorrection.init(t1).approveOrRejectTxnCorrection(tid1, true);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
