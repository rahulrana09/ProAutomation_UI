package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.pinManagement.PinManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.pinManagement.ResetPinPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import static framework.util.jigsaw.CommonOperations.getAvailableBalanceForOperatorUsersBasedOnWalletNo;

public class Suite_ResetPIN extends TestInit {
    OperatorUser opt, usrCreator, usrApprover;
    User whs;
    String pref;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "SecretQuestionOrAnswerManagement");

        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");

        whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("USER_INPUTTED_QUESTIONS_ENABLED");

        if (pref.equalsIgnoreCase("FALSE")) {

            SystemPreferenceManagement.init(eSetup).updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", "TRUE");
        }

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0705() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0705", "To verify that the Reset PIN funtionality has been charged to the user");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PIN_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG).assignAuthor(Author.SARASWATHI);

        try {

            //Setting Service Charge

            ServiceCharge sCharge = new ServiceCharge(Services.RESET_PIN, whs, opt, null, null, null, null);
            sCharge.setNFSCServiceCharge("2");
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(sCharge);

            BigDecimal initialServiceAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int initialServiceChargeAmount = initialServiceAmount.intValue();

            //O2C
            TransactionManagement.init(t1).initiateAndApproveO2CWithProvider(whs, DataFactory.getDefaultProvider().ProviderName, "10");

            //Adding Question to Master

            String quesCode = "QA1";
            String question = "What is the name of your favorite pet";

            HashMap<String, String> questions1 = new HashMap<>();
            questions1.put(quesCode, question);

            Login.init(t1).login(opt);
            PinManagement.init(t1).addQuestionsToMaster(questions1, Constants.LANGUAGE1);

            int flag = 0;

            ArrayList<String> categories = MobiquityGUIQueries.dbGetCategoriesForSelfRestPIN();
            for (String category : categories) {
                if (category.equalsIgnoreCase(Constants.WHOLESALER)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 1) {
                t1.info("Self PIN reset rule for wholesaler is already defined");
            } else {
                ArrayList<String> questions = MobiquityGUIQueries.dbGetQuestionsFromMaster(Constants.LANGUAGE1, "N");

                ArrayList<String> expectedQns = new ArrayList<>();
                expectedQns.add(questions.get(0));

                Login.init(t1).login(opt);

                PinManagement.init(t1).addPinResetRulesForCategory(whs, expectedQns, Constants.MIN_THRESHOLD);
            }

            //Resetting PIN

            Login.init(t1).login(whs);

            ResetPinPage page1 = new ResetPinPage(t1);

            page1.setAnswer(Constants.ANSWER);
            page1.clickNext();
            page1.navRestPin();
            page1.setAnswer(Constants.ANSWER);
            page1.clickNext();

            page1.setNewPin(ConfigInput.tPin);
            page1.setConfirmPin(ConfigInput.tPin);
            page1.selectPinType("TPIN");
            page1.clickSubmit();

            Assertion.verifyActionMessageContain("pin.reset.success", "PIN Reset successful", t1, "2.00", "0.00", "0.00");

            BigDecimal finalServiceAmount = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalServiceChargeAmount = finalServiceAmount.intValue();

            Assert.assertEquals(finalServiceChargeAmount, initialServiceChargeAmount + 2);
            t1.pass("Service Charge is Deducted correctly for Reset Pin");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @AfterClass(alwaysRun = true)
    public void postrequisite() throws Exception {
        ExtentTest fSetup = pNode.createNode("Setup", "SecretQuestionOrAnswerManagement");
        SystemPreferenceManagement.init(fSetup).updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", pref);

    }
}
