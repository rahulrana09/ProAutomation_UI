package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.Wallet;
import framework.entity.InstrumentTCP;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.pricingEngine.PricingEngine;
import framework.features.systemManagement.TCPManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.jigsaw.JigsawOperations;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.jigsaw.CommonOperations.getAvailableBalanceForOperatorUsersBasedOnWalletNo;


/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : InverseC2C
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 5/09/2018
 */

public class Suite_InverseC2C extends TestInit {

    private static User payer, payee;
    private static OperatorUser networkAdmin;
    ServiceCharge inversec2c;
    Wallet wallet;
    private String providerName;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        ExtentTest setup = pNode.createNode("Setup", "Setup Specific for this class");

        try {

            String pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("EXT_REQ_CAT");

            if (pref.contains(Constants.WHOLESALER)) {
                pNode.pass("Code is present in the preference");
            } else {
                pNode.fail("Code is not present in the preference");
                Assert.fail();
            }
            payer = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 0);
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER, 1);
            networkAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            inversec2c = new ServiceCharge(Services.INVERSEC2C, payer, payee,
                    null, null, null, null);
            TransferRuleManagement.init(setup)
                    .configureTransferRule(inversec2c);

            providerName = DataFactory.getDefaultProvider().ProviderName;


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C})
    public void TC_ECONET_0365() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0365",
                "To verify that wholesaler is able to perform Inverse C2C service successfully if the valid details are entered.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C).assignAuthor(Author.SARASWATHI);

        try {
            User payer = DataFactory.getChannelUserWithAccess(Roles.INVERSE_CHANNEL_TO_CHANNEL_TRANSFER);

            checkPre(payer, payee);
            //Login with valid user

            Login.init(t1).login(payer);

            String txnid1 = TransactionManagement.init(t1).inverseC2C(payee, "5");

            //Confirm Inverse C2C
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);

            try {
                JigsawOperations.setUseServiceInBody(false);
                SfmResponse response = Transactions.init(t1)
                        .inversec2cConfirmation(payee, reqID, true);

                response.verifyStatus(Constants.TXN_STATUS_SUCCEEDED);

                checkPost(payer, payee);

                DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0987() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0987",
                "To verify that Inverse channel to channel transaction will not be successful and transaction should fail, if Payer wholesaler enters invalid details.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            //Login with valid user
            Login.init(t1).login(payer);
            String txnid1 = TransactionManagement.init(t1).inverseC2C(payee, "5");

            //Confirm Inverse C2C
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);


            try {
                JigsawOperations.setUseServiceInBody(false); // todo  add comment
                SfmResponse response = Transactions.init(t1)
                        .inversec2cConfirmation(payer, reqID, false);
                response.verifyMessage("txn.operation.notAuthorized");
                t1.pass("Inverse C2C has been failed due to invalid details entered");

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0988_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0988_a",
                "To verify that Inverse channel to channel Should not be successful when payer is barred as Sender.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            //Login with valid user

            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).barChannelUser(payer, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);

            Login.init(t1).login(payee);
            startNegativeTest();
            TransactionManagement.init(t1).inverseC2C(payer, "5");
            Assertion.verifyErrorMessageContain("invc2c.barred.as.sender", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        } finally {
            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).unBarChannelUser(payer, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_SENDER);
        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0988_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0988_b",
                "To verify that Inverse channel to channel Should not be successful when payer is barred as both.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.C2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            //Login with valid user

            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).barChannelUser(payer, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_BOTH);

            Login.init(t1).login(payee);
            startNegativeTest();
            TransactionManagement.init(t1).inverseC2C(payer, "5");
            Assertion.verifyErrorMessageContain("invc2c.barred.as.both", "Verify Error Message", t1, payer.MSISDN);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        } finally {
            Login.init(t1).login(networkAdmin);
            ChannelUserManagement.init(t1).unBarChannelUser(payer, Constants.BAR_CHANNEL_CONST, Constants.BAR_AS_BOTH);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0989_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0989_a",
                "To verify that  wholesaler can not initiate inverse C2C if service charge is not defined");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            BigDecimal initialServiceCharge = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int initialAmount = initialServiceCharge.intValue();

            /*Login.init(t1).login(networkAdmin);
            PricingEngine.init(t1).deleteServiceChargeForDefaultCurrency(inversec2c,"Service");
*/
            //Login with valid user
           /* Login.init(t1).login(payer);

            String txnid1 = TransactionManagement.init(t1).inverseC2C(payee, "5");

            //Confirm Inverse C2C
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);

            try {
                JigsawOperations.setUseServiceInBody(false); // todo  add comment
                SfmResponse response = Transactions.init(t1)
                        .inversec2cConfirmation(payee, reqID, true);

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }

            BigDecimal finalServiceCharge = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalAmount=finalServiceCharge.intValue();

            Assert.assertEquals(initialAmount,finalAmount);
*/
            ExtentTest t2 = pNode.createNode("TC_ECONET_0989_b",
                    "To verify that  wholesaler can not initiate InverseC2C if service charge is suspended");
            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0);

            //Suspending the Service Charge
            Login.init(t2).login(networkAdmin);

            //PricingEngine.init(t2).addServiceChargeForDefaultCurrency(inversec2c,"Service","2");
            PricingEngine.init(t2).suspendService(inversec2c);
            //Login with valid user
            Login.init(t2).login(payer);
            String txnid2 = TransactionManagement.init(t2).inverseC2C(payee, "5");

            //Confirm Inverse C2C
            String reqID1 = MobiquityGUIQueries.dbGetServiceRequestId(txnid2);

            try {
                JigsawOperations.setUseServiceInBody(false); // todo  add comment
                SfmResponse response = Transactions.init(t2)
                        .inversec2cConfirmation(payee, reqID1, true);

            } finally {
                JigsawOperations.setUseServiceInBody(true);
            }


            BigDecimal finalServiceCharge1 = getAvailableBalanceForOperatorUsersBasedOnWalletNo(DataFactory.getDefaultProvider().ProviderId + "IND03");
            int finalAmount1 = finalServiceCharge1.intValue();

            Assert.assertEquals(initialAmount, finalAmount1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0991_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0991_a",
                "To verify that inverse C2C request will not be successful if Transfer rule is not defined.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            TransferRuleManagement.init(t1).deleteTransferRuleForSpecificGrade(inversec2c, Constants.GRADE_ALL, Constants.GRADE_ALL);
            //Login with valid user
            Login.init(t1).login(payer);
            startNegativeTest();
            TransactionManagement.init(t1).inverseC2C(payee, "5");
            Assertion.verifyErrorMessageContain("5001", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {


            TransferRuleManagement.init(t1)
                    .configureTransferRule(inversec2c);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0991_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0991_b",
                "To verify that inverse C2C request will not be successful if Transfer rule is suspended.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            TransferRuleManagement.init(t1).initiateApproveSuspenedTransferRule(inversec2c, "Suspended", true);
            //Login with valid user
            Login.init(t1).login(payer);
            startNegativeTest();
            TransactionManagement.init(t1).inverseC2C(payee, "5");
            Assertion.verifyErrorMessageContain("transfer.rule.suspended", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            TransferRuleManagement.init(t1).initiateApproveSuspenedTransferRule(inversec2c, "Initiated", true);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C})
    public void TC_ECONET_0990() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0990",
                "To verify that wholesaler is not able to perform Inverse C2C service successfully if invalid details are entered.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C).assignAuthor(Author.SARASWATHI);

        try {

            User payer = DataFactory.getChannelUserWithAccess(Roles.INVERSE_CHANNEL_TO_CHANNEL_TRANSFER);

            //Login with valid user
            Login.init(t1).login(payer);

            startNegativeTest();

            TransactionManagement.init(t1).inverseC2C(payer, "5");

            Assertion.verifyErrorMessageContain("sender.receiver.should.not.be.same", "Verify Error Message", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Disabled this case as it is incorrectly automated
     * This case require deletion of TCP
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0992() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0992",
                "To verify that  Wholesaler can not initiate inverse C2C if TCP is not defined.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVERSEC2C, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            checkPre(payer, payee);
            InstrumentTCP insTcp = DataFactory.getInstrumentTCP(payee.DomainName, payee.CategoryName, payee.GradeName, providerName, "Normal");
            String l_tcpID = insTcp.ProfileName;

            Login.init(t1).login(networkAdmin);
            TCPManagement.init(t1).editTcpForChannelUser(payer, l_tcpID, payer.DomainName, payer.DomainName, payee.GradeName, DataFactory.getDefaultProvider().ProviderName, "Inverse C2C Transfer", "0");
            //Login with valid user
            Login.init(t1).login(payer);
            String txnid1 = TransactionManagement.init(t1).inverseC2C(payee, "5");

            //Confirm Inverse C2C
            String reqID = MobiquityGUIQueries.dbGetServiceRequestId(txnid1);

            try {
                JigsawOperations.setUseServiceInBody(false); // todo  add comment
                SfmResponse response = Transactions.init(t1)
                        .inversec2cConfirmation(payee, reqID, true);
                response.verifyMessage("0100004");
                t1.pass("Wholesaler can not initiate inverse C2C if TCP is not defined");

            } finally {
                JigsawOperations.setUseServiceInBody(true);
                Login.init(t1).login(networkAdmin);
                TCPManagement.init(t1).editTcpForChannelUser(payer, l_tcpID, payer.DomainName, payer.DomainName, payee.GradeName, DataFactory.getDefaultProvider().ProviderName, "Inverse C2C Transfer", Constants.MAX_THRESHOLD);

            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}