package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.InstrumentTCP;
import framework.entity.PseudoUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by prashant.kumar on 2/21/2019.
 */
public class Suite_PseudoUser extends TestInit {
    private User whsUsr, sub;
    private PseudoUser pseudoUser;
    private static InstrumentTCP instTCP;
    private String providerID, tcp;

    @BeforeClass(alwaysRun = true)
    public void preRequisite() throws Exception {
        try {
            whsUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            instTCP = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
                    "WALLET", DataFactory.getDefaultWallet().WalletName);
        } catch (Exception e) {

            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1)
    public void TC_USSD_0011() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0011", "To verify that the Psuedo user can not use channel user PIN while performing any transaction.");

        try {
            //pseudoUser.setMpin();
            pseudoUser.setParentUser(whsUsr);
            PseudoUserManagement.init(t1).createCompletePseudoUser(pseudoUser);
            Transactions.init(t1).selfReimbursementforPseudoUser(pseudoUser, Constants.REIMBURSEMENT_AMOUNT);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, enabled = false)
    public void TC_USSD_0015() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0015", "To verify that Pseudo User Should not able to do Cashin if Daily Threshold reach for parent.").assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT);

        try {
            User channelUser = new User(Constants.WHOLESALER);
            TCPManagement.init(t1).addInstrumentTCP(instTCP);
            TCPManagement.init(t1).modifyInitiateInstrumentTCP(instTCP, "1", "10", "1", "5").modifyApproveInstrumentTCP(instTCP);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channelUser, false);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(channelUser);
            // pseudoUser.setMpin();
            pseudoUser.setParentUser(whsUsr);
            PseudoUserManagement.init(t1).createCompletePseudoUser(pseudoUser);
            Login.init(t1).login(pseudoUser.LoginId, pseudoUser.Password);
            startNegativeTest();
            //Performing Transaction
            Transactions.init(t1).initiateCashInthroughPseudoUser(subs, pseudoUser, new BigDecimal(10));
            Transactions.init(t1).initiateCashInthroughPseudoUser(subs, pseudoUser, new BigDecimal(10));
            String Actual = "";
            String Expected = "";
            Assert.assertEquals(Actual, Expected);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }


    /**
     * //TODO Complete the Test Case .
     *
     * @throws Exception
     */
    @Test(priority = 3, enabled = false, groups = {FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void pseudoUserCashinCheck() throws Exception {

        ExtentTest t3 = pNode.createNode("TC_USSD_0012", "To verify that Pseudo User should be able to do Cashin.")
                .assignCategory(FunctionalTag.PSEUDO_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            PseudoUser pseudoUser = new PseudoUser();

            User channeluser = DataFactory.getChannelUserWithAccess(
                    Roles.ADD_PSEUDO_USER);

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        /*pseudoUser.setParentUser(channeluser);
        //TODO Remove the Hard Coded value
        pseudoUser.setCategoryDetails("PsCat98769");

        pseudoUser.setPseudoGroupRole("NewRole");

        PseudoUserManagement.init(t3).
                addPseudoUser(channeluser,pseudoUser);

*/
            pseudoUser.setParentUser(channeluser);
            PseudoUserManagement.init(t3).createCompletePseudoUser(pseudoUser);

            Login.init(t3).login(pseudoUser.LoginId, pseudoUser.Password);

            TransactionManagement.init(t3).performCashIn(subs, "10");
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }


    @Test(priority = 4, enabled = false)
    public void TC_USSD_0014() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0014", "To verify that Pseudo User Should not able to do Cashin if Daily/weekly/monthly Threshold reach.").assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PSEUDO_USER_MANAGEMENT);

        try {
            User channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            providerID = DataFactory.getDefaultProvider().ProviderId;
            tcp = "pseudoTcp" + DataFactory.getRandomNumberAsString(3);
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(channelUser);
            // pseudoUser.setMpin();
            // Login.init(t1).login(channelUser);
            PseudoUser pseudoUser = new PseudoUser();
            pseudoUser.setParentUser(channelUser);
            PseudoUserManagement.init(t1).createCompletePseudoUser(pseudoUser);
            PseudoUserManagement.init(t1).addUpdatePseudoTcp(pseudoUser, providerID, Constants.NORMAL_WALLET, tcp, tcp);
            PseudoUserManagement.init(t1).ModifyCountAndAmountInPseudoTCP(channelUser, pseudoUser, "1", "10");
            // Login.init(t1).login(pseudoUser.LoginId,pseudoUser.Password);
            startNegativeTest();
            //Performing Transaction
            Transactions.init(t1).initiateCashInthroughPseudoUser(subs, pseudoUser, new BigDecimal(10));
            Transactions.init(t1).initiateCashInthroughPseudoUser(subs, pseudoUser, new BigDecimal(10));
            String Actual = "";
            String Expected = "";
            Assert.assertEquals(Actual, Expected);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PSEUDO_USER_MANAGEMENT})
    public void TC_USSD_0012() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0012", "To verify that Pseudo User" +
                " Should be able to do Cashin ");

        try {
            PseudoUser pseudoUser = new PseudoUser();
            pseudoUser.setParentUser(whsUsr);
            PseudoUserManagement.init(t1).createCompletePseudoUser(pseudoUser);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whsUsr, new BigDecimal(100));

            DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(whsUsr, null, null);
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(sub, null, null);
            DBAssertion.preRecon = MobiquityGUIQueries.getReconBalance();

            //perform cash in to subscriber through pseudoUser, but the amount is deducted from whs
            Login.init(t1).login(pseudoUser);
            TransactionManagement.init(t1).performCashIn(sub, Constants.CASHIN_TRANS_AMOUNT, DataFactory.getDefaultProvider().ProviderName);

            DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(whsUsr, null, null);
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(sub, null, null);
            DBAssertion.preRecon = MobiquityGUIQueries.getReconBalance();


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }
}
