package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/*
    TC_ECONET_0508
    Login as nwAdmin
    Navigate ChannelUserManagement > add Channel User
    in first page verify the field "Relationship Officer" is dispalyed

    TC_ECONET_0509
    should be able to initiate Channel user with Relationship Officer Details

*/


public class Suite_RelationOfficercaptureInSubscriberRegistration extends TestInit {
    OperatorUser nwAdmin;
    User chUser;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        try {
            nwAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            chUser = new User(Constants.WHOLESALER);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.SUBS_REL_OFF_CAPTURE, FunctionalTag.ECONET_SIT_5_0})

    public void TC_ECONET_0808() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0508", "To verify that the While Adding " +
                "Wholesaler in the system, there should be a field of 'Relationship Officer Name' instead of 'Designation' .");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SUBS_REL_OFF_CAPTURE, FunctionalTag.ECONET_SIT_5_0);
        try {

            Login.init(t1).login(nwAdmin);

            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(pNode);
            pageOne.navAddChannelUser();

            boolean status = Utils.checkElementPresent("//*[@id='confirm2_confirmAddChannelUser']/table/tbody/tr[10]/td[3]/label",
                    Constants.FIND_ELEMENT_BY_XPATH, t1);

            Assertion.verifyEqual(status, true, "Relationship Officer Field", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REL_OFF_CAPTURE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0509() throws Exception {
        ExtentTest t2 = pNode.createNode("TC_ECONET_0509", "To verify that the " +
                "Field Name 'Relationship Officer Name' should get saved successfully after " +
                "Channel User Registration.");

        try {
            Login.init(t2).login(nwAdmin);

            ChannelUserManagement.init(t2).createChannelUserDefaultMapping(chUser, false);

            String dbDetails = MobiquityGUIQueries.fetchRelationShipDetails(chUser.MSISDN);
            boolean actual = false;

            if (dbDetails != null) {
                actual = true;
            }

            Assertion.assertEqual(actual, true,
                    "Relationship Officer Details Are Available For The User: " + chUser.FirstName + "", t2);


        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }
}