package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

/**
 * Created by Nirupama MK on 21/08/2018.
 */

public class Suite_SingleMSISDN_MultipleUserType extends TestInit {
    private User chUser;

    /*
          TC_ECONET_0809
          wehn preference "CHANNEL_USER_AS_SUBS_ALLOWED" to "Y"
          should be able to create the subscriber with same MSISDN as that of channel user

          TC_ECONET_0810
          set the preference "CHANNEL_USER_AS_SUBS_ALLOWED" to "N"
          should not be able to create the subscriber with same MSISDN as that of channel user

        */
    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific for this suite");

        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(eSetup).makeSureChannelUserHasBalance(chUser, new BigDecimal(250));
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.SINGLE_MSISDN_MUL_USER, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0809() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0809",
                "To verify that Single MSISDN multiple role setup configuration would allow users to get registered in " +
                        "system as Channel user as-well as Consumers with the same mobile number.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_MSISDN_MUL_USER);

        try {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

            User chUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.setMSISDN(chUser.MSISDN);

            startNegativeTest();

            ConfigInput.changePin = false;

            TxnResponse response = SubscriberManagement.init(t1)
                    .createSubscriberWithSpecificPayIdUsingAPI(sub1, GlobalData.defaultWallet.WalletId, false);

            response.assertStatus("200");
            response.getMessageVerificationStatus("dob.enhancement.reg.success", sub1.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "N");

        }

        Assertion.finalizeSoftAsserts();
    }


    // test steps are not clear
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM,
            FunctionalTag.SINGLE_MSISDN_MUL_USER,
            FunctionalTag.CRITICAL_CASES_TAG}, enabled = false)
    public void TC_ECONET_1069() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0810",
                "To verify that the there is two PIN one for channel user and one for customer.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SINGLE_MSISDN_MUL_USER);

        try {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("CHANNEL_USER_AS_SUBS_ALLOWED", "Y");

            User sub1 = new User(Constants.SUBSCRIBER);
            sub1.setMSISDN(chUser.MSISDN);

            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub1, true, false);

            //Perform change Mpin service
            Transactions.init(t1)
                    .changeChannelUserMPinSecondTime(chUser, "2468");
            Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(chUser, "2468", ConfigInput.tPin);
            Transactions.init(t1).subscriberBalanceEnquiry(sub1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
    }
}

