package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

public class Suite_MultiCurrency extends TestInit {



    private static boolean checkNonDefaultProviderPresent(ExtentTest t1){
        if (DataFactory.getAllProviderNames().size() > 1) {
            return true;
        } else {
            t1.skip("Only 1 MFS Provider is present in ConfigInput.xlsx. This Test Case require at-least 2 MFS Providers.");
            return false;
        }
    }




    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0549() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0549", "To verify that the Valid user " +
                "should be able to perform stock initiation with new currency FCA.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0);

        try {

            if (checkNonDefaultProviderPresent(t1)) {

                CurrencyProvider nonDefProvider = DataFactory.getNonDefaultProvider();
                // perform stock initiation for FCA provider and FCA bank
                StockManagement.init(t1).initiateAndApproveNetworkStock(nonDefProvider.ProviderName,
                        nonDefProvider.Bank.BankName, Constants.STOCK_TRANSFER_AMOUNT);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0550() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0550", "To verify that the Valid user " +
                "should be able to perform O2C with new currency FCA.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0);

        try {

            if(checkNonDefaultProviderPresent(t1)) {
                User wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

                String serviceId = Transactions.init(t1)
                        .initiateO2C(DataFactory.getOperatorUserWithAccess(Roles.OWNER_TO_CHANNEL_TRANSFER_INITIATE),
                                wholesaler, Constants.MAX_O2C_AMOUNT,
                                DataFactory.getDefaultWallet().WalletId,
                                DataFactory.getNonDefaultProvider().ProviderId)
                        .ServiceRequestId;

                Transactions.init(t1).
                        approveO2C(DataFactory.getOperatorUserWithAccess(Roles.OWNER_TO_CHANNEL_TRANSFER_APPROVAL2), serviceId);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0548() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0548", "To verify that the Proper accounts " +
                "should be created in mobiquity system as well to support new currency.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0);

        try {
            OperatorUser nwAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            User whs = new User(Constants.WHOLESALER);

            Login.init(t1).login(nwAdmin);
            ChannelUserManagement.init(t1).initiateChannelUser(whs);
            ChannelUserManagement.init(t1).assignHierarchy(whs);

            CommonUserManagement.init(t1).assignWebGroupRole(whs);
            Select selProviderUI = new Select(driver.findElement(By.name("counterList[0].providerSelected")));
            List<WebElement> options = selProviderUI.getOptions();
            ArrayList<String> provider = new ArrayList<String>();
            for (WebElement opt : options) {
                String pro = opt.getText();
                provider.add(pro);
            }
            Assertion.verifyListContains(provider, DataFactory.getDefaultProvider().ProviderName,
                    "Verify ZWL provider is listed in dropDown", t1);

            Assertion.verifyListContains(provider, DataFactory.getNonDefaultProvider().ProviderName,
                    "Verify FCA provider is listed in dropDown", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0547() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0547", "To verify that the Proper accounts " +
                "should be created in mobiquity system as well to support new currency; Reimbursement");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.MULTICURRENCYRAND, FunctionalTag.ECONET_UAT_5_0);

        try {

            if (checkNonDefaultProviderPresent(t1)) {

                OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("STOCK_REINIT");
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                String refNum = DataFactory.getRandomNumberAsString(5);

                ServiceCharge sChargeOPTWithDraw = new ServiceCharge(Services.OPERATOR_WITHDRAW, whs, optUsr,
                        null, null, DataFactory.getNonDefaultProvider().ProviderName, DataFactory.getNonDefaultProvider().ProviderName);

                TransferRuleManagement.init(t1)
                        .configureTransferRule(sChargeOPTWithDraw);

                Login.init(t1).login(optUsr);
                String txnId = StockManagement.init(t1)
                        .stockReimbursementInitiation(whs, DataFactory.getNonDefaultProvider().ProviderId,
                                refNum, Constants.REIMBURSEMENT_AMOUNT, Constants.REMARKS,
                                null, false);

                Login.init(t1).loginAsOperatorUserWithRole(Roles.STOCK_REIMBURSEMENT_APPROVAL);
                StockManagement.init(t1).approveReimbursement(txnId);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
