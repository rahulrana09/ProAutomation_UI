package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 10/5/2018.
 */
public class Suite_TransferRules extends TestInit {

    private static OperatorUser usrTRuleCreator;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        //usrTRuleCreator = DataFactory.getOperatorUserWithAccess("T_RULES");
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0})
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0055 ", "To verify that the Proper error message should get displayed on web if user try to add another transfer rule for the same combination.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        try {
            ServiceCharge tRuleCashIn = new ServiceCharge(Services.AUTO_O2C, new OperatorUser(Constants.NETWORK_ADMIN), new User(Constants.WHOLESALER), null, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(tRuleCashIn);
            TransferRuleManagement.init(t1).configureTransferRule(tRuleCashIn);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0})
    public void Test_02() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0054\n ", "To verify that Users other network admin should not able to add initiate Transfer Rule.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0);
        t1.assignAuthor(Author.PRASHANT);
        /*Login.init(t1).login(DataFactory.getOptUserWithOutAccess("T_RULES",t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TRULES_ALL", "TRULES_T_RULES");
*/
        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("TRULES_T_RULES"));
                t1.fail("Transfer Rule link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("Transfer Rule link is not displayed for Channel Admin as Expected", t1);
            }

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("TRULES_T_RULES"));
                t1.fail("Transfer Rule link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Transfer Rule link is not displayed for Channel User as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0056() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0056 ", "To verify that the During addition" +
                " of transfer rule, Status, Transfer Type, Direct Transfer allowed field should display in " +
                "Radio/Dropdown feature.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_UAT_5_0);

        User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        User sub = DataFactory.getSubscriberUser();
        //Configure Transfer Rule for Cashin
        ServiceCharge sCharge = new ServiceCharge(Services.CASHIN, whs, sub,
                null, null, null, null);
        try {

            OperatorUser usrTRuleCreator = DataFactory.getOperatorUserWithAccess("T_RULES");

            TransferRuleManagement.init(t1)
                    .deleteTransferRule(sCharge);

            Login.init(t1).login(usrTRuleCreator);
            TransferRuleManagement.init(t1).setSenderReceiverDetailsForTransferRule(sCharge);
            TransferRuleManagement.init(t1).setCategoryDetailsForTransferRule(sCharge);

            Boolean isStatusPresent = Utils.checkElementPresent("trRule_confirmCoU_statusId", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(true, isStatusPresent, "Status Element Present", t1, true);

            Boolean isTransferTypePresent = Utils.checkElementPresent("trRule_confirmCoU_transferType", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(true, isTransferTypePresent, "Transfer Type Element Present", t1, true);

            Boolean isDirectTransfrAlowdPresent = Utils.checkElementPresent("trRule_confirmCoU_directTransferAllowedtrue", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(true, isDirectTransfrAlowdPresent, "Direct Transfer Allowed Present Element Present", t1, true);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            TransferRuleManagement.init(t1).configureTransferRule(sCharge);
        }
    }


    /**
     * @Test :
     * @Description :
     */
    @Test(groups = {FunctionalTag.ECONET_SIT_5_0}, priority = 5)
    public void test() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0892", "To verify that valid user(Network admin) can reject the initiated Transfer Rules between the Domains.");

        t1.assignCategory(FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_SIT_5_0);
        try {

            User subs = new User(Constants.SUBSCRIBER);

            ServiceCharge sChargeForTRule = new ServiceCharge(Services.CASH_IN_OTHERS1, new User(Constants.WHOLESALER), subs, null, null, null, null);

            TransferRuleManagement tRuleMgmtObj = TransferRuleManagement.init(t1);

            if (tRuleMgmtObj.isTransferRuleAvailable(sChargeForTRule)) {
                t1.skip("Transfer Rule is already created");
            } else {
                tRuleMgmtObj.initiateTransferRuleCreation(sChargeForTRule).
                        approveRejectTransferRule(sChargeForTRule.TransferRuleID, false);
            }

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        //
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0}, priority = 5)
    public void test1() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0894",
                "To verify that valid user(Network admin) can reject modified initiated Transfer Rules between the two Domains for particular MFS provider.");

        t1.assignCategory(FunctionalTag.TRANSFER_RULE, FunctionalTag.ECONET_SIT_5_0);
        try {

            User whs = new User(Constants.WHOLESALER);

            ServiceCharge sChargeForTRule = new ServiceCharge(Services.INVERSEC2C, whs, whs, null, null, null, null);

            TransferRuleManagement tRuleMgmtObj = TransferRuleManagement.init(t1);

            if (tRuleMgmtObj.isTransferRuleAvailable(sChargeForTRule)) {

                tRuleMgmtObj.modifyTransferRule(sChargeForTRule).
                        approveRejectTransferRule(sChargeForTRule.TransferRuleID, false);
            } else {
                tRuleMgmtObj.initiateTransferRuleCreation(sChargeForTRule).
                        approveRejectTransferRule(sChargeForTRule.TransferRuleID, true);

                tRuleMgmtObj.modifyTransferRule(sChargeForTRule).
                        approveRejectTransferRule(sChargeForTRule.TransferRuleID, false);
            }

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


}
