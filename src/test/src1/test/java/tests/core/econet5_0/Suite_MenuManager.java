package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.entity.User;
import framework.features.menuManager.MenuManager;
import framework.pageObjects.menuManager.MenuManagerChecker_Page;
import framework.pageObjects.menuManager.MenuManager_Page;
import framework.pageObjects.menuManager.MenuManager_Page2;
import framework.pageObjects.menuManager.MenuServlet;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_MenuManager extends TestInit {


    String category = "AGENT";

    @Test(priority = 1, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void enterHeaderText() {
        ExtentTest t1 = pNode.createNode("MenuManager_01",
                "To verify that Admin user is able to define the Header Text successfully.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            driver.navigate().to(ConfigInput.menuManagerMakerUrl);

            MenuManager_Page menuManager = new MenuManager_Page(t1);
            menuManager.selectCategory(category.toUpperCase());
            Thread.sleep(2000);
            try {
                menuManager.clickGatheruserInput();
            } catch (Exception e) {
                pNode.info(e);
                menuManager.clickDiscardAllLink();
                Thread.sleep(2000);
                menuManager.selectCategory(category);
                Thread.sleep(2000);
                menuManager.clickGatheruserInput();
            }
            menuManager.enterText("Welcome to ECOCASH");
            Thread.sleep(2000);
            menuManager.saveHeaderText();
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void addMenuItem() {
        ExtentTest t1 = pNode.createNode("MenuManager_02",
                "To verify that the Maker is able to Add new menu item.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void addSubMenuItem() {
        ExtentTest t1 = pNode.createNode("MenuManager_03",
                "To verify that the Maker is able to add sub menu to Menu Item.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_P2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            menuManager_P2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", "Confirm");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void addMenu() {
        ExtentTest t1 = pNode.createNode("MenuManager_04",
                "To verify that Maker is able to define the menu of categories" +
                        " for which menu are to be constructed.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void associateStaticMenuList() {
        ExtentTest t1 = pNode.createNode("MenuManager_05",
                "To verify that the maker is able to associate the static menu list.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void additionalParameterInMenu() {
        ExtentTest t1 = pNode.createNode("MenuManager_06",
                "To verify that for each menu option in a static choice " +
                        "list additional parameters can be associated.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void associateTerminateService() {
        ExtentTest t1 = pNode.createNode("MenuManager_07",
                "To verify that the maker is able to associate Terminate service.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            //menuManager.clickNextBtn();
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void modifyExistingMenuItem() {
        ExtentTest t1 = pNode.createNode("MenuManager_08",
                "To verify that the Admin is able to modify existing Menu/Sub menu item.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            driver.navigate().to(ConfigInput.menuManagerMakerUrl);
            menuManager.selectCategory(category);
            menuManager.clickNextBtn();
            menuManager_page2.clickOnEditMenuBtn();
            MenuManager.init(t1).modifyMenuLabel("Send Money_1");
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void rejectMenuSentForApproval() {
        ExtentTest t1 = pNode.createNode("MenuManager_09",
                "To verify that checker is able to reject the menu's sent for approval.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", "Confirm");
            Thread.sleep(2000);
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            MenuManager.init(t1).submitForApproval("Approval required for Automation");
            Thread.sleep(2000);
            MenuManager.init(t1).rejectAtChecker(ConfigInput.menuManagerCheckerUrl, category);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            driver.navigate().to(ConfigInput.menuManagerMakerUrl);
            menuManager.selectCategory(category);
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void approveMenuSentForApproval() {
        ExtentTest t1 = pNode.createNode("MenuManager_10",
                "To verify that checker is able to approve the menu's sent for approval.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", "Confirm");
            Thread.sleep(2000);
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            MenuManager.init(t1).submitForApproval("Approval required for Automation");
            Thread.sleep(2000);
            MenuManager.init(t1).approveAtChecker(ConfigInput.menuManagerCheckerUrl, category, "Approving via Automation", 0);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 11, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void checkMenuAlongWithRemarks() {
        ExtentTest t1 = pNode.createNode("MenuManager_11",
                "To verify after submitting for approval of menu to checker, " +
                        "checker is able to see the correct menus with remarks sent for approval.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        String menu1 = "Enter Pin", menu2 = "Send Money", menu3 = "Confirm";
        String remarks = "Approval required for Automation";
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManagerChecker_Page menuChecker = new MenuManagerChecker_Page(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, menu1);
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", menu2);
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", menu3);
            Thread.sleep(2000);
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            MenuManager.init(t1).submitForApproval(remarks);
            Thread.sleep(2000);
            String[] menuElements = new String[]{menu1, menu2, menu3};
            String[] values = MenuManager.init(t1).checkMenuViaChecker(ConfigInput.menuManagerCheckerUrl, category, menuElements);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            menuChecker.selectCheckerActionAtDiff();
            menuChecker.enterRemarks("Approving via Automation");
            menuChecker.clickRejectBtn();
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Assertion.assertEqual(values[0], remarks, "Verifying the remarks at checker", t1);
            Assertion.assertEqual(values[1], "true", "Validating the menus at checker", t1);
            driver.navigate().to(ConfigInput.menuManagerMakerUrl);
            menuManager.selectCategory(category);
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 12, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void changeApplicableDateByChecker() {
        ExtentTest t1 = pNode.createNode("MenuManager_12",
                "To verify that Approver is able to Change the applicable date " +
                        "from which the menu version will be live in production.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        String menu1 = "Enter Pin", menu2 = "Send Money", menu3 = "Confirm";
        String remarks = "Approval required for Automation";
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManagerChecker_Page menuChecker = new MenuManagerChecker_Page(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, menu1);
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", menu2);
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", menu3);
            Thread.sleep(2000);
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            MenuManager.init(t1).submitForApproval(remarks);
            Thread.sleep(2000);
            MenuManager.init(t1).approveAtChecker(ConfigInput.menuManagerCheckerUrl, category, "Approving via Automation", 1);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void reorderingMenuItem() {
        ExtentTest t1 = pNode.createNode("MenuManager_13",
                "To verify that the Maker is able to do reordering of Menu/Sub menu Items.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);
        String[] menuNames = {"Send Money", "Register", "Others"};
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_p2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addmultipleMenus(3, menuNames);
            Thread.sleep(3000);
            menuManager_p2.reorderMenu();
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Thread.sleep(2000);
            menuManager_p2.clickReorderBtn();
            Thread.sleep(2000);
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 14, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void gatherUserInput() {
        ExtentTest t1 = pNode.createNode("MenuManager_14",
                "To verify that the maker is able to associate Gather user input menu.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);
        String[] menuNames = {"Send Money", "Register", "Others"};
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_p2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            MenuManager.init(t1).enterValidationDetail();
            MenuManager.init(t1).enterErrorMessageDetail();
            Thread.sleep(2000);
            MenuManager.init(t1).enterPrefixDetail();
            Thread.sleep(2000);
            menuManager.clickDiscardAllLink();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 15, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void uniqueVersionCheck() {
        ExtentTest t1 = pNode.createNode("MenuManager_15",
                "To verify that all the approved menus for a particular " +
                        "category will have a unique version number associated with it.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_p2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, "Enter Pin");
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", "Send Money");
            menuManager_p2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", "Confirm");
            Thread.sleep(2000);
            menuManager_p2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate("Automation Success Text", "Automation Error Text");
            MenuManager.init(t1).submitForApproval("Approval required for Automation");
            Thread.sleep(2000);
            MenuManager.init(t1).approveAtChecker(ConfigInput.menuManagerCheckerUrl, category, "Approving via Automation", 0);
            Thread.sleep(2000);

            driver.navigate().to(ConfigInput.menuManagerMakerUrl);
            menuManager.selectCategory(category);
            menuManager.clickGatheruserInput();
            menuManager_p2.clickCloneApprovedMenu();
            Thread.sleep(2000);
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            boolean found = menuManager_p2.uniqueMenuVersions();
            Assertion.assertEqual(found, false, "Verifying that duplicate versions exists.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 16, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void rollBackMenu() {
        ExtentTest t1 = pNode.createNode("MenuManager_16",
                "To verify admin is able to Rollback menu to a previously approved version. ").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_p2 = new MenuManager_Page2(t1);

            driver.navigate().to(ConfigInput.menuManagerMakerUrl);
            menuManager.selectCategory(category);
            try {
                menuManager.clickGatheruserInput();
            } catch (Exception e) {
                pNode.info(e);
                menuManager.clickDiscardAllLink();
                Thread.sleep(2000);
                menuManager.selectCategory(category);
                Thread.sleep(2000);
                menuManager.clickGatheruserInput();
            }
            menuManager_p2.clickCloneApprovedMenu();
            Thread.sleep(2000);
            menuManager_p2.clickCloneButton();
            Thread.sleep(1000);
            String message = menuManager_p2.getAlertMessage();
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            Thread.sleep(2000);
            menuManager.clickDiscardAllLink();
            Assertion.assertEqual(message, "Copy of Menu Created Successfully", "Verifying menu rollback.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 17, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void goToMenu() {
        ExtentTest t1 = pNode.createNode("MenuManager_17",
                "To verify that 'Go To Menu' allows user to be re-directed to a specific menu " +
                        "when particular error code is returned as a response from the service.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        String menu1 = "Enter Pin", menu2 = "Send Money", menu3 = "Confirm";
        String remarks = "Approval required for Automation";
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManagerChecker_Page menuChecker = new MenuManagerChecker_Page(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, menu1);
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem("Select Option:", menu2);
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem("Select", menu3);
            menuManager_page2.clickNextMAndSM();
            menuManager_page2.clickAssociateServiceForGotoMenu();
            /*menuManager_page2.clickToSelectService();
            menuManager_page2.clickServiceName();*/
            menuManager_page2.clickPlusIconToAddService();
            menuManager_page2.clickSaveConstructService();
            menuManager_page2.clickConstructServiceRequest();
            menuManager_page2.enterdetailinmandatory("TEXT");
            menuManager_page2.enterGoToMenuResCode("212");
            menuManager_page2.goToMenu();
            Thread.sleep(2000);
            menuManager_page2.selectFromMakerTree();
            Thread.sleep(2000);
            menuManager_page2.clickSaveReqResBtn();
            Thread.sleep(1000);
            String message = menuManager_page2.getAlertMessage();
            Thread.sleep(2000);
            Assertion.assertEqual(message, "Data Saved Successfully", "Verifying Data saved.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 18, groups = {FunctionalTag.v5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.WEB_MENU_MANAGER})
    public void checkMenuServlet() {
        ExtentTest t1 = pNode.createNode("MenuManager_18",
                "To verify that when the end user logs in, system first checks if the MSISDN of the user exists." +
                        " If it exists, appropriate menu that has been defined for his menu category is shown.").
                assignCategory(FunctionalTag.ECONET_UAT_5_0);

        String header1 = "Welcome to ECOCASH";
        String header2 = "Select Option:";
        String header3 = "Select";
        String menu1 = "Enter Pin", menu2 = "Send Money", menu3 = "Confirm";
        String msg1 = "Automation Success Text";
        String url = ConfigInput.menuManagerServletUrl;
        try {
            MenuManager_Page menuManager = new MenuManager_Page(t1);
            MenuServlet menuServlet = new MenuServlet(t1);
            MenuManager_Page2 menuManager_page2 = new MenuManager_Page2(t1);
            MenuManager.init(t1).addMenu(ConfigInput.menuManagerMakerUrl, category, menu1);
            menuManager.clickNextBtn();
            menuManager.clickAssociateStaticMenuList();
            MenuManager.init(t1).addMainMenuItem(header2, menu2);
            menuManager_page2.clickAssociateStaticMenu();
            MenuManager.init(t1).addSubMenuItem(header3, menu3);
            Thread.sleep(2000);
            menuManager_page2.clickTerminateLink();
            Thread.sleep(2000);
            MenuManager.init(t1).associateTerminate(msg1, "Automation Error Text");
            MenuManager.init(t1).submitForApproval("Approval required for Automation");
            Thread.sleep(2000);
            MenuManager.init(t1).approveAtChecker(ConfigInput.menuManagerCheckerUrl, category, "Approving via Automation", 0);
            Thread.sleep(2000);

            User user = DataFactory.getChannelUserWithCategory("RT", 0);
            driver.navigate().to(url);
            menuServlet.enterMobileNumber(user.MSISDN);
            String[] inputValues = {"*101#", ConfigInput.mPin, "1", "1"};
            String menuOutput;
            for (int i = 0; i < inputValues.length; i++) {
                menuOutput = MenuManager.init(t1).checkMenuAtServlet(inputValues[i]).replaceAll("\\r|\\n", " ");
                if (i == 0) {
                    Assertion.verifyContains(menuOutput, header1 + " " + menu1, "Verifying menu:", t1);
                } else if (i == 1) {
                    Assertion.verifyContains(menuOutput, header2 + " 1." + menu2, "Verifying menu:", t1);
                } else if (i == 2) {
                    Assertion.verifyContains(menuOutput, header3 + " 1." + menu3, "Verifying menu:", t1);
                } else if (i == 3) {
                    Assertion.assertEqual(menuOutput, msg1, "Verifying menu:", t1);
                }
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
