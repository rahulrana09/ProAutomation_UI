package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.enterpriseManagement.BulkPaymentPage1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_PayrollEnhancements extends TestInit {
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PAYROLL_ENHANCEMENTS, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0539() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0539", "To verify that" +
                " there should be validation check box while uploading Bulk salary file.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PAYROLL_ENHANCEMENTS, FunctionalTag.ECONET_UAT_5_0);

        try {
            User entp = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            Login.init(t1).login(entp);

            BulkPaymentPage1 page = new BulkPaymentPage1(t1);
            page.NavigateToLink();

            Assertion.verifyEqual(page.name.isDisplayed(), true, "Beneficiary Validation Attributes 'Name'Is displayed", t1);
            Assertion.verifyEqual(page.nation_id.isDisplayed(), true, "Beneficiary Validation Attributes 'National Id' Is displayed", t1, true);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0540", "To verify that the following checkboxes " +
                    "is available to select the validation parameters during the payroll file upload:\n" +
                    "o National ID\n" +
                    "o Full Name (First name last name)\n" +
                    "o both");

            Assertion.verifyEqual(page.name.isEnabled(), true, "Name Checkbox Is Enabled", t2);
            Assertion.verifyEqual(page.nation_id.isEnabled(), true, "National Id Checkbox Is Enabled", t2, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}
