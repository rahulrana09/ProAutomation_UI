package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_BalanceEnquiry extends TestInit {


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_USSD_0238() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_USSD_0238", "To verify that the Subscriber/Channel user should not be able to perform Balance Enquiry if he is barred in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {

            OperatorUser opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);
            ChannelUserManagement.init(t1).barChannelUser(whs, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_BOTH);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, whs, opt,
                    null, null, null, null);

            balance.setNFSCServiceCharge("2");

            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            Transactions.init(t1).BalanceEnquiryWithInvalidMpinTpin(whs, ConfigInput.mPin, ConfigInput.tPin);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }
}
