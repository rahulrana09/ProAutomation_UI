package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.pinManagement.PinManagement;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.pinManagement.ManageSelfPinResetRules_pg1;
import framework.pageObjects.pinManagement.ResetPinPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class Suite_AuthenticationManagement_02 extends TestInit {
    FunctionLibrary fl = new FunctionLibrary(DriverFactory.getDriver());
    private OperatorUser netadmin;
    private User whs;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        netadmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0620() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0620", "To verify that there would be a " +
                "system preference setting to enable the option of having the 'Maximum Account Balance Allowed' field.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG);
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", "TRUE");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MAX_ACC_BAL_ALLOWED", "TRUE");

            Login.init(t1).performLogout();
            Utils.putThreadSleep(Constants.TWO_SECONDS);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.MANAGE_SELF_PIN_RESET_RULES);

            ManageSelfPinResetRules_pg1 page = new ManageSelfPinResetRules_pg1(t1);
            page.navManageSelfRestPinRules();
            page.clickAddNewRule();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            Assertion.verifyEqual(page.isMaxAccountBalanceTextBoxVisible(), true, "Max Acc Bal Allowed Text Box Available", t1, true);

            /**
             * TC_ECONET_0774a
             */
            ExtentTest t2 = pNode.createNode("TC_ECONET_0774a", "To verify that If disabled" +
                    "'Maximum Account Balance Allowed' field., then the field would not be displayed to the user.");
            Login.init(t2).loginAsOperatorUserWithRole(Roles.SYSTEM_PREFERENCES);
            SystemPreferenceManagement.init(t2)
                    .updateSystemPreference("MAX_ACC_BAL_ALLOWED", "FALSE");

            Login.init(t1).loginAsOperatorUserWithRole(Roles.MANAGE_SELF_PIN_RESET_RULES);
            page.navManageSelfRestPinRules();
            page.clickAddNewRule();
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            Assertion.verifyEqual(page.isMaxAccountBalanceTextBoxVisible(), false, "Max Acc Bal Allowed Text Box Not Available", t2, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0775_d() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_d", "To verify that if Subscriber" +
                " is barred as Sender then user is able to perform non- financial transactions across" +
                " any bearer if service charge is more than 0");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            Login.init(t1).login(netadmin);
            SubscriberManagement.init(t1).barSubscriber(subs, Constants.BAR_AS_SENDER);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, subs, netadmin,
                    null, null, null, null);

            balance.setNFSCServiceCharge("2");
            balance.setNFSCCommission("2");

            ServiceChargeManagement.init(t1).deleteNFSChargeForSpecificWallet(balance);
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).subscriberBalanceEnquiry(subs);
            res1.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0775_f() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_f", "To verify that if Subscriber is barred" +
                " as Both then user is able to perform non- financial transactions across any bearer" +
                " if service charge is more than 0");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, new BigDecimal(10));

            Login.init(t1).login(netadmin);
            SubscriberManagement.init(t1).barSubscriber(subs, Constants.BAR_AS_BOTH);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).subscriberBalanceEnquiry(subs);
            res1.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0775_e() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0775_e", "To verify that if Channel User is " +
                "barred as Both then user is able to perform non- financial transactions across " +
                "any bearer if service charge is more than 0");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {

            User whs1 = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs1, false);

            TransactionManagement.init(t1).makeSureChannelUserHasBalance(whs1);

            Login.init(t1).login(netadmin);
            ChannelUserManagement.init(t1).barChannelUser(whs1, Constants.USER_TYPE_CHANNEL, Constants.BAR_AS_BOTH);

            ServiceCharge balance = new ServiceCharge(Services.BALANCE_ENQUIRY, new User(Constants.WHOLESALER), new OperatorUser(Constants.NETWORK_ADMIN),
                    null, null, null, null);

            balance.setNFSCServiceCharge("2");
            balance.setNFSCCommission("2");

            ServiceChargeManagement.init(t1).deleteNFSChargeForSpecificWallet(balance);
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(balance);

            //Non-Financial
            TxnResponse res1 = Transactions.init(t1).BalanceEnquiry(whs1);
            res1.verifyStatus(Constants.TXN_SUCCESS);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0774b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0774b", "To verify that if the user’s total " +
                "account balance is higher than the value set in the field," +
                "then the self PIN reset functionality won’t be available to the user.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.AUTHENTICATION_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG);
        String defaultPref = null;

        //check is there any question already added
        ArrayList<String> questions = MobiquityGUIQueries.dbGetQuestionsFromMaster(Constants.LANGUAGE1, "N");

        try {
            SystemPreferenceManagement.init(t1).
                    updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", "TRUE");

            defaultPref = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("MAX_ACC_BAL_ALLOWED");

            String amt = "" + DataFactory.getRandomNumber(2);

            OperatorUser usr = DataFactory.getOperatorUserWithAccess("STOCK_ENQ");
            Login.init(t1).login(usr);

            if (defaultPref.equalsIgnoreCase("FALSE")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference
                        ("MAX_ACC_BAL_ALLOWED", "TRUE");
            }

            int flag = 0;
            ArrayList<String> categories = MobiquityGUIQueries.dbGetCategoriesForSelfRestPIN();

            for (String category : categories) {
                if (category.equalsIgnoreCase(Constants.WHOLESALER)) {
                    flag = 1;
                    break;
                }
            }

            //if flag is 1, thn Pin set rule is defined, hence edit it
            if (flag == 1) {
                t1.info("Self PIN reset rule for wholesaler is already defined");
                PinManagement.init(t1).editPinResetRulesForCategory(whs, questions, amt);

            } else {

                //if there is no questions exist & no Pin set rule defined, then add new question & define pin set rule
                if (questions.size() == 0) {
                    String quesCode = "QA1";
                    String question = "What is the name of your favorite pet";

                    HashMap<String, String> Mquestions = new HashMap<>();
                    Mquestions.put(quesCode, question);

                    Login.init(t1).login(netadmin);
                    PinManagement.init(t1).addQuestionsToMaster(Mquestions, Constants.LANGUAGE1);

                    String value = MobiquityGUIQueries.dbGetQuestionCodeForQuestion(question);

                    if (value.equalsIgnoreCase(quesCode)) {
                        t1.pass("Question is reflected in DB");
                    } else {
                        t1.fail("Question is not reflected in DB");
                    }
                }
                //if question exist, but not pin set rule defined, thn below code wil add pin set rule
                ArrayList<String> expectedQns = new ArrayList<>();
                expectedQns.add(questions.get(0));
                PinManagement.init(t1).addPinResetRulesForCategory(whs, expectedQns, amt);
            }

            Login.init(t1).login(whs);
            ResetPinPage page = new ResetPinPage(t1);

            if (fl.elementIsDisplayed(page.SkipForLater)) {
                ResetPinPage page1 = new ResetPinPage(t1);
                page1.setAnswer(Constants.ANSWER);
                page1.clickNext();
            }

            fl = FunctionLibrary.init(t1);
            fl.verifyLinkNotAvailable("PINMANAGE_ALL", null);
            /*boolean view = Utils.checkElementPresent("(PINMANAGE_ALL", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyNotEqual(view, true, "Pin Management Link Not present as expected", t1);
*/
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);

        } finally {
            Login.init(t1).login(netadmin);
            PinManagement.init(t1).editPinResetRulesForCategory(whs, questions, Constants.MAX_THRESHOLD);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0776() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0776", "To verify that if operator is barred " +
                "then operator should not be able to login into web portal, Hence Operator will not be able to do " +
                "any financial transactions as well as non-financial transactions as well.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BARRING_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {

            OperatorUser opt = CommonUserManagement.init(t1).getBarredOperatorUser(Constants.NETWORK_ADMIN);
            Login.init(t1)
                    .startNegativeTest()
                    .login(opt);

            Assertion.verifyErrorMessageContain("opt.error.login",
                    "Authentication failed", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

    }

}