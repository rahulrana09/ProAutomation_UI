package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.globalSearch.GlobalSearch;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Nirupama MK on 29/08/2018.
 */
public class Suite_SubscriberSearchUsingIdNumberAndName extends TestInit {

    private OperatorUser optUser;
    private User subs;

    @BeforeMethod(alwaysRun = true)
    public void before_method() throws Exception {

        optUser = DataFactory.getOperatorUserWithAccess("RP_GOS");
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0583() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0583", "To verify that the " +
                "Valid User should be able to search Subscriber using name.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.ECONET_UAT_5_0);
        try {

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(t1).login(optUser);

            GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_FIRSTNAME, subs.FirstName);
            GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LASTNAME, subs.LastName);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0611() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0611", "To verify that the Valid User " +
                "should be able to search Subscriber using ID number.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.GLOBAL_SEARCH, FunctionalTag.ECONET_UAT_5_0);
        try {

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(t1).login(optUser);
            GlobalSearch.init(t1).globalSearch(Constants.GLOBAL_SEARCH_USING_LOGINID, subs.LoginId);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}