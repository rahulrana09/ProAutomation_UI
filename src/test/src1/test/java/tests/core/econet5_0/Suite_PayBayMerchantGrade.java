package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.entity.InstrumentTCP;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.TCPManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;
import java.util.List;

public class Suite_PayBayMerchantGrade extends TestInit {

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PAYBAY_MERCHANT_GRADE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1129() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1129", "To verify that the Valid user should able to add new head merchant of grade PayBay.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.PAYBAY_MERCHANT_GRADE, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            User headMer = DataFactory.getChannelUserWithCategory(Constants.HEAD_MERCHANT);

            Grade grade = new Grade(Constants.HEAD_MERCHANT);
            grade.setGradeCode("PayBay");
            grade.setGradeName("PayBay");

            //check if the grade "PayBay" already exists
            int flag = 0;
            List<String> grades = MobiquityGUIQueries.dbGetGradesPerCategory(headMer.CategoryCode);
            for (String gra : grades) {
                if (gra.equalsIgnoreCase(grade.GradeName)) {
                    flag = 1;
                    break;
                }
            }

            //Create grade "PayBay" if it doesnt exists
            if (flag == 0) {
                Login.init(t1).loginAsSuperAdmin("ADD_GRADES");
                GradeManagement.init(t1).addGrade(grade);
            } else {
                t1.info(grade.GradeName + " already exists in the system");
            }

            //Fetch the TCP and MobileROle for grade 'PayBay'
            ResultSet result = MobiquityGUIQueries.fetchTCPIdAndMobileGroupRoleCode(grade.GradeCode, DataFactory.getDefaultWallet().WalletId, "Wallet");
            System.out.println(result.next());

            if (result.next() == false) {
                //If mobile role doesnt exist for grade then create otherwise skip

                GroupRoleManagement.init(t1).addMobileGroupRolesForSpecificGrade(headMer, grade.GradeName, Constants.PAYINST_WALLET_CONST_MOBILE_ROLE, DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName);

                InstrumentTCP tcp = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                        DataFactory.getDomainName(Constants.HEAD_MERCHANT),
                        DataFactory.getCategoryName(Constants.HEAD_MERCHANT),
                        grade.GradeName,
                        "WALLET", DataFactory.getDefaultWallet().WalletName);
                TCPManagement.init(t1).addInstrumentTCP(tcp);
            }


            //Create a Head Merchant for grade 'PayBay'
            User hmer = new User(Constants.HEAD_MERCHANT, grade.GradeName);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(hmer, false);

            ExtentTest t2 = pNode.createNode("TC_ECONET_1130", "To verify that the operator User should able to add multiple merchant under this grade.");

            //Create a Second Merchant for grade 'PayBay'
            User mer = new User(Constants.HEAD_MERCHANT, grade.GradeName);
            ChannelUserManagement.init(t2).createChannelUserDefaultMapping(mer, false);
            t2.pass("Operator User is able to add multiple merchant under PayBay grade");


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }


    }

}
