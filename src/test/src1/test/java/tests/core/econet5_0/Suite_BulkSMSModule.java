package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.pageObjects.BulkMessageUpdate.BulkMessageUpdatePage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Nirupama MK on 11/10/2018.
 */
public class Suite_BulkSMSModule extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SMS_MODULE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0711() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0711", "To verify that the No user other than network admin can Modify SMS Configurations.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SMS_MODULE, FunctionalTag.ECONET_UAT_5_0);

        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("BULKMSG_ALL"));
                t1.fail("Bulk Message Update link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("Bulk Message Update link is not displayed for Channel Admin as Expected", t1);
            }

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("BULKMSG_ALL"));
                t1.fail("Bulk Message Update link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Bulk Message Update link is not displayed for Channel User as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SMS_MODULE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0409() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0409", "To verify that the Valid User should be able to set SMS Configurations by Bulk SMS Module Update.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SMS_MODULE, FunctionalTag.ECONET_UAT_5_0);

        try {

            OperatorUser nwAdm = DataFactory.getOperatorUserWithAccess(Roles.BULK_MESSAGE_UPDATE);
            Login.init(t1).login(nwAdm);
            BulkMessageUpdatePage page = new BulkMessageUpdatePage(t1);
            page.navigateToBulkMessageUpdate();
            page.checkSMS();
            page.clickSubmit();
            page.DownloadMesageFile();


               /* page.selectService("Cash In");
                page.selectChannelType("SMS");
                page.selectLanguage("English");*/


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }
}
