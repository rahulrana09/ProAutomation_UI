package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.util.common.Assertion;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_NewSubscriberGradeFARMER extends TestInit {

    private Grade grade;

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.NEW_SUBSCRIBER_GRADE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_SIT_1131() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1131", "To verify that the Valid user should able to created new grade of subscriber 'FARMER' in system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEW_SUBSCRIBER_GRADE, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.PUSHPALATHA);

        try {
            grade = new Grade(Constants.SUBSCRIBER);
            grade.setGradeCode("FARMER");
            grade.setGradeName("Farmer");
            Login.init(t1).loginAsSuperAdmin("ADD_GRADES");
            GradeManagement.init(t1).addGrade(grade);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();

    }

    @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest tearDown = pNode.createNode("Teardown", "Delete Grade");

        GradeManagement.init(tearDown).deleteGrade(grade, true);

    }

}
