package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Nirupama MK on 29/08/2018.
 */
public class Suite_SubscriberRegistration_02 extends TestInit {
    private User chAddSubs;
    private OperatorUser nwAdmin, billerRegistor, categoryCreator;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "SingleMSISDN_MultipleUserType");

        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
        billerRegistor = DataFactory.getOperatorUsersWithAccess("UTL_CREG").get(0);
        categoryCreator = DataFactory.getOperatorUsersWithAccess("UTL_MCAT").get(0);
        nwAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");

    }

    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0505a1() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505a1", "To verify that the While adding " +
                "Subscriber in system following modifications in last name field should work." +
                "a. Number of characters can be accommodated by the 'Last Name' field to 50 characters.\n");

        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        try {

            User sub4 = new User(Constants.SUBSCRIBER);

            sub4.setLastName("SUBS1" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(sub4);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify Subscriber Last Name should contains 50 Charaters of Alpha Numeric ", t1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0507a1", "To verify that the While " +
                    "adding Subscriber in system following modification in contact person field should work.\n" +
                    "a. Number of characters that can be accommodated by the 'Contact Person' field to 50 characters.");

            sub4 = new User(Constants.SUBSCRIBER);

            sub4.setContactPerson("CONTA01" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t2).login(chAddSubs);

            SubscriberManagement.init(t2)
                    .setIsContactPersonRequired()
                    .addInitiateSubscriber(sub4);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t2), true,
                    "Verify Contact Person Text Field in Add Subscriber page should contains" +
                            " 50 Charaters of Alpha Numeric ", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0505a2() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505a2", "To verify that the While adding " +
                "Channel User in system following modifications in last name field should work." +
                "a. Number of characters can be accommodated by the 'Last Name' field to 50 characters.");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        try {

            User chUser = new User(Constants.WHOLESALER);

            chUser.setLastName("WHS01" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(nwAdmin);
            ChannelUserManagement.init(t1)
                    .initiateChannelUser(chUser);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify Channel User's Last Name can contains 50 Charaters of Alpha Numeric ", t1);


            ExtentTest t2 = pNode.createNode("TC_ECONET_0507a2", "To verify that the While adding" +
                    " Channel User in system following modification in contact person field should work." +
                    "a. Number of characters that can be accommodated by the 'Contact Person' field to 50 characters.");

            chUser = new User(Constants.WHOLESALER);
            chUser.setContactPerson("CONTA01" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t2).login(nwAdmin);

            ChannelUserManagement.init(t2)
                    .setIsContactPersonRequired()
                    .initiateChannelUser(chUser);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t2), true,
                    "Verify Contact Person Text Field in Add channel User page should contains" +
                            " 50 Charaters of Alpha Numeric ", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0505a3() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505a3", "To verify that the While adding " +
                "Merchant in system following modifications in last name field should work." +
                "a. Number of characters can be accommodated by the 'Last Name' field to 50 characters.");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        try {

            User mercchant = new User(Constants.MERCHANT);

            mercchant.setLastName("MERCH" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify Merchant's Last Name should contains 50 Characters of Alpha Numeric ", t1);


            ExtentTest t2 = pNode.createNode("TC_ECONET_0507a3", "To verify that the While adding" +
                    " Merchant in system following modification in contact person field should work." +
                    "a. Number of characters that can be accommodated by the 'Contact Person' field to 50 characters");

            mercchant = new User(Constants.MERCHANT);
            mercchant.setContactPerson("CONTA01" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t2).login(nwAdmin);

            ChannelUserManagement.init(t2)
                    .setIsContactPersonRequired()
                    .initiateChannelUser(mercchant);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t2), true,
                    "Verify Contact Person Text Field while adding the Merchant should contains " +
                            "50 Charaters of Alpha Numeric ", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0505b4() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0505b4", "To verify that the While adding " +
                "Biller in system following modifications in Biller Name field should work." +
                "b. alpha-numeric characters (e.g. comma, dot, apostrophe, space, forward slash, hyphen) should be allowed.");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        try {

            String providerName = DataFactory.getDefaultProvider().ProviderName;

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.setBillerName("BiL01<>|,. *-^" + (DataFactory.getRandomNumber(7)));

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);

            ConfigInput.isConfirm = false;
            ConfigInput.isAssert = false;

            BillerManagement.init(t1).initiateBillerRegistration(biller);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Biller Name should contains Alpha Numeric and Special characters", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0507c1() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0507c1", "To verify that the While adding" +
                " Subscriber in system following modification in contact person field should work." +
                "c. Allow spacing between words ");

        ExtentTest t2 = pNode.createNode("TC_ECONET_0507b1", "To verify that the While adding" +
                " Subscriber in system following modification in contact person field should work." +
                "b. Allow alpha-numeric and special characters (e.g. comma, dot, apostrophe, space, " +
                "forward slash, hyphen)");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            User sub5 = new User(Constants.SUBSCRIBER);

            sub5.setContactPerson("CONTA01" + " .,'/- "
                    + (DataFactory.getRandomString(4))
                    + "  "
                    + (DataFactory.getRandomString(2))
                    + "  "
                    + (DataFactory.getRandomString(2)));

            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1)
                    .setIsContactPersonRequired()
                    .addInitiateSubscriber(sub5);

            if (!Assertion.isErrorInPage(t1) == true) {
                Assertion.logAsPass("Contact Person Text Field in Add Subscriber page is Allowing spacing between words", t1);
                Assertion.logAsPass("Contact Person Text Field in Add Subscriber page is Allowing alpha numeric " +
                        "and special characters between words", t2);

            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0507c2() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0507c2", "To verify that the While adding" +
                "Channel User in system following modification in contact person field should work." +
                "c. Allow spacing between words ");

        ExtentTest t4 = pNode.createNode("TC_ECONET_0507b2", "To verify that the While adding " +
                "Channel User (wholesaler) in system following modification in contact person field should work." +
                "b. Allow alpha-numeric and special characters (e.g. comma, dot, apostrophe, space, " +
                "forward slash, hyphen)");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            ConfigInput.isAssert = false;
            User chUser = new User(Constants.WHOLESALER);

            chUser.setContactPerson("CONT" + " ,.'/-"
                    + (DataFactory.getRandomString(4))
                    + "  "
                    + (DataFactory.getRandomString(2)));

            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t1)
                    .setIsContactPersonRequired()
                    .initiateChannelUser(chUser);

            if (AddChannelUser_pg2.init(t1).isHierarchyPageOpen() == true) {
                Assertion.logAsPass("Contact Person Text Field is Allowing spacing between the words", t1);
                Assertion.logAsPass("Contact Perdon Text Field allows alpha numeric and special characters", t4);

            } else
                Assertion.markAsFailure("Error in the Contact Person Text Field");


            ExtentTest t2 = pNode.createNode("TC_ECONET_0507c3", "To verify that the While adding " +
                    "Merchant in system following modification in contact person field should work." +
                    "c. Allow spacing between words ");

            ExtentTest t3 = pNode.createNode("TC_ECONET_0507b3", "To verify that the While " +
                    "adding Merchant in system following modification in contact person field should work." +
                    "b. Allow alpha-numeric and special characters (e.g. comma, dot, apostrophe, space, " +
                    "forward slash, hyphen)");

            User mercchant = new User(Constants.MERCHANT);

            mercchant.setContactPerson("CONTACT01"
                    + " -.,/'"
                    + (DataFactory.getRandomString(3))
                    + "  "
                    + (DataFactory.getRandomString(2))
                    + "  ");

            Login.init(t2).login(nwAdmin);

            ChannelUserManagement.init(t2)
                    .setIsContactPersonRequired()
                    .initiateChannelUser(mercchant);

            if (AddChannelUser_pg2.init(t1).isHierarchyPageOpen() == true) {
                Assertion.logAsPass("Contact Person Text Field is Allowing spacing between the words", t1);
                Assertion.logAsPass("Contact Perdon Text Field allows alpha numeric and special characters", t4);

            } else
                Assertion.markAsFailure("Error in the Contact Person Text Field");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0507c4() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0507c4", "To verify that the While adding " +
                "Biller in system following modification in contact person field should work." +
                "c. Allow spacing between words ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            String providerName = DataFactory.getDefaultProvider().ProviderName;

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.setContactName("BiLL"
                    + " "
                    + (DataFactory.getRandomNumber(3))
                    + " "
                    + (DataFactory.getRandomString(3)));

            Login.init(pNode).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(pNode).login(billerRegistor);

            ConfigInput.isConfirm = false;
            ConfigInput.isAssert = false;

            BillerManagement.init(t1).initiateBillerRegistration(biller);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Contact Name Text Field should allow Spacing between the words", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1176() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1176", "To verify that the ID number " +
                "should support special characters.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            User sub8 = new User(Constants.SUBSCRIBER);
            sub8.setExternalCode("1234567" + " .,'/-!");

            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1)
                    .addInitiateSubscriber(sub8);

            if (!Assertion.isErrorInPage(t1)) {
                t1.pass("Identification Number is accepting Special Characters");
                Utils.captureScreen(t1);
            }else {
                t1.fail("Identification Number is not accepting Special Characters");
                Utils.captureScreen(t1);
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }
}