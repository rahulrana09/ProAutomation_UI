package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.channelUserManagement.HierarchyBranchMovement;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.AddChannelUser_pg2;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Automation team
 */
public class Suite_ChannelUserManagement_02 extends TestInit {
    private OperatorUser optWithoutRoles, usrCreator;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific to this suite");
        try {
            String[] notRequiredRoles = {"BLK_CHUSR", "PTY_ACU", "PTY_MCU", "BLK_CHUSR", "BULK_AP", "BNK_APR"};

            ArrayList<String> applicableRole = new ArrayList<>(Arrays.asList("CHECK_ALL"));
            WebGroupRole webGroupRole = new WebGroupRole(Constants.CHANNEL_ADMIN, "AUTBCU1", applicableRole, 1);

            Login.init(t1).loginAsSuperAdmin("GRP_ROL");
            GroupRoleManagement.init(t1)
                    .addWebGroupRole(webGroupRole);

            // now make sure that this role doesnt have notRequiredRoles
            GroupRoleManagement.init(t1).removeSpecificRole(webGroupRole, notRequiredRoles);

            // now create a network admin with this specific Role
            optWithoutRoles = new OperatorUser(Constants.CHANNEL_ADMIN);
            optWithoutRoles.WebGroupRole = webGroupRole.RoleName;
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);

            OperatorUserManagement.init(t1).createOptUser(optWithoutRoles, Constants.NETWORK_ADMIN, Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0875() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0875", "To verify that at the time of " +
                "registration every channel user will be assigned a geographical location in terms of " +
                "Zone & Area.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);

            User user = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).initiateChannelUser(user);
            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(t1);

            hierarchy.selectDomain(user.DomainName);
            hierarchy.selectCategory(user.CategoryName);
            hierarchy.clickNext();

            WebDriverWait wait = new WebDriverWait(DriverFactory.getDriver(), 10);
            String alertMsg = wait.until(ExpectedConditions.alertIsPresent()).getText();

            Assertion.verifyMessageContain(alertMsg, "channeluser.validation.hierarchy.blank",
                    "Zone and Geography cannot be blank", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0876() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0876 ", "To verify that the mobiquity provides" +
                " the functionality to capture ID Type and ID Number for a channel user.");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            Login.init(t1).login(usrCreator);

            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(t1);

            pageOne.navAddChannelUser();
            boolean status = Utils.checkElementPresent("confirm2_confirmAddChannelUser_idType", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "ID Type", t1);

            status = Utils.checkElementPresent("confirm2_confirmAddChannelUser_externalCode", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "ID Number", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0879() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0879", "To verify that during modification of " +
                "channel user, Domain of channel user cannot be changed but category, grades and TCP can be changed.");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).initiateChannelUserModification(user);
            CommonChannelUserPage page = CommonChannelUserPage.init(t1);
            page.clickNextUserDetail();

            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyEqual(fl.elementIsEnabled(page.getDomain()), false,
                    "Verify that the Option to Select Domain is Disabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(page.getCategory()), true,
                    "Verify that the Option to Select Category is Enabled", t1, true);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0880() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0880", "To verify that while modifying the" +
                " user’s mWallet associations, the MFS Provider and mWallet type of existing mWallets remains" +
                " non editable, while the associated grade, mobile group role, TCP & status can be modified.");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).initiateChannelUserModification(user);
            CommonChannelUserPage page = CommonChannelUserPage.init(t1);
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();

            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("0"))), false,
                    "Verify that the Option to Select Domain is Disabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("walletTypeID0"))), false,
                    "Verify that the Option to Select Domain is Disabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("grade0"))), true,
                    "Verify that the Option to Select Category is Enabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("walletRoles0"))), true,
                    "Verify that the Option to Select Category is Enabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("tcp0"))), true,
                    "Verify that the Option to Select Category is Enabled", t1, true);

            Assertion.verifyEqual(fl.elementIsEnabled(driver.findElement(By.id("add1_addChannelUser_counterList_0__statusSelected"))),
                    true, "Verify that the Option to Select Category is Enabled", t1, true);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT})
    public void TC_ECONET_0883() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0883", "Hierarchy Branch Movement" +
                "To verify that Transfer of channel user and its hierarchy will be done in the " +
                "same Channel domain & category.");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT);
        try {
            OperatorUser usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);

            User ret = new User(Constants.RETAILER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(ret, false);

            //User ret = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            Login.init(t1).login(usrCreator);

            User owner = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getOwnerIDOfUser(ret.MSISDN)));
            User parent = DataFactory.getUserUsingMsisdn(MobiquityGUIQueries.getMsisdnUsingUserId(MobiquityGUIQueries.getParentIDOfUser(ret.MSISDN)));

            ret.setOwnerUser(owner);
            ret.setParentUser(parent);

            HierarchyBranchMovement.init(t1).
                    initiateChUserHierarchyMovement(ret, owner, parent);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0637() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0637", "To verify that User other than valid user is not able to add initiate channel user successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0638() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0638", "To verify that User other than valid user can not initiate modification of the channel user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithOutAccess("PTY_MCU", t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0639() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0639", "To verify that User other than valid user can not initiate users modification in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithOutAccess("BLK_CHUSR", t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BLK_CHUSR");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0640() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0640", "To verify that User other than valid user can't reject the initiated modification of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithOutAccess("BLK_CHUSR", t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BULK_AP");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0641() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0641", "To verify that User other than valid user can't apporve the initiated modification of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithOutAccess("BLK_CHUSR", t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BLK_CHUSR");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0642() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0642", "To verify that User other than valid user can't reject the initiated registration of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithOutAccess("BULK_AP", t1));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BULK_AP");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0643() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0643", "To verify that User other than valid user is not able to perform Add/Modify/Delete Bank Accounts Approval.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        Login.init(t1).login(user);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_BNK_APR");
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1200() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1200", "To verify that User other than valid user is not able to add initiate channel user successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1207() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1207", "Channel user Management >> Modify channel user : To verify that User other than valid user can not initiate modification of the channel user");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1208() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1208", "Bulk User Registration and modification >> Bulk User Modification: To verify that User other than valid user can not initiate users modification in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BLK_CHUSR");


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1209() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1209", "Bulk User Registration >> Bulk User Modification Approval:To verify that User other than valid user can't reject the initiated modification of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BULK_AP");


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1210() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1210", "Bulk User Registration >> Bulk User Modification Approval: To verify that User other than valid user can't apporve the initiated modification of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BULK_AP");


    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1211() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1211", "Bulk User Registration >> Bulk User registration Approval: To verify that User other than valid user can't reject the initiated registration of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("BULK_ALL", "BULK_BLK_CHUSR");


    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1212() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1212", "To verify that User other than valid user is not able to perform Add/Modify/Delete Bank Accounts Approval.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).login(optWithoutRoles);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_BNK_APR");
    }


}
