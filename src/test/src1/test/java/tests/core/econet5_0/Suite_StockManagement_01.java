package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.pageObjects.stockManagement.ReimbursementStatus_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by navin.pramanik on 2/26/2019.
 */
public class Suite_StockManagement_01 extends TestInit {

    /**
     * Moved cases from UAP CLass related to Econet
     */


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.STOCK_MANAGEMENT, FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0}, priority = 6)
    public void TC0135_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0135_a",
                "Reimbursement Status: To verify that all the information should be displayed on stock Reimbursement Status page.")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.STOCK_MANAGEMENT,
                        FunctionalTag.PVG_P1, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SIT_5_0);


        try {
            OperatorUser remInit = DataFactory.getOperatorUserWithAccess(Roles.STOCK_REIMBURSEMENT_INITIATION);
            OperatorUser remAprov = DataFactory.getOperatorUserWithAccess(Roles.STOCK_REIMBURSEMENT_APPROVAL);

            String amount = "5";
            String txnId = StockManagement.init(t1)
                    .initiateStockReimbursement(remInit, DataFactory.getRandomNumberAsString(5), amount, "test", null);

            Login loginObj = Login.init(t1);
            //Login method is included
            loginObj.login(remAprov);
            txnId = StockManagement.init(t1).approveReimbursement(txnId);
            ReimbursementStatus_Page1 page1 = new ReimbursementStatus_Page1(t1);

            loginObj.login(DataFactory.getOperatorUserWithAccess(Roles.STOCK_REIMBURSEMENT_INITIATION));
            page1.navigateToLink();

            String reqName = page1.getRequesterName(txnId);
            Assertion.assertEqual(reqName, remInit.getFirstNameAndLastName(), "Verify Requester name", t1);

            String txnID = page1.getTransactionID(txnId);
            Assertion.assertEqual(txnID, txnId, "verify Transaction id", t1);

            String txnDate = page1.getTransferDate(txnId);
            Assertion.assertEqual(txnDate, DateAndTime.getCurrentDate("dd/MM/yy"), "verify transaction date", t1);

            String txnAmount = page1.getTransferAmount(txnID);
            Assertion.assertEqual(txnAmount, amount + " " + DataFactory.getDefaultProvider().Currency, "verify transaction amount", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test
    public void  reimbBiller(){

        ExtentTest t1 = pNode.createNode("","");

    }

}
