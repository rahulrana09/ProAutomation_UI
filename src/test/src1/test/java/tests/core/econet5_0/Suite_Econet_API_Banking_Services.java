package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import com.jayway.restassured.response.ValidatableResponse;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.OldTxnOperations;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.TransactionType;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

import static framework.util.JsonPathOperation.delete;
import static framework.util.JsonPathOperation.set;
import static framework.util.globalConstant.Constants.TXN_STATUS_SUCCEEDED;

/**
 * Created by surya.dhal on 9/24/2018.
 */
public class Suite_Econet_API_Banking_Services extends TestInit {
    User subscriber, wholesaler;
    OperatorUser networkAdmin;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        networkAdmin = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
    }


    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_037
     * DESC : To verify that Bank to Wallet (Bank Originated) service is working fine.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_037() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_037", "To verify that Bank to Wallet (Bank Originated) service is working fine.");

        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID);
            String accountNo = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(accountNo);
            ServiceCharge b2W = new ServiceCharge(Services.BANK_INITIATED_BANK_TO_WALLET, wholesaler, wholesaler, defaultBankID, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);

            SfmResponse res = Transactions.init(t1).bankInitiatedBankToWallet(wholesaler, defaultBankID, trustAccNo);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
            Thread.sleep(Constants.TWO_SECONDS);
            String lastTxnID = MobiquityGUIQueries.getLastTransactionIDUsingMSISDN(Services.BANK_INITIATED_BANK_TO_WALLET, wholesaler.MSISDN, "TF");

            res.verifyStatus(TXN_STATUS_SUCCEEDED);
            res.assertMessage("bank.initiated.to.SVA.success", "10", wholesaler.MSISDN, wholesaler.MSISDN, lastTxnID);

            Assertion.verifyEqual(res.TransactionId, lastTxnID, "Verify Transaction ID", t1);
            Assertion.verifyNotEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre And Post Recon.", t1);

            DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(lastTxnID),
                    Constants.TXN_STATUS_SUCCESS, "Verify DB Transaction Status.", pNode);

            DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(lastTxnID).toString(), "10",
                    "Verify Transaction Amount.", pNode);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_038
     * DESC : To verify that Wallet to Bank (Bank Originated) service is working fine.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_038() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_038", "To verify that Wallet to Bank (Bank Originated) service is working fine.");

        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();

            ServiceCharge b2W = new ServiceCharge(Services.WALLET_TO_BANK, wholesaler, wholesaler, null, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(b2W);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);

            SfmResponse res = Transactions.init(t1).walletToBankService(wholesaler, Constants.SVA_AMOUNT, defaultBankID);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
            String lastTxnID = MobiquityGUIQueries.getLastTransactionIDUsingMSISDN(Services.WALLET_TO_BANK, wholesaler.MSISDN, "TS");

            res.verifyStatus(TXN_STATUS_SUCCEEDED);
            res.assertMessage("sva.success.transfernow.message", lastTxnID);

            Thread.sleep(Constants.TWO_SECONDS);
            Assertion.verifyEqual(res.TransactionId, lastTxnID, "Verify Transaction ID", t1);
            Assertion.verifyNotEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre And Post Recon.", t1);

            DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getMobiquityTransactionDBStatus(lastTxnID),
                    Constants.TXN_STATUS_SUCCESS, "Verify DB Transaction Status.", pNode);

            DBAssertion.verifyDBAssertionContain(MobiquityDBAssertionQueries.getRequestedAmount(lastTxnID).toString(), "10",
                    "Verify Transaction Amount.", pNode);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_039
     * DESC : To verify that Mobiquity is able to associate the Bank account of the user with the MSISDN and store in the database.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_039() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_039", "To verify that Mobiquity is able to associate the Bank account of the user with the MSISDN and store in the database.");

        try {

            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            Transactions.init(t1).associateMSISDNWithBank(subscriber, networkAdmin, "SUBSCRIBER");

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre And Post Recon.", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_039
     * DESC : To verify that valid user is able to perform Transaction Enquiry successfully through XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_040() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_040", "To verify that valid user is able to perform Transaction Enquiry successfully through XML.");

        try {

            String transferID = MobiquityGUIQueries.getLastTransactionID(Services.O2C);

            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations operation = new OldTxnOperations(TransactionType.TRANSACTION_ENQUIRY,
                    set("COMMAND.REFERENCEID", transferID));
            t1.info(operation.bodyString);
            TxnResponse response = operation.postOldTxnURLForHSB(t1).assertStatus(Constants.TXN_SUCCESS);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre And Post Recon.", t1);

            Assertion.verifyEqual(response.getResponse().extract().jsonPath().getString("COMMAND.REFERENCEID"),
                    transferID, "Verify Transaction ID", t1);
            Assertion.verifyEqual(response.getMessage(),
                    "Success", "Verify RESPONSE MESSAGE", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_042
     * DESC : To verify that Balance Enquiry (Bank originated) is working fine.
     *
     * @throws Exception
     * @throws MoneyException
     */
    //TODO - Have to Assert The expected message.
    @Test(priority = 5, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_042() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_042", "To verify that Balance Enquiry (Bank originated) is working fine.");

        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID);
            String accountNo = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(accountNo);

            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            Transactions.init(t1)
                    .channelUsrBalanceEnquiryBANK(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID, trustAccNo);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre And Post Recon.", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_069
     * DESC : To verify that Bank to Wallet (Bank Originated) service is not successful if user is barred
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_069_a() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_069_a", "To verify that Bank to Wallet (Bank Originated) service is not successful if user is barred");

        try {
            User barredUser = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();

            ServiceCharge b2W = new ServiceCharge(Services.BANK_INITIATED_BANK_TO_WALLET, barredUser, barredUser, defaultBankID, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(barredUser, null, null);

            SfmResponse res = Transactions.init(t1).bankInitiatedBankToWallet(barredUser, defaultBankID, "323232323");

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(barredUser, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);

            res.verifyStatus("FAILED");
            res.assertMessage("payer.barred.sender", "10", barredUser.MSISDN, barredUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_069
     * DESC : To verify that Bank to Wallet (Bank Originated) service is not successful if user is suspended.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_069_b() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_069_b", "To verify that Bank to Wallet (Bank Originated) service is not successful if user is suspended.");

        try {
            User suspendedUser = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();

            ServiceCharge b2W = new ServiceCharge(Services.BANK_INITIATED_BANK_TO_WALLET, suspendedUser, suspendedUser, defaultBankID, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(suspendedUser, null, null);

            SfmResponse res = Transactions.init(t1).bankInitiatedBankToWallet(suspendedUser, defaultBankID, "323232323");

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(suspendedUser, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);

            res.verifyStatus("FAILED");
            res.assertMessage("888001", "10", suspendedUser.MSISDN, suspendedUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_070
     * DESC : To verify that Bank to Wallet (Bank Originated) service is not successful if Threshold of user (sender/receiver) is reached.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_070() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_070", "To verify that Bank to Wallet (Bank Originated) service is not successful if Threshold of user (sender/receiver) is reached.");
        String tcpID = null;
        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID);
            String accountNo = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(accountNo);

            ServiceCharge b2W = new ServiceCharge(Services.BANK_INITIATED_BANK_TO_WALLET, wholesaler, wholesaler, defaultBankID, null, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);

            tcpID = MobiquityGUIQueries.getUserTCPID(wholesaler, DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultWallet().WalletId);

            //Modify TCP for Amount
            ValidatableResponse response = OldTxnOperations.modifyTCPDetails(set("COMMAND.PROFILE_ID", tcpID),
                    delete("COMMAND.PROFILE_BALANCES"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].SERVICE_TYPE", Services.BANK_INITIATED_BANK_TO_WALLET),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].COUNT", "1"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].TYPES", "PAYEE"));

            t1.info("ModifyTCPDetails Response : " + response.extract().body().asString());
            SfmResponse res = Transactions.init(t1).bankInitiatedBankToWallet(wholesaler, defaultBankID, trustAccNo);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);

            res.verifyStatus("FAILED");
            res.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            ValidatableResponse response = OldTxnOperations.modifyTCPDetails(set("COMMAND.PROFILE_ID", tcpID),
                    delete("COMMAND.PROFILE_BALANCES"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].SERVICE_TYPE", Services.BANK_INITIATED_BANK_TO_WALLET),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].COUNT", Constants.MAX_THRESHOLD),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].TYPES", "PAYEE"));

            t1.info("ModifyTCPDetails Response : " + response.extract().body().asString());
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_073_a
     * DESC : To verify that Wallet to Bank (Bank Originated) service is not successful if user is barred.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_073_a() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_073_a", "To verify that Wallet to Bank (Bank Originated) service is not successful if user is barred.");
        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            User barredUser = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);
            ServiceCharge w2b = new ServiceCharge(Services.BANK_INITIATED_WALLET_TO_BANK_, wholesaler, wholesaler, null, defaultBankID, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(w2b);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(barredUser, null, null);

            SfmResponse res = Transactions.init(t1).bankInitiatedWalletToBank(barredUser, defaultBankID, "3545664657");

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(barredUser, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);

            res.verifyStatus("FAILED");
            res.assertMessage("payer.barred.sender", "10", barredUser.MSISDN, barredUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_073_b
     * DESC : To verify that Wallet to Bank (Bank Originated) service is not successful if user is suspended.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_073_b() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_073_b", "To verify that Wallet to Bank (Bank Originated) service is not successful if user is suspended.");
        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            User suspendedUser = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);
            ServiceCharge w2b = new ServiceCharge(Services.BANK_INITIATED_WALLET_TO_BANK_, wholesaler, wholesaler, null, defaultBankID, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(w2b);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(suspendedUser, null, null);

            SfmResponse res = Transactions.init(t1).bankInitiatedWalletToBank(suspendedUser, defaultBankID, "3545664657");

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(suspendedUser, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);

            res.verifyStatus("FAILED");
            res.assertMessage("888001", "10", suspendedUser.MSISDN, suspendedUser.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    /**
     * TEST : NEGATIVE
     * ID : TC_API_ECONET_074
     * DESC : To verify that Wallet to Bank (Bank Originated) service is not successful if Threshold of user (sender/receiver) is reached.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 11, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_074() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_074", "To verify that Wallet to Bank (Bank Originated) service is not successful if Threshold of user (sender/receiver) is reached.");
        String tcpID = null;
        try {

            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID);
            String accountNo = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(accountNo);

            ServiceCharge w2b = new ServiceCharge(Services.BANK_INITIATED_WALLET_TO_BANK_, wholesaler, wholesaler, null, defaultBankID, null, null);

            TransferRuleManagement.init(t1).configureTransferRule(w2b);

            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);

            tcpID = MobiquityGUIQueries.getUserTCPID(wholesaler, DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultWallet().WalletId);

            //Modify TCP for Amount
            ValidatableResponse response = OldTxnOperations.modifyTCPDetails(set("COMMAND.PROFILE_ID", tcpID),
                    delete("COMMAND.PROFILE_BALANCES"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].SERVICE_TYPE", Services.BANK_INITIATED_BANK_TO_WALLET),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].COUNT", "1"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].TYPES", "PAYEE"));

            t1.info("ModifyTCPDetails Response : " + response.extract().body().asString());

            SfmResponse res = Transactions.init(t1).bankInitiatedWalletToBank(wholesaler, defaultBankID, trustAccNo);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
//TODO - Have to change the Message status code
            res.verifyStatus("FAILED");
            res.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            ValidatableResponse response = OldTxnOperations.modifyTCPDetails(set("COMMAND.PROFILE_ID", tcpID),
                    delete("COMMAND.PROFILE_BALANCES"),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].SERVICE_TYPE", Services.BANK_INITIATED_BANK_TO_WALLET),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].COUNT", Constants.MAX_THRESHOLD),
                    set("COMMAND.THRESHOLD_DETAILS.SERVICES.SERVICE_DETAIL[0].TYPES", "PAYEE"));
            t1.info("ModifyTCPDetails Response : " + response.extract().body().asString());

        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_082
     * DESC : To verify that valid user is able to perform Wallet to Bank service when all mandatory fields are provided in Request XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_082() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_082", "To verify that valid user is able to perform Wallet to Bank service when all mandatory fields are provided in Request XML.");
        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations otpGeneration = new OldTxnOperations(TransactionType.GENERATE_OTP,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.SRVREQTYPE", TransactionType.DEBIT_MONEY_FROM_SVA));

            TxnResponse otpResponse = otpGeneration.postOldTxnURLForHSB(t1);
            String otp = otpResponse.getResponse().extract().jsonPath().getString("COMMAND.OTP");
            t1.info("OTP For the Transaction : " + otp);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.DEBIT_MONEY_FROM_SVA,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.BANKID", defaultBankID),
                    set("COMMAND.OTP", otp)
            );

            TxnResponse response = operation.postOldTxnURLForHSB(t1).assertStatus(Constants.TXN_SUCCESS);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
//TODO - Have to change the Message status code
            response.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_084
     * DESC : To verify that valid user is able to perform Wallet to Bank service when all mandatory fields are provided along with some optional fields in Request XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 12, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_084() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_084",
                "To verify that valid user is able to perform Wallet to Bank service when all mandatory fields are provided along with some optional fields in Request XML.");
        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations otpGeneration = new OldTxnOperations(TransactionType.GENERATE_OTP,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.SRVREQTYPE", TransactionType.DEBIT_MONEY_FROM_SVA));

            TxnResponse otpResponse = otpGeneration.postOldTxnURLForHSB(t1);
            String otp = otpResponse.getResponse().extract().jsonPath().getString("COMMAND.OTP");
            t1.info("OTP For the Transaction : " + otp);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.DEBIT_MONEY_FROM_SVA,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.BANKID", defaultBankID),
                    set("COMMAND.OTP", otp),
                    set("COMMAND.REMARKS", "Wallet to Bank service")
            );

            TxnResponse response = operation.postOldTxnURLForHSB(t1).assertStatus(Constants.TXN_SUCCESS);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
//TODO - Have to change the Message status code
            response.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_081
     * DESC : To verify that valid user is able to perform Bank to Wallet service when all mandatory fields are provided in Request XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_081() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_081", "To verify that valid user is able to perform Bank to Wallet service when all mandatory fields are provided in Request XML.");
        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations operation = new OldTxnOperations(TransactionType.CREDIT_MONEY_TO_WALLET,
                    set("COMMAND.MSISDN2", wholesaler.MSISDN),
                    set("COMMAND.BANKID", defaultBankID)
            );

            TxnResponse response = operation.postOldTxnURLForHSB(t1).assertStatus(Constants.TXN_SUCCESS);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
//TODO - Have to change the Message status code
            response.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_083
     * DESC : To verify that valid user is able to perform Bank to Wallet service when all mandatory fields are provided along with some optional fields in Request XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_083() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_083",
                "To verify that valid user is able to perform Bank to Wallet service when all mandatory fields are provided along with some optional fields in Request XML.");
        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations operation = new OldTxnOperations(TransactionType.CREDIT_MONEY_TO_WALLET,
                    set("COMMAND.MSISDN2", wholesaler.MSISDN),
                    set("COMMAND.BANKID", defaultBankID),
                    set("COMMAND.REMARKS", "Bank to Wallet service")
            );

            TxnResponse response = operation.postOldTxnURLForHSB(t1).assertStatus(Constants.TXN_SUCCESS);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
//TODO - Have to change the Message status code
            response.assertMessage("888001", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_085
     * DESC : To verify that valid user is not able to perform Bank to Wallet service if all mandatory fields are not provided in Request XML. (BANKID provided as blank)
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 14, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_085() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_085", "To verify that valid user is not able to perform Bank to Wallet service if all mandatory fields are not provided in Request XML.");
        try {
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations operation = new OldTxnOperations(TransactionType.CREDIT_MONEY_TO_WALLET,
                    set("COMMAND.MSISDN2", wholesaler.MSISDN),
                    set("COMMAND.BANKID", Constants.BLANK_CONSTANT)
            );

            TxnResponse response = operation.postOldTxnURLForHSB(t1);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payee Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
            response.assertMessage("999113", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_086
     * DESC : To verify that valid user is not able to perform Wallet to Bank service if all mandatory fields are not provided in Request XML.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 15, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_API_ECONET_086() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_086", "To verify that valid user is not able to perform Wallet to Bank service if all mandatory fields are not provided in Request XML.");
        try {
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            OldTxnOperations otpGeneration = new OldTxnOperations(TransactionType.GENERATE_OTP,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.SRVREQTYPE", TransactionType.DEBIT_MONEY_FROM_SVA));

            TxnResponse otpResponse = otpGeneration.postOldTxnURLForHSB(t1);
            String otp = otpResponse.getResponse().extract().jsonPath().getString("COMMAND.OTP");
            t1.info("OTP For the Transaction : " + otp);

            OldTxnOperations operation = new OldTxnOperations(TransactionType.DEBIT_MONEY_FROM_SVA,
                    set("COMMAND.MSISDN", wholesaler.MSISDN),
                    set("COMMAND.BANKID", Constants.BLANK_CONSTANT),
                    set("COMMAND.OTP", otp));

            TxnResponse response = operation.postOldTxnURLForHSB(t1);
            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
            response.assertMessage("999113", "10", wholesaler.MSISDN, wholesaler.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_API_ECONET_089
     * DESC : To verify that valid user is not able to perform Transaction Enquiry if entered reference ID is invalid.
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(priority = 16, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0})
    public void TC_API_ECONET_089() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_089", "To verify that valid user is not able to perform Transaction Enquiry if entered reference ID is invalid.");

        try {

            String transferID = String.valueOf(DataFactory.generateRandomNumber(8));

            OldTxnOperations operation = new OldTxnOperations(TransactionType.TRANSACTION_ENQUIRY,
                    set("COMMAND.REFERENCEID", transferID));
            t1.info(operation.bodyString);

            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();

            TxnResponse response = operation.postOldTxnURLForHSB(t1);

            DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(wholesaler, null, null);
            DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();

            Assertion.verifyEqual(DBAssertion.prePayeeBal, DBAssertion.postPayeeBal, "Verify Payer Pre and Post balance.", t1);
            Assertion.verifyEqual(DBAssertion.preRecon, DBAssertion.postRecon, "Verify Pre and Post Recon balance.", t1);
            response.assertMessage("TB002");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    @Test(priority = 2, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void TC_ECONET_966() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_966", "To verify that the Subscriber should be able to perform Bank to Wallet Service.");

        try {
            String defaultBankID = DataFactory.getDefaultBankIdForDefaultProvider();
            Map<String, String> AccountNum = MobiquityGUIQueries
                    .dbGetAccountDetails(wholesaler, DataFactory.getDefaultProvider().ProviderId, defaultBankID);
            String accountNo = AccountNum.get("ACCOUNT_NO");
            //decrypt the account no.
            DesEncryptor d = new DesEncryptor();
            String trustAccNo = d.decrypt(accountNo);

            ServiceCharge b2W = new ServiceCharge(Services.BANK_TO_WALLET, wholesaler, wholesaler, defaultBankID, null, null, null);
            TransferRuleManagement.init(t1).configureTransferRule(b2W);

            SfmResponse res = Transactions.init(t1).bankToWalletService(wholesaler, "5", defaultBankID, trustAccNo, false);
            res.verifyStatus(TXN_STATUS_SUCCEEDED);

            Thread.sleep(Constants.TWO_SECONDS);

            String lastTxnID = MobiquityGUIQueries.getLastTransactionIDUsingMSISDN(Services.BANK_TO_WALLET, wholesaler.MSISDN);
            res.assertMessage("bank.to.SVA.success", lastTxnID);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0}, enabled = true)
    public void TC_ECONET_1208() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1208", "To verify that the Bank API should be able to initiate Automated O2C flow for Stanbic Bank..");

        //TODO - Have to Change Bank Name as per the test case after creating Bank
        try {
            String bankID = DataFactory.getBankId("USDBNK");
            //String bankWalletNo = MobiquityGUIQueries.getWalletNumber(bankID);

            ServiceCharge autoO2C = new ServiceCharge(Services.AUTO_O2C, networkAdmin, wholesaler, null,
                    null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(autoO2C);

            Transactions.init(t1).automaticO2C(wholesaler, "5", bankID, DataFactory.getRandomNumberAsString(4));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void TC_ECONET_1210() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1210", "To verify that the Bank API should be able to initiate Automated O2C flow for CBZ Bank.");

        //TODO - Have to Change Bank Name as per the test case after creating Bank
        try {
            String bankID = DataFactory.getBankId("USDSBI");
            //String bankWalletNo = MobiquityGUIQueries.getWalletNumber(bankID);

            ServiceCharge autoO2C = new ServiceCharge(Services.AUTO_O2C, networkAdmin, wholesaler, null,
                    null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(autoO2C);

            Transactions.init(t1).automaticO2C(wholesaler, "5", bankID, DataFactory.getRandomNumberAsString(4));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SMOKE, FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void TC_ECONET_1212() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1212", "To verify that the Bank API should be able to initiate Automated O2C flow for Steward Bank.");

        //TODO - Have to Change Bank Name as per the test case after creating Bank
        try {
            String bankID = DataFactory.getBankId("USDLIQ");
            //String bankWalletNo = MobiquityGUIQueries.getWalletNumber(bankID);

            ServiceCharge autoO2C = new ServiceCharge(Services.AUTO_O2C, networkAdmin, wholesaler, null,
                    null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(autoO2C);

            Transactions.init(t1).automaticO2C(wholesaler, "5", bankID, DataFactory.getRandomNumberAsString(4));

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}
