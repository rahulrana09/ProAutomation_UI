package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.entity.Category;
import framework.entity.Domain;
import framework.entity.OperatorUser;
import framework.features.categoryManagement.CategoryManagement;
import framework.features.common.Login;
import framework.features.domainCategoryManagement.DomainManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.CategoryManagement.AddCategory_Page1;
import framework.pageObjects.CategoryManagement.CategoryModification_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/***
 * Company Name     : Comviva Technologies Ltd.
 * Application Name : Mobiquity 5.0
 * Objective        : Category Management
 * Author Name      : Saraswathi Annamalai
 * Created Date     : 22/08/2018
 */

public class Suite_CategoryManagement extends TestInit {

    private Domain autDomain, dom;
    private OperatorUser channeladmin, networkadmin;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        ExtentTest t1 = pNode.createNode("SetUp", "Creation Of Domain");
        channeladmin = DataFactory.getOperatorUserWithAccess("ADD_CAT");
        networkadmin = DataFactory.getOperatorUserWithAccess("DOM_ADD");

        autDomain = new Domain(Constants.AUT_DOMAIN, Constants.AUT_DOMAIN);
        String domainName = MobiquityGUIQueries.executeQueryAndReturnResult("select DOMAIN_NAME from MTX_DOMAINS where DOMAIN_CODE='" + Constants.AUT_DOMAIN + "'", "DOMAIN_NAME");

        //TODO Uncomment below code before Push
        dom = new Domain();
        Login.init(t1).login(networkadmin);
        DomainManagement.init(t1).addDomain(dom);

        if (domainName == null) {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_DOMAIN);
            DomainManagement.init(t1).addDomain(autDomain);
        } else {
            autDomain.setDomainName(domainName);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0043() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0043", "To verify that only netamdin will be able to approve/reject the initiated category");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);
        try {
            //
            Category cat = new Category(Constants.WHOLESALER);

            CategoryManagement catMgmtObj = CategoryManagement.init(t1);

            catMgmtObj.checkForCategoryAlreadyInitiatedError(true);

            Login.init(t1).login(channeladmin);
            catMgmtObj.addCategory(cat, dom.domainName);

            //Reject Category
            Login.init(t1).login(networkadmin);
            catMgmtObj.
                    rejectCategory(networkadmin, dom.domainName, cat.getCategoryName(), cat.getCategoryName());
            //Create Category
            Login.init(t1).login(channeladmin);
            catMgmtObj.
                    addCategory(cat, dom.domainName);
            //Approve Category
            Login.init(t1).login(networkadmin);

            catMgmtObj.
                    approveCategory(dom.domainName, cat.getCategoryName());


        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0044() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0044", "To verify that Users other than channel admin will not be able to add initiate category in the system");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            Login.init(t1).login(channeladmin);

            AddCategory_Page1 page = new AddCategory_Page1(t1);

            try {

                page.navigateCategoryManagementPage();
                t1.pass("Channel Admin can see add category");
            } catch (Exception e) {
                t1.fail("Channel Admin cannot see add category");
            }
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

            Login.init(t1).login(networkadmin);

            try {
                driver.findElement(By.id("CATADD_ALL"));
                t1.fail("Network Admin can see add category");
            } catch (Exception e) {
                t1.pass("Network Admin cannot see add category");
            }
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0045() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0045", "To verify that Channel admin will be able to add multiple categories under owner categories");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        try {
            //Add Domain
            Domain dom = new Domain();
            Login.init(t1).login(networkadmin);

            DomainManagement.init(t1).addDomain( dom);

            CategoryManagement catMgmtObj = CategoryManagement.init(t1);

            catMgmtObj.checkForCategoryAlreadyInitiatedError(true);

            Category cat = new Category(Constants.WHOLESALER);

            Login.init(t1).login(channeladmin);
            catMgmtObj.addAndApproveCategory(cat,dom.domainName);
            //catMgmtObj.addCategory( cat, dom.domainName);

            //Approve Category
            //Login.init(t1).login(networkadmin);
            //catMgmtObj.approveCategory(dom.domainName,  cat.getCategoryName());

            //Create Second Category with Parent Category Name
            Category cat1 = new Category(Constants.WHOLESALER);
            cat1.setParentCategory(cat.getCategoryName());
            Login.init(t1).login(channeladmin);


            catMgmtObj.addCategory(cat1, dom.domainName);

            Login.init(t1).login(networkadmin);
            catMgmtObj.approveCategory( dom.domainName, cat1.getCategoryName());

            //Create Third Category with Same Parent Category Name
            Login.init(t1).login(channeladmin);
            Category cat2 = new Category(Constants.WHOLESALER);

            cat2.setParentCategory(cat.getCategoryName());
            catMgmtObj.addCategory(cat2, dom.domainName);

            //Modifying the Category since we are using same Parent Category
            //catMgmtObj.modifyCategory(channeladmin, cat2.getCategoryCode());
            Login.init(t1).login(networkadmin);
            catMgmtObj.approveCategory(dom.domainName, cat2.getCategoryName());
            t1.pass("Multiple Categories Can Be Created Under Owner Categories");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0046() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0046", "To verify that the Proper error message should get displayed on web during New category addition initiation if previous category already in initiated state for the same hierarchy level");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            Login.init(t1).login(channeladmin);
            Category cat = new Category(Constants.WHOLESALER);

            CategoryManagement.init(t1).addCategory(cat, autDomain.domainName);

            Login.init(t1).login(channeladmin);
            AddCategory_Page1 page = new AddCategory_Page1(t1);
            page.navigateCategoryManagementPage();
            Assertion.verifyErrorMessageContain("category.AI.exists", "Check Already Initiated Error Present", t1);

            Login.init(t1).login(networkadmin);
            CategoryManagement.init(t1).rejectCategory(networkadmin, autDomain.domainName, cat.getCategoryName(), cat.getCategoryName());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0047() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0047", "To verify that the No option should be available on web to delete the category in system");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            Login.init(t1).login(channeladmin);

            AddCategory_Page1 page = new AddCategory_Page1(t1);

            try {
                page.navigateCategoryManagementPage();
                driver.findElement(By.xpath("//a[contains(text(),'Delete Category')]"));
            } catch (Exception e) {
                t1.pass("Channel Admin cannot delete category");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0760() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0760", "To verify that At a time system will allow only one category creation across all domains");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            //Create Category
            Login.init(t1).login(channeladmin);
            Category cat = new Category(Constants.WHOLESALER);

            CategoryManagement.init(t1).addCategory(cat, autDomain.domainName);

            //Create Domain to verify if category can be created when category is in initiated state

            Domain dom1 = new Domain();
            Login.init(t1).login(networkadmin);
            DomainManagement.init(t1).addDomain(dom1);

            Login.init(t1).login(channeladmin);
            AddCategory_Page1 page = new AddCategory_Page1(t1);
            page.navigateCategoryManagementPage();
            Assertion.verifyErrorMessageContain("Category.Addition.Previous.Category.Aleready.Initiated.State", "Two Category Added At Same Time", t1);


            Login.init(t1).login(networkadmin);
            CategoryManagement.init(t1).approveCategory(autDomain.domainName, cat.getCategoryName());


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0889() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0889", "To verify that , network admin will be able  to reject a category creation request");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {
            //Create Category
            Login.init(t1).login(channeladmin);
            Category cat = new Category(Constants.WHOLESALER);

            CategoryManagement.init(t1).addCategory(cat, autDomain.domainName);

            Login.init(t1).login(networkadmin);
            CategoryManagement.init(t1).rejectCategory(networkadmin, autDomain.domainName, cat.getCategoryName(), cat.getCategoryName());

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0042() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0042", "To verify that channel admin will be able to add initiate category and the Category code & name must be unique in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            CategoryManagement catMgmtObj = CategoryManagement.init(t1);
            String catCode = Constants.WHOLESALER;

            catMgmtObj.checkForCategoryAlreadyInitiatedError(true);

            Category cat = new Category();
            cat.setCategoryCode(catCode);
            cat.setCategoryName(DataFactory.getCategoryName(catCode));

            Login.init(t1).login(channeladmin);

            startNegativeTestWithoutConfirm();

            catMgmtObj.addCategory(cat, autDomain.domainName);

            Assertion.verifyErrorMessageContain("category.catname.catname",
                    "Category name is already existed", t1);

            Assertion.verifyErrorMessageContain("category.code.already.exist",
                    "Category code is already existed", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0890() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0890", "To verify that Once a category is added & approved, the same cannot be modified or deleted");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CATEGORY_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        try {


            Login.init(t1).login(channeladmin);
            Category cat = new Category(Constants.WHOLESALER);

            CategoryManagement.init(t1).addCategory(cat, autDomain.domainName);

            Login.init(t1).login(networkadmin);
            CategoryManagement.init(t1).approveCategory(autDomain.domainName, cat.getCategoryName());

            //Checking Whether approved category can be modified
            Login.init(t1).login(channeladmin);

            CategoryModification_Page1 page = new CategoryModification_Page1(t1);
            page.navCategoryManagementLink();

            try {
                driver.findElement(By.xpath("//tr[td[contains(text(),'" + cat.getCategoryCode() + "')]]/td/a")).click();
            } catch (Exception e) {
                t1.pass("Approved Category Cannot Be Modified");
                t1.pass("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}


