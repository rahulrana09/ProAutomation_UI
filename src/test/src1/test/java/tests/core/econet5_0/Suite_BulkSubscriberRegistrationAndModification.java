package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.features.bulkSubscriberRegistration.BulkSubscriberRegistration;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Nirupama MK on 12/09/2018.
 */

public class Suite_BulkSubscriberRegistrationAndModification extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SUBSCRIBER_REGISTRATION, FunctionalTag.ECONET_UAT_5_0})
    public void bulkSubsUploadTest() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0307", "To verify that the Proper error " +
                "message should get displayed on web logs if user enter any invalid details in uploaded file.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_SUBSCRIBER_REGISTRATION, FunctionalTag.ECONET_UAT_5_0);

        try {
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE", Constants.NETWORK_ADMIN);

            //Error message is displayed while uploading invalid details
            String filename = BulkSubscriberRegistration.init(t1).
                    generateBulkSubsRegnModnCsvFile(0, Constants.BULK_USER_STATUS_ADD,
                            Constants.BULK_USER_STATUS_ADD);

            Login.init(t1).login(netAdmin);
            ConfigInput.isAssert = false;

            BulkSubscriberRegistration.init(t1)
                    .initiateBulkSubscriberUpload(filename);

            Assertion.verifyErrorMessageContain("pinmanagement.upload.file.error", "File is empty", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

}