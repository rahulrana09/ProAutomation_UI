package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.util.globalConstant.Constants.TXN_STATUS_SUCCEEDED;

public class Suite_MerchantRefund extends TestInit {
    private User usrMerchant;
    private User usrSubs;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Merchant Refund");
        try {

            usrMerchant = DataFactory.getChannelUserWithCategory(Constants.HEAD_MERCHANT);
            usrSubs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            TransactionManagement.init(s1).makeSureLeafUserHasBalance(usrSubs);

            ServiceCharge merchPay = new ServiceCharge(Services.MERCHANT_PAY, usrSubs, usrMerchant, null, null, null, null);
            ServiceCharge merchRefund = new ServiceCharge(Services.MERCHANT_REFUND, usrMerchant, usrSubs, null, null, null, null);
            TransferRuleManagement.init(s1).configureTransferRule(merchPay);
            TransferRuleManagement.init(s1).configureTransferRule(merchRefund);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.v5_0})
    public void MerchantRefund() throws Exception {

        ExtentTest t1 = pNode.createNode("790",
                "To verify that merchant is able to send in a request for refund to a subscriber " +
                        "for a merchant to subscriber transaction only.");

        ExtentTest t2 = pNode.createNode("TC_API_ECONET_060",
                "To verify that original transaction ID should be a Merchant Payment Transaction," +
                        " with the merchant requesting the transaction as Payee.");

        try {

            UsrBalance BalMerPay = MobiquityGUIQueries.getUserBalance(usrMerchant, null, null);
            BigDecimal merPreBalMerPay = BalMerPay.Balance;

            UsrBalance preSubBalance = MobiquityGUIQueries.getUserBalance(usrSubs, null, null);
            BigDecimal preSubBal = preSubBalance.Balance;

            //sender sub
            SfmResponse response = Transactions.init(t2)
                    .initiateMerchantPayment(usrMerchant, usrSubs, Constants.minimun_AMOUNT)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            String txnId = response.TransactionId;

            Transactions.init(t2).resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            UsrBalance postBalMerPay = MobiquityGUIQueries.getUserBalance(usrMerchant, null, null);
            BigDecimal merPostBalMerPay = postBalMerPay.Balance;

            ChannelUserManagement.init(t1)
                    .checkIfUserBalanceisCredited(merPreBalMerPay, merPostBalMerPay,
                            false, "Merchant");

            UsrBalance postSubBalance = MobiquityGUIQueries.getUserBalance(usrSubs, null, null);
            BigDecimal postSubBal = postSubBalance.Balance;

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preSubBal, postSubBal,
                    true, "Subscriber");

            response = Transactions.init(t2)
                    .initiateTransactionRefundByChannelUser(usrMerchant,
                            Constants.minimun_AMOUNT, txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND).
                            verifyStatus(TXN_STATUS_SUCCEEDED);

            Transactions.init(t1)
                    .resumeMerchantPayment(response, Services.RESUME_MERCHANT_REFUND)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            UsrBalance balMerRefund = MobiquityGUIQueries
                    .getUserBalance(usrMerchant, null, null);
            BigDecimal merPostBalMerRefund = balMerRefund.Balance;

            ChannelUserManagement.init(t1)
                    .checkIfUserBalanceisCredited(merPostBalMerPay, merPostBalMerRefund,
                            true, "Merchant");

            UsrBalance postRefundSubBal = MobiquityGUIQueries.getUserBalance(usrSubs, null, null);
            BigDecimal postSubBalRefund = postRefundSubBal.Balance;

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(postSubBal, postSubBalRefund,
                    false, "Subscriber");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 2, groups = {FunctionalTag.v5_0})
    public void TC_API_ECONET_059() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_059",
                "To verify that merchant is required to provide the original transaction ID, " +
                        "along with remarks if any for the merchant refund transaction.");

        try {

            //sender sub
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayment(usrMerchant, usrSubs, Constants.minimun_AMOUNT)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //Proper error msg is thrown when invalid txnID is given
            String txnId = "";

            Transactions.init(t1).resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            Transactions.init(t1)
                    .startNegativeTest()
                    .initiateTransactionRefundByChannelUser(usrMerchant, Constants.minimun_AMOUNT,
                            txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND)
                    .verifyMessage("api.merchantRefund.invalid.txn");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 3, groups = {FunctionalTag.v5_0})
    public void TC_API_ECONET_061() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_061",
                "To verify that merchant is not able to reverse the " +
                        "transactions of any other merchant.");

        try {

            /*User newMerchant = new User(Constants.MERCHANT);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(newMerchant,false);
*/
            User newMerchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
            //sender sub
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayment(usrMerchant, usrSubs, Constants.minimun_AMOUNT)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //Proper error msg is thrown when invalid txnID is given
            String txnId = response.TransactionId;

            Transactions.init(t1).resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //Merchant-Refund is not posible for the merchant not involved in merchant pay
            Transactions.init(t1)
                    .startNegativeTest()
                    .initiateTransactionRefundByChannelUser(newMerchant, Constants.minimun_AMOUNT,
                            txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND)
                    .verifyMessage("api.merchantRefund.invalid.mer");


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }

    }

    @Test(priority = 4, groups = {FunctionalTag.v5_0})
    public void TC_API_ECONET_063() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_063",
                "To verify that service charge and tax collected for the " +
                        "original transaction shall not be reversed.");

        try {

            //sender sub
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayment(usrMerchant, usrSubs, Constants.minimun_AMOUNT)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            String txnId = response.TransactionId;

            Transactions.init(t1).resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //Service charge
            BigDecimal preBalanceOfIND03 = MobiquityGUIQueries
                    .fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03");
            t1.info("Pre-Balance of IND03(Service charge) is: " + preBalanceOfIND03);

            //Commission
            BigDecimal preBalanceOfIND03B = MobiquityGUIQueries
                    .fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("Pre-Balance of IND03B(Commission charge) is: " + preBalanceOfIND03B);

            //initiate merchant Refund
            response = Transactions.init(t1)
                    .initiateTransactionRefundByChannelUser(usrMerchant,
                            Constants.minimun_AMOUNT, txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND).
                            verifyStatus(TXN_STATUS_SUCCEEDED);

            //resume merchant refund
            Transactions.init(t1)
                    .resumeMerchantPayment(response, Services.RESUME_MERCHANT_REFUND)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            BigDecimal postBalanceOfIND03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal("101", "IND03");
            t1.info("Post-Balance of IND03(Service charge) is: " + postBalanceOfIND03);

            BigDecimal postBalanceOfIND03B = MobiquityGUIQueries
                    .fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "IND03B");
            t1.info("Post-Balance of IND03B(Commission) is: " + preBalanceOfIND03B);

            // verify that service charge is not reversed
            if (postBalanceOfIND03.intValue() == preBalanceOfIND03.intValue()) ;
            Assertion.logAsPass("Expected: " + (preBalanceOfIND03.intValue()) + " Actual: " + postBalanceOfIND03.intValue(), t1);

            // verify that Commission is not reversed
            if (postBalanceOfIND03B.intValue() == preBalanceOfIND03B.intValue()) ;
            Assertion.logAsPass("Expected: " + (preBalanceOfIND03B.intValue()) + " Actual: " + postBalanceOfIND03B.intValue(), t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.v5_0})
    public void TC_API_ECONET_064() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_API_ECONET_064",
                "To verify that refund transaction will fail if  merchant’s " +
                        "wallet is having insufficient balance.");

        ExtentTest t2 = pNode.createNode("TC_API_ECONET_062",
                "To verify that it will be validated if the requested refund " +
                        "amount is less than or equal to the original transaction amount " +
                        "minus the sum of all the refund transactions against the original" +
                        " transaction.");

        try {

            //sender sub
            SfmResponse response = Transactions.init(t1)
                    .initiateMerchantPayment(usrMerchant, usrSubs, "20")
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            String txnId = response.TransactionId;

            Transactions.init(t1).resumeMerchantPayment(response, Services.RESUME_MERCHANT_PAY)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //Merchant Refund is possible for the Amount less than the Original transaction value
            response = Transactions.init(t2)
                    .initiateTransactionRefundByChannelUser(usrMerchant, "10",
                            txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND);

            Transactions.init(t2)
                    .resumeMerchantPayment(response, Services.RESUME_MERCHANT_REFUND)
                    .verifyStatus(TXN_STATUS_SUCCEEDED);

            //proper error msg is displayed when merchant has insufficient balance
            Transactions.init(t1)
                    .startNegativeTest()
                    .initiateTransactionRefundByChannelUser(usrMerchant, Constants.MAX_TRANSACTION_AMT,
                            txnId, Constants.NORMAL_WALLET, Services.MERCHANT_REFUND)
                    .verifyMessage("api.merchantRefund.invalid.insufficientAmt");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

}
