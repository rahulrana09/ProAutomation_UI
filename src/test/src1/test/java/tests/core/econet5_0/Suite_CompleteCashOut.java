package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_CompleteCashOut extends TestInit {

    private static User payer, payee;
    private ServiceCharge completeCashOut;


    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup", "Setup Specific for this suite");
        try {

            payer = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            payee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(payer, new BigDecimal(50));


            completeCashOut = new ServiceCharge(Services.COMPLETECASHOUT, payer, payee,
                    null, null, null, null);
            TransferRuleManagement.init(t1)
                    .configureTransferRule(completeCashOut);


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    public void checkPre(User payer, User payee) {
        DBAssertion.prePayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.prePayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.preRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    public void checkPost(User payer, User payee) {
        DBAssertion.postPayerBal = MobiquityDBAssertionQueries.getUserBalance(payer, null, null);
        DBAssertion.postPayeeBal = MobiquityDBAssertionQueries.getUserBalance(payee, null, null);
        DBAssertion.postRecon = MobiquityDBAssertionQueries.getReconBalance();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMPLETECASHOUT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0384() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0384",
                "To verify that Channel user is able initiate complete Cash-out for subscribers.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMPLETECASHOUT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);

        try {

            checkPre(payer, payee);

            BigDecimal usrBal = MobiquityGUIQueries.getUserBalance(payer, Constants.NORMAL_WALLET, null).Balance;

            SfmResponse response = Transactions.init(t1).performCompleteCashOut("withdrawer", payer, payee);

            response.verifyMessage("complete.cashout.success",
                    MobiquityGUIQueries.getCurrencyCode(DataFactory.getDefaultProvider().ProviderId), String.valueOf(usrBal.intValue()),
                    payer.MSISDN, payee.MSISDN, response.TransactionId);

            checkPost(payer, payee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMPLETECASHOUT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0823() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0823",
                "To verify that subscriber is able approve complete cashout initiation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMPLETECASHOUT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            checkPre(payer, payee);
            SfmResponse res = Transactions.init(t1).performCompleteCashOut("transactor", payer, payee);
            String serReqId = res.ServiceRequestId;


            SfmResponse response = Transactions.init(t1).approveCompleteCashOut(Services.COMPLETECASHOUT, payer, serReqId);
            t1.pass(response.Message);

            checkPost(payer, payee);

            DBAssertion.verifyUserAndReconBalance(DBAssertion.prePayerBal, DBAssertion.postPayerBal, DBAssertion.postPayeeBal, DBAssertion.prePayeeBal, DBAssertion.preRecon, DBAssertion.postRecon, t1);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }


}
