package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.loyaltyManagement.LMS;
import framework.pageObjects.Promotion.AddPromotion_page1;
import framework.pageObjects.Promotion.AddPromotion_page2;
import framework.pageObjects.Promotion.Reconciliation;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

public class Suite_RewardsAndPromotionManagement extends TestInit {
    private String promoName, fromDate, toDate;
    private OperatorUser nwAdmin;
    private SuperAdmin supAdmin;
    private User chnUser;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        promoName = "LMS" + DataFactory.getRandomNumber(5);

        nwAdmin = DataFactory.getOperatorUserWithAccess("C_ROLE");
        supAdmin = DataFactory.getSuperAdminWithAccess("AC_ROLE");

        String dateFormat = "dd-MM-yyyy";
        String time = " 02:30:00 PM";
        fromDate = new DateAndTime().getDate(dateFormat, 3) + time;
        toDate = new DateAndTime().getDate(dateFormat, 21) + time;

    }

    @Test(priority = 1, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0370() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0370", "To verify that the All list of wallets " +
                "of user should get displayed on web during promotion creation.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        try {
            ArrayList<String> webWallet = new ArrayList<String>();
            List<String> walletList = MobiquityGUIQueries.getAllWallet();

            AddPromotion_page1 addPromotion = new AddPromotion_page1(t1);
            AddPromotion_page2 addPromotion2 = new AddPromotion_page2(t1);

            Login.init(t1).login(nwAdmin);

            addPromotion.NavigateNA();
            addPromotion.sendname(promoName);
            addPromotion.sendFromdate(fromDate);
            addPromotion.sendTodate(toDate);
            addPromotion.selectServiceType(Services.C2C);
            addPromotion.clickOnNextButton();

            List<WebElement> webWalletOptions = addPromotion2.clickPayeeWB();

            for (WebElement ele : webWalletOptions)
                webWallet.add(ele.getText());

            Assertion.verifyListContains(webWallet, walletList, "Wallet List Verified Successfully.", t1);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1252() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1252", "To verify that network admin is able " +
                "to view promotion successfully.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(nwAdmin);
            LMS.init(t1).createPromotion(promoName, Constants.WHS,
                    Constants.MERCHANT, Services.C2C);

            Login.init(t1).loginAsSuperAdmin("AC_ROLE");
            LMS.init(t1).approveLMS(promoName);

            LMS.init(t1).ViewLMS(promoName, fromDate, toDate);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0673() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0673", "To verify that the System should display " +
                "reconciliation screen with all the configured details.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);
        try {

            String RewardAccBalance = null;
            DBAssertion.prePayeeBal = MobiquityDBAssertionQueries
                    .getWalletBalanceByWalletNumber(GlobalData.defaultProvider.ProviderId + Constants.WALLET_108);

            RewardAccBalance = "" + DBAssertion.prePayeeBal.divide(AppConfig.currencyFactor);

            Login.init(t1).login(nwAdmin);

            Reconciliation.init(t1)
                    .navReconciliation()
                    .selectProvider(DataFactory.getDefaultProvider().ProviderId)
                    .clickSubmit();

            Assertion.verifyEqual(RewardAccBalance, reconciliationRewardAccBalance(),
                    "Verify Reconciliation Gap", t1);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1270() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1270", "To verify that User other than Network admin" +
                " will not be able to Create/Modify/view Promotions in the system.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);
        try {

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("PROMOTION_ALL"));
                t1.fail("Promotion link is displayed for Wholesaler");

            } catch (Exception e) {
                Assertion.logAsPass("Promotion link is not displayed for Channel User as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    private String reconciliationRewardAccBalance() {
        return driver.findElement(By.xpath("//td[contains(text(),'e)Reward Account :')]/..//td[2]")).getText();
    }

    @Test(priority = 5, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1275() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1275", "To verify that User other than Network admin will not be able to Create Promotions in the system.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        try {
            User chnUser = DataFactory.getChannelUserWithOutAccess(Roles.CREATE_PROMOTION, t1);
            Login.init(t1).login(chnUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("PROMOTION_ALL", "PROMOTION_C_PRO");

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1275_a() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1275_a", "To verify that User other than Network admin will not be able to modify Promotions in the system.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        try {
            User chnUser = DataFactory.getChannelUserWithOutAccess(Roles.MODIFY_PROMOTION, t1);
            Login.init(t1).login(chnUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("PROMOTION_ALL", "PROMOTION_MC_PRO");

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.PROMOTION, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1275_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1275_b", "To verify that User other than Network admin will not be able to view Promotions in the system.");

        t1.assignCategory(FunctionalTag.PROMOTION, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        try {
            User chnUser = DataFactory.getChannelUserWithOutAccess(Roles.VIEW_PROMOTION, t1);
            Login.init(t1).login(chnUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("PROMOTION_ALL", "PROMOTION_VIEW_PRO");

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }


}

