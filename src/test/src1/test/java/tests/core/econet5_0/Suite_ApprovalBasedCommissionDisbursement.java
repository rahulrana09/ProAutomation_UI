package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.comviva.common.DesEncryptor;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.UsrBalance;
import framework.dataEntity.Wallet;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.commissionDisbursement.CommissionDisbursement;
import framework.features.common.Login;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.commissionDisbursement.Commission_Disbursement_Page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Map;

public class Suite_ApprovalBasedCommissionDisbursement extends TestInit {
    private Wallet commWallet;
    private CurrencyProvider defProvider;
    private String defaultBankId;
    private OperatorUser optCD, optCD1, cce;
    private User chUser, subs;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            optCD = DataFactory.getOperatorUserWithAccess("COMMDIS_INITIATE");
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            commWallet = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
            defProvider = DataFactory.getDefaultProvider();
            defaultBankId = DataFactory.getDefaultBankIdForDefaultProvider();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0360_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0360_a",
                "To verify that the netadmin should able to perform Commission Disbursement for Wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            BigDecimal amount = new BigDecimal(3);
            Login.init(t1).login(optCD);

            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(chUser, commWallet.WalletId);

            UsrBalance channelUsrBalance = MobiquityGUIQueries.getUserBalance(chUser, commWallet.WalletId, null);
            BigDecimal preBalance = channelUsrBalance.Balance;

            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(optCD);

            CommissionDisbursement.init(t1)
                    .getUserCommissionBalance(chUser, defProvider.ProviderName);

            // download the csv file and update the amount with new Amount 'a'
            new Commission_Disbursement_Page2(t1)
                    .selectCommissionDisbursement(chUser.MSISDN)
                    .downloadCommissionDisbursement().updateCsvWithNewAmount(amount.toString());

            //Initiate Commission disbursement
            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, FilePath.fileCommissionDisbursement);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true, true);

            //get post Balance of the User
            UsrBalance channelUsrBalance1 = MobiquityGUIQueries.getUserBalance(chUser, commWallet.WalletId, null);
            BigDecimal postBalance = channelUsrBalance1.Balance;
            Assertion.verifyAccountIsDebited(preBalance, postBalance, amount, "Verify that Commission is getting successfully Disbursed for Wallet account", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0360_b() throws Throwable {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0360_b",
                "To verify that the netadmin should able to perform Commission Disbursement for Bank.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            BigDecimal amount = new BigDecimal(1);
            TransactionManagement.init(t1)
                    .makeSureChannelUserHasBalanceInWallet(chUser, commWallet.WalletId);

            Map<String, String> accDetails = MobiquityGUIQueries
                    .dbGetAccountDetails(chUser, defProvider.ProviderId, defaultBankId);

            DesEncryptor d = new DesEncryptor();

            String BankAccountNum = d.decrypt(accDetails.get("ACCOUNT_NO"));


            UsrBalance preBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");
            BigDecimal preBalance = preBankFICBalance.Balance;


            //Login as Network admin to perform Commission Disbursement
            Login.init(t1).login(optCD);


            // download the csv file and update the amount with new Amount 'a'
            CommissionDisbursement.init(t1).initiateCommissionDisbursementForBank(chUser, defaultBankId, BankAccountNum);

            // download the csv file and update the amount with new Amount 'a'
            new Commission_Disbursement_Page2(t1)
                    .updateCsvWithNewAmount(amount.toString());

            //Initiate Commission disbursement
            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, FilePath.fileCommissionDisbursement);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_COMM_DISBURSEMENT, batchId, true, true);

            //get post Balance of the User
            UsrBalance postBankFICBalance = MobiquityGUIQueries.dbGetAllNetworkStock1("IND041011");
            BigDecimal postBalance = postBankFICBalance.Balance;


            Assertion.verifyAccountIsDebited(preBalance, postBalance, amount, "Verify that Commission is getting successfully Disbursed for Bank Account", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0670() throws Throwable {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0670",
                "To verify that the InValid user should not be able to perform Commission Disbursement");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.COMMISSION_DISBURSMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        try {

            Login.init(t1).login(cce);
            try {
                driver.findElement(By.id("COMMDIS_ALL"));
                t1.fail("CCE can do Commission disbursement");
            } catch (Exception e) {
                t1.pass("CCE cannot do Commission disbursement");
            }
            t1.info("", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}
