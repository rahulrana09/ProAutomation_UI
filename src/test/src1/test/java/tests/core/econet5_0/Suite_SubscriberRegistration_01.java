package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.Wallet;
import framework.entity.Biller;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.entity.WalletPreference;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.userManagement.BulkChUserRegistrationPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.excelManagement.ExcelUtil;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nirupama MK on 29/08/2018.
 */

public class Suite_SubscriberRegistration_01 extends TestInit {
    private User chAddSubs;
    private OperatorUser nwAdmin, billerRegistor, categoryCreator;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "SingleMSISDN_MultipleUserType");

        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);
        billerRegistor = DataFactory.getOperatorUsersWithAccess("UTL_CREG").get(0);
        categoryCreator = DataFactory.getOperatorUsersWithAccess("UTL_MCAT").get(0);
        nwAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");

    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1033() throws Exception {

        ExtentTest TC_ECONET_1033 = pNode.createNode("TC_ECONET_1033", "To verify that the " +
                "Proper error message should be displayed if Subscriber entered age is less than 16 yrs.");

        TC_ECONET_1033.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        try {

            User sub = new User(Constants.SUBSCRIBER);
            sub.setDateOfBirth(new DateAndTime().getDate(0));

            startNegativeTest();

            Login.init(TC_ECONET_1033).login(chAddSubs);
            SubscriberManagement.init(TC_ECONET_1033).addInitiateSubscriber(sub);

            Assertion.verifyErrorMessageContain("subs.error.custageislessthan.limit",
                    "invalid date of birth", TC_ECONET_1033);

        } catch (Exception e) {
            markTestAsFailure(e, TC_ECONET_1033);
        }
    }

    /**
     * @Deprecated functional flow need to be validated, as its changing preferences and environment variables.
     * @throws Exception
     */
    @Deprecated
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void TC_ECONET_1034() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1034", "To verify that the Proper " +
                "error message should be displayed if Passport expiry date is less than 90 days.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_SIT_5_0);
        String isImtRequired = null, modify_allowed = null;

        try {
            modify_allowed = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("IS_IMT_SEND_MONEY_ENABLED");
            MobiquityGUIQueries.updateModifyAllowed("IS_IMT_SEND_MONEY_ENABLED", "Y");


            isImtRequired = MobiquityGUIQueries.fetchDefaultValueOfPreference("IS_IMT_SEND_MONEY_ENABLED");
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", "Y");

            AppConfig.isImtSendMoneyEnabled = true; // to enter IMT details
            AppConfig.isSecurityQuestionOnLoginEnabled = true;  // to skip the question
            ConfigInput.isCoreRelease = false; // to enter expiry date

            User sub = new User(Constants.SUBSCRIBER);
            sub.setExpiryDate(new DateAndTime().getCurrentDate());

            sub.setIssueDate(new DateAndTime().getCurrentDate());
            Login.init(t1).login(chAddSubs);

            AddSubscriber_Page1 page1 = AddSubscriber_Page1.init(t1);

            // page1.setExpiryDate(new DateAndTime().getCurrentDate());
            SubscriberManagement.init(t1).addInitiateSubscriber(sub);

            Assertion.verifyErrorMessageContain("subs.addinitiate.message.fail",
                    "invalid Expiry date", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            MobiquityGUIQueries
                    .updateModifyAllowed("IS_IMT_SEND_MONEY_ENABLED", modify_allowed);

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_IMT_SEND_MONEY_ENABLED", isImtRequired);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0604() throws Exception {

        ExtentTest TC_ECONET_0604 = pNode.createNode("TC_ECONET_0604", "To verify that the DOB and" +
                " Passport expiry limit should also applicable on Existing services." +
                "Note :- ID no should support special characters.");

        TC_ECONET_0604.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            User subscriber = new User(Constants.SUBSCRIBER);

            subscriber.setExternalCode(subscriber.MSISDN + "ABCD");
            Login.init(TC_ECONET_0604).login(chAddSubs);

            SubscriberManagement.init(TC_ECONET_0604)
                    .addInitiateSubscriber(subscriber);

            Assertion.verifyEqual(false, Assertion.isErrorInPage(TC_ECONET_0604),
                    "Verify there is no error and Alphanumeric External Code is acceptable", TC_ECONET_0604);

        } catch (Exception e) {
            markTestAsFailure(e, TC_ECONET_0604);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0506a1() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0506a1", "To verify that the While adding " +
                "Subscriber in system following modifications in first name field should work." +
                "a. Number of characters can be accommodated by the 'First Name' field to 50 characters.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            User sub4 = new User(Constants.SUBSCRIBER);

            sub4.setFirstName("SUBS1" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(sub4);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify subscriber First Name should contains 50 Charaters of Alpha Numeric ", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0506a2() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0506a2", "To verify that the While adding " +
                "Channel User in system following modifications in first name field should work." +
                "a. Number of characters can be accommodated by the 'First Name' field to 50 characters.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            User chUser = new User(Constants.WHOLESALER);
            chUser.setFirstName("WHS01" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(nwAdmin);
            ChannelUserManagement.init(t1).initiateChannelUser(chUser);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Verify Channel User's First Name should contains 50 Charaters of Alpha Numeric ", t1);


            ExtentTest t2 = pNode.createNode("TC_ECONET_0506a3", "To verify that the While adding " +
                    "Merchant in system following modifications in first name field should work." +
                    "a. Number of characters can be accommodated by the 'First Name' field to 50 characters.");

            t2.assignCategory(FunctionalTag.ECONET_UAT_5_0);

            User mercchant = new User(Constants.MERCHANT);

            mercchant.setFirstName("MERCH" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9)));

            Login.init(t1).login(nwAdmin);

            ChannelUserManagement.init(t2).initiateChannelUser(mercchant);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t2), true,
                    "Verify Merchant's First Name should contains 50 Charaters of Alpha Numeric ", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0507a4() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0507a4", "To verify that the While adding " +
                "Biller in system following modification in contact person field should work." +
                "a. Number of characters that can be accommodated by the 'Contact Person' field to 50 characters.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_0507b4", "To verify that the While adding Biller" +
                " in system following modification in contact person field should work." +
                "b. Allow alpha-numeric and special characters (e.g. comma, dot, apostrophe, space, " +
                "forward slash, hyphen)");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBS_REG, FunctionalTag.ECONET_UAT_5_0);
        try {

            String providerName = DataFactory.getDefaultProvider().ProviderName;

            Biller biller = new Biller(providerName, Constants.BILLER_CATEGORY,
                    Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.setContactName("BiL .,'-/" + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(9))
                    + (DataFactory.getRandomNumber(5)));

            Login.init(t1).login(categoryCreator);
            BillerManagement.init(t1).addBillerCategory();

            Login.init(t1).login(billerRegistor);

            ConfigInput.isConfirm = false;
            ConfigInput.isAssert = false;

            BillerManagement.init(t1).initiateBillerRegistration(biller);

            Assertion.verifyEqual(!Assertion.isErrorInPage(t1), true,
                    "Biller Contact Name accept 50 characters including " +
                            "alpha-numeric and special characters", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 100)
    public void run1() throws Exception {
        ExtentTest t1 = pNode.createNode("Test", "Test");

        try {
            User sub = new User(Constants.SUBSCRIBER);
            Wallet w1 = DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB);
            String[] payIdArr = DataFactory.getPayIdApplicableForSubs();
            WalletPreference walletPref2 = new WalletPreference(sub.CategoryCode,
                    Constants.REGTYPE_NO_KYC,
                    GlobalData.defaultBankName,
                    false,
                    null,
                    null);
            walletPref2.setPaymentInst(Constants.PAYINST_BANK_CONST);
            walletPref2.setPreferenceType(Constants.PREFERENCE_TYPE_BANK_ACCOUNT_LINKING);

            CurrencyProviderMapping.init(pNode)
                    .mapWalletPreferencesUserRegistration(sub, payIdArr);

            Preferences.init(t1)
                    .configureWalletPreferences(walletPref2);

            SubscriberManagement.init(t1).createSubscriberWithSpecificPayIdUsingAPI(sub, w1.WalletId, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }

    @Test
    public void MON_7705_3() {
        ExtentTest test = pNode.createNode("Test_05", "asd");
        try {

            User sub_04 = new User(Constants.WHOLESALER);
            User sub_05 = new User(Constants.SPECIAL_SUPER_AGENT);
            List<User> userList = new ArrayList<>();
            userList.add(sub_04);
            userList.add(sub_05);

            String csvFile = ExcelUtil.getBulkUserRegistrationFile();
//            ChannelUserManagement.init(test)
//                    .writeBulkChUserWalletDetailsInCSV(sub_04, csvFile, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletId);
            for (User user : userList) {
                ChannelUserManagement.init(test)
                        .writeBulkChUserWalletDetailsInCSV(user, csvFile, DataFactory.getDefaultProvider().ProviderName, DataFactory.getDefaultWallet().WalletId);
            }

            // Upload the File
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("CHECK_ALL", Constants.NETWORK_ADMIN);
            Login.init(test).login(netAdmin);
            BulkChUserRegistrationPage page = BulkChUserRegistrationPage.init(test);
            page.navBulkUserRegistration();
            page.uploadFile(csvFile);
            page.submitCsv();


            // Verify that user Registration is Successful
            String message = driver.findElements(By.className("actionMessage")).get(0).getText();
            Assertion.verifyMessageContain(message, "bulkUpload.channel.label.success",
                    "Bulk Channel User Registration with Bank Account Same as Parent", test);
//            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
//            Login.init(test).login(optUsr);
//
//            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);
//
//            page.navBulkChUserAndSubsciberRegistrationAndModification()
//                    .SelectSubscriberRegistrationService();
//
//            SubscriberManagement.init(test)
//                    .writeDataToBulkSubsRegistrationCsv(sub_04, FilePath.fileBulkSubReg, false)
//                    .writeDataToBulkSubsRegistrationCsv(sub_05, FilePath.fileBulkSubReg, true);

//            String batchId = page.uploadFile(FilePath.fileBulkSubReg)
//                    .submitCsv()
//                    .getBatchId();
//
//            page.navBulkRegistrationAndModificationMyBatchesPage()
//                    .ClickOnBatchSubmit()
//                    .downloadLogFileUsingBatchId(batchId);

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }
        Assertion.finalizeSoftAsserts();
    }

//    @Test
//    public void MON_7705_3() {
//        ExtentTest test = pNode.createNode("Test_05", "asd");
//        try {
//
//            User sub_04 = new User(Constants.WHOLESALER);
//            User sub_05 = new User(Constants.SPECIAL_SUPER_AGENT);
//            List<User> l = new ArrayList<>();
//            l.add(sub_04);
//            l.add(sub_05);
//
//            ChannelUserManagement.init(test).bulkChannelUserRegistration(l);
//
//            OperatorUser optUsr = DataFactory.getOperatorUserWithAccess("PTY_ACU");
//            Login.init(test).login(optUsr);
//
//            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(test);
//
//            page.navBulkChUserAndSubsciberRegistrationAndModification()
//                    .SelectSubscriberRegistrationService();
//
//            SubscriberManagement.init(test)
//                    .writeDataToBulkSubsRegistrationCsv(sub_04, FilePath.fileBulkSubReg, false)
//                    .writeDataToBulkSubsRegistrationCsv(sub_05, FilePath.fileBulkSubReg, true);
//
//            String batchId = page.uploadFile(FilePath.fileBulkSubReg)
//                    .submitCsv()
//                    .getBatchId();
//
//            page.navBulkRegistrationAndModificationMyBatchesPage()
//                    .ClickOnBatchSubmit()
//                    .downloadLogFileUsingBatchId(batchId);
//
//        } catch (Exception e) {
//            markTestAsFailure(e, test);
//        }
//        Assertion.finalizeSoftAsserts();
//    }
}