package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.enquiries.Enquiries;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_CCEPortal extends TestInit {

    private static User subs, whs;
    private static OperatorUser cce, nwAdmin;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        try {
            Markup m = MarkupHelper.createLabel("Setup", ExtentColor.BLUE);
            pNode.info(m); // Method Start Marker

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            nwAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            nwAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @AfterClass
    public void tearDown() throws Exception {
        // close all windows
        Utils.closeWindows();
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0489_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0489_a",
                "To verify that CCE should able to view all data of Wholesaler through CCE portal Screen.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);
        String mainWindow = null;
        try {
            Login.init(t1).login(cce);

            mainWindow = Enquiries.init(t1)
                    .initiateGlobalSearch(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whs.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0489_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0489_b",
                "To verify that CCE should able to view all data of subscribers through CCE portal Screen.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);
        String mainWindow = null;
        try {
            Login.init(t1).login(cce);
            Enquiries.init(t1)
                    .initiateGlobalSearchForUser(Constants.USR_TYPE_SUBS, Constants.CCE_MOBILE_NUMBER, subs);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0580_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0580_a",
                "To verify that the CCE should be able to apply different filters in Transaction Enquiry.(Wholesaler MSISDN)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);
        String mainWindow = null;
        try {
            Login.init(t1).login(cce);
            mainWindow = Enquiries.init(t1)
                    .initiateGlobalSearch(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whs.MSISDN);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0580_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0580_b",
                "To verify that the CCE should be able to apply different filters in Transaction Enquiry.(Subscriber MSISDN)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.SARASWATHI);
        String mainWindow = null;
        try {
            Login.init(t1).login(cce);

            mainWindow = Enquiries.init(t1)
                    .initiateGlobalSearch(Constants.USR_TYPE_SUBS, Constants.CCE_MOBILE_NUMBER, subs.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0581() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0581",
                "To verify that the Valid User should be able to apply different filters in Transaction Enquiry.(ID number)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        String mainWindow = null;
        try {
            Login.init(t1).login(cce);

            mainWindow = Enquiries.init(t1)
                    .initiateGlobalSearch(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER, whs.MSISDN);

            Utils.captureScreen(t1);
            WebElement transaction = driver.findElement(By.xpath("//li[@id='global_search_transaction_details_id']/a"));
            Assertion.verifyEqual(transaction.isDisplayed(), true,
                    "Verify CCE is able to view transaction for Wholesaler through CCE portal Screen", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0490_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0490_a",
                "To verify that CCE should able to Change Status of Subscriber users  through CCE portal Screen");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);

        User subscriber = new User(Constants.SUBSCRIBER);
        try {
            SubscriberManagement.init(t1)
                    .createSubscriberDefaultMapping(subscriber, true, false);

            Login.init(t1).login(nwAdmin);

            Enquiries.init(t1)
                    .updateUserStatus(subscriber, Constants.BAR_AS_BOTH);

            Enquiries.init(t1)
                    .updateUserStatus(subscriber, Constants.UNBAR);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC_ECONET_0490_b",
                "To verify that CCE should able to Change Status of Subscriber through CCE portal Screen");
        try {
            Enquiries.init(t2).updateUserStatus(subscriber, Constants.SUSPEND);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0490_b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0734b",
                "To verify that the Following features of CCE Portal can be verified:" +
                        " Verify the caller – through auto verification screen");


        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);

        String mainWindow = null;
        try {
            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub);

            Login.init(t1).login(cce);
            mainWindow = Enquiries.init(t1)
                    .initiateGlobalSearch(Constants.SUBSCRIBER_REIMB,
                            Constants.CCE_MOBILE_NUMBER, sub.MSISDN);

            boolean status = Utils.checkElementPresent("msisdnLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "MSISDN Field", t1);

            status = Utils.checkElementPresent("nameLabel", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "name Field", t1);

            status = Utils.checkElementPresent("date_of_birth_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "date_of_birth Field", t1);

            status = Utils.checkElementPresent("date_of_birth_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "address Field", t1);

            status = Utils.checkElementPresent("registered_on_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "Registered on Field", t1);

            status = Utils.checkElementPresent("registered_by_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "registered_by Field", t1);

            status = Utils.checkElementPresent("last_transaction_status_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_status Field", t1);

            status = Utils.checkElementPresent("last_transaction_on_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_on Field", t1);

            status = Utils.checkElementPresent("last_transaction_type_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "last_transaction_type Field", t1);

            status = Utils.checkElementPresent("current_customer_status_label", Constants.FIND_ELEMENT_BY_ID);
            Assertion.verifyEqual(status, true, "current_customer_status Field", t1, true);

            Utils.captureScreen(t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0491a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0491a",
                "To verify that CCE should able to Reset PIN of channel user through CCE portal Screen.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);
        String mainWindow = null;
        try {


            User whs = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs, false);

            //Create an employee under the channel user
           /* Employee emp = new Employee(whs.MSISDN, whs.CategoryCode);
            Transactions.init(t1).createEmployee(emp);*/

            Login.init(t1).login(cce);
            Enquiries.init(t1)
                    .resetUserPin(whs, true);

            // TODO, need to check this as no option for resetting employee Pin is available
            // available dropdown - ChannelUser, Subscriber
            /*ExtentTest t2 = pNode.createNode("TC_ECONET_0734d",
                    "To verify that the Following feature of CCE Portal can be verified:" +
                            " Employee of an agent can get his PIN reset by a CCE");
            try{
                Login.init(t1).login(cce);
                Enquiries.init(t1)
                        .resetUserPin(emp, true);
            }catch (Exception e){
                markTestAsFailure(e, t2);
            }*/

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0491b() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0491b",
                "To verify that CCE should able to Reset PIN of subscriber through CCE portal Screen.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);
        String mainWindow = null;
        try {
            User sub = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(sub, true, false);

            Login.init(t1).login(nwAdmin);
            Enquiries.init(t1)
                    .resetUserPin(sub, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0734c() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0734c",
                "To verify that the Following feature of CCE Portal can be verified:" +
                        " Display of SVA details along with grades ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);
        String mainWindow = null;
        try {
            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(nwAdmin);

            Enquiries.init(t1)
                    .accountInformation(whs, Constants.CHANNEL_REIMB,
                            Constants.ACCOUNT_IDENTIFIER_MOBILE_NUMBER, whs.MSISDN);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Utils.closeWindowsExceptOne(mainWindow);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(enabled = false, priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0577() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0577",
                "To verify that the Valid user should be able to perform Resend SMS service.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CCE, FunctionalTag.ECONET_UAT_5_0);
        String mainWindow = null;
        try {
            BigDecimal txnAmount = new BigDecimal("1");
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs);

            Login.init(t1).login(cce);

            String serviceID = Transactions.init(t1)
                    .initiateCashOut(subs, whs, txnAmount).ServiceRequestId;

            String txnID = Transactions.init(t1)
                    .cashoutApprovalBysubs(subs, serviceID).TransactionId;

            mainWindow = Enquiries.init(t1)
                    .Last5Transactions(Constants.CHANNEL_REIMB, Constants.CCE_MOBILE_NUMBER,
                            whs.MSISDN, txnID);
        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {

            if (mainWindow != null) {
                Utils.closeWindowsExceptOne(mainWindow);
            }
        }
        Assertion.finalizeSoftAsserts();
    }

}