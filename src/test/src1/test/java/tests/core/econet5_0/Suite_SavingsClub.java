package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.TxnResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.savingsClubManagement.AddClub_Page1;
import framework.pageObjects.savingsClubManagement.ViewClub_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class Suite_SavingsClub extends TestInit {
    private User subsChairmanUI, chUser, subsMember1, subsMember2, subsMember3;
    private OperatorUser naAddClub, naViewClub;
    private String bankId;
    private SavingsClub sClub;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup", "Setup for this test");

        try {
            naAddClub = DataFactory.getOperatorUserWithAccess("ADD_CLUB");
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            subsChairmanUI = new User(Constants.SUBSCRIBER);
            subsMember1 = new User(Constants.SUBSCRIBER);
            bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            SubscriberManagement.init(t1)
                    .createSubscriberDefaultMapping(subsChairmanUI, true, true)
                    .createSubscriberDefaultMapping(subsMember1, true, true);

           /* subsChairmanUI=DataFactory.getUserUsingMsisdn("7787008734");
            subsMember1=DataFactory.getUserUsingMsisdn("7787008733");*/


            TransactionManagement.init(t1)
                    .makeSureLeafUserHasBalance(subsChairmanUI, AppConfig.minSVCDepositAmount)
                    .makeSureLeafUserHasBalance(subsMember1, AppConfig.minSVCDepositAmount);

            // Map SVC wallet Preference
            Preferences.init(t1)
                    .setSVCPreferences(subsChairmanUI, bankId);

            sClub = new SavingsClub(subsChairmanUI,
                    Constants.CLUB_TYPE_PREMIUM, bankId, false, false)
                    .addMember(subsMember1);

            sClub.MinMemberCount = 1;
            sClub.MinApproverCount = 1;

            // create Saving Club
            SavingsClubManagement.init(t1)
                    .createSavingsClub(sClub);

            // make sure that Users are joined and are active
            Transactions.init(t1)
                    .joinOrResignSavingClub(sClub, subsMember1, true)
                    .verifyStatus(Constants.TXN_SUCCESS);


            // define transfer rule for Club Deposit
            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(new ServiceCharge(Services.CLUB_DEPOSIT,
                            subsMember1,
                            subsChairmanUI,
                            null,
                            DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                            null,
                            null)
                    );

            Transactions.init(t1)
                    .depositSavingClub(sClub, subsChairmanUI, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);

            Transactions.init(t1)
                    .depositSavingClub(sClub, subsMember1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);


            naViewClub = DataFactory.getOperatorUsersWithAccess("VIEW_CLUB").get(0);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(priority = 1, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1042() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1042", "To verify that the Only one Ecocash member will be part of Club Chairman and can be " +
                    "multiples member group in different savings club.");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            try {

                //Create 2nd Chairman
                User subsChairman2 = new User(Constants.SUBSCRIBER);

                SubscriberManagement.init(t1)
                        .createSubscriberDefaultMapping(subsChairman2, true, true);

                //Create club for that Chairman2
                SavingsClub sClub2 = new SavingsClub(subsChairman2,
                        Constants.CLUB_TYPE_PREMIUM, bankId, false, false);

                SavingsClubManagement.init(t1)
                        .createSavingsClub(sClub2);


                //Join 2nd Club
                Transactions.init(t1)
                        .joinOrResignSavingClub(sClub2, subsMember1, true)
                        .verifyStatus(Constants.TXN_SUCCESS).verifyMessage("savingclub.member.join.success", sClub2.ClubName, sClub2.ClubId);


            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1045() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1045", "To verify that the Valid user should able to " +
                    "View Club List from WEB module as well.");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            try {

                Login.init(t1).login(naViewClub);
                ViewClub_Page1.init(t1)
                        .navViewSavingClub();
                t1.pass("Operator User is able to view club List");

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 8, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1047() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1047", "To verify that the Club Chairman should able " +
                    "to approve Club Transactions. (Disburse club money)");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            try {

                // ServiceCharge Object
                ServiceCharge cDeposit = new ServiceCharge(Services.CLUB_DEPOSIT,
                        subsMember1,
                        subsChairmanUI,
                        null,
                        DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                        null,
                        null);

                //Configure Service Charge For Club Deposit
                ServiceChargeManagement.init(t1)
                        .configureServiceCharge(cDeposit);

                String txnId = Transactions.init(t1)
                        .clubDisbursementInitiation(sClub, subsChairmanUI, subsMember1, "1");

                Transactions.init(t1).clubDisbursementConfirmation(sClub, subsChairmanUI, txnId, "1", "1").verifyStatus(Constants.TXN_SUCCESS);

                ExtentTest t2 = pNode.createNode("TC_ECONET_1168", "To verify that  Club chairman is able to perform Settlement process successfully.");


                Transactions.init(t2)
                        .joinOrResignSavingClub(sClub, subsMember1, false)
                        .verifyStatus(Constants.TXN_SUCCESS);


                String txnId1 = Transactions.init(t2)
                        .clubSettlementInitiation(sClub, subsChairmanUI, subsMember1, "1");

                Transactions.init(t2).clubSettlementConfirmation(sClub, subsChairmanUI, txnId1, "1", "1").verifyStatus(Constants.TXN_SUCCESS);

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 4, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0518() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_0518", "To verify that the Club Chairman should be " +
                    "able to approve Club Transactions. (Withdraw from Bank)");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            try {

                // ServiceCharge Object
                ServiceCharge sWithdraw = new ServiceCharge(Services.CLUB_WITHDRAW, subsChairmanUI, subsMember1, DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                        null,
                        null,
                        null
                );


                //Configure Service Charge For Club Deposit
                ServiceChargeManagement.init(t1)
                        .configureServiceCharge(sWithdraw);

                String svcWithdrawAmount = "4";

                UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subsMember1, null, null);

                // Verify that SVC Withdrawal is successfully Initiated
                TxnResponse response = Transactions.init(t1)
                        .svcWithdrawCash(sClub.ClubId, subsMember1.MSISDN, svcWithdrawAmount)
                        .verifyMessage("savingclub.withdraw.initiated",
                                DataFactory.getDefaultProvider().Currency,
                                svcWithdrawAmount,
                                subsMember1.LoginId
                        );

                Transactions.init(t1)
                        .svcApproverWithdraw(sClub.ClubId, subsChairmanUI.MSISDN, response.TxnId)
                        .verifyStatus(Constants.TXN_SUCCESS);

                // Get The post Approval balance
                UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subsMember1, null, null);
                if (subPreBal.Balance.compareTo(subPostBal.Balance) < 0) {
                    t1.pass("The Member Balance is Credited after SVC Withdrawal");
                } else {
                    t1.fail("Member's Balance is not Credited");
                }

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1167() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1167", "To verify that  Club admin should be able " +
                    "initiate transfer from   club wallet to bank account.");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

            try {

                //Subscriber Balance

                UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subsChairmanUI, Constants.SAVINGS_CLUB, null);
                int preBalanceForSubs = preSubsBalance.Balance.intValue();

                //Bank Balance

                BigDecimal bankBalance = MobiquityGUIQueries.getBalanceBankWallet(DataFactory.getDefaultBankNameForDefaultProvider());
                int bankbal = bankBalance.intValue();

                TxnResponse response = Transactions.init(t1).SVCWalletToBank(sClub, subsChairmanUI.MSISDN, "5").verifyStatus(Constants.TXN_SUCCESS);
                t1.pass("Club admin is able to initiate transfer from   club wallet to bank account.");

                String transactionId = response.TxnId;
                String trId = response.TrId;

                Transactions.init(t1).SVCWalletToBankConfirmation(sClub, subsChairmanUI.MSISDN, transactionId, trId).verifyStatus(Constants.TXN_SUCCESS);
                t1.pass("Club admin is able to approve transfer from club wallet to bank account.");

                //Subscriber Balance

                UsrBalance postSubsBalance = MobiquityGUIQueries.getUserBalance(subsChairmanUI, Constants.SAVINGS_CLUB, null);
                int postBalanceForSubs = postSubsBalance.Balance.intValue();

                //Bank Post Balance

                BigDecimal bankPostBalance = MobiquityGUIQueries.getBalanceBankWallet(DataFactory.getDefaultBankNameForDefaultProvider());
                int bankPostbal = bankPostBalance.intValue();

                Assert.assertEquals(postBalanceForSubs, preBalanceForSubs - 5);
                Assert.assertEquals(bankPostbal, bankbal + 5);


                ExtentTest t2 = pNode.createNode("TC_ECONET_1169", "To verify that  Club admin should be able " +
                        "initiate transfer from   club bank to wallet account.");

                TxnResponse response1 = Transactions.init(t2).SVCbankToWallet(sClub, subsChairmanUI.MSISDN, "5").verifyStatus(Constants.TXN_SUCCESS);
                t2.pass("Club admin initiate transfer from bank account to club wallet .");

                String transactionId1 = response1.TxnId;
                String trId1 = response1.TrId;

                Transactions.init(t2).SVCBankToWalletConfirmation(sClub, subsChairmanUI.MSISDN, transactionId1, trId1).verifyStatus(Constants.TXN_SUCCESS);
                t2.pass("Club admin is able to approve transfer from club bank to wallet account.");

                //Bank Post Balance

                BigDecimal bankFinalBalance = MobiquityGUIQueries.getBalanceBankWallet(DataFactory.getDefaultBankNameForDefaultProvider());
                int bankFinalbal = bankFinalBalance.intValue();

                //Subscriber Balance

                UsrBalance finalSubsBalance = MobiquityGUIQueries.getUserBalance(subsChairmanUI, Constants.SAVINGS_CLUB, null);
                int finalBalanceForSubs = finalSubsBalance.Balance.intValue();

                Assert.assertEquals(postBalanceForSubs, finalBalanceForSubs - 5);
                Assert.assertEquals(bankPostbal, bankFinalbal + 5);

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0567() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_0567", "To verify that the Valid user should be " +
                    "able to generate Generate club statement.");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

            try {

                SavingsClubManagement.init(t1).viewClubReport(sClub.ClubId);

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1128() throws Exception {
        {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1128", "To verify that the Proper error message should get displayed " +
                    "on web if user enter invalid date.");

            t1.assignCategory(FunctionalTag.SAVING_CLUB,
                    FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            try {

                Login.init(pNode).login(naAddClub);

                AddClub_Page1 page1 = AddClub_Page1.init(t1);

                page1.navigateToClubReportPage();
                page1.SelectFromandToDateForApproval(new DateAndTime().getDate(0), new DateAndTime().getDate(1));
                page1.clickViewReport();

                Assertion.verifyErrorMessageContain("btsl.error.msg.todatebeforecurrentdate", "Date should be in correct format", t1);

            } catch (Exception e) {
                markTestAsFailure(e, t1);
            }

            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 3, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0})
    public void assignRoleForDifferentSavingClubUser() throws Exception {

        ExtentTest test = pNode.createNode("TC_ECONET_1040", "To verify that the Valid user should able " +
                "to assign roles for saving club for different users by Group Role Management.");

        try {

            Login.init(test).loginAsSuperAdmin(Roles.GROUP_ROLE_MANAGEMENT);

            test.assignCategory(FunctionalTag.ECONET_SIT_5_0);

            List<String> groupRoleList = Arrays.asList(Roles.ADD_CLUB, Roles.VIEW_CLUB_LIST, Roles.MODIFY_CLUB);

            WebGroupRole groupRole = new WebGroupRole(Constants.NETWORK_ADMIN, "SavingClubRoleTest", groupRoleList);

            GroupRoleManagement.init(test).addWebGroupRole(groupRole);
        } catch (Exception e) {
            markTestAsFailure(e, test);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws Exception
     */
    @Test(enabled = false, priority = 3, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0})
    public void validUserPermissionCheck() throws Exception {

        ExtentTest test = pNode.createNode("TC_ECONET_1166", "To verify that no user other than Club chairman " +
                "is able to approve Club transaction. (Withdraw from Bank)");

        try {

            OperatorUser optUserWithoutAccess = DataFactory.getOptUserWithOutAccess("", test);

            Login.init(test).login(optUserWithoutAccess);

            FunctionLibrary.init(test).verifyLinkNotAvailable("", "");

        } catch (Exception e) {
            markTestAsFailure(e, test);
        }

        Assertion.finalizeSoftAsserts();
    }


    @Test
    public void test() {

        for (WebGroupRole userRole : GlobalData.rnrDetails) {

            System.out.println(userRole.RoleName);
        }
    }

}
