package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkCashInCsv;
import framework.dataEntity.BulkMerchantPaymentCSV;
import framework.dataEntity.UsrBalance;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.bulkPayoutTool.BulkPayoutInitiate_page1;
import framework.pageObjects.inventoryManagementSystem.InitiateBulkUpload_Page01;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.*;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Automation team
 */
public class Suite_BulkPayoutTool_01 extends TestInit {

    private User subscriber, mercUser, whsUser, merchant;
    private OperatorUser netAdmin, bulkdashboard;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "BulkPayoutTool");
        try {

            mercUser = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);
            whsUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            bulkdashboard = DataFactory.getOperatorUserWithAccess("BULK_DASHBOARD");
            merchant = DataFactory.getChannelUserWithCategory(Constants.MERCHANT);


            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(new ServiceCharge(Services.C2C, mercUser, whsUser,
                            null, null, null, null)
                    );

            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(new ServiceCharge(Services.CASHIN, mercUser, subscriber,
                            null, null, null, null)
                    );
            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(new ServiceCharge(Services.MERCHANT_PAY, subscriber, merchant,
                            null, null, null, null)
                    );

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0981() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0981",
                "To verify that system should not be able to perform any service " +
                        "through Bulk payout tool if the file uploaded is not a .csv file.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL,
                        FunctionalTag.ECONET_SIT_5_0);
        try {

            String filePath = FilePath.dirFileUploads + "AutomationBulkPayOut" + DataFactory.getTimeStamp();
            String fileName = filePath + ".txt";

            File f = new File(fileName);
            f.createNewFile();

            Login.init(t1).login(netAdmin);
            BulkPayoutInitiate_page1 page = BulkPayoutInitiate_page1.init(t1);

            page.navigateToNewLink();
            page.serviceSelectText(Constants.BULK_PAYOUT_SERVICE_CASHOUT);

            ConfigInput.isAssert = false;
            page.fileUpload(f.getAbsolutePath());

            String actual = BulkPayoutInitiate_page1.init(t1).verfiy_Errormessage();
            Assertion.verifyMessageContain(actual, "bulk_pay_error",
                    "Verify that only csv files can be uploaded", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0982() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0982", "To verify that system should not be able to do" +
                " service that can be performed through Bulk payout tool if From " +
                "MFS Provider* mandatory field is invalid in the uploaded file.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_SIT_5_0);
        try {

            //provide invalid providerID
            String fileName = BulkPayoutTool.init(t1).generateFileForO2C("" + DataFactory.getRandomNumber(4), DataFactory.getDefaultWallet().WalletId, merchant, "3");

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout("O2C transfer", fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_O2C, batchId, true);

            Login.init(t1).login(bulkdashboard);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_O2C,
                    batchId, true, false);


            //Assertion.verifyMessageContain(actual, "bulk_payout_upload_error",
            //"Verify that Bulk Payout file upload fail if Mandatory field's data is not correct", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0571() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0571 ", "To verify that the NwAdmin" +
                " should be able to perform Bulk C2C.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_0572 ", "To verify that the Proper " +
                "debit and credit should be made for the same & SMS notification should be sent to both channel users.");

        ExtentTest t4 = pNode.createNode("TC_ECONET_0676 ", "To verify that the Following checks " +
                "should be applied during bulk payout tool. " +
                "1.Transfer rules" +
                "2.Service Charge/Commission" +
                "3.Thresholds - Already defined in base setup");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL,
                FunctionalTag.ECONET_UAT_5_0);

        try {
            // define transfer Rule and Service charge
            // make sure Channel user has sufficient balance
            TransactionManagement.init(t4).makeSureChannelUserHasBalance(mercUser);

            //get sender pre balance
            UsrBalance senderBalance = MobiquityGUIQueries.getUserBalance(mercUser, null, null);
            BigDecimal senderPreBalance = senderBalance.Balance;
            t2.info(mercUser.MSISDN + ", Sender Pre Balance: " + senderPreBalance);

            //get reciever pre balance
            UsrBalance recieverBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null);
            BigDecimal recieverPreBalance = recieverBalance.Balance;
            t2.info(whsUser.MSISDN + ", Receiver Pre Balance: " + recieverPreBalance);

            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

            String fileName = bulkPayObj.generateBulkC2CcsvFile(mercUser.MSISDN, whsUser.MSISDN);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_C2C, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_C2C, batchId, true, true);

            //get channel user_01 post balance
            senderBalance = MobiquityGUIQueries.getUserBalance(mercUser, null, null);
            BigDecimal sendePostBalance = senderBalance.Balance;
            t2.info(mercUser.MSISDN + ", Sender Post Balance: " + sendePostBalance);

            //get channel user_02 post balance
            recieverBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null);
            BigDecimal recieverPostBalance = recieverBalance.Balance;
            t2.info(whsUser.MSISDN + ", Reciever Post Balance: " + recieverPostBalance);

            ChannelUserManagement.init(t2).checkIfUserBalanceisCredited(senderPreBalance, sendePostBalance,
                    true, "ChannelUser " + mercUser.MSISDN);

            ChannelUserManagement.init(t2).checkIfUserBalanceisCredited(recieverPreBalance, recieverPostBalance,
                    false, "ChannelUser " + whsUser.MSISDN);

            ExtentTest t3 = pNode.createNode("TC_ECONET_0386 ", "To verify that system should " +
                    "generate unique Batch Id on every transaction/service performed through Bulk payout tool.");

            String batchId_02 = BulkPayoutTool.init(t3)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);

            Assertion.verifyNotEqual(batchId, batchId_02, "Unique Batch ID Generated", t3);

            Map<String, String> txnID = MobiquityGUIQueries
                    .getTransferIDandTransferStatus(Services.C2C, mercUser.MSISDN);

            String txn = txnID.get("TRANSFER_ID");
            String status = txnID.get("TRANSFER_STATUS");

            Assertion.verifyEqual(status, Constants.TXN_STATUS_SUCCESS, "Status is SUCCESS", t2);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM,
            FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0387() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0387 ", "To verify that system should  " +
                "be able to perform the CashIN service through Bulk payout tool.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_0532 ", "Bulk Cashin using excel" +
                " sheet shared by agents, To verify that file is uploaded successfully ");

        ExtentTest t3 = pNode.createNode("TC_ECONET_0533 ", "Bulk Cashin using excel" +
                " sheet shared by agents, To verify that the user is able to approve the initiated Bulk payment");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL,
                FunctionalTag.ECONET_UAT_5_0);

        try {
            //perform o2c transaction
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(mercUser, new BigDecimal(110));

            //get channel user pre balance
            UsrBalance preChannelUsrBalance = MobiquityGUIQueries.getUserBalance(mercUser, null, null);
            BigDecimal preChUsrBal = preChannelUsrBalance.Balance;

            //get subscriber pre balance
            UsrBalance preSubscriberBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal preSubsBal = preSubscriberBalance.Balance;

            //Generate the csv file
            String fileName = BulkPayoutTool.init(t2).generateNewBulkCashinCsvFile(mercUser.MSISDN, subscriber.MSISDN);

            String batchId = BulkPayoutTool.init(t2)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            //Approve bulk payout for commission disbursement
            BulkPayoutTool.init(t3)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true, true);

            //get channel user post balance
            UsrBalance postChannelUsrBalance = MobiquityGUIQueries.getUserBalance(mercUser, null, null);
            BigDecimal postChUsrBal = postChannelUsrBalance.Balance;

            //get subscriber post balance
            UsrBalance postSubscriberBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal postSubsBal = postSubscriberBalance.Balance;

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preChUsrBal, postChUsrBal,
                    true, "ChannelUser");

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preSubsBal, postSubsBal,
                    false, "Subscriber");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0389() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0389 ", "To verify that system should be" +
                " able to perform the Merchant Payment service through Bulk payout tool.");

        t1.assignCategory(FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL,
                FunctionalTag.ECONET_UAT_5_0);

        try {

            //Merchant pay is done to offline merchant
            User offline_merchant = new User(Constants.MERCHANT);
            offline_merchant.setMerchantType("OFFLINE");
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(offline_merchant, false);

            //perform o2c transaction
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subscriber, new BigDecimal(5));

            //get subscriber pre balance
            UsrBalance preSubsBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal preSubsBal = preSubsBalance.Balance;

            //get merchant pre balance
            UsrBalance preMerchantBalance = MobiquityGUIQueries.getUserBalance(offline_merchant, null, null);
            BigDecimal preMerBal = preMerchantBalance.Balance;

            // add data to the csv
            List<BulkMerchantPaymentCSV> merchPayList = new ArrayList<>();
            merchPayList.add(new BulkMerchantPaymentCSV("1",
                    GlobalData.defaultProvider.ProviderId,
                    GlobalData.defaultWallet.WalletId,
                    subscriber.MSISDN,
                    GlobalData.defaultWallet.WalletId,
                    offline_merchant.MSISDN,
                    "2",
                    "Remark Ok")
            );

            String fileName = BulkPayoutTool.init(t1)
                    .downloadAndUpdateBulkMerchantPayCsv(merchPayList);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_MER_PAY, fileName);

            //Approve bulk payout for Merchant Payment
            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_MER_PAY, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_MER_PAY, batchId, true, true);

            //get merchant post balance
            UsrBalance postMerchantBalance = MobiquityGUIQueries.getUserBalance(offline_merchant, null, null);
            BigDecimal postMerBal = postMerchantBalance.Balance;

            //get subscriber post balance
            UsrBalance postSubscriberBalance = MobiquityGUIQueries.getUserBalance(subscriber, null, null);
            BigDecimal postSubBal = postSubscriberBalance.Balance;

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preSubsBal, postSubBal,
                    true, "Subscriber");

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preMerBal, postMerBal,
                    false, "Merchant");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0394() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0394 ", "To verify that the NwAdmin" +
                " should be able to perform Bulk O2C.")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);

        try {
            //get channel user pre balance
            BigDecimal preRecieverBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null).Balance;

            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

            //from MSISDN is not required
            String fileName = BulkPayoutTool.init(t1).generateFileForO2C(DataFactory.getDefaultProvider().ProviderId, DataFactory.getDefaultWallet().WalletId, whsUser, "3");

            String batchId = bulkPayObj
                    .initiateNewBulkPayout(Constants.BULK_O2C, fileName);

            //Approve bulk payout for O2C
            bulkPayObj.approveRejectNewBulkPayout(Constants.BULK_O2C, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1)
                    .verifyBulkPayoutDashboard(Constants.BULK_O2C, batchId, true, true);

            BigDecimal postRecieverBalance = MobiquityGUIQueries.getUserBalance(whsUser, null, null).Balance;

            ChannelUserManagement.init(t1).checkIfUserBalanceisCredited(preRecieverBalance, postRecieverBalance,
                    false, "ChannelUser");

            Map<String, String> txnID = MobiquityGUIQueries
                    .getTransferIDandTransferStatus(Services.O2C, whsUser.MSISDN);

            //String txn = txnID.get("TRANSFER_ID");
            String status = txnID.get("TRANSFER_STATUS");

            Assertion.verifyEqual(status, Constants.TXN_STATUS_SUCCESS, "Verify Transaction is SUCCESS", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0678() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0678 ", "To verify that the Failed transaction" +
                " can be retried or option to download failed transaction is available.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_0677 ", "To verify that the Error logs" +
                " are generated in case of any errors while uploading batch request in the system.");

        ExtentTest t3 = pNode.createNode("TC_ECONET_0675 ", "To verify that the Initiator " +
                "and receiver should be different in the system for a Bulk Payout Tool request.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);
        try {
            String batchId;
            Login.init(t2).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t2);

            //upload incorrect CSV file
            String fileName = bulkPayObj.generateBulkC2CcsvFile("", whsUser.MSISDN);


            batchId = BulkPayoutTool.init(t1)
                    .startNegativeTest()
                    .initiateNewBulkPayoutNegative(Constants.BULK_C2C, fileName);

            String actual = BulkPayoutInitiate_page1.init(t1).verfiy_ErrorStatusLog();

            Assertion.verifyMessageContain(actual, "bulkupload.fail.initiate.product.inventory",
                    "Bulk Payout file upload fail", t2);

            ConfigInput.isAssert = true;
            fileName = bulkPayObj.generateBulkC2CcsvFile(whsUser.MSISDN, whsUser.MSISDN);

            batchId = BulkPayoutTool.init(t3)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);

            BulkPayoutTool.init(t3)
                    .approveRejectNewBulkPayout(Constants.BULK_C2C, batchId, true);

            Login.init(t3).login(bulkdashboard);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t3).verifyBulkPayoutDashboard(Constants.BULK_C2C,
                    batchId, true, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /*@Test(enabled = false, priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ENET() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0678 ", "To verify that the Failed transaction" +
                " can be retried or option to download failed transaction is available.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);
        try {

            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            CustomerBill bill = biller.getAssociatedBill();
            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

            //upload incorrect CSV file
            String fileName = bulkPayObj.generateBulkBilPaycsvFile("", biller.BillerCode, bill.BillAccNum);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_C2C, batchId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }*/


    /**
     * TEST: TC_ECONET_0985
     * Fetch default Subs then Check its Balance , After giving balance bar that user as Receiver
     * then PerformBulk P2P and check the status
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0985() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0985", "To verify that system should be able" +
                " to perform the P2P Transfer service through Bulk payout tool when  subscriber (Payer) is barred as Receiver.");

        try {
            User sub1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User payer = CommonUserManagement.init(t1).getDefaultSubscriber(null);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(payer);

            SubscriberManagement.init(t1)
                    .barSubscriber(payer, Constants.BAR_AS_RECIEVER);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BAR_USER);
            //Generate the csv file
            String fileName1 = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(payer.MSISDN, sub1.MSISDN);

            //Login with user who has bulk payout initiate access
            Login.init(t1).loginAsOperatorUserWithRole(Roles.NEW_BULK_PAYOUT_INITIATE);

            //Initiate bulk payout for Cashin
            String batchId = BulkPayoutTool.init(t1).initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, fileName1);

            //Login with user who has bulk payout approve access
            Login.init(t1).loginAsOperatorUserWithRole(Roles.NEW_BULK_PAYOUT_APPROVE);

            //Approve bulk payout for P2P
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, batchId, true, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1073() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1073 ", "To verify File should not be uploaded successfully and proper " +
                "+error message should be displayed in error log File.");
        try {

            Login.init(t1).login(netAdmin);

            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

            String fileName = bulkPayObj.generateBulkC2CcsvFile(Constants.INVALID_MSISDN_LENGTH, netAdmin.MSISDN);

            startNegativeTest();

            BulkPayoutTool.init(t1)
                    .initiateNewBulkPayoutNegative(Constants.BULK_C2C, fileName);

            String actual = InitiateBulkUpload_Page01.init(t1).verifyErrorMessage();


            Assertion.verifyMessageContain(actual, "bulkupload.fail.initiate.product.inventory",
                    "Invalid Format", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 11, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0528() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0528 ", "Template file should be downloaded for the selected " +
                "service successfully.");
        try {

            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);
            mercUser = DataFactory.getChannelUserWithAccess(Constants.WHOLESALER);

            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            bulkPayObj.downloadTemplateFile(Constants.BULK_C2C);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            Utils.isFileDownloaded(oldFile, newFile);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


    }

    @Test(priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0529() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0529 ", "User should be able to view the uploaded file on " +
                "the confirmation screen successfully.");
        try {
            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);
            mercUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            String fileName = bulkPayObj.generateBulkC2CcsvFile(mercUser.MSISDN, whsUser.MSISDN);


            //   String fileName = bulkPayObj.generateBulkC2CcsvFile(Constants.INVALID_MSISDN_LENGTH, netAdmin.MSISDN);

            BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);

            //   Assertion.verifyResponseSucceeded();


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0530() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0530",
                " should be displayed." + "E.g.Bulk Cron Job have been successfully initiated. " +
                        "Your batch Id is BC1569xxxxxxxxxxxxxxxxxx");
        try {

            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);
            mercUser = DataFactory.getChannelUserWithAccess(Constants.WHOLESALER);

            String oldFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            bulkPayObj.downloadTemplateFile(Constants.BULK_C2C);
            String newFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);

            Utils.isFileDownloaded(oldFile, newFile);


        } catch (Exception e) {
            markTestAsFailure(e, t1);


        }
    }

    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0531() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0531", "User should be able to accept the bulk cron job successfully" +
                " and proper success message should be displayed on the screen.");
        try {

            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);

            mercUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            String fileName = bulkPayObj.generateBulkC2CcsvFile(mercUser.MSISDN, mercUser.MSISDN);
            String batchID = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_C2C, fileName);
            Login.init(t1).login(DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(1));

            bulkPayObj.approveRejectNewBulkPayout(Constants.BULK_C2C, batchID, true);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 14, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0392() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0392", "To verify that system should be able to perform the Bill Payment service through Bulk payout tool.");
        try {
            String service = "BILL PAY";
            Biller biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.addBillForCustomer(subscriber.MSISDN, DataFactory.getRandomNumberAsString(5));

            CustomerBill bill = new CustomerBill(biller, subscriber.MSISDN, DataFactory.getRandomNumberAsString(5), "Test Bill");
            Login.init(t1).login(netAdmin);
            BulkPayoutTool bulkPayObj = BulkPayoutTool.init(t1);
            String fileName = bulkPayObj.generateBulkBilPaycsvFile(subscriber.MSISDN, biller.BillerCode, bill.BillAccNum);

            String batchId = bulkPayObj.initiateNewBulkPayout(service, fileName);

            bulkPayObj.approveRejectNewBulkPayout(service, batchId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(enabled = false, priority = 15, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0824() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0824", "To verify that system should be able to perform the Cash In Others  service through Bulk payout tool.");
        t1.assignCategory(FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PRASHANT);
        try {
            String service = "CASH IN OTHER";
            User subs = new User(Constants.SUBSCRIBER);
            List<BulkCashInCsv> entBillPayList = new ArrayList<>();
            entBillPayList.add(new BulkCashInCsv("1", GlobalData.defaultProvider.ProviderId, GlobalData.defaultWallet.WalletId,
                    mercUser.MSISDN, GlobalData.defaultWallet.WalletId, subs.MSISDN,
                    "99", "Remark Ok"));

            String fileName = BulkPayoutTool.init(t1)
                    .downloadAndUpdateBulkCashInCsv(entBillPayList);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 17, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0983() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0983", "To verify that system should not be able to perform the CashIN service through Bulk payout tool when Channel User(Payer) is barred as Sender");
        t1.assignCategory(FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PRASHANT);
        try {

            Login.init(t1).login(netAdmin);
            User usr = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, "GWS");

            String fileName = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(usr.MSISDN, subscriber.MSISDN);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0986() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0986", "To verify that system should not be able to perform the P2P transfer service through Bulk payout tool if Requested amount is not in multiple of defined value.");
        t1.assignCategory(FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.PRASHANT);
        try {

            Login.init(t1).login(netAdmin);
            User usr = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            String fileName = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(usr.MSISDN, subscriber.MSISDN);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, batchId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0984() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0984", "To verify that system should not be able to perform the CashIN service through Bulk payout tool if channel user is associated with 2 different MFS Provider and channel user's wallet is suspended for one MFS Provider (MFS1).");
        t1.assignCategory(FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_SIT_5_0);
        try {
            if (DataFactory.getAllProviderId().size() < 2) {
                return;
            }

            User usr = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUser(usr);
            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_SCH"));
            ChannelUserManagement.init(t1).suspendOrDeleteUserWallet(usr, "Suspended", Wallets.SAVINGCLUB);
            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("PTY_SCHAPP"));
            ChannelUserManagement.init(t1).modifyUserApproval(usr);

            //usr = CommonUserManagement.init(t1).getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, "GWS");

            Login.init(t1).login(netAdmin);
            String fileName = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(usr.MSISDN, subscriber.MSISDN);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            BulkPayoutTool.init(t1)
                    .approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, batchId, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


}