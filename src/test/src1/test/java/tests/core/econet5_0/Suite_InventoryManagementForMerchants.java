package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.inventoryManagement.InventoryManagement;
import framework.pageObjects.inventoryManagementSystem.InitiateBulkUpload_Page01;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;
/*
 * Created by Nirupama MK on 03/10/2018.
 */

public class Suite_InventoryManagementForMerchants extends TestInit {
    User merBulkInitiator;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest setup = pNode.createNode("SETUP", "Inventory Management System");
        merBulkInitiator = DataFactory.getChannelUserWithAccess("IMS_BULK_INIT");

    }

    /*
        TC_ECONET_0685
        Upload invalid format
        validate the error message

        TC_ECONET_0399
        Upload valid format
        Successful in uploading the product inventory

        TC_ECONET_0398
        Upload valid format
        Successful in uploading the product Catalogue
    */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0685() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0685", "To verify that the Proper Validations" +
                " should happen during upload of voucher.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            InventoryManagement imObj = InventoryManagement.init(t1);

            //upload incorrect file format
            String fileName = imObj.generateProductCatalogueCsvFile();

            startNegativeTest();

            Login.init(t1).login(merBulkInitiator);

            imObj.initiateBulkUpload(Services.PRODUCT_INVENTORY, fileName);

            //Sleep is required
            Utils.putThreadSleep(2000);

            String actual = InitiateBulkUpload_Page01.init(t1).verifyErrorMessage();

            Assertion.verifyMessageContain(actual, "bulkupload.fail.initiate.product.inventory",
                    "Invalid Format", t1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0399", "To verify that the Merchant should able" +
                    " to Upload Product Inventory Vouchers by CSV.");
            imObj = InventoryManagement.init(t2);
            //Upload correct format
            fileName = imObj.generateProductInventoryCsvFile();

            stopNegativeTest();

            Login.init(t2).login(merBulkInitiator);
            imObj.initiateBulkUpload(Services.PRODUCT_INVENTORY, fileName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0398() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0398", "To verify that the Merchant should able" +
                " to Upload Product Catalouge Vouchers by CSV.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            InventoryManagement imObj = InventoryManagement.init(t1);
            String fileName = imObj.generateProductCatalogueCsvFile();

            Login.init(t1).login(merBulkInitiator);
            imObj.initiateBulkUpload(Services.PRODUCT_CATALOGUE, fileName);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0690() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0690", "To verify if No user other than Merchant" +
                " user should be allowed to create inventories of electronic vouchers for any registered" +
                " & active merchants in the system.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SMS_CONFIGURATION, FunctionalTag.ECONET_UAT_5_0);
        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("IMS_MGMT_IMS_BULK_INIT"));
                t1.fail("Inventory Management System link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("Inventory Management System link is not displayed for Channel Admin as Expected", t1);
            }

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("IMS_MGMT_IMS_BULK_INIT"));
                t1.fail("Inventory Management System link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Inventory Management System link is not displayed for Channel User as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0684() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0684", "To verify that the Merchant should be " +
                "able to view uploaded Inventory.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.INVENTORY_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            //check for the status in BulkPayout Dashboard screen
            //no Bulk id's available in Approval page
            InventoryManagement.init(t1).verifyIMSDashboard(Constants.IMS_INVENTORY,
                    "33de51de-d172-4993-8d5c-5969c1c21f0d", true, true);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

}
