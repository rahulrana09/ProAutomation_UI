package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.accountManagement.AccountManagement;
import framework.features.bulkUserDeletion.BulkUserDeletion;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.accountManagement.DeleteWallet_Page1;
import framework.util.common.Assertion;
import framework.util.common.DBAssertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.HashMap;

/**
 * Created by surya.dhal on 9/5/2018.
 */
public class Suite_AccountManagement extends TestInit {
    private static String walletName = null, newWalletName = null, normalWallet = null;
    SuperAdmin saModifyWallet, superadmin;
    private OperatorUser netAdmin, barUser;
    private User chUser, user;

    @BeforeTest(alwaysRun = true)
    public void prerequisite() throws Exception {
        try {

            walletName = "Wallet" + DataFactory.getRandomNumberAsString(4);
            newWalletName = "NewWallet" + DataFactory.getRandomNumberAsString(4);
            netAdmin = DataFactory.getOperatorUserWithAccess(Roles.ADD_WALLET, Constants.NETWORK_ADMIN);
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            saModifyWallet = DataFactory.getSuperAdminWithAccess("MFSMD");
            superadmin = DataFactory.getSuperAdminWithAccess("MFSMD");
            normalWallet = DataFactory.getDefaultWallet().WalletName;

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0001
     * DESC : To verify that Network Admin is able to 'Add wallet' successfully.
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0001() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0001", "To verify that Network Admin is able to 'Add wallet' successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        Login.init(t1).login(netAdmin);

        try {
            AccountManagement.init(t1).addWallet(walletName, true);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0006
     * DESC : To verify that Network Admin should be able to create new wallet and ensure no default services are associated with that wallet.
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0006() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0006", "To verify that Network Admin should be able to create new wallet and ensure no default services are associated with that wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(netAdmin);
            AccountManagement.init(t1).addWallet(walletName, true);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.VIEW_WALLET);
            AccountManagement.init(t1).verifyDefaultServiceAssoication(walletName);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * ID : TC_ECONET_0011
     * DESC : To verify that the Superadmin should be able to associate new wallet with MFS Provider    .
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0011() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0011", "To verify that the Superadmin should be able to associate new wallet with MFS Provider.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            Login.init(t1).login(netAdmin);
            AccountManagement.init(t1).addWallet(walletName, true);
            Login.init(t1).loginAsSuperAdmin("MFSMD");
            CurrencyProviderMapping.init(t1).
                    initiateMFSProviderWalletMapping(DataFactory.getDefaultProvider().ProviderName, walletName);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0004
     * DESC : To verify that the Network Admin should be able to associate or de-associate services of wallet.
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0004() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0004", "To verify that the Network Admin should be able to associate or de-associate services of wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(netAdmin);
            AccountManagement.init(t1).addWallet(walletName, true);

            Login.init(t1).loginAsSuperAdmin("MFSMD");
            CurrencyProviderMapping.init(t1).associateServicesWithWallet(walletName, DataFactory.getDefaultProvider().ProviderName);
            CurrencyProviderMapping.init(t1).deAssociateServicesWithWallet(walletName, Services.P2P, DataFactory.getDefaultProvider().ProviderName);

            Login.init(t1).loginAsSuperAdmin(saModifyWallet);
            CurrencyProviderMapping.init(t1).deleteMFSProviderWalletMapping(DataFactory.getDefaultProvider().ProviderName, walletName);

        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0002
     * DESC : To verify that Network Admin should be able to view/delete/modify the Mwallet successfully.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0002() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0002", "To verify that Network Admin should be able to view/delete/modify the Mwallet successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_WALLET);
            AccountManagement.init(t1).addWallet(walletName, true);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.VIEW_WALLET);
            AccountManagement.init(t1).viewWallet(walletName);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.MODIFY_WALLET);
            AccountManagement.init(t1).modifyWallet(walletName, newWalletName);
            Login.init(t1).loginAsOperatorUserWithRole(Roles.DELETE_WALLET);
            AccountManagement.init(t1).deleteWallet(newWalletName);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_ECONET_0008
     * DESC : To verify that Superadmin is able to Add wallet with existing name of a deleted mwallet name successfully.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0008() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0008",
                "To verify that Network Admin is able to Add wallet with existing name of a deleted mwallet name successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        String walletName = MobiquityGUIQueries.getDeletedWallet();

        try {
            Login.init(t1).login(netAdmin);
            AccountManagement.init(t1).addWallet(walletName, true);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : NEGATIVE
     * ID : TC_ECONET_0003
     * DESC : To verify that User other than Network Admin should not be able to view/delete/modify the Mwallet successfully.
     *
     * @throws Exception
     */
    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0003() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0003", "To verify that User other than superadmin Admin and Network Admin should not be able to view/delete/modify the Mwallet successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        User chnlUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        try {
            Login.init(t1).login(chnlUser);
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_VIEW");
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_MOD");
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_DEL");

            OperatorUser optUsr = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(optUsr);
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_VIEW");
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_MOD");
            AccountManagement.init(t1).verifyLink("Multiple Wallet Management", "MLTPLWALLT_ALL", "MLTPLWALLT_MLTPLWALLT_DEL");
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * TEST : NEGATIVE
     * TC ID : TC_ECONET_0005
     * DESC : To verify that the User other than NetworkAdmin should not be able to associate or de-associate services of wallet.
     *
     * @throws Exception
     */
    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0005() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0005", "To verify that the User other than Network Admin should not be able to associate or de-associate services of wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(chUser);
            AccountManagement.init(t1).verifyLink("MFS Provider Wallet Type Master", "MFSWTM_ALL");
            Login.init(t1).loginAsSuperAdmin(Roles.ADD_WALLET);
            AccountManagement.init(t1).verifyLink("MFS Provider Wallet Type Master", "MFSWTM_ALL");
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * TC ID : TC_ECONET_0007
     * TC DESC : To verify that Network Admin is not able to Add wallet with the same name of already existing wallet.
     *
     * @throws Exception
     */
    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0007() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0007", "To verify that Network Admin is not able to Add wallet with the same name of already existing wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(netAdmin);
            startNegativeTest();
            AccountManagement.init(t1).addWallet(normalWallet);
            Assertion.verifyErrorMessageContain("walletName.is.alreadyExisting", "Verify Error Message", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0009
     * DESC : To verify that Two digit numeric no should get generated for each new wallet creation.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0009() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0009", "To verify that Two digit numeric no should get generated for each new wallet creation.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        String walletName1 = "Wallet" + DataFactory.getRandomNumberAsString(4);
        String walletName2 = "Wallet" + DataFactory.getRandomNumberAsString(3);
        String walletID1, walletID2;
        try {
            Login.init(t1).login(netAdmin);
            walletID1 = AccountManagement.init(t1).addWallet(walletName1, true);
            walletID2 = AccountManagement.init(t1).addWallet(walletName2, true);
            Assertion.verifyNotEqual(walletID1, walletID2, "Verify Unique WalletID Generated.", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * BLOCKED CASE
     * TEST : NEGATIVE
     * ID : TC_ECONET_0010
     * DESSC : To verify that the Proper error message should get generated on screen if user add new wallet and count reaches to configured new wallet count.
     *
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0010() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0010", "To verify that the Proper error message should get generated on screen if user add new wallet and count reaches to configured new wallet count.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        String walletName1 = "Wala" + DataFactory.getRandomNumberAsString(7);
        String walletName2 = "Walb" + DataFactory.getRandomNumberAsString(7);
        try {
            Login.init(t1).login(netAdmin);
            startNegativeTest();
            AccountManagement.init(t1).addWallet(walletName1, true);
            AccountManagement.init(t1).addWallet(walletName2);
            Assertion.verifyErrorMessageContain("walletName.is.limitAlreadyExceeded", "Verify Unique WalletID Generated.", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * TEST : NEATIVE
     * ID : TC_ECONET_0012
     * DESC : To verify that the 'Network Admin' should not be able to delete default/Normal wallet.
     *
     * @throws Exception
     */
    @Test(priority = 10, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0012() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0012", "To verify that the 'Network Admin' should not be able to delete default/Normal wallet.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
        String walletID = DataFactory.getWalletId(normalWallet);

        try {
            Login.init(t1).login(netAdmin);
            DeleteWallet_Page1 page1 = new DeleteWallet_Page1(t1);
            page1.navDeleteWalletLink();
            boolean found = page1.selectRadioButton(DataFactory.getWalletId(normalWallet));
            if (found == false) {
                t1.pass("System Not Allow to Select Default Wallet for delete Operation.");
            } else {
                page1.selectRadioButton(walletID);
                page1.clickOnDeleteButton();
                page1.clickOnConfirmButton();
                String expectedMessage = MessageReader.getDynamicMessage("wallet.default.master.delete", normalWallet, walletID);
                Assertion.verifyEqual(Assertion.getErrorMessage(), expectedMessage, "Verify Error Message.", t1);
            }

            Utils.captureScreen(t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0013
     * DESC : To verify that the Money from User automatically should get transfer to Mwallet from Primary wallet of user in case of User deletion.
     *
     * @throws Exception
     */
    @Test(enabled = true, priority = 12, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0013() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0013",
                "To verify that the Money from User automatically should get transfer to Mwallet from Primary wallet of user in case of User deletion.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs,true,false);

            BigDecimal sub1Prebalance = MobiquityGUIQueries.getUserBalance(subs, null, null).Balance;

            TransactionManagement.init(t1)
                    .makeSureLeafUserHasBalance(subs, new BigDecimal(Constants.MIN_TRANSACTION_AMT));

            BigDecimal prePayerBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);

            HashMap<String, String> users = new HashMap<>();
            users.put(subs.MSISDN, "SUBSCRIBER");

            String fileName = BulkUserDeletion.init(t1).generateFileForDeletion(users);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.BULK_USER_DELETION);
            BulkUserDeletion.init(t1).performBulkUserDeletion(fileName);

            DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, Constants.SUBSCRIBER), "N", "Verification of user status", t1);
            BigDecimal sub1postbalance = (MobiquityGUIQueries.getUserBalanceAfterAccountIsClosed(subs, Constants.NORMAL_WALLET, defaultProvider.ProviderName)).Balance;
            Assertion.verifyEqual(sub1Prebalance, sub1postbalance, "Verification of User Pre & Post Balance:", t1);
            BigDecimal postPayerBal = MobiquityDBAssertionQueries.getWalletBalance(Constants.WALLET_101);
            DBAssertion.verifyPrePostBalanceNotEqual(prePayerBal, postPayerBal, "Verification of  Operator Pre & Post Balance:", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 13, groups = {FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0014_a() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0014_a",
                "To verify that Bulk user deletion should be successful even if users are barred in the system.")
                .assignCategory(FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);


        try {
            user = CommonUserManagement.init(t1).getDefaultSubscriber(null);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.BAR_USER);

            ChannelUserManagement.init(t1).barChannelUser(user, Constants.USER_TYPE_SUBS, Constants.BAR_AS_SENDER);

            BulkUserDeletion bulkDelObj = BulkUserDeletion.init(t1);

            HashMap<String, String> users = new HashMap<>();
            users.put(user.MSISDN, "SUBSCRIBER");

            String fileName = bulkDelObj.generateFileForDeletion(users);
            //Login with User with access to Bulk User Deletion
            Login.init(t1).loginAsOperatorUserWithRole(Roles.BULK_USER_DELETION);

            bulkDelObj.performBulkUserDeletion(fileName);

            //Verify User status after Delete
            DBAssertion.verifyDBAssertionEqual(MobiquityGUIQueries.getUserStatus(user.MSISDN, Constants.SUBSCRIBER), "N", "Verification of user status", t1);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * TEST : POSITIVE
     * ID : TC_ECONET_0636
     * DESC : To verify that the Proper error message should get displayed if user perform delete wallet having some balance in it.
     *
     * @throws Exception
     */
    @Test(priority = 13, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0636() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0636",
                "To verify that the Proper error message should get displayed if user perform delete wallet having some balance in it.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ACCOUNT_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        User subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        User channelUser = DataFactory.getChannelUserWithAccess(Roles.MODIFY_SUBSCRIBER);

        try {
            Login.init(t1).loginAsChannelUserWithRole(Roles.CASH_IN);

            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subscriber);

            startNegativeTestWithoutConfirm();

            SubscriberManagement.init(t1).modifyWalletStatusOfSubscriber(channelUser, subscriber,defaultWallet.WalletName,Constants.STATUS_DELETE);

            Assertion.verifyErrorMessageContain("can.not.delete.wallet.balance.not.zero", "Verify Error Message.", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }


}
