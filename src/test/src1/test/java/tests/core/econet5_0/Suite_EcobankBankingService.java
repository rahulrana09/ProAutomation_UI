package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Bank;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page4;
import framework.pageObjects.userManagement.AddChannelUser_pg5;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_EcobankBankingService extends TestInit {

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECOBANK_BANKING_SERVICES, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0576() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0576", "To verify that the After adding new bank , " +
                "automatic creation of Internal Bank Account should happen");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECOBANK_BANKING_SERVICES, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);

        try {

            Login.init(t1).loginAsSuperAdmin("BANK_ADD");

            String bankName = "CBZ" + DataFactory.getRandomNumberAsString(4);
            //
            Bank bank = new Bank(defaultProvider.ProviderId,
                    "CBZ" + DataFactory.getRandomNumberAsString(4),
                    null,
                    null,
                    false,
                    false);

            CurrencyProviderMapping.init(t1).addBank(bank);

            String bankNumber = MobiquityGUIQueries.getWalletNumber(MobiquityGUIQueries.getBankId(bankName));

            t1.info("Bank A/c No. :" + bankNumber);

            if (bankNumber.contains("IND04")) {
                t1.pass("Internal Bank Account created with A/c No.:" + bankNumber);
            } else {
                t1.fail("Internal Bank Account not created");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1156() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1156",
                "To verify that the Valid user should able to Add Bank Details of Subscriber via WEB.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.SARASWATHI);

        try {
            //Create a Subscriber Without Bank
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            //Add Bank to Subscriber through Modification

            SubscriberManagement.init(t1).initiateSubscriberModification(subs);
            ModifySubscriber_page2 page = new ModifySubscriber_page2(t1);
            page.clickOnNextPg2();
            ModifySubscriber_page4 page1 = new ModifySubscriber_page4(t1);
            page1.clickOnConfirmPg4().clickSubmitPg4().addMoreBank();

            AddChannelUser_pg5 page2 = new AddChannelUser_pg5(t1);
            page2.addBankPreferences(subs);
            page1.clickFinalConfirm();
            page1.finalConfirm1();

            //Modification Approval

            User usrSubsModifyApp = DataFactory.getChannelUserWithAccess("SUBSMODAP");
            Login.init(pNode).login(usrSubsModifyApp);
            SubscriberManagement.init(t1).modifyApprovalSubs(subs);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }
}
