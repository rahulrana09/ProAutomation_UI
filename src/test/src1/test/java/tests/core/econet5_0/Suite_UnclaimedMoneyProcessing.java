package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.getBalanceForOperatorUsersBasedOnUserId;
import static framework.util.jigsaw.CommonOperations.setUMSProperties;

public class Suite_UnclaimedMoneyProcessing extends TestInit {
    private String pref, defaultValue;

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.UNCLAIMED_MONEY, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1178() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1178",
                " To verify that Money lying in IND02 due to Bulk payment on the Fly and Bulk Payment " +
                        "to Unregistered is not marked as unclaimed money");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.UNCLAIMED_MONEY, FunctionalTag.ECONET_UAT_5_0);
        try {

            User new_subs = new User(Constants.SUBSCRIBER);
            BigDecimal txnAmt = new BigDecimal(10);
            OperatorUser operator = new OperatorUser(Constants.OPERATOR);
            User subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            pref = MobiquityGUIQueries
                    .fetchDefaultValueOfPreference("DOMESTIC_REMIT_WALLET_PAYID");

            defaultValue = MobiquityGUIQueries
                    .fetchmodifyAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID");

            //Make sure subscriber has Enough Balance to perform P2P
            TransactionManagement.init(t1)
                    .makeSureLeafUserHasBalance(subscriber);

            //P2P NON-REG transfer rule
            ServiceCharge tRule = new ServiceCharge(Services.P2PNONREG, subscriber, operator,
                    null, null, null, null);

            TransferRuleManagement.init(t1)
                    .configureTransferRule(tRule);

            // Set the Preference Value to 'Y'
            MobiquityGUIQueries
                    .updateDisplayAndModifiedAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID", "Y");

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", "12");

            //ALLOW UNKNOWN PARTY IN THE NETWORK
            setUMSProperties(of(
                    "ignoreUnknownPartyNetwork", true));

            //check the balance in IND02 wallet
            UsrBalance initialBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");
            int preBalance = initialBalanceIND02.Balance.intValue();
            t1.info("pre Normal balance: " + preBalance);

            //check the balance in IND06 wallet (Breakage wallet)
            UsrBalance preP2PBalanceIND06 = getBalanceForOperatorUsersBasedOnUserId("101IND02");
            int preBwalletBalance = preP2PBalanceIND06.Balance.intValue();
            t1.info("Breakage wallet balance: " + preBwalletBalance);

            //perform a p2p non reg transaction
            SfmResponse response = Transactions.init(t1)
                    .p2pNonRegTransaction(subscriber, new_subs, txnAmt);

            t1.info("Message of P2P non reg :" + response.getMessage());
            Assertion.verifyEqual(response.Status, "SUCCEEDED", "Verify P2P Status", t1);

            //check the balance in IND02 wallet
            UsrBalance postP2PBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");
            int postBalance = postP2PBalanceIND02.Balance.intValue();
            t1.info("post Normal balance: " + postBalance);

            //check the balance in IND06 wallet (Breakage wallet) is not credited
            UsrBalance postP2PBalanceIND06 = getBalanceForOperatorUsersBasedOnUserId("101IND02");
            int postBwalletBalance = postP2PBalanceIND06.Balance.intValue();
            t1.info("Breakage wallet balance: " + postBwalletBalance);
            Assertion.verifyEqual(preBwalletBalance, postBalance, "not unclaimed money", t1);

            //assert that IND02 wallet is credited
            BigDecimal amount = initialBalanceIND02.Balance.subtract(postP2PBalanceIND02.Balance);

            Assertion.verifyAccountIsCredited(initialBalanceIND02.Balance,
                    postP2PBalanceIND02.Balance, amount,
                    "verify that IND02 wallet is credited", t1);

            //create a subscriber
            SubscriberManagement.init(t1)
                    .createDefaultSubscriberUsingAPI(new_subs);

            //check the balance in IND02 wallet
            UsrBalance postUserCreationBalanceIND02 = getBalanceForOperatorUsersBasedOnUserId("101IND02");
            int postWBalance2 = postUserCreationBalanceIND02.Balance.intValue();
            t1.info("pre Normal balance: " + postWBalance2);

            //assert that IND02 wallet is debited i.e. hence not marked as Unclaimed money
            Assertion.verifyEqual(initialBalanceIND02.Balance,
                    postUserCreationBalanceIND02.Balance,
                    "Verify that IND02 wallet balance is debited", t1);

            UsrBalance subsBalance = MobiquityGUIQueries
                    .getUserBalance(new_subs, null, null);

            //assert that subscriber balace is same as txnAmount
            Assertion.verifyEqual(subsBalance.Balance, txnAmt,
                    "verify that subscriber balance is same as txnAmount", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("DOMESTIC_REMIT_WALLET_PAYID", pref);

            MobiquityGUIQueries
                    .updateDisplayAndModifiedAllowedFromGUI("DOMESTIC_REMIT_WALLET_PAYID", defaultValue);

        }
    }

}

