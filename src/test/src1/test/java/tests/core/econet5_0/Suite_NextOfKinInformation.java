package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.KinUser;
import framework.entity.User;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.HashMap;


public class Suite_NextOfKinInformation extends TestInit {

    private String prefValue;
    private boolean isPrefModified = false;

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.NEXT_OF_KIN_INFORMATION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_UAT_0526() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0526", "To verify that Next of Kin information is captured during subscriber registration which is the nominee in case of death.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.NEXT_OF_KIN_INFORMATION, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0).assignAuthor(Author.PUSHPALATHA);

        try {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_AGE_LIMIT_REQUIRE", "Y");

            User subsWithKin = new User(Constants.SUBSCRIBER);
            KinUser kin = new KinUser();
            subsWithKin.setKinUser(kin);
            SubscriberManagement.init(t1).createSubscriber(subsWithKin);

            HashMap<String, String> kinInfo = MobiquityGUIQueries.getKinDetailsForUser(subsWithKin.MSISDN);

            Assertion.verifyEqual(kinInfo.get("Kin_FN"), kin.FirstName, "Kin FirstName is captured", t1);
            Assertion.verifyEqual(kinInfo.get("Kin_LN"), kin.LastName, "Kin LastName is captured", t1);
            Assertion.verifyEqual(kinInfo.get("Kin_MN"), kin.MiddleName, "Kin MiddleName is captured", t1);
            Assertion.verifyEqual(kinInfo.get("Kin_RelationShip"), kin.RelationShip, "Kin Relationship is captured", t1);
            Assertion.verifyEqual(kinInfo.get("Kin_ContactNo"), kin.ContactNum, "Kin Contact Number is captured", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_AGE_LIMIT_REQUIRE", "N");
        }
        Assertion.finalizeSoftAsserts();
    }
}
