package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bulkPayoutToolManagement.BulkPayoutTool;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

public class Suite_BulkPayout_02 extends TestInit {

    private User subscriber, sub2, barAsSender, barredAsReciever;
    private OperatorUser netAdmin, bulkdashboard;

    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "BulkPayoutTool");
        try {

            netAdmin = DataFactory.getOperatorUserWithAccess("BULK_INITIATE");
            subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            sub2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            barAsSender = CommonUserManagement.init(eSetup)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_SENDER, null);

            barredAsReciever = CommonUserManagement.init(eSetup)
                    .getBarredUser(Constants.WHOLESALER, Constants.BAR_AS_RECIEVER, null);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1171() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1171 ", "To verify that system should not be able " +
                "to perform the CashIN service through Bulk payout tool when Channel User(Payer) is barred as Sender");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0);
        try {

            //perform o2c transaction
            TransactionManagement.init(t1).makeSureChannelUserHasBalance(barAsSender, new BigDecimal(110));

            //Generate the csv file
            String fileName = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(barAsSender.MSISDN, subscriber.MSISDN);

            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_CASHIN, fileName);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY,
                    batchId, false, false);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_PAYOUT_TOOL, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1170() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1170", "To verify that system should be able to perform" +
                " the P2P Transfer service through Bulk payout tool when  subscriber (Payer) is barred as Receiver.");

        try {

            //Generate the csv file with Payer who is barred as Reciever
            String fileName1 = BulkPayoutTool.init(t1).generateNewBulkCashinCsvFile(barredAsReciever.MSISDN, sub2.MSISDN);

            //Initiate bulk payout for Cashin
            String batchId = BulkPayoutTool.init(t1)
                    .initiateNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY, fileName1);

            //Approve bulk payout for P2P
            BulkPayoutTool.init(t1).approveRejectNewBulkPayout(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY,
                    batchId, true);

            //check for the status in BulkPayout Dashboard screen
            BulkPayoutTool.init(t1).verifyBulkPayoutDashboard(Constants.BULK_PAYOUT_SERVICE_P2P_SENDMONEY,
                    batchId, false, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }
}
