package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.billerManagement.BillerManagement;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import framework.util.propertiesManagement.MessageReader;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by prashant.kumar on 10/5/2018.
 */
public class Suite_Billers_AssociationDisassociation extends TestInit {

    private User subsNew;
    private Biller biller;
    private OperatorUser netAdmin, naUtilBillReg;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        ExtentTest prerequisite = pNode.createNode("Setup");
        try {

            netAdmin = DataFactory.getOperatorUserWithAccess("BILLUP");
            naUtilBillReg = DataFactory.getOperatorUserWithAccess("UTL_BILREG");
            subsNew = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(prerequisite).createSubscriberDefaultMapping(subsNew, true, false);
            TransactionManagement.init(prerequisite).makeSureLeafUserHasBalance(subsNew, new BigDecimal(Constants.MIN_CASHIN_AMOUNT));

            biller = BillerManagement.init(prerequisite)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void tc1() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0522", "To verify that subscriber Biller Association is working properly through web.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);
        t1.assignAuthor(Author.PRASHANT);
        try {
            ServiceCharge sCharge = new ServiceCharge(Services.UTILITY_REGISTRATION, subsNew, netAdmin, null, null, null, null);
            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(sCharge);

            // Configure Non Financial Service Charge for RETAILER_BILLPAY
            ServiceCharge sCharge1 = new ServiceCharge(Services.RETAILER_BILLPAY, subsNew, netAdmin, null, null, null, null);
            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(sCharge1);

            biller = BillerManagement.init(t1)
                    .getBillerFromAppData(Constants.BILL_PROCESS_TYPE_OFFLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.addBillForCustomer(subsNew.MSISDN, DataFactory.getRandomNumberAsString(5));

            CustomerBill bill = new CustomerBill(biller, subsNew.MSISDN, DataFactory.getRandomNumberAsString(5), "Test Bill");

            Login.init(t1).login(naUtilBillReg);

            BillerManagement.init(t1).initiateSubscriberBillerAssociationWithoutBill(biller, bill);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void tc2() throws IOException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0523", "To verify that Bulk Subscriber Biller Association is successful if all the mandatory details are filled in uploaded file.")
                .assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT).assignAuthor(Author.PRASHANT);
        try {

            String accNum = DataFactory.getRandomNumberAsString(5);
            Biller biller = BillerManagement.init(t1).getBillerFromAppData(Constants.BILL_PROCESS_TYPE_ONLINE, Constants.BILL_SERVICE_LEVEL_PREMIUM);

            biller.addBillForCustomer(subsNew.MSISDN, accNum);
            Login.init(t1).login(netAdmin);
            BillerManagement.init(t1)
                    .initiateBulkBillerRegistration(biller)
                    .downloadBulkRegistrationLogFile();

            String logMessage = BillerManagement.getLogEntry(accNum);
            Assertion.verifyContains(logMessage, MessageReader.getDynamicMessage("bulk.biller.association.success.message", subsNew.MSISDN),
                    "Verify Subscriber Biller Association for accnum:" + accNum, t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT})
    public void tc3() throws IOException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0524", "To verify that subscriber Biller Disassociation is working properly through web.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0, FunctionalTag.PVG_SYSTEM, FunctionalTag.BILLER_CATEGORY_MANAGEMENT);
        t1.assignAuthor(Author.PRASHANT);
        try {


            // Configure Non Financial Service Charge for DELETE_SUBSCRIBER_BILLER_ASSOCIATION
            ServiceCharge delSubsReg = new ServiceCharge(Services.DELETE_ASSOCIATION, subsNew, naUtilBillReg, null, null, null, null);
            ServiceChargeManagement.init(t1).configureNonFinancialServiceCharge(delSubsReg);


            String billAccNumber = DataFactory.getRandomNumberAsString(5);

            // add above bill to the Biller object
            biller.addBillForCustomer(subsNew.MSISDN, billAccNumber);

            // Login as Operator user with Bill Registration Role
            Login.init(t1)
                    .login(naUtilBillReg);

            // Initiate Subscriber Biller Association
            BillerManagement.init(t1)
                    .initiateSubscriberBillerAssociation(biller);

            CustomerBill bill = biller.getAssociatedBill();

            BillerManagement.init(t1)
                    .deleteSubsBillerAssociation(bill);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
}

