package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg1;
import framework.pageObjects.bankAccountAssociation.BankAccountRegistration_Pg2;
import framework.pageObjects.bankAccountDisaccociation.BankAccountDisassociate_page1;
import framework.pageObjects.subscriberManagement.*;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Nirupama MK on 21/08/2018.
 */

public class Suite_SubscriberManagement_01 extends TestInit {

    private User chanlUser, subs;
    // private BigDecimal operatorBalance;
    private OperatorUser optUser, bankApprover;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "SingleMSISDN_MultipleUserType");

        try {
            chanlUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            optUser = DataFactory.getOperatorUserWithAccess("SUBS_BNK_ASSOC");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            User existingUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);

            ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, existingUser,
                    new OperatorUser(Constants.OPERATOR), null,
                    null, null, null);

            ServiceChargeManagement.init(eSetup)
                    .configureServiceCharge(sCharge);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0499() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0499",
                "To verify that the Subscriber should be able to add new bank account without any validation with Customer Bank a/c Validation (Real Time).");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);
        String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");

        try {
            //Setting preference BANK_ACC_LINKING_VIA_MSISDN to Y

            if (prefValue.equalsIgnoreCase("N")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "Y");
            }

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);
            Login.init(t1).login(optUser);
            BankAccountRegistration_Pg1 page1 = new BankAccountRegistration_Pg1(t1);
            page1.navigateToLink();
            page1.selectProvider("MFS1");
            page1.setMSISDNPrefN(subs.MSISDN);
            page1.selectUserType(subs.CategoryCode);
            page1.clickSubmitBtnPrefN();
            BankAccountRegistration_Pg2 page2 = new BankAccountRegistration_Pg2(t1);
            page2.clickAddMoreBtn();
            page2.selectLinkedBank(DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId));
            page2.clickSubmitBtn();
            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1).approveDefaultBank(subs);
            BankAccountDisassociate_page1 Disassociate_page1 = new BankAccountDisassociate_page1(t1);
            Login.init(t1).login(optUser);
            Disassociate_page1.navigateToLink();
            Disassociate_page1.selectProvider("MFS1");
            Disassociate_page1.setMSISDN(subs.MSISDN);
            Disassociate_page1.selectUser("SUBSCRIBER");
            Disassociate_page1.clickSubmit();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", prefValue);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0326() throws Exception {

        ExtentTest node = pNode.createNode("TC_ECONET_0326", "To verify that " +
                "Subscriber Account Closure should not be successful if Amount present in wallets " +
                "for an MFS Provider is less than the Service charge applicable for Account Closure.");

        node.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {

            Login.init(node).login(chanlUser);
            //todo uncomment
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(node).createSubscriberDefaultMapping(subs, true, false);

            User delSubs = DataFactory.getChannelUserWithAccess("SUBSDEL");

            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, subs, delSubs, null, null, null, null);
            ServiceChargeManagement.init(node)
                    .configureServiceCharge(sChargeAccClose);

            startNegativeTest();
            //Delete initiate
            SubscriberManagement.init(node)
                    .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);

            Assertion.verifyErrorMessageContain("subs.transaction.fail.insufficient.funds",
                    "Subscriber Cannot be deleted As User doesn't Have Sufficient Fund", node);

        } catch (Exception e) {
            markTestAsFailure(e, node);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0328() throws Exception {

        ExtentTest TC_ECONET_0328 = pNode.createNode("TC_ECONET_0328", "To verify that while " +
                "successful Subscriber Account Closure, account is then deleted and subscriber receives " +
                "SMS for the same");
        TC_ECONET_0328.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(TC_ECONET_0328).login(chanlUser);
            User subs = CommonUserManagement.init(TC_ECONET_0328).getDefaultSubscriber(null);
            TransactionManagement.init(TC_ECONET_0328).performCashIn(subs, "4");

            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, optUser, null, null, null, null);
            ServiceChargeManagement.init(TC_ECONET_0328)
                    .configureServiceCharge(sChargeAccClose);
            //Delete initiate
            String txnid = SubscriberManagement.init(TC_ECONET_0328)
                    .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);


            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode),
                    "N", "Verify User Status in DB", TC_ECONET_0328);
            if (AppConfig.isAccountClosureTwoStep) {
                Transactions.init(TC_ECONET_0328).accountClosurebyAgentConfirmation(subs.MSISDN, txnid);
            }

            Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode),
                    "N", "Verify User Status in DB", TC_ECONET_0328);


            SMSReader.init(TC_ECONET_0328).verifyNotificationContain(subs.MSISDN, "account.close.success.subs", subs.MSISDN);


        } catch (Exception e) {
            markTestAsFailure(e, TC_ECONET_0328);

        }

        Assertion.finalizeSoftAsserts();

    }


    /**
     * Two cases are done in single method as it req only one wallet and we are deleting one wallet in above case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0321_1() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0321",
                "To verify Wallet except Normal wallet should get deleted through Modification Subscriber Module.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            List<String> walletForSubs = DataFactory.getWalletForSubscriberUserCreation();
            if (walletForSubs.size() <= 1) {
                t1.skip("This feature cannot be tested as only default wallet is mentioned in ConfigInput. At least " +
                        "2 Wallets for Subscriber is required !!!");
                return;
            }

            User subs = new User(Constants.SUBSCRIBER);
            User modifyUser = DataFactory.getChannelUserWithAccess("SUBSMOD");

            SubscriberManagement.init(t1).createSubscriber(subs);

            Login.init(t1).login(modifyUser);
            ModifySubscriber_page1 modifySubscriberPage1 = ModifySubscriber_page1.init(t1);
            ModifySubscriber_page2 modifySubscriberPage2 = ModifySubscriber_page2.init(t1);
            ModifySubscriber_page3 modifySubscriberPage3 = ModifySubscriber_page3.init(t1);
            ModifySubscriber_page4 modifySubscriberPage4 = ModifySubscriber_page4.init(t1);
            modifySubscriberPage1.navSubscriberModification();
            modifySubscriberPage1.setMSISDN(subs.MSISDN);
            modifySubscriberPage1.clickOnSubmitPg1();
            modifySubscriberPage2.clickOnNextPg2();
            modifySubscriberPage3.clickOnNextPg3();

            String walletNameOtherThanNormal = null;
            for (int i = 0; i < walletForSubs.size(); i++) {
                if (!walletForSubs.get(i).equalsIgnoreCase(Wallets.NORMAL)) {
                    walletNameOtherThanNormal = walletForSubs.get(i);
                    break;
                }
            }
            modifySubscriberPage4.selectWalletStatus(walletNameOtherThanNormal, Constants.STATUS_DELETE);
            modifySubscriberPage4.clickSubmitPg4();
            modifySubscriberPage4.clickOnConfirmPg4();
            modifySubscriberPage4.clickFinalConfirm();
            Login.init(t1).login(modifyUser);
            SubscriberManagement.init(t1).modifyApprovalSubs(subs);

           /*
            modifySubscriberPage4.selectLinkedBank1Status();
            modifySubscriberPage4.selectLinkedBank2Status();
*/


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Two cases are done in single method as it req only one wallet and we are deleting one wallet in above case
     *
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0324() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0324",
                "To verify that Subscriber Account Closure should be successful if Subscriber  has not changed his PIN first time as well after registration.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            User subs = new User(Constants.SUBSCRIBER);
            User chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            User chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
            OperatorUser bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");

            for (String provider : DataFactory.getAllProviderNames()) {
                ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, chAddSubs, null, null, provider, provider);
                ServiceChargeManagement.init(t1)
                        .configureServiceCharge(sCharge);
            }

            // Login as User having Subscriber Creation Rights
            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(subs);

            if (ConfigInput.isCoreRelease) {
                CommonUserManagement.init(t1).assignWebGroupRole(subs);
            }
            AddSubcriber_Page3 page3 = new AddSubcriber_Page3(t1);
            page3.selectGrade();
            Thread.sleep(Constants.WAIT_TIME);
            page3.clickSubmit();
            // page3.selectBankStatus();
            page3.clickSubmit2();
            page3.clickConfirm();
            // Add Approval
            if (AppConfig.isSubsMakerCheckerAllowed) {
                // Channel User has to approve the subscriber
                Login.init(t1).login(chApproveSubs);
                SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
            }

            // Approve All banks associated with the Subscriber
         /*   Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .approveAllAssociatedBanks(subs);*/

            if (AppConfig.isTwoStepRegistrationForSubs) {
                Transactions.init(t1)
                        .subscriberAcquisition(subs);
            }
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, BigDecimal.valueOf(30));
            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, new OperatorUser(Constants.OPERATOR), null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(sChargeAccClose);

            SubscriberManagement.init(t1).deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0319() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0319",
                "To verify that the During modify subscriber , changing the mobile number of the subscriber is changed, a notification is sent to both the old and the new Mobile numbers indicating the change.");
        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);

        try {
            User subs = new User(Constants.SUBSCRIBER);
            User chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            User chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
            OperatorUser bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");

            for (String provider : DataFactory.getAllProviderNames()) {
                ServiceCharge sCharge = new ServiceCharge(Services.ACQFEE, subs, chAddSubs, null, null, provider, provider);
                ServiceChargeManagement.init(t1)
                        .configureServiceCharge(sCharge);
            }

            // Login as User having Subscriber Creation Rights
            Login.init(t1).login(chAddSubs);

            SubscriberManagement.init(t1).addInitiateSubscriber(subs);

            if (ConfigInput.isCoreRelease) {
                CommonUserManagement.init(t1).assignWebGroupRole(subs);
            }
            AddSubcriber_Page3 page3 = new AddSubcriber_Page3(t1);
            page3.selectGrade();
            Thread.sleep(Constants.WAIT_TIME);
            page3.clickSubmit();
            // page3.selectBankStatus();
            page3.clickSubmit2();
            page3.clickConfirm();
            // Add Approval
            if (AppConfig.isSubsMakerCheckerAllowed) {
                // Channel User has to approve the subscriber
                Login.init(t1).login(chApproveSubs);
                SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
            }

            // Approve All banks associated with the Subscriber
         /*   Login.init(t1).login(bankApprover);
            CommonUserManagement.init(t1)
                    .approveAllAssociatedBanks(subs);*/

            if (AppConfig.isTwoStepRegistrationForSubs) {
                Transactions.init(t1)
                        .subscriberAcquisition(subs);
            }
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(subs, BigDecimal.valueOf(30));
            ServiceCharge sChargeAccClose = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, new OperatorUser(Constants.OPERATOR), null, null, null, null);
            ServiceChargeManagement.init(t1).configureServiceCharge(sChargeAccClose);

            SubscriberManagement.init(t1).deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }



    /*
     @Test(groups = { FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0323() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0323",
                "To verify that Subscriber Account Closure should not be successful if P2P to non registered service (On the fly wallet amount) for all wallets against  MFS Provider which have not been withdrawn.");
        t1.assignCategory( FunctionalTag.ECONET_UAT_5_0);

        try {
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriber(subs);
            Transactions.init(t1).p2pNonRegTransaction(subs,new User(Constants.SUBSCRIBER), BigDecimal.valueOf(10));
            startNegativeTest();
            SubscriberManagement.init(t1).deleteSubscriber(subs,DataFactory.getDefaultProvider().ProviderId);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }
*/
   /* @AfterClass(alwaysRun = true)
    public void postCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "SingleMSISDN_MultipleUserType");
        chanlUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

                SystemPreferenceManagement.init(eSetup)
                .updateSystemPreference("ACCOUNT_CLOSURE_TWO_STEP", defPref_AccountClosure);
    }*/
}
//
//    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
//    public void TC_ECONET_0329() throws Exception {
//
//        ExtentTest TC_ECONET_0329 = pNode.createNode("TC_ECONET_0329", "BLOCK SMS tag will not be applicable if the" +
//                                                                         " Subscriber is getting deleted");
//        TC_ECONET_0329.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
//
//        try {
//            Login.init(TC_ECONET_0329).login(chanlUser);
//            TransactionManagement.init(TC_ECONET_0329).performCashIn(subs, "4");
//
//            //Delete initiate
//            String txnid = SubscriberManagement.init(TC_ECONET_0329)
//                    .deleteSubscriber(subs, DataFactory.getDefaultProvider().ProviderId);
//
//            if (txnid != null) {
//                Transactions.init(TC_ECONET_0329).accountClosurebyAgentConfirmation(subs.MSISDN, txnid);
//                Assertion.verifyEqual(MobiquityGUIQueries.getUserStatus(subs.MSISDN, subs.CategoryCode),
//                        "N", "Verify User Status in DB", TC_ECONET_0329);
//            }
//
//            String messages = MobiquityGUIQueries.getMessageFromSentSMS(subs.MSISDN, "desc");
//
//            //decrypt the messages
//            DesEncryptor d = new DesEncryptor();
//            String decryptedMsg = d.decrypt(messages);
//
//            //verify  Messages
//            Assertion.verifyMessageContain(decryptedMsg, "Subscriber Deleted",
//                    "Check SMS for Subscriber Deletion", TC_ECONET_0329);
//
//        } catch (Exception e) {
//            markTestAsFailure(e, TC_ECONET_0329);
//
//        } catch (MoneyException e) {
//            e.printStackTrace();
//        }
//
//        Assertion.finalizeSoftAsserts();
//
//    }