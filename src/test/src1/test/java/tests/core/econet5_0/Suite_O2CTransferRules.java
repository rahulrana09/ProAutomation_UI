package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import framework.entity.OperatorUser;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.pageObjects.transferRule.O2CTransferRule_Pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalVars.FunctionalTag;
import framework.util.reportManager.ScreenShot;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by prashant.kumar on 9/4/2018.
 */
public class Suite_O2CTransferRules extends TestInit {

    private static OperatorUser netAdmin, netAdminWithO2C;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        netAdmin = DataFactory.getOperatorUserWithAccess("T_RULES", Constants.NETWORK_ADMIN);
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0060() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0060", "To verify that the System will overwrite existing O2C rule for the same combination during modification.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);

        Login.init(t1).loginAsOperatorUserWithRole(Roles.O2C_TRANSFER_RULES);

        TransferRuleManagement.init(t1).setO2CTransferLimit(DataFactory.getDefaultProvider().ProviderName,
                DataFactory.getDomainName(Constants.WHOLESALER),
                DataFactory.getCategoryName(Constants.WHOLESALER),
                Constants.PAYINST_WALLET_CONST, DataFactory.getDefaultWallet().WalletName);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0058() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0058", "To verify that Users other network admin should not able to add O2C Transfer Rule.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_UAT_5_0);
        Login.init(t1).login(DataFactory.getChannelUserWithCategory(Constants.WHOLESALER));
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TRULES_ALL", "TRULES_O2C_TR");
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0763() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0763", "To verify that O2C transfer rule once created in the system, cannot be deleted, viewed or suspended.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0);
        Login.init(t1).login(netAdmin);
        try {
            O2CTransferRule_Pg1 rulePg1 = O2CTransferRule_Pg1.init(t1);
            rulePg1.navAddTransferRule();
            int view = driver.findElements(By.xpath("//body//*[contains(text(),'View')]")).size();
            int delete = driver.findElements(By.xpath("//body//*[contains(text(),'Suspend')]")).size();
            int Suspend = driver.findElements(By.xpath("//body//*[contains(text(),'Delete')]")).size();
            boolean isTrue = true;
            if (view != 0 && delete != 0 && Suspend != 0)
                isTrue = false;
            t1.info("View Transfer Rule", MediaEntityBuilder.createScreenCaptureFromPath(ScreenShot.TakeScreenshot()).build());
            if (isTrue) {
                t1.pass("View/Suspend/Delete Transfer rule text Not Found");
            } else {
                t1.fail("View/Suspend/Delete Transfer rule text Found");
                Assert.fail();
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0356() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0356", "To verify that Network admin is able to define O2C transfer rule successfully through web.");
        t1.assignCategory(FunctionalTag.O2C, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);
        netAdminWithO2C = DataFactory.getOperatorUserWithAccess("O2C_TRULES", Constants.NETWORK_ADMIN);
        Login.init(t1).login(netAdminWithO2C);
        try {
            TransferRuleManagement.init(t1).CreateO2CTRulesForCommissionWallet();
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

}
