package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.bankAccountAssociation.BankAccountAssociation;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_BankingServices extends TestInit {

    private OperatorUser usrCreator, bankApprover, modifyuser, usrApprover;
    private CurrencyProvider defaultProvider;
    private String custId, accNo, provider, bank, bank1, bankId;
    private User subs;


    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");

        try {

            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");
            bankApprover = DataFactory.getOperatorUserWithAccess("BNK_APR");
            modifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            defaultProvider = DataFactory.getDefaultProvider();
            custId = DataFactory.getRandomNumberAsString(6);
            accNo = DataFactory.getRandomNumberAsString(9);
            provider = DataFactory.getDefaultProvider().ProviderName;
            bank = DataFactory.getDefaultBankName(DataFactory.getDefaultProvider().ProviderId);
            bank1 = DataFactory.getNonTrustBankName(DataFactory.getDefaultProvider().ProviderName);
            bankId = DataFactory.getBankId(bank1);
            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            SystemPreferenceManagement.init(eSetup)
                    .updateSystemPreference("BANKING_SERVICES_FOR_TRUST_BANK", "TRUE");
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1064() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1064",
                "To verify that Following parameters should be captured while linking the Bank Account for a merchant/Agent:MFS Provider\n" +
                        "Linked Banks\n" +
                        "Mobile Group Role – Bank\n" +
                        "Grade\n" +
                        "TCP Profile Name\n" +
                        "Customer ID\n" +
                        "Account Number(MSISDN of the user)\n" +
                        "Primary Account (Y/N)\n" +
                        "Account Type\n" +
                        "Status\n");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);

        try {

            User chUser = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).initiateChannelUserModification(chUser);
            CommonUserManagement.init(t1).navModifyToBankMapping(chUser)
                    .mapBankPreferences(chUser, true);
            //Approving the User
            ChannelUserManagement.init(t1).approveChannelUserModification(chUser);
            Login.init(t1)
                    .login(bankApprover);
            CommonUserManagement.init(t1)
                    .approveAllAssociatedBanks(chUser);
            t1.pass("Bank Account is added successfully by entering all parameter details");

            ExtentTest t2 = pNode.createNode("TC_ECONET_1065", "To verify that the operator user is able to view the Banking details of a merchant/agent through the existing functionality.");

            Login.init(t1).login(modifyuser);

            ChannelUserManagement.init(t2).initiateChannelUserModification(chUser);

            CommonUserManagement.init(t2).navModifyToBankMapping(chUser);

            WebElement element = driver.findElement(By.id("walletTypeID0"));
            element.isDisplayed();

            t2.pass("Operator User is able to view Bank details of exsisting user");

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1117() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1117",
                "To verify that Single bank association is working fine through web interface");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);
        String prefValue = null;

        try {
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");
            if (prefValue.equalsIgnoreCase("Y")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            }

            Login.init(t1).login(usrCreator);
            BankAccountAssociation.init(t1).initiateBankAccountRegistration(subs, provider, bank, custId, accNo, false);
            Thread.sleep(2000);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(pNode).approveAssociatedBanksForUser(subs, provider, bank);


        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", prefValue);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1067() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1067",
                "To verify that operator is able to dissociate the Banking services for a customer/agent/merchant in bulk successfully.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BANK_MASTER, FunctionalTag.ECONET_SIT_5_0);
        String prefValue = null;

        try {
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("BANK_ACC_LINKING_VIA_MSISDN");
            if (prefValue.equalsIgnoreCase("Y")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", "N");
            }

            Login.init(t1).login(usrCreator);
            BankAccountAssociation.init(t1).initiateBankAccountRegistration(subs, provider, bank1, custId, accNo, false);
            Thread.sleep(2000);

            Login.init(t1).login(bankApprover);
            CommonUserManagement.init(pNode).approveAssociatedBanksForUser(subs, provider, bank1);

            String fileName = ChannelUserManagement.init(t1)
                    .generateBulkUserBankAssociationcsvFile(subs, subs, bankId);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1)
                    .bulkUserBankDisAssociation(fileName);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);
        } finally {
            SystemPreferenceManagement.init(t1).updateSystemPreference("BANK_ACC_LINKING_VIA_MSISDN", prefValue);
        }
        Assertion.finalizeSoftAsserts();
    }


}
