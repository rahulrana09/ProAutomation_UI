package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_SubscriberManagement_02 extends TestInit {
    String modifyAllowed, displayAllowed, defPref;
    private User chanlUser, chApproveSubs, subs, subs1, subs2;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {
        chanlUser = DataFactory.getChannelUserWithAccess("SUBSADD");
        chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

    }

    private void initAndApproveSubs(User subs, ExtentTest t1) throws Exception {

        Login.init(t1).login(chanlUser);
        SubscriberManagement.init(t1).addSubscriber(subs, false);
        Login.init(t1).login(chApproveSubs);
        SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0293() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0293", "Two Step subscriber registration:" +
                "To verify Network admin can define the time period in the system till which a " +
                "new subscriber remains in the pending state and does not respond to the 2 step subscriber " +
                "registration Push message.");

        try {
            User subs = new User(Constants.SUBSCRIBER);

            defPref = MobiquityGUIQueries.fetchDefaultValueOfPreference("TWO_STEP_SUBS_REG");
            modifyAllowed = MobiquityGUIQueries.fetchmodifyAllowedFromGUI("TWO_STEP_SUBS_REG");
            displayAllowed = MobiquityGUIQueries.fetchDisplayAllowedOnGUI("TWO_STEP_SUBS_REG");

            MobiquityGUIQueries
                    .updateDisplayAndModifiedAllowedFromGUI("TWO_STEP_SUBS_REG", "Y");

            if (AppConfig.isTwoStepRegistrationForSubs != true) {
                SystemPreferenceManagement.init(t1)
                        .updateSystemPreference("TWO_STEP_SUBS_REG", "true");
            }

            //create subscriber without Acquisition
            initAndApproveSubs(subs, t1);

            //status will be in pending until acquisition is done
            String status = MobiquityGUIQueries.fetchStatus(subs);
            Assertion.verifyEqual(status, Constants.STATUS_PENDING, "Subscriber Status is in pending", t1);

            //perform subscriber acquisition as its 2-step registration
            Transactions.init(t1).subscriberAcquisition(subs);

            //status will become active after acquisition performed
            status = MobiquityGUIQueries.fetchStatus(subs);
            Assertion.verifyEqual(status, Constants.STATUS_ACTIVE, "Subscriber Status is active now", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("TWO_STEP_SUBS_REG", defPref);

            MobiquityGUIQueries
                    .updateDisplayAndModifiedAllowedFromGUI("TWO_STEP_SUBS_REG", displayAllowed);

        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0320() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0320", "To verify that system sends notification on change of national ID to the user");

        try {
            Login.init(t1).login(chanlUser);

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            SubscriberManagement.init(t1).modifyInitiateSubscriber(chanlUser, subs, "Passport", subs.MSISDN);
            SubscriberManagement.init(t1).modifyApprovalSubs(subs);


            Login.init(t1).login(chanlUser);
            SubscriberManagement.init(t1).modifyInitiateSubscriber(chanlUser, subs, "Driver card", subs.MSISDN);
            SubscriberManagement.init(t1).modifyApprovalSubs(subs);

            MobiquityDBAssertionQueries m = new MobiquityDBAssertionQueries();
            String message = MobiquityDBAssertionQueries.getLatestSMSAppliedFromSMSDelivered(subs.MSISDN);
            DesEncryptor d = new DesEncryptor();
            String decchanacc = d.decrypt(message);

            Assert.assertEquals(decchanacc, "Your details have been modify approved succesfully.");
            t1.pass("Message is sent to user when National ID is changed");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SUBSCRIBER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0319() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0319", "To verify that the During modify subscriber , changing the mobile number of the subscriber is changed, a notification is sent to both the old and the new Mobile numbers indicating the change.");

        try {

            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "Y");
            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, false);

            String msisdn = "77" + DataFactory.getRandomNumber(8);
            Login.init(t1).login(chanlUser);
            SubscriberManagement.init(t1).modifyInitiateSubscriber(chanlUser, subs, "Passport", msisdn);
            SubscriberManagement.init(t1).modifyApprovalSubs(subs);


            MobiquityDBAssertionQueries m = new MobiquityDBAssertionQueries();
            String message = MobiquityDBAssertionQueries.getLatestSMSAppliedFromSMSDelivered(subs.MSISDN);
            DesEncryptor d = new DesEncryptor();
            String decchanacc = d.decrypt(message);

            Assert.assertEquals(decchanacc, "Your Mobiquity account has been moved from msisdn " + subs.MSISDN + " to " + msisdn + " and  mpin:0000");
            t1.pass("Message is sent to Old Msisdn");

            String message1 = MobiquityDBAssertionQueries.getLatestSMSAppliedFromSMSDelivered(msisdn);
            DesEncryptor d1 = new DesEncryptor();
            String decchanacc1 = d1.decrypt(message1);


            Assert.assertEquals(decchanacc1, "Your account modified successfully.Do use the credentials supplied at modify initiation.Please confirm it by dialling 7451.");
            t1.pass("Message is sent to New Msisdn");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_CHANGE_MSISDN_ALLOWED", "N");
        }
    }
}