package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.common.DesEncryptor;
import com.comviva.mmoney.exception.MoneyException;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Map;

public class Suite_BankCashOut_BankCashIn extends TestInit {
    @Test(priority = 1, groups = {FunctionalTag.BANK_CASH_OUT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0828() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0828",
                "To verify that Retailer is able to initiate Cash Out/Cash In from Bank Account service successfully i.e. agent transfers money from customer’s bank account to his bank account.")
                .assignCategory(FunctionalTag.BANK_CASH_OUT, FunctionalTag.ECONET_SIT_5_0);

        try {

            String defaultProvider = DataFactory.getDefaultBankIdForDefaultProvider();
            User channeluser = new User(Constants.RETAILER);

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(channeluser, true);

            User subs = new User(Constants.SUBSCRIBER);
            SubscriberManagement.init(t1).createSubscriberDefaultMapping(subs, true, true);

            ServiceCharge bankcash = new ServiceCharge(Services.BANK_CASH_OUT, subs, channeluser,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(bankcash);
            TransferRuleManagement.init(t1).configureTransferRule(bankcash);

            Map<String, String> AccountNum = MobiquityGUIQueries.dbGetAccountDetails(channeluser, DataFactory.getDefaultProvider().ProviderId, defaultProvider);

            String channaccount = AccountNum.get("ACCOUNT_NO");
            DesEncryptor d = new DesEncryptor();
            String decchanacc = d.decrypt(channaccount);
            System.out.println(decchanacc);

            Map<String, String> AccountNum1 = MobiquityGUIQueries.dbGetAccountDetails(subs, DataFactory.getDefaultProvider().ProviderId, defaultProvider);

            String subaccount = AccountNum1.get("ACCOUNT_NO");
            DesEncryptor d1 = new DesEncryptor();
            String decsubacc = d1.decrypt(subaccount);
            System.out.println(decsubacc);


            Transactions.init(t1).bankCashOutInitiate(subs, channeluser,
                    decchanacc, decsubacc, 10);

            ServiceCharge bankCashIn = new ServiceCharge(Services.BANK_CASH_IN, channeluser, subs,
                    null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(bankCashIn);
            TransferRuleManagement.init(t1).configureTransferRule(bankCashIn);

            Transactions.init(t1).bankCashInInitiate(subs, channeluser,
                    decchanacc, decsubacc, 10);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();


    }
}
