package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

public class Suite_SubscriberInfoAndSelfInfo extends TestInit {

    private User subs, subsInfoViewer;

    /**
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.SUBSCRIBER_INFORMATION, FunctionalTag.ECONET_UAT_5_0})

    public void TC_ECONET_0395() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0395", "To verify that the Wholesaler should" +
                " able to see Subscriber info by web module.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SUBSCRIBER_INFORMATION, FunctionalTag.ECONET_UAT_5_0);

        try {

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            subsInfoViewer = DataFactory.getChannelUserWithAccess("SUBS_INF");

            Login.init(t1).
                    login(subsInfoViewer);

            SubscriberManagement.init(t1).doSubsInformation(DataFactory.getDefaultProvider().ProviderId,
                    "WALLET", DataFactory.getDefaultWallet().WalletName, subs);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }
}