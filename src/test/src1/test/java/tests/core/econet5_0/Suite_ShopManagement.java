package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Febin.Thomas on 22/10/2018.
 */


public class Suite_ShopManagement extends TestInit {


    private User sub;


    @BeforeClass(alwaysRun = false)
    public void precondition() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to this Script");
        try {

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION})
    public void ShopManagement() throws Exception {
        ExtentTest t1 = pNode.createNode("Shop Management", "To verify that the Subsciber can register itself again using previously churn MSISDN of existing user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.BULK_USER_DELETION).assignAuthor(Author.FEBIN);
        try {
/*

            sub = new User(Constants.SUBSCRIBER);

            Transactions.init(t1).SubscriberRegistrationByChannelUserWithKinDetails(sub);
            Transactions.init(t1).subscriberAcquisition(sub);
*/


            sub = new User(Constants.SUBSCRIBER);
            Transactions.init(t1).SubscriberRegistrationByChannelUserWithKinDetails(sub);
            Transactions.init(t1).subscriberAcquisition(sub);

        } catch (Exception e) {

            markTestAsFailure(e, t1);
        } finally {
//            Assertion.finalizeSoftAsserts();
        }
    }


}