package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.nonFinancialServiceCharge.NonFinancialServiceCharge_Page2;
import framework.pageObjects.reconciliation.Reconciliation_Page;
import framework.pageObjects.serviceCharge.NewSubsCommissionRule_Page1;
import framework.pageObjects.serviceCharge.ServiceCharge_Pg2;
import framework.pageObjects.stockManagement.StockWithdrawal_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by Nirupama MK on 18/09/2018.
 */
public class Suite_ServiceChargeAndCommissionManagement extends TestInit {

    OperatorUser usrSChargeSuspResume;
    private String defaultProvider;

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0069() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0069", "To verify that network admin is " +
                "able to add initiate service charge  successfully for self delete.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            ServiceCharge sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, opt, null,
                    null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0749", "To verify network admin is able to " +
                    "Approve delete financial service charge successfully.");

            ServiceChargeManagement.init(t2).deleteServiceCharge(sCharge);


        } catch (Exception e) {

        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0750() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0750", "To verify User other than any operator user" +
                " is not able to Add Non financial service charge successfully.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("NONFIN_CHARGENON_FIN"));
                t1.fail("Add non-financial Service Charge link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Service Charge Initiate link is not displayed for Channel User as Expected", t1);
            }

            ExtentTest t2 = pNode.createNode("TC_ECONET_0753", "To verify User other than " +
                    "any operator user is not able to Approve Non financial service charge successfully.");

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t2).login(whs);

                driver.findElement(By.id("NONFIN_CHARGENON_APP"));
                t2.fail("non-financial Service Charge Approval link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Service Charge Approval link is not displayed for Channel User as Expected", t2);
            }

            ExtentTest t3 = pNode.createNode("TC_ECONET_1195", "To verify User other than " +
                    "any operator user is not able to view service charge calculator (NON-finalcial Charging)");

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t3).login(whs);

                driver.findElement(By.id("NONFIN_CHARGENON_CALC"));
                t3.fail("service charge calculator link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("service charge calculator link is not displayed for Channel User as Expected", t3);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0747() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0747", "To verify that user other than " +
                "network admin other users is not able to add initiate service charge successfully.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("SERVCHARG_SVC_ADD"));
                t1.fail("Service Charge Initiate link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Service Charge Initiate link is not displayed for Channel User as Expected", t1);
            }

            ExtentTest t2 = pNode.createNode("TC_ECONET_0748", "Service Charge >> " +
                    "Service Charge Approval .To verify that User other than valid user is not able " +
                    "to Approve/Reject the add initiated service charge.");

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t2).login(whs);

                driver.findElement(By.id("SERVCHARG_SVC_APP"));
                t2.fail("Service Charge Approval link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Service Charge Approval link is not displayed for Channel User as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0754() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0754", "To verify User other than NetworkAdmin" +
                " is not able to Suspend/Resume Non financial service charge successfully.").assignCategory
                (FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            fl = FunctionLibrary.init(t1);

            User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            Login.init(t1).login(whs);
            fl = FunctionLibrary.init(t1);

            fl.verifyLinkNotAvailable("NONFIN_ALL", "NONFIN_CHARGENON_SUS");

            OperatorUser cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(cce);
            fl.verifyLinkNotAvailable("NONFIN_ALL", "NONFIN_CHARGENON_SUS");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0755() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0755", "To verify User other than Network Admin " +
                "is not able to View (Non financial) service charge.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("NONFIN_CHARGENON_VIEW"));
                t1.fail("View service charge link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("View service charge link is not displayed for Channel User as Expected", t1);
            }

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("NONFIN_CHARGENON_VIEW"));
                t1.fail("View service charge link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("View service charge link is not displayed for " +
                        "Channel Admin as Expected", t1);
            }

            try {
                OperatorUser cce = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
                Login.init(t1).login(cce);

                driver.findElement(By.id("NONFIN_CHARGENON_VIEW"));
                t1.fail("View service charge link is displayed for Channel Care Executive");

            } catch (Exception e) {
                Assertion.logAsPass("View service charge link is not displayed for" +
                        " Channel Care Executive as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0090() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0090 ", "To verify that the Transaction will not be successful" +
                " in case latest service charge for the corresponding transaction is in suspended state. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        ServiceCharge accCloseAgent = null;
        User chUsr = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        User sub = new User(Constants.SUBSCRIBER);

        try {
            accCloseAgent = new ServiceCharge(Services.ACCOUNT_CLOSURE_BY_AGENT, sub, chUsr, null, null, null, null);

            ServiceChargeManagement.init(t1).configureServiceCharge(accCloseAgent);

            Login.init(t1).loginAsOperatorUserWithRole(Roles.SUSPEND_RESUME_SERVICE_CHARGE);

            ServiceChargeManagement.init(t1).suspendServiceCharge(accCloseAgent);

            SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(sub);
            //Perform Cashin for the subscriber
            TransactionManagement.init(t1).makeSureLeafUserHasBalance(sub, new BigDecimal(4));

            //Initiate Subscriber by Agent
            Login.init(t1).login(chUsr);

            startNegativeTest();

            SubscriberManagement.init(t1).
                    deleteSubscriberByAgent(sub, DataFactory.getDefaultProvider().ProviderId, false);

            Assertion.verifyErrorMessageContain("servicechargeprofile.message.suspendmessage", "Service Charge is set", t1);

            stopNegativeTest();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Login.init(t1).loginAsOperatorUserWithRole(Roles.SUSPEND_RESUME_SERVICE_CHARGE);
            ServiceChargeManagement.init(t1).resumeServiceCharge(accCloseAgent);
        }

    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0087() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0087", "To verify that the if Receiver party" +
                " is inactive/suspended in system then No service charge should get deducted on transaction.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        ServiceCharge sCharge = null;

        try {
            User c2cSender = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            UsrBalance balance = MobiquityGUIQueries.getUserBalance(c2cSender, null, null);
            BigDecimal preBalance = balance.Balance;
            t1.info("Sender Pre Balance: " + preBalance);

            User c2cReceiver = CommonUserManagement.init(t1).getSuspendedUser(Constants.WHOLESALER, null);

            sCharge = new ServiceCharge(Services.C2C, c2cSender, c2cReceiver, null,
                    null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            Login.init(t1).login(c2cSender);

            startNegativeTest();
            TransactionManagement.init(t1)
                    .performC2C(c2cReceiver, Constants.C2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            Assertion.verifyErrorMessageContain("c2c.suspendReciever", "Reciever Suspended", t1);

            balance = MobiquityGUIQueries.getUserBalance(c2cSender, null, null);
            BigDecimal postBalance = balance.Balance;
            t1.info("Sender Post Balance: " + postBalance);

            Assertion.verifyEqual(preBalance, postBalance, "No Service Charge Deducted From Sender", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    /*
     * Automated By Gurudatta Praharaj
     */
    @Test(priority = 9, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0092() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0092", "To verify that the Proper error message " +
                "should displayed on web during Non-financial service charge creation if entered commission " +
                "value is greated than service charge value. ");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        ServiceCharge balEnq = null;
        try {
            User chUser = new User(Constants.WHOLESALER);
            OperatorUser optUsr = new OperatorUser(Constants.NETWORK_ADMIN);

            balEnq = new ServiceCharge(Services.BALANCE_ENQUIRY, chUser, optUsr, null, null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(balEnq)
                    .deleteNFSChargeAllVersions(balEnq);

            ServiceChargeManagement.init(t1)
                    .addInitiateNFSC(balEnq);

            double commAmt = Double.parseDouble(balEnq.NFSCServiceCharge) + 0.1;

            NonFinancialServiceCharge_Page2 nf = NonFinancialServiceCharge_Page2.init(t1);
            nf.setComissionAmount(Double.toString(commAmt));
            nf.clickNextToBonusSetup();

            Assertion.verifyErrorMessageContain("nonfin.servicecharge.commission.commissionAmountVSservicecharge",
                    "Commission amount is higher than service charge", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);

        } finally {
            balEnq.setNFSCCommission(balEnq.NFSCServiceCharge);
            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(balEnq);

            Assertion.finalizeSoftAsserts();
        }
    }

    /*  Automater - Surya Kanta Dhal
     * This Test is blocked as Service Charge and Commission Calculation logic is not implemented.
     *
     * DESC : To verify channel admin/network admin is able to calculate service charge successfully.
     * ID : TC_ECONET_0073
     *
     * @throws Exception
     */
    @Test(enabled = false, priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
            FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})

    public void TC_ECONET_0073() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0073", "To verify channel admin/network admin is able to calculate service charge successfully.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SMOKE_CASE_5_0,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("SVC_CALC", Constants.NETWORK_ADMIN);

            ServiceCharge sCharge = new ServiceCharge(Services.C2C, netAdmin, channeluser, null,
                    null, null, null);

            Login.init(t1).login(netAdmin);

            ServiceChargeManagement.init(t1)
                    .configureServiceCharge(sCharge);

            ServiceChargeManagement.init(t1).financialServiceChargeCalculator(sCharge);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /*  Automater - Surya Kanta Dhal
     * TEST : POSITIVE
     * DESC : To verify network admin is able to View Service charge calculator(Non Financial- charging).
     * ID : TC_ECONET_0083
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0083() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0083", "To verify network admin is able to View Service charge calculator(Non Financial- charging).");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("SVC_CALC", Constants.NETWORK_ADMIN);

            ServiceCharge sCharge = new ServiceCharge(Services.BALANCE_ENQUIRY, subscriber, netAdmin, null,
                    null, null, null);

            ServiceChargeManagement.init(t1)
                    .configureNonFinancialServiceCharge(sCharge);

            Login.init(t1).login(netAdmin);

            ServiceChargeManagement.init(t1).nonFinancialServiceChargeCalculator(sCharge);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    /**
     * TEST : NEGATIVE
     * DESC : To verify that the Proper error message should be displayed on web if user don’t enter amount slab during service charge creation.
     * ID : TC_ECONET_0086
     *
     * @throws Exception
     */
    @Test(priority = 18, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0086() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0086", "To verify that the Proper error message should be displayed on web if user don’t enter amount slab during service charge creation.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM,
                FunctionalTag.SERVICE_CHARGE_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        ServiceCharge sCharge = null;

        try {
            User retailer = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            User wholesalerPayee = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            OperatorUser usrSChargeCreator = DataFactory.getOperatorUsersWithAccess("SVC_ADD").get(0);

            sCharge = new ServiceCharge(Services.C2C, retailer, wholesalerPayee, null,
                    null, null, null);

            //ServiceChargeManagement.init(t1).deleteServiceChargeAllVersions(sCharge);

            Login.init(t1).login(usrSChargeCreator);
            ServiceChargeManagement.init(t1).addInitiateServiceCharge(sCharge);
            startNegativeTestWithoutConfirm();
            ServiceChargeManagement.init(t1).setCommissionDetail(sCharge);
            ServiceCharge_Pg2 pageTwo = ServiceCharge_Pg2.init(pNode);
            pageTwo.setServiceToRange("0");
            pageTwo.clickNextButton();
            Assertion.verifyErrorMessageContain("servicechargeprofile.validation.greaterthanmin", "Verify Error When slabs are not defined.", t1, "1");

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1035() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1035", "To verify that the Valid user should able to define service charge with TAX value with Fixed and Percentage amount.").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {
            User wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            ServiceCharge sCharge = new ServiceCharge(Services.OPERATOR_WITHDRAW, wholesaler, opt, null,
                    null, null, null);
            ServiceChargeManagement.init(t1).deleteServiceCharge(sCharge);
            ServiceChargeManagement.init(t1).configureServiceCharge(sCharge);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1037() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_EzCONET_1037", "To verify that the Ecocash tax wallets should be displayed in Reconciliation screen.").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {
            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);
            Login.init(t1).login(opt);

            new Reconciliation_Page(t1).validateFieldsInReconcilationScreen();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1038() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1038", "To verify that the Ecocash tax wallets should be displayed in Operator Withdrawal  screen.").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {
            OperatorUser stockWithdrawUser = DataFactory.getOperatorUsersWithAccess("STK_WITHDRAW").get(0);
            Login.init(t1).login(stockWithdrawUser);

            StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(pNode);
            stockWithdrawl_page1.navToStockWithdrawalPage();
            List obj = fl.getOptions(stockWithdrawl_page1.getWalletElement());
            Assertion.verifyEqual(obj.contains("INDTAX03"), true, "Assert tax wallet  03", t1, true);
            Assertion.verifyEqual(obj.contains("INDTAX02"), true, "Assert tax wallet  02", t1, false);
            Assertion.verifyEqual(obj.contains("INDTAX01"), true, "Assert tax wallet  01", t1, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1039() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1039", "To verify that the Valid user should able to perform Operator Withdrawal using Ecocash tax wallets.").assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {
            OperatorUser stockWithdrawUser = DataFactory.getOperatorUsersWithAccess("STK_WITHDRAW").get(0);

            BigDecimal optPreBalanceINDTAX03 = MobiquityGUIQueries.fetchOperatorBalanceinTotal(DataFactory.getDefaultProvider().ProviderId, "INDTAX03");
            t1.info("Initial system balance is " + optPreBalanceINDTAX03);
            if (optPreBalanceINDTAX03.intValue() != 0) {
                Login.init(t1).login(stockWithdrawUser);
                StockWithdrawal_page1 page = new StockWithdrawal_page1(t1);
                String txnAmount = "1";
                page.navToStockWithdrawalPage();
                page.walletID_Select("INDTAX03");
                page.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
                page.selectBankName(defaultBank.BankName);
                page.bankAccno_SelectDefault();
                page.setTransferAmount(txnAmount);
                page.submit_Click();
            } else {
                pNode.skip("Available balance is 0");
            }

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0078() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0078",
                "To verify that it will not be possible to set any Commission rule for a new subscriber for " +
                        "that service usage without setting a Service Charge Profile for the service.");

        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        ServiceCharge sCharge = null;

        try {
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            OperatorUser opt = DataFactory.getOperatorUserListWithCategory(Constants.NETWORK_ADMIN).get(0);

            sCharge = new ServiceCharge(Services.ACCOUNT_CLOSURE, subs, opt, null,
                    null, null, null);

            MobiquityGUIQueries dbHandler = new MobiquityGUIQueries();
            NewSubsCommissionRule_Page1 nSubsCommRule = NewSubsCommissionRule_Page1.init(t1);
            List<String> subsGrades = MobiquityGUIQueries.dbGetGradesPerCategory(Constants.SUBSCRIBER);
            String arr = StringUtils.join(subsGrades, "','");
            String reqGrades = "'" + arr + "'";
            ArrayList<String> serviceCharges = dbHandler.dbGetServiceChargeForSpecifiedGrade(reqGrades, reqGrades);
            Login.init(t1).login(DataFactory.getOperatorUserWithAccess(Roles.NEW_SUBSCRIBER_COMMISSION_RULE));
            Thread.sleep(5000);
            nSubsCommRule.navigateToSubsCommRule();

            ArrayList<String> services = nSubsCommRule.getServicesOnPage();

            String serviceCode = null;
            for (String x : services) {
                if (serviceCharges.contains(x)) {
                    t1.info("Service Charge for service " + x + " exists");
                } else if (!serviceCharges.contains(x)) {
                    serviceCode = x;
                    break;
                }
            }


            ServiceChargeManagement.init(t1).addNewSubsCommissionRuleForSpecificService(sCharge.ServiceChargeName,
                    "999", "365", serviceCode);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0120() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0120", "To verify that user is able to specify service charge that a consumer has to pay (i.e. joining fee)" +
                " to onboard into mobiquity® Money through web.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);
        try {
            User chUser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User user = new User(Constants.WHOLESALER);
            //This will initiate and approve Service charge
            ServiceCharge serviceCharge = new ServiceCharge(Services.ACQFEE, chUser, user, null, null, null, null);

            Login.init(t1).login(DataFactory.getOperatorUserWithAccess("SVC_ADD"));

            ServiceChargeManagement.init(t1).configureServiceCharge(serviceCharge);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }

}



