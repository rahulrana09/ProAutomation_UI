package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.TxnResponse;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.pinManagement.PinManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.pinManagement.AddQnToMasterList;
import framework.pageObjects.pinManagement.ResetPinPage;
import framework.pageObjects.userManagement.SecurityQuestions_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Suite_SecretQuestionOrAnswerManagement extends TestInit {

    private OperatorUser opt, usrCreator, usrApprover;
    private User chAddSubs, chApproveSubs;
    private String pref, prefValue, pref_userInput_que;
    private ExtentTest eSetup;

    @BeforeClass(alwaysRun = true)
    public void prerequisite() throws Exception {

        eSetup = pNode.createNode("Setup", "SecretQuestionOrAnswerManagement");

        opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
        usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2");

        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
        chApproveSubs = DataFactory.getChannelUserWithAccess("SUBSADDAP");

        pref = MobiquityGUIQueries.fetchDefaultValueOfPreference("CURRENCY_FACTOR");
        pref_userInput_que = MobiquityGUIQueries.fetchDefaultValueOfPreference("USER_INPUTTED_QUESTIONS_ENABLED");

        SystemPreferenceManagement.init(eSetup).updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", "TRUE");

    }

    @AfterClass(alwaysRun = true)
    public void postrequisite() throws Exception {

        eSetup = pNode.createNode("Setup", "SecretQuestionOrAnswerManagement");

        SystemPreferenceManagement.init(eSetup)
                .updateSystemPreference("USER_INPUTTED_QUESTIONS_ENABLED", pref_userInput_que);
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_UAT_0622() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0622", "To verify that the upload template for adding to the master questions list will have three columns – Language Code, Question Code, and Question. The template file would be in the standard .csv format.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            Login.init(t1).login(opt);
            AddQnToMasterList page = new AddQnToMasterList(t1);

            page.navAddQnsToMasterList();
            page.downloadQuestionTemplate();
            ArrayList<String> header = page.getHeadersFromTemplate();

            ArrayList<String> expected = new ArrayList<String>();
            expected.add("Question Code*");
            expected.add("Language Code(1/2/3)*");
            expected.add("Question*");

            try {
                Assert.assertEquals(header, expected);
                t1.pass("Headers are displayed as expected");
            } catch (Exception o) {
                t1.fail("Headers are not displayed as expected");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_UAT_0623() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0623", "To verify that System would not allow" +
                " deleting questions from the list, if the questions are available under the Self PIN Reset Rule for" +
                " any category.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {
            String question = MobiquityGUIQueries.getSecurityQnAssociatedWithUser();

            Login.init(t1).login(opt);

            startNegativeTest();
            PinManagement.init(t1).deleteQuestion(question);

            Assertion.verifyActionMessageContain("question.cannot.be.deleted",
                    "Question is associated with user", t1, question);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_UAT_0619a_0621_0624() throws Exception, MoneyException {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0619_a", "To verify that the self PIN reset" +
                " functionality would be available to Channel Users(Wholesaler) if the self PIN reset rules have been defined.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);
        boolean isPrefLinkValChanged = false;
        User whs = null;
        try {
            String prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MAX_ACC_BAL_ALLOWED");
            String prefValuePinLink = MobiquityGUIQueries.fetchDefaultValueOfPreference("USER_INPUTTED_QUESTIONS_ENABLED");

            if (prefValue.equalsIgnoreCase("FALSE")) {
                MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("MAX_ACC_BAL_ALLOWED", "Y");
                SystemPreferenceManagement.init(t1).updateSystemPreference("MAX_ACC_BAL_ALLOWED", "TRUE");
            }

            whs = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(t1).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);


            int flag = 0;

            ArrayList<String> categories = MobiquityGUIQueries.dbGetCategoriesForSelfRestPIN();
            for (String category : categories) {
                if (category.equalsIgnoreCase(Constants.WHOLESALER)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 1) {
                t1.info("Self PIN reset rule for wholesaler is already defined");
            } else {
                ArrayList<String> questions = MobiquityGUIQueries.dbGetQuestionsFromMaster(Constants.LANGUAGE1, "N");

                ArrayList<String> expectedQns = new ArrayList<>();
                expectedQns.add(questions.get(0));
                expectedQns.add(questions.get(1));

                PinManagement.init(t1)
                        .addPinResetRulesForCategory(whs, expectedQns, Constants.MIN_THRESHOLD);
            }
            //USER_INPUTTED_QUESTIONS_ENABLED
            if (prefValuePinLink.equalsIgnoreCase("FALSE")) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("MAX_ACC_BAL_ALLOWED", "TRUE");
                isPrefLinkValChanged = true;
            }

            CommonUserManagement.init(t1)
                    .changeFirstTimePasswordWithoutSkippingQuestions(whs);
            CommonUserManagement.init(t1)
                    .setSecurityQuestions(whs);


            Login.init(t1).login(whs);
            SecurityQuestions_pg1.init(t1).checkResetPINLink();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            if (isPrefLinkValChanged)
                SystemPreferenceManagement.init(t1).updateSystemPreference("MAX_ACC_BAL_ALLOWED", "FALSE");
        }


        /*
        Functionality not implemented ::
        No SMS sent to user for "setting security questions"
        */

        ExtentTest t2 = pNode.createNode("TC_ECONET_0624", "To verify that For first time login, " +
                "once the new password, PIN and security questions are set, the mobiquity system will send an SMS to " +
                "the customer MSISDN to inform him/her that his new password, PIN and security questions are set.");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            TxnResponse res = Transactions.init(t2)
                    .changeChannelUserMPin(whs);

            TxnResponse res1 = Transactions.init(t2)
                    .changeChannelUserTPin(whs);

            List<String> messages = MobiquityGUIQueries.getAllMessageFromSentSMS(whs.MSISDN, "desc");

            //decrypt the messages
            String decryptedPinMsg = Utils.decryptMessage(messages.get(0));
            String decryptedMPinMsg = Utils.decryptMessage(messages.get(1));
            String decryptedPassword = Utils.decryptMessage(messages.get(2));
            //String decryptedP = d.decrypt(messages.get(3));

            //verify  Messages
            Assertion.verifyMessageContain(decryptedPassword, "password.change", "Check SMS for password change", t2);

            Assertion.verifyMessageContain(decryptedPinMsg, "pin.change.sms", "Check SMS for PIN change", t2, res1.Message.split("ID ")[1].split(" ")[0].trim(), "0.00", "0.00", "0.00");

            Assertion.verifyMessageContain(decryptedMPinMsg, "mPin.change.sms", "Check SMS for MPIN change", t2, res.Message.split("ID ")[1].split(" ")[0].trim(), "0.00", "0.00", "0.00");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC_ECONET_0621", "To verify that If enabled" +
                "'Maximum Account Balance Allowed' field., then the field would be displayed to the user, " +
                "and if the user’s total account balance is higher than the value set in the field, then the" +
                " self PIN reset functionality won’t be available to the user.");
        t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {
            String maxBal = MobiquityGUIQueries.dbGetMaxAccountBalanceForCategory(whs.CategoryCode);
            int maxBalance = Integer.parseInt(maxBal) / Integer.parseInt(pref);

            TransactionManagement.init(t3).initiateAndApproveO2C(whs, String.valueOf(maxBalance + 100), "Test");

            Login.init(t3).login(whs);
            SecurityQuestions_pg1.init(t3).checkPINManagementLink();

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t3);
        }
    }

    @Test(priority = 4, enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_UAT_0619_b() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0619_b", "To verify that the self PIN reset" +
                " functionality would be available to Subscriber if the self PIN reset rules have been defined.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            User subs = new User(Constants.SUBSCRIBER);
            Login.init(t1).login(chAddSubs);
            SubscriberManagement.init(t1).addSubscriber(subs, false);

            Login.init(t1).login(chApproveSubs);
            SubscriberManagement.init(t1).addInitiatedApprovalSubs(subs);


            //User subs = DataFactory.getUserUsingMsisdn("7791411558");

            int flag = 0;

            ArrayList<String> categories = MobiquityGUIQueries.dbGetCategoriesForSelfRestPIN();
            for (String category : categories) {
                if (category.equalsIgnoreCase(Constants.SUBSCRIBER)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 1) {
                t1.info("Self PIN reset rule for Subscriber is already defined");
            } else {

                ArrayList<String> questions = MobiquityGUIQueries.dbGetQuestionsFromMaster(Constants.LANGUAGE1, "N");

                ArrayList<String> expectedQns = new ArrayList<String>();
                expectedQns.add(questions.get(0));
                expectedQns.add(questions.get(1));

                Login.init(t1).login(opt);
                PinManagement.init(t1).addPinResetRulesForCategory(subs, expectedQns, Constants.MIN_THRESHOLD);
            }


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_UAT_0625_0626_0630() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0625", "To verify that if the information" +
                " entered by user does not match with stored information, then PIN won’t be reset for this user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        User whs = new User(Constants.WHOLESALER);
        String defaultValue = null;
        try {

            Login.init(eSetup).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(whs);
            Login.init(eSetup).login(usrApprover);
            ChannelUserManagement.init(t1).approveChannelUser(whs);

            int flag = 0;

            ArrayList<String> categories = MobiquityGUIQueries.dbGetCategoriesForSelfRestPIN();
            for (String category : categories) {
                if (category.equalsIgnoreCase(Constants.WHOLESALER)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 1) {
                t1.info("Self PIN reset rule for wholesaler is already defined");
            } else {
                ArrayList<String> questions = MobiquityGUIQueries
                        .dbGetQuestionsFromMaster(Constants.LANGUAGE1, "N");

                ArrayList<String> expectedQns = new ArrayList<>();
                expectedQns.add(questions.get(0));
                expectedQns.add(questions.get(1));

                PinManagement.init(t1).addPinResetRulesForCategory(whs, expectedQns, Constants.MIN_THRESHOLD);
            }

            CommonUserManagement.init(t1)
                    .changeFirstTimePasswordWithoutSkippingQuestions(whs);

            CommonUserManagement.init(t1).setSecurityQuestions(whs);

            ResetPinPage page1 = new ResetPinPage(t1);
            page1.navRestPin();

            page1.setAnswer("Kuch Bhi");
            page1.clickNext();

            Assertion.verifyErrorMessageContain("pinmanagement.pinreset.answers.incorrect", "Reset PIN failed", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC_ECONET_0626", "To verify that if there are " +
                "multiple PIN types possible for the user  then the user will be able to specify the PIN " +
                "type for which he is performing the reset.");
        t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {
            PinManagement.init(t2).resetPINbyUser(whs, Constants.RESET_MPIN_CONST, ConfigInput.mPin);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC_ECONET_0630", "To verify that the User is able" +
                " to reset PIN as per the number of times a day it is configured in the system.");
        t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

            defaultValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("PINRESET_COUNT_PER_DAY");

            Login.init(t3).login(chUser);

            Utils.putThreadSleep(3000);

            PinManagement.init(t3).resetPINbyUser(chUser, Constants.RESET_MPIN_CONST, ConfigInput.mPin);

            PinManagement.init(t3).resetPINbyUser(chUser, Constants.RESET_MPIN_CONST, ConfigInput.mPin);

            PinManagement.init(t3).resetPINbyUser(chUser, Constants.RESET_MPIN_CONST, ConfigInput.mPin);

            PinManagement.init(t3).resetPINbyUser(chUser, Constants.RESET_MPIN_CONST, ConfigInput.mPin);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t3);
        } finally {
            SystemPreferenceManagement.init(t3).updateSystemPreference("PINRESET_COUNT_PER_DAY", defaultValue);
        }
    }

    @Test(priority = 6, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_UAT_0627() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0627", "To verify that admin user is able to add question to the master questions list from which the user would be asked questions for self PIN reset.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            String quesCode = "QA1";
            String question = "What is the name of your favorite pet";

            HashMap<String, String> questions = new HashMap<>();
            questions.put(quesCode, question);

            Login.init(t1).login(opt);
            PinManagement.init(t1).addQuestionsToMaster(questions, Constants.LANGUAGE1);

            String value = MobiquityGUIQueries.dbGetQuestionCodeForQuestion(question);

            if (value.equalsIgnoreCase(quesCode)) {
                t1.pass("Question is reflected in DB");
            } else {
                t1.fail("Question is not reflected in DB");
            }

            PinManagement.init(t1).deleteQuestion(questions.get(quesCode));

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 7, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_UAT_0628() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0628", "To verify that no two entries can have the same question code and language code.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT).assignAuthor(Author.PUSHPALATHA);

        try {

            String quesCode = "QA2";
            String question = "What is the name of your first school";

            HashMap<String, String> questions = new HashMap<>();
            questions.put(quesCode, question);

            System.out.println(questions);

            Login.init(t1).login(opt);
            PinManagement.init(t1).addQuestionsToMaster(questions, Constants.LANGUAGE1);

            PinManagement.init(t1).addQuestionsToMaster(questions, Constants.LANGUAGE1);

            AddQnToMasterList.init(t1).checkLogFileSpecificMessage(quesCode, "question.exists");

            PinManagement.init(t1).deleteQuestion(questions.get(quesCode));

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 8, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT})
    public void TC_ECONET_0629() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0629", "To verify that there would be a " +
                "parameter to define the maximum number of questions that can be stored in the master " +
                "questions list and file upload will fail if this limit is breached.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.SECRET_QUESTION_ANSWER_MANAGEMENT);

        try {

            //fetch default number of questions allowed, fetch number of ques already exist
            prefValue = MobiquityGUIQueries.fetchDefaultValueOfPreference("MAX_NO_OF_QNS");
            String questionCount = MobiquityGUIQueries.getMasterQuestionsCount(Constants.LANGUAGE1);

            String quesCode = "" + DataFactory.getRandomNumber(4);
            String question = "Selenium Automation";

            //set the default num of ques allowed
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MAX_NO_OF_QNS", questionCount);

            HashMap<String, String> questions = new HashMap<>();
            questions.put(quesCode, question);

            System.out.println(questions);
            ConfigInput.isAssert = false;

            Login.init(t1).login(opt);
            PinManagement.init(t1).addQuestionsToMaster(questions, Constants.LANGUAGE1);

            Assertion.verifyErrorMessageContain("pinmanagement.questions.addedinthe.error",
                    "exceding maximum num of questions", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        } finally {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("MAX_NO_OF_QNS", prefValue);
        }
    }

}
