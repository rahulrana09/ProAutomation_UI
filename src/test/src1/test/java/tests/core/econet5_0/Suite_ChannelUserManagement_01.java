package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.comviva.mmoney.exception.MoneyException;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.Wallet;
import framework.entity.Geography;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.entity.WebGroupRole;
import framework.features.apiManagement.Transactions;
import framework.features.bulkUserRegnAndModification.BulkUserRegistrationAndModification;
import framework.features.channelUserManagement.HierarchyBranchMovement;
import framework.features.common.Login;
import framework.features.systemManagement.GeographyManagement;
import framework.features.systemManagement.Preferences;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.nonFinancialServices.EmailNotificationConfig_Page;
import framework.pageObjects.userManagement.*;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityDBAssertionQueries;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.*;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import framework.util.propertiesManagement.MessageReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by prashant.kumar on 8/21/2018.
 */
public class Suite_ChannelUserManagement_01 extends TestInit {
    private OperatorUser usrCreator, usrView, usrApprover, delChUser,
            suspendChUsr, suspendChUsrApprove, userModify, userModifyApprover,
            resumeChUsr, resumeChUsrApprove;
    private OperatorUser bulkCreate, bulkApprove, emailnotifier;
    private User chUser, chUserRet;
    private ExtentTest setupNode;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception, MoneyException {
        setupNode = pNode.createNode("Setup", "Setup Specific to this suite");
        try {
            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            chUserRet = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            emailnotifier = DataFactory.getOperatorUserWithAccess("NTF_CONFIG");
            usrCreator = DataFactory.getOperatorUserWithAccess("PTY_ACU", Constants.CHANNEL_ADMIN);
            usrApprover = DataFactory.getOperatorUserWithAccess("PTY_CHAPP2", Constants.CHANNEL_ADMIN);
            userModify = DataFactory.getOperatorUserWithAccess("PTY_MCU", Constants.CHANNEL_ADMIN);
            userModifyApprover = DataFactory.getOperatorUserWithAccess("PTY_MCHAPP", Constants.CHANNEL_ADMIN);
            usrView = DataFactory.getOperatorUserWithAccess("PTY_VCU", Constants.CHANNEL_ADMIN);
            suspendChUsr = DataFactory.getOperatorUserWithAccess("PTY_SCU", Constants.CHANNEL_ADMIN);
            suspendChUsrApprove = DataFactory.getOperatorUserWithAccess("PTY_SCHAPP", Constants.CHANNEL_ADMIN);
            resumeChUsr = DataFactory.getOperatorUserWithAccess("PTY_RCU", Constants.CHANNEL_ADMIN);
            resumeChUsrApprove = DataFactory.getOperatorUserWithAccess("PTY_RCHAPP", Constants.CHANNEL_ADMIN);
            delChUser = DataFactory.getOperatorUserWithAccess("PTY_DCU", Constants.CHANNEL_ADMIN);
            bulkCreate = DataFactory.getOperatorUserWithAccess("BLK_CHUSR");
            bulkApprove = DataFactory.getOperatorUserWithAccess("BULK_AP");

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0, "IN_BASE"})
    public void addAndApprove() throws Exception {

        List<String> arrCategoryNotToInclude = Arrays.asList(
                Constants.WHOLESALER,
                Constants.RETAILER,
                Constants.HEAD_MERCHANT,
                Constants.MERCHANT
        );
        char i = 65;
        for (WebGroupRole grpRole : GlobalData.rnrDetails) {
            // create the user based on the condition
            if (arrCategoryNotToInclude.contains(grpRole.CategoryCode)) {
                String catCode = grpRole.CategoryCode;
                System.out.println(catCode);

                ExtentTest t1 = pNode.createNode("TC_ECONET_0162_" + i, "To verify that Channel admin is able to add initiate channel user successfully (" + grpRole.CategoryName + ")");
                t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
                User user = new User(catCode);

                ExtentTest t2 = pNode.createNode("TC_ECONET_0163_" + i, "To verify that Channel admin is able to approve add initiation of channel user successfully. Check for all channel users.");
                t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);
                try {


                    Login.init(t1).login(usrCreator);
                    ChannelUserManagement.init(t1).addChannelUser(user);


                    Login.init(t2).login(usrApprover);
                    ChannelUserManagement.init(t2).approveChannelUser(user);

                } catch (Exception e) {
                    Assertion.raiseExceptionAndContinue(e, t1);
                    t2.skip("Test case Skipped");
                } catch (AssertionError ea) {
                    t2.skip("Test case Skipped");
                    t1.fail(ea);
                }

                i++;
            }
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void modifyAndApprove() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0164",
                "To verify that Channel admin can initiate modification of the channel user(Wholesaler).")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        User chUser = CommonUserManagement.init(t1).getTempChannelUser(Constants.MERCHANT);
        try {

            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1).initiateChannelUserModification(chUser).completeChannelUserModification();

            String expectedMessage = MessageReader.getDynamicMessage("channeluser.modify.approval", chUser.FirstName, chUser.LastName);
            Assertion.verifyEqual(CommonChannelUserPage.init(t1).getActionMessage(), expectedMessage, "Modify User Approval, " + chUser.LoginId, t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        ExtentTest t2 = pNode.createNode("TC_ECONET_0165",
                "To verify that Channel admin can approve modification of the channel user(Wholesaler).")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t2).login(userModifyApprover);
            ChannelUserManagement.init(t2).modifyUserApproval(chUser);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC_ECONET_0166",
                "To verify that Channel admin is able to view the channel user details.(Wholesaler)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(usrView);
            ChannelUserManagement.init(t1).viewChannelUser(chUser);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws Exception
     */
    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void suspendAndResume() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0167",
                "To Verify that Channel admin can initiate suspension of the channel user.(Wholesaler)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        User whs_0167 = new User(Constants.WHOLESALER);

        try {
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(whs_0167, false);
            Login.init(t1).login(suspendChUsr);

            ChannelUserManagement.init(t1).initiateSuspendChannelUser(whs_0167);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }


        ExtentTest t2 = pNode.createNode("TC_ECONET_0168",
                "To Verify that Channel admin can Approve suspend initiate of channel user.(Wholesaler)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t2).login(suspendChUsrApprove);
            ChannelUserManagement.init(t2).approveSuspendChannelUser(whs_0167, true);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }


        ExtentTest t3 = pNode.createNode("TC_ECONET_0169",
                "To verify that Channel admin can initiate resumption of an suspended channel user.(Wholesaler)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t3).login(resumeChUsr);
            ChannelUserManagement.init(t3).resumeInitiateChannelUser(whs_0167);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        ExtentTest t4 = pNode.createNode("TC_ECONET_0788",
                "Channel user Management >> Resume channel user Approval " +
                        "To verify that Channel admin can reject resumption initiate " +
                        "of an suspended channel user.");
        try {
            Login.init(t4).login(resumeChUsrApprove);
            ChannelUserManagement.init(t4).resumeChannelUserApproval(whs_0167, false);
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }


        ExtentTest t5 = pNode.createNode("TC_ECONET_0170",
                "To verify that Channel admin can approve resumption initiate of an suspended channel user.(Wholesaler)")
                .assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t5).login(resumeChUsr);
            ChannelUserManagement.init(t5).resumeInitiateChannelUser(whs_0167);
            Login.init(t5).login(resumeChUsrApprove);
            ChannelUserManagement.init(t5).resumeChannelUserApproval(whs_0167, true);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();

    }

    // this will affect parallel execution, hence disabled
    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0178() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0178", "To verify that while creating channel user if preference IS_RANDOM_PASS_ALLOW is set as Y then password is automatically generated.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User usr = new User(Constants.WHOLESALER);

        Boolean isChange = false;
        if (AppConfig.isRandomPasswordAllowed) {
            t1.info("System Preferences IS_RANDOM_PASS_ALLOW is Y");
        } else {
            Login.init(t1).loginAsSuperAdmin("PREF001");
            Preferences.init(t1).modifySystemPreferences("IS_RANDOM_PASS_ALLOW", "Y");
            AppConfig.isRandomPasswordAllowed = true;
            isChange = true;
        }
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1).initiateChannelUser(usr);

        if (isChange) {
            Login.init(t1).loginAsSuperAdmin("PREF001");
            Preferences.init(t1).modifySystemPreferences("IS_RANDOM_PASS_ALLOW", "N");
            AppConfig.isRandomPasswordAllowed = false;
        }
    }

    // this will affect parallel execution, hence disabled
    @Test(enabled = false, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.CRITICAL_CASES_TAG})
    public void TC_ECONET_0179() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0179", "To verify that while creating channel user if preference IS_RANDOM_PASS_ALLOW is set as N then password is not generated automatically.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User usr = new User(Constants.WHOLESALER);

        Boolean isChange = false;
        if (!AppConfig.isRandomPasswordAllowed) {
            t1.info("System Preferences IS_RANDOM_PASS_ALLOW is N");
        } else {
            Login.init(t1).loginAsSuperAdmin("PREF001");
            Preferences.init(t1).modifySystemPreferences("IS_RANDOM_PASS_ALLOW", "N");
            AppConfig.isRandomPasswordAllowed = false;
            isChange = true;
        }
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1).initiateChannelUser(usr);

        if (isChange) {
            Login.init(t1).loginAsSuperAdmin("PREF001");
            Preferences.init(t1).modifySystemPreferences("IS_RANDOM_PASS_ALLOW", "Y");
            AppConfig.isRandomPasswordAllowed = true;
        }
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0181() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0181", "To verify that the During addition of channel user(Wholesaler), where category selected is not at the highest level (hence is not the owner category of the domain), then a hierarchy association would need to be made for the channel user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User user = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(user);
            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(t1);
            hierarchy.selectDomain(user.DomainName);
            hierarchy.selectCategory(user.CategoryName);
            Thread.sleep(Constants.TWO_SECONDS);// so that element could change its attribute
            Boolean isTrue = hierarchy.OwnerName.isEnabled();
            Assertion.verifyEqual(isTrue, false, "Assert OwnerName is Selectable", t1, true);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, "IN_BASE"})
    public void TC_ECONET_0182_A() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0182_A", "To verify that valid user is able to perform Add Bank Accounts Approval.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);
        User user = new User(Constants.WHOLESALER);
        Login.init(t1).login(usrCreator);

        ChannelUserManagement.init(t1).createChannelUser(user, false);
        Login.init(t1).login(usrCreator);
        CommonUserManagement.init(t1).approveAssociatedBanks(user, Constants.CREATE);
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0182_B() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0182_B",
                "To verify that valid user is able to perform Modify Bank Accounts Approval.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);
        User user = new User(Constants.WHOLESALER);
        String bankName = DataFactory.getNonTrustBankName(defaultProvider.ProviderName);
        try {

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(user, true);

            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(user)
                    .gotoPage(4)
                    .changeBankStatus(bankName,Constants.STATUS_SUSPEND)
                    .completeChannelUserModification()
                    .modifyUserApproval(user);

            Login.init(t1).login(usrApprover);
            CommonUserManagement.init(t1).approveAssociatedSpecificBank(user,bankName, Constants.MODIFY);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        ExtentTest t2 = pNode.createNode("TC_ECONET_0182_C", "To verify that valid user is able to perform Delete Bank Accounts Approval.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Login.init(t2).login(userModify);
            ChannelUserManagement.init(t2)
                    .initiateChannelUserModification(user)
                    .gotoPage(4)
                    .changeBankStatus(bankName,Constants.STATUS_DELETE)
                    .completeChannelUserModification()
                    .modifyUserApproval(user);

            Login.init(t2).login(usrApprover);
            CommonUserManagement.init(t2).approveAssociatedSpecificBank(user,bankName, Constants.MODIFY);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        ExtentTest t3 = pNode.createNode("TC_ECONET_0190", "To verify that the Modification of more than one mwallet or bank account shouldn't be allowed during modify channel user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        if (DataFactory.getAllBankNamesForUserCreation(DataFactory.getDefaultProvider().ProviderName).size() > 1) {
            Login.init(t3).login(userModify);
            ChannelUserManagement.init(t3)
                    .initiateChannelUserModification(user)
                    .gotoPage(4)
                    .changeBankStatus(Constants.STATUS_SUSPEND, 2)
                    .completeChannelUserModification()
                    .modifyUserApproval(user);

            Login.init(t3).login(usrApprover);
            CommonUserManagement.init(t3).approveAssociatedBanks(user, Constants.MODIFY);
        } else {
            t3.skip("Atleast two banks are required to test this service.");
        }

        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0191() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0191", "To verify that while modifying the user’s bank account associations, the MFS Provider and linked bank accounts remains non editable, while the associated grade, mobile group role, TCP, bank account type & status can be modified.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1)
                    .initiateChannelUserModification(chUserRet)
                    .gotoPage(4)
                    .validateFormFields();
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0172() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0172", "To verify that system can reject the initiated modification of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            Login.init(t1).login(bulkCreate);

            List<User> userList = Arrays.asList(new User(Constants.WHOLESALER));

            String filename = BulkUserRegistrationAndModification.init(t1).
                    generateBulkUserRegnModnCsvFile(userList, Constants.BULK_USER_STATUS_ADD, Constants.BULK_USER_STATUS_ADD);

            String txnID = BulkUserRegistrationAndModification.init(t1).
                    initiateBulkUserRegistration(filename);

            Login.init(t1).login(bulkApprove);

            BulkUserRegistrationAndModification.init(t1).
                    approveBulkRegnAndModn(txnID, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0853() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0853", "Bulk User Registration>> Bulk User Registration\n" +
                "To Verify that system give proper message when user click on submit button without upload any bulk file.");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Login.init(t1).login(bulkCreate);

            startNegativeTest();
            String filename = "";
            String txnID = BulkUserRegistrationAndModification.init(t1).initiateBulkUserRegistration(filename);

            Assertion.verifyErrorMessageContain("bulkupload.error.fileNotUploaded",
                    "Validate File Not Uploaded Error", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0175() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0175", "To verify that system can reject the initiated registration of users in bulk.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            Login.init(t1).login(bulkCreate);
            User user = new User(Constants.WHOLESALER);

            List<User> userList = new ArrayList<>();
            userList.add(user);
            String filename = BulkUserRegistrationAndModification.init(t1).
                    generateBulkUserRegnModnCsvFile(userList, Constants.BULK_USER_STATUS_ADD, Constants.BULK_USER_STATUS_ADD);
            String txnID = BulkUserRegistrationAndModification.init(t1).initiateBulkUserRegistration(filename);
            Login.init(t1).login(bulkApprove);
            BulkUserRegistrationAndModification.init(t1).approveBulkRegnAndModn(txnID, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0183() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0183", "To verify that channel user cannot register with same wallet type for more than one times a MFS provider.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = new User(Constants.WHOLESALER);
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1).initiateChannelUser(user).assignHierarchy(user);
        CommonUserManagement.init(t1).assignWebGroupRole(user);
        try {
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(t1);
            Markup m = MarkupHelper.createLabel("mapDefaultWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
            t1.info(m); // Method Start Marker
            Select selProviderUI = new Select(driver.findElement(By.name("counterList[0].providerSelected")));
            Select selPaymentTypeUI = new Select(driver.findElement(By.name("counterList[0].paymentTypeSelected")));
            Select selGrade = new Select(driver.findElement(By.name("counterList[0].channelGradeSelected")));

            selProviderUI.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);

            try {
                page4.clickAddMore(user.CategoryCode);
            } catch (Exception e) {
                if (e.getMessage().contains("element is not attached")) {
                    Thread.sleep(8000);
                    page4.clickAddMore(user.CategoryCode);
                }
            }

            Select selProviderUI1 = new Select(driver.findElement(By.name("counterList[1].providerSelected")));
            Select selPaymentTypeUI1 = new Select(driver.findElement(By.name("counterList[1].paymentTypeSelected")));
            Select selGrade1 = new Select(driver.findElement(By.name("counterList[1].channelGradeSelected")));

            selProviderUI1.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI1.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade1.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);
            page4.clickNext(user.CategoryCode);

            Assertion.verifyErrorMessageContain("cannot.have.multiple.samewallettype.per.mfs.provider",
                    "Verify that Same wallet type can not be associated to the User twice", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0, "IN_BASE"})
    public void TC_ECONET_0184() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0184",
                "To verify that the During creation/modification of channel user, " +
                        "Zone dropdown will display all configured Zones in the system and " +
                        "Channel User Geography dropdown will display only the Area falling under the selected Zone.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = new User(Constants.WHOLESALER);
        Geography geo = new Geography();
        OperatorUser netAdmin = DataFactory.getOperatorUserWithAccess("VIEWGRPHDOMAIN");

        Login.init(t1).login(netAdmin);
        GeographyManagement.init(t1).addZone(geo);
        GeographyManagement.init(t1).addArea(geo);

        Login.init(t1).login(usrCreator);

        ChannelUserManagement.init(t1).initiateChannelUser(user);
        try {
            Markup m = MarkupHelper.createLabel("assignHierarchy: " + user.LoginId, ExtentColor.BLUE);
            t1.info(m);
            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(t1);

            hierarchy.selectDomain(user.DomainName);
            hierarchy.selectCategory(user.CategoryName);

            hierarchy.selectZone(geo.ZoneName);

            WebElement geoArea = driver.findElement(By.id("geoId"));
            Select options = new Select(geoArea);
            Thread.sleep(Constants.THREAD_SLEEP_1000);
            for (int i = 0; i < options.getOptions().size(); i++) {
                String value = options.getOptions().get(i).getText();
                if (value.contains("Select") || value.contains(geo.getAreaName())) {
                    t1.pass("Drop down contain " + value);
                } else {
                    t1.fail("Area drop down contain other values " + value);
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0185() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0185", "To verify that only one wallet can be set as Primary wallet for each MFS provider.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = new User(Constants.WHOLESALER);
        Login.init(t1).login(usrCreator);

        ChannelUserManagement.init(t1).initiateChannelUser(user).assignHierarchy(user);
        CommonUserManagement.init(t1).assignWebGroupRole(user);
        try {
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(t1);
            Markup m = MarkupHelper.createLabel("mapDefaultWalletPreferences: " + user.LoginId, ExtentColor.BLUE);
            t1.info(m); // Method Start Marker
            Select selProviderUI = new Select(driver.findElement(By.name("counterList[0].providerSelected")));
            Select selPaymentTypeUI = new Select(driver.findElement(By.name("counterList[0].paymentTypeSelected")));
            Select selGrade = new Select(driver.findElement(By.name("counterList[0].channelGradeSelected")));

            selProviderUI.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);

            Select selPrimaryAccount = new Select(driver.findElement(By.name("counterList[0].primaryAccountSelected")));
            selPrimaryAccount.selectByIndex(0);

            try {
                page4.clickAddMore(user.CategoryCode);
            } catch (Exception e) {
                if (e.getMessage().contains("element is not attached")) {
                    Thread.sleep(8000);
                    page4.clickAddMore(user.CategoryCode);
                }
            }
            Thread.sleep(1000);
            Select selProviderUI1 = new Select(driver.findElement(By.name("counterList[1].providerSelected")));
            Select selPaymentTypeUI1 = new Select(driver.findElement(By.name("counterList[1].paymentTypeSelected")));
            Select selGrade1 = new Select(driver.findElement(By.name("counterList[1].channelGradeSelected")));

            selProviderUI1.selectByVisibleText(DataFactory.getDefaultProvider().ProviderName);
            Thread.sleep(1200);
            selPaymentTypeUI1.selectByVisibleText(DataFactory.getDefaultWallet().WalletName);
            Thread.sleep(1200);
            selGrade1.selectByVisibleText(user.GradeName);
            Thread.sleep(2500);

            Select selPrimaryAccountUI1 = new Select(driver.findElement(By.name("counterList[1].primaryAccountSelected")));
            selPrimaryAccountUI1.selectByIndex(0);

            page4.clickNext(user.CategoryCode);

            Assertion.verifyErrorMessageContain("cannot.have.multiple.samewallettype.per.mfs.provider",
                    "Assert error message ", t1);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0186() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0186",
                "To verify that without selecting default wallet application fail to proceeds when adding  user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = new User(Constants.WHOLESALER);
        Login.init(t1).login(usrCreator);
        ChannelUserManagement.init(t1).initiateChannelUser(user).assignHierarchy(user);
        CommonUserManagement.init(t1).assignWebGroupRole(user);
        try {
            AddChannelUser_pg4.init(t1).clickNext(user.CategoryCode);
            Assertion.alertAssertion("ChannelUser.MobileGroupRole", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0188() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0188", "To verify that the need to upload KYC documents can be made mandatory or non-mandatory  basis of configured in the system.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        User user = new User(Constants.WHOLESALER);
        try {
            Login.init(t1).login(usrCreator);

            Markup m = MarkupHelper.createLabel("initiateChannelUser: " + user.LoginId, ExtentColor.BLUE);
            t1.info(m);
            AddChannelUser_pg1 pageOne = AddChannelUser_pg1.init(t1);

            pageOne.navAddChannelUser();

            pageOne.selectPrefix();
            pageOne.setFirstName(user.FirstName);
            pageOne.setLastName(user.LastName);
            pageOne.setExternalCode(user.ExternalCode);
            pageOne.setEmail(user.Email);
            pageOne.selectGender();
            pageOne.setDateOfBirth(new DateAndTime().getDate(-7300));
            pageOne.selectIdType(1);

            if (ConfigInput.isCoreRelease) {
                pageOne.setRegType(Constants.REGTYPE_NO_KYC);
                pageOne.setMerchantType(user.MerchantType);
            }

            pageOne.setWebLoginId(user.LoginId);
            Thread.sleep(Constants.WAIT_TIME);
            if (!AppConfig.isRandomPasswordAllowed) {
                pageOne.setPassword(ConfigInput.userCreationPassword);
                pageOne.setConfirmPassword(ConfigInput.userCreationPassword);
            }

            pageOne.setMSISDN(user.MSISDN);
            pageOne.selectLanguage();
            pageOne.allowAllDays();

            // fields specific To 5.0 Core release
            if (ConfigInput.isCoreRelease) {
                pageOne.setRelationshipOfficer("ROFC");
            }

            if (AppConfig.isImtSendMoneyEnabled) {
                pageOne.setDateOfIssuer(DataFactory.getCurrentDateSlash());
                pageOne.setIssueCountry();
                pageOne.setNationality();
                pageOne.setCurrentResidence();
                pageOne.setPostalCode();
                pageOne.setDateOfExpiry(new DateAndTime().getDate(+180));
                pageOne.setEmployeeName();
            }

            pageOne.clickNext();

            Assertion.verifyErrorMessageContain("channelreg.error.proof2", "Assert Address error message ", t1);
            Assertion.verifyErrorMessageContain("subsreg.error.doc2", "Assert Address error message ", t1);
        /*
            Assertion.verifyErrorMessageContain("channelreg.error.proof2","Assert Photo error message ", t1);
            Assertion.verifyErrorMessageContain("subsreg.error.doc2","Assert Photo error message ", t1);

            Assertion.verifyErrorMessageContain("channelreg.error.proof2","Assert ID error message ", t1);
            Assertion.verifyErrorMessageContain("subsreg.error.doc2","Assert ID error message ", t1);
        */

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0189() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0189", "To verify that the Proper error message should get displayed on WEB if user trying to modify channel user for which request is already in approval stage.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {
            User user = new User(Constants.WHOLESALER);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(user, false);

            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1).initiateChannelUserModification(user).completeChannelUserModification();
            startNegativeTest();
            ChannelUserManagement.init(t1).initiateChannelUserModification(user).completeChannelUserModification();
            Assertion.verifyErrorMessageContain("channeluser.message.update.alreadyinitiated", "Assert error message", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0192() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0192", "To verify that the Valid user should be able to Add channel user for un-registered MSISDN only.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User user = new User(Constants.WHOLESALER);
            String msisdn = user.MSISDN;
            user.setMSISDN(DataFactory.getChannelUserWithCategory(Constants.WHOLESALER).MSISDN);
            Login.init(t1).login(usrCreator);
            startNegativeTest();
            ChannelUserManagement.init(t1).initiateChannelUser(user);
            String msg = MessageReader.getDynamicMessage("channeluser.validation.partyAccessMsisdnidExists", user.MSISDN);
            Assertion.verifyEqual(msg, Assertion.getErrorMessage(), "Assert Already registered MSISDN", t1, true);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0194() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0194", "To verify that primary payment instrument of channel user can not be suspended.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User user = DataFactory.getChannelUserWithCategory(Constants.RETAILER);
            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1).initiateChannelUserModification(user);

            CommonChannelUserPage page = CommonChannelUserPage.init(t1);
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            List<WebElement> val = driver.findElements(By.xpath("//tr/td[8]/select"));
            for (WebElement e : val) {
                Select option = new Select(e);
                option.selectByValue(Constants.STATUS_SUSPEND);
            }
            page.clickNextWalletMap();
            Assertion.verifyErrorMessageContain("channel.error.validate.primary.wallet.suspend",
                    "verify that primary payment instrument of channel user can not be suspended", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0196() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0196",
                "To verify that the Valid user should able to Add channel user for existing MSISDN as well which is status 'N'.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            String existingMSISDN = MobiquityGUIQueries.getinActiveMsisdn();
            if (existingMSISDN != null) {
                User user = new User(Constants.WHOLESALER);
                user.setMSISDN(existingMSISDN);
                ChannelUserManagement.init(t1).createChannelUserDefaultMapping(user, false);
            } else {
                t1.skip("There is no MSISDN available in system with status N to test this case.");
            }
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0782() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0782", "To verify that Channel admin is able to reject add initiation of channel user successfully.(Wholesaler)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
        User user = new User(Constants.WHOLESALER);

        try {
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).addChannelUser(user);


            Login.init(t1).login(usrApprover);
            CommonUserManagement.init(t1).addInitiatedApproval(user, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0783() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0783", "To verify that Channel admin can reject the modification of the channel user.(Wholesaler)");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            User user = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(user, false);

            Login.init(t1).login(userModify);
            ChannelUserManagement.init(t1).initiateChannelUserModification(user).completeChannelUserModification();

            Login.init(t1).login(userModifyApprover);
            ChannelUserManagement.init(t1).approveRejectModifyChannelUser(user, true);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0784", "To verify that Channel admin can approve the Deletion of the channel user.(Wholesaler)");
            t2.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
            Login.init(t2).login(delChUser);
            ChannelUserManagement.init(t2).initiateChannelUserDelete(user);

            ExtentTest t3 = pNode.createNode("TC_ECONET_0785", "To verify that Channel admin can reject the Deletion of the channel user.(Wholesaler)");
            t3.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);
            Login.init(t3).login(delChUser);
            ChannelUserManagement.init(t3).approveRejectDeleteChannelUser(user, false);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0849() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0849", "Channel user Management >> Add channel user\n" +
                "To verify that without selecting default wallet application fail to proceeds when adding  user.");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0);

        try {
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);

            startNegativeTest();
            User user = new User(Constants.WHOLESALER);
            Login.init(t1).login(usrCreator);
            ChannelUserManagement.init(t1).initiateChannelUser(user);
            ChannelUserManagement.init(t1).assignHierarchy(user);
            CommonUserManagement.init(t1).assignWebGroupRole(user);

            List<String> paymentTypeList = DataFactory.getWalletForChannelUserCreation();
            String paymentType = null;
            for (String paymentTypeIterator : paymentTypeList) {
                if (!DataFactory.getDefaultWallet().WalletName.equals(paymentTypeIterator)) {
                    paymentType = paymentTypeIterator;
                    break;
                }
            }

            new Select(driver.findElement(By.name("counterList[0].paymentTypeSelected"))).selectByVisibleText(paymentType);
            t1.info("Selected Payment Type:" + paymentType);
            page4.clickNext(user.CategoryCode);

            Assertion.verifyErrorMessageContain("channel.error.validate.primary.wallet.suspend", "Default Wallet Validation", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.UAP, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0197() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0197",
                "To verify that the Valid user(Channel Admin) should not be " +
                        "able to perform Delete/Suspend bulk feature for Primary wallet/Primary Payment instrument");

        try {
            User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            List<User> userList = new ArrayList<>();
            userList.add(user);

            startNegativeTest();
            Login.init(t1).login(usrCreator);
            String filename = BulkUserRegistrationAndModification.init(t1).generateBulkUserRegnModnCsvFile(userList, Constants.STATUS_SUSPEND, Constants.BULK_USER_STATUS_MODIFY);
            BulkUserRegistrationAndModification.init(t1).initiateBulkUserRegistration(filename);
            String errorFile = Utils.getLatestFilefromDir(FilePath.dirFileDownloads);
            Utils.verifyMessageFromBulkProcessErrorFile(errorFile, "user.error.primaryAccount.cannot.deleted", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_0789() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0789",
                "To verify that channel user can be deleted only if there are no pending transactions against the channel user’s account.");

        BigDecimal amount = new BigDecimal("2");
        User subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        User wholesaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        try {
            // perform Cash out
            SfmResponse response = Transactions.init(t1)
                    .performCashOut(subscriber, wholesaler, amount);

            startNegativeTest();
            ChannelUserManagement.init(t1).initiateChannelUserDelete(wholesaler);
            Assertion.verifyErrorMessageContain("some.wallets.have.balance",
                    "Some Wallets Have Some Balance Left", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.HIERARCHY_BRANCH_MOVEMENT})
    public void TC_ECONET_0201() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0201", "To verify that the Existing balance should also get transfered to the channel user after they are moved to another parent in the same domain.");
        t1.assignCategory(FunctionalTag.HIERARCHY_BRANCH_MOVEMENT, FunctionalTag.ECONET_UAT_5_0);

        try {
            User usrWholeSaler = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            User childUserRet = new User(Constants.RETAILER, usrWholeSaler);
            OperatorUser hierarchyMover = DataFactory.getOperatorUserWithAccess("UTL_MCAT");

            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(childUserRet, false);

            TransactionManagement.init(t1).initiateAndApproveO2C(childUserRet, Constants.O2C_TRANS_AMOUNT, DataFactory.getTimeStamp());

            BigDecimal preUserBalance = MobiquityDBAssertionQueries.getUserBalance(childUserRet, null, null);

            Login.init(t1).login(hierarchyMover);
            HierarchyBranchMovement.init(t1).initiateChUserHierarchyMovement(childUserRet, usrWholeSaler);

            BigDecimal postUserBalance = MobiquityDBAssertionQueries.getUserBalance(childUserRet, null, null);

            Assertion.assertEqual(postUserBalance.toString(), preUserBalance.toString(), "User Balance Validation", t1);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0203", "To verify that the system would send notification to all channel users who are affected by the movement process.");
            SMSReader.init(t2).verifyNotificationContain(usrWholeSaler.MSISDN, "2000001");
            SMSReader.init(t2).verifyNotificationContain(childUserRet.MSISDN, "2000001");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0204_A() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0204_A", "Verify Add Channel User can be accessed by  Network Admin / Channel Admin and CCE only and not by channel user");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Markup test1Marker = MarkupHelper.createLabel("Validating Add Channel User Link on Network Admin", ExtentColor.ORANGE);
            t1.info(test1Marker);
            OperatorUser NetworkAdminUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(NetworkAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");

            Markup test2Marker = MarkupHelper.createLabel("Validating Add Channel User Link on Channel Admin", ExtentColor.ORANGE);
            t1.info(test2Marker);
            OperatorUser ChannelAdminUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(ChannelAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");

            Markup test3Marker = MarkupHelper.createLabel("Validating Add Channel User Link on Customer Care Executive", ExtentColor.ORANGE);
            t1.info(test3Marker);
            OperatorUser CCEUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(CCEUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");

            Markup test4Marker = MarkupHelper.createLabel("Validating Add Channel User Link on Channel User", ExtentColor.ORANGE);
            t1.info(test4Marker);
            User channelUser = DataFactory.getAnyChannelUser();
            Login.init(t1).login(channelUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_ACU");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0204_B() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0204_B", "Verify Modify Channel User can be accessed by Network Admin / Channel Admin and CCE only and not by channel user");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Markup test1Marker = MarkupHelper.createLabel("Validating Modify Channel User Link on Network Admin", ExtentColor.ORANGE);
            t1.info(test1Marker);
            OperatorUser NetworkAdminUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(NetworkAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");

            Markup test2Marker = MarkupHelper.createLabel("Validating Modify Channel User Link on Channel Admin", ExtentColor.ORANGE);
            t1.info(test2Marker);
            OperatorUser ChannelAdminUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(ChannelAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");

            Markup test3Marker = MarkupHelper.createLabel("Validating Modify Channel User Link on Customer Care Executive", ExtentColor.ORANGE);
            t1.info(test3Marker);
            OperatorUser CCEUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(CCEUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");

            Markup test4Marker = MarkupHelper.createLabel("Validating Modify Channel User Link on Channel User", ExtentColor.ORANGE);
            t1.info(test4Marker);
            User channelUser = DataFactory.getAnyChannelUser();
            Login.init(t1).login(channelUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_MCU");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0204_C() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0204_C", "Verify Delete Channel User can be accessed by Network Admin / Channel Admin and CCE only and not by channel user");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Markup test1Marker = MarkupHelper.createLabel("Validating Delete Channel User Link on Network Admin", ExtentColor.ORANGE);
            t1.info(test1Marker);
            OperatorUser NetworkAdminUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(NetworkAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_DCU");

            Markup test2Marker = MarkupHelper.createLabel("Validating Delete Channel User Link on Channel Admin", ExtentColor.ORANGE);
            t1.info(test2Marker);
            OperatorUser ChannelAdminUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(ChannelAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_DCU");

            Markup test3Marker = MarkupHelper.createLabel("Validating Delete Channel User Link on Customer Care Executive", ExtentColor.ORANGE);
            t1.info(test3Marker);
            OperatorUser CCEUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(CCEUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_DCU");

            Markup test4Marker = MarkupHelper.createLabel("Validating Delete Channel User Link on Channel User", ExtentColor.ORANGE);
            t1.info(test4Marker);
            User channelUser = DataFactory.getAnyChannelUser();
            Login.init(t1).login(channelUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_DCU");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0204_D() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0204_D", "Verify Suspend Channel User can be accessed by Network Admin / Channel Admin and CCE only and not by channel user");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Markup test1Marker = MarkupHelper.createLabel("Validating Suspend Channel User Link on Network Admin", ExtentColor.ORANGE);
            t1.info(test1Marker);
            OperatorUser NetworkAdminUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(NetworkAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_SCU");

            Markup test2Marker = MarkupHelper.createLabel("Validating Suspend Channel User Link on Channel Admin", ExtentColor.ORANGE);
            t1.info(test2Marker);
            OperatorUser ChannelAdminUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(ChannelAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_SCU");

            Markup test3Marker = MarkupHelper.createLabel("Validating Suspend Channel User Link on Customer Care Executive", ExtentColor.ORANGE);
            t1.info(test3Marker);
            OperatorUser CCEUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(CCEUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_SCU");

            Markup test4Marker = MarkupHelper.createLabel("Validating Suspend Channel User Link on Channel User", ExtentColor.ORANGE);
            t1.info(test4Marker);
            User channelUser = DataFactory.getAnyChannelUser();
            Login.init(t1).login(channelUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_SCU");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(groups = {FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0204_E() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0204_E", "Verify Resume Channel User can be accessed by Network Admin / Channel Admin and CCE only and not by channel user");
        t1.assignCategory(FunctionalTag.CHANNEL_USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        try {
            Markup test1Marker = MarkupHelper.createLabel("Validating Resume Channel User Link on Network Admin", ExtentColor.ORANGE);
            t1.info(test1Marker);
            OperatorUser NetworkAdminUser = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
            Login.init(t1).login(NetworkAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_RCU");

            Markup test2Marker = MarkupHelper.createLabel("Validating Resume Channel User Link on Channel Admin", ExtentColor.ORANGE);
            t1.info(test2Marker);
            OperatorUser ChannelAdminUser = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            Login.init(t1).login(ChannelAdminUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_RCU");

            Markup test3Marker = MarkupHelper.createLabel("Validating Resume Channel User Link on Customer Care Executive", ExtentColor.ORANGE);
            t1.info(test3Marker);
            OperatorUser CCEUser = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            Login.init(t1).login(CCEUser);
            FunctionLibrary.init(t1).verifyLinkExists("CHUMGMT_ALL", "CHUMGMT_PTY_RCU");

            Markup test4Marker = MarkupHelper.createLabel("Validating Resume Channel User Link on Channel User", ExtentColor.ORANGE);
            t1.info(test4Marker);
            User channelUser = DataFactory.getAnyChannelUser();
            Login.init(t1).login(channelUser);
            FunctionLibrary.init(t1).verifyLinkNotAvailable("CHUMGMT_ALL", "CHUMGMT_PTY_RCU");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 23, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void checkStatusValuesOnBankLinkPageOfUser() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0861", "To verify that status drop down should available on channel user registration assign Linked bank page as a Mandatory field.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(userModify);

            User user = DataFactory.getChannelUserWithCategory(Constants.RETAILER);

            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(user)
                    .gotoPage(4);

            List<String> expectedList = Arrays.asList(Constants.STATUS_ACTIVE, Constants.STATUS_SUSPEND);

            List<String> actualList = new CommonChannelUserPage(t1).getAllValuesFromBankStatusDropdown();

            Assertion.verifyListContains(expectedList, actualList, "Verify Status Dropdown Values from Bank Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * @Test :
     * @Description :
     */
    @Test(priority = 24, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void checkCategoryDropDownValues() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0868", "To verify that on the basis of the domain selected, the category dropdown reloads with the categories present in the selected domain.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        try {
            Login.init(t1).login(usrCreator);

            User user = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(t1).
                    initiateChannelUser(user);

            AddChannelUser_pg2 hierarchy = AddChannelUser_pg2.init(pNode);
            hierarchy.selectDomain(user.DomainName);

            List<String> actualList = hierarchy.getCategoryNamesFromCategoryDropdown();

            Assertion.verifyListContains(MobiquityGUIQueries.getCategoryNameListUsingDomainCode(user.DomainCode), actualList, "Verify Status Dropdown Values from Bank Status", t1);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * @Test : //TODO Pending Case
     * @Description :
     */
    @Test(priority = 25, groups = {FunctionalTag.ECONET_SIT_5_0}, enabled = false)
    public void checkCommissionWalletAsDefaultWallet() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0873",
                "To verify that the commission wallet can never be set as the primary wallet for any MFS allocated to the user.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);
        Wallet commission = DataFactory.getWalletUsingAutomationCode(Wallets.COMMISSION);
        try {
            Login.init(t1).login(usrCreator);

            User user = new User(Constants.WHOLESALER);

            ChannelUserManagement.init(t1).
                    initiateChannelUser(user).assignHierarchy(user).assignWebGroupRole(user);

            int counter = 0;
            AddChannelUser_pg4 walletAssignPg = AddChannelUser_pg4.init(pNode);
            walletAssignPg.selectProvider(counter, DataFactory.getDefaultProvider().ProviderName);
            walletAssignPg.selectPaymentType(counter, DataFactory.getWalletName(commission.WalletId));
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     *
     */
    @Test(priority = 26, groups = {FunctionalTag.ECONET_SIT_5_0, "IN_BASE"})
    public void test_TC_ECONET_0865_TC_ECONET_0866() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0865", "To verify that 1st level channel users can be added by channel administrators only.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {
            User user = new User(Constants.WHOLESALER);

            Login.init(t1).login(usrCreator);

            ChannelUserManagement.init(t1).
                    initiateChannelUser(user).
                    assignHierarchy(user).
                    assignWebGroupRole(user).
                    mapDefaultWalletPreferences(user).
                    mapBankPreferences(user);

            ChannelUserManagement.init(t1).approveChannelUser(user);

            ExtentTest t2 = pNode.createNode("TC_ECONET_0866", "To verify that the channel user would be forced to change password when user login in to the system for the first time.");
            t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

            Login.init(t2).tryLogin(user.LoginId, ConfigInput.userCreationPassword);

            boolean isVisible = new ChangePasswordPage(t2).isChangePasswordPageVisible();

            Assertion.verifyEqual(isVisible, true, "Check Visibility of First Time Change Password Page", t2, true);

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            e.printStackTrace();
            markTestAsFailure(e, t1);
        }

    }


    /**
     * @Test : TC_ECONET_0878 ()
     * @Description :To verify that the drop down ‘Type of Proof’
     * needs to be selected if a document has been uploaded against it and the values for Type of Proof can be:
     */
    @Test(priority = 27, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void verifyProofTypeDropdownValues() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0878",
                "To verify that the drop down 'Type of Proof' needs to be selected if a document has " +
                        "been uploaded against it and the values for Type of Proof can be" +
                        "Passport," +
                        "National Id," +
                        "Driver’s card," +
                        "PAN card," +
                        "SSN," +
                        "Govt issued Id," +
                        "Voter card," +
                        "Ration card");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {

            Login.init(t1).login(usrCreator);

            AddChannelUser_pg1 page = new AddChannelUser_pg1(t1);

            page.navAddChannelUser();

            List<String> expected = Arrays.asList("PASSPORT", "NATIONAL_ID", "DRIVER_CARD", "PAN_CARD", "SSN", "VOTER_CARD", "GOVT_ ID", "RATION_CARD");

            Assertion.verifyListContains(expected,
                    page.getProofTypesFromDropdown(), "Verify Types of Proof", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * @Test : TC_ECONET_0878 ()
     * @Description :To verify that Channel user can be registered with multiple bank
     * accounts of different or even same banks
     */
    @Test(priority = 27, groups = {FunctionalTag.ECONET_SIT_5_0, "IN_BASE"})
    public void verifyUserCreationWithMultipleBankAccount() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0869", "To verify that Channel user can be registered with multiple bank accounts of different or even same banks.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {

            if (DataFactory.getAllBankNamesForUserCreation(DataFactory.getDefaultProvider().ProviderName).size() > 1) {
                User user = new User(Constants.WHOLESALER);
                ChannelUserManagement.init(t1).createChannelUser(user);
            } else {
                t1.skip("Skipping this case as minimum 2 banks required to test this Case");
            }

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    /**
     * This Test Case includes 3 cases related to SMS Notification for Addition/Modification/Deletion
     *
     * @throws Exception
     * @throws MoneyException
     */
    @Test(groups = {FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0150_b() throws Exception, MoneyException {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0150",
                "To verify that the Proper SMS Notification should be sent to users after addition (If configured)");


        t1.assignCategory(FunctionalTag.ECONET_UAT_5_0);

        User user = new User(Constants.WHOLESALER);

        try {
            Login.init(t1).login(emailnotifier);
            EmailNotificationConfig_Page page = new EmailNotificationConfig_Page(t1);
            page.navigateToLink();
            Thread.sleep(Constants.THREAD_SLEEP_1000);
            page.updateSmsEmailService("PTY_MCU", true, false);

            // create Channel User with Default mapping
            ChannelUserManagement.init(t1)
                    .createChannelUserDefaultMapping(user, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        /*
        Test 2, Modify User
         */
        ExtentTest t2 = pNode.createNode("TC_ECONET_0150_b",
                "To verify that the Proper SMS Notification should be sent to users after modification (If configured)");
        try {
            Login.init(t2).login(userModify);
            ChannelUserManagement.init(t2)
                    .initiateChannelUserModification(user)
                    .completeChannelUserModification();

            Login.init(t2).login(userModifyApprover);
            ChannelUserManagement.init(t2).modifyUserApproval(user);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t2);
        }

        /*
         * Test 3 - (TC_ECONET_0150_c)
         */
        ExtentTest t3 = pNode.createNode("TC_ECONET_0150_c",
                "To verify that the Proper SMS Notification should be sent to users after deletion in system. (If configured)");
        try {
            Login.init(t3).login(delChUser);

            // initiate deletion and approve
            ChannelUserManagement.init(t3).
                    initiateChannelUserDelete(user).
                    approveRejectDeleteChannelUser(user, true);

        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        /*
        Test 4
         */
        ExtentTest t4 = pNode.createNode("TC_ECONET_0786",
                "To verify that Channel admin can reject the Deletion of the channel user.(Wholesaler)");
        try {
            Login.init(t4).login(delChUser);
            ChannelUserManagement.init(t4).initiateChannelUserDelete(user);
            ChannelUserManagement.init(t4).approveRejectDeleteChannelUser(user, false);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        Assertion.finalizeSoftAsserts();
    }




    @Test(priority = 28, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void verifyAutoSweepAllowedAvailable() {
        ExtentTest t1 = pNode.createNode("TC_ECONET_9000", "To verify that the Radio Button should be displayed at the time of Channel user registration for selection of auto-sweep.");
        t1.assignCategory(FunctionalTag.ECONET_SIT_5_0);

        try {

            Login.init(t1).loginAsOperatorUserWithRole(Roles.ADD_CHANNEL_USER);

            AddChannelUser_pg1 channelUserPg1 = new AddChannelUser_pg1(t1);

            channelUserPg1.navAddChannelUser();

            Assertion.verifyEqual(channelUserPg1.checkAutoSweepAllowedRadioAvailable(),
                    true,"Check availability of Auto Sweep Allowed Radio Button",t1,true);


        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }


}
