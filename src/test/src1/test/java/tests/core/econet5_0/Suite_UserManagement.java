package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.OperatorUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.ArrayList;

/**
 * Created by Nirupama MK on 26/10/2018.
 */
public class Suite_UserManagement extends TestInit {


    //private OperatorUser  channelAdmin, customerCareExecutive;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        Markup m = MarkupHelper.createLabel("Setup", ExtentColor.BLUE);
        ExtentTest setup = pNode.createNode("Setup");
        setup.info(m); // Method Start Marker


        try {


        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }


    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0708() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1197", "Operator User >> Add Operator User " +
                "To verify that User other than superadmin can not initiate add request of Network admin/ Bank admin.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            try {
                OperatorUser chAdm = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
                Login.init(t1).login(chAdm);

                driver.findElement(By.id("PARTY_PTY_ASU"));
                t1.fail("Add Operator User link is displayed for Channel Admin");

            } catch (Exception e) {
                Assertion.logAsPass("Add Operator User link is not displayed for Channel Admin as Expected", t1);
            }

            try {
                User whs = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
                Login.init(t1).login(whs);

                driver.findElement(By.id("PARTY_PTY_ASU"));
                t1.fail("Add Operator User link is displayed for Channel User");

            } catch (Exception e) {
                Assertion.logAsPass("Add Operator User link is not displayed for Channel User as Expected", t1);
            }

            try {
                OperatorUser nwAdmin = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);
                Login.init(t1).login(nwAdmin);

                driver.findElement(By.id("PARTY_PTY_ASU"));
                t1.fail("Add Operator User link is displayed for NetWorkAdmin ");

            } catch (Exception e) {
                Assertion.logAsPass("Add Operator User link is not displayed for NetWorkAdmin as Expected", t1);
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 2, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_1277() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_1198", "Operator User >> Delete User\n" +
                "To verify that user other than valid user  is not  able to approve/Reject delete initiation of an existing Channel admin/CCE.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            ArrayList<OperatorUser> fields = new ArrayList<OperatorUser>();
            OperatorUser channelAdmin = DataFactory.getOperatorUserWithCategory(Constants.CHANNEL_ADMIN);
            OperatorUser customerCareExecutive = DataFactory.getOperatorUserWithCategory(Constants.CUSTOMER_CARE_EXE);
            fields.add(channelAdmin);
            fields.add(customerCareExecutive);

            int i;
            for (i = 0; i < fields.size(); i++) {

                try {


                    Login.init(t1).login(fields.get(i));
                    fl.contentFrame();
                    driver.findElement(By.xpath("//img[contains(@src,'home.png')]")).click();
                    fl.clickLink("PARTY_ALL");
                    driver.findElement(By.id("PARTY_PTY_ASUD"));
                    t1.fail(fields.get(i).CategoryName + " has rights to approve/Reject delete initiation of an existing Channel admin/CCE.");
                    break;
                } catch (Exception e) {

                    t1.pass(fields.get(i).CategoryName + " has no rights to approve/Reject delete initiation of an existing Channel admin/CCE.");
                    continue;

                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }

    @Test(priority = 3, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0845() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0845", "To verify that Post successful approval" +
                " of an action (add, modify, delete) system users are notified about the same using notification.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.USER_MANAGEMENT, FunctionalTag.ECONET_UAT_5_0);
        try {

            SuperAdmin maker = DataFactory.getSuperAdminWithAccess("PTY_ASU");
            SuperAdmin checker = DataFactory.getSuperAdminWithAccess("PTY_ASUA");

            OperatorUser optUser = new OperatorUser(Constants.NETWORK_ADMIN);
            Login.init(t1).loginAsSuperAdmin(maker);

            OperatorUserManagement.init(t1)
                    .initiateOperatorUser(optUser);

            Login.init(t1).loginAsSuperAdmin(checker);

            OperatorUserManagement.init(t1)
                    .approveOperatorUser(optUser);

            OperatorUserManagement.init(t1)
                    .initAndApproveModifyOrDeleteOptUsr(optUser, true);

            OperatorUserManagement.init(t1)
                    .initAndApproveModifyOrDeleteOptUsr(optUser, false);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
    }
}
