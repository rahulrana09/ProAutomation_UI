package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.google.common.collect.ImmutableMap;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionCorrection.TransactionCorrection;
import framework.features.transactionManagement.TransactionManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.FunctionalTag;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import static framework.util.jigsaw.CommonOperations.setTxnConfig;

public class Suite_ReverseTheLimitWheneverTheTransactionReversed extends TestInit {

    private static User chUser;
    private OperatorUser opt;

    @BeforeClass(alwaysRun = true)
    public void pre_condition() throws Exception {

        Markup m = MarkupHelper.createLabel("Setup", ExtentColor.BLUE);
        pNode.info(m); // Method Start Marker

        try {

            chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
            opt = DataFactory.getOperatorUserWithCategory(Constants.NETWORK_ADMIN);

            ServiceCharge TransCorrec = new ServiceCharge(Services.TXN_CORRECTION, chUser, opt,
                    null, null, null, null);

            TransferRuleManagement.init(pNode)
                    .configureTransferRule(TransCorrec);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
    }

    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.REVERSE_LIMIT, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0559() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0559",
                "To verify that when there is a reversal of transaction, all the limits which got " +
                        "effected due to this transaction also needs to be reversed.");

        ExtentTest t2 = pNode.createNode("TC_ECONET_1117",
                "To verify that limits should be reversed and the" +
                        " money is reversed to wallet.");


        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.REVERSE_LIMIT, FunctionalTag.ECONET_UAT_5_0);
        try {

            setTxnConfig(ImmutableMap.of("txn.reversal.allowed.services", Services.O2C));
            String amount = Constants.MIN_O2C_AMOUNT;
            int o2cAmount = Integer.parseInt(amount);

            //Wholesaler's initial Balance
            UsrBalance preWhsBalance = MobiquityGUIQueries.getUserBalance(chUser, null, null);
            int currentBalance = preWhsBalance.Balance.intValue();

            //initiate and approve O2C
            String tid = TransactionManagement.init(t1).initiateO2CWithProvider(chUser,
                    DataFactory.getDefaultProvider().ProviderName, amount);
            TransactionManagement.init(t1)
                    .o2cApproval1(tid);

            //TCP Count after performing o2c
            String txnId = MobiquityGUIQueries.fetchLastTransactionID(chUser.MSISDN);
            String userID = MobiquityGUIQueries.fetchUserID(txnId);
            String profileID = MobiquityGUIQueries.fetchProfile_id(userID);
            String count1 = MobiquityGUIQueries.fetchPayeeCount(userID, profileID, Constants.O2C_SERVICE_TYPE);
            int tcpcount1 = Integer.parseInt(count1);

            //Wholesaler Balance before Transaction Correction
            UsrBalance WhsBalance = MobiquityGUIQueries.getUserBalance(chUser, null, null);
            int balanceBeforeTxnCorrection = WhsBalance.Balance.intValue();
            currentBalance = currentBalance + o2cAmount;
            Assert.assertEquals(balanceBeforeTxnCorrection, currentBalance);
            Assertion.verifyEqual(balanceBeforeTxnCorrection, currentBalance,
                    "money is reversed to wallet after transaction correction", t2);

            //Transaction Correction Initiation
            String txnid = TransactionCorrection.init(t1)
                    .initiateTxnCorrectionWithTpcApplicable(tid, true, true, true);
            TransactionCorrection.init(t1)
                    .approveOrRejectTxnCorrection(txnid, true);

            //Wholesaler Balance after Transaction Correction
            UsrBalance balanceTxnCorrection = MobiquityGUIQueries.getUserBalance(chUser, null, null);
            int balanceAfterTxnCorrection = balanceTxnCorrection.Balance.intValue();
            currentBalance = currentBalance - o2cAmount;
            Assertion.verifyEqual(balanceAfterTxnCorrection, currentBalance,
                    "wholesaler balance is credited after o2c", t2);

            //TCP Count after performing transaction Correction
            t1.info("Threshold count before performing Transaction Correction is :: " + tcpcount1);
            String count2 = MobiquityGUIQueries.fetchPayeeCount(userID, profileID, Constants.O2C_SERVICE_TYPE);
            int tcpcount2 = Integer.parseInt(count2);

            //threshold count should decrease by one after performing Transaction Correction
            Assert.assertEquals(tcpcount2, tcpcount1 - 1);
            Assertion.verifyEqual(tcpcount2, tcpcount1 - 1, "Verify Threshold count after performing Transaction Correction", t1);

        } catch (Exception e) {
            markTestAsFailure(e, pNode);

            Assertion.finalizeSoftAsserts();
        }

    }
}
