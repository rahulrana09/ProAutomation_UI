package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by vandana.rattan on 22-10-2018.
 */
public class Suite_ConsumerWebPortal extends TestInit {

    @Test(priority = 1, groups = {FunctionalTag.PVG_UAP, FunctionalTag.PVG_SMOKE, FunctionalTag.PVG_SYSTEM, FunctionalTag.CASHIN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0487() throws Exception {

        ExtentTest t1 = pNode.createNode("TC0487: Cashin WEB", "To verify that the Consumer should able to login on WEB portal by his/her credentials")
                .assignCategory(FunctionalTag.PVG_UAP, FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_SMOKE,
                        FunctionalTag.CASHIN, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0);

        try {
            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            Login.init(t1).login(subs);
        } catch (Exception e) {

        }
        Assertion.finalizeSoftAsserts();


        // SentSMS.verifyPayerPayeeDBMessage(cashinUser, subs, Services.CASHIN, transID, t1);
        //String dbMessage = MobiquityGUIQueries.getMobiquityUserMessage(subs.MSISDN,"DESC");
        //DBAssertion.verifyDBMessageContains(dbMessage,"01616","Verify DB Message For O2C Transaction",t1,subs.LoginId,Constants.CASHIN_TRANS_AMOUNT);

    }


}
