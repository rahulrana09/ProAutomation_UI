package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.CustomerTCP;
import framework.entity.InstrumentTCP;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.TCPManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.pageObjects.tcp.AddCustomerTCP_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.FunctionLibrary;
import framework.util.common.Utils;
import framework.util.globalConstant.Author;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by krishan.chawla on 8th Nov 2018
 */
public class Suite_TransferControlProfile extends TestInit {


    private static InstrumentTCP instTCP;
    OperatorUser OptUser;

    @BeforeClass(alwaysRun = true)
    public void setupBeforeClass() throws Exception {
        try {
            instTCP = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                    DataFactory.getDomainName(Constants.WHOLESALER),
                    DataFactory.getCategoryName(Constants.WHOLESALER),
                    DataFactory.getGradesForCategory(Constants.WHOLESALER).get(0).GradeName,
                    "WALLET", DataFactory.getDefaultWallet().WalletName);
            OptUser = DataFactory.getOperatorUsersWithAccess("O2C_INIT").get(0);

        } finally {
            Assertion.finalizeSoftAsserts();
        }
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.TCP})
    public void TC_ECONET_0064() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0064", "To verify that User other than superadmin should not able to add initiate customer level TCP for channel user.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        User chnlUser = DataFactory.getAnyChannelUser();

        Login.init(t1).login(chnlUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TCPROFILE_ALL", "TCPROFILE_TCP_USER");
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.TCP})
    public void TC_ECONET_0745() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0745", "To verify that User other than superadmin is not able to add initiate instrument level TCP.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        User chnlUser = DataFactory.getAnyChannelUser();

        Login.init(t1).login(chnlUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TCPROFILE_ALL", "TCPROFILE_TCP_INSTRMENT");
    }

    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.TCP})
    public void TC_ECONET_0746() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0746", "To verify that User other than superadmin is not able to copy existing customer level TCP.");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.KRISHAN);

        User chnlUser = DataFactory.getAnyChannelUser();

        Login.init(t1).login(chnlUser);
        FunctionLibrary.init(t1).verifyLinkNotAvailable("TCPROFILE_ALL", "TCPROFILE_TCP_USER");
    }


    /**
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.PVG_UAP, FunctionalTag.PVG_UAP, FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ECONET_SMOKE_CASE_5_0})
    public void TC_ECONET_0067() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC_ECONET_0067 :Customer TCP Copy", "To verify that 'Super Admin' is able to copy existing customer level TCP.");

            t1.assignCategory(FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0);

            CustomerTCP custTCP = new CustomerTCP(DataFactory.getDomainName(Constants.SUBSCRIBER),
                    DataFactory.getCategoryName(Constants.SUBSCRIBER), Constants.REGTYPE_NO_KYC_TEXT);

            CustomerTCP copyTCP = new CustomerTCP(DataFactory.getDomainName(Constants.SUBSCRIBER),
                    DataFactory.getCategoryName(Constants.SUBSCRIBER), Constants.REGTYPE_SUBS_INIT_US);

            TCPManagement.init(t1).copyExistingCustomerTCP(custTCP, copyTCP);

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }


    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1159() throws Exception {
        try {
            ExtentTest t1 = pNode.createNode("TC_ECONET_1159 :Instrument level TCP ", "To verify that superadmin is able to Reject the initiated instrument level TCP.");

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            TCPManagement.init(t1).addAndRejectInstrumentTCP(instTCP);

        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        }
    }

    /**
     * //TODO : Specific error message test, Now it is only checking is there any error message on the GUI.
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0})
    public void TC_ECONET_1168() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_1168:Delete Customer TCP", "To verify that the Proper Error message should get display on web " +
                "during customer level TCP deletion " +
                "if instrument level TCP for the same domain, category & registration type exists in the system.");

        try {

            t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_SIT_5_0);

            CustomerTCP custTCP = new CustomerTCP(DataFactory.getDomainName(Constants.SUBSCRIBER),
                    DataFactory.getCategoryName(Constants.SUBSCRIBER), Constants.REGTYPE_NO_KYC_TEXT);

            TCPManagement obj = TCPManagement.init(t1);

            String profileName = obj.getExistingTCPName(custTCP);

            startNegativeTest();

            if (profileName != null) {
                custTCP.ProfileName = profileName;
                TCPManagement.init(t1).deleteCustomerTCP(custTCP);

                AddCustomerTCP_pg1 pageOne = new AddCustomerTCP_pg1(t1);

                String error = pageOne.getErrorMessage();
                Utils.captureScreen(t1);

                if (error != null) {
                    t1.info("Error Message is :" + error);
                    t1.pass("Not allowing to Delete");
                } else {
                    t1.fail("System allowing to delete the TCP");
                }
            } else {
                t1.skip("Profile Not Present. Skipping this test");
            }

            Assertion.finalizeSoftAsserts();

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
    }


    @Test(groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.TCP})
    public void TC_ECONET_0066() throws Exception {
        ExtentTest t1 = pNode.createNode("TC_ECONET_0066", "To verify that the Proper Error Payee maximum transaction amount limit reached message should display for subscriber ");
        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.TCP, FunctionalTag.ECONET_UAT_5_0).assignAuthor(Author.SARASWATHI);
        try {
            Login.init(t1).login(OptUser);

            User subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
            User channeluser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);


            // Edit tcp
            TCPManagement.init(t1).EdittcpforSubs("3", false, true, t1);

            //Perform Cash In
            Login.init(t1).login(channeluser);
            startNegativeTest();
            TransactionManagement.init(t1).performCashIn(subs, "5");
            Assertion.verifyErrorMessageContain("payee.maximum.transaction.amount.limit.reached", "payee.maximum.transaction.amount.limit.reached", t1);


        } catch (Exception e) {
            Assertion.finalizeSoftAsserts();
        } finally {
            Login.init(t1).login(OptUser);
            TCPManagement.init(t1).EdittcpforSubs("99999", false, true, t1);
        }
    }
}
