package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.BulkEnterprisePaymentCSV;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.enterpriseManagement.EnterpriseManagement;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.pageObjects.Enterprise_Management.EntBulkPayDashBoard_Pg1;
import framework.pageObjects.enterpriseManagement.BulkPaymentPage1;
import framework.pageObjects.gradeManagement.GradeAddPage;
import framework.pageObjects.ownerToChannel.O2CInitiation_Page1;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.AddChannelUser_pg4;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.pageObjects.userManagement.ModifyChannelUser_pg1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalConstant.Roles;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Suite_EnterpriseUserManagement_03 extends TestInit {

    private User entUser, subs;
    private OperatorUser o2cInitiator, modifyuser, bpAdminAppL1;


    @BeforeClass(alwaysRun = true)
    public void preCondition() throws Exception {

        ExtentTest eSetup = pNode.createNode("Setup", "Setup specific to this Suite");
        try {
            entUser = DataFactory.getChannelUserWithCategory(Constants.ENTERPRISE);
            modifyuser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
            o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");
            bpAdminAppL1 = DataFactory.getBulkPayerWithAccess("SAL_AP1", entUser);

            subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            ServiceCharge tRule = new ServiceCharge(Services.ENTERPRISE_PAYMENT, entUser, subs, null, null, null, null);
            TransferRuleManagement.init(eSetup)
                    .configureTransferRule(tRule);

        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0216() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0216",
                "To verify that the Bulk payer admin is able to download " +
                        "the uploaded file to check the records which had been " +
                        "successfully passed in bulk initiation.");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId,
                    sub.MSISDN,
                    txnAmount.toString(),
                    "",
                    "",
                    ""));

            EnterpriseManagement.init(t1)
                    .addBulkPayee(sub.MSISDN, DataFactory.getRandomNumberAsString(5));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);

            page.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".pending-policy-list"));
            Utils.captureScreen(t1);
            for (WebElement elem : avaibaleEntry) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    driver.findElement(By.xpath("//a[.='DOWNLOAD']")).click();
                    t1.pass("Uploaded file can be downloaded successfully by BulkPayerAdmin");
                    break;
                } else {
                    t1.fail("Uploaded file cannot be downloaded successfully by BulkPayerAdmin");
                }
            }

            if (avaibaleEntry.size() == 0) {
                t1.warning("No Data available!");
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 2, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0232() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0232", "To verify that the Enterprise should able to view the progress  on completion of Bulk Approval2  Total processed records x, Successful: y, Failed:z");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);

            page.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".pending-policy-list"));

            for (WebElement elem : avaibaleEntry) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    boolean res = driver.findElement(By.xpath("(//div[@id='bulkDirDiv']//span[contains(text(),'Number of entries')])[1]")).isDisplayed();
                    if (res) {
                        t1.pass("Enterprise can view total number of records");
                    }
                    boolean res1 = driver.findElement(By.xpath("//div[@id='bulkDirDiv']//span[.='Success']")).isDisplayed();
                    if (res1) {
                        t1.pass("Enterprise can view total number of Success records");
                    }
                    boolean res2 = driver.findElement(By.xpath("//div[@id='bulkDirDiv']//span[.='Failed']")).isDisplayed();
                    if (res2) {
                        t1.pass("Enterprise can view total number of Failed records");
                    }

                    break;
                } else {
                    t1.fail("Enterprise cannot view Total Processed,success,failed records");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 3, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0224() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0224", "To verify that real time " +
                "progress of all the batch requests can be monitored through new dashboard, wherein it is possible to retry the failed transaction or export all the failed transactions.");
        try {
            User sub = new User(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1).login(entUser);


            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            EntBulkPayDashBoard_Pg1 page = EntBulkPayDashBoard_Pg1.init(pNode);

            page.navigateToLink();
            // get all the requests and check for the batch Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".pending-policy-list"));

            for (WebElement elem : avaibaleEntry) {
                elem.click();
                Thread.sleep(1100);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    driver.findElement(By.xpath("//div[@id='bulkDirDiv']//img[@title='Retry failed records']")).click();
                    Utils.captureScreen(t1);
                    t1.pass("It is possible the retry the failed transaction");
                    break;
                } else {
                    Utils.captureScreen(t1);
                    t1.fail("It is not possible the retry the failed transaction");
                }
            }

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 4, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0794() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0794", "To verify that the Bulk payer admin is able to reject the initiated Enterprise Payment.");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(entUser);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, false);


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 5, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0912() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0912", "To verify that an enterprise user can not have commission wallet associated with him/her.\n");
        try {
            // Modifying Channel User
            Login.init(t1).login(modifyuser);
            ChannelUserManagement.init(t1).
                    initiateChannelUserModification(entUser);
            ModifyChannelUser_pg1 pageOne = ModifyChannelUser_pg1.init(t1);
            if (AppConfig.isImtSendMoneyEnabled) {
                AddChannelUser_pg1.init(t1)
                        .setDateOfIssuer(DataFactory.getCurrentDateSlash());
                pageOne.setIssueCountry();
                pageOne.setNationality();
                pageOne.setCurrentResidence();
                pageOne.setPostalCode();
                pageOne.setEmployeeName();
            }
            CommonChannelUserPage page = CommonChannelUserPage.init(pNode);
            page.clickNextUserDetail();
            page.clickNextUserHeirarcy();
            page.clickNextWebRole();
            page.clickNextEnterprisePreference();
            page.clickNextWalletCombination();
            //Verifying if commission wallet is present
            AddChannelUser_pg4 page4 = AddChannelUser_pg4.init(pNode);
            page4.clickAddMore(Constants.ENTERPRISE);
            Select sel = new Select(driver.findElement(By.id("walletTypeID1")));
            List<WebElement> sel1 = sel.getOptions();
            for (WebElement sel2 : sel1) {
                if (sel2.getText().equalsIgnoreCase("Commission")) {
                    t1.fail("Enterperise can have Commission Wallet");
                } else {
                    continue;
                }
            }
            t1.pass("Enterperise cannot have Commission Wallet");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 6, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0913() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0913", "To verify that while performing O2C, when enterprise domain & category is selected the wallet selection becomes non selectable.");
        try {
            // Modifying Channel User
            Login.init(t1).login(o2cInitiator);

            O2CInitiation_Page1 page1 = O2CInitiation_Page1.init(t1);

            page1.navigateToOwner2ChannelInitiationPage();
            page1.setMobileNumber(entUser.MSISDN);
            page1.selectProviderName(DataFactory.getDefaultProvider().ProviderId);
            Select sel = new Select(driver.findElement(By.id("otherWalletListId")));
            List<WebElement> sel1 = sel.getOptions();
            for (WebElement sel2 : sel1) {
                if (sel2.getText().equalsIgnoreCase("Commission")) {
                    t1.fail("O2C can be done to  Commission Wallet for Enterprise");
                } else {
                    continue;
                }
            }
            t1.pass("O2C cannot be done to  Commission Wallet for Enterprise");


        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 7, groups = {FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0914() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0914", "To verify that if the format of the uploaded file is not as per the template then proper error message will be displayed.");
        try {

            String filePath = FilePath.dirFileUploads + "AutomationBulkPayOutEnterprise" + DataFactory.getTimeStamp();
            String fileName = filePath + ".txt";

            File f = new File(fileName);
            f.createNewFile();

            Login.init(t1).login(bpAdminAppL1);

            BulkPaymentPage1 page = new BulkPaymentPage1(pNode);

            page.NavigateToLink();
            page.serviceSelectText(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT);

            page.uploadFile(f.getAbsolutePath());


            String actual = BulkPaymentPage1.init(t1).verfiy_Errormessage();
            Assertion.verifyMessageContain(actual, "bulk_pay_error",
                    "Verify that only csv files can be uploaded", t1);

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 8, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0916() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0916", "To verify that even if one record in the uploaded file is successfully validated, a unique batch Id is generated.");
        try {
            User sub = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);
            Login.init(t1)
                    .login(bpAdminAppL1);

            // use benificiery as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            t1.pass("BulKPayer Admin is able to generate batch id for one record");

        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * @throws Exception
     */
    @Test(priority = 9, groups = {FunctionalTag.ECONET_UAT_5_0, FunctionalTag.ENTERPRISE_MANAGEMENT})
    public void TC_ECONET_0919() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0919", "To verify that the Bulk payer admin is able to view the progress of the approved batch requests through Enterprise Payment dashboard.");
        try {
            User sub = new User(Constants.SUBSCRIBER);

            BigDecimal txnAmount = new BigDecimal(1);

            Login.init(t1).login(entUser);

            // use beneficiary as Subscriber who is not registered in the system
            List<BulkEnterprisePaymentCSV> entPayList = new ArrayList<>();
            entPayList.add(new BulkEnterprisePaymentCSV(GlobalData.defaultProvider.ProviderId, sub.MSISDN, txnAmount.toString(),
                    "",
                    "",
                    ""));

            String fileName = EnterpriseManagement.init(t1)
                    .downloadAndUpdateEnterprisePaymentCsv(entPayList);

            String batchId = EnterpriseManagement.init(t1)
                    .initiateEnterpriseBulkPay(Constants.BULK_PAYOUT_ENTERPRISE_PAYMENT, fileName, false, false);

            EnterpriseManagement.init(t1)
                    .approveRejectEnterpriseBulkPay(entUser, batchId, txnAmount, true);

            Login.resetLoginStatus();

            Login.init(t1).login(entUser);

            EntBulkPayDashBoard_Pg1.
                    init(t1).
                    navigateToLink();

            // get all the requests and check for the batch Id
            List<WebElement> avaibaleEntry = driver.findElements(By.cssSelector(".pending-policy-list"));

            Utils.putThreadSleep(3000);
            for (WebElement elem : avaibaleEntry) {
                elem.click();
                Utils.putThreadSleep(3000);
                // check if the batch Id is matching
                String batchIdUI = driver.findElement(By.cssSelector(".bulkID")).getText();
                if (batchId.equalsIgnoreCase(batchIdUI)) {
                    t1.pass("Bulk-Payer Admin is able to view Details in Dashboard");
                    break;
                } else {
                    t1.fail("BulkPayer Admin is not able to view Details in Dashboard");
                }
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }



    @Test
    public void TC_SIT_019(){
        ExtentTest t1 = pNode.createNode("TC_SIT_019", "To verify that Grades for the enterprise cannot be created using User Profile Management module.");
        try {

            GradeAddPage page = new GradeAddPage(t1);

            Login.init(t1).loginAsSuperAdmin(Roles.ADD_GRADES);

            page.navigateToLink();
            page.clickAddButton();
            if (! page.getAllOptionsFromDomainCodeDdown().contains(Constants.ENTERPRISE)){
                t1.pass("Enterprise Domain is Not present in Domain Drop down");
                Utils.captureScreen(t1);
            }else {
                t1.fail("Enterprise Domain is present in Domain Drop down");
            }




        }catch (Exception e){
            markTestAsFailure(e,t1);
        }
        Assertion.finalizeSoftAsserts();

    }

}
