package tests.core.econet5_0;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.Grade;
import framework.entity.InstrumentTCP;
import framework.entity.MobileGroupRole;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.GradeManagement;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.systemManagement.TCPManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.ResultSet;

public class Suite_SendInternationalMoney extends TestInit {

    /**
     * Login as SuperAdmin
     * Navigate to User Profile Management > Add grade
     * Add Grade "IMT"
     * <p>
     * Login as nwAdmin
     * Create Channel User with the grade "IMT"
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.PVG_SYSTEM, FunctionalTag.IMT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0})
    public void TC_ECONET_0679() throws Exception {

        ExtentTest t1 = pNode.createNode("TC_ECONET_0679", "To verify that the Valid user " +
                "should be able to add new IMT grade user in the system to perform Send International Money.");

        t1.assignCategory(FunctionalTag.PVG_SYSTEM, FunctionalTag.IMT, FunctionalTag.ECONET_SIT_5_0, FunctionalTag.ECONET_UAT_5_0);
        try {
            boolean isMobileRoleCreated = false, isTCPCreated = false;

            if (AppConfig.isCommissionWalletRequired) {
                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_COMMISSION_WALLET_REQUIRED", "N");
            }
            Grade newGrade = new Grade(Constants.SPECIAL_SUPER_AGENT);
            newGrade.setGradeCode("IMT");
            newGrade.setGradeName("IMT");

            Login.init(t1).loginAsSuperAdmin("ADD_GRADES");
            //fetch grade status, if it doesnt exist only then create newly
            if (!MobiquityGUIQueries.fetchGradeStatus("IMT").contains("Y"))
                GradeManagement.init(t1).addGrade(newGrade);
            else
                Assertion.logAsPass("Grade Code already exist", t1);

            User chUser = new User(Constants.SPECIAL_SUPER_AGENT, newGrade.GradeName);

            //Fetch the TCP and MobileROle for grade 'IMT'
            ResultSet result = MobiquityGUIQueries.fetchTCPIdAndMobileGroupRoleCode(newGrade.GradeCode, DataFactory.getDefaultWallet().WalletId, "Wallet");

            if (result.next()) {
                //If mobile role doesnt exist for grade then create otherwise skip
                if (result.getString("GROUP_ROLE_CODE") != null) {
                    isMobileRoleCreated = true;
                    t1.info("Mobile role for grade " + newGrade.GradeName + " already exists in the system");
                    t1.info("Mobile Role Name:" + result.getString("GROUP_ROLE_CODE"));
                }

                //If TCP doesnt exist for grade then create otherwise skip
                if (result.getString("PROFILE_ID") != null) {
                    isTCPCreated = true;
                    t1.info("TCP for grade " + newGrade.GradeName + " already exists in the system");
                    t1.info("Instrument TCP Profile Name: " + result.getString("PROFILE_ID"));
                }
            }
            //if Not Mobile role exist then Add Mobile Role
            if (!isMobileRoleCreated) {
                GroupRoleManagement.init(t1).addMobileGroupRolesForSpecificGrade(chUser, newGrade.GradeName,
                        Constants.PAYINST_WALLET_CONST_MOBILE_ROLE,
                        DataFactory.getWalletUsingAutomationCode(Wallets.NORMAL).WalletName);
                MobileGroupRole mobRoleImt = new MobileGroupRole(chUser.CategoryCode, newGrade.GradeName);
                mobRoleImt.writeDataToExcel();
                GlobalData.mobileGroupRole.add(mobRoleImt);
            }
            //If not created then Add TCP
            if (!isTCPCreated) {
                InstrumentTCP tcp = new InstrumentTCP(DataFactory.getDefaultProvider().ProviderName,
                        DataFactory.getDomainName(Constants.WHOLESALER),
                        DataFactory.getCategoryName(Constants.SPECIAL_SUPER_AGENT),
                        newGrade.GradeName,
                        "WALLET", DataFactory.getDefaultWallet().WalletName);
                TCPManagement.init(t1).addInstrumentTCP(tcp);
                tcp.writeDataToExcel();
                GlobalData.instrumentTCPs.add(tcp);
            }

            //Creating User With New Grade

            chUser.setRegistrationType(Constants.REGTYPE_NO_KYC);
            ChannelUserManagement.init(t1).createChannelUserDefaultMapping(chUser, false);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        } finally {
            if (AppConfig.isCommissionWalletRequired)
                SystemPreferenceManagement.init(t1).updateSystemPreference("IS_COMMISSION_WALLET_REQUIRED", "Y");

        }
        Assertion.finalizeSoftAsserts();
    }
}