package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.savingsClubManagement.AddClub_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * TODO -  Notifications are not Received, once this issus is fixed, need to implement [22-12-2017]
 */
public class Suite_MON_3440_PromoteOrDemoteApprover extends TestInit {

    private User subsChairman, subsMember1, subsMember2;
    private SavingsClub sClub;
    private String bankId;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        // Extent Test
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Testing Promoting or demoting a club Member. " +
                "Create a chairman user for testing purpose. " +
                "Create saving club, add members and approvers and perform SVC deposit to make Club active")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // get Default Provider
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // User Objects
            subsChairman = new User(Constants.SUBSCRIBER);
            subsMember1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            subsMember2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            // Savings Club object
            sClub = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, DataFactory.getDefaultBankIdForDefaultProvider(), false, false);

            // set Min and Max number of Member and Approver
            sClub.setMemberCount(3, 3);
            sClub.setApproverCount(1, 2); // set the min approver count to 1 and max to 1

        /*
        Add Member to the Club Object
         */
            sClub.addMember(subsMember1);
            sClub.addMember(subsMember2);

            // ServiceCharge Object
            ServiceCharge cDeposit = new ServiceCharge(
                    Services.CLUB_DEPOSIT,
                    subsChairman,
                    subsMember1,
                    null,
                    savingClubWallet.WalletId,
                    null,
                    null
            );

            /**
             P R E   R E Q U I S I T E    C R E A T I O N
             */

            //Create Subscribers with Default Mapping
            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);

            //Configure Service Charge For Club Deposit
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(cDeposit);

            // Create Saving Club
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClub);

            // make sure that Users are joined and are active
            for (User member : sClub.getMembers()) {
                // Join Member 1 and make SVA deposit
                if (!member.MSISDN.equals(sClub.CMMsisdn)) {
                    Transactions.init(s1)
                            .joinOrResignSavingClub(sClub, member, true)
                            .verifyStatus(Constants.TXN_SUCCESS);
                }
            }

            // make sure that the Chairman is active
            Transactions.init(s1)
                    .depositSavingClub(sClub, subsChairman, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Try demote a Member which is not Active
     * Expect an error message
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void tryDemoteMemeberWhichIsNotActive() throws Exception {
        ExtentTest t0 = pNode.createNode("TEST 01", "tryDemoteMemeberWhichIsNotActive Try Demote the Admin")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // now try to demote
            Transactions.init(t0)
                    .svcPromoteDemoteMember(sClub, subsMember1, false)
                    .verifyMessage("savingclub.resign.add.initiate");
        } catch (Exception e) {
            markTestAsFailure(e, t0);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Activate the Member by performing SVC Deposit
     * Demote the Member, Demotion must be successful
     *
     * @throws Exception
     */
    @Test(priority = 2, groups = {FunctionalTag.MONEY_SMOKE})
    public void demoteAnActiveApprover() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 02", "demoteAnActiveApprover Promote a member to approver")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // make user active
            Transactions.init(t1)
                    .depositSavingClub(sClub, subsMember1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("savingclub.deposit.confirmation");

            Transactions.init(t1)
                    .svcPromoteDemoteMember(sClub, subsMember1, false)
                    .verifyMessage("savingclub.successfully.demoted", sClub.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Try Demoting the Chairman, Error is expected
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void tryDemoteAdmin() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 03", "Try Demoting the Chairman, Error is expected")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // now try to demote
            Transactions.init(t2)
                    .svcPromoteDemoteMember(sClub, subsChairman, false)
                    .verifyMessage("savingclub.admin.cannot.be.demoted");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Promote the Active Member
     *
     * @throws Exception
     */
    @Test(priority = 4, groups = {FunctionalTag.MONEY_SMOKE})
    public void promoteMember() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 04", "Promote a Club Member")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // now try to demote
            Transactions.init(t2)
                    .svcPromoteDemoteMember(sClub, subsMember1, true)
                    .verifyMessage("savingclub.successfully.promoted", sClub.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Reduce the Minimum number of Approver Count and try to demote the Member
     * Action must not be allowed
     *
     * @throws Exception
     */
    @Test(priority = 5, groups = {FunctionalTag.MONEY_SMOKE})
    public void demoteBelowMinApproverCount() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 05",
                "Reduce the Minimum number of Approver Count and try to demote the Member Action must not be allowed")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        // get operator User
        OperatorUser naModAppClub = DataFactory.getOperatorUsersWithAccess("MODAPP_CLUB").get(0);


        try {
             /*
            Login as Network admin and navigate to Modify Saving Club Page
             */
            Login.init(t2)
                    .login(naModAppClub);

            SavingsClubManagement.init(t2)
                    .initiateModifyClubUsingChairmanMsisdn(sClub.CMMsisdn);

            AddClub_Page1.init(t2)
                    .setMinApprovers(2)
                    .clickSubmit()
                    .clickSubmit2();
            Assertion.verifyActionMessageContain("savingclub.modified.success", "Successfully Modified Club", t2);

            // Approve Club Modification
            SavingsClubManagement.init(t2)
                    .approveClubModification(sClub);

            // now try to demote
            Transactions.init(t2)
                    .svcPromoteDemoteMember(sClub, subsMember1, false)
                    .verifyMessage("savingclub.min.approver.limit.reached");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

}
