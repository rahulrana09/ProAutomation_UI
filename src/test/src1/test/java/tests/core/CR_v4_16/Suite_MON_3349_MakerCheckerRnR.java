package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.FunctionLibrary;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalVars.GlobalData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

/**
 * Created by rahul.rana on 8/21/2017.
 */
public class Suite_MON_3349_MakerCheckerRnR extends TestInit {

    @Test(enabled = true)
    public void setup() throws SQLException, InterruptedException {
        // load the entire RoleCode and Page Code Map
        // TODO - this can be moved to TestInit or may be a separate Init file for testing RnR related Stuff

        HashMap<String, List<String>> roleMap = DataFactory.getPageCodeRoleCodeMap();

        WebDriver driver = DriverFactory.getDriver();
        FunctionLibrary fl = new FunctionLibrary(driver);
        MobiquityGUIQueries dbConn = new MobiquityGUIQueries();

        ExtentTest t1 = pNode.createNode("Verify Roles and responsibility for SuperAdmin Maker Checker");

        try {
            for (SuperAdmin admin : GlobalData.superAdmins) {
                List<String> roleList = MobiquityGUIQueries.getApplicableRoleCodes(admin.WebGroupRole);
                Login.init(t1).loginAsSuperAdmin(admin);

                List<WebElement> we = driver.findElements(By.tagName("a"));

                for (WebElement link : we) {
                    String linkText = link.getText();
                    if (!linkText.equals("Change Password") && !linkText.equals("Logout") && !linkText.equals("")) {
                        t1.info(link.getText());
                        link.click();
                        fl.contentFrame();
                        List<WebElement> we1 = driver.findElements(By.tagName("a"));
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(enabled = false)
    public void createMerchants() throws Exception {
        // Login as Network Admin and Download the Ambiguous Transaction Excel
        OperatorUser naAmbiguous = DataFactory.getOperatorUsersWithAccess("AMBGTXN_UPLOAD").get(0);


    }

}
