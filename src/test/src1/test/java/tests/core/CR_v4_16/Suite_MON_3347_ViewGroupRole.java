package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.WebGroupRole;
import framework.features.common.Login;
import framework.features.systemManagement.GroupRoleManagement;
import framework.features.userManagement.OperatorUserManagement;
import framework.pageObjects.groupRole.GroupRoleView_Page1;
import framework.pageObjects.groupRole.GroupRole_Page1;
import framework.util.common.*;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Arrays;
import java.util.List;

/**
 * Use Case: MON-3347
 * <p>
 * Verify that superadmin (maker & checker) and network admin should have Preferences > View Group Role link permission
 * Verify that superadmin (maker & checker) and network admin is able to view the group roles
 */

/**
 * Test Scenario:
 * <p>
 * Login as Super admin with view Group Role Permission, verify view Group Role Page is accessible
 * Login as Super admin without Group Role Permission, verify view Group Role Page is not accessible
 * <p>
 * Create a Group Role with role, assign it to network admin, check if it can view
 * update the Group Role, remove the role, check that Newtowk admin now cant view group role
 * <p>
 * Tear Down
 * Delete the Newly added Group Role
 */
public class Suite_MON_3347_ViewGroupRole extends TestInit {

    private OperatorUser naUser;
    private WebDriver driver;
    private WebGroupRole naRole;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        SuperAdmin saGrpRoleCreator;

        ExtentTest tSetup = pNode.createNode("Setup", "Create a Dummy Group Role for testing. " +
                "Create an Admin user with this newly created Group Role.")
                .assignCategory(FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            driver = DriverFactory.getDriver();
            saGrpRoleCreator = DataFactory.getSuperAdminWithAccess("GRP_ROL");

        /*
        Make sure that the Group role exists
        hardcoded name ensures that only one of such group role will be created
        on multiple runs the same group role could be updated
         */
            List<String> applicableRole = Arrays.asList("CHECK_ALL");
            naRole = new WebGroupRole(Constants.NETWORK_ADMIN, "AUTwROLEMON3347", applicableRole);

            Login.init(tSetup)
                    .loginAsSuperAdmin(saGrpRoleCreator);

            GroupRoleManagement.init(tSetup)
                    .addWebGroupRole(naRole);

        /*
        Now assign this web Role to a Network admin
         */
            naUser = new OperatorUser(Constants.NETWORK_ADMIN);
            naUser.setWebGroupRole(naRole.RoleName);

        /*
        Create the Network Admin
         */
            OperatorUserManagement.init(tSetup)
                    .createAdmin(naUser);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }

        Assertion.finalizeSoftAsserts();
    }

    @Test(priority = 1, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_01() throws Exception {
         /*
        Login as Network Admin, with all access
        Verify that the link to view Group Role is available
         */
        ExtentTest t1 = pNode.createNode("TEST 01", "Verify that NetworkAdmin (maker & checker)" +
                " should have Preferences > View Group Role link permission")
                .assignCategory(FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            // get any one of the Existing Group Role, in this case a base Setup Group Role
            WebGroupRole viewRole = DataFactory.getExistingWebRole(Constants.WHOLESALER);

            Login.init(t1)
                    .login(naUser);

            FunctionLibrary.init(t1)
                    .clickLink("MPREF_ALL");

            Assertion.assertEqual(driver.findElements(By.id("MPREF_GRP_ROL_VIEW")).size() > 0, true,
                    "Verify that Link to view Group Role is available", t1);

        /*
        Verify that Network admin can view a Group role
         */
            ExtentTest t2 = pNode.createNode("TEST 02", "Verify that NetworkAdmin (maker & checker) is able to view the group roles")
                    .assignCategory(FunctionalTag.GROUP_ROLE_MANAGEMENT);

            FunctionLibrary.init(t2)
                    .clickLink("MPREF_GRP_ROL_VIEW");
            Thread.sleep(2500);

            GroupRole_Page1 page = GroupRole_Page1.init(t2);
            GroupRoleView_Page1 page2 = new GroupRoleView_Page1(t2);

            page.selectDomainName(viewRole.DomainName);
            page.selectCategoryName(viewRole.CategoryName);
            page.checkWebGroupRole();
            page.clickOnSubmitView();

            Assertion.verifyEqual(page2.btnViewRole.isDisplayed(), true,
                    "Verify that View Web Group role Option is Available", t2);

            page2.selectRoleToView(viewRole.RoleName);
            page2.clickViewRoleDetails();

        /*
        Verify View Group Role
         */
            Utils.putThreadSleep(Constants.MAX_WAIT_TIME);

            Assertion.verifyEqual(page2.getGroupRoleCode(), viewRole.RoleName, "Web Group Role Code", t2);
            Assertion.verifyEqual(page2.getGroupRoleName(), viewRole.RoleName, "Web Group Role Name", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * TEST 03
     * <p>
     * Verify that NetworkAdmin (maker & checker) is Not able to view the group roles,
     * if it does not has specific Permission
     *
     * @throws Exception
     */
    @Test(priority = 2, dependsOnMethods = "test_01", groups = {FunctionalTag.MONEY_SMOKE})
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("TEST 03", "Verify that NetworkAdmin (maker & checker) is " +
                "Not able to view the group roles, if it does not has specific Permission")
                .assignCategory(FunctionalTag.GROUP_ROLE_MANAGEMENT);

        try {
            GroupRoleManagement.init(t3)
                    .removeSpecificRole(naRole, new String[]{"GRP_ROL_VIEW"});

            Login.resetLoginStatus();
            Login.init(t3)
                    .login(naUser);

            FunctionLibrary.init(t3)
                    .clickLink("MPREF_ALL");

            Thread.sleep(2500);
            Assertion.assertEqual(driver.findElements(By.id("MPREF_GRP_ROL_VIEW")).size() == 0, true,
                    "Verify that Link to view Group Role is Not available", t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();

    }
}
