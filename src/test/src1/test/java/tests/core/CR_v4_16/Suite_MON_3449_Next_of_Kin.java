package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.*;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.*;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;


/**
 * Created by ravindra.dumpa on 11/6/2017.
 */
public class Suite_MON_3449_Next_of_Kin extends TestInit {

    private User usrAddSubs;
    private UserFieldProperties fields;
    private MobiquityGUIQueries dbHandler;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup", "setup specific for this script");
        try {
            dbHandler = new MobiquityGUIQueries();
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_AGE_LIMIT_REQUIRE", "Y");

            usrAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD", Constants.WHOLESALER);

            // for property file
            fields = new UserFieldProperties();
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying kin fields on subscriber registration page
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Verifying Kin fields on subscriber registration page");

        try {
            //Login as Channel with add Subs Role
            Login.init(t1)
                    .login(usrAddSubs);

            CommonChannelUserPage.init(t1)
                    .navigateToAddSubscriberPage();

            Utils.captureScreen(t1);

            verifyIfLabelExists(UserFieldProperties.getField("kin.first.name"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.contact.num"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.dob"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.id.number"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.last.name"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.middle.name"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.nationality"), t1);
            verifyIfLabelExists(UserFieldProperties.getField("kin.relationship"), t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


    /**
     * Verifying kin fields in subscriber modification page
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Verifying Kin Fields on subscriber Modification page");

        try {
            SuperAdmin opUser = DataFactory.getSuperAdminWithAccess("CAT_PREF");

            WalletPreference wPref = new WalletPreference(Constants.SUBSCRIBER,
                    Constants.REGTYPE_NO_KYC,
                    DataFactory.getDefaultWallet().WalletName, true,
                    null, null);

            Login.init(t2).loginAsSuperAdmin(opUser);

            Preferences.init(t2)
                    .configureWalletPreferences(wPref);

            //Login as Channel user whose having add subscriber role

            User chUsersubsModifier = DataFactory.getChannelUserWithAccess("SUBSMOD", 0);

            Login.init(t2)
                    .login(chUsersubsModifier);

            //Creating subscriber object
            User subuser = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

            //navigating subscriber modification page

            SubscriberManagement.init(t2)
                    .initiateSubscriberModification(subuser);
            Utils.captureScreen(t2);
            //Verifying kin fields
            verifyIfLabelExists(UserFieldProperties.getField("kin.first.name"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.contact.num"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.dob"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.id.number"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.last.name"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.middle.name"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.nationality"), t2);
            verifyIfLabelExists(UserFieldProperties.getField("kin.relationship"), t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verifying kin fields in bulk subscriber registration csv file
     * Verifying kin fields are optional in bulk subscriber registration
     *
     * @throws Exception
     */

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Verify the kin details bulk sub csv file");
        try {
            /**
             * login as operator user
             * go to bulk subscriber registration and download template
             * verify kin details and pass the valuse with out * to verify kin fileds are optional
             */
            OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

            Login.init(t3).login(dwFileUser);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t3);

            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectSubscriberRegistrationService();

            BulkChUserAndSubRegAndModPage.clickOnStartDownload();
            Utils.captureScreen(t3);
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin First Name");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin Middle Name");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin Last Name");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Relationship");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Nationality");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin IdNumber");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin First Name");
            BulkChUserAndSubRegAndModPage.init(t3).verifyFieldInCsvFile("BulkSubRegId", "Kin First Name");
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Verifying kin fields in self subscriber registration api
     * Verifying kin fields are stored database
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04", "Verifying kin details subscriber self registration");
        try {
            /**
             * Create a subscriber user object
             * set date of birth to less tha age limit
             * create a kin user object and assign it to the subscriber
             * hhit the API, Error message should be Customer age is below permitted age.
             */
            User subsUser_04 = new User(Constants.SUBSCRIBER);
            subsUser_04.setDateOfBirth(new DateAndTime().getDate(-((365 * AppConfig.custAgeLimit) - 5)));//TODO caliculate leaf years

            KinUser neKinUser = new KinUser();
            subsUser_04.setKinUser(neKinUser);
            subsUser_04.KinUser.Nationality = "IN";//todo-have to do for both web and api

            //self registering subscriber with kin details
            Transactions.init(t4).startNegativeTest()
                    .selfRegistrationForSubscriberWithKinDetails(subsUser_04)
                    //Verifying status of the subscriber registration
                    .assertStatus("770155");
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();


    }

    /**
     * Verifying kin fields in subscriber registration from AS400 in api
     * Verifying kin fields are stored database
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_05", "Verifying Kin Details while Customer Registration from AS400 ");
        try {
            /**
             * Create a subscriber user object
             * set date of birth to less than age limit
             * create a kin user object and assign it to the subscriber
             * hit the API, Error message should be Customer age is below permitted age.
             */
            User subsUser_05 = new User(Constants.SUBSCRIBER);
            subsUser_05.setDateOfBirth(new DateAndTime().getDate(-((365 * AppConfig.custAgeLimit) - 5)));//TODO caliculate leaf years
            KinUser neKinUser = new KinUser();
            subsUser_05.setKinUser(neKinUser);
            subsUser_05.KinUser.Nationality = "IN";//todo-have to do for both web and api

            //self registering subscriber with kin details
            Transactions.init(t5).startNegativeTest()
                    .subRegistrationByAS400(subsUser_05)
                    //Verifying status of the subscriber registration
                    .assertStatus("770155");
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying kin fields in self subscriber registration api
     * Verifying kin fields are stored database
     *
     * @throws Exception
     */
    @Test(priority = 6)
    public void Test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test_06", "Verifying Kin Details while Subscriber Registration by channel user ");
        /**
         * Create channel user object and pas his msisdn
         * Create a subscriber user object
         * set date of birth to less than 10 yrs
         * enter kin details
         * hit the api,Error message should be Customer age is below permitted age.
         */

        try {
            User subsUser_06 = new User(Constants.SUBSCRIBER);
            subsUser_06.setDateOfBirth(new DateAndTime().getDate(-((365 * AppConfig.custAgeLimit) - 5)));//TODO calculate leaf years


            KinUser neKinUser = new KinUser();
            subsUser_06.setKinUser(neKinUser);
            subsUser_06.KinUser.Nationality = "IN";//todo-have to do for both web and api


            //self registering subscriber with kin details
            startNegativeTest();
            Transactions.init(t6)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subsUser_06)
                    //Verifying status of the subscriber registration
                    .assertStatus("770155");
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying kin fields are optional in web
     *
     * @throws Exception
     */

    @Test(priority = 7)
    public void Test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("Test_07", "Verifying Kin Details in web are optional");
        /**
         * Create subscriber object
         * Create a subscriber with out kin details
         * verify the error message
         */

        try {
            User subs = new User(Constants.SUBSCRIBER);

            SubscriberManagement.init(t7).createSubscriber(subs);

            Assertion.verifyEqual(Assertion.isErrorInPage(t7), false,
                    "Verify if Kin details are optional", t7);
        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying kin fields are optional in bulk
     *
     * @throws Exception
     */
    @Test(priority = 8)
    public void Test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("Test_08", "Verifying Kin Details are optional in USSD");

        /**
         * Create channel user object and pas his msisdn
         * Create a subscriber user object
         * set date of birth to more than 10 yrs
         * don't enter kin details
         * hit the api,it should be successful
         */
        try {
            User subsUser_08 = new User(Constants.SUBSCRIBER);

            subsUser_08.setDateOfBirth(new DateAndTime().getDate(-((365 * AppConfig.custAgeLimit) - 5)));//TODO caliculate leaf years

            //self registering subscriber with kin details
            startNegativeTest();
            Transactions.init(t8)
                    .SubscriberRegistrationByChannelUserWithKinDetails(subsUser_08)
                    //Verifying status of the subscriber registration
                    .assertStatus("770155");
        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Verifying kin fields are stored database
     *
     * @throws Exception
     */
    @Test(priority = 9)
    public void Test_09() throws Exception {
        /**
         * Create a subscriber user object
         * set date of birth to more than age limit
         * Enter kin details
         * Create Subscriber and check in database using msisdn
         */
        ExtentTest t09 = pNode.createNode("Test_09", "Verifying Kin Details in DB whatever we enter subscriber registration");

        try {
            User subs = new User(Constants.SUBSCRIBER);

            KinUser neKinUser = new KinUser();

            subs.setKinUser(neKinUser);

            SubscriberManagement.init(t09).createSubscriber(subs);

            Assertion.assertEqual(MobiquityGUIQueries.checkMsisdnExistMobiquity(subs.MSISDN), true, "Verify Subscriber registered in db", t09);
        } catch (Exception e) {
            markTestAsFailure(e, t09);
        }
        Assertion.finalizeSoftAsserts();

    }

    private void verifyIfLabelExists(String labelName, ExtentTest t1) throws IOException {
        try {
            if (DriverFactory.getDriver()
                    .findElement(By.xpath("//*[contains(text(),'" + labelName + "')]")).isDisplayed()) {
                t1.pass("Successfuuly verified the Label for element - " + labelName);
            } else {
                t1.fail("Failed to verify the label for element -" + labelName);
                Utils.captureScreen(t1);
            }
        } catch (Exception e) {
            Assertion.raiseExceptionAndContinue(e, t1);
        }

    }
}