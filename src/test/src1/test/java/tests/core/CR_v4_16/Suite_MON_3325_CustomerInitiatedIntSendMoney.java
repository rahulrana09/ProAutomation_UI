package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.apiManagement.Transactions;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.ChurnUser_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.CommonOperations;
import org.junit.Ignore;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static framework.features.apiManagement.Transactions.validateResponseMessageForAmbiguousTransaction;

/**
 * Created by shubham.kumar1
 * <p>
 * Use Case: MON-3325
 * <p>
 * To verify that when transaction is ambiguous admin should be able to download the ambiguous transactions from the UI and set the transaction status as:
 * 1. success after which amount in receiver FIC should be moved to available balance
 * 2. failed transaction after which amount in receiver FIC should be moved back to sender available balance
 * To verify that If subscriber is delete initiated after the transaction is moved to ambiguous none of the parties involved in the transaction can be:
 * 1. delete initiated
 * 2. churn initiated
 */
@Ignore
public class Suite_MON_3325_CustomerInitiatedIntSendMoney extends TestInit {

    User remittancePartner, subsDeleteChurnInitiate, subsDeleteInitiator, subs;
    CurrencyProvider defaultProvider;
    MobiquityGUIQueries dbCon;

    /**
     * Setup
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest t1 = pNode.createNode("Setup Specific for this Test");
        dbCon = new MobiquityGUIQueries();
        remittancePartner = DataFactory.getChannelUserWithCategory(Constants.HEAD_MERCHANT);
        subsDeleteInitiator = DataFactory.getChannelUserWithAccess("SUBSDEL");
        defaultProvider = DataFactory.getDefaultProvider();
        subs = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
        subsDeleteChurnInitiate = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

        //setting the third party response to be one minute
        MobiquityGUIQueries.updateRetryIntervalForThirdParty("1", t1);

        // Make sure parties involved in test cases Have Enough Balance
        TransactionManagement.init(t1)
                .makeSureLeafUserHasBalance(subs)
                .makeSureLeafUserHasBalance(subsDeleteChurnInitiate);

        ServiceCharge tRule = new ServiceCharge(Services.INTERNAT_SEND_MONEY, subsDeleteChurnInitiate, remittancePartner, null, null, null, null);
        TransferRuleManagement.init(t1)
                .configureTransferRule(tRule);

        // Delete all pricing policy S Charge
        CommonOperations.deleteAllPricingPolicies();
    }

    @Test(priority = 1)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TS() throws Exception {

        ExtentTest t1 = pNode.createNode("MON-3325/TEST-AMBIGUOUS_TRANSACTION_SET_STATUS_TS", "success transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");

        ExtentTest t2 = t1.createNode("TEST", "To verify that when transaction is ambiguous admin should be able to " +
                " download the ambiguous transactions from the UI and set the transaction status as success");

        /**
         * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        BigDecimal amountTx = new BigDecimal(2);

        SfmResponse response = Transactions.init(t1)
                .initiateRemittancePartnerPayment(remittancePartner, subs, amountTx);

        /**
         * Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        AmbiguousTransactionManagement.init(t1).
                initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTx);


        AmbiguousTransactionManagement.init(t2)
                .downloadAmbiguousTxSheet(remittancePartner)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, remittancePartner.MSISDN)
                .setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, remittancePartner);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        /**
         * Check Balance & Db Status
         */
        AmbiguousTransactionManagement.init(t2)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        AmbiguousTransactionManagement.init(t2).
                initiatePostProcessBalanceValidation(true, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTx);
    }

    /**
     * Test Ambiguous Transaction with Set Status as 'TF' FAILURE
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void AMBIGUOUS_TRANSACTION_SET_STATUS_TF() throws Exception {
        ExtentTest t3 = pNode.createNode("MON-3325/TEST-AMBIGUOUS_TRANSACTION_SET_STATUS_TF", "Failed transactions the FIC amount in the receiver account should me moved\n" +
                " to available balance and there should be no reconciliation mismatch");
        ExtentTest t4 = t3.createNode("TEST", "To verify that when transaction is ambiguous admin should be able to " +
                " download the ambiguous transactions from the UI and set the transaction status as failed");

        /**
         * Initiate merchant Payment, Transaction Status Should Go to Ambiguous
         */
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        BigDecimal amountTx = new BigDecimal(2);

        SfmResponse response = Transactions.init(t3)
                .initiateRemittancePartnerPayment(remittancePartner, subs, amountTx);

        /**
         * Check Balance
         */
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        AmbiguousTransactionManagement.init(t3).
                initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, amountTx);

        AmbiguousTransactionManagement.init(t4)
                .downloadAmbiguousTxSheet(remittancePartner)
                .verifyAmbiguousTransactionFile(response, subs.MSISDN, remittancePartner.MSISDN)
                .setTransactionStatus(response, false)
                .uploadUpatedAmbiguousTxnFile(response, remittancePartner);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed
        /**
         * Check Balance and DB Process Status
         */
        AmbiguousTransactionManagement.init(t4)
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

        UsrBalance subPostProcessBal = MobiquityGUIQueries.getUserBalance(subs, null, null);
        UsrBalance merPostProcessBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        AmbiguousTransactionManagement.init(t4).
                initiatePostProcessBalanceValidation(false, subPreBal, merPreBal, subPostProcessBal, merPostProcessBal, amountTx);
    }

    /**
     * @throws Exception
     */
    @Test(priority = 3)
    public void deleteChurnInitiateUserValidationCheckForAmbigiousTransaction() throws Exception {
        /*                                 MONEY 5.0 MON-3325
            To verify  none of the parties involved in the transaction can be delete initiated
             after the transaction is moved to ambiguous
         */
        ExtentTest t5 = pNode.createNode(" MON-3325/TEST-deleteInitiateUserValidationCheckForAmbigiousTransaction", "To verify that If subscriber is delete initiated after the transaction is moved to ambiguous \n" +
                "none of the parties involved in the transaction can be delete initiated ");

        //fetching user balance before transaction
        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subsDeleteChurnInitiate, null, null);
        UsrBalance merPreBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);
        BigDecimal txnAmt = new BigDecimal(2);


        SfmResponse response = Transactions.init(t5)
                .initiateRemittancePartnerPayment(remittancePartner, subsDeleteChurnInitiate, txnAmt);

        String transactionId = response.TransactionId;
        validateResponseMessageForAmbiguousTransaction(response.validatableResponse, "International Send Money", transactionId, "en");

        //fetching user balance after ambigious transaction
        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subsDeleteChurnInitiate, null, null);
        UsrBalance merPostBal = MobiquityGUIQueries.getUserBalance(remittancePartner, null, null);

        AmbiguousTransactionManagement.init(t5).
                initiateAmbiguousBalanceValidation(subPreBal, merPreBal, subPostBal, merPostBal, txnAmt);

        /*
            Verify that User Could Not be Delete Initiated as it has Transaction In Pending state
             */
        SubscriberManagement.init(t5)
                .startNegativeTest()
                .initiateDeleteSubscriber(subsDeleteChurnInitiate, subsDeleteInitiator, defaultProvider.ProviderId);

        Assertion.verifyErrorMessageContain("subs.error.pending.txn.exists", "Verify Payee Could not be Delete Initiate", t5);

        ExtentTest t6 = pNode.createNode(" MON-3325/TEST-churnInitiateUserValidationCheckForAmbigiousTransaction", "To verify that If subscriber is delete initiated after the transaction is moved to ambiguous \n" +
                "none of the parties involved in the transaction can be churn initiated");

        /*
        Verify that User Could Not be Churn Initiated as it has Transaction In Pending state
         */
        CommonUserManagement.init(t6)
                .churnInitiateUser(subsDeleteChurnInitiate);
        ChurnUser_Page1.init(t6).checkLogFileSpecificMessage(subsDeleteChurnInitiate.MSISDN, "not.churn.channel.existing.transaction");

    }

}