package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.bankMaster.AddBank_Page1;
import framework.pageObjects.bankMaster.DeleteBank_Page1;
import framework.pageObjects.stockManagement.StockInitiation_Page1;
import framework.pageObjects.stockManagement.StockWithdrawal_page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;


/**
 * Addition of Banks as Trust and Non Trust: MON-3346
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 1        Done            BaseSetup
 * 3        Done            test_01: Positive case is covered in base Setup - stockToSystemWallet()
 * 6        Done            test_02: Positive case is covered in Suite_MON_3469_Stock_Withdrawal > stockWithdrawalBalanceCheck()
 * 9        Done            test_03
 * 12-15    Done            BaseSetup
 * 15       Pending
 * 17       Done            BaseSetup
 * 19       Pending
 * 20       Pending
 */
public class Suite_MON_3346_AddBankTrustNonTrust_Y extends TestInit {
    private CurrencyProvider defaultProvider;
    private SuperAdmin saAddBank, saDeleteBank;
    private String nonTrustBank;

    @AfterClass(alwaysRun = true)
    public void teardown() {
        AppConfig.init(); // Get the latest DB Map for MTX_PREFERENCE_TABLE and load to Global Data
    }

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Specific to test Add Bank Trust/ Non Trust. " +
                "Update the Systemn preference IS_BANKID_REQ = Y. " +
                "Check for Sytem Preference is Bank ID required and update the preference IS_BANKID_REQ based on the result")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        AppConfig.init(); // Get the latest DB Map for MTX_PREFERENCE_TABLE

        // Make sure that the Preference is set yo Y

        if (!AppConfig.isBankIdRequired) {
            MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_BANKID_REQ", "Y");

            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("IS_BANKID_REQ", "Y");
        } else {
            s1.pass("IS_BANKID_REQ is already set to 'Y'");
        }

        if (!AppConfig.isBankBranchCSVUploadRequired) {
            MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_FILE_UPLOAD_REQ", "Y");

            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("IS_FILE_UPLOAD_REQ", "Y");
        } else {
            s1.pass("IS_FILE_UPLOAD_REQ is already set to 'Y'");
        }

        // Users For Test
        saAddBank = DataFactory.getSuperAdminWithAccess("BANK_ADD");
        saDeleteBank = DataFactory.getSuperAdminWithAccess("BANK_DELETE");
        defaultProvider = DataFactory.getDefaultProvider();
        nonTrustBank = DataFactory.getNonTrustBankName(defaultProvider.ProviderName);
    }

    /**
     * Verify that system is able to do stock initiation
     * from the trust bank only and balance in the trust bank is
     * always negative after any transaction when banks created
     * when system preference IS_FILE_UPLOAD_REQ = Y & IS_BANKID_REQ = Y
     */
    @Test
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Verify that Non Trust Bank is not available on Stock initiation Page")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        OperatorUser netAdmin = DataFactory.getOperatorUsersWithAccess("O2C_INIT", Constants.NETWORK_ADMIN).get(0);
        Login.init(t1)
                .login(netAdmin);

        List<String> bankList = StockInitiation_Page1.init(t1)
                .navigateToStockInitiationPage()
                .selectProviderName(defaultProvider.ProviderName)
                .getBankNameOptionsValues();

        Assertion.verifyListNotContains(bankList, nonTrustBank,
                "Verify that Non Trust Bank is not available on Stock initiation Page", t1);
    }

    /**
     * Verify that Stock Initiation from a NON trust bank is not Allowed
     *
     * @throws Exception
     */
    @Test
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Verify that Stock Initiation from a NON trust bank is not Allowed")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        OperatorUser stockWithdrawUser = DataFactory.getOperatorUsersWithAccess("STK_WITHDRAW").get(0);
        Login.init(t2)
                .login(stockWithdrawUser);

        List<String> bankList = new StockWithdrawal_page1(t2)
                .navToStockWithdrawalPage()
                .getBankNameOptionsValues();

        Assertion.verifyListNotContains(bankList, nonTrustBank,
                "Verify that Non Trust Bank is not available on Stock Withdrawal Page", t2);
    }

    /**
     * Verify that Bank Id shall not exceed BANK_ID_LENGTH
     *
     * @throws Exception
     */
    @Test
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Verify Bank Id Length shall not exceed BANK_ID_LENGTH")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        Login.init(t3)
                .loginAsSuperAdmin(saAddBank);

        new AddBank_Page1(t3)
                .navAddBank();

        // verify that the max length is equal to the BANK_ID_LENGTH
        Assertion.verifyEqual(Integer.parseInt(new AddBank_Page1(t3).BankId.getAttribute("maxlength")), AppConfig.bankIdLength,
                "Verify that Bank Id shall not exceed BANK_ID_LENGTH", t3);

    }

    /**
     * Verify that banks created pool account number
     * should Not display under Bank Master > Delete Bank screen for deletion
     *
     * @throws Exception
     */
    @Test
    public void test_04() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_04", "Verify that banks created pool account number " +
                "should Not display under Bank Master > Delete Bank screen for deletion")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        Login.init(t3)
                .loginAsSuperAdmin(saDeleteBank);

        String bankName = DataFactory.getTrustBankName(defaultProvider.ProviderName);

        // navigate and get the list of banks available for deletion
        List<String> banksForDeletion = new DeleteBank_Page1(pNode)
                .navDeleteBank()
                .getBankOptionsfromSelect();

        // Verify that the bank which was creted with the Pool account number is not available
        Assertion.verifyListNotContains(banksForDeletion, bankName,
                "Verify that the bank created with Pool Account number is not available for deletion", t3);

    }


}
