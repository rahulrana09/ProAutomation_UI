package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.SfmResponse;
import framework.dataEntity.UsrBalance;
import framework.entity.OperatorUser;
import framework.entity.SuperAdmin;
import framework.entity.SystemBankAccount;
import framework.features.apiManagement.AmbiguousTransactionManagement;
import framework.features.common.Login;
import framework.features.stockManagement.StockManagement;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.pageObjects.bankMaster.ModifyServiceProviderBankAccount_Pg1;
import framework.pageObjects.stockManagement.StockWithdrawal_page1;
import framework.pageObjects.stockManagement.StockWithdrawal_page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.propertiesManagement.MessageReader;
import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.*;


/**
 * Created by shubham.kumar1
 * <p>
 * <p>
 * Use Case: MON-3469
 * <p>
 * To verify that when the sender wallet type is selected in the UI
 * and clicked on available balance then only available balance should be displayed .i.e. excluding FIC and Frozen
 * To verify that when the sender wallet type is selected in the UI then transaction initiated balance should display only FIC and Frozen amount of the sender wallet
 * To verify that on successful initiation of stock withdrawal Appropriate message should be displayed in the UI with the transaction ID
 * To verify that If transaction is moved to ambiguous the admin should be able to download this ambiguous transaction and upload as:
 * 1. successful transaction after which amount in receiver FIC should be moved to available balance
 * 2. failed transaction after which amount in receiver FIC should be moved back to sender available balance
 **/
public class Suite_MON_3469_Stock_Withdrawal extends TestInit {

    private OperatorUser stockWithdrawUser;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        stockWithdrawUser = DataFactory.getOperatorUsersWithAccess("STK_WITHDRAW").get(0);

        setUMSProperties(of(
                "stock.withdrawl.offline.online.flag", "ONLINE"
        ));

    }

    @Test(priority = 1)
    public void stockWithdrawalBalanceCheck() throws Exception {

        ExtentTest t1 = pNode.createNode("stockWithdrawalBalanceCheck", "To verify that on when the sender wallet type is selected in the UI and clicked on available balance then only available balance should be displayed .i.e. excluding FIC and Frozen AND " +
                "To verify that on when the sender wallet type is selected in the UI then transaction initiated balance should display only FIC and Frozen amount of the sender wallet");

        //need to check whether any bank account exists in the system or not
        //if no bank account exists we need to create a account first

        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();

        SuperAdmin saAddBank = DataFactory.getSuperAdminWithAccess("OPT_BANK_ACC_MOD");
        Login.init(t1)
                .loginAsSuperAdmin(saAddBank);

        ModifyServiceProviderBankAccount_Pg1 page = new ModifyServiceProviderBankAccount_Pg1(pNode);
        try {
            //first checking if any bank account exists
            page.navModifyServiceProviderBankAc();
            page.selectBank(defaultBank.BankName);
            page.clickSubmit();

            if (Assertion.isErrorInPage(t1)) {
                //this means no bank account exists in the system so we need to add a bank account in the system
                CurrencyProviderMapping.init(t1)
                        .addServiceProviderBankAccount(defaultBank.BankName, RandomStringUtils.randomNumeric(9));
            } else {
                t1.info("Account exists in the system to which we can withdraw the stocks");
            }

            Login.init(t1).login(stockWithdrawUser);
            try {
                StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(t1);
                stockWithdrawl_page1.navToStockWithdrawalPage();
                stockWithdrawl_page1.walletID_Select("IND03");
                stockWithdrawl_page1.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
                stockWithdrawl_page1.selectBankName(defaultBank.BankName);
                stockWithdrawl_page1.bankAccno_SelectDefault();
                stockWithdrawl_page1.setTransferAmount("10");
                //take out balances of IND03
                //[gurudatta][29 Mar2018][commented below line and added getWalletBalanceForOperatorUsersBasedOnWalletNumber][v4.16]
                //UsrBalance initialBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
                UsrBalance initialBalanceIND03 = getWalletBalanceForOperatorUsersBasedOnWalletNumber(defaultProvider.ProviderId + "IND03");
                BigDecimal bal = initialBalanceIND03.FIC.add(initialBalanceIND03.frozenBalance);
                stockWithdrawl_page1.availableBalanceLinkCheck(initialBalanceIND03.Balance.toString());
                stockWithdrawl_page1.transactionInitiatedBalanceCheck(bal.toString());
            } catch (Exception e) {
                e.printStackTrace();
                Assertion.raiseExceptionAndStop(e, t1);
            }

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t1);
        }

    }


    @Test(priority = 2)
    public void stockWithdrawal() throws Exception {

        ExtentTest t2 = pNode.createNode("stockWithdrawal", "To verify that on successful initiation of stock withdrawal Appropriate message should be displayed in the UI with the transaction ID");

        Login.init(t2).login(stockWithdrawUser);

        StockManagement.init(t2)
                .stockWithdrawal("10", "IND03");

    }

    @Test(priority = 3)
    public void stockWithdrawalSetTransactionStatusTS() throws Exception {

        ExtentTest t3 = pNode.createNode("stockWithdrawalSetTransactionStatusTS", " To verify that If transaction is moved to ambiguous the admin should be able to download this ambiguous transaction" +
                " and upload as successful transaction after which amount in receiver FIC should be moved to available balance");

        Login.init(t3).login(stockWithdrawUser);
        String txnAmount = "2";

        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
        SystemBankAccount receiverWallet = new SystemBankAccount(defaultBank.BankName, "IND03");


        //GET WALLET BALANCE BEFORE TRANSACTION
        UsrBalance initialBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance initialBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(t3);
        StockWithdrawal_page2 stockWithdrawl_page2 = new StockWithdrawal_page2(t3);
        try {
            stockWithdrawl_page1.navToStockWithdrawalPage();
            stockWithdrawl_page1.walletID_Select("IND03");
            stockWithdrawl_page1.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
            stockWithdrawl_page1.selectBankName(defaultBank.BankName);
            stockWithdrawl_page1.bankAccno_SelectDefault();
            stockWithdrawl_page1.setTransferAmount(txnAmount);
            stockWithdrawl_page1.submit_Click();
            Thread.sleep(800);
            stockWithdrawl_page2.confirm_Click();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t3);
        }

        String msg = Assertion.getActionMessage();
        String txnMsg = msg.split("SO")[1].trim().split(" ")[0].trim();
        String txnId = "SO".concat(txnMsg);
        t3.info("Transaction ID :" + txnId);

        String actual = Assertion.getActionMessage();
        String expectedMsg = MessageReader.getDynamicMessage("stock.withdraw.ambigious", txnId);
        Assertion.verifyEqual(actual, expectedMsg, "Stock withdraw status", t3);

        //SfmResponse response = Transactions.init(t2).initiateStockWithdrawal(stockWithdrawUser, txnAmount, receiverWallet.WalletCode);

        SfmResponse response = new SfmResponse(txnId);
        response.ServiceFlow = ""; //doing this is mandatory  else null pointer excp
        // would come as we are checking for serviceflow in setTransactionStatus()

        //GET WALLETS BALANCE AFTER AMBIGIOUS TRANSACTION
        UsrBalance ambiguousBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance ambiguousBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        // Verify Balances in Ambiguous State
        AmbiguousTransactionManagement ambiguousTransactionManagement = AmbiguousTransactionManagement.init(t3);
        ambiguousTransactionManagement
                .initiateAmbiguousBalanceValidation(initialBalanceIND03, initialBalanceIND04, ambiguousBalanceIND03, ambiguousBalanceIND04, new BigDecimal(txnAmount), true);

        AmbiguousTransactionManagement
                .init(t3)
                .downloadAmbiguousTxSheet(receiverWallet)
                .verifyAmbiguousTransactionFileForBank(response, false)
                .setTransactionStatus(response, true)
                .uploadUpatedAmbiguousTxnFile(response, receiverWallet);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        //     Check Balance & Db Status
        ambiguousTransactionManagement
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, true);

        UsrBalance postBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance postBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);


        ambiguousTransactionManagement.
                initiatePostProcessBalanceValidation(true, initialBalanceIND03, initialBalanceIND04, postBalanceIND03, postBalanceIND04, new BigDecimal(txnAmount));


    }

    @Test(priority = 4)
    public void stockWithdrawalSetTransactionStatusTF() throws Exception {

        ExtentTest t4 = pNode.createNode("stockWithdrawalSetTransactionStatusTF", "To verify that If transaction is moved to ambiguous the admin should be able to download this ambiguous transaction " +
                "and upload as failed transaction after which amount in receiver FIC should be moved back to sender available balance");

        Login.init(t4).login(stockWithdrawUser);
        String txnAmount = "5";

        CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
        SystemBankAccount receiverWallet = new SystemBankAccount(defaultBank.BankName, "IND03");


        //GET WALLET BALANCE BEFORE TRANSACTION
        UsrBalance initialBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance initialBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        StockWithdrawal_page1 stockWithdrawl_page1 = new StockWithdrawal_page1(t4);
        StockWithdrawal_page2 stockWithdrawl_page2 = new StockWithdrawal_page2(t4);

        try {
            stockWithdrawl_page1.navToStockWithdrawalPage();
            stockWithdrawl_page1.walletID_Select("IND03");
            stockWithdrawl_page1.selectProviderByValue(DataFactory.getDefaultProvider().ProviderId);
            stockWithdrawl_page1.selectBankName(defaultBank.BankName);
            stockWithdrawl_page1.bankAccno_SelectDefault();
            stockWithdrawl_page1.setTransferAmount(txnAmount);
            stockWithdrawl_page1.submit_Click();
            Thread.sleep(200);
            stockWithdrawl_page2.confirm_Click();
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t4);
        }

        String msg = Assertion.getActionMessage();
        String txnMsg = msg.split("SO")[1].trim().split(" ")[0].trim();
        String txnId = "SO".concat(txnMsg);
        t4.info("Transaction ID :" + txnId);

        String actual = Assertion.getActionMessage();
        String expectedMsg = MessageReader.getDynamicMessage("stock.withdraw.ambigious", txnId);
        Assertion.verifyEqual(actual, expectedMsg, "Stock withdraw status", t4);

        //SfmResponse response = Transactions.init(t2).initiateStockWithdrawal(stockWithdrawUser, txnAmount, receiverWallet.WalletCode);

        SfmResponse response = new SfmResponse(txnId);
        response.ServiceFlow = ""; //doing this is mandatory  else null pointer excp
        // would come as we are checking for serviceflow in setTransactionStatus()

        //GET WALLETS BALANCE AFTER AMBIGIOUS TRANSACTION
        UsrBalance ambiguousBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance ambiguousBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);

        // Verify Balances in Ambiguous State
        AmbiguousTransactionManagement ambiguousTransactionManagement = AmbiguousTransactionManagement.init(t4);
        ambiguousTransactionManagement
                .initiateAmbiguousBalanceValidation(initialBalanceIND03, initialBalanceIND04, ambiguousBalanceIND03, ambiguousBalanceIND04, new BigDecimal(txnAmount), true);

        AmbiguousTransactionManagement
                .init(t4)
                .downloadAmbiguousTxSheet(receiverWallet)
                .verifyAmbiguousTransactionFileForBank(response, false)
                .setTransactionStatus(response, false)
                .uploadUpatedAmbiguousTxnFile(response, receiverWallet);

        Thread.sleep(120000); // wait for the Ambiguous Transaction to Get effective and processed

        //     Check Balance & Db Status
        ambiguousTransactionManagement
                .dbVerifyAmbiguousIsProcessed(response.TransactionId, false);

        UsrBalance postBalanceIND03 = getBalanceForOperatorUsersBasedOnUserId("IND03");
        UsrBalance postBalanceIND04 = getBalanceForOperatorUsersBasedOnUserId(receiverWallet.BankId);


        ambiguousTransactionManagement.
                initiatePostProcessBalanceValidation(false, initialBalanceIND03, initialBalanceIND04, postBalanceIND03, postBalanceIND04, new BigDecimal(txnAmount));

    }

}