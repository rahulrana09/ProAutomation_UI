package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.OperatorUser;
import framework.entity.SavingsClub;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.savingsClubManagement.AddClub_Confirm_Page1;
import framework.pageObjects.savingsClubManagement.AddClub_Page1;
import framework.pageObjects.savingsClubManagement.ApproveClub_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Modify Club: MON-3440
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 1       Done            Test_05
 * 2       Done            Test_04
 * 3       OPEN
 * 4       Done
 * 6       Done
 * 7       Done            Test_01
 * 8       Done            Test_02
 * 9       Done
 * 10      Done
 */
public class Suite_MON_3440_ModifySavingClub extends TestInit {
    private SavingsClub sClubModify;
    private OperatorUser naModAppClub, naModClub;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Testing Modify Savings Club." +
                "Update the System Preference ADD_MAKER_REQ_CLUB = Y. " +
                "Create Chairman for Testing. " +
                "Create Saving Club For testing Saving Club Modification")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {

            // get Default Provider
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            /*
            Required Operator Users
             */
            naModClub = DataFactory.getOperatorUserWithAccess("MOD_CLUB");
            naModAppClub = DataFactory.getOperatorUserWithAccess("MODAPP_CLUB");

            User subsChairman = new User(Constants.SUBSCRIBER);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            /*
            Create Subscribers with Default Mapping
             */
            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);

            // Savings Club object, with default Member count and Approved Count : 1, 1
            sClubModify = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, DataFactory.getDefaultBankIdForDefaultProvider(), false, false);

            // Set the Member and Approver Count
            sClubModify.setMemberCount(1, 2);
            sClubModify.setApproverCount(1, 2);

            /*
            Create the Savings Club
             */
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClubModify);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 01:
     * Verify that network admin is able to access the club for modification by providing only Club ID
     *
     * @throws Exception
     */
    @Test(priority = 1, groups = {FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0})
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test 01", "Verify that network admin is able to access " +
                "the club for modification by providing only Club ID ")
                .assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0);

        try {
            /*
            Login as Network admin and navigate to Modify Saving Club Page
             */
            Login.init(t1)
                    .login(naModClub);

            SavingsClubManagement.init(t1)
                    .initiateModifyClubUsingClubId(sClubModify.ClubId);

            /*
            Verify if Modify is successfully Initiated
             */
            AddClub_Page1 page = AddClub_Page1.init(t1);
            if (page.clubName.isDisplayed() && page.clubName.getAttribute("value").equals(sClubModify.ClubName)) {
                t1.pass("Successfully able to initiate Club Modification by just providing Club ID");
            } else {
                t1.fail("Failed to initiate Club Modification by just providing Club ID");
            }
            Utils.captureScreen(t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 02
     * Verify that network admin is able to access the club for modification by providing only MSISDN of Chairman
     */
    @Test(priority = 2, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test 02", "Verify that network admin is able to " +
                "access the club for modification by providing only MSISDN of Chairman")
                .assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0);

        /*
        Login as Network admin and navigate to Modify Saving Club Page
         */
        Login.init(t2)
                .login(naModClub);

        SavingsClubManagement.init(t2)
                .initiateModifyClubUsingChairmanMsisdn(sClubModify.CMMsisdn);

        try {
            AddClub_Page1 page = AddClub_Page1.init(t2);
            if (page.clubName.isDisplayed() && page.clubName.getAttribute("value").equals(sClubModify.ClubName)) {
                t2.pass("Successfully able to initiate Club Modification by just providing Chairman's MSISDN");
            } else {
                t2.fail("Failed to initiate Club Modification by just providing Chairman's MSISDN");
            }
            Utils.captureScreen(t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 03
     * Verify that network admin is able to access the club for modification by providing only MSISDN of Chairman
     */
    @Test(priority = 3, groups = {FunctionalTag.ECONET_SIT_5_0})
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test 03", "Verify that network admin is able " +
                "to access the club for modification by providing only Club Name Initials & Club Name")
                .assignCategory(FunctionalTag.SAVING_CLUB, FunctionalTag.ECONET_SIT_5_0);

        /*
        Login as Network admin and navigate to Modify Saving Club Page
         */
        Login.init(t3)
                .login(naModClub);

        SavingsClubManagement.init(t3)
                .initiateModifyClubUsingClubName(sClubModify.ClubName);

        try {
            AddClub_Page1 page = AddClub_Page1.init(t3);
            if (page.clubName.isDisplayed() && page.clubName.getAttribute("value").equals(sClubModify.ClubName)) {
                t3.pass("Successfully able to initiate Club Modification by just providing ClubName");
            } else {
                t3.fail("Failed to initiate Club Modification by just providing ClubName");
            }
            Utils.captureScreen(t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 04
     * <p>
     * ADD_MAKER_REQ_CLUB =  Y' reject Club Modification
     * Reject the club modification and check the msgs
     */
    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test 04", "Initiate Club Modification")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        /*
        Login as Network admin and navigate to Modify Saving Club Page
         */
        Login.init(t4)
                .login(naModClub);

        SavingsClubManagement.init(t4)
                .initiateModifyClubUsingClubName(sClubModify.ClubName);

        // modify club
        try {
            AddClub_Page1.init(t4)
                    .selectClubType(Constants.CLUB_TYPE_BASIC)
                    .clickSubmit();

            AddClub_Confirm_Page1.init(t4)
                    .clickSubmit2();
            Thread.sleep(2000);

            /*
            Verify that Modification is successful and approval is required
             */
            Assertion.verifyActionMessageContain("savingclub.modified.success",
                    "Verify Successfully Initiated Club Modification", t4);

        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 05
     * <p>
     * Verify that Notification is sent on Club Modification initiation
     */
    @Test(priority = 5)
    public void test_5() throws Exception {
        ExtentTest t5 = pNode.createNode("Test 05", "Verify that Notification is sent to Chairman on Club Modification initiation")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SMSReader.init(t5).verifyRecentNotification(sClubModify.CMMsisdn,
                    "savingclub.modify.initiate.sms", sClubModify.ClubName, sClubModify.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test 06
     * Reject the Club Modification
     */
    @Test(priority = 6)
    public void test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test 06", "Reject Club Modification")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        /*
        Login as Network admin and navigate to Modify Saving Club Page
         */
        try {
            Login.resetLoginStatus();
            Login.init(t6)
                    .login(naModAppClub);

            /*
            Reject the Modify Approval and verify that notification is sent
             */
            ApproveClub_Page1.init(t6)
                    .navModifyApproveClub()
                    .selectClub(sClubModify.ClubName)
                    .setRejectReason("Reject To test")
                    .rejectClub();

            Assertion.verifyActionMessageContain("savingclub.modified.approve.reject",
                    "Verify Rejecting Club Modification", t6);
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 07
     * Verify on Rejection of Club modification a notification is sent
     */
    @Test(priority = 7)
    public void test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("Test 07", "Verify on Rejection of Club modification a notification is sent")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SMSReader.init(t7).verifyRecentNotification(sClubModify.CMMsisdn,
                    "savingclub.modify.initiate.reject.sms", sClubModify.ClubName, sClubModify.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t7);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Test 08
     * <p>
     * Verify Approving Club Modification
     */
    @Test(priority = 8)
    public void test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("Test 08", "Verify Approving Club Modification")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        /*
        Login as Network admin and navigate to Modify Saving Club Page
         */
        Login.init(t8)
                .login(naModClub);

        SavingsClubManagement.init(t8)
                .initiateModifyClubUsingClubName(sClubModify.ClubName);

        // modify club
        try {
            AddClub_Page1.init(t8)
                    .selectClubType(Constants.CLUB_TYPE_BASIC)
                    .clickSubmit();

            AddClub_Confirm_Page1.init(t8)
                    .clickSubmit2();

            /*
            Verify that Modification is successful and approval is required
             */
            Assertion.verifyActionMessageContain("savingclub.modified.success", "Verify Successfully Initiated Club Modification", t8);

            Login.resetLoginStatus(); // re login [issue with application, in same session, approval post rejecting same club is failing.]
            Login.init(t8)
                    .login(naModAppClub);

            ApproveClub_Page1.init(t8)
                    .navModifyApproveClub()
                    .selectClub(sClubModify.ClubName)
                    .approveClub()
                    .confirmAction();

            Assertion.verifyActionMessageContain("savingclub.modified.approve",
                    "Verify Approving Club Modification", t8);

        } catch (Exception e) {
            markTestAsFailure(e, t8);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 09
     * Verify on Approval of Club modification a notification is sent to the Club's Chairman
     */
    @Test(priority = 9)
    public void test_09() throws Exception {
        ExtentTest t9 = pNode.createNode("Test 09", "Verify on Approval of Club modification a notification is sent to the Club's Chairman")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            SMSReader.init(t9).verifyRecentNotification(sClubModify.CMMsisdn,
                    "savingclub.modify.initiate.approve.sms", sClubModify.ClubName, sClubModify.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t9);
        }
        Assertion.finalizeSoftAsserts();
    }
}