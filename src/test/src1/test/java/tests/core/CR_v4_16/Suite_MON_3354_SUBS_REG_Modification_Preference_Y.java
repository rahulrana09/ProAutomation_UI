package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page2;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;

/**
 * Subscriber Registration Modification - MON- 3354 Summary
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 3        Done            test_01
 * 4        Done            test_01
 * 5        Done            test_02
 * 6        Done            test_02
 * 7        Done            test_03
 * 8        Done            test_04
 * 9        Duplicate
 * 10       Duplicate
 * 12       Existing        Base Set
 * 15       Existing        System Test for User Management
 * 16       Duplicate
 * 17       Pending         System Test for User Management
 */
public class Suite_MON_3354_SUBS_REG_Modification_Preference_Y extends TestInit {
    private User chAddSubs, subsBaseSetUsr;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Update System Preference IS_ID_REQUIRED = Y")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        try {
            // Make sure that the Preference is set to 'Y'
            SystemPreferenceManagement.init(s1)
                    .updateSystemPreference("IS_ID_REQUIRED", "Y");

            // Users for Test
            chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
            subsBaseSetUsr = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that Identification Number field is mandatory when
     * IS_ID_REQUIRED = Y in the system while subscriber registration through web by channel user
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "Verify that ID Number field is mandatory when " +
                "IS_ID_REQUIRED = Y in the system while subscriber registration through web by channel user")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);


        User sub_01 = new User(Constants.SUBSCRIBER);

        sub_01.setExternalCode(""); // set the Identification Number as Empty

        Login.init(t1).login(chAddSubs);

        SubscriberManagement.init(t1)
                .startNegativeTest()
                .addInitiateSubscriber(sub_01);

        // verify that Identification Number is required
        Assertion.verifyErrorMessageContain("subs.error.extcoderequired", "verify that Identification Number is Required", t1);

    }

    /**
     * Verify that Identification Number field is mandatory when
     * IS_ID_REQUIRED = Y in the system while subscriber modification through web by channel user
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 02", "Verify that ID Number field is mandatory when IS_ID_REQUIRED = Y " +
                " in the system while subscriber modification through web by channel user ")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        Login.init(t2).login(chAddSubs);

        ModifySubscriber_page1 page = ModifySubscriber_page1.init(t2);
        page.navSubscriberModification();
        page.setMSISDN(subsBaseSetUsr.MSISDN);
        page.clickOnSubmitPg1();

        ModifySubscriber_page2 page2 = ModifySubscriber_page2.init(t2);
        page2.externalCode_SetText("");
        page2.clickOnNextPg2();

        Assertion.verifyErrorMessageContain("subs.error.extcoderequired", "verify that Identification Number is Required", t2);
    }

    /**
     * Verify that Passport option is populated in the ID Type drop down
     * list while on-boarding a subscriber when PASSPORT_ALLOWED = Y in the system
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("TEST 04", "Verify that Passport option is populated in the ID Type drop down " +
                "list while on-boarding a subscriber when PASSPORT_ALLOWED = Y in the system")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        // Make sure that the value for PASSPORT_ALLOWED is Set to Y
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("PASSPORT_ALLOWED", "Y");
        SystemPreferenceManagement.init(t4)
                .updateSystemPreference("PASSPORT_ALLOWED", "Y");

        User sub_03 = new User(Constants.SUBSCRIBER);

        Login.init(t4).login(chAddSubs);

        List<String> optionList = AddSubscriber_Page1.init(t4)
                .navAddSubscriber()
                .getIdTypeOptionsValues();

        Assertion.verifyListContains(optionList, "PASSPORT", "PassPort is an Option for Id Type", t4);
    }

    /**
     * Verify that Passport option is Not populated in the ID Type drop down
     * list while on-boarding a subscriber when PASSPORT_ALLOWED = N in the system
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("TEST 05", "Verify that Passport option is Not populated in the ID Type drop down " +
                "list while on-boarding a subscriber when PASSPORT_ALLOWED = N in the system")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        try {
            SystemPreferenceManagement.init(t5)
                    .updateSystemPreference("PASSPORT_ALLOWED", "N");

            User sub_04 = new User(Constants.SUBSCRIBER);

            Login.init(t5)
                    .login(chAddSubs);

            List<String> optionList = AddSubscriber_Page1.init(t5)
                    .navAddSubscriber()
                    .getIdTypeOptionsValues();

            Assertion.verifyListNotContains(optionList, "PASSPORT", "PassPort is Not an Option for Id Type", t5);

        } finally {
            SystemPreferenceManagement.init(t5)
                    .updateSystemPreference("PASSPORT_ALLOWED", "Y");
        }
    }
}
