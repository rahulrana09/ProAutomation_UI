package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.globalConstant.Constants;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by Gurudatta Praharaj on 12/11/2017.
 */
public class Suite_MON_3453_Minor_Customization extends TestInit {

    private User chUser;
    private OperatorUser optUser;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        optUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);
    }

    /*
        creating subscriber by allowing spaces in the address field
     */
    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("test_01", "Allowing spaces in Address field while subscriber registration");

        //login as channel user
        Login.init(t1).login(chUser);

        User subscriber = new User(Constants.SUBSCRIBER);
        //set address with spaces
        subscriber.setAddress("  Bangalore  Karnataka   ");
        //create a subscriber by allowing spaces in the address field
        SubscriberManagement.init(t1)
                .createSubscriberDefaultMapping(subscriber, true, false);

        Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                "subscriber created with address with spaces", t1, true);

    }

    /*
       creating channeluser by allowing spaces in the address field
    */
    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("test_02", "Allowing spaces in Address field while channeluser registration");

        //login as operator user
        Login.init(t2).login(optUser);

        //create channel user by allowing spaces in the address field
        User channelUser = new User(Constants.WHOLESALER);
        channelUser.setAddress("  Bangalore  Karnataka  ");
        ChannelUserManagement.init(t2).createChannelUser(channelUser);

        //validate channel user successfully created
        Assertion.verifyEqual(Assertion.isErrorInPage(t2), false,
                "channel user created with address with spaces", t2, true);

    }

    /*
        modifying subscriber by allowing spaces in the address field
     */

    @Test(priority = 3)
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("test_03", "Allowing spaces in Address field while subscriber modifications");

        //login as channel user
        Login.init(t3).login(chUser);

        //fetching subscriber 1 from exusting subscriber
        User subsUser_01 = new User(Constants.SUBSCRIBER);
        subsUser_01 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);

        //modifying subscriber 1 by entering address with spaces
        subsUser_01.setAddress("    Banglore   Karnakata    ");
        SubscriberManagement.init(t3)
                .initiateSubscriberModification(subsUser_01);
        AddSubscriber_Page1.init(t3)
                .modifyClickNext();

        //validating subscriber creation message
        Assertion.verifyEqual(Assertion.isErrorInPage(t3), false,
                "subscriber modified with address with spaces", t3, true);
    }

    /*
    modifying channel user by allowing spaces in the address field
    */
    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("test_04", "Allowing spaces in Address field while channel user modifications");

        //login as operator user.
        Login.init(t4).login(optUser);

        //fetching channel user from exixting channel users
        User channelUser = new User(Constants.WHOLESALER);
        ChannelUserManagement.init(t4).createChannelUserDefaultMapping(channelUser, false);

        Login.init(t4).login(optUser);

        ChannelUserManagement.init(t4).initiateChannelUserModification(channelUser);
        channelUser.setAddress("   Banglore  Karnakata   ");
        ChannelUserManagement.init(t4).completeChannelUserModification();
        //ModifyChannelUser_pg1.init(t).modifyAddressOfChannelUser();
        Assertion.verifyEqual(Assertion.isErrorInPage(t4), false,
                "channel user modified with address with spaces", t4, true);
    }
}
