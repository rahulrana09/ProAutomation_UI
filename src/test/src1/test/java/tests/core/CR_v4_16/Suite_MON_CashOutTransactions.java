package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.SfmResponse;
import framework.entity.OperatorUser;
import framework.entity.PseudoUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.common.Login;
import framework.features.systemManagement.TransferRuleManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.PseudoUserManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.Utils;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.jigsaw.CommonOperations;
import org.junit.Ignore;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.math.BigDecimal;

import static com.google.common.collect.ImmutableMap.of;
import static framework.util.jigsaw.CommonOperations.getTransactionDetails;
import static framework.util.jigsaw.CommonOperations.setServiceValidatorPropertiesAsync;

public class Suite_MON_CashOutTransactions extends TestInit {

    private User wholesaler, subscriber;
    private OperatorUser chAdmin;
    private String empId;
    private PseudoUser psUser;

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest tSetup = pNode.createNode("Setup", "Create Channel User, Make sure it has balance, Configure CASHOUT Transfer Rule" +
                ", Delete All pricing Policies, enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer, " +
                "setBearerForInitiatorPartyOTPValidation, createEmployee and Create a Pseudo User");

        chAdmin = DataFactory.getOperatorUsersWithAccess("PSEUDO_ADD2", Constants.CHANNEL_ADMIN).get(0);
        wholesaler = new User(Constants.WHOLESALER);
        subscriber = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        empId = "1";

        //create a wholesaler
        ChannelUserManagement.init(tSetup)
                .createChannelUserDefaultMapping(wholesaler, true);

        //Make sure agent partner involved in test case Has Enough Balance
        TransactionManagement.init(tSetup)
                .makeSureChannelUserHasBalance(wholesaler);

        ServiceCharge tRule = new ServiceCharge(Services.CASHOUT, subscriber, wholesaler, null, null, null, null);
        TransferRuleManagement.init(tSetup)
                .configureTransferRule(tRule);

        CommonOperations.deleteAllPricingPolicies();

        //enabling 2nd party authentication when initiator is withdrawer
        //enableSecondPartyAuthenticationWhenInitiatorIsWithdrawer();

        //enable OTP when bearer is IVR
        //setBearerForInitiatorPartyOTPValidation();

        //create an employee for the wholesaler
        Transactions.init(tSetup)
                .createEmployee(wholesaler, empId);

        //create a pseudo user
        psUser = new PseudoUser();
        psUser.setParentUser(wholesaler);
        PseudoUserManagement.init(tSetup)
                .createCompletePseudoUser(psUser);

    }

    @AfterClass(alwaysRun = true)
    public void afterEnd() throws Exception {
        //disabling 2nd party authentication when initiator is withdrawer
        //disableSecondPartyAuthenticationWhenInitiatorIsWithdrawer();

        setServiceValidatorPropertiesAsync(of(
                "identical.txn.enabled." + "CASHOUT", false,
                "txn.pause.expiryIntervalInMillis", "3600000"
        ));
    }

    @Test(priority = 1)
    public void MON_4131_transactionApprovalCashOutPendingWithChannelUserNotVisibleToPseudoUser() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_4131_transactionApprovalCashOutPendingWithChannelUserNotVisibleToPseudoUser",
                " To verify that Cashout transaction is successful" +
                        " when channel user approves the transaction in UI from transaction approval list for 2 step transaction and the transaction shouldn't " +
                        " be visible to pseudo user for approval");

        BigDecimal amount = new BigDecimal("2");

        // perform Cash out
        SfmResponse response = Transactions.init(t1)
                .performCashOut(subscriber, wholesaler, amount);

        //check that the transaction shouldn't be visible to pseudo user
        Login.init(t1).login(psUser);
        Assertion.verifyEqual(TransactionManagement.init(t1).isTransactionIdAvailable(response.TransactionId),
                false,
                "Transaction should not be visible", t1);

        //now login as channel user and approve the transaction
        Login.init(t1)
                .login(wholesaler);

        TransactionManagement.init(t1)
                .approvePendingCashout(response.TransactionId, true);

        //validate that transaction is successfully approved and stored in db
        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TS", "transaction status validation", t1);
    }

    @Test(priority = 2)
    public void MON_4131_transactionRejectionCashOutPendingWithChannelUserNotVisibleToPseudoUser() throws Exception {
        ExtentTest t2 = pNode.createNode("MON_4131_transactionRejectionCashOutPendingWithChannelUserNotVisibleToPseudoUser",
                " To verify that Cashout transaction is rejected successfully" +
                        " when channel user rejects the transaction in UI from transaction approval list for 2 step transaction and also the same transaction shouldn't " +
                        " be visible for rejection to the pseudo user");

        BigDecimal amount = new BigDecimal("2");
        SfmResponse response = Transactions.init(t2)
                .performCashOut(subscriber, wholesaler, amount);

        //now login as channel user and approve the transaction
        Login.init(t2)
                .login(wholesaler);

        TransactionManagement.init(t2)
                .approvePendingCashout(response.TransactionId, false);

        //validate that transaction is successfully rejected and stored in db
        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TF", "transaction status validation", t2);

    }

    @Test(priority = 3)
    public void MON_4139_transactionApprovalRejectionAfterTimeout() throws Exception {
        ExtentTest t3 = pNode.createNode("MON_4139_transactionApprovalRejectionAfterTimeout",
                " To verify that a channel user shouldn't be able to approve /reject a Cashout transaction pending with him" +
                        " if he tries to approve/reject the same after transaction expiry intervals");

        BigDecimal amount = new BigDecimal("2");
        try {
            //setting transaction time out to be 5 secs
            setServiceValidatorPropertiesAsync(of(
                    "identical.txn.enabled." + "CASHOUT", false,
                    "txn.pause.expiryIntervalInMillis", "5000"
            ));

           /*
           Perform Cash out
           Wait for Timeout
           reject a txt and expect an error message
            */
            SfmResponse response = Transactions.init(t3)
                    .performCashOut(subscriber, wholesaler, amount);

            Utils.putThreadSleep(5000);

            Login.init(t3)
                    .login(wholesaler);
            startNegativeTest();
            TransactionManagement.init(t3)

                    .approvePendingCashout(response.TransactionId, false);

            Assertion.verifyActionMessageContain("cashout.timeout.message",
                    "verifying that transaction has timed out user tries to reject the transaction", t3, response.TransactionId);

            /*
           Approve a txt and expect an error message
            */
            SfmResponse response1 = Transactions.init(t3)
                    .performCashOut(subscriber, wholesaler, amount);
            startNegativeTest();
            TransactionManagement.init(t3)

                    .approvePendingCashout(response1.TransactionId, true);

            Assertion.verifyActionMessageContain("cashout.timeout.message",
                    "verifying that transaction has timed out user tries to approve the transaction", t3, response1.TransactionId);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            setServiceValidatorPropertiesAsync(of(
                    "identical.txn.enabled." + "CASHOUT", false,
                    "txn.pause.expiryIntervalInMillis", "3600000"
            ));
        }
    }

    @Test(priority = 4)
    public void MON_4130_cashOutPendingForEmployeeNotVisibleToChannelUserAndPseudoUser() throws Exception {
        ExtentTest t4 = pNode.createNode("MON_4130_cashOutPendingForEmployeeNotVisibleToChannelUserAndPseudoUser",
                "To verify that for transaction pending with ann employee the same transaction " +
                        "shouldn't be visible to channel user (parent) or pseudo user");

        BigDecimal amount = new BigDecimal("2");

        //perform a cash out pending for employee
        SfmResponse response = Transactions.init(t4)
                .cashOutPendingForEmployee(subscriber, wholesaler, empId, amount, t4);

        //check that the transaction shouldn't be visible to channel user
        Login.init(t4).login(wholesaler);
        Assertion.verifyEqual(TransactionManagement.init(t4).isTransactionIdAvailable(response.TransactionId),
                false,
                "Transaction should not be visible to Channel User", t4);

        //check that the transaction shouldn't be visible to pseudo user
        Login.init(t4).login(psUser);
        Assertion.verifyEqual(TransactionManagement.init(t4).isTransactionIdAvailable(response.TransactionId), false,
                "Transaction should not be visible to Pseudo User", t4);
    }

    @Test(priority = 5)
    public void MON_4132_cashOutPendingForApprovalWithPseudoUserNotVisibleToChannelUser() throws Exception {
        ExtentTest t5 = pNode.createNode("MON_4132_cashOutPendingForApprovalWithPseudoUserNotVisibleToChannelUser",
                "To verify that for transaction pending with a pseudo user " +
                        "shouldn't be visible to channel user (parent) and also the pseudo user should be able to approve the transaction");

        BigDecimal amount = new BigDecimal("2");
        SfmResponse response = Transactions.init(t5)
                .performCashOutToPseudoUser(subscriber, wholesaler, psUser, amount, t5);

        //check that this transaction isn't pending for approval with the channel user
        Login.init(t5).login(wholesaler);
        Assertion.verifyEqual(TransactionManagement.init(t5).isTransactionIdAvailable(response.TransactionId), false,
                "Transaction should not be visible to Channel User", t5);

        //check that the transaction is visible to pseudo user and approve it
        Login.init(t5)
                .login(psUser);

        TransactionManagement.init(t5)
                .approvePendingCashout(response.TransactionId, true);

        //validate that transaction is successfully approved and stored in db
        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TS", "transaction status validation", t5);
    }

    @Test(priority = 6)
    public void MON_4132_cashOutPendingForRejectionWithPseudoUserNotVisibleToChannelUser() throws Exception {
        ExtentTest t6 = pNode.createNode("MON_4132_cashOutPendingForRejectionWithPseudoUserNotVisibleToChannelUser",
                "To verify that for transaction pending with a pseudo user" +
                        "shouldn't be visible to channel user (parent) and also the pseudo user should be able to reject the transaction");

        BigDecimal amount = new BigDecimal("2");
        SfmResponse response = Transactions.init(t6)
                .performCashOutToPseudoUser(subscriber, wholesaler, psUser, amount, t6);

        //check that the transaction is visible to pseudo user and approve it
        Login.init(t6)
                .login(psUser);

        TransactionManagement.init(t6)
                .approvePendingCashout(response.TransactionId, false);

        //validate that transaction is successfully approved and stored in db
        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TF", "transaction status validation", t6);
    }

    @Ignore("Bug: MON-4165")
    @Test(priority = 7, enabled = false)
    public void MON_4161_cashOutSuccessfulWhenTwoFactorTransactionEnabledAndPendingWithChannelUser() throws Exception {
        ExtentTest t7 = pNode.createNode("MON_4161_cashOutSuccessfulWhenTwoFactorTransactionEnabledAndPendingWithChannelUser",
                "To verify that when two factor transaction is enabled(OTP transaction) and when the transaction is pending with" +
                        " channel user then channel user should be able to reject the transaction");

        BigDecimal amount = new BigDecimal("2");

        //perform a cash out using IVR bearer
        SfmResponse response = Transactions.init(t7)
                .performCashOutUsingIVRAndValidateOTP(subscriber, wholesaler, amount, t7);

        //now approve the transaction pending for channel user
        Login.init(t7)
                .login(wholesaler);

        //approve the transaction
        TransactionManagement.init(t7)
                .approvePendingCashout(response.TransactionId, true);

        //validate that transaction is successfully approved and stored in db
        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TS", "transaction status validation", t7);

        //again perform a cash out using IVR bearer
        SfmResponse response1 = Transactions.init(t7)
                .performCashOutUsingIVRAndValidateOTP(subscriber, wholesaler, amount, t7);

        //Reject the transaction
        TransactionManagement.init(t7)
                .approvePendingCashout(response1.TransactionId, false);

        //validate that transaction is successfully approved and stored in db
        String transactionStatus1 = getTransactionDetails(response1.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus1, "TF", "transaction status validation", t7);
    }

    @Ignore("Bug: MON-4165")
    @Test(priority = 8, enabled = false)
    public void MON_4161_cashOutSuccessfulWhenTwoFactorTransactionEnabledAndPendingWithPseudoUser() throws Exception {
        ExtentTest t8 = pNode.createNode("MON_4161_cashOutSuccessfulWhenTwoFactorTransactionEnabledAndPendingWithChannelUser",
                "To verify that when two factor transaction is enabled(OTP transaction) and when the transaction is pending with" +
                        " channel user then channel user should be able to reject the transaction");

        BigDecimal amount = new BigDecimal("2");

        //perform a cash out using IVR bearer
        SfmResponse response = Transactions.init(t8)
                .performCashOutToPseudoUserUsingIVRAndValidateOTP(subscriber, wholesaler, psUser, amount, t8);

        //now approve the transaction pending for pseudo user
        Login.init(t8)
                .login(psUser);

        //approve the transaction
        TransactionManagement.init(t8)
                .approvePendingCashout(response.TransactionId, true);

        String transactionStatus = getTransactionDetails(response.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus, "TF", "transaction status validation", t8);

        //again perform a cash out using IVR bearer
        SfmResponse response1 = Transactions.init(t8)
                .performCashOutToPseudoUserUsingIVRAndValidateOTP(subscriber, wholesaler, psUser, amount, t8);

        //Reject the transaction
        TransactionManagement.init(t8)
                .approvePendingCashout(response.TransactionId, false);

        //validate that transaction is successfully rejected and stored in db
        String transactionStatus1 = getTransactionDetails(response1.TransactionId).extract().jsonPath().getString("transferStatus");
        Assertion.verifyEqual(transactionStatus1, "TF", "transaction status validation", t8);
    }
}



