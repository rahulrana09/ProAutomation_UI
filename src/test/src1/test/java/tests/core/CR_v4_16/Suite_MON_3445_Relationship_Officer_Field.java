package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.CommonUserManagement;
import framework.pageObjects.userManagement.AddChannelUser_pg1;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.pageObjects.userManagement.CommonChannelUserPage;
import framework.util.common.DataFactory;
import framework.util.common.DriverFactory;
import framework.util.common.Utils;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.propertiesManagement.UserFieldProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.io.IOException;

/**
 * Created by ravindra.dumpa on 11/2/2017.
 */
public class Suite_MON_3445_Relationship_Officer_Field extends TestInit {

    private UserFieldProperties fields;
    private OperatorUser usrCanAddChUser, usrCanModChUser, usrCanApprChUser, usrCanApprModChUser, usrCanAddBulkUser;

    /**
     * Verifying Relationship officer field in add channel user page
     *
     * @throws Exception
     */
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        //create objects for users for
        usrCanAddChUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        usrCanModChUser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        usrCanApprChUser = DataFactory.getOperatorUserWithAccess("PTY_MCU");
        usrCanApprModChUser = DataFactory.getOperatorUserWithAccess("BNK_APR");
        usrCanAddBulkUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");

    }

    @Test(priority = 1)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Verifying Relationship officer field in add channel user page");
        /*
         * Login as Operator user
         * Navigate to Add Channel user page
         * Verify Relationship Officer Field
         */
        //login as operator user
        Login.init(t1).login(usrCanAddChUser);
        //Navigating to Add Channel user page
        AddChannelUser_pg1.init(t1)
                .navAddChannelUser();
        //Verifying Relation ship Officer Field
        verifyIfLabelExists(UserFieldProperties.getField("relation.officer"), t1);
    }

    /**
     * Generic Method to verify label
     *
     * @throws IOException
     */

    private void verifyIfLabelExists(String labelName, ExtentTest t1) throws IOException {
        if (DriverFactory.getDriver()
                .findElement(By.xpath("//label[contains(text(),'" + labelName + "')]")).isDisplayed()) {
            t1.pass("Successfuuly verified the Label for element - " + labelName);
        } else {
            t1.fail("Failed to verify the label for element -" + labelName);
            Utils.captureScreen(t1);
        }
    }

    /**
     * Verifying Relationship officer field in modify channel user page
     *
     * @throws Exception
     */

    @Test(priority = 2)
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Verifying Relationship officer field in Modify Channel user page");
        /*
         * Login as Operator user
         * Navigate to Modify Channel user page
         * Verify Relationship Officer Field
         */
        //Login as operator user
        Login.init(t2).login(usrCanModChUser);

        //Creating channel user object
        User user = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);

        //navigating channel user modification page
        ChannelUserManagement.init(t2)
                .initiateChannelUserModification(user);
        //Verifying Relation ship Officer Field
        verifyIfLabelExists(UserFieldProperties.getField("relation.officer"), t2);

    }

    /**
     * Verifying Relationship officer field in approval channel user page
     *
     * @throws Exception
     */

    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Verifying Relationship officer field in Approve Channel user page");
        /*
         * Login as Operator user
         * Navigate to Approval Channel user page
         * Verify Relationship Officer Field
         */
        //Login as operator user
        Login.init(t3).login(usrCanApprChUser);

        //Creating channel user object
        User user = new User(Constants.WHOLESALER);

        //add channel user and navigate and approval page
        ChannelUserManagement.init(t3)
                .addChannelUser(user);
        CommonChannelUserPage.init(t3)
                .navigateToChannelUserApproval();

        //Verifying Relation ship Officer Field
        ChannelUserManagement.init(t3).verifyFieldInApprovalPage(user, UserFieldProperties.getField("relation.officer"));

        //Verifying relationship officer value in DB
        String relationshipOfficer = MobiquityGUIQueries.getRelationshipOfficer(user.MSISDN);

        try {
            Assert.assertEquals(relationshipOfficer, user.RelationshipOfficer);
        } catch (NoSuchElementException e) {
            Assert.fail();
        }


    }

    /**
     * Verifying Relationship officer field in Channel modification approval page
     *
     * @throws Exception
     */

    @Test(priority = 4)
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04", "Verifying Relationship officer field in Modify Channel user approve  page");
        /*
         * Login as Operator user
         * Navigate to Modify Channel approval page
         * Verify Relationship Officer Field
         */
        //login as Operator users
        Login.init(t4).login(usrCanApprModChUser);

        //Creating channel user object
        User user = new User(Constants.WHOLESALER);

        //Create channel user and approve bank
        ChannelUserManagement.init(t4)
                .addChannelUser(user);
        CommonUserManagement.init(t4)
                .addInitiatedApproval(user, true)
                .approveDefaultBank(user);

        //Modify channel user
        ChannelUserManagement.init(t4)
                .initiateChannelUserModification(user);

        // change any field
        user.setFirstAndLastName("newFirst", "newLast");

        //Verify field in channel user modifcation approve page
        ChannelUserManagement.init(t4).completeChannelUserModification()
                .verifyFieldInModApprovalPage(user, "Relationship Officer");

    }

    /**
     * Verifying Relationship officer field in Bulk ch user reg csv
     *
     * @throws Exception
     */
    @Test(priority = 5)
    public void Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test_05", "verify relationship officer field in bulk csv");

        /**
         * login as operator user
         * go to bulk subscriber registration and download template
         * Verifying Relationship officer field in csv file
         */
        //login as operator user
        Login.init(t5).login(usrCanAddBulkUser);

        //goto bulk ch user and sub reg page
        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t5);
        page.navBulkChUserAndSubsciberRegistrationAndModification();

        //select the service and download the file
        page.SelectSubscriberRegistrationService();
        BulkChUserAndSubRegAndModPage.clickOnStartDownload();

        //verify the field in csv file
        BulkChUserAndSubRegAndModPage.init(t5).verifyFieldInCsvFile("BulkSubRegId", "Relationship Officer*");

    }

    /**
     * Verifying Relationship officer field on delete Channel user page
     *
     * @throws Exception
     */

    @Test(priority = 6)
    public void Test_06() throws Exception {

        ExtentTest t6 = pNode.createNode("Test_06", "Verifying Relationship officer field in delete Channel user page");

        /**
         * login as operator user
         * go to delete channel user approval page
         * Verifying Relationship officer field
         */
        //login as operator user
        Login.init(t6).login(usrCanAddChUser);

        //navigate delete ch user page
        User chUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        CommonChannelUserPage.init(t6).navDeleteChannelUser();

        //Verifying Relation ship Officer Field
        ChannelUserManagement.init(t6).verifyFieldOnDeleteChUserPage(chUser, "Relationship Officer");

    }

}













