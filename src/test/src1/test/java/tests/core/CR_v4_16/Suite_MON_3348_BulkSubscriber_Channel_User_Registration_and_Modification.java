package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.userManagement.ChannelUserManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.userManagement.BulkChUserAndSubRegAndModPage;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.DateAndTime;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.FilePath;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.Date;


/**
 * Created by ravindra.dumpa on 10/16/2017.
 */
public class Suite_MON_3348_BulkSubscriber_Channel_User_Registration_and_Modification extends TestInit {

    private User whs1, hmer;
    private String batchId, subBatchId;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        //ExtentTest t1 = pNode.createNode("SETUP", "Create Test Data Specific to this Suite!");
        // whs1 = DataFactory.getChannelUserList(Constants.WHOLESALER).get(0);
        //hmer = DataFactory.getChannelUserList(Constants.HEAD_MERCHANT).get(0);
    }

    /**
     * Verifying Channel admin able to download bulk channel user registration csv file
     *
     * @throws Exception
     */
    @Test(priority = 11)
    public void Test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Verifying Channel admin able to download bulk channel user Registration csv file");
        /*
         * Login as Operator user
         * Goto bulk channel and subscriber registration page
         * Select the service
         * Click on download file
         */

        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        Login.init(t1).login(dwFileUser);

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t1);

        try {
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectChannelUserRegistrationService();

            BulkChUserAndSubRegAndModPage.clickOnStartDownload();

            Assertion.verifyEqual(Assertion.isErrorInPage(t1), false,
                    "Verify channel admin download bulk channel user registration csv file ", t1);
        } catch (Exception e) {
            Assertion.raiseExceptionAndStop(e, t1);
        }
    }

    /**
     * Verifying Channel admin able to download bulk subscriber registration csv file
     *
     * @throws Exception
     */

    @Test(priority = 12)
    public void Test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test_02", "Verifying Channel admin able to download bulk Subscriber Registration csv file");
        /*
         * Login as Operator user
         * Goto bulk channel and subscriber registration page
         * Select the service
         * Click on download file
         */
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        Login.init(t2).login(dwFileUser);

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t2);

        try {
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectSubscriberRegistrationService();

            BulkChUserAndSubRegAndModPage.clickOnStartDownload();

            Assertion.verifyEqual(Assertion.isErrorInPage(t2), false,
                    "Verify channel admin download bulk channel user registration csv file ", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

    }

    /**
     * Verifying Channel admin able to initiate Wholesaler/retailer/Merchant through Bulk
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void Test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test_03", "Verifying Channel admin able to initiate Wholesaler/retailer/Merchant through Bulk");
        /*
         * Login as Operator user
         * Create Wholesaler/Retailer/Merchant Objects
         * Goto bulk channel and subscriber registration page
         * Select the service
         * write the data in csv file
         * Upload csv file
         */
        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        User whs_03 = new User(Constants.WHOLESALER);

        User retailerParent = DataFactory.getChannelUserWithCategory(DataFactory.getParentCategoryCode(Constants.RETAILER));

        User retailer = new User(Constants.RETAILER, retailerParent);

        User merchantParent = DataFactory.getChannelUserWithCategory(DataFactory.getParentCategoryCode(Constants.MERCHANT));

        User merchant = new User(Constants.MERCHANT, merchantParent);

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t3);

        try {
            // login
            Login.init(t3).login(dwFileUser);

            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectChannelUserRegistrationService();

            ChannelUserManagement.init(t3)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            whs_03,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false);

            //ChannelUserManagement.init(t3).writeBulkChUserRegDetailsInCSV(retailer, FilePath.fileBulkChUserReg,true,true);

            //ChannelUserManagement.init(t3).writeBulkChUserRegDetailsInCSV(merchant, FilePath.fileBulkChUserReg,true,true);

            page.uploadFile(FilePath.fileBulkChUserReg);

            page.submitCsv();

            batchId = page.getBatchId();

            Assertion.verifyActionMessageContain("bulkUpload.user.reg.success",
                    "Bulk Channel User Registration ", t3, "1", "1", "0");
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t3);
        }
    }

    /**
     * Verifying Channel admin able to approve the bulk channel user through bulk
     *
     * @throws Exception
     */

    @Test(priority = 4, dependsOnMethods = "Test_03")
    public void Test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test_04", "Channel Admin able to approve the bulk channel user through bulk");
        /*
         * Login as Operator user
         * Goto bulk channel and subscriber  approve page
         * Select the Date range
         * Approve using batch id
         */

        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        Login.init(t4).login(dwFileUser);

        try {
            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t4);

            page.navBulkRegistrationAndModificationApproval();

            page.selectDateRangeForApproval(new DateAndTime().getDate(-14), new DateAndTime().getDate(0));

            page.ClickOnSubmit();

            page.approveUsingBatchId(batchId);

            Assertion.assertEqual(Assertion.isErrorInPage(t4), false, "approved batch", t4);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t4);
        }


    }

    /**
     * Verifying Channel admin able to Subscribrt through Bulk
     * Verifying summary filed
     *
     * @throws Exception
     */

    @Test(priority = 5, groups = {FunctionalTag.BULK_SUB_REG_MOD, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0})
    public void MON_3348_Test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("MON_3348_Test_05", "Channel admin able register subscriber through bulk");
        t5.assignCategory(FunctionalTag.BULK_SUB_REG_MOD, FunctionalTag.PVG_SYSTEM, FunctionalTag.ECONET_UAT_5_0);

        /*
         * Login as Operator user
         * Create Subscriber Object
         * Goto bulk channel and subscriber registration page
         * Select the service
         * write the data in csv file
         * Upload csv file
         * go to approve page and verify summary filed
         */
        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("BULK_INITIATE").get(0); // todo  move to before class

        try {
            Login.init(t5).login(dwFileUser);

            User subsriber = new User(Constants.SUBSCRIBER);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t5);

            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectSubscriberRegistrationService();

            SubscriberManagement.init(t5)
                    .writeDataToBulkSubsRegistrationCsv(subsriber,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false);

            page.uploadFile(FilePath.fileBulkSubReg);

            page.submitCsv();

            subBatchId = page.getBatchId();

            Assertion.verifyActionMessageContain("bulkUpload.user.reg.success",
                    "Bulk Channel subscriber Registration", t5, "1", "1", "0");

            OperatorUser dwFileUser1 = DataFactory.getOperatorUsersWithAccess("NEW_BULK_APPROVE").get(0); // todo  move to before class

            Login.init(t5).login(dwFileUser1);

            page.navBulkRegistrationAndModificationApproval();

            page.ClickOnSubmit();

            page.clickOnSummaryandVerifyFields(subBatchId);

        } catch (Exception e) {
            markTestAsFailure(e, t5);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Channel Admin able to approve the bulk subscriber through bulk
     *
     * @throws Exception
     */

    @Test(priority = 6)
    public void Test_06() throws Exception {

        ExtentTest t6 = pNode.createNode("Test_06", "Channel Admin able to approve the bulk subscriber through bulk");
        /*
         * Login as Operator user
         * Goto bulk channel and subscriber  approve page
         * Select the Date range
         * Approve using batch id
         */
        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        try {
            Login.init(t6).login(dwFileUser);

            BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t6);

            page.navBulkRegistrationAndModificationApproval();

            Date d1 = new Date();

            page.selectDateRangeForApproval(new DateAndTime().getDate(-14), new DateAndTime().getDate(0));

            page.ClickOnSubmit();

            page.approveUsingBatchId(subBatchId);

            Assertion.assertEqual(Assertion.isErrorInPage(t6), false, "approved batch", t6);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t6);
        }

    }

    /**
     * Channel Admin able to verify logs when mandatory fields are not entered in channel userbulk csv
     *
     * @throws Exception
     */
    @Test(priority = 7)
    public void Test_07() throws Exception {
        ExtentTest t7 = pNode.createNode("Test_07", "Channel Admin able to verify logs when mandatory fields are not entered");

        /**
         * Login as Operator user
         * Create Wholesaler/Retailer/Merchant Objects
         * Change some fields as empty
         * Select the service
         * write the data in csv file
         * Upload csv file
         * Go to My batches page and verify log file
         */
        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        Login.init(t7).login(dwFileUser);
        //Create Channel user Object and dont enter data in some fields in csv file
        User wholesaler = new User(Constants.WHOLESALER);

        wholesaler.FirstName = "";

        String errmsg1 = "First Name cannot be empty!!."; // move to property file

        wholesaler.setExternalCode("");

        String errmsg2 = "Identification Number  cannot be empty!!."; // move to property file

        wholesaler.LoginId = "";

        String errmsg3 = "Web Login Id cannot be empty!!.";

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t7);

        try {
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectChannelUserRegistrationService();

            ChannelUserManagement.init(t7)
                    .writeBulkChUserRegistrationDetailsInCSV(FilePath.fileBulkChUserReg,
                            wholesaler,
                            defaultProvider.ProviderName,
                            defaultWallet.WalletName,
                            true,
                            false);
            //upload the csv file
            page.uploadFile(FilePath.fileBulkChUserReg);

            page.submitCsv();

            String batchid = page.getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage();

            page.ClickOnBatchSubmit();
            // verify error messages based on batch id
            page.downloadLogFileUsingBatchId(batchid).verifyErrorMessageInLogFile(errmsg1, batchid, t7);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t7);
        }
    }

    /**
     * Channel Admin able to verify logs when mandatory fields are not entered in channel subscriber bulk csv
     *
     * @throws Exception
     */
    @Test(priority = 8)
    public void Test_08() throws Exception {
        ExtentTest t8 = pNode.createNode("Test_08", "Channel Admin able to verify error messages in bulk subscriber log");

        /**
         * Login as Operator user
         * Create Subscriber Object
         * Change some fields as empty
         * Select the service
         * write the data in csv file
         * Upload csv file
         * Go to My batches page and verify log file
         */

        //Login as Operator user
        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0); // move to setup

        Login.init(t8).login(dwFileUser);
        //Create Subscriber Object and dont enter data in some fields in csv file
        User subscriber = new User(Constants.SUBSCRIBER);

        subscriber.FirstName = "";

        String errmsg1 = "First Name cannot be empty!!.";

        subscriber.setExternalCode("");

        String errmsg2 = "Identification Number  cannot be empty!!.";

        subscriber.LoginId = "";

        String errmsg3 = "Web Login Id cannot be empty!!.";
        //upload the csv file
        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t8);

        // todo - separate the common step for uploading csv and validating error

        try {
            page.navBulkChUserAndSubsciberRegistrationAndModification();

            page.SelectSubscriberRegistrationService();

            SubscriberManagement.init(t8)
                    .writeDataToBulkSubsRegistrationCsv(subscriber,
                            FilePath.fileBulkSubReg,
                            GlobalData.defaultProvider.ProviderName,
                            GlobalData.defaultWallet.WalletName,
                            "A",
                            "A",
                            true,
                            false);

            page.uploadFile(FilePath.fileBulkSubReg);

            page.submitCsv();
            //store the batch id and go to my batches page
            String subBatchId = page.getBatchId();

            page.navBulkRegistrationAndModificationMyBatchesPage();

            page.selectDateRangeForApproval(new DateAndTime().getDate(-14), new DateAndTime().getDate(0));

            page.ClickOnBatchSubmit();
            // verify error messages based on batch id
            page.downloadLogFileUsingBatchId(subBatchId)
                    .verifyErrorMessageInLogFile(errmsg1, subBatchId, t8);

        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t8);
        }
    }

    /**
     * Channel Admin able to click on summary and verify fields
     *
     * @throws Exception
     */
    @Test(dependsOnMethods = "Test_08")
    public void Test_09() throws Exception {
        ExtentTest t9 = pNode.createNode("Test_09", "Channel Admin able to click on summary and verify fields");
        /**
         * Login as Operator user
         * Go to My batches page
         * select Date range
         * and verify summary fields
         */

        OperatorUser dwFileUser = DataFactory.getOperatorUserWithAccess("PTY_ACU");

        Login.init(t9).login(dwFileUser);

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t9);

        try {
            page.navBulkRegistrationAndModificationMyBatchesPage();

            page.selectDateRangeForApproval(new DateAndTime().getDate(-14), new DateAndTime().getDate(0));

            page.ClickOnBatchSubmit();

            page.clickOnSummaryandVerifyFields(batchId);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t9);
        }


    }

    /**
     * Channel Admin able to click on Details and verify fields
     *
     * @throws Exception
     */
    @Test(dependsOnMethods = "Test_08")
    public void Test_10() throws Exception {
        ExtentTest t10 = pNode.createNode("Test_10", "Channel Admin able to click on Details and verify fields");
        /**
         * Login as Operator user
         * Go to My batches page
         * select Date range
         * and verify details fields
         */

        OperatorUser dwFileUser = DataFactory.getOperatorUsersWithAccess("PTY_ACU").get(0);

        Login.init(t10).login(dwFileUser);

        BulkChUserAndSubRegAndModPage page = BulkChUserAndSubRegAndModPage.init(t10);

        try {
            page.navBulkRegistrationAndModificationMyBatchesPage();

            page.selectDateRangeForApproval(new DateAndTime().getDate(-14), new DateAndTime().getDate(0));

            page.ClickOnBatchSubmit();

            page.clickOnDetailsandVerifyFields(subBatchId);
        } catch (Exception e) {
            e.printStackTrace();
            Assertion.raiseExceptionAndStop(e, t10);
        }


    }

}
