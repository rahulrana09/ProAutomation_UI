package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.entity.Bank;
import framework.entity.SuperAdmin;
import framework.features.common.Login;
import framework.features.systemManagement.CurrencyProviderMapping;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.pageObjects.bankMaster.AddBank_Page1;
import framework.pageObjects.bankMaster.DeleteBank_Page1;
import framework.util.common.Assertion;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.ConfigInput;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

import java.util.List;


/**
 * Addition of Banks as Trust and Non Trust: MON-3346
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 15       Done           Test01
 * 16       Done           Test02
 * 19       Done           Test03
 */
public class Suite_MON_3346_AddBankTrustNonTrust_N extends TestInit {
    private SuperAdmin saAddBank, saDeleteBank;
    private Bank trustBank;
    private CurrencyProvider defaultProvider;

    /**
     * TearDown
     *
     * @throws Exception
     */

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {

        ExtentTest t1 = pNode.createNode("Setup", "setup specific for this test");
        try {
            SystemPreferenceManagement.init(t1)
                    .updateSystemPreference("IS_POOL_ACC_REQ", "N");

            // Create a Trust Bank Object which will be deleted inTearDown
            trustBank = new Bank(defaultProvider.ProviderId,
                    Constants.TEMP_BANK_TRUST,
                    null,
                    null,
                    true,
                    false);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }


    /**
     * Verify that when IS_POOL_ACC_REQ = 'N' and pool account number
     * is provided during Bank Creation then the Pool Account Type and CBS type are mandatory
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_3346_01", "Verify that when IS_POOL_ACC_REQ = 'N' and pool account number" +
                "is provided during Bank Creation then the Pool Account Type and CBS type are mandatory")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        try {
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_ADD");

            AddBank_Page1 page = new AddBank_Page1(t1);

            page.navAddBank();
            page.selectProviderName(trustBank.ProviderName);
            page.setBankName(trustBank.BankName);

            if (AppConfig.isBankIdRequired) {
                page.setBankId(trustBank.BankID);
            } else {
                t1.info("Bank Id is not required!");
            }

            // set the Pool Account Number,
            page.setPoolAccountNumber(trustBank.PoolAccNum);

            if (ConfigInput.isCoreRelease) {
                page.selectBankType(trustBank.TrustType);
            }

            if (AppConfig.isBankBranchCSVUploadRequired) {
                // upload the file
                page.uploadBranchCSV(trustBank.BranchFileCSV);
            }

            page.clickOnSubmit();

            // Expecting error
            Assertion.verifyErrorMessageContain("Bank.validation.PoolAccType.select",
                    "Verify PoolAccount Type is mandatory when IS_POOL_ACC_REQ = 'N' and yet pool account number is provided", t1);

            Assertion.verifyErrorMessageContain("Bank.validation.cbsType.select",
                    "Verify CBS Type is mandatory when IS_POOL_ACC_REQ = 'N' and yet pool account number is provided", t1);
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }

    /**
     * Able to add Bank without providing  Pool Details when " +
     * IS_POOL_ACC_REQ = 'N'
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("MON_3346_02", "Able to add Bank without providing  Pool Details when " +
                "IS_POOL_ACC_REQ = 'N'")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        try {
            Login.init(t2)
                    .loginAsSuperAdmin("BANK_ADD");

            AddBank_Page1 page = new AddBank_Page1(t2);

            page.navAddBank();
            page.selectProviderName(trustBank.ProviderName);
            page.setBankName(trustBank.BankName);

            if (AppConfig.isBankIdRequired) {
                page.setBankId(trustBank.BankID);
            } else {
                t2.info("Bank Id is not required!");
            }

            // Do Not Provide the Pool Account details

            if (ConfigInput.isCoreRelease) {
                page.selectBankType(trustBank.TrustType);
            }

            if (AppConfig.isBankBranchCSVUploadRequired) {
                // upload the file
                page.uploadBranchCSV(trustBank.BranchFileCSV);
            }

            page.clickOnSubmit();

            Assertion.verifyActionMessageContain("Bank.branches.added.successfully", "Add Bank", t2);
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that Bank Created without Pool account is available under Bank deletion Page
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("MON_3346_03", "Verify that Bank Created without Pool account " +
                "is available under Bank deletion Page")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        try {
            Login.init(t3)
                    .loginAsSuperAdmin("BANK_DELETE");

            // navigate and get the list of banks available for deletion
            List<String> banksForDeletion = new DeleteBank_Page1(t3)
                    .navDeleteBank()
                    .getBankOptionsfromSelect();

            Assertion.verifyListContains(banksForDeletion, trustBank.BankName, "Verify that Bank Created without Pool account " +
                    "is available under Bank deletion Page", t3);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }
        Assertion.finalizeSoftAsserts();

    }

    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t1 = pNode.createNode("MON_3346_04", "Set the preference IS_POOL_ACC_REQ to Y, delet Banks")
                .assignCategory(FunctionalTag.ADD_BANK_TRUST_NONTRUST);

        try {
            // Login as Superadmin and delete the Bank
            Login.init(t1)
                    .loginAsSuperAdmin("BANK_DELETE");

            CurrencyProviderMapping.init(t1)
                    .deleteBank(trustBank.BankName);

        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }
        Assertion.finalizeSoftAsserts();

    }


}
