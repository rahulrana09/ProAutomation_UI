package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.dataEntity.CurrencyProvider;
import framework.dataEntity.TxnResponse;
import framework.entity.SavingsClub;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.apiManagement.Transactions;
import framework.features.savingClubManagement.SavingsClubManagement;
import framework.features.systemManagement.Preferences;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.common.SMSReader;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import framework.util.globalConstant.Wallets;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import framework.util.globalVars.GlobalData;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Modify Club: MON-3440
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 1       Done
 * NOTE - for Debugging Techniques used for this script Scroll to Bottom
 */
public class Suite_MON_3440_ResignAndDisbursefund extends TestInit {
    private User subsChairman, subsMember1, subsMember2;
    private SavingsClub sClubToResign;


    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        // Extent Test
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific for Resign and Disburse Funds. " +
                "Make Sure that Service Charge CLUB DEPOSIT is configured. " +
                "Create Chairman User and also create the Saving Club for testing")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // get Default Provider
            CurrencyProvider defaultProvider = DataFactory.getDefaultProvider();
            String bankId = DataFactory.getBankId(GlobalData.defaultBankName);

            // User Objects
            subsChairman = new User(Constants.SUBSCRIBER);
            subsMember1 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 0);
            subsMember2 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER, 1);

            // Map SVC wallet Preference
            Preferences.init(s1)
                    .setSVCPreferences(subsChairman, bankId);

            // Savings Club object
            sClubToResign = new SavingsClub(subsChairman, Constants.CLUB_TYPE_PREMIUM, defaultBank.BankID, false, false);

            //Set the Min member to 2 and max member to 3
            sClubToResign.setMemberCount(2, 3);

            //Set min Approver count to 2 and Max approver Count to 3
            sClubToResign.setApproverCount(2, 2);

        /*
         mark the users as approver
         As max approver count is set to 2,, first member including the Chairman will become automatically the Approver
         Mark them as Approver
          */
            subsMember1.setAsSvaApprover();

            // Add Member to the Club Object
            sClubToResign.addMember(subsMember1);
            sClubToResign.addMember(subsMember2);

            // ServiceCharge Object
            ServiceCharge cDeposit = new ServiceCharge(Services.CLUB_DEPOSIT,
                    subsMember1,
                    subsChairman,
                    null,
                    DataFactory.getWalletUsingAutomationCode(Wallets.SAVINGCLUB).WalletId,
                    null,
                    null);

            /**
             P R E   R E Q U I S I T E    C R E A T I O N
             */

            //Create Chairman Subscribers with Default Mapping
            SubscriberManagement.init(s1)
                    .createSubscriberDefaultMapping(subsChairman, true, true);
            //Configure Service Charge For Club Deposit
            ServiceChargeManagement.init(s1)
                    .configureServiceCharge(cDeposit);

            // Create Saving Club
            SavingsClubManagement.init(s1)
                    .createSavingsClub(sClubToResign);

            // Join Member 1 and make SVA deposit
            // Member 1 will be automatically become the Approver
            Transactions.init(s1)
                    .joinOrResignSavingClub(sClubToResign, subsMember1, true)
                    .verifyStatus(Constants.TXN_SUCCESS);

            // deposit from Chairman to make it Active
            Transactions.init(s1)
                    .depositSavingClub(sClubToResign, subsChairman, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);

            // deposit for member 1 to make it active
            Transactions.init(s1)
                    .depositSavingClub(sClubToResign, subsMember1, AppConfig.minSVCDepositAmount.toString())
                    .verifyStatus(Constants.TXN_SUCCESS);

            // Join Member 2 and do not do SVA Deposit
            // Member 2 will not be the Approved as the Max approver Count has reached
            TxnResponse rJoin2 = Transactions.init(s1)
                    .joinOrResignSavingClub(sClubToResign, subsMember2, true)
                    .verifyStatus(Constants.TXN_SUCCESS);
        } catch (Exception e) {
            markSetupAsFailure(e);
        }
        Assertion.finalizeSoftAsserts();
    }

    /**
     * Verify that when minimum approver count limit is reached
     * As in setup we have set the min approver / Max approver =  2, 2
     * and we only have two members in the club
     * both of which are approver by default (members added till max num of approver reached
     * automatically becomes the approver)
     * Resign should not happen
     * and an error is expected as Minimum number of approver has reached
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("Test 01", "subsMember1 rejected and member count < min member count , " +
                "subsMember1 status should be AI")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // resign Saving Club
            TxnResponse response = Transactions.init(t1)
                    .joinOrResignSavingClub(sClubToResign, subsMember1, false)
                    .verifyStatus("9999189")
                    .verifyMessage("savingclub.min.approver.limit.reached");

            // verify that member receive the relevant message
            SMSReader.init(t1)
                    .verifyRecentNotification(subsMember1.MSISDN, "savingclub.min.approver.limit.reached");
        } catch (Exception e) {
            markTestAsFailure(e, t1);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 02
     * Verify that A member with Status NOT Active could not be resigned
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("Test 02", "Verify that A member with Status NOT Active could not be resigned")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // resign Saving Club
            TxnResponse response = Transactions.init(t2)
                    .joinOrResignSavingClub(sClubToResign, subsMember2, false)
                    .verifyStatus("9999195")
                    .verifyMessage("savingclub.resign.add.initiate");

            SMSReader.init(t2)
                    .verifyRecentNotification(subsMember2.MSISDN, "savingclub.resign.add.initiate");
        } catch (Exception e) {
            markTestAsFailure(e, t2);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 03
     * Verify Successful Resignation
     * <p>
     * make the SVC deposit for Member 2,
     * Now the member is active and it's also not the approver so This user could be deleted Successfully
     *
     * @throws Exception
     */
    @Test(priority = 3, groups = {FunctionalTag.MONEY_SMOKE})
    public void test_03() throws Exception {
        ExtentTest t3 = pNode.createNode("Test 03", "Resign a Member")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // deposit, SVC amount so the User is now Active
            Transactions.init(t3)
                    .depositSavingClub(sClubToResign, subsMember2, AppConfig.minSVCDepositAmount.toString());

            // resign Saving Club
            Transactions.init(t3)
                    .joinOrResignSavingClub(sClubToResign, subsMember2, false)
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("savingclub.resign.successfully", sClubToResign.ClubName, sClubToResign.ClubId);

            SMSReader.init(t3)
                    .verifyRecentNotification(subsMember2.MSISDN, "savingclub.resign.successfully", sClubToResign.ClubName, sClubToResign.ClubId);
        } catch (Exception e) {
            markTestAsFailure(e, t3);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 04
     * Verify that Resigned Member can not avail any Club Services
     * Should not be able to Deposit or withdraw
     *
     * @throws Exception
     */
    @Test(priority = 4)
    public void test_04() throws Exception {
        ExtentTest t4 = pNode.createNode("Test 04", "Verify that Resigned Member can not avail any Club Services")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            String amt = DataFactory.getRandomNumber(2) + "";
            // verify that Resigned user cannot deposit to SVC
            Transactions.init(t4)
                    .depositSavingClub(sClubToResign, subsMember2, amt)
                    .verifyMessage("savingclub.user.not.associated");

            // verify that resigned user cannot withdraw from SVC
            Transactions.init(t4)
                    .svcWithdrawCash(sClubToResign.ClubId, subsMember2.MSISDN, amt)
                    .verifyMessage("savingclub.user.not.associated");
        } catch (Exception e) {
            markTestAsFailure(e, t4);
        }

        Assertion.finalizeSoftAsserts();
    }

    /**
     * Test 05
     * Verify that after resignation from club user is still
     * be able to receive any payables through club settlement service
     * NWADM7792046134
     * Descoped
     * @throws Exception
     */
    /*@Test(priority = 5)
    public void test_05() throws Exception {
        ExtentTest t5 = pNode.createNode("Test 05", "Verify that after resignation from club user is " +
                "still be able to receive any payables through club settlement service ")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        String disbursementAmt = "10";

        UsrBalance subPreBal = MobiquityGUIQueries.getUserBalance(subsMember2, null, null);

        // Perform SVC Disbursement
        TxnResponse response = Transactions.init(t5)
                .svcStockDisbursement(sClubToResign, subsMember2.MSISDN, disbursementAmt)
                .verifyStatus(Constants.TXN_SUCCESS)
                .verifyMessage("savingclub.disbursement.initiated");

        // for the Disbursement to be approved, it has to be approved by minimum number of approvers
        for (User member : sClubToResign.getMembers()) {
            if (member.isSvaApprover) {
                Transactions.init(t5)
                        .svcStockDisbursementConfirm(sClubToResign.ClubId, member.MSISDN, disbursementAmt, response.TxnId)
                        .verifyStatus(Constants.TXN_SUCCESS)
                        .verifyMessage("savingclub.disbursement.approved");
            }
        }

        UsrBalance subPostBal = MobiquityGUIQueries.getUserBalance(subsMember2, null, null);

        t5.info("PreBalance: " + subPreBal.Balance + " / PostBalance: " + subPostBal.Balance);
        if (subPreBal.Balance.compareTo(subPostBal.Balance) < 0) {
            t5.pass("The Member Balance is Credited after SVC Withdrawal");
        } else {
            t5.fail("Member's Balance is not Credited");
        }

        Assertion.finalizeSoftAsserts();
    }*/

    /**
     * Test 06 // todo
     * Verify that the resigned user can re join the Club
     * Verify that its services are resumed, and also stock disbursement is now Not valid
     *
     * @throws Exception
     */
    @Test(priority = 6)
    public void test_06() throws Exception {
        ExtentTest t6 = pNode.createNode("Test 06", "Verify that the resigned user can re join the Club")
                .assignCategory(FunctionalTag.SAVING_CLUB);

        try {
            // re join the Club
            Transactions.init(t6)
                    .joinOrResignSavingClub(sClubToResign, subsMember2, true)
                    .verifyStatus(Constants.TXN_SUCCESS);

            // re Joined User can Deposit amount, and First time Deposit should not be less than the Min SVC Deposit Amount
            Transactions.init(t6)
                    .depositSavingClub(sClubToResign, subsMember2, "1") // deposit Amount Less than Min SVC Deposit
                    .verifyStatus("9999197")
                    .verifyMessage("savingclub.min.deposit", AppConfig.minSVCDepositAmount.toString());

            // deposit a valid amount and it should be successful
            Transactions.init(t6)
                    .depositSavingClub(sClubToResign, subsMember2, AppConfig.minSVCDepositAmount.toString()) // deposit Correct Amount
                    .verifyStatus(Constants.TXN_SUCCESS)
                    .verifyMessage("savingclub.deposit.confirmation");

            // verify the Notification message
            SMSReader.init(t6)
                    .verifyRecentNotification(subsMember2.MSISDN, "savingclub.deposit.confirmation.msg");

            //Descoped
        /* // verify that resigned user cannot receive Amount by Stock Disbursement
        Transactions.init(t6)
                .svcStockDisbursement(sClubToResign, subsMember2.MSISDN, DataFactory.getRandomNumber(2) + "")
                .verifyStatus(Constants.TXN_SUCCESS) // todo  this must fail
                .verifyMessage("savingclub.disbursement.initiated"); // todo this must fail*/
        } catch (Exception e) {
            markTestAsFailure(e, t6);
        }

        Assertion.finalizeSoftAsserts();
    }


    /**
     * Debugging techniques:
     *
     * After Creating and User, write the user to xlsx file
     * eg. subsChairman.writeDataToExcel();
     *
     * Now if you are running second time u can fetch the user easily by providing its msisdn
     * to get the msisdn refer ChannelUser.xlsx sheet
     *
     * subsChairman = DataFactory.getUserUsingMsisdn("<MSISDN>")
     *
     *
     * Same goes for a Saving Club
     * sClubToResign.writeDataToExcel(); ->> write to xlsx
     *
     * use below code to fetch the Saving club from TempSavingClu.xlsx
     * sClubToResign = DataFactory.getSavingClubFromAppdata()
     */

}