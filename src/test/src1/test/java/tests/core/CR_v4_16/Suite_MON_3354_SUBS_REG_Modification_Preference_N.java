package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.pageObjects.subscriberManagement.AddSubscriber_Page1;
import framework.pageObjects.subscriberManagement.ModifySubscriber_page3;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalVars.AppConfig;
import framework.util.globalVars.FunctionalTag;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Subscriber Registration Modification - MON- 3354 Summary
 * -----------------------------------
 * JIRA     STATUS          TEST CASE
 * -----------------------------------
 * 1        Done            test_01
 * 2        Done            test_02
 * 11       Done            test_03
 * 13       Existing
 * 14       Duplicate
 */
public class Suite_MON_3354_SUBS_REG_Modification_Preference_N extends TestInit {
    private User chAddSubs, whsBaseSetUsr, subs_01;

    @AfterClass(alwaysRun = true)
    public void tearDownTest() throws Exception {
        SystemPreferenceManagement.init(pNode)
                .updateSystemPreference("IS_ID_REQUIRED", "Y");

        // RELOAD THE APPCONFIG, so that other cases also access the current state of preferences
        AppConfig.init();
    }

    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Update System Preference IS_ID_REQUIRED. " +
                "Make sure that the Channel User for Test is having sufficient Balance")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        // Set the Preference Value to 'N'
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("IS_ID_REQUIRED", "Y");
        SystemPreferenceManagement.init(s1)
                .updateSystemPreference("IS_ID_REQUIRED", "N");

        // Users for Test
        chAddSubs = DataFactory.getChannelUserWithAccess("SUBSADD");
        whsBaseSetUsr = DataFactory.getChannelUserWithAccess("CIN_WEB");
        subs_01 = new User(Constants.SUBSCRIBER);

        // Make sure that the Wholesaler has enough Balance For Cash in
        TransactionManagement.init(s1)
                .makeSureChannelUserHasBalance(whsBaseSetUsr);

    }

    /**
     * Verify that Identification Number field
     * is non-mandatory when IS_ID_REQUIRED = N in the system while
     * subscriber registration through web by channel user and subscriber is
     * successfully registered without entering identification number
     *
     * @throws Exception
     */
    @Test(priority = 1, enabled = false) //Functionality Changed in 5.0.
    public void test_01() throws Exception {
        ExtentTest t1 = pNode.createNode("TEST 01", "Verify that Identification Number field " +
                "is non-mandatory when IS_ID_REQUIRED = N in the system while " +
                "subscriber registration through web by channel user and subscriber is " +
                "successfully registered without entering identification number")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        subs_01.ExternalCode = ""; // Set the external code to empty

        SubscriberManagement.init(t1)
                .createSubscriberDefaultMapping(subs_01, true, false);
    }

    /**
     * Verify that Identification Number field is " +
     * non-mandatory when IS_ID_REQUIRED = N in the system while subscriber " +
     * modification through web by channel user and subscriber is successfully " +
     * modified without entering identification number
     *
     * @throws Exception
     */
    @Test(priority = 2, dependsOnMethods = "test_01", enabled = false) //Functionality Changed in 5.0.
    public void test_02() throws Exception {
        ExtentTest t2 = pNode.createNode("TEST 02", "Verify that Identification Number field is " +
                "non-mandatory when IS_ID_REQUIRED = N in the system while subscriber " +
                "modification through web by channel user and subscriber is successfully " +
                "modified without entering identification number")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        Login.init(t2).login(chAddSubs);

        // Initiate Subscriber Modification
        SubscriberManagement.init(t2)
                .initiateSubscriberModification(subs_01);

        // Set the Identification number as Nothing
        AddSubscriber_Page1.init(t2)
                .setIdentificationNumber("")
                .modifyClickNext();

        // Verify that the User Creation has moved to the Next page
        Assertion.assertEqual(Assertion.isErrorInPage(t2), false,
                "Verify no error on Page", t2);

        if (ModifySubscriber_page3.init(t2).isNextButtonShown()) {
            t2.pass("Verify that Identification number is Not mandatory when Modifying Subscriber as IS_ID_REQUIRED = 'N'");
        } else {
            t2.fail("Failed to verify that Identification number is Not mandatory when Modifying Subscriber as IS_ID_REQUIRED = 'N'");
        }
    }

    /**
     * Verify that when identification number Not is mandatory while
     * on-boarding then during any transaction (say Cash In,
     * whereas for Cash In service identification number check is mandatory)
     * user should enter the ID number
     * " and the particular transaction should be successful
     *
     * @throws Exception
     */
    @Test(priority = 3) //id num and identification number both are same in 5.0
    public void test_03() throws Exception {
        ExtentTest t4 = pNode.createNode("TEST 03", "Verify that when identification number is Not mandatory while " +
                "on-boarding then during any transaction (say Cash In, whereas for Cash In service " +
                "identification number check is mandatory) user should enter the ID number " +
                "and the particular transaction should be successful")
                .assignCategory(FunctionalTag.SUBS_REG_MOD);

        Login.init(t4).login(whsBaseSetUsr); // login as user with Cash In permission

        subs_01 = DataFactory.getChannelUserWithCategory(Constants.SUBSCRIBER);
        subs_01.setExternalCode("");
        TransactionManagement.init(t4)
                .performCashInUsingIDnum(subs_01, "1");
    }
}
