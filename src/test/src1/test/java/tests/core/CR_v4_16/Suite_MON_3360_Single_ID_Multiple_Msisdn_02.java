package tests.core.CR_v4_16;

import com.aventstack.extentreports.ExtentTest;
import framework.entity.OperatorUser;
import framework.entity.ServiceCharge;
import framework.entity.User;
import framework.features.common.Login;
import framework.features.systemManagement.ServiceChargeManagement;
import framework.features.systemManagement.SystemPreferenceManagement;
import framework.features.transactionManagement.TransactionManagement;
import framework.features.userManagement.SubscriberManagement;
import framework.util.common.Assertion;
import framework.util.common.DataFactory;
import framework.util.dbManagement.MobiquityGUIQueries;
import framework.util.globalConstant.Constants;
import framework.util.globalConstant.Services;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import tests.core.base.TestInit;

/**
 * Created by gurudatta.praharaj on 12/19/2017.
 */
public class Suite_MON_3360_Single_ID_Multiple_Msisdn_02 extends TestInit {

    private OperatorUser payer, channelAdmin, o2cInitiator;
    private User channelUser;

    /*
         setting up KYC preforence , acq fee and FREQ_EXT_CODE_N_SUBS to 1
    */
    @BeforeClass(alwaysRun = true)
    public void setup() throws Exception {
        ExtentTest s1 = pNode.createNode("Setup", "Setup Specific to test Single id Multiple MSISDN. " +
                "Make sure O2C service charge is configured. " +
                "update the system preference FREQ_EXT_CODE_N_SUBS = Y. " +
                "update the preference FREQ_EXT_CODE_N_SUBS = 2");

        payer = new OperatorUser(Constants.NETWORK_ADMIN);
        channelAdmin = DataFactory.getOperatorUserWithAccess("PTY_ACU");
        channelUser = DataFactory.getChannelUserWithCategory(Constants.WHOLESALER);
        o2cInitiator = DataFactory.getOperatorUserWithAccess("O2C_INIT");


        ServiceCharge sCharge = new ServiceCharge(Services.O2C, payer, channelUser, null, null, null, null);

        ServiceChargeManagement.init(s1)
                .configureServiceCharge(sCharge);
        // set the default value of FREQ_EXT_CODE_N_SUBS, either set it to 1 or 2 based on test
        MobiquityGUIQueries.updateDisplayAndModifiedAllowedFromGUI("FREQ_EXT_CODE_N_SUBS", "Y");

        SystemPreferenceManagement.init(s1)
                .updateSystemPreference("FREQ_EXT_CODE_N_SUBS", "2");


    }

    /*
    Verify that transactions are successful for the multiple
    subscribers having the same identification number
     */
    @Test
    public void test_1() throws Exception {
        ExtentTest t1 = pNode.createNode("Test_01", "Verify that transactions are successful for the multiple subscribers having the same identification number ");

        //subscriber 01
        User subsUser_01 = new User(Constants.SUBSCRIBER);
        SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(subsUser_01);

        //creating self subscriber 02
        User subuser_02 = new User(Constants.SUBSCRIBER);

        //setting subscriber 02 identification number with subscriber 01 identification number
        subuser_02.setExternalCode(subsUser_01.ExternalCode);
        SubscriberManagement.init(t1).createDefaultSubscriberUsingAPI(subuser_02);

        //login as channel user

        Login.init(t1).login(channelUser);

        Login.init(t1).login(o2cInitiator);

        TransactionManagement.init(t1).initiateAndApproveO2CWithProvider(channelUser, DataFactory.getDefaultProvider().ProviderName, "200");

        Login.init(t1).login(channelUser);

        //perform cash in to subscriber 01
        TransactionManagement.init(t1).performCashIn(subsUser_01, "1", null);
        Assertion.assertMessageContain("cashin.success.msg", "CASHIN done successfully to Transaction id:", "cash in to sub1", t1);

        //perform cashin for subscriber 02
        TransactionManagement.init(t1).performCashIn(subuser_02, "1", null);
        Assertion.assertMessageContain("cashin.success.msg", "CASHIN done successfully to Transaction id:", "cash in to sub2", t1);
    }
}
