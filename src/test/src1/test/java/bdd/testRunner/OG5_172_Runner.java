package bdd.testRunner;

import bdd.utility.AfterSuite;
import bdd.utility.BeforeSuite;
import bdd.utility.ExtendedCucumberRunner;
import cucumber.api.CucumberOptions;
import framework.util.dbManagement.OracleDB;
import framework.util.reportManager.ExtentManager;
import org.junit.runner.RunWith;
import tests.core.base.TestInit;

import java.io.IOException;

@RunWith(ExtendedCucumberRunner.class)
@CucumberOptions(
        format = {"pretty", "html:target/html/"}
        , features = "src/test/java/bdd/features/OG5-172.feature"
        , glue = {"bdd.stepDefinations"}
        , tags = "~@Ignore"
)

public class OG5_172_Runner {
    private static TestInit init;

    @BeforeSuite
    public static void setUp() throws Exception {
        //ExtentManager.setFileName("CucumberTest-OG5-172.html");
        init = new TestInit();
        init.suiteInit();
    }

    @AfterSuite
    public static void tearDown() throws IOException {
        OracleDB.CloseConnection();
        init.deInitSuite();
    }
}
